</div>
<!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>

<!-- Bootstrap Js -->
<script src="../assets/js/bootstrap.min.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="../assets/js/dataTables/jquery.dataTables.js"></script>
<script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
<script>
       $(document).ready(function () {

           $('#dataTables-example').dataTable();
           $('#dataTables-example2').dataTable();
           $('#dataTables-example3').dataTable();

          $("#hide").on('click', function() {
            $("#element").hide();
            $("#titulo2").hide();
            $("#titulo").show();
            $("#titulo3").hide();
            $("#element2").show();
            $("#element3").show();
            $("#element4").hide();
            $("#linea").hide();
            return false;
          });
          $("#hide2").on('click', function() {
            $("#element").hide();
            $("#titulo2").hide();
            $("#titulo").show();
            $("#titulo3").hide();
            $("#element2").show();
            $("#element3").hide();
            $("#element4").hide();
            return false;
          });
          $("#show").on('click', function() {
            $("#element").show();
            $("#element2").hide();
            $("#element3").hide();
            $("#titulo").hide();
            $("#titulo2").show();
            $("#linea").show();
            return false;
          });
          $("#show2").on('click', function() {
            $("#element3").show();
            $("#element4").show();
            $("#element2").hide();
            $("#titulo").hide();
            $("#titulo2").hide();
            $("#titulo3").show();
            return false;
          });
          $("#show3").on('click', function() {
            $("#element3").hide();
            $("#element4").hide();
            $("#element2").hide();
            $("#element").show();
            $("#titulo").hide();
            $("#titulo2").show();
            $("#titulo3").hide();
            return false;
          });

       });
  </script>
<?php
 if ($ntas==0): ?>
<script type="text/javascript">
  $(document).ready(function()
  {
     $("#myModal").modal("show");
  });
</script>
<?php endif; ?>
</body>
</html>
