<?php
  include "../Funciones/BD.php";
  #Perfil de Usuario
  $sql_per="SELECT su.usu_id,su.usu_iden,su.usu_nombres_apellido,su.usu_nomusu,su.usu_password,su.gru_id, sg.gru_nombre
      FROM sys_usuarios su, sys_usu_grupos sg WHERE usu_iden='$usuid' AND sg.gru_id = su.gru_id; ";
  $result_per=mysqli_query($con,$sql_per);
  $rsql_per=mysqli_fetch_array($result_per,MYSQLI_ASSOC);
  $xdni = $rsql_per['usu_iden'];
  $xnom = $rsql_per['usu_nombres_apellido'];
  $xgru = $rsql_per['gru_nombre'];
  $xusu = $rsql_per['usu_nomusu'];
  $xpass = $rsql_per['usu_password'];
  $id =  $rsql_per['usu_id'];

 ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-lg-12">
          <div class="row" id="titulo">
            <div class="col-lg-6">
              <h2 class="azul"><i class="fa fa-user fa-fw"></i><strong>Perfil</strong></h2>
            </div>
          </div>
        </div>
        <div class="table-responsive col-lg-12"><hr class="black" /></div>
           <form  id="RegistroC" role="form" action="index.php?menu=1" method="post">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <input type="hidden" name="xdni" value="<?php echo $xdni; ?>">
            <input type="hidden" name="xusu" value="<?php echo $xusu; ?>">
            <div class="form-group col-xs-12 col-md-12 col-lg-2">
              <label>DNI:</small></label>
              <input class="form-control" placeholder="000000000" name="xdni" value="<?php echo $xdni; ?>" disabled>
            </div>
            <div class="form-group col-xs-12 col-md-12 col-lg-4">
                <label>NOMBRES Y APELLIDOS:</label>
                <input class="form-control text-uppercase" name="xnom" value="<?php echo $xnom; ?>"  disabled>
            </div>
            <div class="form-group col-xs-12 col-md-12 col-lg-3">
               <label>GRUPOS:</label>
              <select class="form-control"  name="xgru" disabled>
                <?php echo '<option value="'.$xgru.'">'.$xgru.'</opcion>'; ?>
             </select>
            </div>
            <div class="form-group col-xs-12 col-md-12 col-lg-3">
                  <label>USUARIO:</label>
                  <input class="form-control" name="xusu" value="<?php echo $xusu; ?>"  disabled>
            </div>
            <div class="form-group col-xs-12 col-md-12 col-lg-3">
                  <label>CLAVE ANTERIOR:</label>
                  <input type="password" class="form-control" name="xpass2" value="<?php echo $xpass; ?>"  disabled>
            </div>
            <div class="form-group col-xs-12 col-md-12 col-lg-3">
                  <label>CLAVE NUEVA:</label>
                  <input type="password" class="form-control" placeholder="Clave" name="xpass" pattern="[0-9.- ].{4,}" required>
            </div>
          <div class="col-lg-4 col-md-4 hidden-xs top">
            <button type="submit" class="btn btn-primary" name="actualiza" value="act">Actualizar </button>
            <button type="reset" class="btn btn-default">Limpiar</button>
          </div>
          <div class="col-xs-12 visible-xs top">
            <button type="submit" class="btn btn-primary" name="actualiza" value="act">Actualizar </button>
            <button type="reset" class="btn btn-default">Limpiar</button>
          </div>
        </form>
        </div> <!--- pbody -->
    </div> <!--- pdefault -->
</div><!-- /.col-lg-12 -->
</div>
