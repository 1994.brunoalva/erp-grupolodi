<?php
error_reporting(E_ERROR | E_PARSE);
include "../../funciones/BD.php";
$empid = $_GET['emp'];
$sqlemp = "SELECT emp_ruc,emp_nombre,Nmes('$xmes') as nmes FROM sys_empresas WHERE emp_id='$empid'";
$remp = mysqli_query($con,$sqlemp);
$aemp = mysqli_fetch_array($remp,MYSQLI_ASSOC);
$emp_nom = $aemp['emp_nombre'];
$emp_ruc = $aemp['emp_ruc'];
$mes_nom = $aemp['nmes'];
$xfecha = date('d/m/Y');
$xanual = date('Y');
$xanual_ant = $xanual - 1;
$xanual_ant2 = $xanual - 2;

$sqlproductos = "SELECT *,DATE_FORMAT(dep_fcompra,'%d/%m%Y') as fcompra,Deprecia(dep_id,'0','D') as depreciacion,Deprecia(dep_id,'0','P') as periodo
, Deprecia(dep_id,'1','A') as c1,Deprecia(dep_id,'2','A') as c2 FROM sys_conta_depreciacion WHERE emp_id='$empid' AND dep_estatus='ACTIVO'";
$rproductos = mysqli_query($con,$sqlproductos);
?>
<style>
  .p1 {
    line-height: 200%;
    text-align: justify;
  }
  .Estilo0 {font-size: 12px; font-weight: bold; }
  .Estilo1 {font-size: 11px; font-weight: bold; }
  .Estilo01 {font-size: 10px; }
  .Estilo13 {font-size: 11px; font-weight: bold; }
  .tborde {
    border: 1.1px solid black;
  }
</style>
<page orientation="landscape" style="font-size: 7px" backtop="1mm" backbottom="1mm" backleft="1mm" backright="1mm">
    <page_header>
      <table  border="0">
        <tr>
          <td width="200"><span class="Estilo0"><?=$emp_nom;?></span></td>
          <td width="650">&nbsp;</td>
          <td width="200" align="right"><em>Fecha: </em><strong><?=$xfecha;?></strong></td>
        </tr>
        <tr>
          <td><span class="Estilo0">RUC: <?=$emp_ruc;?></span></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td class="tborde"><span class="Estilo0">CUENTA: 33</span></td>
          <td class="tborde"><span class="Estilo0">INMUEBLE MAQUINARIA Y EQUIPO - <?=$xanual_ant;?></span></td>
          <td></td>
        </tr>
      </table>&nbsp;
      <table width="791" height="30" border="0">
  <tr>
    <td colspan="6">&nbsp;</td>
    <td  class="tborde" align="center"><span class="Estilo01">DREPRECIACION</span></td>
    <td colspan="3" class="tborde" align="center"><span class="Estilo01">DEPRECIACION, ACUMULADOS Y AGOTAMIENTO ACUMULADO </span></td>
  </tr>
  <tr>
    <td width="80" class="tborde" align="center"><span class="Estilo13">FECHA DE COMPRA </span></td>
    <td width="59" class="tborde" align="center"><span class="Estilo13">CODIGO CTA </span></td>
    <td width="270" class="tborde" align="center"><span class="Estilo13">DESCRIPCION </span></td>
    <td width="59" class="tborde" align="center"><span class="Estilo13">VALOR ACTUAL 31-12-<?=$xanual_ant2;?> </span></td>
    <td width="59" class="tborde" align="center"><span class="Estilo13">COMPRAS</span></td>
    <td width="59" class="tborde" align="center"><span class="Estilo13">VALOR ACTUAL 31-12-<?=$xanual_ant;?> </span></td>
    <td width="95" class="tborde" align="center"><span class="Estilo13">DEPRECIACION TOTAL 31-12-<?=$xanual_ant;?></span></td>
    <td width="20" class="tborde" align="center"><span class="Estilo13"><br>TASA A DEPREC.% </span></td>
    <td width="30" class="tborde" align="center"><span class="Estilo13"><br>DEPREC. DEL PERIODO </span></td>
    <td width="30" class="tborde" align="center"><span class="Estilo13">DEPRECIACION TOTAL </span></td>
  </tr>
  <?php while($row=mysqli_fetch_array($rproductos,MYSQLI_ASSOC)){ ?>
  <tr>
    <td class="tborde" align="center"><span class="Estilo01"><?=$row['fcompra']?></span></td>
    <td class="tborde" align="center"><span class="Estilo13"><?=$row['plade_codrela']?></span></td>
    <td class="tborde" align="center"><span class="Estilo01"><?=$row['dep_descripcion'];?></span></td>
    <td class="tborde" align="center"><span class="Estilo01"><?=number_format($row['c2'], 2,",",".");
    $corte2=$row['c2'];
    $acum_c2 = $acum_c2 + $corte2;?></span></td>
    <td class="tborde" align="center">&nbsp;</td>
    <td class="tborde" align="center"><span class="Estilo01"><?=number_format($row['c1'], 2,",",".");?></span></td>
    <td class="tborde" align="center"><span class="Estilo01"><?=number_format($row['c1'], 2,",",".");
    $corte1=$row['c1'];
    $acum_c1 = $acum_c1 + $corte1;
    ?></span></td>
    <td class="tborde" align="center"><span class="Estilo01"><?=$row['dep_tasa'].' %';?></span></td>
    <td class="tborde" align="center"><span class="Estilo01"><?=number_format($row['periodo'], 2,",",".");
    $perido=$row['periodo'];
    $acum_per = $acum_per +$perido;
    ?></span></td>
    <td class="tborde" align="center"><span class="Estilo01"><?=number_format($row['depreciacion'], 2,",",".");
    $depre = $row['depreciacion'];
    $acum_depre = $acum_depre + $depre;
    ?></span></td>
  </tr>
<?php } ?>
</table>
<table width="956" class="tborde">
  <tr>

    <td width="436" align="right"><span class="Estilo13">TOTAL:</span></td>
    <td width="59" align="center"><span class="Estilo13"><?=number_format($acum_c2, 2,",",".");?></span></td>
    <td width="70">&nbsp;</td>
    <td width="59" align="center"><span class="Estilo13"><?=number_format($acum_c1, 2,",",".");?></span></td>
    <td width="95">&nbsp;</td>
    <td width="100">&nbsp;</td>
    <td width="100" align="center"><span class="Estilo13"><?=number_format($acum_per, 2,",",".");?></span></td>
    <td width="96" align="center"><span class="Estilo13"><?=number_format($acum_depre, 2,",",".");?></span></td>
  </tr>
</table>

    </page_header>
</page>
