<?php
  error_reporting(E_ERROR | E_PARSE);
  include "../../../funciones/BD.php";
  $opc=$_GET['opcion'];
  $otrop=$_GET['otrop'];
    if ($otrop=='') {
      $otrop = '0.00';
    }
  $oruc=$_GET['xruc'];
    if ($oruc=='') {
      $oruc='0';
    }
  $onom=$_GET['xnom'];
  if ($onom=='') {
    $onom='NULL';
  }
  $otipo=$_GET['aggr'];
  #EMPRESA QUE GENERA EL REPORTE
  $empid= $_GET['empid'];
  $id =$_GET['id'];
  $periodo = $_GET['xper'];
  if ($periodo=='') {
    $periodo=date("Y");
  }
  $diar=date("d");
  $mes=date("m");

  if ($otipo=='new') {
    #buscar si existe el registro
    $sqlb="SELECT per_ndoc FROM sys_rh_personal_otros WHERE per_ndoc='$id' and ot_periodo='$periodo'";
    $rbusca=mysqli_query($con,$sqlb);
    $rsqlb=mysqli_fetch_array($rbusca,MYSQLI_ASSOC);
    $rb=$rsqlb['per_ndoc'];
      if ($rb=='') {
        $fecr=date("Y-m-d");
        #AGREGAR ID DE PERSONAL
        $sqlper_id = "SELECT per_id  as perid FROM sys_rh_personal WHERE per_ndoc='$id' ";
        $rperid = mysqli_query($con,$sqlper_id);
        $aperid = mysqli_fetch_array($rperid,MYSQLI_ASSOC);
        $numperid = $aperid['perid'];
        $sql2="INSERT INTO sys_rh_personal_otros VALUES ('0','$periodo','$otrop','$oruc','$onom','$id','$fecr','$numperid')";
        if (!mysqli_query ($con,$sql2)) { echo("Error description: " . mysqli_error($con)); }
      }
    } else {
        $sql2b="SELECT ot_periodo,ot_nombre,ot_ruc, ot_monto,extract(month from ot_fecha) as mes, extract(day from ot_fecha) diar FROM sys_rh_personal_otros WHERE ot_id='$otipo'";
        $r2busca=mysqli_query($con,$sql2b);
        $rsqlb2=mysqli_fetch_array($r2busca,MYSQLI_ASSOC);
        $periodo=$rsqlb2['ot_periodo'];
        $onom=$rsqlb2['ot_nombre'];
        $oruc=$rsqlb2['ot_ruc'];
        $otrop=$rsqlb2['ot_monto'];
        $diar=$rsqlb2['diar'];
        $mes=$rsqlb2['mes'];
    }
    if ($mes=='1') { $mes = 'ENERO'; } if ($mes=='2') { $mes = 'FEBRERO'; } if ($mes=='3') {  $mes = 'MARZO'; } if ($mes=='4') { $mes = 'ABRIL'; }
    if ($mes=='5') { $mes = 'MAYO'; }  if ($mes=='6') { $mes = 'JUNIO'; } if ($mes=='7') { $mes = 'JULIO'; } if ($mes=='8') { $mes = 'AGOSTO'; }
    if ($mes=='9') { $mes = 'SEPTIEMBRE'; }  if ($mes=='10') { $mes = 'OCTUBRE'; }  if ($mes=='11') { $mes = 'NOVIEMBRE'; } if ($mes=='12') { $mes = 'DICIEMBRE'; }




  $sqle="SELECT emp_ruc,emp_nombre FROM sys_empresas WHERE emp_id = '$empid'";
  $resule=mysqli_query($con,$sqle);
    $rsqle=mysqli_fetch_array($resule,MYSQLI_ASSOC);
    $rucc=$rsqle['emp_ruc'];
    $nomm=$rsqle['emp_nombre'];

  $sqln="SELECT per_tdoc,per_nomape,per_direc from sys_rh_personal WHERE per_ndoc = '$id'";
    $resuln=mysqli_query($con,$sqln);
    $rsqln=mysqli_fetch_array($resuln,MYSQLI_ASSOC);
    $nombres=$rsqln['per_nomape'];
    $direcci=$rsqln['per_direc'];
    $tdoc=$rsqln['per_tdoc'];
    if ($tdoc==0) {
     $todc ='OTRO';
   } elseif ($tdoc==1) {
     $todc ='DNI';
   } elseif ($tdoc==7){
     $todc ='PASS';
   }elseif ($tdoc=4){
     $todc ='C.E.';
   } else {
     $todc ='C.D.';
   }
?>
<style type="text/css">
.Estilo1 {
	font-size: 10px;
	font-weight: bold;
}
.Estilo0 {
	font-size: 12px;
}
.Estilo2 {
	font-size: 9px;
}
.tborde {
  border: 1.1px solid black;
}
</style>
<page orientation="portrait" style="font-size: 8px" backtop="20mm" backbottom="10mm" backleft="500mm" backright="500mm">

  <page_header>
  <table  border="0">
    <tr>
      <td width="630"><span class="Estilo1"><?php echo $nomm; ?></span></td>
      <td width="100"><em>P&aacute;gina: </em><strong>[[page_cu]]</strong></td>
    </tr>
    <tr>
      <td colspan="2"><span class="Estilo1">RUC: <?php echo $rucc; ?></span></td>
    </tr>

    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td align="center" colspan="2"><span class="Estilo1">DECLARACION JURADA DE INGRESOS O RENTAS ADICIONALES PARA RETENCIONES DE QUINTA CATEGORIA (D.S.N&ordm; 122-94-EF. Art. 44 inciso b) </span></td>
    </tr>
    <tr>
      <td align="center" colspan="2"><span class="Estilo1">A&Ntilde;O <?php echo $periodo; ?></span></td>
    </tr>
  </table>&nbsp;
  <table border="0">
    <tr>
      <td align="center" width="180"><span class="Estilo1"><u>RUBRO I : </u></span></td>
      <td width="550"><span class="Estilo1"><u> DATOS DE IDENTIFICACION DEL TRABAJADOR </u></span></td>
    </tr>
    <tr>
      <td align="right"><span class="Estilo1">&nbsp;APELLIDOS Y NOMBRES:</span></td>
      <td><span class="Estilo1">&nbsp;<?php  echo $nombres;?></span></td>
    </tr>
    <tr>
      <td align="right"><span class="Estilo1">&nbsp;DOMICILIO FISCAL:</span></td>
      <td><span class="Estilo1">&nbsp;<?php echo $direcci; ?></span></td>
    </tr>
  </table>
  <table  border="0">
    <tr>
      <td colspan="2"><span class="Estilo0">Por la presente cumplo con informar que mi empleador principal (1) es: </span></td>
    </tr>
    <tr>
      <td width="400"><span class="Estilo1">&nbsp;( X ) <?php echo $nomm; ?> </span></td>
      <td width="330"><span class="Estilo1">&nbsp;RUC: <?php echo $rucc; ?></span></td>
    </tr>
    <tr>
      <td><span class="Estilo1"> <?php if ($opc=='SI') {
          $muestra='( X ) '.STRTOUPPER($onom);
        }else {
        $muestra ='(&nbsp;&nbsp;&nbsp;&nbsp;) Otra Instituci&oacute;n';
        }
        echo $muestra;
        ?>
      </span></td>
      <td><span class="Estilo1"> <?php if ($opc=='SI') {
          $muestr='RUC: '.STRTOUPPER($oruc);
        }else {
        $muestr ='RUC: ';
        }
        echo $muestr;
        ?>
      </span></td>
    </tr>
    <tr>
      <td colspan="2"><p class="Estilo0">&nbsp;Y ser&aacute; la UNICA encargada de efectuar las retenciones de Quinta Categoria durante el ejercio <?php echo $periodo; ?>.</p>
      <p class="Estilo0">&nbsp;En el caso de haber designado a la EMPRESA como empleador principal,  informo que percibo otras remuneraciones que &eacute;ste las acumule y efectu&eacute; la retenci&oacute;n correspondiente, conforme a la ley.   </p><br></td>
    </tr>
    <tr>
    <td colspan="2"><span class="Estilo0">
      <?php if ($opc=='SI') {
      $decisi='SI ( X ) NO (   ) percibo otras rentas de Quinta Categor&iacute;a';
    } else {
      $decisi='SI (   ) NO ( X ) percibo otras rentas de Quinta Categor&iacute;a';
    } echo $decisi;?>
    </span></td>
  </tr>
</table>&nbsp;
  <table border="0">
    <tr>
       <td align="center"><span class="Estilo1"><u>RUBRO II : </u></span></td>
       <td>&nbsp;</td>
       <td><span class="Estilo1"><u> DECLARACION DE INGRESOS </u></span></td>
     </tr>
     <tr>
       <td width="280"><span class="Estilo1"><p>&nbsp;1. RENTAS VITALICIAS Y PENSIONES <br>&nbsp;(Indicar monto aproximado para el año <?php echo $periodo; ?>) </p></span></td>
       <td width="150">&nbsp;</td>
       <td class="tborde" width="260"><span class="Estilo1">S/.</span></td>
     </tr>
     <tr>
       <td width="300"><span class="Estilo1"><p>&nbsp;2. REMUNUERACIONES PERCIBIDAS DE OTROS EMPLEADORES (Indicar el monto para el año <?php echo $periodo; ?>)  </p></span></td>
       <td width="150">&nbsp;</td>
       <td class="tborde" width="260"><span class="Estilo1">S/.<?php echo $otrop; ?></span></td>
     </tr>
     <tr>
       <td width="300"><span class="Estilo1"><p>&nbsp;3. EXCESO DEL IMPUESTO PAGADO SEG&Uacute;N DECLARACION JURADA DEL A&Ntilde;O ANTERIOR (ACREDITAR).  </p></span></td>
       <td width="150">&nbsp;</td>
       <td class="tborde" width="260"><span class="Estilo1">S/.</span></td>
     </tr>
     <tr>
       <td width="300">&nbsp;</td>
       <td width="150">&nbsp;</td>
       <td width="260">&nbsp;</td>
     </tr>
     <tr>
       <td colspan="3"><span class="Estilo0"><p>&nbsp;EXP&Igrave;DO EL PRESENTE DOCUMENTO CON CAR&Aacute;CTER DE DECLARACI&Oacute;N JURADA, SE&Ntilde;ALANDO QUE SU CONTENIDO SE AJUSTA A LA VERDAD. </p></span></td>
     </tr>
     <tr>
       <td width="300">&nbsp;</td>
       <td width="150">&nbsp;</td>
       <td width="260">&nbsp;</td>
     </tr>
     <tr>
       <td colspan="3">&nbsp;LIMA,<?php echo ' '.$diar; ?> DE <?php echo $mes; ?> DEL <?php echo $periodo; ?></td>
     </tr>
     <tr>
       <td colspan="3">&nbsp;</td>
     </tr>
     <tr>
       <td colspan="3">&nbsp;</td>
     </tr>
     <tr>
      <td colspan="3">_____________________________________________</td>
    </tr>
    <tr>
      <td colspan="3"><span class="Estilo1">FIRMA: <?php echo $nombres; ?></span></td>
    </tr>
    <tr>
      <td colspan="3"><span class="Estilo1">Tipo de documento de Identidad y N&uacute;mero : <?php echo $todc.' - '.$id; ?> </span></td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3"><span class="Estilo2">(1) El que abona mayor remuneraci&oacute;n. </span></td>
    </tr>
  </table>
</page_header>

</page>
