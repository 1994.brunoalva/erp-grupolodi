<?php
  error_reporting(E_ERROR | E_PARSE);
  include "../../Funciones/BD.php";
  $ruc = $_GET['ruc'];
  $nombre = $_GET['nom'];
  $anual = $_GET['anual'];
  $mes = $_GET['mes']; $mes2=$mes;
  $emp = $_GET['emp'];
  $opc = $_GET['opcion'];
 if ($mes=='1') { $mes = 'ENERO'; } if ($mes=='2') { $mes = 'FEBRERO'; } if ($mes=='3') {  $mes = 'MARZO'; } if ($mes=='4') { $mes = 'ABRIL'; }
 if ($mes=='5') { $mes = 'MAYO'; }  if ($mes=='6') { $mes = 'JUNIO'; } if ($mes=='7') { $mes = 'JULIO'; } if ($mes=='8') { $mes = 'AGOSTO'; }
 if ($mes=='9') { $mes = 'SEPTIEMBRE'; }  if ($mes=='10') { $mes = 'OCTUBRE'; }  if ($mes=='11') { $mes = 'NOVIEMBRE'; } if ($mes=='12') { $mes = 'DICIEMBRE'; }
#DATOS GENERALES DEL ASIENTO CONTABLE
$sqlgral="SELECT *,DATE_FORMAT(asig_fecha,'%d/%m/%Y') as fechaasig FROM sys_conta_asientos_general WHERE emp_id='$emp' AND  asig_tipoa='ASIENTO'  ORDER BY asig_cod";
$rgral=mysqli_query($con,$sqlgral);

?>
<style type="text/css">
.Estilo0 {font-size: 12px; font-weight: bold; }
.Estilo1 {font-size: 11px; font-weight: bold; }
.Estilo01 {font-size: 10px; font-weight: bold; }
.Estilo11 {font-size: 10px; font-weight: bold;  }
.Estilo12 {font-size: 10px;  }
.Estilo13 {font-size: 9px; font-weight: bold; }
.Estilo14 {font-size: 8px; font-weight: bold; }
.tborde {
  border: 1.1px solid black;
}
</style>
<page orientation="portrait" style="font-size: 7" backtop="1mm" backbottom="1mm" backleft="1mm" backright="1mm">
<?php if ($opc =="basico") { ?>
  <page_header>
  <table  border="0">
    <tr>
      <td width="633"><span class="Estilo0"><?php echo $nombre; ?></span></td>
      <td width="100"><em>P&aacute;gina: </em><strong>[[page_cu]]</strong></td>
    </tr>
    <tr>
      <td colspan="2"><span class="Estilo0">R.U.C : <?= $ruc; ?></span></td>
    </tr>
  </table>
  <table  border="0">
      <tr>
        <td colspan="8" align="center"><span class="Estilo0"><?php echo $mes.' '.$anual; ?> <br />Expresado en Moneda Nacional</span></td>
      </tr>
      <tr>
        <td colspan="8"><hr /></td>
      </tr>
    <tr>
      <td width="76"  align="center"><span class="Estilo13">CUENTA</span></td>
      <td width="130"  align="center"><span class="Estilo13">DETALLE</span></td>
      <td width="70"  align="center"><span class="Estilo13">AUXILIAR</span></td>
      <td width="110"  align="center"><span class="Estilo13">Nro. Documento</span></td>
      <td width="52"  align="center"><span class="Estilo13">C. Cost.</span></td>
      <td width="95"  align="center"><span class="Estilo13">IMPORTE US$</span></td>
      <td width="85"  align="center"><span class="Estilo13">DEBE</span></td>
      <td width="85"  align="center"><span class="Estilo13">HABER</span></td>
    </tr>
    <tr>
      <td colspan="8"><hr /></td>
    </tr>
  </table>
  <table width="760" border="0">
<?php  while($row=mysqli_fetch_array($rgral,MYSQLI_ASSOC)){  ?>
 <tr>
   <td  width="76"><span class="Estilo13">ASIENTO: <strong><?= $row['asig_id']; ?></strong> </span></td>
   <td  width="130"><?php
        $iddet= $row['asig_cod'];
        $sqldetalle ="SELECT * from sys_conta_asientos_detalle where asid_cod='$iddet' and  asid_estatus='C'";
        $rdetalle=mysqli_query($con,$sqldetalle);
   ?></td>
   <td width="67"><span class="Estilo13">FECHA REG: </span></td>
   <td width="110"><span class="Estilo13"><?=$row['fechaasig'];?></span></td>
   <td width="55"><span class="Estilo14">FECHA MOV:</span></td>
   <td width="95"><span class="Estilo13"><?=$row['fechaasig'];?></span></td>
   <td width="85">&nbsp;</td>
   <td width="85">&nbsp;</td>
 </tr>
    <?php  while($row2=mysqli_fetch_array($rdetalle,MYSQLI_ASSOC)){  ?>
 <tr>
  <td width="76" align="center"><span class="Estilo13"><?= $row2['asid_cuentad']; ?></span></td>
  <td width="110" align="center"><span class="Estilo13"><?php
    $sqlndoc = "SELECT CONCAT ('POR ',asig_ndoc)dettasig, asig_ndoc numfac  FROM sys_conta_asientos_general WHERE asig_cod='$iddet'";
    $rndoc = mysqli_query($con,$sqlndoc);
    $andoc = mysqli_fetch_array($rndoc,MYSQLI_ASSOC);
    $dndoc = $andoc['dettasig'];
    echo $dndoc;
  ?></span></td>
  <td width="67" align="center"><span class="Estilo13"><?php $cliente = $row['cli_ruc'];
  if ($cliente !='' AND $cliente>'0') {
    echo $cliente;
  }
  ?></span></td>
  <td width="100" align="center"><span class="Estilo13"><?= $row['asig_ndoc']; ?></span></td>
  <td width="55"  align="center">&nbsp;</td>
  <td width="95" align="right"><span class="Estilo13">
    <?php
        $tasac = $row['asig_tasa'];
        $debeusd=  $row2['asid_debe'];
        $haberusd=  $row2['asid_haber'];
        if ($debeusd>'0') {
          $debeusd = ($debeusd / $tasac);
          $importe  = $debeusd;
        }
        if ($haberusd>'0') {
          $haberusd = ($haberusd / $tasac);
          $importe  = $haberusd;
        }
      #IMPORTE USD - DEBE / HABER
      if ($importe >'0') {
        echo number_format($importe, 2, '.', '');
      }
    ?>
  </span></td>
  <td width="85" align="right"><span class="Estilo13"><?= $row2['asid_debe']; ?></span></td>
  <td width="85" align="right"><span class="Estilo13"><?=$row2['asid_haber']; ?></span></td>
</tr>
<?php } ?>
<tr>
<td colspan="8"><hr /></td>
</tr>
<tr align="right">
  <td> &nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><span class="Estilo13">Moneda: US$</span></td>
  <td></td>
  <td><span class="Estilo13">T. Cambio:<?=$row['asig_tasa']; ?></span></td>
  <td><span class="Estilo13"><?php
        $iddet= $row['asig_cod'];
        $sqltdebe = "SELECT sum(asid_debe) as tdbe FROM sys_conta_asientos_detalle WHERE asid_cod= '$iddet' AND asid_estatus ='C' ";
        $rdebe=mysqli_query($con,$sqltdebe);
        $debsql=mysqli_fetch_array($rdebe,MYSQLI_ASSOC);
        $tdbe=$debsql['tdbe'];
        if ($tdbe>'0') {
          echo '<strong>'.$tdbe.'</strong>';
        }

   ?></span></td>
  <td><span class="Estilo13"><?php
        $iddet= $row['asig_cod'];
        $sqlthaber = "SELECT sum(asid_haber) as tdha FROM sys_conta_asientos_detalle WHERE asid_cod= '$iddet' AND asid_estatus ='C' ";
        $rhaber=mysqli_query($con,$sqlthaber);
        $habesql=mysqli_fetch_array($rhaber,MYSQLI_ASSOC);
        $tdha=$habesql['tdha'];
        if ($tdha>'0') {
          echo '<strong>'.$tdha.'</strong>';
        }

   ?></span></td>
</tr>
<tr>
  <td colspan="8"><hr /></td>
</tr>
 <?php } ?>
 </table></page_header>
<?php }  if ($opc == "sunat") {?>

  <page_header>
    <table width="834" border="0">
      <tr>
        <td width="190"><span class="Estilo0"><?=$nombre; ?></span></td>
        <td width="470">&nbsp;</td>
        <td width="60"><em>P&aacute;gina: </em><strong>[[page_cu]]</strong></td>
      </tr>
      <tr>
        <td><span class="Estilo0">R.U.C. : <?= $ruc; ?></span></td>
        <td align="center"><span class="Estilo0">LIBRO DIARIO </span></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><span class="Estilo0">FORMATO 5.1 </span></td>
        <td align="center"><span class="Estilo0"><?= $mes.' '.$anual; ?></span></td>
        <td>&nbsp;</td>
      </tr>
    </table>&nbsp;
    <table width="832" border="0">
    <tr align="center">
      <td width="40" rowspan="2" class="tborde"><span class="Estilo11">N&ordm; Correl. Operaci </span></td>
      <td width="57" rowspan="2" class="tborde"><span class="Estilo11">Fecha Operaci. </span></td>
      <td width="90" rowspan="2" class="tborde"><span class="Estilo11">Glosa o Descripci&oacute;n de la Operaci&oacute;n </span></td>
      <td colspan="3" class="tborde"><span class="Estilo11">Referencia de la Operaci&oacute;n </span></td>
      <td colspan="2" class="tborde"><span class="Estilo11">Cuenta Contable Asociada a la Operaci&oacute;n </span></td>
      <td colspan="2" class="tborde"><span class="Estilo11">MOVIMIENTOS</span></td>
    </tr>
    <tr align="center">
      <td width="35" class="tborde"><span class="Estilo11">Cod. Libro o Regi. </span></td>
      <td width="47" class="tborde"><span class="Estilo11">N&ordm; Correl. </span></td>
      <td width="74" class="tborde"><span class="Estilo11">N&ordm; Documento Sustentario </span></td>
      <td width="67" class="tborde"><span class="Estilo11">CUENTA</span></td>
      <td width="100" class="tborde"><span class="Estilo11">DENOMINANCION</span></td>
      <td width="60" class="tborde"><span class="Estilo11">DEBE</span></td>
      <td width="60" class="tborde"><span class="Estilo11">HABER</span></td>
    </tr>
    <?php
    #DATOS GENERALES DEL ASIENTO CONTABLE
    $sqlgral="SELECT *,DATE_FORMAT(asig_fecha,'%d/%m/%Y') as fechaasig FROM sys_conta_asientos_general WHERE emp_id='$emp' AND  asig_tipoa='ASIENTO'  ORDER BY asig_cod";
    $rgral=mysqli_query($con,$sqlgral);
    while($row=mysqli_fetch_array($rgral,MYSQLI_ASSOC)){
       $idcod = $row['asig_id'];
      $sqldetalle ="SELECT * from sys_conta_asientos_detalle where asid_cod='$idcod' and  asid_estatus='C'";
      $rdetalle=mysqli_query($con,$sqldetalle);
      while($row2=mysqli_fetch_array($rdetalle,MYSQLI_ASSOC)){
    ?>
    <tr align="center">
      <td ><span class="Estilo12"><?=$anual.$mes2.$row['asig_id'];?></span></td>
      <td ><span class="Estilo12"><?=$row['fechaasig'];?></span></td>
      <td ><span class="Estilo12"><?='POR'.' '.$row['asig_ndoc'];?></span></td>
      <td ><span class="Estilo12"><?='0'.''.$row['asig_libro'];?></span></td>
      <td ><span class="Estilo12"><?=$row['asig_id'];?></span></td>
      <td ><span class="Estilo12"><?php
      $iddoc = $row['asig_tdoc'];
      $sqldoc ="SELECT doc_sunat_id as ttdoc FROM sys_adm_documentos WHERE doc_id='$iddoc'";
      $rdoc = mysqli_query($con,$sqldoc);
      $adoc = mysqli_fetch_array($rdoc);
      $ttdoc = $adoc['ttdoc'];
      echo $ttdoc.' '.$row['asig_ndoc'];
      ?></span></td>
      <td ><span class="Estilo12"><?=$row2['asid_cuentad'];?></span></td>
      <td  align="left"><span class="Estilo12"><?php
      $idcuentad = $row2['asid_cuentad'];
      $sqlnom = "SELECT plade_nombre FROM sys_conta_plan_detalle WHERE plade_codrela='$idcuentad'";
      $rnom = mysqli_query($con,$sqlnom);
      $anom = mysqli_fetch_array($rnom,MYSQLI_ASSOC);
      $nnom = $anom['plade_nombre'];
      echo $nnom;
      ?></span></td>
      <td ><span class="Estilo12"><?php
      $debedet = $row2['asid_debe'];
      $tdebe = $tdebe+$debedet ;
      if ($debedet>'0') {
       echo number_format($row2['asid_debe'], 2,",",".");
      }
      ?></span></td>
      <td ><span class="Estilo12"><?php
      $haberdet = $row2['asid_haber'];
      $thaber = $thaber + $haberdet;
      if ($haberdet>'0') {
      echo number_format($row2['asid_haber'], 2,",",".");
      }
      ?></span></td>
    </tr>
  <?php }} ?>
    <tr>
      <td colspan="8"  align="right"><span class="Estilo11">TOTAL:</span></td>
      <td class="tborde" align="center"><span class="Estilo11"><?=number_format($tdebe, 2,",",".");?></span></td><td class="tborde"><span class="Estilo11"><?=number_format($thaber, 2,",",".");?></span></td>
    </tr>
  </table>
  </page_header>

<?php } ?>
</page>
