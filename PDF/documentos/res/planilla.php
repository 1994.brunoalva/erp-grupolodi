<?php
  error_reporting(E_ERROR | E_PARSE);
  $opc = $_REQUEST['opc'];
  $id = $_REQUEST['id'];
  include "../../../funciones/BD.php";


  if (isset($_REQUEST['opc']) AND $_REQUEST['opc']=='bancos') {
    include "Bancos_mensual.php";
  }
  if ($_REQUEST['opc'] =='boletas' or $_REQUEST['opc'] =='resumen' or $_REQUEST['opc'] =='bancos') {
    #TITULO
    $sqltitulo = "SELECT date_format(ht.hist_desde,'%Y%m%d') as fec,se.emp_id,se.emp_logo,se.emp_nombre,se.emp_ruc,Nmes(EXTRACT(MONTH FROM ht.hist_desde)) AS hmes,
      EXTRACT(YEAR FROM ht.hist_desde) AS anual,EXTRACT(YEAR FROM ht.hist_desde) AS anual,EXTRACT(DAY FROM ht.hist_hasta) AS dias,hist_periodo,
      EXTRACT(MONTH FROM ht.hist_hasta) AS mhasta,ht.hist_hasta FROM sys_rh_historico ht,sys_empresas se
      WHERE hist_id =$id and se.emp_id = ht.emp_id";
      $rtitulo=mysqli_query($con,$sqltitulo);
      $rsqltt=mysqli_fetch_array($rtitulo,MYSQLI_ASSOC);
      $mes=$rsqltt['hmes'];
      $anual=$rsqltt['anual'];
      $dias=$rsqltt['dias'];
      $xperiodo=$rsqltt['hist_periodo'];
      $mes_hasta=$rsqltt['mhasta'];
      $xhasta=$rsqltt['hist_hasta'];
    #Datos de la empresa
      $nomepm=$rsqltt['emp_nombre'];
      $rucepm=$rsqltt['emp_ruc'];
      $hempid=$rsqltt['emp_id'];
      $heloho=$rsqltt['emp_logo'];
      $rutaimg = '../../../Logos/'.$heloho;
      if ($xperiodo=='MENSUAL') {
        $title = 'PLANILLA DE REMUNERACIONES';
      }
      if ($xperiodo=='CTS') {
        $title = 'PLANILLA DE REMUNERACIONES Y CTS';
        $subtitle= 'CUADRO CTS';
        $ordena = ' ORDER BY histdet_area';
        if ($mes_hasta=='5') {
          $anualb = $anual -1;
          #PERIODO CTS
          $subtitle2='PERIODO DE NOVIEMBRE - '.$anualb.' ABRIL / '.$anual;
          $campo = 'NOVIEMBRE - ABRIL '.$anual;
        }else {
          $subtitle2='PERIODO DE MAYO - '.$anual.' OCTUBRE / '.$anual;
          $campo = 'MAYO - OCTUBRE '.$anual;
        }

      }
      if ($xperiodo=='GRATIFICACION') {
        $title = 'PLANILLA DE REMUNERACIONES Y CTS';

      }
  }
  if ($_REQUEST['opc'] =='resumen') {
    $sqlprin="SELECT histdet_id,histdet_ndoc,histdet_nomape,truncate(histdet_sueldo,2) histdet_sueldo from sys_rh_historico_detalle
      WHERE hist_id='$id'".$ordena;
      $rprin=mysqli_query($con,$sqlprin);
    #SECUNDARIO
    $sqlsecun="SELECT histdet_id,histdet_ndoc,histdet_nomape,histdet_cargo,histdet_cuspp,histdet_fondo,histdet_area,truncate(histdet_sueldo,2) histdet_sueldo,
      date_format(histdet_fecing,'%d-%m-%Y') histdet_fecing from sys_rh_historico_detalle WHERE hist_id='$id'".$ordena;
      $rsecun=mysqli_query($con,$sqlsecun);

    $sqlcant="SELECT COUNT(histdet_ndoc) as ncant FROM sys_rh_historico_detalle WHERE hist_id='$id'";
      $rcant=mysqli_query($con,$sqlcant);
      $rcantt=mysqli_fetch_array($rcant,MYSQLI_ASSOC);
      $cantid=$rcantt['ncant'];

  }
#REPORTE DE BANCOS - ENVIADO AL BCP
  if ($opc == "bancos") {
      $cbanco = $_GET['xcuent'];
      $ccliente =$_GET['xcodban'];
      $rbanco =$_GET['banid'];

      if (isset($_GET['fecpago']) AND $_GET['fecpago'] !='') {
        $fec = $_GET['fecpago'];
        $fecrepor = date ("Ymd",strtotime($fec));
      }
      $sqlempresa = "SELECT emp_codban from sys_empresas_bancos WHERE emp_id='$hempid' AND emp_codban = '$ccliente' ";
      $rempresa=mysqli_query($con,$sqlempresa);
      $row_emp = mysqli_num_rows($rempresa);
    if ($row_emp==0) {
      $sql2="insert into sys_empresas_bancos values ('0','$ccliente',UPPER('S'),'$cbanco','$rbanco','$hempid')";
      if (!mysqli_query ($con,$sql2)) { echo("Error description: " . mysqli_error($con)); }

      #$upemp="UPDATE  sys_empresas set emp_codban = '$ccliente', emp_cuenta='$cbanco', ban_id='$rbanco' WHERE emp_id = '$hempid'";
      #$rupdate=mysqli_query($con,$upemp);
    }
    #bancos
    $sqlbancos="select histdet_id,histdet_ndoc,histdet_ncuenta,histdet_tdoc,histdet_nomape,truncate(histdet_sueldo,2) histdet_sueldo from sys_rh_historico_detalle WHERE hist_id='$id'";
    $rbanc=mysqli_query($con,$sqlbancos);
    #TOTAL REMUNERACIONES - BANCO
      $sqlttremu="SELECT truncate(sum(histmov_monto),2) ttre FROM sys_rh_historico_moviento WHERE hist_id='$id' and histmov_tipo=1";
       $rttmenu  = mysqli_query($con,$sqlttremu);
       $ttremu=mysqli_fetch_array($rttmenu,MYSQLI_ASSOC);
       $ttrem=$ttremu['ttre'];

    #TOTAL REMUNERACIONES - BANCO
     $sqlttdesu="SELECT truncate(sum(histmov_monto),2) ttde FROM sys_rh_historico_moviento WHERE hist_id='$id' and histmov_tipo=0";
      $rttdesu = mysqli_query($con,$sqlttdesu);
      $ttdes=mysqli_fetch_array($rttdesu,MYSQLI_ASSOC);
      $ttdesc=$ttdes['ttde'];
  }



?>
<style type="text/css">
.Estilo0 {font-size: 10px;}
.Estilo01 {font-size: 9.2px;}
.Estilo00 {font-size: 11px;}
.Estilo03 {font-size: 9px; }
.Estilo1 {font-size: 9px; font-weight: bold; }
.Estilo11 {font-size: 7px; font-weight: bold; }
.Estilo13 {font-size: 8px; font-weight: bold; }
.Estilo3 {font-size: 7px; font-weight: bold; }
.Estilo33 {font-size: 6.5px; font-weight: bold; }
.Estilo34 {font-size: 6.5px;  }
.ttotal {
border: 1.1px solid black;
background-color: #eaa392;
}
.trem {
border: 1.5px solid black;
background-color:#85bdde;
}
.Estilo2 {font-size: 11px; font-weight: bold; }
.subban {font-size: 9.5px; font-weight: bold; }
.subbande {font-size: 9px;  }
.tban {
  border: 1.1px solid black;
  background-color:#e9f764;
}
.tborde {
  border: 1.1px solid black;
}
.tbordee {
  border: 1.1px solid white;
}
.Estilo4 {font-size: 9px; font-weight: bold; color:red; }
.vl {
  border-left: 1.5px dotted black;
  height: 650px;
  text-align: center;
}

</style>

<?php if ($opc=='boletas'){
  #Config Reporte
  $sqldreport="SELECT MIN(histdet_id) as minimo, max(histdet_id) as maximo, count(histdet_ndoc) ntraba
FROM sys_rh_historico_detalle WHERE hist_id='$id'";
  $rdreport=mysqli_query($con,$sqldreport);
  $rreport=mysqli_fetch_array($rdreport,MYSQLI_ASSOC);
  $pos_mini=$rreport['minimo'];
  $pos_maxi=$rreport['maximo'];
  $ntrabajador=$rreport['ntraba'];

  $i = '1'; $posicion=$pos_mini;
  while ($i <= $ntrabajador) {

    $sqldni ="SELECT histdet_ndoc,histdet_nomape,truncate(histdet_sueldo,2) histdet_sueldo,histdet_tipfondo,date_format(histdet_fecnac,'%d/%m/%Y') histdet_fecnac,
    histdet_fondo,date_format(histdet_fecing,'%d/%m/%Y') histdet_fecing,histdet_cuspp,histdet_cargo,histdet_area,Nhoras(histdet_ndoc,'$id') as Nhora
      FROM sys_rh_historico_detalle WHERE histdet_id ='$posicion' AND hist_id='$id' ";
      $rdni=mysqli_query($con,$sqldni);
      $rrdni=mysqli_fetch_array($rdni,MYSQLI_ASSOC);
      $xdni=$rrdni['histdet_ndoc'];
      $xnomape=$rrdni['histdet_nomape'];
      $xsueldo=$rrdni['histdet_sueldo'];
      $xfecnac=$rrdni['histdet_fecnac'];
      $xfondo=$rrdni['histdet_fondo'];
      $xfecing=$rrdni['histdet_fecing'];
      $xcuspp=$rrdni['histdet_cuspp'];
      $xcargo=$rrdni['histdet_cargo'];
      $xarea=$rrdni['histdet_area'];
      $tipfon=$rrdni['histdet_tipfondo'];
      $Nhora=$rrdni['Nhora'];

    #REMUNERACIONES
    $sqlremu="SELECT histmov_nombre,truncate(histmov_monto,2) histmov_monto FROM sys_rh_historico_moviento WHERE hist_id ='$id'
    and histmov_ndoc='$xdni' and histmov_tipo =1";
      $rremunera  = mysqli_query($con,$sqlremu);

    #Espacios en Blanco
    $sqlfilasr="SELECT COUNT(histmov_nombre) as nfr FROM sys_rh_historico_moviento WHERE hist_id ='$id'
    and histmov_ndoc='$xdni' and histmov_tipo =1";
      $rsqlfilasr  = mysqli_query($con,$sqlfilasr);
      $rsqfilas=mysqli_fetch_array($rsqlfilasr,MYSQLI_ASSOC);
      $n_filas = $rsqfilas['nfr'];
      $nt = 8;
      $nt = $nt - $n_filas;

    #TOTAL REMUNERACIONES
    $sqlttremu="SELECT truncate(sum(histmov_monto),2) ttre FROM sys_rh_historico_moviento WHERE hist_id='$id' and histmov_ndoc='$xdni' and histmov_tipo=1";
     $rttmenu  = mysqli_query($con,$sqlttremu);
     $ttremu=mysqli_fetch_array($rttmenu,MYSQLI_ASSOC);
     $ttrem=$ttremu['ttre'];
    if ($xperiodo=='MENSUAL' or $xperiodo=='CTS') {
      #DESCUENTOS
      $sqlrdescu="SELECT histmov_nombre, truncate(histmov_monto,2) histmov_monto FROM sys_rh_historico_moviento WHERE hist_id ='$id'
      and histmov_ndoc='$xdni' and histmov_tipo =0";
        $rdescu  = mysqli_query($con,$sqlrdescu);
      #TOTAL DESCUENTOS
       $sqlttdesu="SELECT truncate(sum(histmov_monto),2) ttde FROM sys_rh_historico_moviento WHERE hist_id='$id' and histmov_ndoc='$xdni' and histmov_tipo=0";
        $rttdesu = mysqli_query($con,$sqlttdesu);
        $ttdes=mysqli_fetch_array($rttdesu,MYSQLI_ASSOC);
        $ttdesc=$ttdes['ttde'];

      #Espacios en Blanco
      $sqlfil="SELECT COUNT(histmov_nombre) as nfd FROM sys_rh_historico_moviento WHERE hist_id ='$id' and histmov_ndoc='$xdni' and histmov_tipo =0";
        $rsqlfil  = mysqli_query($con,$sqlfil);
        $rsqfila=mysqli_fetch_array($rsqlfil,MYSQLI_ASSOC);
        $n_fila = $rsqfila['nfd'];
        $ntt = 8;
        $ntt = $ntt - $n_fila;
      #APORTES
      $sqlaport="SELECT histmov_nombre,truncate(histmov_monto,2) histmov_monto,truncate(histmov_monto,2) montoe  FROM sys_rh_historico_moviento WHERE hist_id ='$id'
      and histmov_ndoc='$xdni' and histmov_tipo =2";
        $raporte  = mysqli_query($con,$sqlaport);
        $aprow=mysqli_fetch_array($raporte,MYSQLI_ASSOC);
        $appor=$aprow['histmov_nombre'];
        $montoa=$aprow['montoe'];
    }


#BOLETA MENSUAL DE PAGOS
if (isset($_REQUEST['opc']) AND $_REQUEST['opc']=='boletas') {
  include "Boleta_mensual.php";
}
#BOLETA DE CTS
#BOLETA DE GRATIFICACIONES
 #Contador de empleados - Reporte de Boletas
 $i++;  $posicion++;}
} ?>
<?php if ($opc=='resumen'){ ?>
<page orientation="landscape" style="font-size: 9px" backtop="1mm" backbottom="5mm" backleft="0mm" backright="1mm">
  <page_header>
  <table width="898" border="0">
    <tr>
      <td colspan="2"><strong><?php echo $nomepm; ?></strong></td>
      <td width="95"><em>P&aacute;gina: </em><strong>[[page_cu]]</strong></td>
    </tr>
    <tr>
      <td width="345"><strong><?php echo 'RUC:'.' '.$rucepm; ?></strong></td>
      <td width="600">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3"><div align="center"><strong><?=$title;?></strong></div></td>
    </tr>
    <tr>
      <td colspan="3"><div align="center"><strong><?php echo $mes.' '.$anual; ?></strong> </div></td>
    </tr>
  </table>
  <table  border="0">
        <tr align="center">
          <td  height="20">&nbsp;</td>
          <td>&nbsp;</td>
          <td height="25">&nbsp;</td>
          <?php if ($xperiodo=='GRATIFICACION'): ?>
            <td class="trem" colspan="7"><span class="Estilo1">REMUNERACION</span></td>
          <?php endif; ?>
          <?php if ($xperiodo=='MENSUAL' or $xperiodo=='CTS'): ?>
            <td class="trem" colspan="5"><span class="Estilo1">REMUNERACION</span></td>
            <td class="trem" colspan="9"><span class="Estilo1">DESCUENTOS</span></td>
          <?php endif; ?>
          <td>&nbsp;</td>
        </tr>
        <tr align="center">
            <!---->
            <td class="tban"  height="20" width="20"><strong>#</strong></td>
            <td class="tban" width="50"><span class="Estilo1">DNI</span></td>
            <td class="tban" width="160"><span class="Estilo1">NOMBRE</span></td>
            <td class="tban" width="55"><span class="Estilo33">REMUNERACION BASICA </span></td>
            <?php if ($rucepm=='20106482897') {
              echo '<td class="tban" width="45"><span class="Estilo33">INCREMENTO POR INCORP. AFP </span></td>';
            } else {
              echo '<td class="tban" width="45"><span class="Estilo33">COMISIONES </span></td>';
            } ?>
            <td class="tban" width="40"><span class="Estilo33">ASIGNACION FAMILIAR </span></td>
            <?php if ($xperiodo=='GRATIFICACION'): ?>
              <td class="tban" width="50"><span class="Estilo33">GRATIFICACION</span></td>
              <td class="tban" width="55"><span class="Estilo33">BONIFICACION EXT LEY 29714</span></td>
              <td class="tban" width="55"><span class="Estilo33">TOTAL REMUNERACION </span></td>
              <td class="tban" width="55"><span class="Estilo33">REAL NETO A PAGAR </span></td>
            <?php endif; ?>
            <?php if ($xperiodo=='MENSUAL' OR $xperiodo=='CTS'): ?>
            <td class="tban" width="40"><span class="Estilo33">VACACIONES</span></td>
            <td class="tban" width="55"><span class="Estilo33">TOTAL REMUNERACION </span></td>
            <td class="tban" width="45"><span class="Estilo3">FONDO PENSIONES AFP </span></td>
            <td class="tban" width="45"><span class="Estilo3">COMISION PORCENTUAL
              -AFP </span></td>
            <td class="tban" width="35"><span class="Estilo3">PRIMA SEGURO
              -AFP </span></td>
            <td class="tban" width="40"><span class="Estilo33">5TA CATEGORIA </span></td>
            <?php if ($rucepm<>'20106482897') {
            echo '<td class="tban" width="45"><span class="Estilo33">ADELANTOS</span></td>
            <td class="tban" width="40"><span class="Estilo33">PRESTAMOS</span></td>
            <td class="tban" width="40"><span class="Estilo33">FALTAS / SUSPENCION L.</span></td>';
            } ?>
            <td class="tban" width="50"><span class="Estilo33">TOTAL DESCUENTOS </span></td>
            <td class="tban" width="45"><span class="Estilo33">ESSALUD</span></td>
            <td class="tban" width="55"><span class="Estilo33">NETO A PAGAR </span></td>
            <?php endif; ?>
          </tr>

<?php
$cuenta = 1;
while($row=mysqli_fetch_array($rprin,MYSQLI_ASSOC)){ ?>
          <tr>
            <td align="center" class="tborde"><span class="Estilo33"><?= $cuenta++; ?></span></td>
            <!-- DNI-->
            <td align="center" class="tborde"><span class="Estilo33"><?= $row['histdet_ndoc']; ?></span></td>
            <!-- NOMBRES Y APELLIDOS -->
            <td class="tborde"><span class="Estilo33"><?= $row['histdet_nomape']; ?></span></td>
            <!-- SUELDO-->
            <td align="center" class="tborde"><span class="Estilo33"><?php
            $doc = $row['histdet_ndoc'];
            $sqlvaca = "SELECT Rresumen('$doc','$id','VACACIONES',0) as vacaciones";
              $rvaca= mysqli_query($con,$sqlvaca);
              $rvac=mysqli_fetch_array($rvaca,MYSQLI_ASSOC);
              $vacaciones = $rvac['vacaciones'];
              if ($vacaciones >0) { $sueldo = '0.00'; } else { $sueldo=$row['histdet_sueldo'];}

            $acum_suel = $acum_suel + $sueldo;  echo $sueldo; ?></span></td>
            <!-- COMISIONES - INCRE AFP -->
            <td align="center" class="tborde"><span class="Estilo33">
              <?php
              $sqlcomi="SELECT Rresumen('$doc','$id','COMI',0) AS comi,Rresumen('$doc','$id','ASIGF',0) AS asigf";
              $rcomi=mysqli_query($con,$sqlcomi);
              $rsqlcomi=mysqli_fetch_array($rcomi,MYSQLI_ASSOC);
              $comi=$rsqlcomi['comi'];  $acum_comi = $acum_comi +$comi; echo number_format($comi, 2, '.', '') ?>
            </span></td>
            <!-- ASIGNACION FAMILIAR  -->
            <td align="center" class="tborde"><span class="Estilo33">
              <?php
              $asigf=$rsqlcomi['asigf'];  $acum_asigf = $acum_asigf + $asigf; echo $asigf; ?></span></td>
            <!--GRATIFICACION -->
            <?php if ($xperiodo=='GRATIFICACION'): ?>
              <td align="center" class="tborde"><span class="Estilo33"><?php
                $sqlgrati = "SELECT Rresumen('$doc','$id','GRATI',1) as grati,Rresumen('$doc','$id','GRATI',2) as ley";
                  $rgrati = mysqli_query($con,$sqlgrati);
                  $rgra=mysqli_fetch_array($rgrati,MYSQLI_ASSOC);
                  $grati = $rgra['grati']; $acum_grati = $acum_grati + $grati; echo $grati; ?></span></td>
              <!-- LEY 9% -->
              <td align="center" class="tborde"><span class="Estilo33"><?php
                  $ley=$rgra['ley']; $acum_ley = $acum_ley + $ley; echo $ley; ?></span></td>
            <?php endif; ?>
            <?php if ($xperiodo=='MENSUAL' OR $xperiodo=='CTS'): ?>
            <!-- VACACIONES-->
              <td align="center" class="tborde"><span class="Estilo33"><?php
                $vacaciones = $rvac['vacaciones']; $acum_vaca = $acum_vaca + $vacaciones; echo $vacaciones; ?></span></td>
            <!-- TOTAL REMUNERACION -->
              <td align="center" class="tborde"><span class="Estilo33"><?php
              #TOTAL PLANILLA MENSUAL
               $rtotal =  $sueldo + $comi + $asigf + $vacaciones;
               $acum_rtotal = $acum_rtotal + $rtotal; echo number_format($rtotal, 2, '.', '');?>
              </span></td>
            <?php endif; ?>
            <?php if ($xperiodo=='GRATIFICACION'): ?>
              <!-- TOTAL REMUNERACION -->
              <td align="center" class="tborde"><span class="Estilo33"><?php $rtotal =  $sueldo + $comi + $asigf + $ley + $grati;
              $acum_rtotal = $acum_rneto +$rtotal; echo number_format($rtotal, 2, '.', ''); ?>
              </span></td>
              <!-- REAL NETO-->
              <td align="center" class="tborde"><span class="Estilo33"><?php $rneto = $ley + $grati;
              $acum_rneto = $acum_rneto +$rneto; echo number_format($rneto, 2, '.', ''); ?>
              </span></td>
            <?php endif; ?>
            <!-- AFP PENSIONES -->
            <?php if ($xperiodo=='MENSUAL' OR $xperiodo=='CTS'): ?>
            <td align="center" class="tborde"><span class="Estilo33"><?php
            $sqlfonp="SELECT Rresumen('$doc','$id','AFP',1) as fonp, Rresumen('$doc','$id','AFP',2) as porcen,Rresumen('$doc','$id','AFP',3) as segu";
              $rfonp=mysqli_query($con,$sqlfonp);
              $rfon=mysqli_fetch_array($rfonp,MYSQLI_ASSOC);
              $fonp=$rfon['fonp'];
              $acum_fonp = $acum_fonp + $fonp; echo $fonp; ?>
            </span>
            </td>
            <!-- AFP PORCENTUAL -->
            <td align="center" class="tborde"><span class="Estilo33"><?php
              $porcen=$rfon['porcen'];
              $acum_porcen = $acum_porcen + $porcen; echo $porcen; ?></span></td>
            <!-- AFP SEGURO -->
            <td align="center" class="tborde"><span class="Estilo33"><?php
              $seguro=$rfon['segu'];
              $acum_seg = $acum_seg + $seguro; echo $seguro; ?></span>
            </td>
            <!---QUINTA CATEGORIA -->
            <td align="center" class="tborde"><span class="Estilo33"><?php
              $sqlquin="SELECT Rresumen('$doc','$id','QUIN',1) as quinta";
              $rquin=mysqli_query($con,$sqlquin); $rqui=mysqli_fetch_array($rquin,MYSQLI_ASSOC);
              $quinta=$rqui['quinta'];  $acum_qui = $acum_qui + $quinta;
              echo $quinta; ?></span></td>
            <!--LODI IMPORT CENTER NO TIENE DESCUENTOS -->
            <?php if ($rucepm!='20106482897'){ ?>
              <!--ADELANTOS -->
           <td align="center" class="tborde"><span class="Estilo33"><?php
               $sqladela="SELECT Rresumen('$doc','$id','DESCU',1) as adelan,Rresumen('$doc','$id','DESCU',2) as pres,Rresumen('$doc','$id','DESCU',3) as falta";
               $radela=mysqli_query($con,$sqladela);
               $rade=mysqli_fetch_array($radela,MYSQLI_ASSOC);
               $adelanto=$rade['adelan'];
               $falta=$rade['falta'];
               $prestado=$rade['pres'];
               $acum_adelanto = $acum_adelanto + $adelanto;
               echo $adelanto;
               ?></span>
           </td>
           <!--PRESTAMOS -->
           <td align="center" class="tborde"><span class="Estilo33"><?php
               $acum_prestado = $acum_prestado + $prestado;
               echo $prestado;
               ?></span></td>
            <!--FALTAS - SUSPENCION LABORAL -->
           <td align="center" class="tborde"><span class="Estilo33"><?php
               $acum_falta = $acum_falta + $falta;
               echo $falta; ?></span></td>
             <?php } ?>
             <?php endif; ?>
             <!--TOTAL DESCUENTOS -->
             <?php if ($xperiodo=='MENSUAL' OR $xperiodo=='CTS'): ?>
             <td align="center" class="tborde"><span class="Estilo33"><?php
             $rtotalde =  $fonp + $porcen + $seguro + $adelanto + $prestado + $falta +$quinta;
             $acum_rtotalde =  $acum_rtotalde + $rtotalde;
             echo number_format($rtotalde, 2, '.', '');?></span></td>
             <!--ESALUD -->
           <td align="center" class="tborde"><span class="Estilo33"><?php
                 $sqlsalud="SELECT Rresumen('$doc','$id','APORT',0) as salud";
                 $rsalud=mysqli_query($con,$sqlsalud);
                 $rsal=mysqli_fetch_array($rsalud,MYSQLI_ASSOC);
                 $salud=$rsal['salud']; $acum_salud = $acum_salud + $salud;
                 echo $salud; ?></span></td>
          <!--TOTAL A PAGAR -->
           <td align="center" class="tborde"><span class="Estilo33"><?php
             $ttotal = $rtotal - $rtotalde; echo number_format($ttotal, 2, '.', ''); ?>
           </span></td><?php endif; ?>
          </tr>
  <?php } ?>
        </table>
      <table width="1095" border="0">
      <tr>
        <td width="255"><div align="right"><span class="Estilo1">TOTAL</span></div></td>
        <td align="center" class="ttotal" width="55"><span class="Estilo33"><?php echo number_format($acum_suel, 2, '.', ''); ?></span></td>
        <!--COMISIONES - INC. AFP.-->
        <td align="center" class="ttotal" width="45"><span class="Estilo33"><?php echo number_format($acum_comi, 2, '.', ''); ?></span></td>
        <!--ASIGNACION FAMILIAR-->
        <td  align="center" class="ttotal" width="40"><span class="Estilo33"><?php echo number_format($acum_asigf, 2, '.', ''); ?></span></td>
        <!--VACACIONES-->
        <?php if ($xperiodo=='MENSUAL' OR $xperiodo=='CTS'): ?>
          <td align="center" class="ttotal" width="40"><span class="Estilo33"><?php echo number_format($acum_vaca, 2, '.', ''); ?></span></td>
        <?php endif; ?>
        <?php if ($xperiodo=='GRATIFICACION'): ?>
          <td align="center" class="ttotal" width="50"><span class="Estilo33"><?= number_format($acum_grati, 2, '.', ''); ?></span></td>
          <td align="center" class="ttotal" width="55"><span class="Estilo33"><?= number_format($acum_ley, 2, '.', ''); ?></span></td>
          <!--TOTAL DE REMUNERACION-->
          <td align="center" class="ttotal" width="55"><span class="Estilo33"><?php echo number_format($acum_rtotal, 2, '.', ''); ?></span></td>
          <!--TOTAL NETO A PAGAR-->
          <td align="center" class="ttotal" width="55"><span class="Estilo33"><?php echo number_format($acum_rneto, 2, '.', ''); ?></span></td>
        <?php endif; ?>
        <?php if ($xperiodo=='MENSUAL' OR $xperiodo=='CTS'): ?>
        <!--TOTAL DE REMUNERACION-->
        <td align="center" class="ttotal" width="55"><span class="Estilo33"><?php echo number_format($acum_rtotal, 2, '.', ''); ?></span></td>
        <!--TOTAL DE AFP PENSIONES-->
        <td align="center" class="ttotal" width="45"><span class="Estilo33"><?php echo number_format($acum_fonp, 2, '.', ''); ?></span></td>
        <!--TOTAL DE AFP PORCENTUAL-->
        <td align="center" class="ttotal" width="45"><span class="Estilo33"><?php echo number_format($acum_porcen, 2, '.', ''); ?></span></td>
        <!--TOTAL DE AFP SEGURO-->
        <td align="center" class="ttotal" width="35"><span class="Estilo33"><?php echo number_format($acum_seg, 2, '.', ''); ?></span></td>
        <!--TOTAL DE QUINTA CATEGORIA-->
        <td align="center" class="ttotal" width="40"><span class="Estilo33"><?php echo number_format($acum_qui, 2, '.', ''); ?></span></td>
        <?php if ($rucepm<>'20106482897') { ?>
        <!-- TOTAL ADELANTOS -->
        <td align="center" class="ttotal" width="45"><span class="Estilo33"><?php echo number_format($acum_adelanto, 2, '.', ''); ?></span></td>
        <!-- TOTAL PRESTAMOS -->
        <td align="center" class="ttotal" width="40"><span class="Estilo33"><?php echo number_format($acum_prestado, 2, '.', ''); ?></span></td>
        <!-- TOTAL FALTAS / SUSPENCION -->
        <td align="center" class="ttotal" width="40"><span class="Estilo33"><?php echo number_format($acum_falta, 2, '.', ''); ?></span>
        </td>
      <?php } ?>
       <!--- TOTAL DESCUENTOS -->
        <td class="ttotal" align="center" width="50"><span class="Estilo33"><?php echo number_format($acum_rtotalde, 2, '.', ''); ?></span></td>
        <!--- TOTAL ESALUD -->
        <td class="ttotal" align="center" width="45"><span class="Estilo33"><?php echo number_format($acum_salud, 2, '.', ''); ?></span></td>
        <td width="40">&nbsp;</td><?php endif; ?>
      </tr>
</table>
&nbsp;
<?php if ($xperiodo=='GRATIFICACION' OR $xperiodo=='MENSUAL'): ?>
<table width="908" border="0">
  <tr align="center">
    <td class="tban" width="20"><strong>#</strong></td>
    <td class="tban" width="50"><span class="Estilo13">DNI</span></td>
    <td class="tban" width="160"><span class="Estilo13">NOMBRE</span></td>
    <td class="tban" width="110"><span class="Estilo13">CARGO</span></td>
    <td class="tban" width="65"><span class="Estilo13">FONDO</span></td>
    <td class="tban" width="75"><span class="Estilo13">CUSPP</span></td>
    <td class="tban" width="75"><span class="Estilo13">AREA</span></td>
    <td class="tban" width="50"><span class="Estilo13">INGRESO </span></td>
    <?php if ($xperiodo=='MENSUAL'): ?>
    <td class="tban" width="50"><span class="Estilo13">ING. VACA.</span></td>
    <td class="tban" width="50"><span class="Estilo13">TER. VACA.</span></td>
    <td class="tban" width="50"><span class="Estilo13">DIAS VACA.</span></td>
    <?php endif; ?>
  </tr>
<?php $cuent =1;
while($row2=mysqli_fetch_array($rsecun,MYSQLI_ASSOC)){ ?>
  <tr>
    <td align="center" class="tborde"><span class="Estilo33"><?php echo $cuent ++; ?></span></td>
    <td align="center" class="tborde"><span class="Estilo33"><?php echo $row2['histdet_ndoc']; ?></span></td>
    <td class="tborde"><span class="Estilo33"><?php echo $row2['histdet_nomape']; ?></span></td>
    <td align="center" class="tborde"><span class="Estilo33"><?php echo $row2['histdet_cargo']; ?></span></td>
    <td align="center" class="tborde"><span class="Estilo33"><?php echo $row2['histdet_fondo']; ?></span></td>
    <td align="center" class="tborde"><span class="Estilo33"><?php echo $row2['histdet_cuspp']; ?></span></td>
    <td align="center" class="tborde"><span class="Estilo33"><?php echo $row2['histdet_area']; ?></span></td>
    <td align="center" class="tborde"><span class="Estilo33"><?php echo $row2['histdet_fecing']; ?></span></td>
    <?php if ($xperiodo=='MENSUAL'): ?>
    <td align="center" class="tborde"><span class="Estilo33"></span></td>
    <td align="center" class="tborde"><span class="Estilo33"></span></td>
    <td align="center" class="tborde"><span class="Estilo33"></span></td>
    <?php endif; ?>
  </tr>
<?php } ?>
</table>
<?php endif; ?>
<?php if ($xperiodo=='CTS'): ?>
  <table width="908" border="0">
    <tr align="center">
      <td  class="trem" width="790"><span class="Estilo2"><?=$subtitle.' '.$subtitle2; ?> </span></td>
    </tr>
  </table>
  <table width="908" border="0">
    <tr align="center">
      <td class="tban" width="20"><strong>#</strong></td>
      <td class="tban" width="75"><span class="Estilo13">AREA</span></td>
      <td class="tban" width="50"><span class="Estilo13">DNI</span></td>
      <td class="tban" width="160"><span class="Estilo13">NOMBRE</span></td>
      <td class="tban" width="55"><span class="Estilo33">REMUNERACION BASICA</span></td>
      <?php if ($rucepm!='20106482897'): ?>
      <td class="tban" width="55"><span class="Estilo33">COMISIONES</span></td>
      <?php endif; ?>
      <?php if ($rucepm=='20106482897'): ?>
      <td class="tban" width="55"><span class="Estilo33">INC. AFP</span></td>
      <?php endif; ?>
      <td class="tban" width="55"><span class="Estilo33">FAMILIAR</span></td>
      <td class="tban" width="55"><span class="Estilo33">1/6 GRATIF</span></td>
      <td class="tban" width="55"><span class="Estilo33">TOTAL</span></td>
      <td class="tban" width="55"><span class="Estilo33"><?=$campo; ?></span></td>
      <td class="tban" width="55"><span class="Estilo33">TOTAL A PAGAR</span></td>
    </tr>
    <?php $cuent =1;
    while($row2=mysqli_fetch_array($rsecun,MYSQLI_ASSOC)){ ?>
      <tr>
        <td align="center" class="tborde"><span class="Estilo33"><?= $cuent ++; ?></span></td>
        <td align="center" class="tborde"><span class="Estilo33"><?= $row2['histdet_area']; ?></span></td>
        <td align="center" class="tborde"><span class="Estilo33"><?php
          $ndocu =$row2['histdet_ndoc'];  echo $ndocu;?></span></td>
        <td class="tborde"><span class="Estilo33"><?= $row2['histdet_nomape']; ?></span></td>
        <td align="center" class="tborde"><span class="Estilo33"><?php
          $xsuel=$row2['histdet_sueldo']; $acum_xsuel =$acum_xsuel+$xsuel; echo $xsuel; ?></span></td>
        <td align="center" class="tborde"><span class="Estilo33"><?php
        if ($rucepm=='20106482897'){
        $sqlccomi="SELECT IncAfp('$ndocu', '$hempid', 2) AS comiss,Rresumen('$ndocu','$id','ASIGF',0) AS fam,
            CTS('$ndocu','$hempid','CG','$xhasta') AS cgra,CTS('$ndocu','$hempid','CT','$xhasta') AS ctotal";
        } else {
          $sqlccomi="SELECT CTS('$ndocu','$hempid','CS','$xhasta') AS comiss,Rresumen('$ndocu','$id','ASIGF',0) AS fam,
          CTS('$ndocu','$hempid','CG','$xhasta') AS cgra,CTS('$ndocu','$hempid','CT','$xhasta') AS ctotal";
        }
        $rccomi=mysqli_query($con,$sqlccomi);
        $rscomi=mysqli_fetch_array($rccomi,MYSQLI_ASSOC);
        $comiss=$rscomi['comiss']; $acum_comiss= $acum_comiss + $comiss;
        $fam=$rscomi['fam']; $acum_fam = $acum_fam +$fam;
        $cgra=$rscomi['cgra']; $acum_cgra = $acum_cgra +$cgra;
        $ctotal = $rscomi['ctotal']; $acum_ctotal = $acum_ctotal+$ctotal;
        $tcts = $comiss + $xsuel + $fam + $cgra; $acum_cts= $acum_cts +$tcts;
        echo number_format($comiss, 2, '.', '');

         ?></span></td>
        <td align="center" class="tborde"><span class="Estilo33"><?=$fam;?></span></td>
        <td align="center" class="tborde"><span class="Estilo33"><?=number_format($cgra, 2, '.', '');?></span></td>
        <td align="center" class="tborde"><span class="Estilo33"><?=number_format($tcts, 2, '.', '');?></span></td>
        <td align="center" class="tborde"><span class="Estilo33"><?=number_format($ctotal, 2, '.', '');?></span></td>
        <td align="center" class="tborde"><span class="Estilo33"><?=number_format($ctotal, 2, '.', '');?></span></td>

      </tr>
      <?php } ?>
      <tr>
        <td align="right" colspan="4"><span class="Estilo33"><strong>TOTAL</strong></span></td>
        <td align="center" class="ttotal"><span class="Estilo33"><?=number_format($acum_xsuel, 2, '.', '');?></span></td>
        <td align="center" class="ttotal"><span class="Estilo33"><?=number_format($acum_comiss, 2, '.', '');?></span></td>
        <td align="center" class="ttotal"><span class="Estilo33"><?=number_format($acum_fam, 2, '.', '');?></span></td>
        <td align="center" class="ttotal"><span class="Estilo33"><?=number_format($acum_cgra, 2, '.', '');?></span></td>
        <td align="center" class="ttotal"><span class="Estilo33"><?=number_format($acum_cts, 2, '.', '');?></span></td>
        <td align="center" class="ttotal"><span class="Estilo33"><?=number_format($acum_ctotal, 2, '.', '');?></span></td>
        <td align="center" ><span class="Estilo33">&nbsp;</span></td>
      </tr>
</table>
<?php endif; ?>
</page_header>
</page>
<?php }?>
<?php if ($opc=='bancos'){ ?>
<page orientation="landscape" style="font-size: 9px" backtop="1mm" backbottom="5mm" backleft="1mm" backright="1mm">
  <page_header>
  <table width="960" height="104" border="0">
    <tr align="center">
      <td class="tban" width="120" rowspan="2"><span class="Estilo2">DATOS GENERALES </span></td>
      <td width="127" rowspan="2"><br />
      <br /></td>
      <td width="290" rowspan="3">&nbsp;</td>
      <td width="470">&nbsp;</td>
    </tr>
    <tr align="left">
      <td height="23"><span class="Estilo4">Nota 1: Los formatos de celda deben ser de tipo texto </span></td>
    </tr>
    <tr>
      <td height="23" class="tban" align="center"><span class="Estilo2">C&oacute;digo de Cliente <?=$row_emp;?> </span></td>
      <td width="127" class="tban" align="center"><span class="Estilo2">Tipo de Planilla </span></td>
      <td width="470"><span class="Estilo4">Nota 2: A modo de ejemplo, se ha ingresado datos ficticios en color rojo. </span></td>
    </tr>
    <tr align="center">
      <td class="tborde"><span class="Estilo2"><?php echo $ccliente; ?></span></td>
      <td class="tborde"><span class="Estilo2">HABER</span></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  &nbsp;
  <table width="994" border="0">
    <tr align="center">
      <td class="tban" width="120"><span class="Estilo2">DATOS DEL CARGO</span></td>
      <td colspan="7">&nbsp;</td>
    </tr>
    <tr align="center">
      <td width="120" class="tban"><span class="subban">Tipo de Registro </span></td>
      <td class="tban" width="95"><span class="subban">Cantidad de abonos de la planilla </span></td>
      <td class="tban" width="105"><span class="subban">Fecha de proceso </span></td>
      <td class="tban" width="70"><span class="subban">Subtipo de Planilla de Haberes </span></td>
      <td class="tban" width="90"><span class="subban">Tipo de Cuenta de cargo </span></td>
      <td class="tban" width="170"><span class="subban">Cuenta de Cargo </span></td>
      <td class="tban" width="100"><span class="subban">Monto total de la planilla </span></td>
      <td class="tban" width="110"><span class="subban">Referencia de la planilla </span></td>
    </tr>
    <tr align="center">
      <td class="tborde"><span class="subban">C</span></td>
      <td class="tborde"><span class="subban"><?php echo '0000'.$cantid; ?></span></td>
      <td class="tborde"><span class="subban"><?php echo $fecrepor; ?></span></td>
      <td class="tborde"><span class="subban">X</span></td>
      <td class="tborde"><span class="subban">C</span></td>
      <td class="tborde"><span class="subban"><?php echo $cbanco; ?></span></td>
      <td class="tborde"><span class="subban"><?php echo $tbanco=$ttrem-$ttdesc; ?></span></td>
      <td class="tborde"><span class="subban">Referencia Haberes </span></td>
    </tr>
  </table>

  &nbsp;
  <table width="993" border="0">
    <tr align="center">
      <td  class="tban" width="120"><span class="Estilo2">DATOS DEL ABONO </span></td>
      <td colspan="8">&nbsp;</td>
    </tr>
    <tr align="center">
      <td class="tban" width="120"><span class="subban">Tipo de Registro </span></td>
      <td class="tban" width="95"><span class="subban">Tipo de Cuenta de Abono </span></td>
      <td class="tban" width="105"><span class="subban">Cuenta de Abono </span></td>
      <td class="tban" width="70"><span class="subban">Tipo de documento de Identidad </span></td>
      <td class="tban" width="92"><span class="subban">N&uacute;mero de documento de identidad </span></td>
      <td class="tban" width="172"><span class="subban">Nombre del trabajador </span></td>
      <td class="tban" width="100"><span class="subban">Tipo de Moneda de Abono </span></td>
      <td class="tban" width="110"><span class="subban">Monto de Abono </span></td>
      <td class="tban" width="80"><span class="subban">Validaci&oacute;n IDC del proveedor vs Cuenta </span></td>
    </tr>
<?php while($row3=mysqli_fetch_array($rbanc,MYSQLI_ASSOC)){ ?>
    <tr>
      <td align="center" class="tborde"> <span class="Estilo13">A</span></td>
      <td align="center" class="tborde"><span class="Estilo13">A</span></td>
      <td align="center" class="tborde"><span class="Estilo13"><?php echo $row3['histdet_ncuenta']; ?></span></td>
      <td align="center" class="tborde"><span class="Estilo13"><?php echo $row3['histdet_tdoc']; ?></span></td>
      <td align="center" class="tborde"><span class="Estilo13"><?php echo $row3['histdet_ndoc']; ?></span></td>
      <td align="left" class="tborde"><span class="Estilo3"><?php echo $row3['histdet_nomape']; ?></span></td>
      <td align="center" class="tborde"><span class="Estilo3">S</span></td>
      <td align="center" class="tborde"><span class="Estilo13"><?php
      $dn = $row3['histdet_ndoc'];
      #TOTAL REMUNERACIONES
        $stire="SELECT truncate(sum(histmov_monto),2) tire FROM sys_rh_historico_moviento WHERE
         hist_id='$id' and histmov_ndoc='$dn' and histmov_tipo=1";
         $srtire  = mysqli_query($con,$stire);
         $srtiree=mysqli_fetch_array($srtire,MYSQLI_ASSOC);
         $tirem=$srtiree['tire'];

         $stide="SELECT truncate(sum(histmov_monto),2) tird FROM sys_rh_historico_moviento WHERE
          hist_id='$id' and histmov_ndoc='$dn' and histmov_tipo=0";
          $srtide  = mysqli_query($con,$stide);
          $srtidee=mysqli_fetch_array($srtide,MYSQLI_ASSOC);
          $tidesc=$srtidee['tird'];
        echo $tirem-$tidesc;
      ?></span></td>
      <td align="center" class="tborde"><span class="Estilo13">S</span></td>
    </tr>
  <?php } ?>
  </table>
</page_header>
</page>
<?php }?>
