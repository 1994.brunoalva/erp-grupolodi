<?php
error_reporting(E_ERROR | E_PARSE);
include "../../../Funciones/BD.php";
include "../../Funciones/Recursos/Liquidacion.php";

$opc = $_REQUEST['opc'];
if ($opc =='resumen') {
  $emp = $_REQUEST['emp'];
  #datos basicos para el reporte
  $sqlemp = "SELECT UPPER(emp_nombre) emp_nombre,UPPER(emp_direccion) emp_direccion,emp_ruc FROM sys_empresas WHERE  emp_id = '$emp'";
    $rsqle = mysqli_query($con,$sqlemp);
    $rem = mysqli_fetch_array($rsqle,MYSQLI_ASSOC);
    $nomemp = $rem['emp_nombre'];
    $rucemp = $rem['emp_ruc'];
    $diremp = 'JR. EDGAR ZUÑIGA 165 - SAN LUIS';
  #datos personales
    $dni = $_GET['id'];
    $permot = $_GET['xmov'];
    #fecha pago
    $perfin = $_GET['fecpago'];
    $perffin=date("d-m-Y", strtotime($perfin));
    $perffin= str_replace('-','/',$perffin);
    #desde
    $desliqui=date("d-m-Y", strtotime($desliqui));
    $desliqui= str_replace('-','/',$desliqui);
    #hasta
    $hasliqui=date("d-m-Y", strtotime($hasliqui));
    $hasliqui= str_replace('-','/',$hasliqui);
  #Fecha del reporte
  $sqlfecha = "SELECT Nmes(EXTRACT(MONTH FROM now())) AS MES,EXTRACT(YEAR FROM NOW()) AS ANUAL,EXTRACT(DAY FROM NOW()) DIA";
    $rsqlf = mysqli_query($con,$sqlfecha);
    $rfec = mysqli_fetch_array($rsqlf,MYSQLI_ASSOC);
    $nommes = $rfec['MES'];
    $nomanu = $rfec['ANUAL'];
    $nomdia = $rfec['DIA'];
}
?>
<style>
  .p1 {
    line-height: 200%;
    text-align: justify;
  }
  .title {font-size: 12px; font-weight: bold; }
  .title2 {font-size: 11px; }
  .title3 {font-size: 11px; font-weight: bold; margin-left: 15px;}
  .title4 {font-size: 11px; font-weight: bold; margin-left: 85px;}

  .title33 {font-size: 9px;  margin-left: 85px;}
  .info3 {font-size: 11px; font-weight: bold; }
  .hr1 {
    height: 0.8px;
    margin-left: 0%;
    margin-top: 0%;
    margin-bottom: 0%;
    background-color: black;
  }
  .top {
    margin-top: 10px;
  }
  .topp {
    margin-top: 15px;
  }
  .Subtitle {font-size: 10px; font-weight: bold; }
  .Estilo11 {font-size: 7px; font-weight: bold; }
  .Estilo13 {font-size: 8px; font-weight: bold; }
  .tborde {
    border-radius: 6px 6px 6px 6px;
    -moz-border-radius: 6px 6px 6px 6px;
    -webkit-border-radius: 6px 6px 6px 6px;
    border: 1.1px solid #000000;
  }
  .p1 {
    text-align: justify;
    line-height: 1.2;
    font-size: 11px;
  }
</style>

<page orientation="portrait" style="font-size: 7px" backtop="1mm" backbottom="1mm" backleft="1mm" backright="1mm">
<page_header>
  <?php if ($opc =='resumen'): ?>


  <table border="0">
    <tr>
      <td width="740"><span class="title"><?=$nomemp;?></span></td>
    </tr>
    <tr>
      <td ><span class="title2"><?=$diremp;?></span></td>
    </tr>
    <tr>
      <td><span class="title2">RUC: <?=$rucemp;?></span></td>
    </tr>
    <tr>
      <td align="center"><span class="title"><u>LIQUIDACION DE BENEFICIOS SOCIALES</u></span></td>
    </tr>
    <tr>
      <td align="center"><span class="Subtitle">ART. 29&ordm; D.S N&ordm; 001 - 97 - TR - TUO - L - CTS </span></td>
    </tr>
  </table>
  <table border="0">
  <tr>
    <td width="250"><span class="title">1. DATOS DEL TRABAJADOR </span></td>
    <td width="250">&nbsp;</td>
    <td width="220">&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title3">NOMBRES Y APELLIDOS </span></td>
    <td><span class="info3">: <?= $xnom;?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title3">CATEGORIA</span></td>
    <td><span class="info3">: <?=$percat;?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title3">OCUPACION</span></td>
    <td><span class="info3">: <?=$xcar;?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title3">MOTIVO DEL CESE</span></td>
    <td><span class="info3">: <?=$permot;?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title3">FECHA DE INGRESO</span> </td>
    <td><span class="info3">: <?=$pering;?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title3">FECHA DE CESE</span> </td>
    <td><span class="info3">: <?=$perffin;?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr >
    <td><span class="title3">PERIODOS LIQUIDADOS</span> </td>
    <td><span class="info3">: DESDE <?=$pering.' HASTA '.$desliqui;?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr >
    <td><span class="title3">PERIODOS POR LIQUIDAR</span> </td>
    <td><span class="info3">: DESDE <?=$hasliqui.' HASTA '.$perffin;?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title3">TIEMPO COMPUTABLE: </span></td>
    <td><span class="info3">: <?=$tc; ?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr></table>
  <table border="0"><tr>
    <td width="250"><span class="title">2. REMUNERACION COMPUTABLE </span></td>
    <td width="250" colspan="3">&nbsp;</td>
    <td width="220">&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title3">BASICO</span></td>
    <td width="25"><span class="info3">: S/</span></td>
    <td align="right" width="90"><span class="info3"><?=''.$basicor.'';?></span></td>
    <td width="90">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title3">COMISIONES (PROMEDIO SEMESTRAL)</span></td>
    <td width="25"><span class="info3">: S/</span></td>
    <td align="right" width="90"><span class="info3"><?=''.$coms.'';?></span></td>
    <td width="90">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title3">1/6 GRATIFICACION</span></td>
    <td ><span class="info3">: S/ </span></td>
    <td align="right" width="90"><span class="info3"><?=''.$comg.'';?></span></td>
    <td width="90">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><span class="title3 top">TOTAL</span></td>
    <td width="20"><span class="info3 top">: S/</span></td>
    <td align="right" width="90"><span class="info3"><hr class="hr1"><?=''.$rctotal.'';?></span></td>
    <td  >&nbsp;</td>
<td  >&nbsp;</td>
  </tr></table>
  <table border="0">
  <tr>
    <td width="250">&nbsp;</td>
    <td width="250">&nbsp;</td>
    <td width="90">&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title">3. CALCULOS </span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title3"><u>CALCULO DE C.T.S.:</u> <?=$tc; ?></span></td>
    <td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>
  <tr>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$rctotal;?> / 12 &nbsp;&nbsp;&nbsp;X&nbsp;&nbsp;&nbsp;<?=$mescts;?></span></td>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;<?php echo number_format($ctsmen, 2,",",".");?></span></td>
    <td>&nbsp;</td>

  </tr>
  <tr>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$rctotal;?> / 12  / 30 &nbsp;&nbsp;&nbsp;X&nbsp;&nbsp;&nbsp;<?=$diacts;?></span></td>
    <td><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;<?php echo number_format($ctsdia, 2,",",".");?></span></td>
    <td>&nbsp;</td>

  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="right"><hr class="hr1"><span class="info3">S/</span></td>
    <td align="right"><span class="info3 topp"><?php $ctstotal = $ctsmen+$ctsdia; echo number_format($ctstotal, 2,",",".");?></span></td>

  </tr>
  <tr>
    <td><span class="title3"><u>VACACIONES TRUNCAS:</u> <?=$tv;?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$basi;?> / 12 &nbsp;&nbsp;&nbsp;X&nbsp;&nbsp;&nbsp;<?=$mesva;?></span></td>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;<?php echo number_format($vacmen, 2,",",".");?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$basi;?> / 12 / 30&nbsp;&nbsp;&nbsp;X&nbsp;&nbsp;&nbsp;<?=$diasva;?></span></td>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;<?php echo number_format($vacdia, 2,",",".");?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td align="right"><hr class="hr1"><span class="info3">S/</span></td>
    <td align="right"><span class="info3 topp"><?php $vactotal = $vacmen+$vacdia; echo number_format($vactotal, 2,",",".");?></span></td>
  </tr>
  <tr>
    <td><span class="title3"><u>GRATIFICACIONES TRUNCAS:</u> <?=$tg;?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$gbasico;?> / 12 &nbsp;&nbsp;&nbsp;X&nbsp;&nbsp;&nbsp;<?=$mesgr;?></span></td>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;<?php echo number_format($grames, 2,",",".");?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$gbasico;?> / 12 / 30 &nbsp;&nbsp;&nbsp;X&nbsp;&nbsp;&nbsp;<?=$diasgr;?></span></td>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;<?php echo number_format($gradia, 2,",",".");?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="title33">BONIFICACION EXTRAORDINARIO LEY 29714</span></td>
    <td ><span class="title4">S/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$gporcen;?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="right"><hr class="hr1"><span class="info3">S/</span></td>
    <td align="right"><span class="info3 topp"><?php $gratotal = $grames+$gradia+$gporcen; echo number_format($gratotal, 2,",",".");?></span></td>
  </tr>
  <tr>
    <td><span class="title3">DESCUENTOS REALIZADOS: </span></td>
    <td><span class="title3">DESCUENTOS LEY </span></td>
    <td></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="right"><span class="title3"><hr class="hr1"><?=$perfon.' - S/ '.$desvac;?></span></td>
    <td align="left"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="right"><span class="info3 top">S/</span></td>
    <td align="right"><hr class="hr1"><span class="info3"><?=$desvac;?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="right"><span class="title ">TOTAL A PAGAR </span></td>
    <td align="right"><span class="title"><hr class="hr1">&nbsp;&nbsp;&nbsp;&nbsp;S/ <?=$totalpago;?></span><hr class="hr1"></td>
  </tr></table>
    <table border="0"><tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" width="740"><span class="title">SON: <?=$tletras;?> </span></td>
  </tr>
  <tr>
    <td colspan="3"><p class="p1">Por la presente dejo constancia del pago de mis Beneficios Sociales que por ley me corresponde, por tal motivo, firmo la presente en se&ntilde;al de conformidad, no teniendo nada que reclamar a la empresa por conceptos de mis Beneficios Sociales ni por nigun otro concepto. </p></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp; </td>
  </tr>
  <tr>
    <td colspan="3"><p class="p1">LIMA, <?=$nomdia;?> DE <?=$nommes;?> DEL <?=$nomanu;?></p></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td class="tborde" align="center" ><span class="info3">TOTAL DE DERECHOS PAGAGOS (Dato Contable):</span></td>
    <td align="right"><span class="info3">Recibi Conforme </span></td>
    <td align="center">.....................................................................</td>

  </tr>
  <tr>
   <td align="center" class="tborde"><span class="info3"><?=$totalcont;?></span></td>
    <td>&nbsp;</td>
    <td align="center"><span class="info3"><?= $xnom;?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center"><span class="info3">D.N.I.: <?= $xnum;?></span></td>
  </tr>

</table>
<?php endif; ?>
</page_header>
</page>
