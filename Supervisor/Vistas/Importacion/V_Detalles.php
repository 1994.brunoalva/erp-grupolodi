<?php
  include "../Funciones/BD.php";
  include "Funciones/Importacion/Listas.php";
  if ($_GET['opc'] != '') {
    if ($_GET['opc']=='folder') {
      $titulo ='Detalles del Folder';
      include "Vistas/Modal/M_Marca.php";
      include "Vistas/Modal/M_Pais.php";
      include "Vistas/Modal/M_Puerto.php";
    }
    if ($_GET['opc']=='provee') {
      $titulo ='Detalles del Proveedor';
      include "Vistas/Modal/B_Proveedor.php";
      include "Vistas/Modal/M_Proveedor.php";
    }
    if ($_GET['opc']=='produc') {
      $titulo ='Listado de Productos';
      include "Vistas/Modal/M_Marca.php";
      include "Vistas/Modal/M_Modelo.php";
      include "Vistas/Modal/B_Producto.php";
    }
    if ($_GET['opc']=='orden') {
      $titulo ='Detalles de la Orden';
      include "Vistas/Modal/M_Forwarder.php";
      include "Vistas/Modal/M_Despacho.php";
      include "Vistas/Modal/M_Linea.php";
      include "Vistas/Modal/M_Nave.php";
      include "Vistas/Modal/M_Almacen.php";
    }
    if ($_GET['opc']=='seguro') {
      $titulo ='Detalles de la P&oacute;liza';
      include "Vistas/Modal/M_Poliza.php";
      include "Vistas/Modal/M_Tasa.php";
    }
    if ($_GET['opc']=='packing') {
      $titulo ='Detalles del Packing';
      include "Vistas/Modal/M_Contenedor.php";
    }
    if ($_GET['opc']=='aduana') {
      $titulo ='Detalles de Aduana';
      include "Vistas/Modal/M_Agencia.php";
    }
    if ($_GET['opc']=='pagos') {
      $titulo ='Detalles del Pago';
    }
  }
 ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-lg-12">
          <div class="row" id="titulo">
            <div class="col-lg-3">
              <h4 class="azul"><i class="fa fa-folder-open-o fa-fw"></i><strong><?=$titulo;?></strong></h4>
            </div>
            <div class="col-lg-9 text-right" >
              <a href="index.php?menu=2&opc=folder"  class="btn btn-primary"><i class="fa fa-plus fa-2x"></i> Folio</a>
              <a href="index.php?menu=2&opc=provee" class="btn btn-default"><i class="fa fa-group fa-2x"></i> Proveedor</a>
              <a href="index.php?menu=2&opc=produc" class="btn btn-info"><i class="fa fa-asterisk fa-2x"></i> Producto</a>
              <a href="index.php?menu=2&opc=orden" class="btn btn-danger"><i class="fa fa-bookmark-o fa-2x"></i> Orden</a>
              <a href="index.php?menu=2&opc=seguro" class="btn btn-success"><i class="fa fa-check-circle-o fa-2x"></i> Seguro</a>
              <a href="index.php?menu=2&opc=packing" class="btn btn-warning"><i class="fa fa-list fa-2x"></i> Packing</a>
              <a href="index.php?menu=2&opc=aduana" class="btn btn-info"><i class="fa fa-truck fa-2x"></i> Aduanas</a>
              <a href="index.php?menu=2&opc=pagos" class="btn btn-success"><i class="fa fa-money fa-2x"></i> Pagos</a>
            </div>
          </div>
        </div>
        <div class="table-responsive col-lg-12"><hr class="black" />
          <?php if ($_GET['opc']=='folder'): ?>
            <form name="frm_folio" id="frm_folio"  class="form-group" action="#" method="get">
                <div class="row">
                  <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                    <label>FOLIO:</label>
                      <div id="div_folios" class="input-group">
                        <input id="xfolder" name="xfolder" type="text" class="form-control" readonly placeholder="Ej: 2019" aria-describedby="basic-addon1" value="<?php echo $folder_año; ?>">
                        <span class="input-group-addon" id="basic-addon1">N°</span>
                        <input id="xnrfolder" name="xnrfolder" type="text" class="form-control" readonly placeholder="Folio" aria-describedby="basic-addon1" value="<?php echo $folder_nr; ?>">
                      </div>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                      <label>EMPRESA:</label>
                      <select name="empresa"  id="empresa" class="form-control selectpicker show-tick" >
                          <option value="Seleccione"  selected>--</option>
                          <?php foreach ($dataEmpresa as $empresa) { ?>
                                  <option value="<?= $empresa->id ?>" ><?= $empresa->nombre ?></option>
                          <?php } ?>
                      </select>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>INCOTERM:</label>
                      <select name="folio_incoterm"  id="folio_incoterm" class="form-control "   >
                        <option value="" selected>--</option>
                        <option value="FOB" >FOB</option>
                        <option value="CFR" >CFR</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label>TIPO DE CTN:</label>
                      <select name="type_ctn"  id="type_ctn" class="form-control " >
                        <option value=""  selected>--</option>
                          <option value="40HC">40HC</option>
                          <option value="20ST">20ST</option>
                      </select>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label for="" class="control-label" style="padding-right:5px !important">QTY CTN</label>
                      <input type="text" id="folio_qty" name="folio_qty" class="form-control"  value="<?php echo $folder_qty; ?>" onchange="calcularIncoTerm(this.value)">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>TIPO DE PRODUCTO:</label>
                      <select name="tipo_pr" id="categoria"  class="form-control selectpicker show-tick">
                        <option value="Seleccione"  selected>--</option>
                        <?php foreach ($dataTipoProducto as $tipoProoducto) { ?>
                            <option value="<?= $tipoProoducto->cat_id ?>"><?= $tipoProoducto->cat_descripcion ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>MARCA:</label>
                      <select name="subcategoria"  id="subcategoria" class="form-control" ></select>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>PA&Iacute;S:</label>
                      <select name="txtpais" class="form-control">
                          <option value="" disabled selected>--</option>
                          <?php foreach ($dataPais as $pais) { ?>
                              <option value="<?= $pais->id ?>"><?= $pais->nombre ?></option>
                          <?php }
                           echo '<option data-toggle="modal" data-target="#M_Pais">AÑADIR</option>'; ?>
                      </select>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                        <label>PUERTO:</label>
                        <select name="txtpuerto" class="form-control selectpicker show-tick">
                            <option value="" disabled selected>--</option>
                            <?='<option data-toggle="modal" data-target="#M_Puerto">AÑADIR</option>'; ?>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-4 top">
                      <button type="submit" class="btn btn-primary" name="Guardar" value="Insertar">Guardar </button>
                      <button type="reset" class="btn btn-default">Limpiar</button>
                      <a href="index.php?menu=1" class="btn btn-warning"><i class="glyphicon glyphicon-chevron-left"></i></a>
                    </div>
                </div><!--ROW-->
            </form>
              <?php endif; ?>
              <?php if ($_GET['opc']=='provee'): ?>
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-4  col-md-4" >
                      <label>Proveedor</label>
                      <div class="row">
                          <div class="col-xs-10 col-md-10" style="padding-right:0 !important">
                              <input type="text" class="form-control" readonly name="proveedor" id="proveedor" onclick="BuscarProveedor()">
                              <input type="hidden" name="provee_int_id" id="provee_int_id" class="form-control" value="">
                              <small id="helpId" class="text-muted text-justify">Haga doble clic en la casilla para buscar un proveedor</small>
                          </div>
                          <div class="col-xs-2 col-md-1">
                              <button data-target="#M_Proveedor" data-toggle="modal" type="button" id="nproveedor" class="btn btn-primary">
                                  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                              </button>
                          </div>
                      </div>
                    </div>
                  <div class="col-md-4">
                    <label for="" class="control-label">DIRECICON</label>
                    <input type="text" class="form-control" value="<?php echo $provee_direc; ?>" readonly name="direccion_proveedor" id="direccion_proveedor">
                  </div>
                  <div class="col-md-4">
                    <label for="" class="control-label" style="padding-right:5px !important">TELEFONO</label>
                    <input type="text" readonly value="<?php echo $provee_telf; ?>" id="telefono_proveedor" name="telefono_proveedor" class="form-control">
                  </div>
                  <div class="col-md-4">
                    <label for="" class="control-label">EMAIL</label>
                    <input type="email" readonly id="email_proveedor" value="<?php echo $provee_email; ?>" name="email_proveedor" class="form-control">
                  </div>
                  <div class="col-md-4">
                    <label for="" class="control-label">CONTACTO</label>
                    <input type="text" readonly id="proveedor_contacto" value="<?php echo $provee_contac; ?>" name="proveedor_contacto" class="form-control">
                  </div>
                </div>
              </div>
              <?php endif; ?>
              <?php if ($_GET['opc']=='produc'): ?>
              <div class="row">
                <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                  <label>TIPO DE PRODUCTO:</label>
                  <select name="tipo_pr" id="categoria"  class="form-control selectpicker show-tick">
                    <option value="Seleccione"  selected>--</option>
                    <?php foreach ($dataTipoProducto as $tipoProoducto) { ?>
                        <option value="<?= $tipoProoducto->cat_id ?>"><?= $tipoProoducto->cat_descripcion ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                  <label>MARCA:</label>
                  <select name="subcategoria"  id="subcategoria" class="form-control" ></select>
                </div>
                <div  class="col-md-6 col-xs-12 col-lg-2">
                    <label>MODELO:</label>
                    <select name="mod" id="mode"  class="form-control"  required>
                        <option value="" selected>--</option>
                        <?php foreach ($dataMod as $unaMod) : ?>
                        <option value="<?= $unaMod->id ?>"><?= $unaMod->nombre ?></option>
                        <?php endforeach; ?>
                        <option data-toggle="modal" data-target="#M_Modelo">AÑADIR</option>
                    </select>
                </div>
                <div id="div_product" class="form-group  col-md-6 col-xs-12 col-lg-6">
                    <div class="col-xs-10 col-md-10" style="padding-right:0 !important">
                    <label for="" class="control-label">PRODUCTO</label>
                        <input type="text" id="product" name="product" onclick="BuscarProducto()" class="form-control" required ="required">
                        <small class='text-danger'>Haga doble click para mostrar los productos disponibles</small>
                        <input type="hidden" id="product_id" name="product_id" class="form-control">
                    </div>
                    <div class="col-xs-1 col-md-1" style="top:26px">
                        <button type="button" id="btn_addProducto" class="btn btn-sm btn-primary" title="Crear Producto">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
                <div id="div_adv" class="form-group col-md-6 col-xs-12 col-lg-1" >
                <label>ADV:</label>
                <div id="div_folios" class="input-group" >
                        <input type="text" class="form-control text-uppercase" name="xordADV" id="xordADV" >
                        <span class="input-group-addon" id="basic-addon1"><strong>%</strong></span>
                    </div>
                </div>
                <div class="form-group col-md-6 col-xs-12 col-lg-2">
                    <label>UNIDAD DE MEDIDA:</label>
                    <select name="medida" id="medida" class="form-control selectpicker show-tick" required >
                        <option value="" selected>--</option>
                            <?php foreach ($dataUni as $unaUni) : ?>
                            <option value="<?= $unaUni->id ?>"><?= $unaUni->nombre ?></option>
                            <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                    <label for="" class="control-label">CANTIDAD</label>
                    <div id="div_folios" class="input-group" >
                        <input id="product_qty" name="product_qty" type="text" class="form-control" onchange="calcularDescuento();"  aria-describedby="basic-addon1" required ="required">
                        <span class="input-group-addon" id="basic-addon1"><strong>CANT</strong></span>
                    </div>
                </div>
                <div  class="form-group col-md-6 col-xs-12 col-lg-2">
                    <label for="" class="control-label">PRECIO UNITARIO:</label>
                    <div id="div_folios" class="input-group" >
                        <input type="text" class="form-control moneda" id="unit_price" name="unit_price" onchange="calcularDescuento();"   aria-describedby="basic-addon1" required ="required">
                        <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                    </div>
                </div>
                <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                    <label for="" class="control-label">DTO EN %:</label>
                    <div id="div_folios" class="input-group" >
                        <input id="discount" name="discount" type="text" class="form-control"  onchange="calcularDescuento();" aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon1"><strong>%</strong></span>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 top">
                    <button type="submit" class=" btn btn-success" id="GuardarPr" name="GuardarPr" value="Insertar"><span class="glyphicon glyphicon-plus" disabled></span> Agregar</button>
                    <a href="index.php?menu=1" class="btn btn-warning" role="button"><i class="glyphicon glyphicon-chevron-left"></i></a>
                </div>
              </div>
            </form>
              <?php endif; ?>
              <?php if ($_GET['opc']=='orden'): ?>
              <div class="row">
                <form name="frm_orden" id="frm_orden"  class="form-group" action="#" method="get">
                  <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                    <label>PROFORMA:</label>
                    <input id="nro_proforma" name="nro_proforma" class="form-control" type="text" value="<?php echo $nro_profo; ?>">
                  </div>
                  <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                    <label>FECHA/PROFORMA:</label>
                    <input name="fecha_proforma" class="form-control" type="date" value="<?php echo $fech_profo; ?>">
                  </div>
                  <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                  <label>FOB TOTAL:</label>
                    <div id="div_folios" class="input-group" >
                      <input name="xordMontoTotal2"  readonly class="form-control" type="text" value="<?php echo $totalFob; ?>">
                      <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                    </div>
                  </div>
                  <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>FLETE/CONTENEDOR:</label>
                      <div id="div_folios" class="input-group" >
                        <input type="text"  class="form-control money" id="xordFleteCNT" name="xordFleteCNT"  onchange="calcularFlete();" onclick="calcularFlete();" >
                        <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                      </div>
                  </div>
                  <div id="div_thc" class="form-group  col-md-6 col-xs-12 col-lg-2" style="">
                    <label>THC / CTN:</label>
                    <div class="input-group" >
                      <input type="text" class="form-control money" name="xordTHC" id="xordTHC" onchange="calcularFlete();" onclick="calcularFlete();">
                      <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                    </div>
                  </div>
                  <div id="div_thc" class="form-group  col-md-6 col-xs-12 col-lg-2" style="">
                    <label>TOTAL FLETE:</label>
                    <div class="input-group" >
                      <input   type="text"    class="form-control text-uppercase"   name="xordTotalFlete"  id="xordTotalFlete">
                      <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                    </div>
                  </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>BL:</label>
                        <input type="text" id="xordBl" name="xordBl" class="form-control ancla"  >
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>FORWARDER:</label>
                        <select name="fowarder" class="form-control selectpicker show-tick">
                            <option value="">--</option>
                            <?php foreach ($dataFowarder as $unFowarder) { ?>
                                <option value="<?= $unFowarder->id ?>" ><?= $unFowarder->nombre ?></option>
                            <?php } ?>
                            <option data-toggle="modal" data-target="#M_Forwarder">AÑADIR</option>
                        </select>
                    </div>
                        <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                          <label>TIPO/DESPACHO</label>
                          <select name="tipo_despacho" class="form-control selectpicker show-tick" >
                            <option value="">--</option>
                            <?php foreach ($dataTipoDesc as $unTipoDesc) { ?>
                                <option value="<?= $unTipoDesc->id ?>" ><?= $unTipoDesc->nombre ?></option>
                            <?php } ?>
                            <option data-toggle="modal" data-target="#M_Despacho">AÑADIR</option>
                          </select>
                        </div>
                        <div class="form-group  col-md-6 col-xs-12 col-lg-2" >
                          <label>LINEA:</label>
                            <select name="linea" class="form-control selectpicker show-tick">
                              <option value="">--</option>
                                <?php foreach ($dataLinea as $linea) { ?>
                                    <option value="<?= $linea->id ?>"><?= $linea->nombre ?></option>
                                <?php } ?>
                              <option data-toggle="modal" data-target="#M_Linea">AÑADIR</option>
                            </select>
                        </div>
                        <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                          <label for="" class="col-md-2" >NAVE:</label>
                          <select name="nave" class="form-control selectpicker show-tick">
                            <option value="" disabled selected>--</option>
                            <?php foreach ($dataNave as $nave) : ?>
                            <option value="<?= $nave->id ?>" ><?= $nave->nombre ?></option>
                            <?php endforeach; ?>
                            <option data-toggle="modal" data-target="#M_Nave">AÑADIR</option>
                          </select>
                        </div>
                        <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                            <label>ETD:</label>
                            <input name="etd" type="date" class="form-control" >
                        </div>
                        <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                          <label >ETA:</label>
                          <input name="eta" type="date" class="form-control" onchange="CalcularVige();"  >
                        </div>
                        <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                          <label>LIBRE/SOBREESTADIA:</label>
                          <input type="text" class="form-control" name="libre_estadia" id="libre_estadia"  readonly  >
                        </div>
                        <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                            <label>ALMACEN:</label>
                            <select name="almacen" class="form-control selectpicker showtick" data-ajax="almacen">
                              <option value="" disabled selected>--</option>
                              <?php foreach ($dataAlmacen as $almacen) { ?>
                                  <option value="<?= $almacen->id ?>" ><?= $almacen->nombre ?></option>
                              <?php } ?>
                              <option data-toggle="modal" data-target="#M_Almacen">AÑADIR</option>
                            </select>
                        </div>
                        <div class="col-md-4 col-lg-4 col-xs-12 top">
                          <button type="submit" class="btn btn-primary" name="GuardarOrden" value="<?php echo $edit; ?>">Grabar </button>
                          <button type="reset" class="btn btn-default">Limpiar</button>
                          <a href="index.php?menu=1" class="btn btn-warning"><i class="glyphicon glyphicon-chevron-left"></i></a>
                        </div>
                </form></div>
              <?php endif; ?>
              <?php if ($_GET['opc']=='seguro'): ?>
                <div class="row">
                <form name="frm_seguro" class="form-group" action="#" method="get">
                    <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                      <label>TOTAL FOB:</label>
                      <div class="input-group" >
                        <input type="text" class="form-control" id="seg_fob_total" value="<?php echo $totalFob; ?>" name="seg_fob_total" onchange="calcularPrimaNeta();" readonly >
                        <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                      </div>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                        <label>TOTAL FLETE:</label>
                        <div class="input-group" >
                          <input type="text" class="form-control" id="seg_flete_total" name="seg_flete_total" value="<?php echo $totalFlete; ?>" onchange="calcularPrimaNeta();"  readonly >
                          <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                        </div>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                      <label for="prima_total" class="control-label">SUMA ASEGURADA:</label>
                      <div class="input-group" >
                        <input type="text"      class="form-control" id="seg_suma_aseg2" name="seg_suma_aseg2" readonly>
                        <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                      </div>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                      <label>REFERENCIA:</label>
                      <input type="text" class="form-control" id="referencia" name="referencia">
                    </div>
                    <div class="form-group col-md-12 col-xs-12 col-lg-2">
                      <label>VIGENCIA DESDE:</label>
                      <input type="date" class="form-control" name="fecha_poliza_ini" id="fecha_poliza_ini">
                    </div>
                    <div class="form-group  col-md-12 col-xs-12 col-lg-2">
                      <label>VIGENCIA HASTA:</label>
                      <input type="date" class="form-control" name="fecha_poliza_fin" id="fecha_poliza_fin">
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                      <label>P&Oacute;LIZA:</label>
                      <select name="nro_poliza" id="nro_poliza" class="form-control selectpicker show tick"  data-ajax="nro_poliza">
                        <option value="" disabled selected>--</option>
                          <?php foreach ($dataPoliza as $nro_poliza) { ?>
                              <option value="<?= $nro_poliza->id ?>"><?= $nro_poliza->nombre ?></option>
                          <?php } ?>
                        <option data-toggle="modal" data-target="#M_Poliza">AÑADIR</option>
                      </select>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                      <label>APLICACI&Oacute;N:</label>
                      <input type="text" class="form-control" id="poliza_aplicacion" name="poliza_aplicacion">
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                        <label>TASA:</label>
                        <div class="input-group" >
                        <select name="seg_tasa" id="seg_tasa" class="form-control selectpicker show tick" data-ajax="seg_tasa" onchange="ShowTasa();calcularPrimaNeta();calcularPrimaTotal();calcularCIF();" >
                        <option value="" disabled selected>--</option>
                        <?php foreach ($dataTasaPoliza as $seg_tasa) { ?>
                        <option value="<?= $seg_tasa->id ?>" ><?= $seg_tasa->nombre ?></option>
                        <?php }?>
                        <option data-toggle="modal" data-target="#M_Tasa">AÑADIR</option>
                        </select>
                        <span class="input-group-addon" id="basic-addon1"><strong>%</strong></span>
                        </div>
                    </div>

                    <input type="hidden" class="form-control" id="seg_tasa_valor" name="seg_tasa_valor" onchange="calcularPrimaNeta();">
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>PRIMA NETA:</label>
                      <div  class="input-group" >
                        <input type="text" class="form-control" id="seg_prima_neta" name="seg_prima_neta" >
                        <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                      </div>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>PRIMA TOTAL:</label>
                      <div class="input-group" >
                        <input type="text" class="form-control" readonly id="seg_prima_total2" name="seg_prima_total2">
                        <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                      </div>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>CIF MONTO TOTAL:</label>
                      <div class="input-group" >
                        <input type="text" class="form-control" readonly id="seg_cif" name="seg_cif">
                        <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                      </div>
                    </div>
                    <div class="col-md-4 col-lg-4 top">
                      <button type="submit" class="btn btn-primary" name="GuardarSeguro" value="<?php echo $edit; ?>">Grabar </button>
                        <button type="reset" class="btn btn-default">Limpiar</button>
                        <a href="index.php?menu=1" class="btn btn-warning"><i class="glyphicon glyphicon-chevron-left"></i></a>
                      </div>
                    </form></div>
              <?php endif; ?>
              <?php if ($_GET['opc']=='packing'): ?>
                <form id="frm_packing" name="frm_packing"   class="form-group" action="#" method="get">
                  <div class="row">
                  <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                    <label>FOLIO:</label>
                      <div id="div_folios" class="input-group">
                        <input id="xfolder" name="xfolder" type="text" class="form-control" readonly placeholder="Ej: 2019" aria-describedby="basic-addon1" value="<?php echo $folder_año; ?>">
                        <span class="input-group-addon" id="basic-addon1">N°</span>
                        <input id="xnrfolder" name="xnrfolder" type="text" class="form-control" readonly placeholder="Folio" aria-describedby="basic-addon1" value="<?php echo $folder_nr; ?>">
                      </div>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                      <label>IMPORTADOR:</label>
                      <select name="empresa"  id="empresa" class="form-control selectpicker show-tick" >
                          <?php if ($emp_id <> '0'){ ?>
                          <option value="<?php echo $emp_id; ?>"  selected><?php echo $emp_nombre; ?></option>
                          <?php }else{ ?>
                          <option value="Seleccione"  selected>Seleccione</option>
                          <?php } ?>
                          <?php foreach ($dataEmpresa as $empresa) { ?>
                                  <option value="<?= $empresa->id ?>" ><?= $empresa->nombre ?></option>
                          <?php } ?>
                      </select>
                    </div>
                    <div class="group col-lg-2 col-lg-2">
                      <label>TIPO DE CTN:</label>
                      <select name="type_ctn"  id="type_ctn" class="form-control " >
                        <option value="<?php echo $folder_tipo; ?>"  selected><?php echo $folder_tipo; ?></option>
                        <optgroup label="Existente">
                            <option value="40HC">40HC</option>
                            <option value="20ST">20ST</option>
                        </option-group>
                      </select>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>MARCA:</label>
                      <select name="marca"  id="marca" class="form-control selectpicker show-tick" >
                        <option value="<?php echo $mar_id; ?>"  selected><?php echo $mar_nombre; ?></option>
                        <!-- <option value="" disabled >----Existente----</option> -->
                        <?php foreach ($dataMarca as $marca) { ?>
                                <option value="<?= $marca->id ?>"><?= $marca->nombre ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>PROCEDENCIA:</label>
                      <select name="pais" id="pais"  class="form-control selectpicker show-tick">
                      <option value="<?php echo $pais_id; ?>"  selected><?php echo $pais_nombre; ?></option>
                      <?php foreach ($dataPais as $pais) { ?>
                              <option value="<?= $pais->id ?>" ><?= $pais->nombre ?></option>
                      <?php } ?>
                    </select>
                    </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>CANTIDAD-TOTAL:</label>
                      <input type="text" class="form-control" id="" name="" >
                    </div>
                    <div class="col-lg-3 col-md-4 top">
                      <a href="#M_Contenedor" data-toggle="modal" class="btn btn-success" id="GuardarPr" name="GuardarPr" value="Insertar"><span class="glyphicon glyphicon-plus" disabled></span> Contenedor</a>
                      <!--<button type="reset" class="btn btn-default">Limpiar</button>
                      <a href="index.php?menu=1" class="btn btn-warning"><i class="glyphicon glyphicon-chevron-left"></i></a>-->
                    </div>
              </div></form>
              <?php endif; ?>
              <?php if ($_GET['opc']=='aduana'): ?>
                <div class="row"><form id="frm_aduana" name="frm_aduana"   class="form-group" action="#" method="get">
                  <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                    <label>AGENCIA DE ADUANAS:</label>
                      <select name="aduana_agencia" id="aduana_agencia" class="form-control selectpicker show-tick " data-ajax="impor_aduana_agencia">
                      <option value="" disabled selected>--</option>
                      <?php foreach ($dataAgencia as $unAgencia) { ?>
                              <option value="<?= $unAgencia->id ?>"><?= $unAgencia->nombre ?></option>
                      <?php } ?>
                      <option data-toggle="modal" data-target="#M_Agencia">AÑADIR</option>
                      </select>
                  </div>
                    <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                      <label>DUA:</label>
                      <input type="text" class="form-control" id="aduana_dua" name="aduana_dua"  >
                    </div>
                      <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                        <label for="prima_total" class="control-label">FECHA DE NUMERACION:</label>
                        <input type="date" class="form-control" id="aduana_fecha_num" name="aduana_fecha_num" >
                      </div>
                      <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                        <label>CANAL:</label>
                        <select name="aduana_canal" id="aduana_canal" class="form-control selectpicker ">
                          <option value="" disabled selected>--</option>
                          <option value="VERDE" >VERDE</option>
                          <option value="ROJO" >ROJO</option>
                          <option value="NARANJA" >NARANJA</option>
                        </select>
                      </div>
                      <div class="form-grou col-xs-12 col-md-4 col-lg-3 top">
                          <button type="submit" class="btn btn-primary" name="GuardarAduana" value="<?php echo $edit; ?>">Grabar </button>
                          <button type="reset" class="btn btn-default">Limpiar</button>
                          <a href="index.php?menu=1" class="btn btn-warning"><i class="glyphicon glyphicon-chevron-left"></i></a>
                      </div>
              </form></div>
              <?php endif; ?>
              <?php if ($_GET['opc']=='pagos'): ?>
              <div class="row">
              <form id="frm_pago" name="frm_pago"   class="form-group" action="#" method="get">
              <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                <label>INCOTERM:</label>
                <input type="text" class="form-control" id="pago_inco" name="pago_inco" value="<?php echo $Incoterm; ?>"  readonly >
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                <label>MONTO TOTAL:</label>
                <div class="input-group" >
                <input type="text" class="form-control" id="pago_fob" value="<?php echo $totalFob; ?>" name="pago_fob" readonly >
                    <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                </div>
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                <label for="prima_total" class="control-label">PENDIENTE:</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="pago_pendiente" value="<?php echo $totalPent; ?>" name="pago_pendiente" readonly >
                  <span class="input-group-addon" id="basic-addon1"><strong>$</strong></span>
                </div>
            </div>
            <div class="form-group  col-md-6 col-xs-12 col-lg-4">
              <label>CONDICION DE PAGO:</label>
              <select name="pago_condicion" class="form-control " required>
                <option value="" disabled selected>--</option>
                <option value="CANCELADO AL 30% DE LA P.I">CANCELADO AL 30% DE LA P.I</option>
                <option value="CANCELADO EL 70% RESTANTE DE LA P.I">CANCELADO EL 70% RESTANTE DE LA P.I</option>
                <option value="CANCELADO AL 100%">CANCELADO AL 100%</option>
                <option value="CANCELADO AL 100% ANTES DEL EMBARQUE">CANCELADO AL 100% ANTES DEL EMBARQUE</option>
              </select>
            </div>
            <div  id="fecha_pago" class="form-group  col-md-6 col-xs-12 col-lg-2" style="display:block;">
              <label>FECHA:</label>
              <input type="date" class="form-control" id="pago_fecha" value="<?php echo $totalFob; ?>" name="pago_fecha"  required  >
            </div>
            <div class="col-lg-2 col-md-4 hidden-xs top">
              <button type="submit" class=" btn btn-sm btn-success" id="GuardarPago" name="GuardarPago" value="Insertar"><span class="glyphicon glyphicon-plus"></span> Agregar</button>
              <a href="index.php?menu=1" class="btn btn-sm btn-warning" role="button"><i class="glyphicon glyphicon-chevron-left"></i></a>
            </div>
          </form>
          <div class="table-responsive col-lg-12" ><hr class="black" />
            <table class="table table-hover"  >
            <thead>
              <tr>
                <th class="text-center">#</th>
                <th class="text-center">CONDICION DE PAGO</th>
                <th class="text-center">FECHA</th>
                <th class="text-center">MONTO</th>
                <th class="text-center">ACTUALIZAR</th>
              </tr>
            </thead>
            <tbody class="text-center">
              <?php
                $fil =1;
                while ($row2 = mysqli_fetch_array($resultConsulPago, MYSQLI_ASSOC)) { ?>
                <tr>
                  <td><?=$fil++;?></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>
                      <button type="submit" class="btn btn-danger btn-sm glyphicon glyphicon-trash" name="eliminarPago"></button>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <?php endif; ?>
        </div><!--col-lg-12-->
      </div>
    </div>
  </div>
</div>
<script>
 $(document).ready(function () {
  //SELECT DEPENDIENTE
  $("#categoria").on('change', function () {
      $("#categoria option:selected").each(function () {
          var id_categoria = $(this).val();
          $.post("Funciones/Importacion/GetMarca.php", { id_categoria: id_categoria }, function(data) {
              $("#subcategoria").html(data);
          });
      });
    });
  });
  function BuscarProveedor() {
    $("#B_Proveedor").modal("show");
  }
  function BuscarProducto() {
    $("#B_Producto").modal("show");
  }
  </script>
