<?php
  include "../Funciones/BD.php";

?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-lg-12">
              <div class="row" id="titulo">
                <div class="col-lg-6">
                  <h2 class="azul"><i class="fa fa-inbox fa-fw"></i><strong>Personal</strong></h2>
                </div>
                <div class="col-lg-6 text-right" >
                  <button class="btn btn-primary" id="show"><i class="fa fa-plus"></i> Agregar Personal</button>
                </div>
                </div>
              </div>
            <div class="col-lg-12">
              <div class="row">
                <div id="titulo2" class="col-lg-6" style="display: none;" >
                  <h2 class="azul"><i class="fa fa-inbox fa-fw"></i><strong>Agregar Personal</strong></h2>
                </div>
                </div>
              </div>
            <div class="table-responsive col-lg-12"><hr class="black" />
              <div id="element2">
               <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                 <thead>
                   <tr>
                    <!-- <th class="text-center">ID</th>-->
                    <!--<th class="text-center">RUC</th>
                    <th class="text-center">RAZON SOCIAL</th>-->
                     <th class="text-center">DNI</th>
                     <th class="text-center">APELLIDOS Y NOMBRES</th>
                     <!--<th class="text-center">CARGO</th>-->

                    <th class="text-center">D. PERSONALES</th>
                    <!--<th class="text-center">D. LABORALES</th>-->
                    <!--<th class="text-center">O. REMUNERACIONES</th>-->
                    <!--<th class="text-center">C. Trabajo</th>-->
                   </tr>
                 </thead>
                 <tbody class="text-center">
                 <?php
                 /*$sql="SELECT sp.per_id,sp.per_ndoc,sp.per_nomape,sl.emp_id,sl.car_id,sc.car_nombre, se.emp_nombre,se.emp_ruc FROM sys_rh_personal sp, sys_rh_personal_lab sl, sys_rh_cargos sc,sys_empresas se
                  WHERE sl.per_ndoc = sp.per_ndoc and sc.car_id = sl.car_id and se.emp_id = sl.emp_id
		              ORDER BY sl.emp_id,se.emp_ruc,sl.per_ndoc";*/
                  $sql="SELECT DISTINCT(sp.per_ndoc),sp.per_id,sp.per_nomape FROM sys_rh_personal sp
ORDER BY sp.per_ndoc";
                 $result=mysqli_query($con,$sql);
                 $ncue = 1;
                 while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ ?>
                   <tr>
                       <!--<td><?php echo $ncue++; ?></td>-->
                       <!--<td><?= $row['emp_ruc']; ?></td>
                       <td><?= $row['emp_nombre']; ?></td>-->
                       <td><?= $row['per_ndoc']; ?></td>
                       <td><?= $row['per_nomape']; ?></td>
                       <!--<td><?= $row['car_nombre']; ?></td>-->

                     <!--EDITAR-->
                       <td class="centeralign">
                           <form  id="EditarE" role="form" action="index.php?menu=13" method="post">
                                <input type="hidden" name="opcion" value="per">
                             <button class="btn btn-warning btn-sm glyphicon glyphicon-user" name="edit" value='<?php echo $row['per_ndoc']; ?>'></button>
                           </form>
                       </td>
                       <!--EDITAR-->
                         <!---<td class="centeralign">
                             <form  id="EditarE" role="form" action="index.php?menu=13" method="post">
                              <input type="hidden" name="opcion" value="lab">
                              <input type="hidden" name="empid" value="<?= $row['emp_id']; ?>">
                               <button class="btn btn-primary btn-sm glyphicon glyphicon-edit" name="edit" value='<?php echo $row['per_ndoc']; ?>'></button>
                             </form>
                         </td>-->

                   </tr>
                     <?php } ?>
                 </tbody>
               </table>
               </div>
              </div>
               <!--Div Oculto-->
               <div id="content">
                 <div id="element" style="display: none;">
                   <form  id="RegistroE" role="form" action="index.php?menu=12" enctype="multipart/form-data"  method="post">
                     <input name="action" type="hidden" value="upload" />
                     <div class="upload_image col-lg-3">
                       <table width="260" height="320"  class="table-responsive">
                       <tr>
                       <td class="bordeazul" align="center"><img id="uploadPreview1" class="img-responsive" src="../Imagenes/Personal/sinf.png" /></td>
                       </tr>
                       <tr>
                         <td height="20" align="center"><input class="form-control btn btn-primary" title="AGREGAR FOTO" accept="image/png, image/jpeg, image/gif" id="uploadImage1" type="file" name="uploadImage1" onchange="previewImage(1);" /></td>
                       </tr>
                       </table>
                     </div>
                      <div class="form-group  col-md-8 col-xs-12 col-lg-2">
                         <label>TIPO DOCUMENTO:</label>
                         <select class="form-control"  name="tdoc" required>
                           <?php
                             #Tipo de documentos
                            $sql2="SELECT doc_sunat_id ,(CASE WHEN doc_sunat_id = 6 THEN 'RUC'
                                    WHEN doc_sunat_id = '0' THEN 'OTROS'
                                    WHEN doc_sunat_id = '1' THEN 'DNI'
                                    WHEN doc_sunat_id = '7' THEN 'PASAPORTE'
                                    WHEN doc_sunat_id = '4' THEN 'CARNET E.'
                                    WHEN doc_sunat_id = 'A' THEN 'CEDULA D.'
                                    ELSE 'NT'
                                    END) AS doc_nombre
                       FROM sys_documentos WHERE doc_tipo='DE IDENTIDAD'
                           ORDER BY doc_sunat_id";
                            $rsql2=mysqli_query($con,$sql2);
                            echo "<option value=''>--</option>";
                            if( $row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC)     ){
                            do{
                               echo '<option value="'.$row2['doc_sunat_id'].'">'.$row2['doc_nombre'].'</option>';
                               } while($row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC));
                            }
                            ?>
                         </select>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>N&Uacute;MERO:</small></label>
                           <input class="form-control" placeholder="000000000" name="xnum" pattern="[0-9]{6,}" title="Ingrese numeros" required>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-5">
                           <label>APELLIDOS Y NOMBRES :</label>
                           <input class="form-control text-uppercase" name="xnom"  pattern="[A-z0-9 ].{1,}" title="Ingrese solo letras" required>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>SEXO:</label>
                           <select class="form-control"  name="xsex" required>
                             <option value="">--</option>
                             <option value="M">MASCULINO</option>
                             <option value="F">FEMENINO</option>
                           </select>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>F. NACIMIENTO:</label>
                           <input class="form-control input-group-sm" type="date"  name="fnac" id="fnac" required>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>ESTADO CIVIL:</label>
                           <select class="form-control"  name="xciv">
                             <option value="">--</option>
                             <option value="S">SOLTERO(A)</option>
                             <option value="C">CASADO(A)</option>
                             <option value="V">VIUDO(A)</option>
                           </select>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-1">
                           <label>Nº HIJOS:</label>
                           <input class="form-control" placeholder="0" name="nhij" pattern="[0-9]" required>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>TELEFONO:</label>
                           <input class="form-control" placeholder="+00-000000" name="xtel" pattern="[0-9+].{6,}">
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>MOVIL:</label>
                           <input class="form-control" placeholder="+00-000000" name="xmov" pattern="[0-9+].{6,}">
                         </div>

                         <div class="form-group col-xs-12 col-md-12 col-lg-3">
                           <label>NACIONALIDAD:</label>
                           <select class="form-control"  name="xnaci" required>
                             <option value="">--</option>
                             <option value="9589">PERU</option>
                             <option value="9011">OTROS PAISES</option>
                           </select>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-4">
                           <label>GRADO DE INST.:</label>
                           <select class="form-control"  name="gins" required>
                             <option value="">--</option>
                             <option value="07">SECUNDARIA COMPLETA</option>
                             <option value="08">TECNICA INCOMPLETA</option>
                             <option value="09">TECNICA COMPLETA</option>
                             <option value="12">UNIVERSITARIA INCOMPLETA</option>
                             <option value="13">UNIVERSITARIA COMPLETA</option>
                             <option value="14">BACHILLER</option>
                             <option value="15">TITULADO</option>
                           </select>
                         </div>

                         <div class="form-group col-xs-12 col-md-12 col-lg-5">
                           <label>PROFESION / TITULO / OCUPACI&Oacute;N:</label>
                           <input class="form-control text-uppercase" placeholder="Ingrese grado de educativo" name="xocu" pattern="[A-z0-9 ].{1,}" required>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-4">
                           <label>EMAIL:</label>
                           <input class="form-control" placeholder="Ingrese email" name="xmail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" >
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-5">
                           <label>DIRECCI&Oacute;N:</label>
                           <input class="form-control text-uppercase" placeholder="ingrese Direccion o Referencia" name="xdir" pattern="[A-z0-9 ].{1,}" required>
                         </div>
                         <!--<div class="col-lg-12"><hr class="black">
                           <h3 class="azul" align="center"><i class="fa fa-building-o fa-fw"></i><strong>Datos Laborales</strong></h3>
                           <hr class="black">
                           <div class="form-group col-xs-12 col-md-12 col-lg-2">
                             <label>FECHA DE INGRESO:</label>
                             <input class="form-control input-group-sm" type="date"  name="fing" id="fing" required>
                           </div>
                           <div class="form-group col-xs-12 col-md-12 col-lg-2">
                              <label>FECHA DE CESE:</label>
                              <input class="form-control input-group-sm" type="date"  name="ffin" id="ffin">
                           </div>
                           <div class="form-group col-xs-12 col-md-12 col-lg-3">
                             <label>CATEGORIA:</label>
                             <select class="form-control"  name="tcate" required>
                               <option value="">--</option>
                               <option value="21">TRABAJADOR</option>
                               <option value="24">PENSIONISTA</option>
                               <option value="98">PRESTADOR DE SERVICIOS</option>
                             </select>
                           </div>
                           <div class="form-group col-xs-12 col-md-12 col-lg-3">
                             <label>ESTADO:</label>
                             <select class="form-control"  name="estad" required>
                               <option value="">--</option>
                               <option value="0">BAJA</option>
                               <option value="1">ACTIVO O SUBSDIADO</option>
                               <option value="2">SIN VINCULO LABORAL</option>
                               <option value="3">SUSPENSION PERFECTA DE LABORES</option>
                             </select>
                           </div>
                           <div class="form-group col-xs-12 col-md-12 col-lg-2">
                             <label>TIPO:</label>
                             <select class="form-control"  name="tiemp" required>
                               <option value="">--</option>
                               <option value="01">EJECUTIVO</option>
                               <option value="02">OBRERO</option>
                               <option value="03">EMPLEADO</option>
                             </select>
                           </div>
                           <div class="form-group col-xs-12 col-md-12 col-lg-2">
                             <label>TIPO DE PLANILLA:</label>
                             <select class="form-control"  name="tpla" required>
                               <option value="">--</option>
                               <option value="GENERAL">GENERAL</option>
                               <option value="MYPE">MYPE</option>
                             </select>
                           </div>
                           <div class="form-group col-xs-12 col-md-12 col-lg-2">
                             <label>TIPO DE BOLETA:</label>
                             <select class="form-control"  name="tbol" required>
                               <option value="">--</option>
                               <option value="QUINCENAL">QUINCENAL</option>
                               <option value="MENSUAL">MENSUAL</option>
                             </select>
                           </div>
                           <div class="form-group col-xs-12 col-md-12 col-lg-4">
                            <label>EMPRESA:</label>
                            <select class="form-control"  name="xemp" id="xemp" required>
                              <?php
                                #$sql2="SELECT * FROM sys_empresas WHERE emp_id ='$id_emp'";
                                $sql2="select se.emp_id,se.emp_nombre from sys_empresas se ORDER BY se.emp_id";
                                $rsql2=mysqli_query($con,$sql2);
                                  echo "<option value=''>--</option>";
                                 if( $row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC)     ){
                                 do{
                                    echo '<option value="'.$row2['emp_id'].'">'.$row2['emp_nombre'].'</option>';
                                    } while($row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC));
                                 }
                               ?>
                           </select>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-4">
                            <label>CARGO:</label>
                            <select class="form-control"  name="xcar" required>
                              <?php
                                #CARGOS
                                 $sqlb="SELECT DISTINCT(car_nombre),car_id FROM sys_rh_cargos	ORDER BY car_nombre";
                                 $rsqlb=mysqli_query($con,$sqlb);
                                 echo "<option value=''>--</option>";
                                 if( $rowb=mysqli_fetch_array($rsqlb,MYSQLI_ASSOC)     ){
                                 do{
                                    echo '<option value="'.$rowb['car_id'].'">'.$rowb['car_nombre'].'</option>';
                                  } while($rowb=mysqli_fetch_array($rsqlb,MYSQLI_ASSOC));
                                 }
                               ?>
                           </select>
                          </div>
                          <div class="form-group col-md-8 col-xs-12 col-lg-2">
                            <label>AREA:</label>
                            <select class="form-control"  name="xarea" required>
                              <?php
                                #AREAS
                                 $sqla="SELECT * FROM sys_rh_area	ORDER BY area_id";
                                 $rsqla=mysqli_query($con,$sqla);
                                 echo "<option value=''>--</option>";
                                 if( $rowa=mysqli_fetch_array($rsqla,MYSQLI_ASSOC)     ){
                                 do{
                                    echo '<option value="'.$rowa['area_id'].'">'.$rowa['area_nombre'].'</option>';
                                  } while($rowa=mysqli_fetch_array($rsqla,MYSQLI_ASSOC));
                                 }
                               ?>
                           </select>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>SUELDO (S/):</label>
                           <input class="form-control" placeholder="0.00" name="xsuel" pattern="\d+(\.\d{2})?">
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>O. PAGOS (S/):</label>
                           <input class="form-control" placeholder="0.00" name="otrop" pattern="\d+(\.\d{2})?">
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>TIPO DE AFP:</label>
                          <select class="form-control"  name="xtfon" required>
                            <option value="">--</option>
                            <option value="REMUNERACION FLUJO">REMUNERACION F.</option>
                            <option value="MIXTA">MIXTA</option>
                            <option value="SIN PAGO">SIN PAGO</option>
                          </select>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>F. INSCRIPCION:</label>
                           <input class="form-control input-group-sm" type="date"  name="finc" id="finc" required>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>FONDO:</label>
                            <select class="form-control"  name="xfon" required>
                              <option value="">--</option>
                              <option value="A.F.P. INTEGRA">A.F.P. INTEGRA</option>
                              <option value="A.F.P. PRIMA">A.F.P. PRIMA</option>
                              <option value="A.F.P. PROFUTURO">A.F.P. PROFUTURO</option>
                              <option value="A.F.P. HABITAT">A.F.P. HABITAT</option>
                            </select>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-2">
                           <label>Nº CUSPP:</label>
                           <input class="form-control text-uppercase" placeholder="00000000" name="ncuspp" pattern="[A-z0-9 ].{1,}" required>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-3">
                           <label>Nº CUENTA PERSONAL:</label>
                           <input class="form-control" placeholder="00000000" name="nper" pattern="[0-9+].{6,}" required>
                         </div>
                         <div class="form-group col-xs-12 col-md-12 col-lg-3">
                           <label>Nº CUENTA CTS:</label>
                           <input class="form-control" placeholder="00000000" name="ncts" pattern="[0-9+].{6,}" required>
                         </div>-->
                         <div class="col-lg-3 col-md-3 hidden-xs top">
                            <button type="submit" class="btn btn-primary" name="Guardarp" value="Insertar">Guardar </button>
                            <button type="reset" class="btn btn-default">Limpiar</button>
                            <a href="#" class="btn btn-warning" id="hide"><i class="glyphicon glyphicon-chevron-left"></i></a>
                          </div>
                      </form>
                    </div>
                 </div> <!--element-->
              </div> <!--content-->
      </div><!-- /.pbody -->
    </div><!-- /.pdefault -->
  </div><!-- /.col -->
<!--</div> /.row -->
<!-- /.row uploadImage1-->
<script>
function previewImage(nb) {
    var reader = new FileReader();
    reader.readAsDataURL(document.getElementById('uploadImage'+nb).files[0]);
    reader.onload = function (e) {
        document.getElementById('uploadPreview'+nb).src = e.target.result;
    };
}
</script>
