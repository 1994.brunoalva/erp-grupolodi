<?php
if ($_REQUEST['editc'] !='') {
  $idcuenta = $_REQUEST['editc'];
  $sqlcuenta = "SELECT * FROM sys_empresas_bancos WHERE emb_id='$idcuenta'";
  $rcuenta = mysqli_query($con,$sqlcuenta);
  $acuenta = mysqli_fetch_array($rcuenta,MYSQLI_ASSOC);
  $xcodban = $acuenta['emp_codban'];
  $xcuent = $acuenta['emp_cuenta'];
}
  ?>
<style>
     .M_Cuentalto{
       height: 320px;
     }
 </style>
 <div class="modal fade" id="E_Cuenta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-list fa-fw"></i>&nbsp;<strong>Editar Cuenta Bancaria</strong></h3>
           </div>
           <div class="modal-body M_Cuentalto">
             <form  id="RegistroC" role="form" action="index.php?menu=10" method="post">
              <input type="hidden" name="agg" value="<?=$agreg;?>">
              <input type="hidden" name="idcuenta" value="<?=$idcuenta?>">
              <input type="hidden" name="form" value="banco">
              <div class="form-group col-md-8 col-xs-12 col-lg-4">
                <label>RUC / DNI:</label>
                <input class="form-control text-uppercase" value="<?php echo $xiden; ?>"  disabled>
              </div>
             <div class="form-group col-md-8 col-xs-12 col-lg-8">
               <label>NOMBRE / RAZON SOCIAL:</label>
               <input class="form-control text-uppercase" value="<?php echo  $xnom;?>" disabled>
             </div>
             <div class="form-group col-xs-12 col-md-12 col-lg-12" >
               <label>BANCO:</label>
               <select class="form-control"  name="banid"  required>
                 <?php
                    $sql3="SELECT ban_id,ban_nombre from sys_bancos order by ban_id";
                    $rsql3=mysqli_query($con,$sql3);
                    echo "<option value=''>--</option>";
                    if( $row3=mysqli_fetch_array($rsql3,MYSQLI_ASSOC)     ){
                    do{
                       echo '<option value="'.$row3['ban_id'].'">'.$row3['ban_nombre'].'</option>';
                     } while($row3=mysqli_fetch_array($rsql3,MYSQLI_ASSOC));
                    }
                  ?>
                </select>
             </div>
             <div class="form-group col-xs-12 col-md-12 col-lg-6" required>
                   <label>COD. CLIENTE:</label>
                   <input class="form-control text-uppercase" name="xcodban" value="<?=$xcodban;?>"  pattern="[A-Za-z0-9+].{3,}" title="Ingrese numeros o letras"  required>
             </div>
             <div class="form-group col-xs-12 col-md-12 col-lg-6">
               <label>MONEDA:</label>
                 <select class="form-control" name="xmon" id="xmon" required>
                   <option value="" selected ="selected">--</option>
                     <option value="S">SOLES</option>
                     <option value="USD">DOLARES</option>
                 </select>
             </div>
             <div class="form-group col-xs-12 col-md-12 col-lg-12" required>
               <label>Nº CUENTA:</label>
               <input class="form-control" name="xcuent" value="<?=$xcuent;?>" pattern="[0-9+].{6,}" title="Ingrese solo numeros" required>
             </div>
           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitCuenta" value="Edit"><i class="glyphicon glyphicon-floppy-saved"></i> Actualizar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>
<?php if ($_GET['editc'] != ''): ?>
  <script type="text/javascript">
    $(document).ready(function()
    {
       $("#E_Cuenta").modal("show");
    });
  </script>
<?php endif; ?>
