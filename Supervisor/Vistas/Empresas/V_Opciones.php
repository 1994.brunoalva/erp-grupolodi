<?php
  include "../Funciones/BD.php";
  #incluir archivos de funciones
  if ($_REQUEST['form'] !='') {
    include "Funciones/Empresas/Opciones.php";
    if ($_REQUEST['form'] =='banco') {
      #incluir ventanas modales
      include "M_Cuenta.php";
      include "E_Cuenta.php";
    }
    if ($_REQUEST['form']=='repre') {
      include "M_Repre.php";
      include "E_Repre.php";
    }
  }
 ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-lg-12">
          <div class="row" id="titulo">
            <div class="col-lg-6">
              <h2 class="azul"><i class="fa fa-building-o fa-fw"></i><strong><?=$xnom;?></strong></h2>
            </div>
            <div class="col-lg-6 text-right">
              <?php if ($_REQUEST['form']=='banco'): ?>
                <a  class="btn btn-primary" href="#M_Cuenta" data-toggle="modal"><i class="fa fa-plus"></i> Agregar</a>
              <?php endif; ?>
              <?php if ($_REQUEST['form']=='repre'): ?>
                <a  class="btn btn-primary" href="#M_Repre" data-toggle="modal"><i class="fa fa-plus"></i> Agregar</a>
              <?php endif; ?>
              <a href="index.php?menu=7" class="btn btn-warning"><i class="glyphicon glyphicon-chevron-left"></i></a>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12"><hr class="black" /></div>
          <?php if ($_REQUEST['form']=='banco'): ?>
            <div class="col-lg-12">
              <h4 align="center" class="azul"><i class="fa fa-building-o fa-fw"></i><strong>Cuentas Bancarias Agregadas</strong></h4>
                <hr class="black" />
              <?=$rmsg;?>
            </div>
            <div class="col-lg-12 table-responsive">
              <table class="table table-hover" id="dataTables-example">
                  <thead>
                    <tr>
                      <th class="text-center warning">COD. CLIENTE</th>
                      <th class="text-center warning">BANCO</th>
                      <th class="text-center warning">MONEDA</th>
                      <th class="text-center info">Nº CUENTA</th>
                      <th class="text-center">EDITAR</th>
                    </tr>
                  </thead>
                  <tbody class="text-center">
                    <?php
                      $nfil =1;
                      while($row=mysqli_fetch_array($resultb2,MYSQLI_ASSOC)){ ?>
                    <tr>
                        <td class="warning"><?= $row['emp_codban']; ?></td>
                        <td class="warning"><?= $row['ban_nombre']; ?></td>
                        <td class="warning"><?= '<strong>'.$row['emp_moneda'].'<strong>';?></td>
                        <td class="info"><?= $row['emp_cuenta']; ?></td>
                        <td>
                          <?php
                          echo '<form  role="form" action="index.php?" method="GET">
                          <input type="hidden" name="menu" value="10">
                          <input type="hidden" name="form" value="banco">
                          <input type="hidden" name="agg" value="'.$agreg.'">
                           <button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="editc" value='.$row['emb_id'].'></button>
                          </form>';
                         ?>
                        </td>
                    </tr>
                 <?php  } ?>
                  </tbody>
                </table>
          </div>
          <?php endif; ?>
          <?php if ($_REQUEST['form']=='repre'): ?>
            <div class="col-lg-12">
              <h4 align="center" class="azul"><i class="fa fa-group fa-fw"></i><strong>Representates Legales</strong></h4>
                <hr class="black" />
                <?=$rmsg;?>
            </div>
            <div class="col-lg-12 table-responsive">
              <table class="table table-hover" id="dataTables-example">
                  <thead>
                    <tr>
                      <th class="text-center warning">DOCUMENTO</th>
                      <th class="text-center warning">NUMERO</th>
                      <th class="text-center info">APELLIDOS Y NOMBRES</th>
                      <th class="text-center info">CARGO</th>
                      <th class="text-center">EDITAR</th>
                    </tr>
                  </thead>
                  <tbody class="text-center">
                    <?php
                      $nfil =1;
                      while($row=mysqli_fetch_array($resultb2,MYSQLI_ASSOC)){ ?>
                    <tr>
                        <td class="warning"><?=$row['doc_nombre'];?></td>
                        <td class="warning"><?=$row['rep_ndoc'];?></td>
                        <td class="info"><?=$row['rep_nomape'];?></td>
                        <td class="info"><?=$row['rep_cargo'];?></td>
                        <td><?php
                        echo '<form  role="form" action="index.php?" method="GET">
                        <input type="hidden" name="menu" value="10">
                        <input type="hidden" name="form" value="repre">
                        <input type="hidden" name="agg" value="'.$agreg.'">
                         <button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="editr" value='.$row['rep_id'].'></button>
                        </form>';
                       ?></td>
                    </tr>
                      <?php  } ?>
                  </tbody>
                </table>
          </div>
          <?php endif; ?>

      </div><!-- /.pbody -->
    </div><!-- /.pdefault -->
  </div><!-- /.col-lg-12 -->
</div><!-- /.row -->
