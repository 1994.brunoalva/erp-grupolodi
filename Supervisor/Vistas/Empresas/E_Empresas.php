<?php if ($_REQUEST['edit'] != ''):
  $edit=$_REQUEST['edit'];
  $sql="select * from sys_empresas WHERE (emp_id>0 and length(emp_id)>0) AND emp_id='$edit'";
  $result=mysqli_query($con,$sql);
  $rsql1=mysqli_fetch_array($result,MYSQLI_ASSOC);
  $xnom=$rsql1['emp_nombre'];
  $xiden=$rsql1['emp_ruc'];
  $id=$rsql1['emp_id'];
  $logo=$rsql1['emp_logo'];
  $xdire=$rsql1['emp_direccion'];
  $xtel=$rsql1['emp_telefono'];
  if ($logo != '') { $txtruta = '../Imagenes/Logos/'.$logo;}
  else { $txtruta = "../Imagenes/Logos/noimage.png";}

  endif; ?>
<style>
     .M_Reprealto{
       height: 350px;
     }
     .Mancho{
        width: 800px;
     }
 </style>
 <div class="modal fade" id="M_Empresa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Mancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-building-o fa-fw"></i>&nbsp;<strong>Editar Empresa</strong></h3>
           </div>
           <div class="modal-body M_Reprealto">
             <form  id="RegistroC" role="form" action="index.php?menu=8" enctype="multipart/form-data" method="post">
              <input name="action" type="hidden" value="upload" />
              <input type="hidden" name="logo" value="<?php echo $logo; ?>">
              <input type="hidden" name="idem" value="<?=$id;?>">
              <div class="form-group col-md-8 col-xs-12 col-lg-3">
                <label><strong>RUC / DNI:</strong></label>
                <input class="form-control text-uppercase" name="xiden" pattern="[0-9+].{6,}" title="Ingrese numeros" value="<?php echo $xiden; ?>" required>
              </div>
               <div class="form-group col-md-8 col-xs-12 col-lg-6">
                 <label><strong>NOMBRE / RAZON SOCIAL:</strong></label>
                 <input class="form-control text-uppercase" name="xnom" pattern="[A-z ]+.{5}" title="Ingrese solo letras" value="<?php echo $xnom; ?>" required>
               </div>
               <div class="form-group col-md-8 col-xs-12 col-lg-3">
                 <label><strong>TELEFONO:</strong></label>
                 <input class="form-control text-uppercase" name="xtel" value="<?php echo $xtel; ?>" pattern="[0-9._%+-]{1,25}$" placeholder="+51000000000" value="<?php echo $xtel;?>" required>
               </div>
               <div class="form-group col-md-8 col-xs-12 col-lg-12">
                 <label><strong>DIRECCION:</strong></label>
                 <input class="form-control text-uppercase" name="xdire" placeholder="Ingrese Direccion" value="<?php echo $xdire; ?>" pattern="[A-z-ñÑ0-9.-/ ]+.{3}" title="Ingrese solo letras" required>
               </div>
              <div class="form-group col-md-12 col-xs-12 col-lg-12">
               <img id="uploadPreview6" class="bordeazul" width="706" height="120" src="<?=$txtruta;?>" />
               <input height="20" class="form-control btn btn-primary" title="AGREGAR LOGO" accept="image/png, image/jpeg, image/gif" id="uploadImage6" type="file" name="uploadImage6" onchange="previewImage(6);" />
              </div>
           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitEmp" value="Edit"><i class="glyphicon glyphicon-floppy-saved"></i> Actualizar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>
 <script>
 function previewImage(nb) {
     var reader = new FileReader();
     reader.readAsDataURL(document.getElementById('uploadImage'+nb).files[0]);
     reader.onload = function (e) {
         document.getElementById('uploadPreview'+nb).src = e.target.result;
     };
 }
 </script>
 <?php if ($_GET['edit'] != ''): ?>
   <script type="text/javascript">
     $(document).ready(function()
     {
        $("#M_Empresa").modal("show");
     });
   </script>
 <?php endif; ?>
