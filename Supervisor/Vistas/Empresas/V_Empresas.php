<?php
  include "../Funciones/BD.php";
  if ($_REQUEST['edit']) {
    include "E_Empresas.php";
  }
  $sql="select * from sys_empresas WHERE (emp_id>0 and length(emp_id)>0) ORDER BY emp_id";
  $result=mysqli_query($con,$sql);
 ?>
 <div class="row">
   <div class="col-lg-12 col-md-12 col-xs-12">
     <div class="panel panel-default">
             <div class="panel-body">
              <div class="col-lg-12">
                  <div class="row" id="titulo">
                     <div class="col-lg-6">
                        <h2 class="azul"><i class="fa fa-building-o fa-fw"></i><strong>Empresas</strong></h2>
                     </div>
                     <div class="col-lg-6 text-right" >
                        <button class="btn btn-primary" id="show"><i class="fa fa-plus "></i> Agregar</button>
                     </div>
                  </div>
                </div>
             <div class="row">
                <div class="col-lg-12">
                   <div id="titulo2" class="col-lg-6" style="display: none;" >
                     <h2 class="azul"><i class="fa fa-building-o fa-fw"></i><strong>Nueva Empresa</strong></h2>
                  </div>
                </div>
              </div>
              <div class="col-lg-12 table-responsive"><hr class="black" />
              <div id="element2">
               <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">RUC / DNI</th>
                                <th class="text-center">DESCRIPCION</th>
                                <th class="text-center">TELEFONO</th>
                                <th class="text-center">BANCOS</th>
                                <th class="text-center">REPRESENTATES</th>
                                <!--<th class="text-center">HABILITAR</th>
                                <th class="text-center">DESHABILITAR</th>-->
                                <th class="text-center">EDITAR</th>
                              </tr>
                            </thead>
                            <tbody class="text-center">
                            <?php
                              $nfil =1;
                              while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ ?>
                              <tr>
                                  <td><?= $nfil++;?></td>
                                  <td><?= $row['emp_ruc']; ?></td>
                                  <td><?= $row['emp_nombre']; ?></td>
                                  <td><?= $row['emp_telefono']; ?></td>
                                  <!--Agg-->
                                    <td class="centeralign">
                                        <form  id="EditarE" role="form" action="index.php?menu=10" method="post">
                                          <input type="hidden" name="form" value="banco">
                                          <button class="btn btn-primary btn-sm glyphicon glyphicon-plus" name="agg" value='<?php echo $row['emp_id']; ?>'></button>
                                        </form>
                                    </td>
                                    <!--REPRESENTATES-->
                                    <td><form  id="EditarE" role="form" action="index.php?menu=10" method="post">
                                      <input type="hidden" name="form" value="repre">
                                      <button class="btn btn-info btn-sm glyphicon glyphicon-user" name="agg" value='<?php echo $row['emp_id']; ?>'></button>
                                    </form>
                                    </td>
                                <!--EDITAR-->
                                  <td class="centeralign">
                                      <form  id="EditarE" role="form" action="index.php?" method="get">
                                        <input type="hidden" name="menu" value="7">
                                        <button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="edit" value='<?php echo $row['emp_id']; ?>'></button>
                                      </form>
                                  </td>

                              </tr>
                               <?php } ?>
                            </tbody>
                          </table>
              </div></div>
              <!--Div Oculto-->
              <div id="content">
                 <div id="element" style="display: none;">
                          <form role="form" action="index.php?menu=8"  enctype="multipart/form-data"  method="post" >
                            <input name="action" type="hidden" value="upload" />
                          <div class="upload_image col-lg-6 col-md-12">
                            <table  class="col-lg-12 col-md-12 col-xs-12">
                            <tr>
                            <td  class="bordeazul hidden-xs" align="center"><img id="uploadPreview1" width="500" height="180"  src="../Imagenes/Logos/noimage.png" /></td>
                            <td  class="bordeazul hidden-md hidden-lg visible-xs" align="center"><img id="uploadPreview1" class="img-responsive"  src="../Imagenes/Logos/noimage.png" /></td>
                            </tr>
                            <tr>
                              <td height="20" align="center"><input class="form-control btn btn-primary" title="AGREGAR LOGO" accept="image/png, image/jpeg, image/gif" id="uploadImage1" type="file" name="uploadImage1" onchange="previewImage(1);" /></td>
                            </tr>
                          </table></div>
                           <div class="form-group col-md-4 col-xs-12 col-lg-2">
                             <label>RUC / DNI:</label>
                             <input class="form-control text-uppercase" name="xiden" placeholder="000000000" pattern="[0-9+].{6,}" title="Ingrese numeros" required>
                           </div>
                            <div class="form-group col-md-8 col-xs-12 col-lg-4">
                              <label>NOMBRE / RAZON SOCIAL:</label>
                              <input class="form-control text-uppercase" name="xnom" placeholder="Ingrese Razon Social" pattern="[A-z ]+.{5}" title="Ingrese solo letras" required>
                            </div>
                            <div class="form-group col-md-8 col-xs-12 col-lg-6">
                              <label>DIRECCION:</label>
                              <input class="form-control text-uppercase" name="xdire" placeholder="Ingrese Direccion" pattern="^([a-zA-Z ])[a-zA-Z0-9-_–\. ]+{1,60}$" title="Ingrese solo letras" required>
                            </div>
                            <div class="form-group col-md-4 col-xs-12 col-lg-2">
                              <label>TELEFONO:</label>
                              <input class="form-control text-uppercase" name="xtel" pattern="[0-9._%+-]{1,25}$" placeholder="+51000000000"  required>
                            </div>
                            <div class="col-lg-3 col-md-4 hidden-xs top">
                             <button type="submit" class="btn btn-primary" name="Guardar" value="Insertar">Guardar </button>
                             <button type="reset" class="btn btn-default">Limpiar</button>
                             <a href="#" class="btn btn-warning" id="hide"><i class="glyphicon glyphicon-chevron-left"></i></a>
                           </div>
                           <div class="col-xs-12 visible-xs top">
                             <button type="submit" class="btn btn-primary" name="Guardar" value="Insertar">Guardar </button>
                             <button type="reset" class="btn btn-default">Limpiar</button>
                             <a href="#" class="btn btn-warning" id="hide"><i class="glyphicon glyphicon-chevron-left"></i></a>
                          </div>
                         </form>

                 </div>
               </div>
             </div>
       </div>
    </div>
         <!-- /.panel -->
 </div>
     <!-- /.col-lg-12 -->
 <script>
 function previewImage(nb) {
     var reader = new FileReader();
     reader.readAsDataURL(document.getElementById('uploadImage'+nb).files[0]);
     reader.onload = function (e) {
         document.getElementById('uploadPreview'+nb).src = e.target.result;
     };
 }
 </script>
