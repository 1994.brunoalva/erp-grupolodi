<?php
  $mar_id = $_REQUEST['idm'];
  include "../Funciones/BD.php";
  $sqlmarca = "SELECT * FROM sys_alm_marca WHERE mar_id = '$mar_id'";
  $rmarca = mysqli_query($con,$sqlmarca);
  $vmarca = mysqli_fetch_array($rmarca,MYSQLI_ASSOC);
  $nom_marca = $vmarca['mar_nombre'];
  include "M_Modelo.php";
  include "E_Modelo.php";

 ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body">
              <div class="col-lg-12">
                <div class="row" id="titulo">
                      <div class="col-lg-6">
                      <h2 class="azul"><i class="fa fa-list fa-fw"></i><strong>MARCA: <?=$nom_marca;?></strong></h2>
                    </div>
                      <div class="col-lg-6 text-right" >
                          <a  class="btn btn-primary" href="#M_Modelo" data-toggle="modal"><i class="fa fa-plus"></i> Agregar</a>
                        	<a href="index.php?menu=14" class="btn btn-warning"><i class="glyphicon glyphicon-chevron-left"></i></a>
                      </div>
                </div>
              </div>
              <div class="table-responsive col-lg-12"><hr class="black" />
                 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                   <thead>
                     <tr>
                       <tr>
                         <th class="text-center">CATEGORIA</th>
                         <th class="text-center">MODELO</th>
                         <th class="text-center">EDITAR</th>
                       </tr>
                     </tr>
                   </thead>
                   <tbody class="text-center">
                     <?php
                     $sqlmod="SELECT sm.mar_id,sm.mar_nombre,sc.cat_descripcion,sc.cat_id,so.mod_nombre,so.mod_id FROM sys_alm_marca sm, sys_alm_categoria sc, sys_alm_modelo so
                     WHERE  sc.cat_id = sm.cat_id AND sm.mar_id ='$mar_id' AND sm.mar_id = so.mar_id";
                     $rmod=mysqli_query($con,$sqlmod);
                     while($row=mysqli_fetch_array($rmod,MYSQLI_ASSOC)){ ?>
                     <tr>
                         <td><?=$row['cat_descripcion'];?></td>
                         <td><?=$row['mod_nombre'];?></td>
                         <td><form   role="form" action="index.php?" method="get">
                           <input type="hidden" name="menu" value="16">
                           <input type="hidden" name="idm" value="<?=$row['mar_id'];?>">
                              <button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="editm" value="<?=$row['mod_id'];?>">
                          </button>
                         </form></td>
                     </tr>
                   <?php } ?>
                   </tbody>
                 </table>
                 </div>
      </div><!-- /.panel-body -->
    </div><!-- /.panel -->
  </div><!-- /.col-lg-12 -->
</div><!-- /.row-->
