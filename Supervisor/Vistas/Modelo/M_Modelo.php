<style>
     .M_Marcalto{
       height: 370px;
     }
 </style>

 <div class="modal fade" id="M_Modelo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-list fa-fw"></i>&nbsp;<strong>Nuevo Modelo</strong></h3>
           </div>
           <div class="modal-body M_Marcalto">
             <form  id="RegistroC" role="form" action="index.php?menu=17" enctype="multipart/form-data" method="post">
               <input name="action" type="hidden" value="upload" />
               <input type="hidden" name="mar_id" value="<?=$mar_id; ?>">
               <div class="form-group col-xs-12 col-md-12 col-lg-6">
                 <label><strong>MARCA:</strong></label>
                 <select class="form-control" name="xcat" disabled>
                   <option value="<?=$mar_id;?>"><?=$nom_marca;?></option>
                 </select>
               </div>
               <div class="form-group col-md-12 col-xs-12 col-lg-6">
                 <label><strong>DESCRIPCION:</strong></label>
                 <input class="form-control text-uppercase" name="xmodeloNom" title=" Ingrese Letras y Numeros. Caracteres Admitidos(/*.-)" autofocus>
               </div>
               <div class="form-group col-md-12 col-xs-12 col-lg-12">
                 <img id="uploadPreview3" class="bordeazul" width="506" height="200" src="../Imagenes/Logos/noimage.png" />
                 <input height="20" class="form-control btn btn-primary" title="AGREGAR LOGO" accept="image/png, image/jpeg, image/gif" id="uploadImage3" type="file" name="uploadImage3" onchange="previewImage(3);" />
                </div>

           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitModelo" value="Save"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>
 <script>
 function previewImage(nb) {
     var reader = new FileReader();
     reader.readAsDataURL(document.getElementById('uploadImage'+nb).files[0]);
     reader.onload = function (e) {
         document.getElementById('uploadPreview'+nb).src = e.target.result;
     };
 }
 </script>
