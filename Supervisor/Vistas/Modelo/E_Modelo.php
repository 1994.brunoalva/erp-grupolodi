<?php
if ($_REQUEST['editm'] !='') {
  $idmodel = $_REQUEST['editm'];
  $sqlmodel = "SELECT sa.mar_id,sa.mar_nombre,sm.mod_nombre,sm.mod_logo FROM sys_alm_modelo sm, sys_alm_marca  sa WHERE sa.mar_id = sm.mar_id AND sm.mar_id='$mar_id'
  AND sm.mod_id='$idmodel'";
  $rmodel = mysqli_query($con,$sqlmodel);
  $amodel = mysqli_fetch_array($rmodel,MYSQLI_ASSOC);
  $xmodeloNom = $amodel['mod_nombre'];
  $fotomodel = $amodel['mod_logo'];
  $nom_marca = $amodel['mar_nombre'];
  #SI LA MARCA NO TIENE IMG AGREGADA
  if ($fotomodel != '') { $txtruta = '../Imagenes/Modelo/'.$fotomodel;}
  else { $txtruta = "../Imagenes/Logos/noimage.png";}

}
?>
<style>
     .M_Marcalto{
       height: 370px;
     }
 </style>
 <div class="modal fade" id="E_Modelo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-list fa-fw"></i>&nbsp;<strong>Editar Modelo</strong></h3>
           </div>
           <div class="modal-body M_Marcalto">
             <form  id="RegistroC" role="form" action="index.php?menu=17" enctype="multipart/form-data" method="post">
               <input name="action" type="hidden" value="upload" />
               <input type="hidden" name="mar_id" value="<?=$mar_id;?>">
               <input type="hidden" name="antfoto" value="<?=$fotomodel;?>">
              <input name="EditM" type="hidden" value="<?=$idmodel;?>">
               <div class="form-group col-xs-12 col-md-12 col-lg-6">
                 <label><strong>MARCA:</strong></label>
                 <select class="form-control" name="xcat" disabled>
                    <option value="<?=$mar_id;?>"><?=$nom_marca;?></option>
                 </select>
               </div>
               <div class="form-group col-md-12 col-xs-12 col-lg-6">
                 <label><strong>DESCRIPCION:</strong></label>
                 <input class="form-control text-uppercase" name="xmodeloNom" value="<?=$xmodeloNom;?>" pattern="^([a-zA-Z ])[a-zA-Z0-9-_–\./* ]+{1,60}$" title=" Ingrese Letras y Numeros. Caracteres Admitidos(/*.-)" autofocus>
               </div>
               <div class="form-group col-md-12 col-xs-12 col-lg-12">
                 <img id="uploadPreview4" class="bordeazul" width="506" height="200" src="<?=$txtruta;?>" />
                 <input height="20" class="form-control btn btn-primary" title="AGREGAR LOGO" accept="image/png, image/jpeg, image/gif" id="uploadImage4" type="file" name="uploadImage4" onchange="previewImage(4);" />
              </div>
           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitModelo" value="Edit"><i class="glyphicon glyphicon-floppy-saved"></i> Actualizar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>
 <script>
 function previewImage(nb) {
     var reader = new FileReader();
     reader.readAsDataURL(document.getElementById('uploadImage'+nb).files[0]);
     reader.onload = function (e) {
         document.getElementById('uploadPreview'+nb).src = e.target.result;
     };
 }
 </script>
 <?php if ($_REQUEST['editm'] !=''): ?>
   <script type="text/javascript">
     $(document).ready(function()
     {
        $("#E_Modelo").modal("show");
     });
   </script>
 <?php endif; ?>
