
 <style>
     .Proveancho2 {
       width: 1150px;
     }
     .Provealto2{
       height: 500px;
     }
 </style>
<div class="modal fade" id="B_Proveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Proveancho2">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-group fa-fw"></i>&nbsp;<strong>Proveedores Registrados</strong></h3>
           </div>

           <div class="modal-body Provealto2">
             <div class="col-lg-12 table-responsive">

              <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                           <thead>
                             <tr>
                               <th class="text-center">ID</th>
                               <th class="text-center">RUC / DNI</th>
                               <th class="text-center">DESCRIPCION</th>
                               <th class="text-center">AGREGAR</th>
                             </tr>
                           </thead>
                           <tbody class="text-center">

                             <tr>
                                 <td><?= $nfil++;?></td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                               <!--EDITAR-->
                                 <td class="centeralign">
                                     <form  id="EditarE" role="form" action="index.php?" method="get">
                                       <input type="hidden" name="menu" value="7">
                                       <button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="edit" value='<?php echo $row['emp_id']; ?>'></button>
                                     </form>
                                 </td>
                             </tr>

                           </tbody>
                         </table>
             </div>
           </div>
           <div class="modal-footer">
          <!--   <button type="submit" class="btn btn-sm btn-primary" name="SubmitMarca" value="Save"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>-->
           </div>
       </div>

   </div>
 </div>
