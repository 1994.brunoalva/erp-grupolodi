<?php if ($_GET['unit'] !=''):
  $idu =$_GET['unit'];
  $sqluni="SELECT sc.cat_descripcion,su.uni_nombre FROM sys_alm_unidad su,sys_alm_categoria sc WHERE sc.cat_id ='$idu' AND sc.cat_id=su.cat_id";
  $runit= mysqli_query($con,$sqluni);
endif; ?>
 <style>
     .Uniancho {
       width: 600px;
     }
     .Unialto{
       height: 200px;
     }
 </style>
<div class="modal fade" id="M_Unidades" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Uniancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-magnet fa-fw"></i>&nbsp;<strong>Unidades de Medida</strong></h3>
           </div>

           <div class="modal-body Unialto">
             <div class="col-lg-12 table-responsive">

              <table class="table table-striped table-bordered table-hover" >
                           <thead>
                             <tr>
                               <th class="text-center">ID</th>
                               <th class="text-center">CATEGORIA</th>
                               <th class="text-center">DESCRIPCION</th>
                             </tr>
                           </thead>
                           <tbody class="text-center">
                             <?php $nfil=1; while($rowu=mysqli_fetch_array($runit,MYSQLI_ASSOC)){ ?>
                             <tr>
                                 <td><?= $nfil++;?></td>
                                 <td><?=$rowu['cat_descripcion'];?></td>
                                 <td><?=$rowu['uni_nombre'];?></td>
                             </tr>
                           <?php } ?>
                           </tbody>
                         </table>
             </div>
           </div>
           <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>
       </div>

   </div>
 </div>
 <?php if ($_GET['unit'] !=''): ?>
   <script type="text/javascript">
     $(document).ready(function()
     {
        $("#M_Unidades").modal("show");
     });
   </script>
 <?php endif; ?>
