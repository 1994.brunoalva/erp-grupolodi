
 <style>
     .Camarancho {
       width: 600px;
     }
     .Camaralto{
       height: 316px;
     }
 </style>
<div class="modal fade" id="M_Camara" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Aroancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-list fa-fw"></i>&nbsp;<strong>Agregar Camara</strong></h3>
           </div>

           <div class="modal-body Camaralto">
             <div class="form-group col-xs-12 col-md-12 col-lg-6">
               <label><strong>SKU:</strong></label>
               <input class="form-control text-uppercase"  disabled>
             </div>
             <div class="form-group col-xs-12 col-md-12 col-lg-6">
                 <label><strong>CODIGO SUNAT:</strong></label>
                 <input class="form-control text-uppercase"  disabled>
             </div>
             <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label>MARCA :</label>
                <select class="form-control" name="xmar5" id="xmar5"  title="Ingrese Marca">
                <?php foreach ($dataMarcaAce as $marcaAce) { ?>
                  <option value="<?= $marcaAce->id ?>" ><?= $marcaAce->nombre ?></option>
                <?php } ?>
                </select>
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-6">
                    <label>TIPO:</label>
                    <select class="form-control" id="aro_xmod" name="aro_xmod" title="Ingrese Tipo" onchange="PasarValor3();" onclick="PasarValor3();">
                        <option value="0" selected>--</option>
                        <option value="RADIAL">RADIAL</option>
                        <option value="CONVENCIONAL">CONVENCIONAL</option>
                    </select>
                </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-6">
                  <label>PAIS:</label>
                  <select class="form-control" id="xpais3" name="xpais3" onchange="ShowPais3();" title="Ingrese Pais"  onclick="PasarValor3();">
                      <!-- Leer los Clasificacion -->
                      <?php
                      $sql8 = "SELECT pais_id,pais_nombre from sys_pais WHERE pais_estatus=1 order by pais_id";
                      $rsql8 = mysqli_query($con, $sql8);
                      echo "<option value='0' selected>--</option>";
                      if ($row8 = mysqli_fetch_array($rsql8, MYSQLI_ASSOC)) {
                          do {
                              echo '<option value="' . $row8['pais_id'] . '">' . $row8['pais_nombre'] . '</option>';
                          } while ($row8 = mysqli_fetch_array($rsql8, MYSQLI_ASSOC));
                      }
                      ?>
                  </select>
              </div>
              <div class="form-group col-xs-12 col-md-6 col-lg-3">
                  <label>MEDIDA:</label>
                  <input type="text" class="form-control text-uppercase" name="aro_xmed" id="aro_xmed" title="Ingrese la Medida" onkeyup="PasarValor3();" >
              </div>
              <div class="form-group col-md-6 col-xs-12 col-lg-3">
                  <label> ARO:</label>
                  <input type="text" class="form-control text-uppercase" name="aro_xespe" id="aro_xespe" title="Ingrese el Espesor">
              </div>
              <div class="form-group col-md-6 col-xs-12 col-lg-3">
                  <label> VALVULA: </label>
                  <input type="text" class="form-control text-uppercase" name="aro_xhueco" id="aro_xhueco" title="Ingrese el N° Huecos" onkeyup="PasarValor3();">
              </div>
              <div class="form-group col-md-6 col-xs-12 col-lg-9">
                  <label>PARTIDA ARANCELARIA:</label>
                  <input type="text" class="form-control text-uppercase" id="aro_xparti" name="aro_xparti" title="Ingrese Partida Arancelaria">
              </div>
           </div><!--pbody-->
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitMarca" value="Save"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>
