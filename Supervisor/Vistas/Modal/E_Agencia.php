<?php
  if ($_GET['edit'] != '') {
    $edit = $_GET['edit'];
    $sqlb = "SELECT * FROM sys_alm_agencia WHERE age_id='$edit'";
    $rsqlb = mysqli_query($con,$sqlb);
    $asqlb = mysqli_fetch_array($rsqlb,MYSQLI_ASSOC);
    $txtnum = $asqlb['age_ndoc'];
    $txtnom = $asqlb['age_nomape'];
    $txttele =$asqlb['age_tele'];
    $txtdirec=$asqlb['age_direc'];
  }

 ?>

<style>
  .Agenciaalto {
    height: 300px;
  }
  .Agenciaancho {
    width: 550px;
  }
</style>
<div class="modal fade" id="E_Agencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog Agenciaancho">
      <div class="modal-content">
          <div class="modal-header modal-header-primary">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
              <h3 align="center" id="myModalLabel"><i class="fa fa-truck fa-fw"></i>&nbsp;<strong>Agregar Agencia de transporte</strong></h3>
          </div>
          <div class="modal-body Agenciaalto">
            <form  id="RegistroC" role="form" action="index.php?menu=6" method="post">
              <input type="hidden" name="edit" value="<?=$edit;?>">
            
              <div class="form-group  col-md-8 col-xs-12 col-lg-6">
                 <label><strong>TIPO DOCUMENTO:</strong></label>
                 <select class="form-control"  name="txtdoc" required>
                   <?php
                     #Tipo de documentos
                    $sql2="SELECT doc_sunat_id ,(CASE WHEN doc_sunat_id = 6 THEN 'RUC'
                            WHEN doc_sunat_id = '0' THEN 'OTROS'
                            WHEN doc_sunat_id = '1' THEN 'DNI'
                            WHEN doc_sunat_id = '7' THEN 'PASAPORTE'
                            WHEN doc_sunat_id = '4' THEN 'CARNET E.'
                            WHEN doc_sunat_id = 'A' THEN 'CEDULA D.'
                            ELSE 'NT'
                            END) AS doc_nombre
               FROM sys_documentos WHERE doc_tipo='DE IDENTIDAD'
                   ORDER BY doc_sunat_id";
                    $rsql2=mysqli_query($con,$sql2);
                    echo "<option value=''>--</option>";
                    if( $row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC)     ){
                    do{
                       echo '<option value="'.$row2['doc_sunat_id'].'">'.$row2['doc_nombre'].'</option>';
                       } while($row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC));
                    }
                    ?>
                 </select>
                 </div>
                 <div class="form-group col-xs-12 col-md-12 col-lg-6">
                   <label><strong>N&Uacute;MERO:</strong></label>
                   <input class="form-control" placeholder="000000000" name="txtnum" value="<?=$txtnum;?>" pattern="[0-9+].{6,}" title="Ingrese numeros" required>
                 </div>
                 <div class="form-group col-xs-12 col-md-12 col-lg-8">
                   <label><strong>RAZ&Oacute;N SOCIAL:</strong></label>
                   <input class="form-control text-uppercase" name="txtnom" value="<?=$txtnom;?>"  pattern="[A-z0-9 ].{1,}" title="Ingrese solo letras" required>
                 </div>
                 <div class="form-group col-xs-12 col-md-12 col-lg-4">
                   <label><strong>TELEFONO:</strong></label>
                   <input class="form-control" placeholder="+00-000000" name="txttele" value="<?=$txttele;?>" pattern="[0-9+].{6,}">
                 </div>
                 <div class="form-group col-xs-12 col-md-12 col-lg-12">
                   <label><strong>DIRECCI&Oacute;N:</strong></label>
                   <textarea class="form-control text-uppercase" rows="3" name="txtdirec" placeholder="Ingrese Direccion" pattern="^([a-zA-Z ])[a-zA-Z0-9-_–\. ]+{1,60}$" required><?=$txtdirec;?></textarea>
                 </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-sm btn-primary" name="SubmitAgencia" value="Edit"><i class="glyphicon glyphicon-floppy-saved"></i> Actualizar </button>
            <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
          </div>   </form>
      </div>
  </div>
</div>
<?php if ($_GET['edit'] !=''): ?>
  <script type="text/javascript">
    $(document).ready(function()
    {
       $("#E_Agencia").modal("show");
    });
  </script>
<?php endif; ?>
