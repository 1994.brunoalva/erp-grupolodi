
 <style>
     .Proteancho {
       width: 600px;
     }
     .Protealto{
       height: 316px;
     }
 </style>
<div class="modal fade" id="M_Protector" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Proteancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-list fa-fw"></i>&nbsp;<strong>Agregar Protector</strong></h3>
           </div>
           <div class="modal-body Protealto">
             <div class="form-group col-xs-12 col-md-12 col-lg-6">
               <label><strong>SKU:</strong></label>
               <input class="form-control text-uppercase"  disabled>
             </div>
             <div class="form-group col-xs-12 col-md-12 col-lg-6">
                 <label><strong>CODIGO SUNAT:</strong></label>
                 <input class="form-control text-uppercase"  disabled>
             </div>
             <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label>MARCA :</label>
                <select class="form-control" name="xmar5" id="xmar5"  title="Ingrese Marca">
                <?php foreach ($dataMarcaAce as $marcaAce) { ?>
                  <option value="<?= $marcaAce->id ?>" ><?= $marcaAce->nombre ?></option>
                <?php } ?>
                </select>
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-6">
                  <label>PAIS:</label>
                  <select class="form-control" id="xpais3" name="xpais3" onchange="ShowPais3();" title="Ingrese Pais"  onclick="PasarValor3();">
                      <!-- Leer los Clasificacion -->
                      <?php
                      $sql8 = "SELECT pais_id,pais_nombre from sys_pais WHERE pais_estatus=1 order by pais_id";
                      $rsql8 = mysqli_query($con, $sql8);
                      echo "<option value='0' selected>--</option>";
                      if ($row8 = mysqli_fetch_array($rsql8, MYSQLI_ASSOC)) {
                          do {
                              echo '<option value="' . $row8['pais_id'] . '">' . $row8['pais_nombre'] . '</option>';
                          } while ($row8 = mysqli_fetch_array($rsql8, MYSQLI_ASSOC));
                      }
                      ?>
                  </select>
              </div>
              <div class="form-group col-md-6 col-xs-12 col-lg-12">
                  <label>DESCRIPCION:</label>
                  <input type="text" class="form-control text-uppercase" id="aro_xparti" name="aro_xparti" title="Ingrese Partida Arancelaria">
              </div>
              <div class="form-group col-md-6 col-xs-12 col-lg-12">
                  <label>PARTIDA ARANCELARIA:</label>
                  <input type="text" class="form-control text-uppercase" id="aro_xparti" name="aro_xparti" title="Ingrese Partida Arancelaria">
              </div>
           </div><!--pbody-->
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitMarca" value="Save"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>
