
 <style>
     .Neuancho {
       width: 1100px;
     }
     .Neualto{
       height: 530px;
     }
     .Estilo {
       font-size: 10.5px;
     }
     .Estilo1 {
       font-size: 11px;
     }
     .select2 {
       font-size: 12px;
     }
 </style>
<div class="modal fade" id="M_Neumatico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Neuancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-list fa-fw"></i>&nbsp;<strong>Agregar Neumatico</strong></h3>
           </div>

           <div class="modal-body Neualto">
             <div class="form-group col-xs-12 col-md-12 col-lg-3">
               <label><span class="Estilo1">SKU:</span></label>
               <input class="form-control text-uppercase"  disabled>
             </div>
             <div class="form-group col-xs-12 col-md-12 col-lg-3">
                 <label><span class="Estilo1">CODIGO SUNAT:</span></label>
                 <input class="form-control text-uppercase"  disabled>
             </div>
             <div id="div_nomen" class="form-group  col-md-6 col-xs-12 col-lg-3">
              <label><span class="Estilo1">NOMENCLATURA:</span></label>
              <select class="form-control" id="xnomen" name="xnomen" title="Ingrese la Nomenclatura" onchange="OcultarSunat(2);" onclick="OcultarSunat(2);">
                  <option value="0" selected>--</option>
                  <option value="NUMERICA">NUMERICA</option>
                  <option value="MILIMETRICA">MILIMETRICA</option>
              </select>
            </div>
            <div id="div_marca" class=" miClase form-group  col-md-6 col-xs-12 col-lg-3">
              <label><span class="Estilo1">MARCA :</span></label>
              <select class="form-control" name="xmar1" id="xmar1" title="Ingrese la Marca">
                <?php foreach ($dataMarcaNeu as $marcaNeu) { ?>
                        <option value="<?= $marcaNeu->id ?>" ><?= $marcaNeu->nombre ?></option>
                <?php } ?>
                <option value="-1">AÑADIR</option>
              </select>
            </div>
            <div id="div_modelo" class="form-group  col-md-6 col-xs-12 col-lg-3">
                <label><span class="Estilo1">MODELO:</span></label>
                <select class="form-control" name="xmod" id="xmod" title="Ingrese el Modelo"></select>
            </div>
            <div id="div_pais_neu" class="form-group  col-md-6 col-xs-12 col-lg-3">
                <label><span class="Estilo1">PAIS:</span></label>
                <select class="form-control" id="xpais1" name="xpais1" onchange="ShowPais1();" title="Ingrese el Pais">
                    <?php
                    $sql4 = "SELECT pais_id,pais_nombre from sys_pais WHERE pais_estatus=1 order by pais_id";
                    $rsql4 = mysqli_query($con, $sql4);
                    echo "<option value='0' selected>--</option>";
                    if ($row4 = mysqli_fetch_array($rsql4, MYSQLI_ASSOC)) {
                        do {
                            echo '<option value="' . $row4['pais_id'] . '">' . $row4['pais_nombre'] . '</option>';
                        } while ($row4 = mysqli_fetch_array($rsql4, MYSQLI_ASSOC));
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-xs-12 col-md-6 col-lg-2">
                <label><span class="Estilo">ANCHO/SECCION(MM) :</span></label>
                <input type="text" class="form-control text-uppercase" id="neu_xanc" name="neu_xanc" title="Ingrese el Ancho/Seccion" onkeyup="PasarValor();">
            </div>
            <div class="form-group col-xs-12 col-md-6 col-lg-2">
                <label><span class="Estilo1">SERIE :</span></label>
                <input type="text" class="form-control text-uppercase" id="neu_xserie" name="neu_xserie" title="Ingrese el Serie">
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-2">
                <label><span class="Estilo1"> ARO :</span></label>
                <input type="text" class="form-control text-uppercase" id="neu_xaro" name="neu_xaro" title="Ingrese solo el Aro" onkeyup="PasarValor();">
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-2">
                <label><span class="Estilo1">PLIEGUES:</span></label>
                <input type="text" class="form-control text-uppercase" id="neu_xpli" name="neu_xpli" title="Ingrese solo el Pliegue">
            </div>
            <div id="div_tipo_neu" class="form-group  col-md-6 col-xs-12 col-lg-2">
                <label><span class="Estilo1">TIPO :</span></label>
                <select class="form-control select2" id="xtp" name="xtp" title="Ingrese Tipo">
                    <option value="" selected>--</option>
                    <option value="RADIAL">RADIAL</option>
                    <option value="CONVENCIONAL">CONVENCIONAL</option>
                </select>
            </div>
            <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                <label><span class="Estilo1">SET:</span></label>
                <select class="form-control select2" id="neu_set" name="neu_set" title="Ingrese el Set">
                    <option value="0" selected>--</option>
                    <option value="LL">LL</option>
                    <option value="LL/C/P">LL/C/P</option>
                    <option value="LL/C">LL/C</option>
                    <option value="LL/P">LL/P</option>
                </select>
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-3">
              <label><span class="Estilo1">ANCHO/SECCION ADUANAS:</span></label>
              <input type="text" class="form-control text-uppercase" id="neu_xanc_adua" name="neu_xanc_adua" title="Ingrese Ancho/Seccion Aduanas">
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-3">
                <label><span class="Estilo1">SERIE/ADUANAS:</span></label>
                <input type="text" class="form-control text-uppercase" id="neu_xserie_adua" name="neu_xserie_adua" title="Ingrese la Serie/Aduanas">
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-2">
                <label><span class="Estilo1">DIAMETRO/EXTERNO:</span></label>
                <input type="text" class="form-control text-uppercase" id="neu_xexte" name="neu_xexte" title="neu_xexte" title="Ingrese la Diametro/Externo">
            </div>
            <div class="form-group  col-md-6 col-xs-12 col-lg-4">
                <label><span class="Estilo1">TIPO DE CONSTRUCCIÓN No.04:</span></label>
                <select class="form-control Estilo1" id="neu_xcons" name="neu_xcons" title="Ingrese Tipo de Construccion">
                    <option value="0" selected>--</option>
                    <option value="CTT-CONVENCIONAL CON CAMARA">CTT-CONVENCIONAL CON CAMARA</option>
                    <option value="CTL-CONVENCIONAL SIN CAMARA">CTL-CONVENCIONAL SIN CAMARA </option>
                    <option value="RTT-RADIAL CON CAMARA">RTT-RADIAL CON CAMARA</option>
                    <option value="RTL-RADIAL SIN CAMARA">RTL-RADIAL SIN CAMARA</option>
                </select>
            </div>
            <div class="form-group  col-md-6 col-xs-12 col-lg-6">
                <label><span class="Estilo1">USO COMERCIAL No. 01:</span></label>
                <select class="form-control Estilo1" id="neu_xuso" name="neu_xuso" title="Ingrese Uso Comercial" onclick="PasarValor();" onchange="PasarValor();">
                    <option value="" selected>--</option>
                    <option value="PPE-PASAJEROS, USO PERMANENTE">PPE-PASAJEROS, USO PERMANENTE</option>
                    <option value="PTE-PASAJEROS, USO TEMPORAL">PTE-PASAJEROS, USO TEMPORAL</option>
                    <option value="CLT-COMERCIALES, PARA CAMIONETA DE CARGA, MICROBUSES O CAMIONES LIGEROS">CLT-COMERCIALES, PARA CAMIONETA DE CARGA, MICROBUSES O CAMIONES LIGEROS</option>
                    <option value="CTR-COMERCIALES, PARA CAMION Y/O OMNIBUS">CTR-COMERCIALES, PARA CAMION Y/O OMNIBUS</option>
                    <option value="CML-COMERCIALES, PARA USO MINERO Y FORESTAL">CML-COMERCIALES, PARA USO MINERO Y FORESTAL</option>
                    <option value="CMH-COMERCIALES, PARA CASAS RODANTES">CMH-COMERCIALES, PARA CASAS RODANTES</option>
                    <option value="CST-COMERCIALES, REMOLCADORES EN CARRETERA">CST-COMERCIALES, REMOLCADORES EN CARRETERA</option>
                    <option value="ALN-AGRICOLAS">ALN-AGRICOLAS</option>
                    <option value="OTG-MAQUINARIA, TRACTORES NIVELADORAS">OTG-MAQUINARIA, TRACTORES NIVELADORAS</option>
                    <option value="OTR-OTROS PARA MAQUINARIAS">OTR-OTROS PARA MAQUINARIAS</option>
                </select>
            </div>
            <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                <label><span class="Estilo1">MATERIAL/CARCASA No. 02:</span></label>
                <select class="form-control Estilo1" id="neu_xmate" name="neu_xmate" title="Ingrese Material/Carcasa">
                    <option value="0" selected>--</option>
                    <option value="NYLON">NYLON</option>
                    <option value="ACERO">ACERO </option>
                    <option value="POLIESTER">POLIESTER</option>
                    <option value="RAYON">RAYON</option>
                    <option value="POLIAMIDA">POLIAMIDA</option>
                </select>
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-3">
                <label><span class="Estilo1">INDICE DE CARGA(KG) No.05 :</span></label>
                <input type="text" class="form-control text-uppercase" id="neu_xcarga" name="neu_xcarga" title="Ingrese Indice de Carga">
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-3">
                <label><span class="Estilo1">PROFUNDIDAD DE PISADA :</span></label>
                <input type="text" class="form-control text-uppercase" id="neu_xpisada" name="neu_xpisada" title="Ingrese Profundidad de Pisada">
            </div>
            <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                <label><span class="Estilo1">CODIGO/VELOCIDAD/No.06 :</span></label>
                <select class="form-control select2" id="neu_xvel" name="neu_xvel" title="Ingrese Codigo/Velocidad">
                    <option value="0" selected>--</option>
                    <option value="E-70KM/H">E-70KM/H</option>
                    <option value="F-80KM/H">F-80KM/H </option>
                    <option value="G-90KM/H">G-90KM/H</option>
                    <option value="J-100KM/H">J-100KM/H</option>
                    <option value="K-110KM/H">K-110KM/H</option>
                    <option value="L-120KM/H">L-120KM/H</option>
                    <option value="M-130KM/H">M-130KM/H</option>
                    <option value="N-140KM/H">N-140KM/H</option>
                    <option value="P-150KM/H">P-150KM/H</option>
                    <option value="Q-160KM/H">Q-160KM/H</option>
                    <option value="R-170KM/H">R-170KM/H</option>
                    <option value="S-180KM/H">S-180KM/H</option>
                    <option value="T-190KM/H">T-190KM/H</option>
                    <option value="U-200KM/H">U-200KM/H</option>
                    <option value="H-210KM/H">H-210KM/H</option>
                    <option value="V-240KM/H">V-240KM/H</option>
                    <option value="W-270KM/H">W-270KM/H</option>
                    <option value="Y-300KM/H">Y-300KM/H</option>
                    <option value="Z-MAYOR A 300KM/H">Z-MAYOR A 300KM/H</option>
                </select>
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-5">
                <label><span class="Estilo1">CONSTANCIA/CUMPLIMIENTO:</span></label>
                <input type="text" class="form-control text-uppercase Estilo1" name="neu_xcum" id="neu_xcum" title="Ingrese Constancia/Cumplimiento">
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-5">
                <label><span class="Estilo1">ITEM DE LA CONSTANCIA:</span></label>
                <input type="text" class="form-control text-uppercase Estilo1" name="neu_xitem" id="neu_xitem" title="Ingrese Item de la Constancia">
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-2">
                <label><span class="Estilo1">VIGENCIA :</span></label>
                <input type="date" class="form-control text-uppercase Estilo1" name="neu_xvigen" id="neu_xvigen" title="Ingrese Vigencia">
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label><span class="Estilo1">DECLARACION/CONFORMIDAD:</span></label>
                <input type="text" class="form-control text-uppercase" id="neu_xconfor" name="neu_xconfor" title="Ingrese Declaracion/Conformidad">
            </div>
            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label><span class="Estilo1">PARTIDA ARANCELARIA :</span></label>
                <input type="text" class="form-control text-uppercase" id="neu_xparti" name="neu_xparti" title="Ingrese Partida Arancelaria">
            </div>
           </div><!--pbody-->
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitMarca" value="Save"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>
