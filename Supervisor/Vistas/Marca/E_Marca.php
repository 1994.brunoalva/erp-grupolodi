<?php
  include "../Funciones/BD.php";
  if ($_GET['edit'] !='') {
    $idm = $_GET['edit'];
    $sqlmarca = "SELECT * FROM sys_alm_marca WHERE mar_id='$idm'";
    $rsqlmar = mysqli_query($con,$sqlmarca);
    $rmac = mysqli_fetch_array($rsqlmar,MYSQLI_ASSOC);
    $xmarcaNom = $rmac['mar_nombre'];
    $fotomarca = $rmac['mar_logo'];
    #SI LA MARCA NO TIENE IMG AGREGADA
    if ($fotomarca != '') { $txtruta = '../Imagenes/Marca/'.$fotomarca;}
    else { $txtruta = "../Imagenes/Logos/noimage.png";}
  }
 ?>
 <style>
     .M_Marcalto{
       height: 370px;
     }
 </style>
 <div class="modal fade" id="E_Marca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-list fa-fw"></i>&nbsp;<strong>Editar Marca <?=$xid;?></strong></h3>
           </div>
           <div class="modal-body M_Marcalto">
             <form  id="RegistroC" role="form" action="index.php?menu=15" enctype="multipart/form-data" method="post">
               <input name="action" type="hidden" value="upload" />
               <input name="antfoto" type="hidden" value="<?=$fotomarca?>">
               <input name="EditM" type="hidden" value="<?=$idm;?>">
               <div class="form-group col-xs-12 col-md-12 col-lg-6">
                 <label><strong>CLASIFICACION:</strong></label>
                 <select class="form-control" name="xcat" required>
                   <?php
                   $sqlcat = "SELECT * FROM sys_alm_categoria ORDER BY cat_id";
                   $rsqlcat = mysqli_query($con, $sqlcat);
                   echo "<option value=''>--</option>";
                   if ($row2 = mysqli_fetch_array($rsqlcat, MYSQLI_ASSOC)) {
                     do {
                       echo '<option value="' . $row2['cat_id'] . '">' . $row2['cat_descripcion'] . '</option>';
                     } while ($row2 = mysqli_fetch_array($rsqlcat, MYSQLI_ASSOC));
                   }
                   ?>
                 </select>
               </div>
               <div class="form-group col-md-12 col-xs-12 col-lg-6">
                 <label><strong>DESCRIPCION:</strong></label>
                 <input class="form-control text-uppercase" pattern="[A-z0-9 ]+.{1}" name="xmarcaNom" value="<?=$xmarcaNom;?>" title=" Ingrese Nombre de la Marca .Letras y Numeros"  required>
               </div>
               <div class="form-group col-md-12 col-xs-12 col-lg-12">
                 <img id="uploadPreview2" class="bordeazul" width="506" height="200" src="<?=$txtruta;?>" />
                 <input height="20" class="form-control btn btn-primary" title="AGREGAR LOGO" accept="image/png, image/jpeg, image/gif" id="uploadImage2" type="file" name="uploadImage2" onchange="previewImage(2);" />
                </div>

           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitMarca" value="Edit"><i class="glyphicon glyphicon-floppy-saved"></i> Actualizar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>
 <script>
 function previewImage(nb) {
     var reader = new FileReader();
     reader.readAsDataURL(document.getElementById('uploadImage'+nb).files[0]);
     reader.onload = function (e) {
         document.getElementById('uploadPreview'+nb).src = e.target.result;
     };
 }
 </script>
<?php if ($_GET['edit'] !=''): ?>
  <script type="text/javascript">
    $(document).ready(function()
    {
       $("#E_Marca").modal("show");
    });
  </script>
<?php endif; ?>
