<?php
  include "../Funciones/BD.php";
  include "M_Marca.php";
  include "E_marca.php";
 ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="panel panel-default">
              <div class="panel-body">
              <div class="col-lg-12">
                <div class="row" id="titulo">
                      <div class="col-lg-6">
                      <h2 class="azul"><i class="fa fa-list fa-fw"></i><strong>Marcas</strong></h2>
                    </div>
                      <div class="col-lg-6 text-right">
                        <a  class="btn btn-primary" href="#M_Marca" data-toggle="modal"><i class="fa fa-plus"></i> Agregar</a>
                      </div>
                </div>
              </div>
            <div class="table-responsive col-lg-12"><hr class="black" />
              <div  id="element2">
               <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                 <thead>
                   <tr>
                     <tr>
                       <th class="text-center">NOMBRE</th>
                       <th class="text-center">CLASIFICACION</th>
                       <th class="text-center">EDITAR</th>
                       <th class="text-center">MODELOS</th>
                     </tr>
                   </tr>
                 </thead>
                 <tbody class="text-center">
                 <?php
                 $sql="SELECT sm.mar_id,sm.mar_nombre,sc.cat_descripcion,sc.cat_id FROM sys_alm_marca sm, sys_alm_categoria sc WHERE  sc.cat_id = sm.cat_id";
                 $result=mysqli_query($con,$sql);
                 while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ ?>
                   <tr>
                       <td><?= $row['mar_nombre']; ?></td>
                       <td><?= $row['cat_descripcion']; ?></td>
                     <!--EDITAR-->
                       <td class="centeralign">
                           <form   role="form" action="index.php?" method="get">
                             <input type="hidden" name="menu" value="14">
                                <button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="edit" value="<?=$row['mar_id'];?>">
                            </button>
                           </form>
                       </td>
                       <td><?php if ($row['cat_id'] =='1'): ?>
                         <form role="form" action="index.php?menu=16" method="post">
                           <button class="btn btn-primary btn-sm glyphicon glyphicon-eye-open" name="idm" value='<?php echo $row['mar_id']; ?>'></button>
                         </form>
                       <?php else: ?>
                         <button class="btn btn-primary btn-sm glyphicon glyphicon-eye-open" disabled></button>
                       <?php endif; ?></td>
                   </tr>
                     <?php } ?>
                 </tbody>
               </table>
               </div>
             </div>
      </div><!-- /.pbody -->
    </div><!-- /.pdefault -->
  </div><!-- /.col-lg-12 -->
</div><!-- /.row -->
