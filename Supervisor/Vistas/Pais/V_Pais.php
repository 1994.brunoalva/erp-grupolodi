<?php
  include "../Funciones/BD.php";
  include "Vistas/Modal/M_Pais.php";
  include "Vistas/Modal/E_Pais.php";
  include "Vistas/Modal/M_VPuertos.php";
  include "Funciones/Pais/Pais.php";


 ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-lg-12">
          <div class="row" id="titulo">
            <div class="col-lg-6">
              <h2 class="azul"><i class="fa fa-flag-o fa-fw"></i><strong>Pa&iacute;ses</strong></h2>
            </div>
            <div class="col-lg-6 text-right" >
              <a  class="btn btn-primary" href="#M_Pais" data-toggle="modal"><i class="fa fa-plus"></i> Agregar</a>
            </div>
          </div>
        </div>
        <div class="table-responsive col-lg-12"><hr class="black" />
          <?=$rmsg;?>
           <table class="table table-striped table-bordered table-hover" id="dataTables-example">
             <thead>
               <tr>
                 <th class="text-center">ID</th>
                 <th class="text-center">NOMBRE</th>
                 <th class="text-center">PUERTOS</th>
                <th class="text-center">EDITAR</th>
               </tr>
             </thead>
             <tbody class="text-center">
             <?php $sql="SELECT * FROM sys_com_pais";
             $result=mysqli_query($con,$sql);
             while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ ?>
               <tr>
                   <td><?= $row['pais_id']; ?></td>
                   <td><?= $row['pais_nombre']; ?></td>
                   <td>
                     <?php $sqlcant="SELECT COUNT(pais_id) canti FROM sys_com_puerto WHERE pais_id=".$row['pais_id'];
                     $rcant = mysqli_query($con,$sqlcant);
                     $acant = mysqli_fetch_array($rcant,MYSQLI_ASSOC);
                     $canti = $acant['canti'];
                     ?>
                     <?php if ($canti>'0'): ?>
                       <form  id="EditarE" role="form" action="index.php?" method="get">
                          <input type="hidden" name="menu" value="4">
                       <button class="btn btn-primary btn-sm glyphicon glyphicon-plus" name="puer" value='<?=$row['pais_id']; ?>'></button>
                     </form>
                   <?php else: ?>
                     <button class="btn btn-primary btn-sm glyphicon glyphicon-plus" disabled></button>
                     <?php endif; ?>
                    </td>
                 <!--EDITAR-->
                   <td class="centeralign">
                     <form  id="EditarE" role="form" action="index.php?" method="get">
                          <input type="hidden" name="menu" value="4">
                       <button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="edit" value='<?=$row['pais_id']; ?>'></button>
                     </form>
                   </td>
               </tr>
              <?php } ?>
             </tbody>
           </table>
        </div>
      </div>
    </div>
  </div>
</div>
