<?php
  include "../Funciones/BD.php";
  include "Vistas/Modal/E_Categoria.php";
  include "Vistas/Modal/M_Unidades.php";
  include "Funciones/Categorias/Categorias.php";
 ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-lg-12">
          <div class="row" id="titulo">
            <div class="col-lg-6">
              <h2 class="azul"><i class="fa fa-cog fa-fw"></i><strong>Categorias</strong></h2>
            </div>
          </div>
        </div>
        <div class="table-responsive col-lg-12"><hr class="black" />
          <?=$rmsg;?>
           <table class="table table-striped table-bordered table-hover" id="dataTables-example">
             <thead>
               <tr>
                 <th class="text-center">ID</th>
                 <th class="text-center">DESCRIPCION</th>
                 <th class="text-center">UNIDAD DE MEDIDA</th>
                <th class="text-center">EDITAR</th>
               </tr>
             </thead>
             <tbody class="text-center">
             <?php $sql="SELECT * FROM sys_alm_categoria";
             $result=mysqli_query($con,$sql);
             while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ ?>
               <tr>
                   <td><?= $row['cat_id']; ?></td>
                   <td><?= $row['cat_descripcion']; ?></td>
                   <td>
                     <?php $sqlcant="SELECT COUNT(cat_id) canti FROM sys_alm_unidad WHERE cat_id=".$row['cat_id'];
                     $rcant = mysqli_query($con,$sqlcant);
                     $acant = mysqli_fetch_array($rcant,MYSQLI_ASSOC);
                     $canti = $acant['canti'];

                     ?>
                     <?php if ($canti>'0'): ?>
                       <form  id="EditarE" role="form" action="index.php?" method="get">
                          <input type="hidden" name="menu" value="5">
                       <button class="btn btn-primary btn-sm glyphicon glyphicon-plus" name="unit" value='<?=$row['cat_id']; ?>'></button>
                     </form>
                     <?php else: ?>
                       <button class="btn btn-primary btn-sm glyphicon glyphicon-plus" disabled></button>
                     <?php endif; ?>
                     </td>
                 <!--EDITAR-->
                   <td class="centeralign">
                     <form  id="EditarE" role="form" action="index.php?" method="get">
                          <input type="hidden" name="menu" value="5">
                       <button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="edit" value='<?=$row['cat_id']; ?>'></button>
                     </form>
                   </td>
               </tr>
              <?php } ?>
             </tbody>
           </table>
        </div>
      </div>
    </div>
  </div>
</div>
