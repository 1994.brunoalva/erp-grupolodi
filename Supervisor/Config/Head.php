<?php
  error_reporting(E_ERROR | E_PARSE);
 ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta  http-equiv="Content-Type" content="text/html"; charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ERP - GrupoLodi</title>
    <!-- Bootstrap Styles-->
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="../assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href="../assets/css/open_sans.css" rel='stylesheet' />
    <!-- Data Tables-->
    <link href="../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="../assets/css/bootstrap-iso.css" rel="stylesheet" />
    <!-- jQuery Js -->
    <script src="../assets/js/jquery-1.10.2.js"></script>
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
              <!-- <div class="visible-md visible-lg">
                <a class="navbar-brand" href="index.php"><strong>ERP - GrupoLodi</strong></a>
              </div>-->
                <!--MENU PARA MOVIL-->
                <div class="visible-xs">
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                            <i class="fa fa-user fa-fw"></i> <?php echo $usuario; ?>&nbsp;<i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configuraci&oacute;n</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="../Funciones/Salir.php"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
              </div>
            </div>
            <div class="visible-md visible-lg">
                <ul class="nav navbar-top-links">
                  <li><a href="index.php"><strong><i class="fa fa-home fa-fw"></i> Incio</a></strong></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                            <strong><i class="fa fa-book fa-fw"></i> Almacen&nbsp;<i class="fa fa-caret-down"></i></strong>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?menu=14"><i class="fa fa-asterisk fa-fw"></i> Marcas / Modelos <span class="fa arrow rtop"></span></a></li>
                        </ul>
                    </li>

                    <li><a href="index.php?menu=7"><strong><i class="fa fa-building-o fa-fw"></i> Empresas</a></strong></li>
                    <li><a href="index.php?menu=18&opc=acc"><strong><i class="fa fa-asterisk fa-fw"></i> Productos</a></strong></li>
                    <li><a href="index.php?menu=19&opc=consul"><strong><i class="fa fa-group fa-fw"></i> Proveedores</a></strong></li>
                    <li><a href="index.php?menu=1"><strong><i class="fa fa-exchange fa-fw"></i> Importacion</a></strong></li>
                    <li class="dropdown text-right">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                            <strong><i class="fa fa-cogs fa-fw"></i> Configuraci&oacute;n&nbsp;<i class="fa fa-caret-down"></i></strong>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a  href="index.php?menu=6&opc=consul" ><i class="fa fa-truck fa-fw"></i> Agencias<span class="fa arrow rtop"></span></a></li>
                            <li><a  href="index.php?menu=5" data-toggle="modal"><i class="fa fa-cog fa-fw"></i> Categorias<span class="fa arrow rtop"></span></a></li>
                            <li><a  href="index.php?menu=4" data-toggle="modal"><i class="fa fa-flag fa-fw"></i> Pa&iacute;ses<span class="fa arrow rtop"></span></a></li>
                            <li><a  href="#M_Perfil" data-toggle="modal"><i class="fa fa-user fa-fw"></i> Perfil<span class="fa arrow rtop"></span></a></li>
                            <li><a href="../Funciones/Salir.php"><i class="fa fa-sign-out fa-fw"></i> Salir <span class="fa arrow rtop"></span></a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!--#hidden-xs oculto / visible-xs -->
        </nav>
<?php
require_once('M_Perfil.php');
?>
