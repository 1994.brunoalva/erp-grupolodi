<?php
$indexRuta=1;
require '../conexion/Conexion.php';


$conexion = (new Conexion())->getConexion();


$nombremodule= "Productos";

$listaPrecios = $conexion->query("SELECT
  tipl_id,
  tipl_categoria,
  tipl_estatus
FROM sys_cos_tipo_lista");

$listaEmpresas = $conexion->query("SELECT * FROM sys_empresas");


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../public/css/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../public/plugins/sweetalert2/sweetalert2.min.css">

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

    <style>
        .ui-autocomplete { z-index:2147483647; }
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Productos</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES
                                    <div class="col-lg-6 text-right">
                                        <button  data-toggle="modal" data-target="#modal_devoluciones"  class="btn btn-primary">
                                            <i class="fa fa-plus "></i>
                                        </button>

                                    </div>-->

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id=""  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div id="conten-modales">
                                    <table id="table-productos-almacen" class="table table-striped table-bordered table-hover">
                                        <thead class="bg-head-table">
                                        <tr  style="background-color: #007ac3; color: white" >

                                            <th style="border-right-color: #007ac3" class="text-center">PRODUCTO</th>
                                            <th style="border-right-color: #007ac3" class="text-center">SKU</th>
                                            <th style="border-right-color: #007ac3" class="text-center">MEDIDA</th>
                                            <th style="border-right-color: #007ac3" class="text-center">CATEGORIA</th>
                                            <th style="border-right-color: #007ac3" class="text-center">MARCA</th>
                                            <th style="border-right-color: #007ac3" class="text-center">PAIS</th>
                                            <th  class="text-center">STOCK</th>
                                            <th  class="text-center">OPCION</th>

                                        </tr>
                                        </thead>
                                        <tbody>



                                        </tbody>

                                    </table>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>
    <div class="modal fade" id="modal_data_p_s" tabindex="-1" role="dialog" aria-hidden="true"
         style="z-index: 1800; display: none;">
        <div class="modal-dialog modal-xs " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Informacion del producto</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form action="#">
                        <div class="container-fluid">
                            <div class="form-group col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Nombre:</label>
                                <input id="modal-producto-input-nombre-list" class="form-control" type="text" placeholder=""
                                       disabled>
                            </div>
                            <div class="form-group col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Tipo de precios:</label>
                                <select class="form-control">
                                    <?php
                                    foreach ($listaPrecios as $rowPr){
                                        echo "<option value='{$rowPr['tipl_id']}'>{$rowPr['tipl_categoria']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group  col-xs-12 no-padding">
                                <table style="width:100%" class="table table-striped">
                                    <tr style="background-color:  rgb(0, 122, 195);">
                                        <th style="border-right-color: rgb(0, 122, 195); color: white" class="text-center">EMPRESA</th>
                                        <th style="border-right-color: rgb(0, 122, 195); color: white" class="text-center">PRECIO</th>
                                        <th style=" color: white" class="text-center">STOCK</th>
                                    </tr>
                                    <tr>
                                        <td class="text-center">LODI IMPORT CENTER S.R.L.</td>
                                        <td class="text-center">$20000</td>
                                        <td class="text-center">10</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">LODI IMPORT CENTER S.R.L.</td>
                                        <td class="text-center">$20000</td>
                                        <td class="text-center">10</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">


                            <button type="button" class="btn btn-success" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_devoluciones" tabindex="-1" role="dialog" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Devolución de productos</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form action="#">
                        <div class="container-fluid">

                            <div class="form-group col-xs-6 no-padding">
                                <label class="col-xs-12 no-padding">EMPRESA:</label>
                                <select v-model="dataD.empresa" class="form-control" >
                                    <?php
                                    foreach ($listaEmpresas as $empr){
                                        echo "<option value='{$empr['emp_id']}'>{$empr['emp_nombre']}</option>";
                                    }
                                    ?>

                                </select>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">TIPO DOCUMENTO</label>
                                <div class="input-group col-xs-12">
                                    <select  v-model="dataD.tipoDoc"  id="tipodo" class="form-control">
                                        <option value="1">FACTURA</option>
                                        <option value="2">BOLETA</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">N. DOCUMENTO</label>
                                <div class="input-group col-xs-12">
                                    <input  v-model="dataD.numDoc"  class="form-control col-xs-6">
                                    <span class="input-group-btn">
                                                                            <button v-on:click="buscarDocumentoV()" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_incoterm">
                                                                                <i class="fa fa-search"></i>
                                                                            </button>
                                                                        </span>
                                </div>
                            </div>

                            <div v-if="encontrado" class="form-group col-xs-12 no-padding text-center">
                                <a  :href="'../ventas/comprobante_pdf.php?venta='+dataD.docVentaId" target="_blank" class="btn btn-info">Ver Comprobante de venta</a>
                            </div>

                            <div class="form-group col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Observaciones</label>
                                <div class="input-group col-xs-12">
                                    <textarea v-model="dataD.obs" class="form-control"></textarea>
                                </div>

                            </div>

                            <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                <label class="col-xs-12 no-padding">PRODUCTO</label>
                                <div class="input-group col-xs-12">
                                    <input id="input-buscar-producto" v-model="datos.nombre" placeholder="Escriba para buscar" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">CANTIDAD</label>
                                <div class="input-group col-xs-12">
                                    <input v-model="datos.catidad"  class="form-control col-xs-6">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">ESTADO</label>
                                <div class="input-group col-xs-12">
                                    <select  v-model="datos.estado" class="form-control">
                                        <option value="EXCELENTE">EXCELENTE</option>
                                        <option value="REGULAR">REGULAR</option>
                                        <option value="MALO">MALO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding" style="color: #ffffff;">.</label>
                                <div class="input-group col-xs-12 text-center" style="margin-bottom: 5px;">
                                    <button v-on:click="agregar()" type="button" class="btn btn-primary">AGREGAR</button>
                                </div>

                            </div>

                            <div class="form-group  col-xs-12 no-padding">
                                <table style="width:100%" class="table table-striped">
                                    <tr style="background-color:  rgb(0, 122, 195);">
                                        <th style="border-right-color: rgb(0, 122, 195); color: white" class="text-center">PRODUCTO</th>
                                        <th style="border-right-color: rgb(0, 122, 195); color: white" class="text-center">CANTIDAD</th>
                                        <th style=" color: white" class="text-center">ESTADO</th>
                                        <th style=" color: white" class="text-center"></th>
                                    </tr>
                                    <tr v-for="(item, index) in productos">
                                        <td class="text-center">{{item.producto}}</td>
                                        <td class="text-center">{{item.cnt}}</td>
                                        <td class="text-center">{{item.estado}}</td>
                                        <td class="text-center"><button v-on:click="eliminar(index)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button> </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">
                            <button type="button"  v-on:click="guaradrDevolucion()">
                                Registrar
                            </button>

                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script src="../public/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script>

        const app_dinamis_prod_devo = new Vue({
            el:"#modal_devoluciones",
            data:{

                dataD:{
                    empresa:0,
                    tipoDoc:0,
                    numDoc:'',
                    obs:'',
                    docVentaId:0
                },

                encontrado:false,
                datos:{
                    preData:{},
                    nombre:'',
                    catidad:'',
                    estado:''
                },
                productos:[]
            },
            methods: {
                guaradrDevolucion(){
                    swal({
                        title: "¿Desea guardar esta devolución?",
                        text: "Revise bien antes de continuar",
                        icon: "warning",
                        dangerMode: false,
                        buttons: ["NO", "SI"],
                    })
                        .then((ressss) => {
                            console.log(ressss);
                            if (ressss){
                                swal("Se guardó correctamente", "", "success")
                                    .then(function () {
                                        $('#modal_devoluciones').modal('hide');
                                    });
                            }
                        });
                },
                buscarDocumentoV(){
                    var dateD = {
                        doc:this.dataD.tipoDoc,
                        nunDoc:parseInt(this.dataD.numDoc+""),
                        empre:this.dataD.empresa
                    }
                    console.log(dateD)
                    $.ajax({
                        type: "POST",
                        url: "../ajaxs/buscar_documento_venta.php",
                        data: dateD,
                        success: function (data) {
                            console.log(data)
                            if (isJson(data)){

                                var jso = JSON.parse(data);
                                if (jso.res){
                                    // this.dataNE.serieNumero = jso;
                                    app_dinamis_prod_devo._data.dataD.docVentaId =jso.data.fac_id;
                                    app_dinamis_prod_devo._data.encontrado = true;

                                    swal("Documento encontrado", "", "success");
                                }else{
                                    swal("Documento no encontrado")
                                }


                            }else{
                                swal("Documento no encontrado")
                            }
                        }
                    });

                },
                agregar(){
                    this.productos.push({
                        idProdEmpr: this.datos.preData.prod_empre_id,
                        producto:this.datos.nombre,
                        cnt:this.datos.catidad,
                        estado:this.datos.estado
                    });
                    this.datos.preData={};
                        this.datos.nombre='';
                       this.datos.catidad='';
                        this.datos.estado='';
                },
                eliminar(index){
                    this.productos.splice(index, 1);
                }
            }
        });

        const app_dimanic_precio= new Vue({
            el:"#modal_data_p_s",
            data:{
                idproducto:0,
                listaDinamic:[]
            },
            methods:{
                getDatainfoProduct(idp){
                    this.idproducto = idp
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Producto/getDataPS_stock.php",
                        data: {idp},
                        success: function (data) {
                            data = JSON.parse(data);
                            $("#modal-producto-input-nombre-list").val(data.nombre)


                        }
                    });

                }
            }
        });

        $('#select_nomenglatura').change(function () {
            genNombreProducto();
        });
        $('#select_modal_prod_modelo').change(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-pliege").keyup(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-serie").keyup(function () {
            genNombreProducto();
        });
        function getDataPrecios(idpro) {
            app_dimanic_precio.getDatainfoProduct(idpro)
        }

        var tabla_productos ;
        $( document ).ready(function() {

            tabla_productos = $("#table-productos-almacen").DataTable({
                "processing": true,
                "serverSide": true,
                "sAjaxSource": "../ServerSide/serversideProductosAlmacen.php",
                order: [[3, "desc" ]],
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel'
                ],
                columnDefs: [
                    {
                        "targets": 0,
                        "data": "coti_id",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[0]+'</span>';

                        }
                    },
                    {
                        "targets": 1,
                        "data": "cli_tdoc",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[1]+'</span>';

                        }
                    },
                    {
                        "targets": 2,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[2]+'</span>';

                        }
                    },
                    {
                        "targets": 3,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[3]+'</span>';

                        }
                    },
                    {
                        "targets": 4,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[4]+'</span>';

                        }
                    },
                    {
                        "targets": 5,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[5]+'</span>';

                        }
                    },
                    {
                        "targets": 6,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[6]+'</span>';

                        }
                    },
                    {
                        "targets": 7,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<button  data-toggle="modal" data-target="#modal_data_p_s" onclick="getDataPrecios('+row[7]+')" class="btn btn-info" style="display: block;margin: auto;text-align: center;"><i class="fa fa-eye"></i></button>';

                        }
                    },
                ],
                language: {
                    url: '../assets/Spanish.json'
                }
            });

            $("#input-buscar-producto").autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "../ajaxs/buscar_productos2.php",
                        data: { term: request.term,empresa:app_dinamis_prod_devo._data.dataD.empresa},
                        success: function(data){
                            response(data);
                            console.log("aaaa")
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log(jqXHR.responseText)
                            console.log("bbbbb")
                        },
                        dataType: 'json'
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    console.log(ui.item)
                    app_dinamis_prod_devo._data.datos.preData = ui.item;
                    app_dinamis_prod_devo._data.datos.nombre = ui.item.produ_nombre;
                    app_dinamis_prod_devo._data.datos.catidad = '';
                    app_dinamis_prod_devo._data.datos.estado = '';

                    event.preventDefault();

                }
            });


        });

    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>



<script type="text/javascript">

    $(document).ready(function() {



        $('#tttttttttttttttt').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
    /* $('#1111111111111').DataTable({

            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });*/



    });

    function NumeroAleatorio(min, max) {
        var num = Math.round(Math.random() * (max - min) + min);
        return num;
    }


</script>

</html>
