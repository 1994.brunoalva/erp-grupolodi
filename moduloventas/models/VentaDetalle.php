<?php


class VentaDetalle
{
    private $facd_id;
    private $prod_id;
    private $facd_cantidad;
    private $facd_preciou;
    private $facd_descuento;
    private $facd_subtotal;
    private $fac_id;
    private $origen;
    private $estado;

    /**
     * @return mixed
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * @param mixed $origen
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getFacdId()
    {
        return $this->facd_id;
    }

    /**
     * @param mixed $facd_id
     */
    public function setFacdId($facd_id)
    {
        $this->facd_id = $facd_id;
    }

    /**
     * @return mixed
     */
    public function getProdId()
    {
        return $this->prod_id;
    }

    /**
     * @param mixed $prod_id
     */
    public function setProdId($prod_id)
    {
        $this->prod_id = $prod_id;
    }

    /**
     * @return mixed
     */
    public function getFacdCantidad()
    {
        return $this->facd_cantidad;
    }

    /**
     * @param mixed $facd_cantidad
     */
    public function setFacdCantidad($facd_cantidad)
    {
        $this->facd_cantidad = $facd_cantidad;
    }

    /**
     * @return mixed
     */
    public function getFacdPreciou()
    {
        return $this->facd_preciou;
    }

    /**
     * @param mixed $facd_preciou
     */
    public function setFacdPreciou($facd_preciou)
    {
        $this->facd_preciou = $facd_preciou;
    }

    /**
     * @return mixed
     */
    public function getFacdDescuento()
    {
        return $this->facd_descuento;
    }

    /**
     * @param mixed $facd_descuento
     */
    public function setFacdDescuento($facd_descuento)
    {
        $this->facd_descuento = $facd_descuento;
    }

    /**
     * @return mixed
     */
    public function getFacdSubtotal()
    {
        return $this->facd_subtotal;
    }

    /**
     * @param mixed $facd_subtotal
     */
    public function setFacdSubtotal($facd_subtotal)
    {
        $this->facd_subtotal = $facd_subtotal;
    }

    /**
     * @return mixed
     */
    public function getFacId()
    {
        return $this->fac_id;
    }

    /**
     * @param mixed $fac_id
     */
    public function setFacId($fac_id)
    {
        $this->fac_id = $fac_id;
    }



}