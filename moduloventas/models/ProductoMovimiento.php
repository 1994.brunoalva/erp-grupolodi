<?php


class ProductoMovimiento
{
    public $mov_id;
    public $pro_emp_id;
    public $mov_tipo;
    public $mov_fecha;
    public $mov_cantidad;
    public $mov_descripcion;
    public $estado;

    /**
     * @return mixed
     */
    public function getMovId()
    {
        return $this->mov_id;
    }

    /**
     * @param mixed $mov_id
     */
    public function setMovId($mov_id)
    {
        $this->mov_id = $mov_id;
    }

    /**
     * @return mixed
     */
    public function getProEmpId()
    {
        return $this->pro_emp_id;
    }

    /**
     * @param mixed $pro_emp_id
     */
    public function setProEmpId($pro_emp_id)
    {
        $this->pro_emp_id = $pro_emp_id;
    }

    /**
     * @return mixed
     */
    public function getMovTipo()
    {
        return $this->mov_tipo;
    }

    /**
     * @param mixed $mov_tipo
     */
    public function setMovTipo($mov_tipo)
    {
        $this->mov_tipo = $mov_tipo;
    }

    /**
     * @return mixed
     */
    public function getMovFecha()
    {
        return $this->mov_fecha;
    }

    /**
     * @param mixed $mov_fecha
     */
    public function setMovFecha($mov_fecha)
    {
        $this->mov_fecha = $mov_fecha;
    }

    /**
     * @return mixed
     */
    public function getMovCantidad()
    {
        return $this->mov_cantidad;
    }

    /**
     * @param mixed $mov_cantidad
     */
    public function setMovCantidad($mov_cantidad)
    {
        $this->mov_cantidad = $mov_cantidad;
    }

    /**
     * @return mixed
     */
    public function getMovDescripcion()
    {
        return $this->mov_descripcion;
    }

    /**
     * @param mixed $mov_descripcion
     */
    public function setMovDescripcion($mov_descripcion)
    {
        $this->mov_descripcion = $mov_descripcion;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }




}