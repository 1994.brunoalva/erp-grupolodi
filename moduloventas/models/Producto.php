<?php


class Producto
{
    private  $prod_empre_id;
    private  $produ_codprod;
    private $cantidad;

    /**
     * Producto constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return mixed
     */
    public function getProdEmpreId()
    {
        return $this->prod_empre_id;
    }

    /**
     * @param mixed $prod_empre_id
     */
    public function setProdEmpreId($prod_empre_id)
    {
        $this->prod_empre_id = $prod_empre_id;
    }

    /**
     * @return mixed
     */
    public function getProduCodprod()
    {
        return $this->produ_codprod;
    }

    /**
     * @param mixed $produ_codprod
     */
    public function setProduCodprod($produ_codprod)
    {
        $this->produ_codprod = $produ_codprod;
    }


}