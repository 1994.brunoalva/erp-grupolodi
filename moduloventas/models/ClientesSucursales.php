<?php


class ClientesSucursales
{
    private $su_id;
    private $su_ruc;
    private $su_codigo;
    private $su_tipo;
    private $su_direccion;
    private $id_agencia;
    private $ubigeo;

    /**
     * @return mixed
     */
    public function getUbigeo()
    {
        return $this->ubigeo;
    }

    /**
     * @param mixed $ubigeo
     */
    public function setUbigeo($ubigeo)
    {
        $this->ubigeo = $ubigeo;
    }
    /**
     * @return mixed
     */
    public function getSuId()
    {
        return $this->su_id;
    }

    /**
     * @param mixed $su_id
     */
    public function setSuId($su_id)
    {
        $this->su_id = $su_id;
    }

    /**
     * @return mixed
     */
    public function getSuRuc()
    {
        return $this->su_ruc;
    }

    /**
     * @param mixed $su_ruc
     */
    public function setSuRuc($su_ruc)
    {
        $this->su_ruc = $su_ruc;
    }

    /**
     * @return mixed
     */
    public function getSuCodigo()
    {
        return $this->su_codigo;
    }

    /**
     * @param mixed $su_codigo
     */
    public function setSuCodigo($su_codigo)
    {
        $this->su_codigo = $su_codigo;
    }

    /**
     * @return mixed
     */
    public function getSuTipo()
    {
        return $this->su_tipo;
    }

    /**
     * @param mixed $su_tipo
     */
    public function setSuTipo($su_tipo)
    {
        $this->su_tipo = $su_tipo;
    }

    /**
     * @return mixed
     */
    public function getSuDireccion()
    {
        return $this->su_direccion;
    }

    /**
     * @param mixed $su_direccion
     */
    public function setSuDireccion($su_direccion)
    {
        $this->su_direccion = $su_direccion;
    }

    /**
     * @return mixed
     */
    public function getIdAgencia()
    {
        return $this->id_agencia;
    }

    /**
     * @param mixed $id_agencia
     */
    public function setIdAgencia($id_agencia)
    {
        $this->id_agencia = $id_agencia;
    }


}