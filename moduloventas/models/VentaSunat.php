<?php


class VentaSunat
{
    private $id_venta_sunat;
    private $hash_v;
    private $nombreXML;
    private $dataQR;
    private $estado;

    /**
     * @return mixed
     */
    public function getIdVentaSunat()
    {
        return $this->id_venta_sunat;
    }

    /**
     * @param mixed $id_venta_sunat
     */
    public function setIdVentaSunat($id_venta_sunat)
    {
        $this->id_venta_sunat = $id_venta_sunat;
    }

    /**
     * @return mixed
     */
    public function getHashV()
    {
        return $this->hash_v;
    }

    /**
     * @param mixed $hash_v
     */
    public function setHashV($hash_v)
    {
        $this->hash_v = $hash_v;
    }

    /**
     * @return mixed
     */
    public function getNombreXML()
    {
        return $this->nombreXML;
    }

    /**
     * @param mixed $nombreXML
     */
    public function setNombreXML($nombreXML)
    {
        $this->nombreXML = $nombreXML;
    }

    /**
     * @return mixed
     */
    public function getDataQR()
    {
        return $this->dataQR;
    }

    /**
     * @param mixed $dataQR
     */
    public function setDataQR($dataQR)
    {
        $this->dataQR = $dataQR;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }



}