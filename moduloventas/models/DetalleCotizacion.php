<?php


class DetalleCotizacion
{
    private $id;
    private $id_cotizacion;
    private $id_prod_empre;
    private $id_producto;
    private $id_empresa;
    private $cantidad;
    private $descuento;
    private $precio_unitario;
    private $producto_desc;
    private $estado;

    /**
     * DetalleCotizacion constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getIdProdEmpre()
    {
        return $this->id_prod_empre;
    }

    /**
     * @param mixed $id_prod_empre
     */
    public function setIdProdEmpre($id_prod_empre)
    {
        $this->id_prod_empre = $id_prod_empre;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdCotizacion()
    {
        return $this->id_cotizacion;
    }

    /**
     * @param mixed $id_cotizacion
     */
    public function setIdCotizacion($id_cotizacion)
    {
        $this->id_cotizacion = $id_cotizacion;
    }

    /**
     * @return mixed
     */
    public function getIdProducto()
    {
        return $this->id_producto;
    }

    /**
     * @param mixed $id_producto
     */
    public function setIdProducto($id_producto)
    {
        $this->id_producto = $id_producto;
    }

    /**
     * @return mixed
     */
    public function getIdEmpresa()
    {
        return $this->id_empresa;
    }

    /**
     * @param mixed $id_empresa
     */
    public function setIdEmpresa($id_empresa)
    {
        $this->id_empresa = $id_empresa;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return mixed
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * @param mixed $descuento
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;
    }

    /**
     * @return mixed
     */
    public function getPrecioUnitario()
    {
        return $this->precio_unitario;
    }

    /**
     * @param mixed $precio_unitario
     */
    public function setPrecioUnitario($precio_unitario)
    {
        $this->precio_unitario = $precio_unitario;
    }

    /**
     * @return mixed
     */
    public function getProductoDesc()
    {
        return $this->producto_desc;
    }

    /**
     * @param mixed $producto_desc
     */
    public function setProductoDesc($producto_desc)
    {
        $this->producto_desc = $producto_desc;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}