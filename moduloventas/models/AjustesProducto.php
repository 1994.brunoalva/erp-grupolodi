<?php


class AjustesProducto
{
    private $ajus_id;
    private $emp_id;
    private $fecha;
    private $motivo;
    private $cnt_prod;

    /**
     * @return mixed
     */
    public function getEmpId()
    {
        return $this->emp_id;
    }

    /**
     * @param mixed $emp_id
     */
    public function setEmpId($emp_id)
    {
        $this->emp_id = $emp_id;
    }

    /**
     * @return mixed
     */
    public function getAjusId()
    {
        return $this->ajus_id;
    }

    /**
     * @param mixed $ajus_id
     */
    public function setAjusId($ajus_id)
    {
        $this->ajus_id = $ajus_id;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * @param mixed $motivo
     */
    public function setMotivo($motivo)
    {
        $this->motivo = $motivo;
    }

    /**
     * @return mixed
     */
    public function getCntProd()
    {
        return $this->cnt_prod;
    }

    /**
     * @param mixed $cnt_prod
     */
    public function setCntProd($cnt_prod)
    {
        $this->cnt_prod = $cnt_prod;
    }




}