<?php


class NotaDetalle
{
    private $nota_detalle_id;
    private $nota_id;
    private $descripcion;
    private $medida;
    private $catidad;
    private $precio;

    /**
     * @return mixed
     */
    public function getNotaDetalleId()
    {
        return $this->nota_detalle_id;
    }

    /**
     * @param mixed $nota_detalle_id
     */
    public function setNotaDetalleId($nota_detalle_id)
    {
        $this->nota_detalle_id = $nota_detalle_id;
    }

    /**
     * @return mixed
     */
    public function getNotaId()
    {
        return $this->nota_id;
    }

    /**
     * @param mixed $nota_id
     */
    public function setNotaId($nota_id)
    {
        $this->nota_id = $nota_id;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getMedida()
    {
        return $this->medida;
    }

    /**
     * @param mixed $medida
     */
    public function setMedida($medida)
    {
        $this->medida = $medida;
    }

    /**
     * @return mixed
     */
    public function getCatidad()
    {
        return $this->catidad;
    }

    /**
     * @param mixed $catidad
     */
    public function setCatidad($catidad)
    {
        $this->catidad = $catidad;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }



}