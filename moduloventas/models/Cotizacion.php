<?php


class Cotizacion
{
    private $coti_id;
    private $id_cliente;
    private $id_direccion;
    private $id_agencia;
    private $coti_tp_docu;
    private $coti_ruc;
    private $coti_razon;
    private $coti_ate;
    private $coti_telf;
    private $coti_pago;
    private $coti_tp_pago;
    private $coti_dias_credito;
    private $coti_tp_cambio;
    private $coti_tp_moneda;
    private $coti_obs;
    private $coti_llega;
    private $coti_estatus;
    private $coti_fecha;
    private $coti_total;
    private $coti_fecha_venta;
    private $tipo_costo_prod;
    private $estado;

    /**
     * Cotizacion constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getTipoCostoProd()
    {
        return $this->tipo_costo_prod;
    }

    /**
     * @param mixed $tipo_costo_prod
     */
    public function setTipoCostoProd($tipo_costo_prod)
    {
        $this->tipo_costo_prod = $tipo_costo_prod;
    }


    /**
     * @return mixed
     */
    public function getCotiId()
    {
        return $this->coti_id;
    }

    /**
     * @param mixed $coti_id
     */
    public function setCotiId($coti_id)
    {
        $this->coti_id = $coti_id;
    }

    /**
     * @return mixed
     */
    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    /**
     * @param mixed $id_cliente
     */
    public function setIdCliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;
    }

    /**
     * @return mixed
     */
    public function getIdDireccion()
    {
        return $this->id_direccion;
    }

    /**
     * @param mixed $id_direccion
     */
    public function setIdDireccion($id_direccion)
    {
        $this->id_direccion = $id_direccion;
    }

    /**
     * @return mixed
     */
    public function getIdAgencia()
    {
        return $this->id_agencia;
    }

    /**
     * @param mixed $id_agencia
     */
    public function setIdAgencia($id_agencia)
    {
        $this->id_agencia = $id_agencia;
    }

    /**
     * @return mixed
     */
    public function getCotiTpDocu()
    {
        return $this->coti_tp_docu;
    }

    /**
     * @param mixed $coti_tp_docu
     */
    public function setCotiTpDocu($coti_tp_docu)
    {
        $this->coti_tp_docu = $coti_tp_docu;
    }

    /**
     * @return mixed
     */
    public function getCotiRuc()
    {
        return $this->coti_ruc;
    }

    /**
     * @param mixed $coti_ruc
     */
    public function setCotiRuc($coti_ruc)
    {
        $this->coti_ruc = $coti_ruc;
    }

    /**
     * @return mixed
     */
    public function getCotiRazon()
    {
        return $this->coti_razon;
    }

    /**
     * @param mixed $coti_razon
     */
    public function setCotiRazon($coti_razon)
    {
        $this->coti_razon = $coti_razon;
    }

    /**
     * @return mixed
     */
    public function getCotiAte()
    {
        return $this->coti_ate;
    }

    /**
     * @param mixed $coti_ate
     */
    public function setCotiAte($coti_ate)
    {
        $this->coti_ate = $coti_ate;
    }

    /**
     * @return mixed
     */
    public function getCotiTelf()
    {
        return $this->coti_telf;
    }

    /**
     * @param mixed $coti_telf
     */
    public function setCotiTelf($coti_telf)
    {
        $this->coti_telf = $coti_telf;
    }

    /**
     * @return mixed
     */
    public function getCotiPago()
    {
        return $this->coti_pago;
    }

    /**
     * @param mixed $coti_pago
     */
    public function setCotiPago($coti_pago)
    {
        $this->coti_pago = $coti_pago;
    }

    /**
     * @return mixed
     */
    public function getCotiTpPago()
    {
        return $this->coti_tp_pago;
    }

    /**
     * @param mixed $coti_tp_pago
     */
    public function setCotiTpPago($coti_tp_pago)
    {
        $this->coti_tp_pago = $coti_tp_pago;
    }

    /**
     * @return mixed
     */
    public function getCotiDiasCredito()
    {
        return $this->coti_dias_credito;
    }

    /**
     * @param mixed $coti_dias_credito
     */
    public function setCotiDiasCredito($coti_dias_credito)
    {
        $this->coti_dias_credito = $coti_dias_credito;
    }

    /**
     * @return mixed
     */
    public function getCotiTpCambio()
    {
        return $this->coti_tp_cambio;
    }

    /**
     * @param mixed $coti_tp_cambio
     */
    public function setCotiTpCambio($coti_tp_cambio)
    {
        $this->coti_tp_cambio = $coti_tp_cambio;
    }

    /**
     * @return mixed
     */
    public function getCotiTpMoneda()
    {
        return $this->coti_tp_moneda;
    }

    /**
     * @param mixed $coti_tp_moneda
     */
    public function setCotiTpMoneda($coti_tp_moneda)
    {
        $this->coti_tp_moneda = $coti_tp_moneda;
    }

    /**
     * @return mixed
     */
    public function getCotiObs()
    {
        return $this->coti_obs;
    }

    /**
     * @param mixed $coti_obs
     */
    public function setCotiObs($coti_obs)
    {
        $this->coti_obs = $coti_obs;
    }

    /**
     * @return mixed
     */
    public function getCotiLlega()
    {
        return $this->coti_llega;
    }

    /**
     * @param mixed $coti_llega
     */
    public function setCotiLlega($coti_llega)
    {
        $this->coti_llega = $coti_llega;
    }

    /**
     * @return mixed
     */
    public function getCotiEstatus()
    {
        return $this->coti_estatus;
    }

    /**
     * @param mixed $coti_estatus
     */
    public function setCotiEstatus($coti_estatus)
    {
        $this->coti_estatus = $coti_estatus;
    }

    /**
     * @return mixed
     */
    public function getCotiFecha()
    {
        return $this->coti_fecha;
    }

    /**
     * @param mixed $coti_fecha
     */
    public function setCotiFecha($coti_fecha)
    {
        $this->coti_fecha = $coti_fecha;
    }

    /**
     * @return mixed
     */
    public function getCotiTotal()
    {
        return $this->coti_total;
    }

    /**
     * @param mixed $coti_total
     */
    public function setCotiTotal($coti_total)
    {
        $this->coti_total = $coti_total;
    }

    /**
     * @return mixed
     */
    public function getCotiFechaVenta()
    {
        return $this->coti_fecha_venta;
    }

    /**
     * @param mixed $coti_fecha_venta
     */
    public function setCotiFechaVenta($coti_fecha_venta)
    {
        $this->coti_fecha_venta = $coti_fecha_venta;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function sqlInsert(){
        return "INSERT INTO sys_cotizacion
VALUES (NULL,
        '$this->id_cliente',
        '$this->id_direccion',
        '$this->id_agencia',
        '$this->coti_tp_docu',
        '$this->coti_ruc',
        '$this->coti_razon',
        '$this->coti_ate',
        '$this->coti_telf',
        '$this->coti_pago',
        '$this->coti_tp_pago',
        '$this->coti_dias_credito',
        '$this->coti_tp_cambio',
        '$this->coti_tp_moneda',
        '$this->coti_obs',
        '$this->coti_llega',
        '$this->coti_estatus',
        '$this->coti_fecha',
        '$this->coti_total',
        '$this->coti_fecha_venta',
        '$this->tipo_costo_prod',
        '$this->estado');";
    }

}