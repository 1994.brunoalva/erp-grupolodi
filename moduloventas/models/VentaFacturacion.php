<?php


class VentaFacturacion
{
    private $fac_id;
    private $fac_num;
    private $fac_ncontrol;
    private $fac_fechae;
    private $tip_docu_sunat;
    private $fac_hora;
    private $fac_fechav;
    private $fac_diasc;
    private $fecha_a;
    private $tas_id;
    private $sun_id;
    private $cli_ndoc;
    private $pag_id;
    private $pag_forma;
    private $emp_id;
    private $fac_total;
    private $coti_id;
    private $fac_serie;
    private $fac_cost_list_id;
    private $fac_estatus;
    private $direccion;
    private $observaciones;

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * @param mixed $observaciones
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;
    }

    /**
     * @return mixed
     */
    public function getFacCostListId()
    {
        return $this->fac_cost_list_id;
    }

    /**
     * @param mixed $fac_cost_list_id
     */
    public function setFacCostListId($fac_cost_list_id)
    {
        $this->fac_cost_list_id = $fac_cost_list_id;
    }


    /**
     * @return mixed
     */
    public function getTipDocuSunat()
    {
        return $this->tip_docu_sunat;
    }

    /**
     * @param mixed $tip_docu_sunat
     */
    public function setTipDocuSunat($tip_docu_sunat)
    {
        $this->tip_docu_sunat = $tip_docu_sunat;
    }

    /**
     * @return mixed
     */
    public function getFacId()
    {
        return $this->fac_id;
    }

    /**
     * @param mixed $fac_id
     */
    public function setFacId($fac_id)
    {
        $this->fac_id = $fac_id;
    }

    /**
     * @return mixed
     */
    public function getFacNum()
    {
        return $this->fac_num;
    }

    /**
     * @param mixed $fac_num
     */
    public function setFacNum($fac_num)
    {
        $this->fac_num = $fac_num;
    }

    /**
     * @return mixed
     */
    public function getFacNcontrol()
    {
        return $this->fac_ncontrol;
    }

    /**
     * @param mixed $fac_ncontrol
     */
    public function setFacNcontrol($fac_ncontrol)
    {
        $this->fac_ncontrol = $fac_ncontrol;
    }

    /**
     * @return mixed
     */
    public function getFacFechae()
    {
        return $this->fac_fechae;
    }

    /**
     * @param mixed $fac_fechae
     */
    public function setFacFechae($fac_fechae)
    {
        $this->fac_fechae = $fac_fechae;
    }

    /**
     * @return mixed
     */
    public function getFacHora()
    {
        return $this->fac_hora;
    }

    /**
     * @param mixed $fac_hora
     */
    public function setFacHora($fac_hora)
    {
        $this->fac_hora = $fac_hora;
    }

    /**
     * @return mixed
     */
    public function getFacFechav()
    {
        return $this->fac_fechav;
    }

    /**
     * @param mixed $fac_fechav
     */
    public function setFacFechav($fac_fechav)
    {
        $this->fac_fechav = $fac_fechav;
    }

    /**
     * @return mixed
     */
    public function getFacDiasc()
    {
        return $this->fac_diasc;
    }

    /**
     * @param mixed $fac_diasc
     */
    public function setFacDiasc($fac_diasc)
    {
        $this->fac_diasc = $fac_diasc;
    }

    /**
     * @return mixed
     */
    public function getFechaA()
    {
        return $this->fecha_a;
    }

    /**
     * @param mixed $fecha_a
     */
    public function setFechaA($fecha_a)
    {
        $this->fecha_a = $fecha_a;
    }

    /**
     * @return mixed
     */
    public function getTasId()
    {
        return $this->tas_id;
    }

    /**
     * @param mixed $tas_id
     */
    public function setTasId($tas_id)
    {
        $this->tas_id = $tas_id;
    }

    /**
     * @return mixed
     */
    public function getSunId()
    {
        return $this->sun_id;
    }

    /**
     * @param mixed $sun_id
     */
    public function setSunId($sun_id)
    {
        $this->sun_id = $sun_id;
    }

    /**
     * @return mixed
     */
    public function getCliNdoc()
    {
        return $this->cli_ndoc;
    }

    /**
     * @param mixed $cli_ndoc
     */
    public function setCliNdoc($cli_ndoc)
    {
        $this->cli_ndoc = $cli_ndoc;
    }

    /**
     * @return mixed
     */
    public function getPagId()
    {
        return $this->pag_id;
    }

    /**
     * @param mixed $pag_id
     */
    public function setPagId($pag_id)
    {
        $this->pag_id = $pag_id;
    }

    /**
     * @return mixed
     */
    public function getPagForma()
    {
        return $this->pag_forma;
    }

    /**
     * @param mixed $pag_forma
     */
    public function setPagForma($pag_forma)
    {
        $this->pag_forma = $pag_forma;
    }

    /**
     * @return mixed
     */
    public function getEmpId()
    {
        return $this->emp_id;
    }

    /**
     * @param mixed $emp_id
     */
    public function setEmpId($emp_id)
    {
        $this->emp_id = $emp_id;
    }

    /**
     * @return mixed
     */
    public function getFacTotal()
    {
        return $this->fac_total;
    }

    /**
     * @param mixed $fac_total
     */
    public function setFacTotal($fac_total)
    {
        $this->fac_total = $fac_total;
    }

    /**
     * @return mixed
     */
    public function getCotiId()
    {
        return $this->coti_id;
    }

    /**
     * @param mixed $coti_id
     */
    public function setCotiId($coti_id)
    {
        $this->coti_id = $coti_id;
    }

    /**
     * @return mixed
     */
    public function getFacSerie()
    {
        return $this->fac_serie;
    }

    /**
     * @param mixed $fac_serie
     */
    public function setFacSerie($fac_serie)
    {
        $this->fac_serie = $fac_serie;
    }

    /**
     * @return mixed
     */
    public function getFacEstatus()
    {
        return $this->fac_estatus;
    }

    /**
     * @param mixed $fac_estatus
     */
    public function setFacEstatus($fac_estatus)
    {
        $this->fac_estatus = $fac_estatus;
    }


    public function getSttring(){
        return "INSERT INTO sys_ven_facturacion
VALUES ('$this->fac_id',
        '$this->fac_num',
        '$this->fac_ncontrol',
        '$this->fac_fechae',
        '$this->fac_hora',
        '$this->tip_docu_sunat',
        '$this->fac_fechav',
        '$this->fac_diasc',
        '$this->fecha_a',
        '$this->tas_id',
        '$this->sun_id',
        '$this->cli_ndoc',
        '$this->pag_id',
        '$this->pag_forma',
        '$this->emp_id',
        '$this->fac_total',
        '$this->coti_id',
        '$this->fac_serie',
        '$this->fac_cost_list_id',
        '$this->fac_estatus');";
    }

}