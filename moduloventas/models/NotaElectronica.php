<?php


class NotaElectronica
{
    private $notc_id;
    private $not_sn;
    private $not_tipo_doc;
    private $notc_tipo;
    private $notc_descripcion;
    private $not_cod_motivo;
    private $nota_sustento;
    private $notc_monto;
    private $notc_disponible;
    private $notc_usado;
    private $sun_id;
    private $fac_id;
    private $cli_ndoc;
    private $notc_fecha;
    private $notc_estatus;
    private $emp_id;
    private $asig_id;

    /**
     * @return mixed
     */
    public function getNotaSustento()
    {
        return $this->nota_sustento;
    }

    /**
     * @param mixed $nota_sustento
     */
    public function setNotaSustento($nota_sustento)
    {
        $this->nota_sustento = $nota_sustento;
    }

    /**
     * @return mixed
     */
    public function getNotSn()
    {
        return $this->not_sn;
    }

    /**
     * @param mixed $not_sn
     */
    public function setNotSn($not_sn)
    {
        $this->not_sn = $not_sn;
    }

    /**
     * @return mixed
     */
    public function getNotCodMotivo()
    {
        return $this->not_cod_motivo;
    }

    /**
     * @param mixed $not_cod_motivo
     */
    public function setNotCodMotivo($not_cod_motivo)
    {
        $this->not_cod_motivo = $not_cod_motivo;
    }

    /**
     * @return mixed
     */
    public function getNotcId()
    {
        return $this->notc_id;
    }

    /**
     * @param mixed $notc_id
     */
    public function setNotcId($notc_id)
    {
        $this->notc_id = $notc_id;
    }

    /**
     * @return mixed
     */
    public function getNotTipoDoc()
    {
        return $this->not_tipo_doc;
    }

    /**
     * @param mixed $not_tipo_doc
     */
    public function setNotTipoDoc($not_tipo_doc)
    {
        $this->not_tipo_doc = $not_tipo_doc;
    }

    /**
     * @return mixed
     */
    public function getNotcTipo()
    {
        return $this->notc_tipo;
    }

    /**
     * @param mixed $notc_tipo
     */
    public function setNotcTipo($notc_tipo)
    {
        $this->notc_tipo = $notc_tipo;
    }

    /**
     * @return mixed
     */
    public function getNotcDescripcion()
    {
        return $this->notc_descripcion;
    }

    /**
     * @param mixed $notc_descripcion
     */
    public function setNotcDescripcion($notc_descripcion)
    {
        $this->notc_descripcion = $notc_descripcion;
    }

    /**
     * @return mixed
     */
    public function getNotcMonto()
    {
        return $this->notc_monto;
    }

    /**
     * @param mixed $notc_monto
     */
    public function setNotcMonto($notc_monto)
    {
        $this->notc_monto = $notc_monto;
    }

    /**
     * @return mixed
     */
    public function getNotcDisponible()
    {
        return $this->notc_disponible;
    }

    /**
     * @param mixed $notc_disponible
     */
    public function setNotcDisponible($notc_disponible)
    {
        $this->notc_disponible = $notc_disponible;
    }

    /**
     * @return mixed
     */
    public function getNotcUsado()
    {
        return $this->notc_usado;
    }

    /**
     * @param mixed $notc_usado
     */
    public function setNotcUsado($notc_usado)
    {
        $this->notc_usado = $notc_usado;
    }

    /**
     * @return mixed
     */
    public function getSunId()
    {
        return $this->sun_id;
    }

    /**
     * @param mixed $sun_id
     */
    public function setSunId($sun_id)
    {
        $this->sun_id = $sun_id;
    }

    /**
     * @return mixed
     */
    public function getFacId()
    {
        return $this->fac_id;
    }

    /**
     * @param mixed $fac_id
     */
    public function setFacId($fac_id)
    {
        $this->fac_id = $fac_id;
    }

    /**
     * @return mixed
     */
    public function getCliNdoc()
    {
        return $this->cli_ndoc;
    }

    /**
     * @param mixed $cli_ndoc
     */
    public function setCliNdoc($cli_ndoc)
    {
        $this->cli_ndoc = $cli_ndoc;
    }

    /**
     * @return mixed
     */
    public function getNotcFecha()
    {
        return $this->notc_fecha;
    }

    /**
     * @param mixed $notc_fecha
     */
    public function setNotcFecha($notc_fecha)
    {
        $this->notc_fecha = $notc_fecha;
    }

    /**
     * @return mixed
     */
    public function getNotcEstatus()
    {
        return $this->notc_estatus;
    }

    /**
     * @param mixed $notc_estatus
     */
    public function setNotcEstatus($notc_estatus)
    {
        $this->notc_estatus = $notc_estatus;
    }

    /**
     * @return mixed
     */
    public function getEmpId()
    {
        return $this->emp_id;
    }

    /**
     * @param mixed $emp_id
     */
    public function setEmpId($emp_id)
    {
        $this->emp_id = $emp_id;
    }

    /**
     * @return mixed
     */
    public function getAsigId()
    {
        return $this->asig_id;
    }

    /**
     * @param mixed $asig_id
     */
    public function setAsigId($asig_id)
    {
        $this->asig_id = $asig_id;
    }


}