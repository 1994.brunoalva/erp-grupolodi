<?php


class ClienteSaldo
{
    private $cli_sal_id;
    private $cliete_id;
    private $clie_saldo;
    private $estado;

    /**
     * @return mixed
     */
    public function getCliSalId()
    {
        return $this->cli_sal_id;
    }

    /**
     * @param mixed $cli_sal_id
     */
    public function setCliSalId($cli_sal_id)
    {
        $this->cli_sal_id = $cli_sal_id;
    }

    /**
     * @return mixed
     */
    public function getClieteId()
    {
        return $this->cliete_id;
    }

    /**
     * @param mixed $cliete_id
     */
    public function setClieteId($cliete_id)
    {
        $this->cliete_id = $cliete_id;
    }

    /**
     * @return mixed
     */
    public function getClieSaldo()
    {
        return $this->clie_saldo;
    }

    /**
     * @param mixed $clie_saldo
     */
    public function setClieSaldo($clie_saldo)
    {
        $this->clie_saldo = $clie_saldo;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }
}