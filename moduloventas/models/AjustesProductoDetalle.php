<?php


class AjustesProductoDetalle
{
    private $ajus_det_id;
    private $ajus_id;
    private $prod_empr_id;
    private $cnt_actual;
    private $nuevo_cnt;
    private $estado;

    /**
     * @return mixed
     */
    public function getAjusDetId()
    {
        return $this->ajus_det_id;
    }

    /**
     * @param mixed $ajus_det_id
     */
    public function setAjusDetId($ajus_det_id)
    {
        $this->ajus_det_id = $ajus_det_id;
    }

    /**
     * @return mixed
     */
    public function getAjusId()
    {
        return $this->ajus_id;
    }

    /**
     * @param mixed $ajus_id
     */
    public function setAjusId($ajus_id)
    {
        $this->ajus_id = $ajus_id;
    }

    /**
     * @return mixed
     */
    public function getProdEmprId()
    {
        return $this->prod_empr_id;
    }

    /**
     * @param mixed $prod_empr_id
     */
    public function setProdEmprId($prod_empr_id)
    {
        $this->prod_empr_id = $prod_empr_id;
    }

    /**
     * @return mixed
     */
    public function getCntActual()
    {
        return $this->cnt_actual;
    }

    /**
     * @param mixed $cnt_actual
     */
    public function setCntActual($cnt_actual)
    {
        $this->cnt_actual = $cnt_actual;
    }

    /**
     * @return mixed
     */
    public function getNuevoCnt()
    {
        return $this->nuevo_cnt;
    }

    /**
     * @param mixed $nuevo_cnt
     */
    public function setNuevoCnt($nuevo_cnt)
    {
        $this->nuevo_cnt = $nuevo_cnt;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}