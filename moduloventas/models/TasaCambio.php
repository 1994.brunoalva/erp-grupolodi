<?php


class TasaCambio
{
    private $tas_id;
    private $tas_sunat_venta;
    private $tas_sunat_compra;
    private $tas_banco_venta;
    private $tas_banco_compra;
    private $tas_comercial_venta;
    private $tas_fecha;

    /**
     * @return mixed
     */
    public function getTasId()
    {
        return $this->tas_id;
    }

    /**
     * @param mixed $tas_id
     */
    public function setTasId($tas_id)
    {
        $this->tas_id = $tas_id;
    }

    /**
     * @return mixed
     */
    public function getTasSunatVenta()
    {
        return $this->tas_sunat_venta;
    }

    /**
     * @param mixed $tas_sunat_venta
     */
    public function setTasSunatVenta($tas_sunat_venta)
    {
        $this->tas_sunat_venta = $tas_sunat_venta;
    }

    /**
     * @return mixed
     */
    public function getTasSunatCompra()
    {
        return $this->tas_sunat_compra;
    }

    /**
     * @param mixed $tas_sunat_compra
     */
    public function setTasSunatCompra($tas_sunat_compra)
    {
        $this->tas_sunat_compra = $tas_sunat_compra;
    }

    /**
     * @return mixed
     */
    public function getTasBancoVenta()
    {
        return $this->tas_banco_venta;
    }

    /**
     * @param mixed $tas_banco_venta
     */
    public function setTasBancoVenta($tas_banco_venta)
    {
        $this->tas_banco_venta = $tas_banco_venta;
    }

    /**
     * @return mixed
     */
    public function getTasBancoCompra()
    {
        return $this->tas_banco_compra;
    }

    /**
     * @param mixed $tas_banco_compra
     */
    public function setTasBancoCompra($tas_banco_compra)
    {
        $this->tas_banco_compra = $tas_banco_compra;
    }

    /**
     * @return mixed
     */
    public function getTasComercialVenta()
    {
        return $this->tas_comercial_venta;
    }

    /**
     * @param mixed $tas_comercial_venta
     */
    public function setTasComercialVenta($tas_comercial_venta)
    {
        $this->tas_comercial_venta = $tas_comercial_venta;
    }

    /**
     * @return mixed
     */
    public function getTasFecha()
    {
        return $this->tas_fecha;
    }

    /**
     * @param mixed $tas_fecha
     */
    public function setTasFecha($tas_fecha)
    {
        $this->tas_fecha = $tas_fecha;
    }




}