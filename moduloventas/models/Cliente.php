<?php


class Cliente
{
    private $cli_id;
    private $cli_tdoc;
    private $cli_ndoc;
    private $cli_nomape;
    private $cli_direc;
    private $cli_tele;
    private $cli_tele2;
    private $cli_conta;
    private $cli_mail1;
    private $cli_mail2;
    private $cli_clasi;
    private $cli_vende;
    private $cli_estad;
    private $cli_condi;
    private $dep_cod;
    private $pro_cod;
    private $dis_cod;
    private $ubi_cod;

    /**
     * Cliente constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getCliId()
    {
        return $this->cli_id;
    }

    /**
     * @param mixed $cli_id
     */
    public function setCliId($cli_id)
    {
        $this->cli_id = $cli_id;
    }

    /**
     * @return mixed
     */
    public function getCliTdoc()
    {
        return $this->cli_tdoc;
    }

    /**
     * @param mixed $cli_tdoc
     */
    public function setCliTdoc($cli_tdoc)
    {
        $this->cli_tdoc = $cli_tdoc;
    }

    /**
     * @return mixed
     */
    public function getCliNdoc()
    {
        return $this->cli_ndoc;
    }

    /**
     * @param mixed $cli_ndoc
     */
    public function setCliNdoc($cli_ndoc)
    {
        $this->cli_ndoc = $cli_ndoc;
    }

    /**
     * @return mixed
     */
    public function getCliNomape()
    {
        return $this->cli_nomape;
    }

    /**
     * @param mixed $cli_nomape
     */
    public function setCliNomape($cli_nomape)
    {
        $this->cli_nomape = $cli_nomape;
    }

    /**
     * @return mixed
     */
    public function getCliDirec()
    {
        return $this->cli_direc;
    }

    /**
     * @param mixed $cli_direc
     */
    public function setCliDirec($cli_direc)
    {
        $this->cli_direc = $cli_direc;
    }

    /**
     * @return mixed
     */
    public function getCliTele()
    {
        return $this->cli_tele;
    }

    /**
     * @param mixed $cli_tele
     */
    public function setCliTele($cli_tele)
    {
        $this->cli_tele = $cli_tele;
    }

    /**
     * @return mixed
     */
    public function getCliTele2()
    {
        return $this->cli_tele2;
    }

    /**
     * @param mixed $cli_tele2
     */
    public function setCliTele2($cli_tele2)
    {
        $this->cli_tele2 = $cli_tele2;
    }

    /**
     * @return mixed
     */
    public function getCliConta()
    {
        return $this->cli_conta;
    }

    /**
     * @param mixed $cli_conta
     */
    public function setCliConta($cli_conta)
    {
        $this->cli_conta = $cli_conta;
    }

    /**
     * @return mixed
     */
    public function getCliMail1()
    {
        return $this->cli_mail1;
    }

    /**
     * @param mixed $cli_mail1
     */
    public function setCliMail1($cli_mail1)
    {
        $this->cli_mail1 = $cli_mail1;
    }

    /**
     * @return mixed
     */
    public function getCliMail2()
    {
        return $this->cli_mail2;
    }

    /**
     * @param mixed $cli_mail2
     */
    public function setCliMail2($cli_mail2)
    {
        $this->cli_mail2 = $cli_mail2;
    }

    /**
     * @return mixed
     */
    public function getCliClasi()
    {
        return $this->cli_clasi;
    }

    /**
     * @param mixed $cli_clasi
     */
    public function setCliClasi($cli_clasi)
    {
        $this->cli_clasi = $cli_clasi;
    }

    /**
     * @return mixed
     */
    public function getCliVende()
    {
        return $this->cli_vende;
    }

    /**
     * @param mixed $cli_vende
     */
    public function setCliVende($cli_vende)
    {
        $this->cli_vende = $cli_vende;
    }

    /**
     * @return mixed
     */
    public function getCliEstad()
    {
        return $this->cli_estad;
    }

    /**
     * @param mixed $cli_estad
     */
    public function setCliEstad($cli_estad)
    {
        $this->cli_estad = $cli_estad;
    }

    /**
     * @return mixed
     */
    public function getCliCondi()
    {
        return $this->cli_condi;
    }

    /**
     * @param mixed $cli_condi
     */
    public function setCliCondi($cli_condi)
    {
        $this->cli_condi = $cli_condi;
    }

    /**
     * @return mixed
     */
    public function getDepCod()
    {
        return $this->dep_cod;
    }

    /**
     * @param mixed $dep_cod
     */
    public function setDepCod($dep_cod)
    {
        $this->dep_cod = $dep_cod;
    }

    /**
     * @return mixed
     */
    public function getProCod()
    {
        return $this->pro_cod;
    }

    /**
     * @param mixed $pro_cod
     */
    public function setProCod($pro_cod)
    {
        $this->pro_cod = $pro_cod;
    }

    /**
     * @return mixed
     */
    public function getDisCod()
    {
        return $this->dis_cod;
    }

    /**
     * @param mixed $dis_cod
     */
    public function setDisCod($dis_cod)
    {
        $this->dis_cod = $dis_cod;
    }

    /**
     * @return mixed
     */
    public function getUbiCod()
    {
        return $this->ubi_cod;
    }

    /**
     * @param mixed $ubi_cod
     */
    public function setUbiCod($ubi_cod)
    {
        $this->ubi_cod = $ubi_cod;
    }


}