<?php


class Egresos
{
    private $egr_id;
    private $egr_descrip;
    private $egr_monto;
    private $sun_id;
    private $egr_fecha;
    private $egr_numope;
    private $egr_tipo;
    private $egr_estatus;
    private $egr_numref;
    private $egr_tref;
    private $emp_id;

    /**
     * @return mixed
     */
    public function getEgrId()
    {
        return $this->egr_id;
    }

    /**
     * @param mixed $egr_id
     */
    public function setEgrId($egr_id)
    {
        $this->egr_id = $egr_id;
    }

    /**
     * @return mixed
     */
    public function getEgrDescrip()
    {
        return $this->egr_descrip;
    }

    /**
     * @param mixed $egr_descrip
     */
    public function setEgrDescrip($egr_descrip)
    {
        $this->egr_descrip = $egr_descrip;
    }

    /**
     * @return mixed
     */
    public function getEgrMonto()
    {
        return $this->egr_monto;
    }

    /**
     * @param mixed $egr_monto
     */
    public function setEgrMonto($egr_monto)
    {
        $this->egr_monto = $egr_monto;
    }

    /**
     * @return mixed
     */
    public function getSunId()
    {
        return $this->sun_id;
    }

    /**
     * @param mixed $sun_id
     */
    public function setSunId($sun_id)
    {
        $this->sun_id = $sun_id;
    }

    /**
     * @return mixed
     */
    public function getEgrFecha()
    {
        return $this->egr_fecha;
    }

    /**
     * @param mixed $egr_fecha
     */
    public function setEgrFecha($egr_fecha)
    {
        $this->egr_fecha = $egr_fecha;
    }

    /**
     * @return mixed
     */
    public function getEgrNumope()
    {
        return $this->egr_numope;
    }

    /**
     * @param mixed $egr_numope
     */
    public function setEgrNumope($egr_numope)
    {
        $this->egr_numope = $egr_numope;
    }

    /**
     * @return mixed
     */
    public function getEgrTipo()
    {
        return $this->egr_tipo;
    }

    /**
     * @param mixed $egr_tipo
     */
    public function setEgrTipo($egr_tipo)
    {
        $this->egr_tipo = $egr_tipo;
    }

    /**
     * @return mixed
     */
    public function getEgrEstatus()
    {
        return $this->egr_estatus;
    }

    /**
     * @param mixed $egr_estatus
     */
    public function setEgrEstatus($egr_estatus)
    {
        $this->egr_estatus = $egr_estatus;
    }

    /**
     * @return mixed
     */
    public function getEgrNumref()
    {
        return $this->egr_numref;
    }

    /**
     * @param mixed $egr_numref
     */
    public function setEgrNumref($egr_numref)
    {
        $this->egr_numref = $egr_numref;
    }

    /**
     * @return mixed
     */
    public function getEgrTref()
    {
        return $this->egr_tref;
    }

    /**
     * @param mixed $egr_tref
     */
    public function setEgrTref($egr_tref)
    {
        $this->egr_tref = $egr_tref;
    }

    /**
     * @return mixed
     */
    public function getEmpId()
    {
        return $this->emp_id;
    }

    /**
     * @param mixed $emp_id
     */
    public function setEmpId($emp_id)
    {
        $this->emp_id = $emp_id;
    }



}