<?php


class GuiaConductor
{
    private $condu_id;
    private $guia_remi_id;
    private $tipo_doc;
    private $numero_documento;
    private $numero_licencia;

    /**
     * @return mixed
     */
    public function getConduId()
    {
        return $this->condu_id;
    }

    /**
     * @param mixed $condu_id
     */
    public function setConduId($condu_id)
    {
        $this->condu_id = $condu_id;
    }

    /**
     * @return mixed
     */
    public function getGuiaRemiId()
    {
        return $this->guia_remi_id;
    }

    /**
     * @param mixed $guia_remi_id
     */
    public function setGuiaRemiId($guia_remi_id)
    {
        $this->guia_remi_id = $guia_remi_id;
    }

    /**
     * @return mixed
     */
    public function getTipoDoc()
    {
        return $this->tipo_doc;
    }

    /**
     * @param mixed $tipo_doc
     */
    public function setTipoDoc($tipo_doc)
    {
        $this->tipo_doc = $tipo_doc;
    }

    /**
     * @return mixed
     */
    public function getNumeroDocumento()
    {
        return $this->numero_documento;
    }

    /**
     * @param mixed $numero_documento
     */
    public function setNumeroDocumento($numero_documento)
    {
        $this->numero_documento = $numero_documento;
    }

    /**
     * @return mixed
     */
    public function getNumeroLicencia()
    {
        return $this->numero_licencia;
    }

    /**
     * @param mixed $numero_licencia
     */
    public function setNumeroLicencia($numero_licencia)
    {
        $this->numero_licencia = $numero_licencia;
    }

}