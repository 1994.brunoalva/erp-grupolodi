<?php


class GuiaRemisionDetalle
{

    private $gui_deta_id;
    private $gui_re_id;
    private $produ_id;
    private $unidad;
    private $cantidad;

    /**
     * @return mixed
     */
    public function getUnidad()
    {
        return $this->unidad;
    }

    /**
     * @param mixed $unidad
     */
    public function setUnidad($unidad)
    {
        $this->unidad = $unidad;
    }

    /**
     * @return mixed
     */
    public function getGuiDetaId()
    {
        return $this->gui_deta_id;
    }

    /**
     * @param mixed $gui_deta_id
     */
    public function setGuiDetaId($gui_deta_id)
    {
        $this->gui_deta_id = $gui_deta_id;
    }

    /**
     * @return mixed
     */
    public function getGuiReId()
    {
        return $this->gui_re_id;
    }

    /**
     * @param mixed $gui_re_id
     */
    public function setGuiReId($gui_re_id)
    {
        $this->gui_re_id = $gui_re_id;
    }

    /**
     * @return mixed
     */
    public function getProduId()
    {
        return $this->produ_id;
    }

    /**
     * @param mixed $produ_id
     */
    public function setProduId($produ_id)
    {
        $this->produ_id = $produ_id;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }



}