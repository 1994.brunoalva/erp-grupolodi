<?php


class Empresa
{
    private $emp_id;
    private $emp_ruc;
    private $emp_nombre;
    private $emp_telefono;
    private $emp_direccion;
    private $emp_logo;
    private $emp_estatus;

    /**
     * @return mixed
     */
    public function getEmpId()
    {
        return $this->emp_id;
    }

    /**
     * @param mixed $emp_id
     */
    public function setEmpId($emp_id)
    {
        $this->emp_id = $emp_id;
    }

    /**
     * @return mixed
     */
    public function getEmpRuc()
    {
        return $this->emp_ruc;
    }

    /**
     * @param mixed $emp_ruc
     */
    public function setEmpRuc($emp_ruc)
    {
        $this->emp_ruc = $emp_ruc;
    }

    /**
     * @return mixed
     */
    public function getEmpNombre()
    {
        return $this->emp_nombre;
    }

    /**
     * @param mixed $emp_nombre
     */
    public function setEmpNombre($emp_nombre)
    {
        $this->emp_nombre = $emp_nombre;
    }

    /**
     * @return mixed
     */
    public function getEmpTelefono()
    {
        return $this->emp_telefono;
    }

    /**
     * @param mixed $emp_telefono
     */
    public function setEmpTelefono($emp_telefono)
    {
        $this->emp_telefono = $emp_telefono;
    }

    /**
     * @return mixed
     */
    public function getEmpDireccion()
    {
        return $this->emp_direccion;
    }

    /**
     * @param mixed $emp_direccion
     */
    public function setEmpDireccion($emp_direccion)
    {
        $this->emp_direccion = $emp_direccion;
    }

    /**
     * @return mixed
     */
    public function getEmpLogo()
    {
        return $this->emp_logo;
    }

    /**
     * @param mixed $emp_logo
     */
    public function setEmpLogo($emp_logo)
    {
        $this->emp_logo = $emp_logo;
    }

    /**
     * @return mixed
     */
    public function getEmpEstatus()
    {
        return $this->emp_estatus;
    }

    /**
     * @param mixed $emp_estatus
     */
    public function setEmpEstatus($emp_estatus)
    {
        $this->emp_estatus = $emp_estatus;
    }



}