<?php


class DevolucionesDetalle
{
    private $devo_detalle_id;
    private $devo_id;
    private $prod_empre_id;
    private $cantidad;
    private $estado;
    private $ubica;

    /**
     * @return mixed
     */
    public function getUbica()
    {
        return $this->ubica;
    }

    /**
     * @param mixed $ubica
     */
    public function setUbica($ubica)
    {
        $this->ubica = $ubica;
    }

    /**
     * @return mixed
     */
    public function getDevoDetalleId()
    {
        return $this->devo_detalle_id;
    }

    /**
     * @param mixed $devo_detalle_id
     */
    public function setDevoDetalleId($devo_detalle_id)
    {
        $this->devo_detalle_id = $devo_detalle_id;
    }

    /**
     * @return mixed
     */
    public function getDevoId()
    {
        return $this->devo_id;
    }

    /**
     * @param mixed $devo_id
     */
    public function setDevoId($devo_id)
    {
        $this->devo_id = $devo_id;
    }

    /**
     * @return mixed
     */
    public function getProdEmpreId()
    {
        return $this->prod_empre_id;
    }

    /**
     * @param mixed $prod_empre_id
     */
    public function setProdEmpreId($prod_empre_id)
    {
        $this->prod_empre_id = $prod_empre_id;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }



}