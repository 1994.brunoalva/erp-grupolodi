<?php


class GuiaRemision
{

    private $gui_re_id;
    private $fecha;
    private $cliente_id;
    private $empresa_id;
    private $serie;
    private $numero;
    private $direccion;
    private $motivo;
    private $agencia_id;
    private $tipo_transporte;
    private $peso_cargamento;
    private $fact_rela;
    private $num_placa;
    private $marca;
    private $modelo;
    private $doc_conduc;
    private $nun_doc_conduc;
    private $num_licen;
    private $estado;

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getEmpresaId()
    {
        return $this->empresa_id;
    }

    /**
     * @param mixed $empresa_id
     */
    public function setEmpresaId($empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return mixed
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * @param mixed $serie
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getGuiReId()
    {
        return $this->gui_re_id;
    }

    /**
     * @param mixed $gui_re_id
     */
    public function setGuiReId($gui_re_id)
    {
        $this->gui_re_id = $gui_re_id;
    }

    /**
     * @return mixed
     */
    public function getClienteId()
    {
        return $this->cliente_id;
    }

    /**
     * @param mixed $cliente_id
     */
    public function setClienteId($cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * @param mixed $motivo
     */
    public function setMotivo($motivo)
    {
        $this->motivo = $motivo;
    }

    /**
     * @return mixed
     */
    public function getAgenciaId()
    {
        return $this->agencia_id;
    }

    /**
     * @param mixed $agencia_id
     */
    public function setAgenciaId($agencia_id)
    {
        $this->agencia_id = $agencia_id;
    }

    /**
     * @return mixed
     */
    public function getTipoTransporte()
    {
        return $this->tipo_transporte;
    }

    /**
     * @param mixed $tipo_transporte
     */
    public function setTipoTransporte($tipo_transporte)
    {
        $this->tipo_transporte = $tipo_transporte;
    }

    /**
     * @return mixed
     */
    public function getPesoCargamento()
    {
        return $this->peso_cargamento;
    }

    /**
     * @param mixed $peso_cargamento
     */
    public function setPesoCargamento($peso_cargamento)
    {
        $this->peso_cargamento = $peso_cargamento;
    }

    /**
     * @return mixed
     */
    public function getFactRela()
    {
        return $this->fact_rela;
    }

    /**
     * @param mixed $fact_rela
     */
    public function setFactRela($fact_rela)
    {
        $this->fact_rela = $fact_rela;
    }

    /**
     * @return mixed
     */
    public function getNumPlaca()
    {
        return $this->num_placa;
    }

    /**
     * @param mixed $num_placa
     */
    public function setNumPlaca($num_placa)
    {
        $this->num_placa = $num_placa;
    }

    /**
     * @return mixed
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * @param mixed $marca
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;
    }

    /**
     * @return mixed
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * @param mixed $modelo
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }

    /**
     * @return mixed
     */
    public function getDocConduc()
    {
        return $this->doc_conduc;
    }

    /**
     * @param mixed $doc_conduc
     */
    public function setDocConduc($doc_conduc)
    {
        $this->doc_conduc = $doc_conduc;
    }

    /**
     * @return mixed
     */
    public function getNunDocConduc()
    {
        return $this->nun_doc_conduc;
    }

    /**
     * @param mixed $nun_doc_conduc
     */
    public function setNunDocConduc($nun_doc_conduc)
    {
        $this->nun_doc_conduc = $nun_doc_conduc;
    }

    /**
     * @return mixed
     */
    public function getNumLicen()
    {
        return $this->num_licen;
    }

    /**
     * @param mixed $num_licen
     */
    public function setNumLicen($num_licen)
    {
        $this->num_licen = $num_licen;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}