<?php


class RepresentantesLegales
{
    private $rep_id;
    private $rep_ruc;
    private $rep_tdoc;
    private $rep_ndoc;
    private $rep_nomape;
    private $rep_cargo;
    private $rep_desde;

    /**
     * @return mixed
     */
    public function getRepId()
    {
        return $this->rep_id;
    }

    /**
     * @param mixed $rep_id
     */
    public function setRepId($rep_id)
    {
        $this->rep_id = $rep_id;
    }

    /**
     * @return mixed
     */
    public function getRepRuc()
    {
        return $this->rep_ruc;
    }

    /**
     * @param mixed $rep_ruc
     */
    public function setRepRuc($rep_ruc)
    {
        $this->rep_ruc = $rep_ruc;
    }

    /**
     * @return mixed
     */
    public function getRepTdoc()
    {
        return $this->rep_tdoc;
    }

    /**
     * @param mixed $rep_tdoc
     */
    public function setRepTdoc($rep_tdoc)
    {
        $this->rep_tdoc = $rep_tdoc;
    }

    /**
     * @return mixed
     */
    public function getRepNdoc()
    {
        return $this->rep_ndoc;
    }

    /**
     * @param mixed $rep_ndoc
     */
    public function setRepNdoc($rep_ndoc)
    {
        $this->rep_ndoc = $rep_ndoc;
    }

    /**
     * @return mixed
     */
    public function getRepNomape()
    {
        return $this->rep_nomape;
    }

    /**
     * @param mixed $rep_nomape
     */
    public function setRepNomape($rep_nomape)
    {
        $this->rep_nomape = $rep_nomape;
    }

    /**
     * @return mixed
     */
    public function getRepCargo()
    {
        return $this->rep_cargo;
    }

    /**
     * @param mixed $rep_cargo
     */
    public function setRepCargo($rep_cargo)
    {
        $this->rep_cargo = $rep_cargo;
    }

    /**
     * @return mixed
     */
    public function getRepDesde()
    {
        return $this->rep_desde;
    }

    /**
     * @param mixed $rep_desde
     */
    public function setRepDesde($rep_desde)
    {
        $this->rep_desde = $rep_desde;
    }

}