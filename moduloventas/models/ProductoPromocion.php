<?php


class ProductoPromocion
{
    private $prod_promo_id;
    private $prod_emp_id;
    private $prod_id;
    private $emp_id;
    private $precio;
    private $cantidad;
    private $estado;

    /**
     * @return mixed
     */
    public function getProdPromoId()
    {
        return $this->prod_promo_id;
    }

    /**
     * @param mixed $prod_promo_id
     */
    public function setProdPromoId($prod_promo_id)
    {
        $this->prod_promo_id = $prod_promo_id;
    }

    /**
     * @return mixed
     */
    public function getProdEmpId()
    {
        return $this->prod_emp_id;
    }

    /**
     * @param mixed $prod_emp_id
     */
    public function setProdEmpId($prod_emp_id)
    {
        $this->prod_emp_id = $prod_emp_id;
    }

    /**
     * @return mixed
     */
    public function getProdId()
    {
        return $this->prod_id;
    }

    /**
     * @param mixed $prod_id
     */
    public function setProdId($prod_id)
    {
        $this->prod_id = $prod_id;
    }

    /**
     * @return mixed
     */
    public function getEmpId()
    {
        return $this->emp_id;
    }

    /**
     * @param mixed $emp_id
     */
    public function setEmpId($emp_id)
    {
        $this->emp_id = $emp_id;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}