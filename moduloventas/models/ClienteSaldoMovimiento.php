<?php


class ClienteSaldoMovimiento
{
    private $mov_id;
    private $clie_sal_id;
    private $detalle;
    private $tipo_mov;
    private $monto;
    private $num_ref;
    private $tipo_doc;

    /**
     * @return mixed
     */
    public function getTipoMov()
    {
        return $this->tipo_mov;
    }

    /**
     * @param mixed $tipo_mov
     */
    public function setTipoMov($tipo_mov)
    {
        $this->tipo_mov = $tipo_mov;
    }

    /**
     * @return mixed
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * @param mixed $monto
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;
    }

    /**
     * @return mixed
     */
    public function getMovId()
    {
        return $this->mov_id;
    }

    /**
     * @param mixed $mov_id
     */
    public function setMovId($mov_id)
    {
        $this->mov_id = $mov_id;
    }

    /**
     * @return mixed
     */
    public function getClieSalId()
    {
        return $this->clie_sal_id;
    }

    /**
     * @param mixed $clie_sal_id
     */
    public function setClieSalId($clie_sal_id)
    {
        $this->clie_sal_id = $clie_sal_id;
    }

    /**
     * @return mixed
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * @param mixed $detalle
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;
    }

    /**
     * @return mixed
     */
    public function getNumRef()
    {
        return $this->num_ref;
    }

    /**
     * @param mixed $num_ref
     */
    public function setNumRef($num_ref)
    {
        $this->num_ref = $num_ref;
    }

    /**
     * @return mixed
     */
    public function getTipoDoc()
    {
        return $this->tipo_doc;
    }

    /**
     * @param mixed $tipo_doc
     */
    public function setTipoDoc($tipo_doc)
    {
        $this->tipo_doc = $tipo_doc;
    }



}