<?php


class Devoluciones
{
    private $devo_id;
    private $emp_id;
    private $fac_id;
    private $tipo_doc;
    private $num_doc;
    private $fecha;
    private $observaciones;
    private $estado;
    private $ubicacion;

    /**
     * @return mixed
     */
    public function getUbicacion()
    {
        return $this->ubicacion;
    }

    /**
     * @param mixed $ubicacion
     */
    public function setUbicacion($ubicacion)
    {
        $this->ubicacion = $ubicacion;
    }


    /**
     * @return mixed
     */
    public function getDevoId()
    {
        return $this->devo_id;
    }

    /**
     * @param mixed $devo_id
     */
    public function setDevoId($devo_id)
    {
        $this->devo_id = $devo_id;
    }

    /**
     * @return mixed
     */
    public function getEmpId()
    {
        return $this->emp_id;
    }

    /**
     * @param mixed $emp_id
     */
    public function setEmpId($emp_id)
    {
        $this->emp_id = $emp_id;
    }

    /**
     * @return mixed
     */
    public function getFacId()
    {
        return $this->fac_id;
    }

    /**
     * @param mixed $fac_id
     */
    public function setFacId($fac_id)
    {
        $this->fac_id = $fac_id;
    }

    /**
     * @return mixed
     */
    public function getTipoDoc()
    {
        return $this->tipo_doc;
    }

    /**
     * @param mixed $tipo_doc
     */
    public function setTipoDoc($tipo_doc)
    {
        $this->tipo_doc = $tipo_doc;
    }

    /**
     * @return mixed
     */
    public function getNumDoc()
    {
        return $this->num_doc;
    }

    /**
     * @param mixed $num_doc
     */
    public function setNumDoc($num_doc)
    {
        $this->num_doc = $num_doc;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * @param mixed $observaciones
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }



}