<?php
$indexRuta=1;
require '../conexion/Conexion.php';


$conexionp =  (new Conexion())->getConexion();

$nombremodule="Agencia de transporte";

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../public/css/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../public/plugins/sweetalert2/sweetalert2.min.css">

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }
        .onclick-mouse:hover{
            cursor: pointer;
        }



        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/

    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Agencia de transporte</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <button type="button"   class="btn btn-primary" data-toggle="modal" data-target="#modal_agregar_agencia_transporte">
                                            <i class="fa fa-plus "></i> Nuevo
                                        </button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <table id="table-agencia-transporte" class="table table-striped table-bordered table-hover">
                                    <thead class="bg-head-table">
                                    <tr style="background-color: #007ac3; color: white">
                                        <th style="border-right-color: #007ac3" class="text-center"></th>
                                        <th style="border-right-color: #007ac3" class="text-center">NRO. RUC</th>
                                        <th style="border-right-color: #007ac3" class="text-center">RAZON SOCIAL</th>
                                        <th style="border-right-color: #007ac3" class="text-center">DIRECCION</th>
                                        <th style="border-right-color: #007ac3" class="text-center">TELEFONO</th>
                                        <th class="text-center">OPCION</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>

                            </div>

                            <div  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div id="content-agencia-transporte">

                                    <div id="modal_agregar_agencia_transporte" tabindex="-1" role="dialog"
                                         aria-hidden="true" class="modal fade" style="display: none;">
                                        <div role="document" class="modal-dialog" style="width: 75%;">
                                            <div class="modal-content">
                                                <div class="modal-header no-border no-padding">
                                                    <div class="modal-header text-center color-modal-header"><h3
                                                                class="modal-title">Registrar Agencia De Transporte</h3>
                                                    </div>
                                                </div>
                                                <div class="modal-body  no-border">
                                                    <form v-on:submit.prevent="gregistrar">
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                                                    <label class="col-xs-12 no-padding">RUC:</label>
                                                                    <div class="input-group col-xs-12 no-padding">
                                                                        <input  v-model="dataR.ruc"
                                                                                required type="text"
                                                                                class="form-control"> <span
                                                                                class="input-group-btn"><button
                                                                                    v-on:click="consultaRUC(1)"
                                                                                    type="button"
                                                                                    class="btn btn-primary"><i
                                                                                        class="fa fa-search"></i></button></span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                                                    <label class="col-xs-12 no-padding">NOMBRE / RAZON
                                                                        SOCIAL :</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input required  v-model="dataR.razon"
                                                                                required="required" disabled="disabled"
                                                                                type="text"
                                                                                aria-describedby="basic-addon1" value=""
                                                                                placeholder=""
                                                                                class="form-control input-number"></div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">ESTADO:</label>
                                                                    <div class="input-group col-xs-12"><input
                                                                                v-model="dataR.estado"
                                                                                disabled="disabled" type="text"
                                                                                aria-describedby="basic-addon1" value=""
                                                                                placeholder="" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">CONDICION:</label>
                                                                    <div class="input-group col-xs-12"><input
                                                                                v-model="dataR.condicion"
                                                                                required="required" disabled="disabled"
                                                                                type="text"
                                                                                aria-describedby="basic-addon1" value=""
                                                                                placeholder="" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-5 col-sm-6 col-md-8">
                                                                    <label class="col-xs-12 no-padding">DIRECCION
                                                                        FISCAL:</label>
                                                                    <div class="input-group col-xs-12"><input
                                                                                v-model="dataR.direccion"
                                                                                required="required" type="text"
                                                                                aria-describedby="basic-addon1" value=""
                                                                                placeholder="" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                                                    <label class="col-xs-12 no-padding">TELEFONO:</label>
                                                                    <div class="input-group col-xs-12"><input
                                                                                v-model="dataR.telefono"
                                                                                type="text"
                                                                                aria-describedby="basic-addon1" value=""
                                                                                placeholder=""
                                                                                class="form-control input-number"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    Datos Extra<!--Padding is optional-->
                                                  </span>

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="col-md-5">
                                                                <button type="button" v-on:click="regCarDataR()" style="margin-bottom: 3px;" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>
                                                                <div style="width: 100%;  height: 280px;overflow: auto;">
                                                                    <table style="width:100%" class="table table-hover">
                                                                        <tr >
                                                                            <th>PLACA</th>
                                                                            <th>MARCA</th>
                                                                            <th>MODELO</th>
                                                                            <th></th>
                                                                        </tr>
                                                                        <tr v-for="(item, index) in dataR.listaCarrosCon" v-on:click="seleccionConducR(index)" class="onclick-mouse">
                                                                            <td>{{item.placa}}</td>
                                                                            <td>{{item.marca}}</td>
                                                                            <td>{{item.modelo}}</td>
                                                                            <td><button v-on:click="elimanrRegCarR(index)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
                                                                        </tr>

                                                                        <tr v-if="temRC.isReg" class="onclick-mouse">
                                                                            <td><input style="width: 100px" v-model="temRC.placa"></td>
                                                                            <td><input style="width: 100px" v-model="temRC.marca"></td>
                                                                            <td><input style="width: 100px" v-model="temRC.modelo"></td>
                                                                            <td><button type="button" v-on:click="addCarDataR()" class="btn btn-success"><i class="fa fa-check"></i></button></td>
                                                                        </tr>

                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <button  :disabled="indexTemConR == -1" v-on:click="regConDataR()" type="button" style="margin-bottom: 3px;" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>
                                                                <div style="width: 100%;  height: 280px;overflow: auto;">
                                                                    <table style="width:100%" class="table table-hover">
                                                                        <tr >
                                                                            <th>NOMBRE</th>
                                                                            <th>TI. DOC</th>
                                                                            <th>NRO. DOC.</th>
                                                                            <th>NRO. LICE.</th>
                                                                            <th></th>
                                                                        </tr>
                                                                        <tr v-for="(item, index) in listaConducR" class="onclick-mouse">
                                                                            <td>{{item.nombre}}</td>
                                                                            <td>{{item.doc}}</td>
                                                                            <td>{{item.nro_doc}}</td>
                                                                            <td>{{item.nro_lice}}</td>
                                                                            <td><button v-on:click="elimanrRegConR(index)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
                                                                        </tr>
                                                                        <tr v-if="temRCon.isReg" class="onclick-mouse">
                                                                            <td><input v-model="temRCon.nombre"></td>
                                                                            <td><select v-model="temRCon.doc">
                                                                                    <option value="DNI">DNI</option>
                                                                                </select></td>
                                                                            <td><input style="width: 110px;" v-model="temRCon.nro_doc"></td>
                                                                            <td><input style="width: 110px;" v-model="temRCon.nro_lice"></td>
                                                                            <td><button type="button" v-on:click="addConDataR()" class="btn btn-success"><i class="fa fa-check"></i></button></td>
                                                                        </tr>


                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="container-fluid">
                                                            <hr class="line-frame-modal">
                                                        </div>
                                                        <div class="container-fluid text-right">
                                                            <button type="submit" class="btn btn-primary">
                                                                Agregar
                                                            </button>
                                                            <button type="button" data-dismiss="modal"
                                                                    class="btn btn-success">
                                                                Cerrar
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="modal_edit_agencia_transporte" tabindex="-1" role="dialog"
                                         aria-hidden="true" class="modal fade">
                                        <div role="document" class="modal-dialog" style="width: 75%;">
                                            <div class="modal-content">
                                                <div class="modal-header no-border no-padding">
                                                    <div class="modal-header text-center color-modal-header"><h3
                                                                class="modal-title">Actualizar Agensia De
                                                            Transporte</h3></div>
                                                </div>
                                                <div class="modal-body  no-border">
                                                    <form v-on:submit.prevent="actualizar">
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                                                    <label class="col-xs-12 no-padding">RUC:</label>
                                                                    <div class="input-group col-xs-12 no-padding"><input
                                                                                v-model="dataE.ruc"
                                                                                required="required" type="text"
                                                                                class="form-control"> <span
                                                                                class="input-group-btn"><button
                                                                                    v-on:click="consultaRUC(2)"
                                                                                    type="button"
                                                                                    class="btn btn-primary"><i
                                                                                        class="fa fa-search"></i></button></span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                                                    <label class="col-xs-12 no-padding">NOMBRE / RAZON
                                                                        SOCIAL :</label>
                                                                    <div class="input-group col-xs-12"><input
                                                                                v-model="dataE.razon"
                                                                                required="required" disabled="disabled"
                                                                                type="text"
                                                                                aria-describedby="basic-addon1" value=""
                                                                                placeholder=""
                                                                                class="form-control input-number"></div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">ESTADO:</label>
                                                                    <div class="input-group col-xs-12"><input
                                                                                v-model="dataE.estado"
                                                                                disabled="disabled" type="text"
                                                                                aria-describedby="basic-addon1" value=""
                                                                                placeholder="" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">CONDICION:</label>
                                                                    <div class="input-group col-xs-12"><input
                                                                                v-model="dataE.condicion"
                                                                                required="required" disabled="disabled"
                                                                                type="text"
                                                                                aria-describedby="basic-addon1" value=""
                                                                                placeholder="" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-5 col-sm-6 col-md-8">
                                                                    <label class="col-xs-12 no-padding">DIRECCION
                                                                        FISCAL:</label>
                                                                    <div class="input-group col-xs-12"><input
                                                                                v-model="dataE.direccion"
                                                                                required="required" type="text"
                                                                                aria-describedby="basic-addon1" value=""
                                                                                placeholder="" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                                                    <label class="col-xs-12 no-padding">TELEFONO:</label>
                                                                    <div class="input-group col-xs-12"><input
                                                                                v-model="dataE.telefono"
                                                                                type="text"
                                                                                aria-describedby="basic-addon1" value=""
                                                                                placeholder=""
                                                                                class="form-control input-number"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    Datos Extra<!--Padding is optional-->
                                                  </span>

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="col-md-5">
                                                                <button type="button" v-on:click="regCarDataE()" style="margin-bottom: 3px;" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>
                                                                <div style="width: 100%;  height: 280px;overflow: auto;">
                                                                    <table style="width:100%" class="table table-hover">
                                                                        <tr >
                                                                            <th>PLACA</th>
                                                                            <th>MARCA</th>
                                                                            <th>MODELO</th>
                                                                            <th></th>
                                                                        </tr>
                                                                        <tr v-for="(item, index) in dataE.listaCarrosCon" v-on:click="seleccionConducE(index)" class="onclick-mouse">
                                                                            <td>{{item.nro_placa}}</td>
                                                                            <td>{{item.marca}}</td>
                                                                            <td>{{item.modelo}}</td>
                                                                            <td><button v-on:click="elimanrRegCarE(index)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
                                                                        </tr>

                                                                        <tr v-if="temEC.isReg" class="onclick-mouse">
                                                                            <td><input style="width: 100px" v-model="temEC.placa"></td>
                                                                            <td><input style="width: 100px" v-model="temEC.marca"></td>
                                                                            <td><input style="width: 100px" v-model="temEC.modelo"></td>
                                                                            <td><button style="width: 100px" type="button" v-on:click="addCarDataE()" class="btn btn-success"><i class="fa fa-check"></i></button></td>
                                                                        </tr>

                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <button  :disabled="indexTemConE == -1" v-on:click="regConDataE()" type="button" style="margin-bottom: 3px;" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>
                                                                <div style="width: 100%;  height: 280px;overflow: auto;">
                                                                    <table style="width:100%" class="table table-hover">
                                                                        <tr >
                                                                            <th>NOMBRE</th>
                                                                            <th>TI. DOC</th>
                                                                            <th>NRO. DOC.</th>
                                                                            <th>NRO. LICE.</th>
                                                                            <th></th>
                                                                        </tr>
                                                                        <tr v-for="(item, index) in listaConducE" class="onclick-mouse">
                                                                            <td>{{item.nombre}}</td>
                                                                            <td>{{item.tipo_doc}}</td>
                                                                            <td>{{item.nro_doc}}</td>
                                                                            <td>{{item.nro_licen}}</td>
                                                                            <td><button v-on:click="elimanrRegConE(index)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
                                                                        </tr>
                                                                        <tr v-if="temECon.isReg" class="onclick-mouse">
                                                                            <td><input v-model="temECon.nombre"></td>
                                                                            <td><select v-model="temECon.doc">
                                                                                    <option value="DNI">DNI</option>
                                                                                </select></td>
                                                                            <td><input style="width: 110px;" v-model="temECon.nro_doc"></td>
                                                                            <td><input style="width: 110px;" v-model="temECon.nro_lice"></td>
                                                                            <td><button type="button" v-on:click="addConDataE()" class="btn btn-success"><i class="fa fa-check"></i></button></td>
                                                                        </tr>


                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="container-fluid">
                                                            <hr class="line-frame-modal">
                                                        </div>
                                                        <div class="container-fluid text-right">
                                                            <button type="submit" class="btn btn-primary">
                                                                Actualizar
                                                            </button>
                                                            <button type="button" data-dismiss="modal"
                                                                    class="btn btn-success">
                                                                Cerrar
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>





    <style>
        #img-file-preview-zone{
            -webkit-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            -moz-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            border-radius: 3px;
        }
    </style>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="module" src="../public/js/Input_validate.js"></script>
    <script src="../public/plugins/sweetalert2/vue-swal.js"></script>
    <!--script  type="module" src="../aConfig/scripts/sku.js"></script-->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="../public/js/modal_agencia_transporte.js"></script>
    <script>


        var  fecha =  new Date();




    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>


<script type="text/javascript">


    function getEstados(num){
        var estado="";
        switch (num+"") {
            case '1':
                    estado="NO ENVIADO";
                break;
        }
        return estado;
    }
    var tabla_agencias_transporte;

    APP_agencia_transporte.setFuntionExe(function () {
        tabla_agencias_transporte.ajax.reload();
    });

    $(document).ready(function() {
        tabla_agencias_transporte = $("#table-agencia-transporte").DataTable({
            "processing": true,
            "serverSide": true,
            filter: true,
            "sAjaxSource": "../ServerSide/serversideAgencia.php",
            "dom": 'Bfrtip',
            "order": [[ 0, "desc" ]],
            "buttons": [
                'csv', 'excel'
            ],
            columnDefs:[
                {
                    "targets": 0,
                    "data": "",
                    "render": function ( data, type, row, meta ) {
                        //console.log(meta)
                        return '<span style="display: block;margin: auto;text-align: center; color: white">'+row[0]+'</span>';
                    }
                },
                {
                    "targets": 1,
                    "data": "",
                    "render": function ( data, type, row, meta ) {
                        //console.log(meta)
                        return '<span style="display: block;margin: auto;text-align: center;">'+row[1]+'</span>';
                    }
                },
                {
                    "targets": 2,
                    "data": "",
                    "render": function ( data, type, row, meta ) {
                        //console.log(meta)
                        return '<span style="display: block;margin: auto;text-align: center;">'+row[2]+'</span>';
                    }
                },
                {
                    "targets": 3,
                    "data": "",
                    "render": function ( data, type, row, meta ) {
                        //console.log(meta)
                        return '<span style="display: block;margin: auto;text-align: center;">'+row[3]+'</span>';
                    }
                },
                {
                    "targets": 4,
                    "data": "",
                    "render": function ( data, type, row, meta ) {
                        //console.log(meta)
                        return '<span style="display: block;margin: auto;text-align: center;">'+row[4]+'</span>';
                    }
                },
                {
                    "targets": 5,
                    "data": "id",
                    "render": function ( data, type, row, meta ) {
                        //console.log(meta)
                        return '<button style="display: block;margin: auto"  data-toggle="modal" data-target="#modal_edit_agencia_transporte" onclick="APP_agencia_transporte.setDatoEdit('+row[5]+')"  class="btn btn-sm btn-info fa fa-edit btn-selector-cliente"\n' +
                            '                                                        title="Anadir item" ></button>';
                    }
                }
            ]

        });


    });


</script>

</html>
