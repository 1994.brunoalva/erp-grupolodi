<?php
$indexRuta=1;
require  '../dao/TipoPagoDao.php';
require  '../dao/TipoDePrecioDao.php';
require  '../dao/EmpresaDao.php';

$conexion = (new Conexion())->getConexion();


$tipoPagoDao = new TipoPagoDao();
$tipoDePrecioDao = new TipoDePrecioDao();
$empresaDao = new EmpresaDao();

$departamentos = $conexion->query("SELECT * FROM sys_ven_departamento");
$listaPa= $tipoPagoDao->getdata();
$listaTipoPrecio= $tipoDePrecioDao->getdata();
$listaTemTP = [];

$isCo='true';
$idCoti=0;
$cotizacionActual='';

$listaEmpre = $empresaDao->getLista();

foreach ($listaPa as $item){
    $listaTemTP []= $item;
}

if (isset($_GET['view'])){
    $isCo='false';
    $idCoti = $_GET['view'];
}

$nombremodule = "Guia Remision";

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../public/css/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../public/plugins/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <!--suppress JSAnnotator -->
    <script>
        var isRegister = <?php echo $isCo ?>;
        var idCoti = <?php echo $idCoti ?>;

        var idVenta = -1;
        var isventa=false;


        function openPrint() {
            if (idCoti >0){
                window.open("cotizacion_pdf2.php?coti="+idCoti)
            }else{
                swal('Primero Guarde la cotizacion')
            }

        }
    </script>
    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
        #loader{

            position:absolute;/*agregamos una posición absoluta para que nos permita mover la capa en el espacio del navegador*/

            top:0;/*posicionamiento en Y */

            left:0;/*pocisionamiento en X*/

            z-index:9999; /* Le asignamos la pocisión más alta en el DOM */

            background-color:#ffffff; /* le asignamos un color de fondo */

            width:100%; /* maximo ancho de la pantalla */

            height:100%; /* maxima altura de la pantalla */

            display:block; /* mostramos el layer */

        }

        .preloader {
            width: 70px;
            height: 70px;
            border: 10px solid #eee;
            border-top: 10px solid #666;
            border-radius: 50%;
            animation-name: girar;
            animation-duration: 2s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }
        @keyframes girar {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
    </style>

</head>

<body>

<!--div id="loader">Cargando.........</div-->
<div id="wrapper">
    <?php

    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <input type="hidden" id="input-id-empresa-user" value="3">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                                <strong id="tittle-header-body">Nueva Guia de Remision</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                            <button onclick="$('#guardarGuia').click()"  type="button" class="btn btn-primary"><i
                                                        class="glyphicon glyphicon-floppy-save"></i> Guardar Guia Remision
                                            </button>


                                        <span style="color: white">----------------------------</span>
                                        <a id="btn-salir" href="guia_remision.php" type="reset" class="btn btn-warning"><i
                                                class="glyphicon glyphicon-chevron-left"></i>Salir
                                        </a>
                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <!--<div class="col-lg-6 text-right">
                                        <a href="new-folder.php" id="folder_btn_nuevo_folder" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Folder
                                        </a>

                                    </div>-->

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div>

                                        <input id="fecha"  type="hidden" value="<?php echo date("Y-m-d") ?>" class="form-control  text-center">
                                        <div class="form-group">
                                            <form v-on:submit.prevent="guardarGuiaRemision()">
                                                <input type="submit" id="guardarGuia" style="display: none">
                                                <div class="row">
                                                    <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                                        <label class="col-xs-12 no-padding">EMPRESA:</label>

                                                        <div class="input-group col-xs-12 no-padding">
                                                            <select required v-model="guiaremision.empresa"  class="form-control">
                                                                <?php

                                                                foreach ($listaEmpre as $emp){
                                                                    echo "<option value='{$emp['emp_id']}'>{$emp['emp_nombre']}</option>";
                                                                }

                                                                ?>
                                                            </select>

                                                        </div>
                                                    </div>

                                                    <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                        <label class="col-xs-12 no-padding">FACTURA RELACIONADA</label>
                                                        <div class="input-group col-xs-12">
                                                            <input   @keypress="onlyNumber" v-model="guiaremision.factura"  type="text"   class="form-control" placeholder="Ejem: 000001">
                                                            <span class="input-group-btn">
                                                            <button v-on:click="buscarDocumentoV()" type="button" class="btn btn-primary">
                                                                <i class="fa fa-search"></i></button>
                                                        </span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                                        <label class="col-xs-12 no-padding">CLIENTE:</label>

                                                        <div class="input-group col-xs-12 no-padding">
                                                            <input required v-model="guiaremision.nombre" id="input-coti-cliente"  type="text" class="form-control" autocomplete="off"
                                                                   aria-describedby="basic-addon1"
                                                                   value="" placeholder="">

                                                        </div>
                                                    </div>

                                                    <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                        <label class="col-xs-12 no-padding">NUMERO DOCUMENTO:</label>
                                                        <div class="input-group col-xs-12">
                                                            <input  disabled  v-model="guiaremision.ruc" type="text" class="form-control"
                                                                   aria-describedby="basic-addon1"
                                                                   value="" placeholder="">


                                                        </div>
                                                    </div>
                                                    <div class="form-group col-xs-5 col-sm-7 col-md-4">
                                                        <label class="col-xs-12 no-padding">DIRECCION DE LLEGADA:</label>

                                                        <div class="input-group col-xs-12">
                                                            <select required data-width="100%"  v-model="guiaremision.idDireccion" class="form-control">
                                                                <option v-for="dir in guiaremision.direcciones" v-bind:value="dir.id" >{{dir.direccion}}</option>
                                                            </select>
                                                            <span class="input-group-btn">
                                                                <button v-on:click="aponModalNewDireccion()"   type="button" class="btn btn-primary">
                                                                    <i class="fa fa-plus"></i></button>
                                                            </span>
                                                        </div>



                                                    </div><div class="form-group col-xs-4 col-sm-4 col-md-4">
                                                        <label class="col-xs-12 no-padding">MOTIVO DEL TRASLADO:</label>
                                                        <div class="input-group col-xs-12">
                                                            <select required v-model="guiaremision.motivo"   class="form-control selectpicker">
                                                                <option>Venta</option>
                                                                <option>Compra</option>
                                                                <option>Devolución</option>
                                                                <option>Consignación</option>
                                                                <option>Importación</option>
                                                                <option>Exportación</option>
                                                                <option>Venta sujeta a confirmación</option>
                                                                <option>Traslado entre establecimientos de la misma empresa</option>
                                                                <option>Traslado de bienes para transformación</option>
                                                                <option>Recojo de bienes</option>
                                                                <option>Traslado por emisor itinerante</option>
                                                                <option>Traslado zona primaria</option>
                                                                <option>Venta con entrega a terceros</option>
                                                                <option>Otras no incluida en los puntos anteriores.</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                        <label class="col-xs-12 no-padding">TIPO DE TRANSPORTE:</label>
                                                        <div class="input-group col-xs-12">
                                                            <select required v-model="guiaremision.tpTrans"   class="form-control">
                                                                <option>PUBLICO</option>
                                                                <option>PRIVADO</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                        <label class="col-xs-12 no-padding">PESO DEL CARGAMENTO (KGM):</label>
                                                        <div class="input-group col-xs-12">
                                                            <input disabled guardarGuia @keypress="onlyNumber" v-model="totalPeso"  type="text"   @keypress="onlyNumber" class="form-control">

                                                        </div>
                                                    </div>

                                                    <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                                        <label class="col-xs-12 no-padding">OBSERVACIONES DE LA VENTA:</label>
                                                        <div class="input-group col-xs-12">
                                                            <textarea disabled  id="observacionesVenta"  class="form-control">

                                                            </textarea>

                                                        </div>
                                                    </div>

                                                </div>
                                            </form>



                                        </div>
                                            <div class="row">


                                        </div>

                                        <div class="form-group " style="margin-top: 20px;">
                                            <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    Datos del transportista <!--Padding is optional-->
                                                  </span>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                                <label class="col-xs-12 no-padding">AGENCIA DE TRANSPORTE:</label>

                                                <div class="input-group col-xs-12 no-padding">
                                                    <input v-model="guiaremision.agenciaNom" id="input-agencia-transporte"  type="text" class="form-control" autocomplete="off"
                                                           aria-describedby="basic-addon1"
                                                           value="" placeholder="">
                                                    <span class="input-group-btn">
                                                            <button  data-toggle="modal" data-target="#modal_agregar_agencia_transporte" type="button" class="btn btn-primary">
                                                                <i class="fa fa-plus"></i></button>
                                                        </span>
                                                </div>
                                            </div>
                                            <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                                <label class="col-xs-12 no-padding">RAZON SOCIAL:</label>

                                                <div class="input-group col-xs-12 no-padding">
                                                    <input disabled v-model="guiaremision.razonAgencia"  type="text" class="form-control" autocomplete="off"
                                                              aria-describedby="basic-addon1"
                                                              value="" placeholder="">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                                <label class="col-xs-12 no-padding">NRO. PLACA:</label>

                                                <div class="input-group col-xs-12 no-padding">
                                                    <input  v-model="guiaremision.numPlaca"  type="text" class="form-control" autocomplete="off"
                                                              aria-describedby="basic-addon1"
                                                              value="" placeholder="">
                                                    <span class="input-group-btn">
                                                            <button data-toggle="modal" data-target="#modal_carros_agen"  type="button" class="btn btn-primary">
                                                                <i class="fa fa-eye"></i></button>
                                                        </span>
                                                </div>
                                            </div>
                                            <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                                <label class="col-xs-12 no-padding">MARCA:</label>

                                                <div class="input-group col-xs-12 no-padding">
                                                    <input  v-model="guiaremision.marcaCar"  type="text" class="form-control" autocomplete="off"
                                                              aria-describedby="basic-addon1"
                                                              value="" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                                <label class="col-xs-12 no-padding">MODELO:</label>

                                                <div class="input-group col-xs-12 no-padding">
                                                    <input  v-model="guiaremision.modeloCar"  type="text" class="form-control" autocomplete="off"
                                                              aria-describedby="basic-addon1"
                                                              value="" placeholder="">
                                                </div>
                                            </div>




                                        </div>
                                        <div class="form-group " style="margin-top: 10px;">
                                            <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    Lista de Conductores<!--Padding is optional-->
                                                  </span>

                                            </div>

                                        </div>
                                        <div class="row">
                                            <form v-on:submit.prevent="agregarConductor">
                                                <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                                    <label class="col-xs-12 no-padding">DOCUMENTO:</label>

                                                    <div class="input-group col-xs-12 no-padding">
                                                        <select required v-model="condutoR.docAgencia" id="tipoDocIdentidad" class="form-control">
                                                            <option value="40">DNI</option>
                                                            <option value="41">CARNET DE EXTRANJERIA</option>
                                                            <option value="42">REGISTRO UNICO DE CONTRIBUYENTES</option>
                                                            <option value="43">PASAPORTE</option>
                                                            <option value="A">CEDULA DIPLOMATICA DE IDENTIDAD</option>
                                                            <option value="39">OTROS</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                                    <label class="col-xs-12 no-padding">NRO. DOCUMENTO:</label>

                                                    <div class="input-group col-xs-12 no-padding">
                                                        <input required  v-model="condutoR.numDocConduc"  type="text" class="form-control" autocomplete="off"
                                                                aria-describedby="basic-addon1"
                                                                value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                                    <label class="col-xs-12 no-padding">NRO. LICENCIA:</label>

                                                    <div class="input-group col-xs-12 no-padding">
                                                        <input required  v-model="condutoR.numLicenConduc"  type="text" class="form-control" autocomplete="off"
                                                                aria-describedby="basic-addon1"
                                                                value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                                    <label class="col-xs-12 no-padding">.</label>

                                                    <div class="input-group col-xs-12 no-padding">
                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>
                                                    </div>
                                                </div>
                                            </form>


                                            <div class="form-group col-xs-12">
                                                <table  class="table table-bordered table-hover" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 50px;">#</th>
                                                        <th class="text-center">TIPO DOC.</th>
                                                        <th class="text-center">NRO. DOCUMENTO</th>
                                                        <th class="text-center">NRO. LICENCIA</th>
                                                        <th class="text-center"></th>


                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr v-for="(item, index) in listaConductores">
                                                        <td class="text-center">{{index + 1}}</td>
                                                        <td class="text-center">{{item.docum}}</td>
                                                        <td  class="text-center">{{item.numDocConduc}}</td>
                                                        <td  class="text-center">{{item.numLicenConduc}}</td>
                                                        <td class="text-center"> <button v-on:click="eliminarConduc(index)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button> </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                            <div class="form-group" style="margin-top: 10px;">
                                                <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    Lista de productos<!--Padding is optional-->
                                                  </span>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <form v-on:submit.prevent="agregarProducto()">
                                                    <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                                        <label class="col-xs-12 no-padding">BUSCAR PRODUCTO:</label>
                                                        <div class="input-group col-xs-12">
                                                            <input required v-model="producto.nombre" id="input-buscar-producto"  type="text" class="form-control"
                                                                   aria-describedby="basic-addon1"
                                                                   value="" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                                        <label class="col-xs-12 no-padding">CODIGO:</label>
                                                        <div class="input-group col-xs-12">
                                                            <input v-model="producto.codigo" style="text-align: center" disabled  type="text" class="form-control"
                                                                   aria-describedby="basic-addon1"
                                                                   value="" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                                        <label class="col-xs-12 no-padding">UN. MEDIDA:</label>
                                                        <div class="input-group col-xs-12">
                                                            <input v-model="producto.medida" style="text-align: center" disabled  type="text" class="form-control"
                                                                   aria-describedby="basic-addon1"
                                                                   value="" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                                        <label class="col-xs-12 no-padding">CANTIDAD:</label>
                                                        <div class="input-group col-xs-12">
                                                            <input required v-model="producto.cantidad" style="text-align: center"   type="text" class="form-control"
                                                                   aria-describedby="basic-addon1"  @keypress="onlyNumber"
                                                                   value="" placeholder="">
                                                        </div>
                                                    </div>
                                                    <!--div class="form-group col-xs-7 col-sm-7 col-md-1">
                                                        <label class="col-xs-12 no-padding">DESCUENTO%:</label>
                                                        <div class="input-group col-xs-12">
                                                            <input v-model="producto.descuento" style="text-align: center"   type="text" class="form-control"
                                                                   aria-describedby="basic-addon1"
                                                                   value="" placeholder="%">
                                                        </div>
                                                    </div-->

                                                    <div class="form-group col-xs-7 col-sm-7 col-md-1">
                                                        <label style="color: white" class="col-xs-12 no-padding"></label>
                                                        <div class="input-group col-xs-12">
                                                            <label  class="col-xs-12 no-padding" style="color: white">.</label>
                                                            <button style="margin-bottom: 5px;" type="submit" class="btn btn-primary">AGREGAR</button>


                                                        </div>
                                                    </div>
                                                </form>



                                            </div>
                                            <div class="form-group">
                                                <table id="tabla-poductos-coti" class="table table-bordered table-hover" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 50px;">#</th>
                                                        <th class="col-sm-6 text-center">PROODUCTO</th>
                                                        <th class="col-sm-4 text-center">SKU</th>
                                                        <th class="col-sm-1 text-center">UN. MEDIDA</th>
                                                        <th class="col-sm-1 text-center">CANTIDAD</th>
                                                        <th class="col-sm-1 text-center"></th>


                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr v-for="(prod, index ) in productos" v-on:click="seleccionarProducnto(index)">
                                                        <td class="text-center">{{index+1}}</td>
                                                        <td class="text-center">{{prod.producto}}</td>
                                                        <td class="text-center">{{prod.sku}}</td>
                                                        <td class="text-center">{{prod.medida}}</td>
                                                        <td class="text-center">{{prod.cantidad}}</td>
                                                        <td class="text-center"> <button v-on:click="eliminarProdutoLista(index)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button> </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <div hidden class="form-group text-right">


                                            <div class="col-md-12 no-padding">
                                                <div class="refresh-table">
                                                    <button  type="submit" class="btn btn-primary">Guardar</button>
                                                    <button id="frame-new-folder-btn-limpiar" type="button" class="btn btn-default">Limpiar</button>
                                                    <a href="import.php" id="frame-new-folder-btn-back" type="reset" class="btn btn-warning"><i
                                                            class="glyphicon glyphicon-chevron-left"></i>Salir
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                </div>


                                <div id="modal_carros_agen" tabindex="-1" role="dialog"
                                     aria-hidden="true" class="modal fade">
                                    <div role="document" class="modal-dialog" style="width: 60%;">
                                        <div class="modal-content">
                                            <div class="modal-header no-border no-padding">
                                                <div class="modal-header text-center color-modal-header"><h3
                                                            class="modal-title">Vehículo de la agencia</h3></div>
                                            </div>
                                            <div class="modal-body  no-border">

                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <table class="table table-bordered" style="width:100%">
                                                            <tr>
                                                                <th class="text-center">Nro. Placa</th>
                                                                <th class="text-center">Marca</th>
                                                                <th class="text-center">Modelo</th>
                                                                <th class="text-center"></th>
                                                            </tr>
                                                            <tr v-for="(item, index) in listaCarros">
                                                                <td class="text-center">{{item.nro_placa}}</td>
                                                                <td class="text-center">{{item.marca}}</td>
                                                                <td class="text-center">{{item.modelo}}</td>
                                                                <td class="text-center"><button v-on:click="seleccionCarro(index)" class="btn btn-success"><i class="fa fa-check"></i></button></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="container-fluid">
                                                    <hr class="line-frame-modal">
                                                </div>
                                                <div class="container-fluid text-right">

                                                    <button type="button" data-dismiss="modal"
                                                            class="btn btn-success">
                                                        Cerrar
                                                    </button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>
    <div id="content-agencia-transporte">

        <div id="modal_agregar_agencia_transporte" tabindex="-1" role="dialog"
             aria-hidden="true" class="modal fade" style="display: none;">
            <div role="document" class="modal-dialog" style="width: 75%;">
                <div class="modal-content">
                    <div class="modal-header no-border no-padding">
                        <div class="modal-header text-center color-modal-header"><h3
                                    class="modal-title">Registrar Agencia De Transporte</h3>
                        </div>
                    </div>
                    <div class="modal-body  no-border">
                        <form v-on:submit.prevent="gregistrar">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                        <label class="col-xs-12 no-padding">RUC:</label>
                                        <div class="input-group col-xs-12 no-padding">
                                            <input  v-model="dataR.ruc"
                                                    required type="text"
                                                    class="form-control"> <span
                                                    class="input-group-btn"><button
                                                        v-on:click="consultaRUC(1)"
                                                        type="button"
                                                        class="btn btn-primary"><i
                                                            class="fa fa-search"></i></button></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                        <label class="col-xs-12 no-padding">NOMBRE / RAZON
                                            SOCIAL :</label>
                                        <div class="input-group col-xs-12">
                                            <input required  v-model="dataR.razon"
                                                   required="required" disabled="disabled"
                                                   type="text"
                                                   aria-describedby="basic-addon1" value=""
                                                   placeholder=""
                                                   class="form-control input-number"></div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">ESTADO:</label>
                                        <div class="input-group col-xs-12"><input
                                                    v-model="dataR.estado"
                                                    disabled="disabled" type="text"
                                                    aria-describedby="basic-addon1" value=""
                                                    placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">CONDICION:</label>
                                        <div class="input-group col-xs-12"><input
                                                    v-model="dataR.condicion"
                                                    required="required" disabled="disabled"
                                                    type="text"
                                                    aria-describedby="basic-addon1" value=""
                                                    placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-5 col-sm-6 col-md-8">
                                        <label class="col-xs-12 no-padding">DIRECCION
                                            FISCAL:</label>
                                        <div class="input-group col-xs-12"><input
                                                    v-model="dataR.direccion"
                                                    required="required" type="text"
                                                    aria-describedby="basic-addon1" value=""
                                                    placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                        <label class="col-xs-12 no-padding">TELEFONO:</label>
                                        <div class="input-group col-xs-12"><input
                                                    v-model="dataR.telefono"
                                                    type="text"
                                                    aria-describedby="basic-addon1" value=""
                                                    placeholder=""
                                                    class="form-control input-number"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    Datos Extra<!--Padding is optional-->
                                                  </span>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-5">
                                    <button type="button" v-on:click="regCarDataR()" style="margin-bottom: 3px;" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>
                                    <div style="width: 100%;  height: 280px;overflow: auto;">
                                        <table style="width:100%" class="table table-hover">
                                            <tr >
                                                <th>PLACA</th>
                                                <th>MARCA</th>
                                                <th>MODELO</th>
                                                <th></th>
                                            </tr>
                                            <tr v-for="(item, index) in dataR.listaCarrosCon" v-on:click="seleccionConducR(index)" class="onclick-mouse">
                                                <td>{{item.placa}}</td>
                                                <td>{{item.marca}}</td>
                                                <td>{{item.modelo}}</td>
                                                <td><button v-on:click="elimanrRegCarR(index)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
                                            </tr>

                                            <tr v-if="temRC.isReg" class="onclick-mouse">
                                                <td><input style="width: 100px" v-model="temRC.placa"></td>
                                                <td><input style="width: 100px" v-model="temRC.marca"></td>
                                                <td><input style="width: 100px" v-model="temRC.modelo"></td>
                                                <td><button type="button" v-on:click="addCarDataR()" class="btn btn-success"><i class="fa fa-check"></i></button></td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <button  :disabled="indexTemConR == -1" v-on:click="regConDataR()" type="button" style="margin-bottom: 3px;" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>
                                    <div style="width: 100%;  height: 280px;overflow: auto;">
                                        <table style="width:100%" class="table table-hover">
                                            <tr >
                                                <th>NOMBRE</th>
                                                <th>TI. DOC</th>
                                                <th>NRO. DOC.</th>
                                                <th>NRO. LICE.</th>
                                                <th></th>
                                            </tr>
                                            <tr v-for="(item, index) in listaConducR" class="onclick-mouse">
                                                <td>{{item.nombre}}</td>
                                                <td>{{item.doc}}</td>
                                                <td>{{item.nro_doc}}</td>
                                                <td>{{item.nro_lice}}</td>
                                                <td><button v-on:click="elimanrRegConR(index)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
                                            </tr>
                                            <tr v-if="temRCon.isReg" class="onclick-mouse">
                                                <td><input v-model="temRCon.nombre"></td>
                                                <td><select v-model="temRCon.doc">
                                                        <option value="DNI">DNI</option>
                                                    </select></td>
                                                <td><input style="width: 110px;" v-model="temRCon.nro_doc"></td>
                                                <td><input style="width: 110px;" v-model="temRCon.nro_lice"></td>
                                                <td><button type="button" v-on:click="addConDataR()" class="btn btn-success"><i class="fa fa-check"></i></button></td>
                                            </tr>


                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="container-fluid">
                                <hr class="line-frame-modal">
                            </div>
                            <div class="container-fluid text-right">
                                <button type="submit" class="btn btn-primary">
                                    Agregar
                                </button>
                                <button type="button" data-dismiss="modal"
                                        class="btn btn-success">
                                    Cerrar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_edit_agencia_transporte" tabindex="-1" role="dialog"
             aria-hidden="true" class="modal fade">
            <div role="document" class="modal-dialog" style="width: 75%;">
                <div class="modal-content">
                    <div class="modal-header no-border no-padding">
                        <div class="modal-header text-center color-modal-header"><h3
                                    class="modal-title">Actualizar Agensia De
                                Transporte</h3></div>
                    </div>
                    <div class="modal-body  no-border">
                        <form v-on:submit.prevent="actualizar">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                        <label class="col-xs-12 no-padding">RUC:</label>
                                        <div class="input-group col-xs-12 no-padding"><input
                                                    v-model="dataE.ruc"
                                                    required="required" type="text"
                                                    class="form-control"> <span
                                                    class="input-group-btn"><button
                                                        v-on:click="consultaRUC(2)"
                                                        type="button"
                                                        class="btn btn-primary"><i
                                                            class="fa fa-search"></i></button></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                        <label class="col-xs-12 no-padding">NOMBRE / RAZON
                                            SOCIAL :</label>
                                        <div class="input-group col-xs-12"><input
                                                    v-model="dataE.razon"
                                                    required="required" disabled="disabled"
                                                    type="text"
                                                    aria-describedby="basic-addon1" value=""
                                                    placeholder=""
                                                    class="form-control input-number"></div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">ESTADO:</label>
                                        <div class="input-group col-xs-12"><input
                                                    v-model="dataE.estado"
                                                    disabled="disabled" type="text"
                                                    aria-describedby="basic-addon1" value=""
                                                    placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">CONDICION:</label>
                                        <div class="input-group col-xs-12"><input
                                                    v-model="dataE.condicion"
                                                    required="required" disabled="disabled"
                                                    type="text"
                                                    aria-describedby="basic-addon1" value=""
                                                    placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-5 col-sm-6 col-md-8">
                                        <label class="col-xs-12 no-padding">DIRECCION
                                            FISCAL:</label>
                                        <div class="input-group col-xs-12"><input
                                                    v-model="dataE.direccion"
                                                    required="required" type="text"
                                                    aria-describedby="basic-addon1" value=""
                                                    placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                        <label class="col-xs-12 no-padding">TELEFONO:</label>
                                        <div class="input-group col-xs-12"><input
                                                    v-model="dataE.telefono"
                                                    type="text"
                                                    aria-describedby="basic-addon1" value=""
                                                    placeholder=""
                                                    class="form-control input-number"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    Datos Extra<!--Padding is optional-->
                                                  </span>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-5">
                                    <button type="button" v-on:click="regCarDataE()" style="margin-bottom: 3px;" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>
                                    <div style="width: 100%;  height: 280px;overflow: auto;">
                                        <table style="width:100%" class="table table-hover">
                                            <tr >
                                                <th>PLACA</th>
                                                <th>MARCA</th>
                                                <th>MODELO</th>
                                                <th></th>
                                            </tr>
                                            <tr v-for="(item, index) in dataE.listaCarrosCon" v-on:click="seleccionConducE(index)" class="onclick-mouse">
                                                <td>{{item.nro_placa}}</td>
                                                <td>{{item.marca}}</td>
                                                <td>{{item.modelo}}</td>
                                                <td><button v-on:click="elimanrRegCarE(index)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
                                            </tr>

                                            <tr v-if="temEC.isReg" class="onclick-mouse">
                                                <td><input style="width: 100px" v-model="temEC.placa"></td>
                                                <td><input style="width: 100px" v-model="temEC.marca"></td>
                                                <td><input style="width: 100px" v-model="temEC.modelo"></td>
                                                <td><button style="width: 100px" type="button" v-on:click="addCarDataE()" class="btn btn-success"><i class="fa fa-check"></i></button></td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <button  :disabled="indexTemConE == -1" v-on:click="regConDataE()" type="button" style="margin-bottom: 3px;" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>
                                    <div style="width: 100%;  height: 280px;overflow: auto;">
                                        <table style="width:100%" class="table table-hover">
                                            <tr >
                                                <th>NOMBRE</th>
                                                <th>TI. DOC</th>
                                                <th>NRO. DOC.</th>
                                                <th>NRO. LICE.</th>
                                                <th></th>
                                            </tr>
                                            <tr v-for="(item, index) in listaConducE" class="onclick-mouse">
                                                <td>{{item.nombre}}</td>
                                                <td>{{item.tipo_doc}}</td>
                                                <td>{{item.nro_doc}}</td>
                                                <td>{{item.nro_licen}}</td>
                                                <td><button v-on:click="elimanrRegConE(index)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
                                            </tr>
                                            <tr v-if="temECon.isReg" class="onclick-mouse">
                                                <td><input v-model="temECon.nombre"></td>
                                                <td><select v-model="temECon.doc">
                                                        <option value="DNI">DNI</option>
                                                    </select></td>
                                                <td><input style="width: 110px;" v-model="temECon.nro_doc"></td>
                                                <td><input style="width: 110px;" v-model="temECon.nro_lice"></td>
                                                <td><button type="button" v-on:click="addConDataE()" class="btn btn-success"><i class="fa fa-check"></i></button></td>
                                            </tr>


                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="container-fluid">
                                <hr class="line-frame-modal">
                            </div>
                            <div class="container-fluid text-right">
                                <button type="submit" class="btn btn-primary">
                                    Actualizar
                                </button>
                                <button type="button" data-dismiss="modal"
                                        class="btn btn-success">
                                    Cerrar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div id="modal-direccion" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form v-on:submit.prevent="agregarDireccion">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #0866c6;text-align: center">
                        <h3 class="modal-title" id="exampleModalLabel" style="color: white"> Agregar Direccion</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <label class="col-xs-12 no-padding">Direccion:</label>
                                    <div class="input-group col-xs-12">
                                        <input   type='text' class="form-control"
                                                 v-model="direcData.direccion"
                                                 aria-describedby="basic-addon1"
                                                 required
                                                 value="" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group col-xs-4">
                                    <label class="col-xs-12 no-padding">DEPARTAMENTO:</label>
                                    <div class="input-group col-xs-12">
                                        <select @change="selectDepart2($event)" v-model="direcData.depart" required data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                            <?php
                                            foreach ($departamentos as $dep){
                                                echo "<option value='" .$dep['dep_cod']. "'>" .$dep['dep_nombre']. "</option>";
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-xs-4 ">
                                    <label class="col-xs-12 no-padding">PROVINCIA:</label>
                                    <div class="input-group col-xs-12">
                                        <select required @change="selectProvin2($event)"  v-model="direcData.provin" data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                            <option v-for="item in listProvincias2" :value="item.pro_cod">{{item.pro_nombre}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-xs-4">
                                    <label class="col-xs-12 no-padding">DISTRITO:</label>
                                    <div class="input-group col-xs-12">
                                        <select required v-model="direcData.distrito" data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                            <option v-for="item in listDistritos2" :value="item.dis_codigo">{{item.dis_nombre}}</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">

                        <button  type="submit" class="btn btn-primary">Cuardar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>


    <div id="conten-modales">

       <?php // include 'modales/bucar_clientes.php' ?>
       <?php //include 'modales/registrar_cliente.php' ?>
       <?php //include 'modales/buscar_agensia.php' ?>
       <?php //include 'modales/registrar_agensia.php' ?>
       <?php //include 'modales/buscar_productos.php' ?>


    </div>

    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <!--script type="module" src="../public/alertToas.js"></script-->
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <!--script type="text/javascript" src="../public/js/contador_espinner.js"></script-->
    <script type="module" src="../public/js/Input_validate.js"></script>
    <script src="../public/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="../public/js/modal_agencia_transporte.js"></script>
    <script>

        const APP_dirc= new Vue({
            el:"#modal-direccion",
            data:{
                cliente:'',
                direcData:{
                    depart:'',
                    provin:'',
                    distrito:'',
                    direccion:''
                },
                listProvincias2:[],
                listDistritos2:[],
            },
            methods: {
                agregarDireccion(){
                    const depar = this.direcData.depart
                    const prov =this.direcData.provin
                    const distr = this.direcData.distrito
                    const dir =this.direcData.direccion;
                    const cliente= this.cliente;
                    $.ajax({
                        type: "POST",
                        url: '../ajaxs/ajs_direcciones.php',
                        data: {tipo:'in',cliente,ubigeo:depar+""+prov+""+distr,direccion:dir,tipo_dir:'OTROS'},
                        success: function (resp) {
                            console.log(resp);
                            resp = JSON.parse(resp);
                            if (resp.res){
                                APP.getDataInfoDirecciones();
                                APP_dirc._data.direcData.depart='';
                                APP_dirc._data.direcData.provin='';
                                APP_dirc._data.direcData.distrito='';
                                APP_dirc._data.direcData.direccion='';
                                console.log(depar+" "+prov+" "+distr+" "+dir);
                                $("#modal-direccion").modal('hide');
                            }
                        }
                    });


                },
                setCliente(cli){
                    this.cliente= cli;
                },
                selectProvin2(event) {
                    const depa = this.direcData.depart;
                    const prov =event.target.value;
                    $.ajax({
                        type: "POST",
                        url: "../ajaxs/ajs_bd_datas.php",
                        data: {tipo:'list_distri',dep:depa,pro:prov},
                        success: function (respDistr) {
                            console.log(respDistr)
                            APP_dirc._data.listDistritos2=JSON.parse(respDistr);
                            setTimeout(function () {
                                $(".selectpicker").selectpicker('refresh');
                            },200)
                        }
                    });
                },
                selectDepart2(event) {
                    const depa =event.target.value;
                    $.ajax({
                        type: "POST",
                        url: "../ajaxs/ajs_bd_datas.php",
                        data: {tipo:'list_prov',dep:depa},
                        success: function (respProv) {
                            console.log(respProv)
                            APP_dirc._data.listProvincias2=JSON.parse(respProv);
                            setTimeout(function () {
                                $(".selectpicker").selectpicker('refresh');
                            },200)

                        }
                    });
                },
            }
        })
       //parseFloat()
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>
</body>


<script type="text/javascript">

   // MODALES._data.clientes.iniciarDatos= true;
    //MODALES._data.agenciaTansporte.iniciarDatos= true;
    //MODALES._data.productos.iniciarDatos= true;



   var  fecha =  new Date();
    const APP = new Vue({
        el:"#contenedorprincipal",
        data:{
            listaCarros:[],
            idcotizacion:0,
            clasificacion:[],
            funExeSavaCoti:function () {

            },
            guiaremision:{
                empresa:0,
                idClie:0,
                nombre:'',
                ruc:'',
                fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate(),
                direcciones:[],
                idDireccion:'',
                tpTrans:'',
                motivo:'',
                peso:'',
                factura:'',
                agenciaid:0,
                agenciaNom:'',
                razonAgencia:'',
                numPlaca:'',
                marcaCar:'',
                modeloCar:'',

            },
            condutoR:{
                docAgencia:'',
                numDocConduc:'',
                numLicenConduc:'',
            },
            listaConductores:[],
            cliente:{
                id:0,
                nombre:'',
                ruc:'',
                direcciones:[],
                idDireccion:'',
                telefono:'',
                atencion:'',
                formapago:'',
                moneda:4,
                tipopago:'',
                tipreciopro:'',
                cantidaddias:'',
                agenciatrasporte:'',
                idagencia:'',
                fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate(),
                observaciones:''
            },
            listaTipoPago:<?php echo json_encode($listaTemTP)?>,
            listaDetallePago:[],
            tasaCambio:{},
            igv:0,
            descuento:0,
            subTotal:0,
            totalcoti:0,
            producto:{
                predata:{},
                codigo:'',
                medida:'',
                nombre:'',
                precio:0,
                cantidad:'',
                stock:0,
                descuento:0,
                total:0
            },
            index_producto:-1,
            productos:[]
        },
        methods:{
            getDataInfoDirecciones(){

                $.ajax({
                    type: "POST",
                    url: "../ajaxs/ajs_cliente.php",
                    data: {tipo:'s',acc:"direcciones",idc:APP._data.guiaremision.idClie},
                    success: function (data) {
                        if (isJson(data)){
                            var direc =JSON.parse(data);
                            console.log(direc);
                            APP._data.guiaremision.direcciones = direc;
                            APP._data.guiaremision.idDireccion = 0;
                            //APP._data.cliente.idDireccion=(typeof direc[0]!=='undefined')?direc[0].id:'';

                        }else{
                            console.log(data);
                        }

                    }
                });

            },
            aponModalNewDireccion(){
                if (this.guiaremision.ruc.length>0){
                    $('#modal-direccion').modal('show');
                    APP_dirc.setCliente(this.guiaremision.ruc);
                }else{
                    swal("Bloqueado","Primero seleccione un cliente o una venta",'warning')
                }
            },
            seleccionCarro(index){
                this.listaConductores=[];
                const dat = this.listaCarros[index];
                this.guiaremision.numPlaca=dat.nro_placa;
                this.guiaremision.marcaCar=dat.marca;
                this.guiaremision.modeloCar=dat.modelo;
                $.ajax({
                    type: "POST",
                    url:"../ajaxs/ajs_bd_datas.php",
                    data: {tipo:'carros_agen_conduc',carro:dat.carr_id},
                    success: function (resp) {
                        resp = JSON.parse(resp);
                        console.log(resp)
                        resp.forEach(function (element, index) {
                            APP._data.listaConductores.push({
                                docAgencia: element.tipo_doc,
                                docum: element.tipo_doc,
                                numDocConduc: element.nro_doc,
                                numLicenConduc: element.nro_licen,
                            });
                        });
                        $("#modal_carros_agen").modal('hide');
                    }
                });


            },
            recargarCarros(agencia){
                $.ajax({
                    type: "POST",
                    url: "../ajaxs/ajs_bd_datas.php",
                    data: {tipo:'carros_agen',agencia},
                    success: function (resp) {
                        console.log(resp);
                        resp = JSON.parse(resp);
                        APP._data.listaCarros = resp;
                        if (resp.length>0){
                            $("#modal_carros_agen").modal('show');
                        }
                    }
                });

            },
            eliminarConduc(index){
                this.listaConductores.splice( index, 1 );
            },
            agregarConductor(){
                var datos = {...this.condutoR};
                datos.docum=$("#tipoDocIdentidad option:selected").text()
                this.listaConductores.push(datos)
                this.condutoR= {
                    docAgencia: '',
                    numDocConduc: '',
                    numLicenConduc: '',
                }
            },
            buscarDocumentoV(){
                var dateD = {
                    doc:1,
                    nunDoc:parseInt(this.guiaremision.factura+""),
                    empre:this.guiaremision.empresa
                }
                console.log(dateD)
                $.ajax({
                    type: "POST",
                    url: "../ajaxs/buscar_documento_venta.php",
                    data: dateD,
                    success: function (data) {
                        console.log(data)
                        if (isJson(data)){
                            var jso = JSON.parse(data);
                            $("#observacionesVenta").text(jso.data.observaciones)
                            console.log(jso)
                            APP._data.productos=[];
                           /* for each (var item in jso.data.productos) {
                                APP._data.productos.push({

                                    idProdEmpr:item.prod_id,
                                    producto:item.produ_nombre,
                                    sku:item.produ_sku,
                                    medida:'NIU',
                                    cantidad:item.cantidad

                                });
                            }*/
                            jso.data.productos.forEach(function (item) {
                                APP._data.productos.push({

                                    idProdEmpr:item.prod_id,
                                    producto:item.produ_nombre,
                                    sku:item.produ_sku,
                                    medida:'NIU',
                                    cantidad:item.cantidad,
                                    peso:item.peso

                                });
                            })

                            $.ajax({
                                type: "POST",
                                url: '../ajaxs/ajs_cliente.php',
                                data: {tipo:'s',acc:"clie",ruc:jso.data.cli_ndoc},
                                success: function (data) {
                                    console.log(data)
                                    data = JSON.parse(data);
                                    APP._data.guiaremision.idClie=data.cli_id;
                                    APP._data.guiaremision.nombre=data.cli_nomape;
                                    APP._data.guiaremision.ruc=data.cli_ndoc;
                                    $.ajax({
                                        type: "POST",
                                        url: "../ajaxs/ajs_cliente.php",
                                        data: {tipo:'s',acc:"direcciones",idc:data.cli_id},
                                        success: function (data) {
                                            if (isJson(data)){
                                                var direc =JSON.parse(data);
                                                console.log(direc);
                                                APP._data.guiaremision.direcciones = direc;
                                                APP._data.guiaremision.idDireccion = 0;
                                                //APP._data.cliente.idDireccion=(typeof direc[0]!=='undefined')?direc[0].id:'';

                                            }else{
                                                console.log(data);
                                            }

                                        }
                                    });
                                }
                            });



                            swal("Documento encontrado", "", "success");

                        }else{
                            swal("Documento no encontrado")
                        }
                    }
                });

            },
            onlyNumber ($event) {
                //console.log($event.keyCode); //keyCodes value
                let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
                if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                    $event.preventDefault();
                }
            },
            guardarGuiaRemision(){

                if (this.productos.length>0){
                    var data = {...this.guiaremision};
                    data.tipo= "i";
                    data.direcciones= "";
                    data.productosG= JSON.stringify(this.productos);
                    data.conductoresG= JSON.stringify(this.listaConductores);
                    console.log(data)
                    $.ajax({
                        type: "POST",
                        url: '../ajaxs/ajs_guia_remision.php',
                        data: data,
                        success: function (data) {
                            console.log(data);
                            if (isJson(data)){
                                data = JSON.parse(data);
                                if (data.res){
                                    swal("Guia Remision Registrada", "", "success").then(function (){
                                        window.open("./guia_remision_pdf.php?guia="+data.gia, '_blank');
                                        window.location.href ="./guia_remision.php";
                                    });
                                }else{
                                    swal('Hubo un problema al guardar la Guia Remision')
                                }
                            }else{
                                swal('Error, no de pudo guardar la Guia Remision');
                            }
                        }
                    });

                }else{
                    swal("No hay productos agregados", "", "warning");
                }

            },
            eliminarProdutoLista(index){
                this.productos.splice( index, 1 );
            },
            realizarVenta(){
                var d = new Date();

                var datacoti = {... APP._data.cliente};
                datacoti.tipo= "i";
                datacoti.direcciones='';
                datacoti.fechaVenta=$("#fecha").val();
                datacoti.horaVenta= d.getHours()+":"+((d.getMinutes()<10)?"0"+d.getMinutes():d.getMinutes());
                datacoti.id_empre=$("#input-id-empresa-user").val();
                datacoti.idcotizacion = idCoti;
                datacoti.tasacambio= this.tasaCambio.idc;
                datacoti.totalcoti =this.totalcoti;
                $.ajax({
                    type: "POST",
                    url: "../ajaxs/ajs_ventas.php",
                    data: datacoti,
                    success: function (resp) {
                        console.log(resp)
                        if (isJson(resp)){
                            var json = JSON.parse(resp);
                            if (json.res){
                                $.ajax({
                                    type: "POST",
                                    url: '../ajaxs/ajs_venta_detalle.php',
                                    data: {
                                        idCoti:json.idCoti,
                                        tipo:'i',
                                        productos: JSON.stringify(APP._data.productos)
                                    },
                                    success: function (response) {
                                        console.log(response)
                                        if (isJson(response)){
                                            swal("Venta Procesada ", "", "success").then(function (){
                                                window.location.href = "../ventas/venta_procesada.php?view="+json.idCoti;
                                            });

                                        }else{
                                            swal('Hubo un problema al guardar los productos de la venta')
                                        }
                                    }
                                });
                            }else{
                                console.log("error")
                                console.log(resp)
                                swal('Error, no de pudo guardar la venta');
                            }
                        }else{
                            console.log(resp);
                        }
                    }
                });

            },
            cargarDatoCotizacion(){

                $.ajax({
                    type: "POST",
                    url: '../ajaxs/ajs_cotizacion.php',
                    data:{
                        tipo:'s',
                        idCoti
                    },
                    success: function (data) {
                        console.log(data)
                        if (isJson(data)){
                            var json = JSON.parse(data);
                            console.log(json)
                            APP._data.cliente.id=json.id_cliente;
                            APP._data.cliente.nombre=json.coti_razon;
                            APP._data.cliente.ruc=json.coti_ruc;
                            APP._data.cliente.idDireccion=json.id_direccion;
                            APP._data.cliente.telefono=json.coti_telf;
                            APP._data.cliente.atencion=json.coti_ate;
                            APP._data.cliente.formapago=json.coti_pago;
                            APP._data.cliente.tipopago=json.coti_tp_pago;
                            APP._data.cliente.cantidaddias =json.coti_dias_credito;
                            APP._data.cliente.agenciatrasporte=json.agencia;
                            APP._data.cliente.idagencia=json.id_agencia;
                            APP._data.cliente.fecha=json.coti_fecha;
                            APP._data.cliente.tipreciopro=json.tipo_costo_prod;
                            APP._data.cliente.observaciones=json.coti_obs;
                            APP._data.idcotizacion = json.coti_id;
                            APP._data.cliente.moneda =json.coti_tp_moneda;

                            console.log(APP._data.cliente.cantidaddias + "<>" + json.coti_dias_credito);

                            json.productos.forEach(function (prod) {
                                var totalT =parseInt(prod.cantidad) *parseInt(prod.precio_unitario);
                                APP._data.productos.push({
                                    id:prod.produ_id,
                                    producto:prod.produ_nombre,
                                    empresa:prod.emp_nombre,
                                    idempresa:prod.emp_id,
                                    idProdEmpr:prod.id_prod_empre,
                                    marca:prod.mar_nombre,
                                    sku:prod.produ_sku,
                                    pais:prod.pais_nombre,
                                    cantidad:prod.cantidad,
                                    descuento:prod.descuento,
                                    precio:prod.precio_unitario,
                                    preciostemp:prod.precios,
                                    subtotal:totalT-((totalT* prod.descuento)/100)

                                });
                            });

                            $.ajax({
                                type: "POST",
                                url: "../ajaxs/ajs_cliente.php",
                                data: {tipo:'s',acc:"direcciones",idc:APP._data.cliente.id},

                                success: function (data) {
                                    if (isJson(data)){
                                        var direc =JSON.parse(data);
                                        APP._data.cliente.direcciones = direc;
                                    }else{
                                        console.log(data);
                                    }

                                }
                            });


                           /* setTimeout(function () {
                                $('select').selectpicker('refresh');
                            },100);*/
                            $.ajax({
                                type: "POST",
                                url: '../ajaxs/ajs_bd_datas.php',
                                data: {id:json.coti_pago,tipo:"pagodetalle"},

                                success: function (resp) {
                                   // console.log(resp)
                                    if (isJson(resp)){
                                        APP._data.listaDetallePago = JSON.parse(resp);
                                       /* setTimeout(function () {
                                            $('#select-tipoPago').selectpicker('refresh');
                                        },100)*/
                                    }else{
                                        console.log(resp)
                                    }
                                }
                            });

                            //console.log(json)
                        }else{
                            console.log(data)
                        }
                    }
                });
            },
            seleccionarProducnto(index){
                this.index_producto=index;
                //console.log('hhhhholaaaaaaaaaaa'+ index)
            },
            eliminarItemProducto(){
                if (this.index_producto!=-1){
                    this.productos.splice(  this.index_producto, 1 );
                    this.index_producto=-1;
                }else{
                   swal('Primero selecione Una fila');
                }

            },
            guardarCotizacion(){
                var datacoti = {... APP._data.cliente};
                datacoti.tipo= isRegister?"i":'u';
                datacoti.direcciones='';
                datacoti.estcot='1';
                datacoti.idcotizacion = idCoti;
                datacoti.tasacambio= this.tasaCambio.idc;
                datacoti.totalcoti =this.totalcoti;
                console.log(datacoti)
                $.ajax({
                    type: "POST",
                    url: "../ajaxs/ajs_cotizacion.php",
                    data: datacoti,
                    success: function (resp) {
                        console.log(resp)
                        if (isJson(resp)){
                            var json = JSON.parse(resp);
                            if (json.res){
                                console.log(APP._data.productos);
                                $.ajax({
                                    type: "POST",
                                    url: '../ajaxs/ajs_detalle_cotizacion.php',
                                    data: {
                                        tipo:isRegister?'i':'u',
                                        idCoti:json.idCoti,
                                        productos: JSON.stringify(APP._data.productos)
                                    },
                                    success: function (response) {
                                        console.log(response)
                                        if (isJson(response)){
                                            const jsRe = JSON.parse(response)
                                            if(jsRe.res){
                                                swal("Cotizacion Guardada", "", "success");
                                                idCoti=json.idCoti;
                                                isRegister=false;
                                            }else{
                                                swal('Hubo un problema al guardar los productos de la cotización')
                                            }

                                        }else{
                                            swal('Hubo un problema al guardar los productos de la cotización EROOR:Server')
                                        }
                                    }
                                });
                            }else{
                                console.log("error")
                                console.log(resp)
                                swal('Error, no de pudo guardar la cotizacion');
                            }
                        }else{
                            console.log(resp);
                        }
                    }
                });
            },
            guardarYVenterCotizacion(){
                var datacoti = {... APP._data.cliente};
                datacoti.tipo= isRegister?"i":'u';
                datacoti.direcciones='';
                datacoti.estcot='2';
                datacoti.idcotizacion = idCoti;
                datacoti.tasacambio= this.tasaCambio.idc;
                datacoti.totalcoti =this.totalcoti;
                console.log(datacoti)

                console.log(datacoti)

                swal({
                    title: "¿Desea realizar la venta?",
                    text: "",
                    icon: "warning",
                    dangerMode: false,
                    buttons: ["NO", "SI"],
                })
                    .then((ressss) => {
                        console.log(ressss);
                        if (ressss){
                            $.ajax({
                                type: "POST",
                                url: "../ajaxs/ajs_cotizacion.php",
                                data: datacoti,
                                success: function (resp) {
                                    console.log(resp)
                                    if (isJson(resp)){
                                        var json = JSON.parse(resp);
                                        if (json.res){
                                            console.log(APP._data.productos);
                                            $.ajax({
                                                type: "POST",
                                                url: '../ajaxs/ajs_detalle_cotizacion.php',
                                                data: {
                                                    tipo:isRegister?'i':'u',
                                                    idCoti:json.idCoti,
                                                    productos: JSON.stringify(APP._data.productos)
                                                },
                                                success: function (response) {
                                                    console.log(response)
                                                    if (isJson(response)){
                                                        const jsRe = JSON.parse(response)
                                                        if(jsRe.res){
                                                            idCoti=json.idCoti;
                                                            isRegister=false;
                                                            APP.realizarVenta();

                                                        }else{
                                                            swal('Hubo un problema al guardar los productos de la cotización')
                                                        }

                                                    }else{
                                                        swal('Hubo un problema al guardar los productos de la cotización EROOR:Server')
                                                    }
                                                }
                                            });
                                        }else{
                                            console.log("error")
                                            console.log(resp)
                                            swal('Error, no de pudo guardar la cotizacion');
                                        }
                                    }else{
                                        console.log(resp);
                                    }
                                }
                            });

                        }

                    });


            },
            recalcularPrecioGeneral(){
                const idtipoprecio = this.cliente.tipreciopro;
                for(var i=0; i<this.productos.length;i++){
                    this.productos[i]
                    const resultado =   this.productos[i].preciostemp.find(function(elt){ return elt.tipl_id == idtipoprecio});
                    var precionuevo;
                    if (typeof resultado === 'undefined'){
                        precionuevo='0.00';
                    }else{
                        precionuevo = resultado.lisd_vdolar;
                    }
                    this.productos[i].precio=precionuevo;
                    this.productos[i].subtotal= parseFloat(precionuevo+"") * parseInt( this.productos[i].cantidad);

                }
                if(this.producto.nombre.length>6){
                    const resultado =this.producto.predata.precios.find(function(elt){ return elt.tipl_id == idtipoprecio});
                    var precionuevo;
                    if (typeof resultado === 'undefined'){
                        precionuevo='0.00';
                    }else{
                        precionuevo = resultado.lisd_vdolar;
                    }
                    this.producto.precio=precionuevo
                }

            },
            agregarProducto(){
                console.log(this.producto.predata);

                var indexProd=-1;
                var cambio = 1;

                for (var i =0; i<this.productos.length;i++){
                    if (this.producto.predata.produ_id==this.productos[i].id && this.producto.predata.emp_id ==this.productos[i].idempresa){
                        indexProd=i;

                    }
                }


                if (indexProd==-1){
                    this.productos.push({
                        id:this.producto.predata.produ_id,
                        idProdEmpr:this.producto.predata.prod_empre_id,
                        preciostemp:this.producto.predata.precios,
                        producto:this.producto.nombre,
                        empresa:this.producto.predata.emp_nombre,
                        idempresa:this.producto.predata.emp_id,
                        marca:this.producto.predata.mar_nombre,
                        sku:this.producto.predata.produ_sku,
                        medida:'NIU',
                        cantidad:this.producto.cantidad,
                        precio:this.producto.precio,
                        subtotal:this.producto.total

                    });
                }else {
                    this.productos[indexProd].cantidad = parseInt( this.productos[indexProd].cantidad ) +parseInt(this.producto.cantidad);
                }

                $('#tabla-poductos-coti tbody tr').removeClass('bg-success');
                this.producto.predata = {};
                this.producto.nombre ='';
                this.producto.codigo ='';
                this.producto.unidad ='';
                this.producto.stock =0;
                this.producto.cantidad='';
                this.producto.precio=0;
                this.producto.descuento='';


            },
            setDataCliente(dat){
                console.log(dat);
                this.guiaremision.idClie=dat.cli_id;
                this.guiaremision.nombre=dat.cli_nomape;
                this.guiaremision.ruc=dat.cli_ndoc;
                this.guiaremision.direccion=dat.cli_direc;
            },
            setDataTransporte(data){
                this.cliente.agenciatrasporte=data.razon_social;
                this.cliente.idagencia=data.id;
            },
            setDataProducto(data){
                var cambio = 1;
                if (this.cliente.moneda==1){
                    cambio= this.tasaCambio.venta;
                }
                this.producto.predata = data;
                this.producto.nombre =data.produ_nombre;
                this.producto.stock = parseInt( data.cantidad);
                this.producto.cantidad='';
                this.producto.precio= (parseFloat( data.precio)/cambio).toFixed(2);
                this.producto.descuento=0;


            },
            onformaPago(evt){
                var id = evt.target.value;
                $.ajax({
                    type: "POST",
                    url: '../ajaxs/ajs_bd_datas.php',
                    data: {id,tipo:"pagodetalle"},
                    success: function (resp) {
                        console.log(resp)
                        if (isJson(resp)){
                            APP._data.listaDetallePago = JSON.parse(resp);
                            /*setTimeout(function () {
                                $('#select-tipoPago').selectpicker('refresh');
                            },100)*/
                        }
                    }
                });
            }
        },
        computed:{
            totalPeso(){
                var peso =0;
                for (var i=0; i<this.productos.length;i++){
                    peso +=this.productos[i].peso*this.productos[i].cantidad;
                }
                peso = peso.toFixed(2);
                this.guiaremision.peso = peso;
                return peso;
            },
            simboleModena(){
              return this.cliente.moneda==1?'$':'S/.';
            },
            isCredito(){
                if (this.cliente.formapago==2){
                    this.cliente.cantidaddias="";
                }

                return this.cliente.formapago!=1;
            },
            isDolar(){
                return this.cliente.moneda==1;
            },
            totalpropetido(){
                var desc = (this.producto.descuento +"").length>0?this.producto.descuento:0;
                var cnt = (this.producto.cantidad +"").length>0?this.producto.cantidad:0;
                var toT = this.producto.precio * cnt;
                this.producto.total = toT - ((toT*desc)/100);
                return this.producto.total.toFixed(2);
            },
            totalTabla(){
                setTimeout(function () {
                    $('#tabla-poductos-coti tbody tr').click(function() {
                        //console.log('sasasasasasasasasa')
                        $(this).addClass('bg-success').siblings().removeClass('bg-success');
                    });
                },200);

                var total = 0;
                for (var i=0; i<this.productos.length; i++){
                    total += parseFloat(this.productos[i].subtotal);
                }
                this.subTotal=total;
                this.igv = (total*0.18);
                //console.log((total+ this.igv)+'/*************************');
                this.totalcoti=(total+ this.igv);
                return this.totalcoti;
            }
        }
    });

    function geTasaCambio(){
        $.ajax({
            type: "POST",
            url: "../ajax/CambioMoneda/iniciartipoCambio.php",
            data: {
                fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate()
            },
            success: function (dat) {
                if (isJson(dat)){
                    APP._data.tasaCambio= JSON.parse(dat);
                    swal("Tasa de cambio: "+APP._data.tasaCambio.fecha, "Venta: "+APP._data.tasaCambio.venta+"\nCompra: "+APP._data.tasaCambio.compra)
                }else{
                    console.log(dat);
                }
            }
        });
    }

    $(document).ready(function() {

       // geTasaCambio();

        if (!isRegister){
            APP.cargarDatoCotizacion();
        }

       /* $('#tttttttttttttttt').DataTable({

            serverSide: true,
            processing: true,
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
        $('#1111111111111').DataTable({

            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });*/
        //$('select').selectpicker();

        $("#input-coti-cliente").autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "../ajaxs/buscar_cliente_dnumdoc.php",
                    data: { term: request.term},
                    success: function(data){
                        response(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        console.log(jqXHR.responseText)
                    },
                    dataType: 'json'
                });
            },
            minLength: 2,
            select: function (event, ui) {
                console.log(ui.item)
                event.preventDefault();
                APP.setDataCliente(ui.item);

                $.ajax({
                    type: "POST",
                    url: "../ajaxs/ajs_cliente.php",
                    data: {tipo:'s',acc:"direcciones",idc:APP._data.guiaremision.idClie},
                    success: function (data) {
                        if (isJson(data)){
                            var direc =JSON.parse(data);
                            console.log(direc);
                            APP._data.guiaremision.direcciones = direc;
                            APP._data.guiaremision.idDireccion = 0;
                            //APP._data.cliente.idDireccion=(typeof direc[0]!=='undefined')?direc[0].id:'';

                        }else{
                            console.log(data);
                        }

                    }
                });

               /* $('#hidden_id_proveedor').val(ui.item.id);
                $('#input_ruc_proveedor').val(ui.item.ruc);
                $('#input_razon_social').val(ui.item.razon_social);
                $('#input_direccion').val(ui.item.direccion);
                $('#input_producto').focus();*/
            }
        });
        $("#input-agencia-transporte").autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "../ajaxs/buscar_agencia_transporte.php",
                    data: { term: request.term},
                    success: function(data){
                        response(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        console.log(jqXHR.responseText)
                    },
                    dataType: 'json'
                });
            },
            minLength: 2,
            select: function (event, ui) {
                console.log(ui.item)
                event.preventDefault();
                APP._data.guiaremision.agenciaid=ui.item.id;
                APP._data.guiaremision.agenciaNom=ui.item.ruc;
                APP._data.guiaremision.razonAgencia=ui.item.razon_social;
                APP._data.guiaremision.numPlaca='';
                APP._data.guiaremision.marcaCar='';
                APP._data.guiaremision.modeloCar='';
                APP._data.guiaremision.docAgencia='';
                APP._data.guiaremision.numDocConduc='';
                APP._data.guiaremision.numLicenConduc='';
                APP.recargarCarros(ui.item.id);
               /* APP.setDataCliente(ui.item);

                $.ajax({
                    type: "POST",
                    url: "../ajaxs/ajs_cliente.php",
                    data: {tipo:'s',acc:"direcciones",idc:APP._data.cliente.id},
                    success: function (data) {
                        if (isJson(data)){
                            var direc =JSON.parse(data);
                            console.log(direc);
                            APP._data.cliente.direcciones = direc;
                            APP._data.cliente.idDireccion = 0;

                        }else{
                            console.log(data);
                        }

                    }
                });*/

               /* $('#hidden_id_proveedor').val(ui.item.id);
                $('#input_ruc_proveedor').val(ui.item.ruc);
                $('#input_razon_social').val(ui.item.razon_social);
                $('#input_direccion').val(ui.item.direccion);
                $('#input_producto').focus();*/
            }
        });

        $("#input-buscar-producto").autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "../ajaxs/buscar_productos2.php",
                    data: { term: request.term,empresa :APP._data.guiaremision.empresa},
                    success: function(data){
                        response(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        console.log(jqXHR.responseText)
                    },
                    dataType: 'json'
                });
            },
            minLength: 2,
            select: function (event, ui) {
                console.log(ui.item)
                ui.item.cantidad -= nuevoStock(ui.item.produ_id,ui.item.emp_id);
                APP._data.producto.predata = ui.item;
                APP._data.producto.nombre = ui.item.produ_nombre;
                APP._data.producto.codigo = ui.item.produ_sku;
                //APP._data.producto.medida = ui.item.unidad_nombre;
                APP._data.producto.medida = 'NIU';

                //APP._data.producto.precio = ui.item.precio;

                APP._data.producto.cantidad = '';
                APP._data.producto.stock = ui.item.cantidad ;
                APP._data.producto.total = 0;
                console.log( nuevoStock(ui.item.produ_id,ui.item.prod_empre_id));

                event.preventDefault();

            }
        });

        $("#input-coti-cliente").focus(function () {
            //$("#modal_buscar_cliente").modal('show');
        });
      /*  $("#input-producto-buscar").focus(function () {
            $("#modal_buscar_productos").modal('show');
        });*/

        $("#input-coti-agendia").focus(function () {
            $("#modal_buscar_Agencia_transporte").modal('show');
        });

        $('#modal_buscar_cliente').on('hidden.bs.modal', function () {
            if (MODALES._data.clientes.isSelected){
                APP.setDataCliente(MODALES._data.clientes.clienteSelected);
            }
        });
        $('#modal_buscar_Agencia_transporte').on('hidden.bs.modal', function () {
            if (MODALES._data.agenciaTansporte.isSelected){
                APP.setDataTransporte(MODALES._data.agenciaTansporte.agenciaSelected);
            }
        });
        $('#modal_buscar_productos').on('hidden.bs.modal', function () {
            if (MODALES._data.productos.isSelected){
                APP.setDataProducto(MODALES._data.productos.productoSelected);
            }
        });
        //$("#loader").fadeOut("slow");
    });
   
    function nuevoStock(idProd, idEmpre) {
        const prods = APP._data.productos;
        var cnt =0;
        for (var i=0; i< prods.length;i++){
            if (prods[i].idempresa==idEmpre&&prods[i].id==idProd){
                cnt = prods[i].cantidad;
            }
        }
        return cnt;
    }
    function NumeroAleatorio(min, max) {
        var num = Math.round(Math.random() * (max - min) + min);
        return num;
    }







</script>

</html>
