<?php
$indexRuta=1;
require '../dao/NotaElectronicaDao.php';

$nombremodule="Notas Electronicas";

$notaElectronicaDao = new NotaElectronicaDao();

$listaNotas = $notaElectronicaDao->getNotasE();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../public/css/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../public/plugins/sweetalert2/sweetalert2.min.css">

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/

    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Notas Electronicas</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <button onclick="" type="button"  data-toggle="modal" data-target="#modal-gen-nota-electronica" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo
                                        </button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <table id="table-productos" class="table table-striped table-bordered table-hover">
                                    <thead class="bg-head-table">
                                    <tr style="background-color: #007ac3; color: white">
                                        <th style="border-right-color: #007ac3" class="text-center"></th>
                                        <th style="border-right-color: #007ac3" class="text-center">DOCUMENTO</th>
                                        <th style="border-right-color: #007ac3" class="text-left">FECHA</th>
                                        <th style="border-right-color: #007ac3" class="text-center">NUMERO / SERIE</th>
                                        <th style="border-right-color: #007ac3" class="text-center">CLIENTE</th>
                                        <th style="border-right-color: #007ac3" class="text-center">MONEDA</th>
                                        <th style="border-right-color: #007ac3" class="text-center">TOTAL</th>
                                        <th style="border-right-color: #007ac3" class="text-center">ESTADO</th>
                                        <th class="text-center">OPCION</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach ($listaNotas as $item){   ?>
                                            <tr >
                                                <td class="text-center" style="color: white"><?=$item['notc_id'] ?></td>
                                                <td class="text-center"><?=$item['doc_nombre'] ?></td>
                                                <td class="text-center"><?=$item['notc_fecha'] ?></td>
                                                <td class="text-center"><?=$item['not_sn'] ?></td>
                                                <td class="text-center"><?=$item['cli_nomape'] ?></td>
                                                <td class="text-center"><?=$item['sun_id']==2?'DOLAR':'SOLES' ?></td>
                                                <td class="text-center"><?=$item['notc_monto'] ?></td>
                                                <td class="text-center"><?=$item['notc_estatus'] ?></td>
                                                <td class="text-center">
                                                    <a href="comprobante3_pdf.php?ne=<?=$item['notc_id'] ?>" target="_blank" class="btn btn-sm btn-warning fa fa-file-pdf btn-selector-cliente"   ></a>
                                                    <button  class="btn btn-sm btn-info fa fa-archive btn-selector-cliente"  onclick="" ></button>
                                                </td>
                                            </tr>

                                        <?php    }
                                    ?>




                                    </tbody>

                                </table>


                                <div class="modal fade" id="modal-gen-nota-electronica" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog" role="document" style="width: 60%;">
                                        <div class="modal-content">
                                            <div class="modal-header no-border no-padding">
                                                <div class="modal-header text-center color-modal-header">
                                                    <h3 class="modal-title">NOTA ELECTRONICA</h3>
                                                </div>
                                            </div>

                                            <div class="modal-body  no-border">
                                                <form action="#">
                                                    <div class="container-fluid">
                                                        <form>
                                                            <div class="row">
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <label class="col-xs-12 no-padding">DOCUMENTO:</label>
                                                                    <div class="input-group col-xs-12">

                                                                        <select @change="cambioTipoNota($event)" v-model="dataNE.tipoD" class="form-control">
                                                                            <option value="5">NOTA DE CREDITO</option>
                                                                            <option value="6">NOTA DE DEBITO</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">S -N:</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input v-model="dataNE.serieNumero" disabled class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">FECHA:</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input v-model="dataNE.fecha" disabled value="" class="form-control">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                                                    <label class="col-xs-12 no-padding">TIPO NOTA:</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <select @change="establecerTipoNota($event)" v-model="dataNE.motivo" class="form-control" id="motivo-doc-sun" name="select_motivo">
                                                                            <option v-if="dataNE.tipoD==5" value="01">ANULACION DE LA OPERACION</option>
                                                                            <option v-if="dataNE.tipoD==5" value="02">ANULACION POR ERROR EN EL RUC</option>
                                                                            <option v-if="dataNE.tipoD==5 && dataNE.tipDoc!=2" value="08">BONIFICACION</option>
                                                                            <option v-if="dataNE.tipoD==5" value="03">CORRECCION POR ERROR EN LA DESCRIPCION</option>
                                                                            <option v-if="dataNE.tipoD==5 && dataNE.tipDoc!=2" value="04">DESCUENTO GLOBAL</option>
                                                                            <option v-if="dataNE.tipoD==5 && dataNE.tipDoc!=2" value="05">DESCUENTO POR ITEM</option>
                                                                            <option v-if="dataNE.tipoD==5" value="07">DEVOLUCION POR ITEM</option>
                                                                            <option v-if="dataNE.tipoD==5" value="06">DEVOLUCION TOTAL</option>
                                                                            <option v-if="dataNE.tipoD==5 && dataNE.tipDoc!=2" value="09">DISMINUCION EN EL VALOR</option>
                                                                            <option v-if="dataNE.tipoD==6 && dataNE.tipDoc!=2" value="01">Intereses por mora</option>
                                                                            <option v-if="dataNE.tipoD==6 && dataNE.tipDoc!=2" value="02">Aumento en el valor</option>
                                                                            <option v-if="dataNE.tipoD==6 && dataNE.tipDoc!=2" value="03">Penalidades/ otros conceptos</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <label class="col-xs-12 no-padding">TIPO DOCUMENTO</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <select v-model="dataNE.tipDoc"  id="tipodo" class="form-control">
                                                                            <option value="1">FACTURA</option>
                                                                            <option value="2" >BOLETA</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <label class="col-xs-12 no-padding">EMPRESA</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <select @change="cambioTipoNota($event)"
                                                                                v-model="dataNE.idEmpresa"
                                                                                id="empresa"
                                                                                class="form-control">
                                                                            <option value="3">GRUPO LODI SRL</option>
                                                                            <option value="2">CONCORD PLUS S.R.LTDA.</option>
                                                                            <option value="4">LODI IMPORT CENTER S.R.L.</option>
                                                                            <option value="6">PERU TIRES S.R.L.</option>
                                                                            <option value="1">ASIAPERU TRADING S.R.L.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <label class="col-xs-12 no-padding">NUMERO DE DOCUMENTO</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input v-model="dataNE.documentoRelacionado"  class="form-control col-xs-6">
                                                                        <span class="input-group-btn">
                                                                            <button v-on:click="  buscarDocumentoV()" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_incoterm">
                                                                                <i class="fa fa-search"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <label class="col-xs-12 no-padding">TOTAL DEL DOCUMENTO</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input :value="simbolo+' '+dataNE.totalDocumento" disabled class="form-control col-xs-6">
                                                                        <span class="input-group-btn">
                                                                            <a :href="'../ventas/comprobante_pdf.php?venta='+dataNE.idVenta" target="_blank" class="btn btn-primary" >
                                                                                <i class="fa fa-eye"></i>
                                                                            </a>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                                                    <label class="col-xs-12 no-padding">SUSTENTO</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input v-model="dataNE.sustento"  class="form-control col-xs-6">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" v-if="dataNE.tipoD==6 || isRegistro">
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                                                    <label class="col-xs-12 no-padding">DESCRIPCION</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input v-model="dataNE.dataRegistro.descrip"  class="form-control col-xs-6">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">CANTIDAD</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input @keypress="onlyNumber" v-model="dataNE.dataRegistro.cantidad"  class="form-control col-xs-6">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">PRECIO U.: {{simbolo}}</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input @keypress="onlyNumber" v-model="dataNE.dataRegistro.precio_unitario"  class="form-control col-xs-6">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <div class="input-group col-xs-12 text-center" >
                                                                        <label style="color: white">.</label>
                                                                    </div>
                                                                    <div class="input-group col-xs-12 text-center" style="margin-bottom: 5px;">
                                                                        <button v-on:click="agregar()" type="button" class="btn btn-primary">AGREGAR</button>
                                                                    </div>


                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <table id="tabla-poductos-coti" class="table table-bordered table-hover" style="width:100%">
                                                                        <thead>
                                                                        <tr  style="background-color: #007ac3; color: white">

                                                                            <th style="border-right-color: #007ac3" >#</th>
                                                                            <th style="border-right-color: #007ac3">DESCRIPCION</th>
                                                                            <th style="border-right-color: #007ac3">MEDIDA</th>
                                                                            <th style="border-right-color: #007ac3">CANTIDAD</th>
                                                                            <th style="border-right-color: #007ac3">PRECIO</th>
                                                                            <th style="border-right-color: #007ac3">IMPORTE</th>
                                                                            <th style="border-right-color: #007ac3"></th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr v-for="(pro, ix) in dataNE.productos">
                                                                            <td>{{ix + 1}}</td>
                                                                            <td  v-if="pro.estado < 2 ">{{pro.descrip}}</td>
                                                                            <td  v-if="pro.estado >= 2 "><input style="width: 100%;" type="text" v-model="pro.descrip"></td>
                                                                            <td>{{pro.medida}}</td>
                                                                            <td  v-if="pro.estado <= 2 " class="text-center">{{pro.cantidad}}</td>
                                                                            <td  v-if="pro.estado > 2 "><input @keypress="onlyNumber"  type="text" v-model="pro.cantidad"></td>
                                                                            <td v-if="pro.estado <= 2 "  class="text-center">{{simbolo}} {{pro.precio_unitario}}</td>
                                                                            <td  v-if="pro.estado >2 "><input @keypress="onlyNumber"  type="text" v-model="pro.precio_unitario"></td>
                                                                            <td  class="text-center" style="background-color: #feffcb">{{simbolo}} {{ (pro.cantidad * pro.precio_unitario).toFixed(2)}}</td>
                                                                            <td class="text-center"><button v-if="pro.estado!=1" v-on:click="eliminarItem(ix)" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
                                                                        </tr>
                                                                        </tbody>
                                                                        <tfoot>
                                                                        <tr>
                                                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">Sub. Total</td>
                                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simbolo}} {{dataNE.subTotal.toFixed(2)}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">IGV</td>
                                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simbolo}} {{dataNE.igv.toFixed(2)}} </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">Total</td>
                                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simbolo}} {{getTotoal.toFixed(2)}}</td>
                                                                        </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </div>
                                                    <div class="container-fluid">
                                                        <hr class="line-frame-modal">
                                                    </div>
                                                    <div class="container-fluid text-right">

                                                        <button v-on:click="registrarNE()" type="button"  class="btn btn-primary">
                                                            Guardar
                                                        </button>
                                                        <button type="button" id="modal-buscar-empresa-btn-cerrar" class="btn btn-danger"
                                                                data-dismiss="modal">
                                                            Cancelar
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div id="conten-modales">


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>



       <script>
        $(document).ready(function () {
            $('#table-codigo-sunat').DataTable({
                /*scrollY: false,*/
                /*scrollX: true,*/
                paging: false,
                /* lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],*/
                language: {
                    url: '../assets/Spanish.json'
                }
            });
        });
    </script>


    <style>
        #img-file-preview-zone{
            -webkit-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            -moz-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            border-radius: 3px;
        }
    </style>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="module" src="../public/js/Input_validate.js"></script>
    <script src="../public/plugins/sweetalert2/vue-swal.js"></script>
    <!--script  type="module" src="../aConfig/scripts/sku.js"></script-->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>

        function llenar0(num,rang){
            var numfinal="";
            if (num.length>=rang){
                numfinal=num;
            }else{
                for (var i = 0; i< (rang-num.length);i++){
                    numfinal+="0";
                }
                numfinal+=num;
            }
            return numfinal;
        }

        var  fecha =  new Date();
        const APP = new Vue({
            el:"#contenedorprincipal",
            data:{
                simboleModena:'S/.',
                contador:3,
                lista:[
                    {fecha:'21/08/2020',ns:'F001 - 000002',cliente:"AGRONEGOCIOS VIRGEN DE YAUCA S.A.C.",moneda:"DOLARES",total:"4300.00",}
                ],
                simbolo:'',
                dataNE:{
                    idVenta:0,
                    cli_ndoc:'',
                    idEmpresa:1,
                    tipoD:1,
                    subTotal:0,
                    tipDoc:0,
                    sustento:'',
                    igv:0,
                    moneda:0,
                    total:0,
                    serieNumero:"",
                    fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate(),
                    motivo:1,
                    documentoRelacionado:"",
                    totalDocumento:0,
                    dataRegistro:{
                        descrip:'',medida:'UND',cantidad:0,precio_unitario:0,estado:1
                    },
                    productos:[]
                },
                isRegistro:false,
                productosVenta:[]

            },
            methods:{
                registrarNE(){

                    var dateD = {...this.dataNE};
                    dateD.tipo ='i';
                    dateD.nombreMotivo= $("#motivo-doc-sun option:selected").text();
                    dateD.productos = JSON.stringify(dateD.productos);

                    console.log(dateD)
                    $.ajax({
                        type: "POST",
                        url: "../ajaxs/ajs_nota_electronica.php",
                        data: dateD,
                        success: function (data) {
                            console.log(data)
                            if (isJson(data)){
                                swal("Documento Registrado", "", "success")
                                    .then(function () {
                                        location.reload();
                                    });
                            }

                        }
                    });

                },
                buscarDocumentoV(){
                    var dateD = {
                        doc:this.dataNE.tipDoc,
                        nunDoc:parseInt(this.dataNE.documentoRelacionado+""),
                        empre:this.dataNE.idEmpresa
                    }
                    console.log(dateD)
                    $.ajax({
                        type: "POST",
                        url: "../ajaxs/buscar_documento_venta.php",
                        data: dateD,
                        success: function (data) {
                            console.log(data)
                            if (isJson(data)){
                                var jso = JSON.parse(data);
                                console.log(jso)
                                // this.dataNE.serieNumero = jso;
                                APP._data.dataNE.cli_ndoc =jso.data.cli_ndoc;
                                APP._data.dataNE.idVenta =jso.data.fac_id;
                                APP._data.dataNE.totalDocumento =jso.data.fac_total;
                                APP._data.dataNE.moneda =jso.data.sun_id;
                                APP._data.simbolo =jso.data.sun_id==1?'S/.':'$';
                                APP._data.dataNE.productos=[];
                               /* jso.data.productos.forEach(function (item) {
                                    APP._data.dataNE.productos.push({
                                        descrip:item.produ_nombre,medida:'UND',cantidad:item.facd_cantidad,precio_unitario:item.facd_preciou,
                                    });
                                });*/
                                APP._data.productosVenta = jso.data.productos;
                                APP.establecerTipoNota();
                                swal("Documento encontrado", "", "success");

                            }else{
                                swal("Documento no encontrado")
                            }
                        }
                    });

                },
                eliminarItem(index){
                    this.dataNE.productos.splice(index, 1);
                },
                cambioTipoNota(event){
                    const idtipono = this.dataNE.tipoD;
                    const idEmpre=this.dataNE.idEmpresa;
                    $.ajax({
                        type: "POST",
                        url: "../ajaxs/ajs_bd_datas.php",
                        data: {idtipono,tipo:'numdocsunat',idEmpre},
                        success: function (res) {
                            console.log(res)
                            if (isJson(res)){
                                var jsn = JSON.parse(res);
                                APP._data.dataNE.serieNumero = jsn.serie + "-"+ llenar0(jsn.numero+"",6);
                            }else{
                            }
                        }
                    });

                },
                establecerTipoNota(){
                    //console.log(event.target.value)
                    const valor = this.dataNE.motivo;

                    this.dataNE.productos = [];
                    if (this.dataNE.tipoD==5){
                        /*if (valor == '01'&& valor == '02' && valor == '03' && valor == '06' && valor == '09'){

                        }*/
                        if (valor == '08' || valor == '04'){
                            this.isRegistro=true;
                        }else{
                            this.isRegistro=false;
                            if (valor == '01' || valor == '02' || valor == '06'){
                                this.productosVenta.forEach(function (item) {
                                    APP._data.dataNE.productos.push({
                                        descrip:item.produ_nombre,medida:'UND',cantidad:item.facd_cantidad,precio_unitario:item.facd_preciou, estado:1
                                    });
                                })
                            }else if (valor == '03' ){
                                this.productosVenta.forEach(function (item) {
                                    APP._data.dataNE.productos.push({
                                        descrip:item.produ_nombre,medida:'UND',cantidad:item.facd_cantidad,precio_unitario:item.facd_preciou, estado:2
                                    });
                                })
                            }else{
                                this.productosVenta.forEach(function (item) {
                                    APP._data.dataNE.productos.push({
                                        descrip:item.produ_nombre,medida:'UND',cantidad:item.facd_cantidad,precio_unitario:item.facd_preciou, estado:3
                                    });
                                })
                            }

                        }


                    }else{
                        /*this.productosVenta.forEach(function (item) {
                            APP._data.dataNE.productos.push({
                                descrip:item.produ_nombre,medida:'UND',cantidad:item.facd_cantidad,precio_unitario:item.facd_preciou, estado:1
                            });
                        })*/
                    }
                },
                buscarDocumento(){
                    var tipo = $("#tipodo").val();
                    var empresa = $("#empresa").val();
                    var numero = this.dataNE.documentoRelacionado;
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Ventas/busquedaVenta.php",
                        data: {
                            empres:empresa,
                            tipo:tipo,
                            numero:numero
                        },
                        success: function (dataResp) {
                            console.log(dataResp)
                            if (isJson(dataResp)){
                                var jso = JSON.parse(dataResp);
                                //console.log(jso)
                               // this.dataNE.serieNumero = jso;
                                APP._data.dataNE.totalDocumento =jso.monto;
                                swal("Documento encontrado", "", "success");

                            }else{
                                swal("Documento no encontrado")
                            }
                        }
                    });
                },
                GuardarDocumento(){


                    this.lista.push({fecha:APP._data.dataNE.fecha,
                        ns:"F001 - "+APP._data.dataNE.serieNumero,
                        cliente:"AGUILAR EVENTOS S.A.C.",
                        moneda:"SOLES",
                        total:APP._data.dataNE.total,});
                    swal("Documento Guardado", "", "success");
                    this.dataNE={
                        idVenta:0,
                            tipo:1,
                            subTotal:0,
                            igv:0,
                            total:0,
                            serieNumero:"0000"+APP._data.contador,
                            fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate(),
                            motivo:1,
                            documentoRelacionado:"",
                            totalDocumento:0,
                            dataRegistro:{
                            descrip:'',medida:'UND',cantidad:0,precio_unitario:0,
                        },
                        productos:[]
                    };
                    APP._data.contador++;
                    $('#modal-gen-nota-electronica').modal('toggle');


                },
                agregar(){
                    const tempo = {... APP._data.dataNE.dataRegistro};
                    tempo.estado=1
                    this.dataNE.productos.push(tempo)
                    this.dataNE.dataRegistro={
                        descrip:'',medida:'UND',cantidad:0,precio_unitario:0,
                    }

                },
                onlyNumber ($event) {
                    //console.log($event.keyCode); //keyCodes value
                    let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
                    if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                        $event.preventDefault();
                    }
                }
            },
            computed:{
                getTotoal(){
                    this.dataNE.subTotal=0;
                    this.dataNE.igv=0;
                    this.dataNE.total=0;
                    var val_est=true;
                    if (this.dataNE.tipoD==5){
                        if (this.dataNE.motivo=='03'){
                            val_est=false;
                        }
                    }
                    if (val_est){
                        for (var i=0 ;  i< this.dataNE.productos.length; i++){
                            this.dataNE.subTotal+= parseInt(this.dataNE.productos[i].cantidad) * parseFloat(this.dataNE.productos[i].precio_unitario);
                        }
                        this.dataNE.igv= this.dataNE.subTotal * 0.18;
                        this.dataNE.total =  this.dataNE.subTotal+this.dataNE.igv;
                    }


                    return this.dataNE.total;
                }
            }
        });


    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>


<script type="text/javascript">


    $(document).ready(function() {


        $('#table-productos').DataTable({
            "order": [[0, "desc" ]]
        });
    });


</script>

</html>
