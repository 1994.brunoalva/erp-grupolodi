<?php
require "../dao/VentaFacturacionDao.php";
require "../dao/ClienteDao.php";
require "../dao/VentaDetalleDao.php";
require "../dao/EmpresaDao.php";
require "../dao/VentaSunatDao.php";
require "../dao/TasaCambioDao.php";
require "../dao/NotaElectronicaDao.php";
require "../dao/NotaDetalleDao.php";
require  "../utils/Tools.php";
require_once('../../lib/mpdf/vendor/autoload.php');
require_once('../../lib/vendor/autoload.php');
use Endroid\QrCode\QrCode;
use Luecano\NumeroALetras\NumeroALetras;

$idnotaReg = $_GET['ne'];

$tools = new Tools();
$formatter = new NumeroALetras;


$ventaFacturacionDao = new VentaFacturacionDao();
$ventaDetalleDao = new VentaDetalleDao();
$clienteDao= new ClienteDao();
$empresaDao = new EmpresaDao();
$ventaSunatDao= new VentaSunatDao();
$tasaCambioDao= new TasaCambioDao();
$notaElectronicaDao= new NotaElectronicaDao();
$notaDetalleDao= new NotaDetalleDao();

$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);


$notaElectronicaDao->setNotcId($idnotaReg);
$notaDetalleDao->setNotaId($idnotaReg);


$resultC = $notaElectronicaDao->getDato();
$listaProd=$notaDetalleDao->getDatos();

$resultC = $resultC->fetch_assoc();

$listaProd = $ventaDetalleDao->getProductos();

//$resultC = $ventaFacturacionDao->getdataVenta()->fetch_assoc();

$empresaDao->setEmpId($resultC['emp_id']);
/*$tasaCambioDao->setTasId($resultC['tas_id']);*/

$restEmpre = $empresaDao->getData()->fetch_assoc();

//$resDataTasaCambio = $tasaCambioDao->getDatos()->fetch_assoc();
//echo  json_encode($resultC);


    $clienteDao->setCliId($resultC['cli_id']);
    $resultClie =$clienteDao->getdata()->fetch_assoc();

    if ($resultC['direccion']==0){
        //echo json_encode($resultClie);
        $direccionClientFac=$resultClie['cli_direc'];
    }else{
        $dirClieFa = $clienteDao->exeSQL("SELECT * FROM sys_ven_clientes_sucursales WHERE su_ruc='{$resultClie['cli_ndoc']}' AND su_id=".$resultC['direccion'])->fetch_assoc();
        $direccionClientFac=$dirClieFa['su_direccion'];
    }


//echo $resultC['direccion'].'    '.$direccionClientFac;
//echo json_encode($resultC);
$empresaDao->setEmpId($resultC['emp_id']);
$restEmpre = $empresaDao->getData()->fetch_assoc();


$rowHTML ="";
$subtotal = 0;
$contador =1;
foreach ($listaProd as $prod ){

    $precio =  $prod['facd_preciou'];
    $importe = $precio * $prod['facd_cantidad'];
    $subtotal = $subtotal + $importe;
    $precio = number_format($precio, 2, '.', ',');
    $importe = number_format($importe, 2, '.', ',');
    $rowHTML = $rowHTML . "
      <tr>
        <td class='borde-p' style=' font-size: 11px; text-align: center'>$contador</td>
        <td class='borde-p' style=' font-size: 11px; text-align: center'>{$prod['facd_cantidad']}</td>
        <td class='borde-p' style=' font-size: 11px; text-align: center'>{$prod['produ_nombre']}</td>
        <td class='borde-p' style=' font-size: 11px; text-align: center'>$precio</td>
        <td class='borde-p' style=' font-size: 11px; text-align: center'>0.00</td>
        
        
        <td class='borde-p' style=' font-size: 11px; text-align: center'>$importe</td>
      </tr>
    ";
    $contador++;
}

$igv = $subtotal *0.18;
$total = $subtotal+ $igv;

$TipoMoneda = $resultC['sun_id']==2?'DOLARES':'SOLES';
$totalLetras =   $formatter->toInvoice(number_format($total, 2, '.', ''), 2, $TipoMoneda);
$subtotal= number_format($subtotal, 2, '.', ',');
$igv =number_format($igv, 2, '.', ',');
$total =number_format($total, 2, '.', ',');

$dataDocumento = strlen($resultC['cli_ndoc'])>8?"RUC":"DNI";

/*$stylesheet = file_get_contents('../public/css/stylepdf.css');

$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);*/

$tipoDocNom = $resultC['tip_docu_sunat']==1?'FACTURA':'BOLETA';

$S_N = $resultC['fac_serie']. " - ". $tools->numeroParaDocumento($resultC['fac_num'],6);

$dataContQR = '';
$dataHashSunat = 'efsssssssssssssssssssss';
if ($rowSD = $ventaSunatDao->getData()->fetch_assoc()){
    $dataContQR = $rowSD['dataQR'];
    $dataHashSunat = $rowSD['hash_v'];
}



$qrCode = new QrCode($dataContQR);
$qrCode->setSize(150);
$image= $qrCode->writeString();//Salida en formato de texto
$imageData = base64_encode($image);
$qrImage='<img style="width: 130px;" src="data:image/png;base64,'.$imageData.'">';





/*$mpdf->SetDefaultBodyCSS("background","url('../public/img/cotilodi.png");
$mpdf->SetDefaultBodyCSS('background-image-resize', 6);*/

$htmlCuadroHead="<div style=' width: 34%;text-align: center; background-color: #ffffff ; float: right;'>

<div style='width: 100%; height: 100px; border: 2px solid #1e1e1e' class=''>
<div style='margin-top:10px'></div>
<span>RUC: {$restEmpre['emp_ruc']}</span><br>
<div style='margin-top: 10px'></div>
<span><strong>$tipoDocNom ELECTRONICA</strong></span><br>
<div style='margin-top: 10px'></div>
<span>Nro. $S_N </span>
</div>
</div>
</div>";

$mpdf->WriteFixedPosHTML("<img style='max-width: 300px' src='../public/img/empresalogo/{$restEmpre['emp_logo']}'>",5,5,150,120);
$mpdf->WriteFixedPosHTML($htmlCuadroHead,0,5,205,130);
$mpdf->WriteFixedPosHTML("<span style=' font-size: 13px'><strong>Central Telefonico: </strong> 203-1300</span>",5,27,210,130);
$mpdf->WriteFixedPosHTML("<span style=' font-size: 13px'><strong>Email: </strong> facturacion@grupolodi.com</span>",5,32,210,130);
$mpdf->WriteFixedPosHTML("<span style=' font-size: 13px'><strong>Direccion:</strong> {$restEmpre['emp_direccion']}</span>",5,37,210,130);

//$mpdf->WriteFixedPosHTML("<span style=' font-size: 13px'><strong>FECHA DE EMISION: {$resultC['fac_fechae']}</strong></span>",124,32,210,130);

$formaPago = $resultC['pag_id'] == 2?'CONTADO':'CREDITO';


$html= "<div style='width: 1000%;padding-top: 110px; overflow: hidden;clear: both;'>
<div style='width: 100%;border: 1px solid black'>
<div style='width: 55%; float: left; '>

<table style='width:100%'>
  <tr>
    <td style=' font-size: 13px;text-align: left'><strong>$dataDocumento:</strong></td>
    <td style=' font-size: 13px;'>{$resultC['cli_ndoc']}</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: left'><strong>CLIENTE:</strong></td>
    <td style=' font-size: 13px;'>{$resultC['cli_nomape']}</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: left'><strong>DIRECCION:</strong></td>
    <td style=' font-size: 13px;'>$direccionClientFac</td>
  </tr>
</table>
</div>
<div style='width: 45%; float: left'>
<table style='width:100%'>
  <tr>
    <td style=' font-size: 13px;text-align: left'><strong>TIPO DE PAGO:</strong></td>
    <td style=' font-size: 13px;'>$formaPago</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: left'><strong>VENDEDOR:</strong></td>
    <td style=' font-size: 13px;'>BRUNO</td>
  </tr>
   <tr>
    <td style=' font-size: 13px;text-align: left'><strong>MONEDA:</strong></td>
    <td style=' font-size: 13px;'>$TipoMoneda</td>
  </tr>
</table>
</div>
</div>


</div>

<div style='width: 100%; padding-top: 20px;'>
<table style='width:100%'>
  <tr style='border-bottom: 1px solid #363636'>
    <td style=' font-size: 13px;text-align: center; color: #000000;border-bottom: 1px solid #363636'><strong>ITEM</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #000000;border-bottom: 1px solid #363636'><strong>CANT</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #000000;border-bottom: 1px solid #363636'><strong>DESCRIPCION</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #000000;border-bottom: 1px solid #363636'><strong>PRECIO U.</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #000000;border-bottom: 1px solid #363636'><strong>DESCUENTO.</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #000000;border-bottom: 1px solid #363636'><strong>IMPORTE</strong></td>
    
  </tr>
  $rowHTML

<tr style=''>
<td colspan='3'></td>
 <td colspan='2' style=' font-size: 12px; text-align: right; font-weight: bold;background-color: #d3d3d3'>SUB TOTAL:</td>
 <td style=' font-size: 12px; text-align: center; font-weight: bold;background-color: #d3d3d3'>$subtotal</td>
</tr>
<tr>
 <td colspan='5' style=' font-size: 11px; text-align: right;'>IGV:</td>
 <td style=' font-size: 12px; text-align: center; '>18.00 %</td>
</tr>
<tr>
 <td colspan='5' style=' font-size: 11px; text-align: right; '>TOTAL IMPUESTOS:</td>
 <td style=' font-size: 12px; text-align: center; '>$igv</td>
</tr>
<tr>
<td colspan='3'></td>
 <td class='border-top' colspan='2' style=' font-size: 12px; text-align: right; font-weight: bold;background-color: #d3d3d3'>TOTAL</td>
 <td class='border-top' style=' font-size: 12px; text-align: center; font-weight: bold;background-color: #d3d3d3'>$total</td>
</tr>

</table>
</div>

";
$mpdf->SetHTMLFooter("
<div style='width: 100%; padding-bottom: 5px; font-size: 10px;'>Tipo cambio: {$resDataTasaCambio['tas_comercial_venta']} Valido solo {$resDataTasaCambio['tas_fecha']} || S/. $total</div>
<div style='width: 100%; padding-bottom: 0px;font-size: 10px;'>SON: $totalLetras</div>
<div style='width: 100%; padding-bottom: 0px;font-size: 10px;'>HASH SUNAT: $dataHashSunat</div>

<div style='width: 100%; '>
<div style='float: left; width: 140px'>
$qrImage
</div>
 <div style='width: 60%; padding-bottom: 5px;font-size: 12px; float: left; padding-top: 20px;'>
 <div style='width: 100%'>Observaciones:</div>
 <div style='width: 100%; border: 1px solid black; padding: 3px; font-size: 10px'>{$resultC['observaciones']}</div>
 </div>
</div>
<div style='width: 100%; text-align: center;color: #006ec2; padding-bottom: 10px;'>¡Gracias por su compra!</div>
");
//echo "<style>$stylesheet</style>".$html;

$mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

$mpdf->Output();







