<?php
$indexRuta=1;
require '../conexion/Conexion.php';


$conexionp =  (new Conexion())->getConexion();

$nombremodule="";

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../public/css/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../public/plugins/sweetalert2/sweetalert2.min.css">

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/

    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">titulo</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES
                                    <div class="col-lg-6 text-right">
                                        <a href="registro_guia_remision.php" type="button"   class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo
                                        </a>

                                    </div>-->

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">





                            </div>
                            <div  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div id="conten-modales">


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>





    <style>
        #img-file-preview-zone{
            -webkit-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            -moz-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            border-radius: 3px;
        }
    </style>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="module" src="../public/js/Input_validate.js"></script>
    <script src="../public/plugins/sweetalert2/vue-swal.js"></script>
    <!--script  type="module" src="../aConfig/scripts/sku.js"></script-->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>


        var  fecha =  new Date();
        const APP = new Vue({

        });


    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>


<script type="text/javascript">


    function getEstados(num){
        var estado="";
        switch (num+"") {
            case '1':
                    estado="NO ENVIADO";
                break;
        }
        return estado;
    }

    $(document).ready(function() {
        $("#table-guias-remision").DataTable({
            "processing": true,
            "serverSide": true,
            "sAjaxSource": "../ServerSide/serversideGuiaRemision.php",
                order: [[ 0, "desc" ]],
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel'
                ],
                columnDefs: [
                    {
                        "targets": 0,
                        "data": "coti_id",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;color: white">'+row[0]+'</span>';

                        }
                    },
                    {
                        "targets": 1,
                        "data": "cli_tdoc",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[1]+'</span>';

                        }
                    },
                    {
                        "targets": 2,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[2]+'</span>';

                        }
                    },
                    {
                        "targets": 3,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[3]+'</span>';

                        }
                    },

                    {
                        "targets": 4,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[4]+'</span>';

                        }
                    },

                    {
                        "targets": 5,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+getEstados(row[5])+'</span>';

                        }
                    },

                    {
                        "targets": 6,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<a target="_blank" href="guia_remision_pdf.php?guia='+row[6]+'" style="display: block;margin: auto;text-align: center;"><button class="btn btn-primary"><i class="fa fa-file-pdf"></i></button></a>';

                        }
                    },

                ],
                language: {
                    url: '../assets/Spanish.json'
                }
        }
        );


    });


</script>

</html>
