<?php
require "../dao/GuiaRemisionDao.php";
require "../dao/GuiaConductorDao.php";
require "../dao/GuiaRemisionDetalleDao.php";
require "../dao/ClienteDao.php";
require "../dao/AgenciaTransporteDao.php";
require "../utils/Tools.php";
require_once('../../lib/mpdf/vendor/autoload.php');

$idGuia = $_GET['guia'];

$guiaRemisionDao= new GuiaRemisionDao();
$clienteDao= new ClienteDao();
$tools= new Tools();
$guiaRemisionDetalleDao = new GuiaRemisionDetalleDao();
$agenciaTransporteDao=new AgenciaTransporteDao();
$guiaConductorDao=new GuiaConductorDao();
$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);

$guiaRemisionDao->setGuiReId($idGuia);
$resGuia = $guiaRemisionDao->getData();
$guiaRemisionDetalleDao->setGuiReId($idGuia);
$guiaConductorDao->setGuiaRemiId($idGuia);
$resulProd = $guiaRemisionDetalleDao->getDatos();
$conductore=$guiaConductorDao->getDatas();

$logoGuia = "";
$empresa = "";
$empresaRuc="";
$direccionEmp="";
$serie_numero="";

$fechaEmision="";
$fechaTranslado="";
$motivo="";
$modalidar="";
$pesoBruto="";
$RazonDestinatario="";
$documentoDest="";
$direccionPartida="";
$direccionLlegada="";

$rucTransp="";
$razonTrans="";

$carModelo="";
$carplaca="";
$carmarcacc="";
$carDocCh="";
$carLiCh="";
$carTiDO="";


if ($rowGuia = $resGuia->fetch_assoc()){
    $logoGuia = $rowGuia['emp_logo'];
    $empresa = $rowGuia['emp_nombre'];
    $empresaRuc = $rowGuia['emp_ruc'];
    $direccionEmp = $rowGuia['emp_direccion'];
    $serie_numero= $rowGuia['serie']."-". $tools->numeroParaDocumento($rowGuia['numero'],6);

    $fechaEmision= date("d/m/Y", strtotime($rowGuia['fecha']));;
    $fechaTranslado="";
    $motivo=$rowGuia['motivo'];
    $modalidar=$rowGuia['tipo_transporte'];
    $pesoBruto=$rowGuia['peso_cargamento'];
    $RazonDestinatario=$rowGuia['cli_nomape'];
    $documentoDest=$rowGuia['cli_ndoc'];
    $direccionPartida=$rowGuia['emp_direccion'];

    $carModelo=$rowGuia['modelo'];
    $carplaca=$rowGuia['num_placa'];

    $carDocCh='';
    $carLiCh='';
    $carTiDO='';

    if ($rowGuia['direccion']=='0'){
        $direccionLlegada=$rowGuia['cli_direc'];
    }else{
        $resDirec = $clienteDao->getDirecion($rowGuia['direccion']);
        if ($rowClieDire = $resDirec->fetch_assoc()){
            $direccionLlegada=$rowClieDire['su_direccion'];
        }
    }
    $agenciaTransporteDao->setId($rowGuia['agencia_id']);
    $resAgencia = $agenciaTransporteDao->getDato();
    if ($rowAgencia = $resAgencia->fetch_assoc()){
        $rucTransp=$rowAgencia['ruc'];
        $razonTrans=$rowAgencia['razon_social'];
    }
    //echo  json_encode($rowGuia);
    $carmarcacc=$rowGuia['marca'];
}

$rowHtml="";
$rowHtmlCon="";
$count=1;

foreach ($conductore as $cond){
    $rowHtmlCon=$rowHtmlCon . "<tr>
        <td style='text-align: center;font-size: 13px'>$count</td>
        <td style='text-align: left;font-size: 13px'>{$cond['tipo_doc']}</td>
        <td style='text-align: left;font-size: 13px'>{$cond['numero_documento']}</td>
        <td style='text-align: left;font-size: 13px'>{$cond['numero_licencia']}</td>
      </tr>";
    $count++;
}
$count=1;
foreach ($resulProd as $prod){
    $rowHtml=$rowHtml."<tr>
        <th style='text-align: center;font-size: 12px'>$count</th>
        <td style='text-align: center;font-size: 12px'>{$prod['produ_sku']}</td>
        <td style='text-align: left;font-size: 12px'>{$prod['produ_nombre']}</td>
        <td style='text-align: center;font-size: 12px'>{$prod['unidad']}</td>
        <td style='text-align: center;font-size: 12px'>{$prod['cantidad']}</td>
      </tr>";
    $count++;
}


$html= "
<div style='width: 100%'>
    <div style='width: 50%;float: left'>
        <img style='max-width: 80%' src='../public/img/empresalogo/$logoGuia'>
        <div style='width: 100%; margin-top: 15px'>
        <strong>$empresa</strong>
        </div>
        <div style='width: 100%;font-size: 12px;'>
        $direccionEmp 
        </div>
        <div style='width: 100%'>
        <strong>Central Telefonica:</strong> 203-1300
        </div>
        <div style='width: 100%'>
        <strong>Email:</strong> facturacion@grupolodi.com
        </div>
    </div>
    <div style='width: 50%;float: left'>
        <div style='width: 80%;border: 2px solid black; margin: auto;padding: 10px'>
            <div style='width: 100%;text-align: center'>GUIA DE REMISION</div>
            <div style='width: 100%;text-align: center'>ELECTRONICA - REMITENTE</div>
            <div style='width: 100%;text-align: center'>RUC: $empresaRuc</div>
            <div style='width: 100%;text-align: center'>$serie_numero</div>
        </div>
    </div>
</div>
<div style='width: 100%;margin-top: 15px'>
    <div style='width: 100%;font-weight: bold;margin-bottom: 10px;'>DATOS DEL TRASLADO</div>
    <table style='width:100%;text-align: left'>
      <tr>
        <th style='text-align: left;width: 350px;font-size: 13px;'>Fecha de emision:</th>
        <td style='text-align: left;font-size: 13px;'>$fechaEmision</td>
      </tr>
      <tr>
        <th style='text-align: left;font-size: 13px'>Fecha de Inicio del translado:</th>
        <td style='text-align: left;font-size: 13px'>$fechaEmision</td>
      </tr>
      <tr>
        <th style='text-align: left;font-size: 13px'>Motivo de translado:</th>
        <td style='text-align: left;font-size: 13px'>$motivo</td>
      </tr>
      <tr>
        <th style='text-align: left;font-size: 13px'>Modalidad de transporte</th>
        <td style='text-align: left;font-size: 13px'>$modalidar</td>
      </tr>
      <tr>
        <th style='text-align: left;font-size: 13px'>Peso Bruto Total de la  Guia (KGM)</th>
        <td style='text-align: left;font-size: 13px'>$pesoBruto</td>
      </tr>
    </table>
</div>
<div style='width: 100%;margin-top: 15px'>
    <div style='width: 100%;font-weight: bold;margin-bottom: 10px;'>DATOS DEL DESTINATARIO</div>
    <table style='width:100%;text-align: left'>
      <tr>
        <th style='text-align: left;width: 350px;font-size: 13px'>Apellido y nombre, denominacion o razon:</th>
        <td style='text-align: left;font-size: 13px;'>$RazonDestinatario</td>
      </tr>
      <tr>
        <th style='text-align: left;font-size: 13px'>Documento de identidad:</th>
        <td style='text-align: left;font-size: 13px'>$documentoDest</td>
      </tr>
     
    </table>
</div>
<div style='width: 100%;margin-top: 15px'>
    <div style='width: 100%;font-weight: bold;margin-bottom: 10px;'>DATOS DEL PUNTO DE PARTIDA Y PUNTO DE LLEGADA</div>
    <table style='width:100%;text-align: left'>
      <tr>
        <th style='text-align: left;width: 350px;font-size: 13px'>Direccion del punto de partida:</th>
        <td style='text-align: left;font-size: 13px;'>$direccionPartida</td>
      </tr>
      <tr>
        <th style='text-align: left;font-size: 13px'>Direccion del punto de llegada:</th>
        <td style='text-align: left;font-size: 13px'>$direccionLlegada</td>
      </tr>
    </table>
</div>

<div style='width: 100%;margin-top: 15px'>
    <div style='width: 100%;font-weight: bold;margin-bottom: 10px;'>DATOS DEL TRANSPORTISTA</div>
    <table style='width:100%;text-align: left'>
      <tr style='background-color: #989898'>
        <th style='text-align: left;width: 350px; color: white;font-size: 13px'>Numero de Ruc:</th>
        <th style='text-align: left; color: white;font-size: 13px;'>Razon Social</th>
      </tr>
      <tr>
        <th style='text-align: left;font-size: 13px'>$rucTransp</th>
        <td style='text-align: left;font-size: 13px'>$razonTrans</td>
      </tr>
    </table>
</div>

<div style='width: 100%;margin-top: 15px'>
    <div style='width: 100%;font-weight: bold;margin-bottom: 10px;'>DATOS DEL TRANSPORTE</div>
    <div style='width: 100%;font-weight: bold;margin-bottom: 10px;'>Datos de los Vehiculos</div>
    <table style='width:100%;text-align: left'>
      <tr style='background-color: #989898'>
        <th style='text-align: left;width: 350px; color: white;font-size: 13px'>Nro. placa</th>
        <th style='text-align: left; color: white;font-size: 13px;'>Marca</th>
        <th style='text-align: left; color: white;font-size: 13px;'>Modelo</th>
      </tr>
      <tr>
        <th style='text-align: left;font-size: 13px'>$carplaca</th>
        <td style='text-align: left;font-size: 13px'>$carmarcacc</td>
        <td style='text-align: left;font-size: 13px'>$carModelo</td>
      </tr>
    </table>
</div>
<div style='width: 100%;margin-top: 15px'>
    <div style='width: 100%;font-weight: bold;margin-bottom: 10px;'>Datos de los Conductores</div>
    <table style='width:100%;text-align: left'>
      <tr style='background-color: #989898'>
        <th style='text-align: left;  color: white;font-size: 13px'>Nro.</th>
        <th style='text-align: left;  color: white;font-size: 13px'>Nro. Tipo doc.</th>
        <th style='text-align: left; color: white;font-size: 13px;'>Nro. documento</th>
        <th style='text-align: left; color: white;font-size: 13px;'>Lic. Conducir</th>
      </tr>
      $rowHtmlCon
    </table>
</div>
<div style='width: 100%;margin-top: 15px'>
    <div style='width: 100%;font-weight: bold;margin-bottom: 10px;'>DATOS DE LOS BIENES</div>
    <table style='width:100%;text-align: left'>
      <tr style='background-color: #989898'>
        <th style='text-align: left;width: 50px; color: white;font-size: 13px'>Nro</th>
        <th style='text-align: left; color: white;font-size: 13px'>Cod. bien</th>
        <th style='text-align: left; color: white;font-size: 13px;'>Descripción</th>
        <th style='text-align: left; color: white;font-size: 13px;'>Unidad de Medida</th>
        <th style='text-align: left; color: white;font-size: 13px;'>Cantidad</th>
      </tr>
      $rowHtml
    </table>
</div>
";
$mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

$mpdf->Output();

