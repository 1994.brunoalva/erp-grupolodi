<?php
header('Content-Type: application/json');

require ("../src/autoload.php");
$cookie = array(
    'cookie' 		=> array(
        'use' 		=> true,
        'file' 		=> __DIR__ . "/cookie.txt"
    )
);
$config = array(
    'representantes_legales' 	=> true,
    'cantidad_trabajadores' 	=> true,
    'establecimientos' 			=> true,
    'cookie' 					=> $cookie
);
$company = new \Sunat\tipo_cambio( $config );

$mes =$_GET['m'];
$año =$_GET['a'];

$result = $company->consulta( $mes , $año);

echo json_encode($result,JSON_PRETTY_PRINT);
?>
