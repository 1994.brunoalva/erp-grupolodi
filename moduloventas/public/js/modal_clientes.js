const APP_clientes  = new Vue({
    el:"#container-cliente",
    data:{
        fun_exe:function(){},
        editDirec:false,
        dataR:{
            tipoDoc:'',
            numDoc:'',
            nomRaz:'',
            estado:'',
            condicion:'',
            direccion:'',
            depart:'',
            provin:'',
            distrito:'',
            ubigeo:'',
            email1:'',
            email2:'',
            telf1:'',
            telf2:'',
            clas:'',
            contac:'1',
            vend:'',
            direcciones:[],
            representantes:[]

        },
        dataE:{
            idCli:'',
            tipoDoc:'',
            numDoc:'',
            nomRaz:'',
            estado:'',
            condicion:'',
            direccion:'',
            depart:'',
            provin:'',
            distrito:'',
            ubigeo:'',
            email1:'',
            email2:'',
            telf1:'',
            telf2:'',
            clas:'',
            contac:'1',
            vend:'',
            direcciones:[],
            representantes:[]

        },
        listProvincias:[],
        listDistritos:[],
        listProvincias2:[],
        listDistritos2:[],
        direcData:{
            depart:'',
            provin:'',
            distrito:'',
            direccion:''
        },
        tipeDirEA:0

    },
    methods:{
        addSwiDir(tipe){
            this.tipeDirEA= tipe;
        },
        eliminarItemDataR(index){
            this.dataR.direcciones.splice( index, 1 );
        },
        agregarDireccion(){
            const depar = this.direcData.depart
            const prov =this.direcData.provin
            const distr = this.direcData.distrito
            const dir =this.direcData.direccion
            if(this.tipeDirEA==2){
                this.dataR.direcciones.push({direc:dir,depar:'',provin:'',distri:'',ubigeo:depar+prov+distr,tipo:'OTROS',codigo:''})

            }else{
                this.dataE.direcciones.push({su_id:'0',su_direccion:dir,depar:'',provin:'',distri:'',ubigeo:depar+prov+distr,su_tipo:'OTROS',su_codigo:''});
            }
            this.direcData.depart=''
            this.direcData.provin=''
            this.direcData.distrito=''
            this.direcData.direccion=''
            console.log(depar+" "+prov+" "+distr+" "+dir)
            //this.renderDataTableRdic();
            $("#modal-direccion").modal('hide');
        },
        onlyNumber ($event) {
            //console.log($event.keyCode); //keyCodes value
            let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
            if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                $event.preventDefault();
            }
        },
        getDataCliente(clien){
            console.log(clien)
            $.ajax({
                type: "POST",
                url: "../ajaxs/ajs_cliente.php",
                data: {tipo:'get_data',clien},
                success: function (resp) {
                    console.log(resp)
                    resp= JSON.parse(resp)
                    console.log(resp)
                    APP_clientes.setDataEdit(resp);
                }
            });

        },
        setDataEdit(data){
            this.dataE = {
                idCli:data.cli_id,
                tipoDoc:data.cli_tdoc,
                numDoc:data.cli_ndoc,
                nomRaz:data.cli_nomape,
                estado:data.cli_estad,
                condicion:data.cli_condi,
                direccion:data.cli_direc,
                depart:data.dep_cod,
                provin:data.pro_cod,
                distrito:data.dis_cod,
                ubigeo:data.ubi_cod,
                email1:data.cli_mail1,
                email2:data.cli_mail2,
                telf1:data.cli_tele,
                telf2:data.cli_tele2,
                clas:data.cli_clasi,
                contac:data.cli_conta,
                vend:data.cli_vende,
                direcciones:data.direcciones,
                representantes:data.representantes


            };
            setTimeout(function () {
                $(".selectpicker").selectpicker('refresh');

            },100)
        },
        limpiarDataR(){
            this.dataR={
                tipoDoc:'',
                numDoc:'',
                nomRaz:'',
                estado:'',
                condicion:'',
                direccion:'',
                depart:'',
                provin:'',
                distrito:'',
                ubigeo:'',
                email1:'',
                email2:'',
                telf1:'',
                telf2:'',
                clas:'',
                contac:'1',
                vend:'',
                direcciones:[],
                representantes:[]

            };
        },
        setFuntExe(fun){
            this.fun_exe = fun;
        },
        ejecutarFun(data){
            this.fun_exe(data);
        },

        selectProvin(event) {
            const depa = this.dataR.depart;
            const prov =event.target.value;
            $.ajax({
                type: "POST",
                url: "../ajaxs/ajs_bd_datas.php",
                data: {tipo:'list_distri',dep:depa,pro:prov},
                success: function (respDistr) {
                    //console.log(respDistr)
                    APP_clientes._data.listDistritos=JSON.parse(respDistr);
                    setTimeout(function () {
                        $(".selectpicker").selectpicker('refresh');
                    },100)
                }
            });
        },
        selectDepart(event) {
            const depa =event.target.value;
            $.ajax({
                type: "POST",
                url: "../ajaxs/ajs_bd_datas.php",
                data: {tipo:'list_prov',dep:depa},
                success: function (respProv) {
                    //console.log(respProv)
                    APP_clientes._data.listProvincias=JSON.parse(respProv);
                    setTimeout(function () {
                        $(".selectpicker").selectpicker('refresh');
                    },100)

                }
            });
        },

        selectProvin2(event) {
            const depa = this.direcData.depart;
            const prov =event.target.value;
            $.ajax({
                type: "POST",
                url: "../ajaxs/ajs_bd_datas.php",
                data: {tipo:'list_distri',dep:depa,pro:prov},
                success: function (respDistr) {
                    //console.log(respDistr)
                    APP_clientes._data.listDistritos2=JSON.parse(respDistr);
                    setTimeout(function () {
                        $(".selectpicker").selectpicker('refresh');
                    },100)
                }
            });
        },
        selectDepart2(event) {
            const depa =event.target.value;
            $.ajax({
                type: "POST",
                url: "../ajaxs/ajs_bd_datas.php",
                data: {tipo:'list_prov',dep:depa},
                success: function (respProv) {
                    //console.log(respProv)
                    APP_clientes._data.listProvincias2=JSON.parse(respProv);
                    setTimeout(function () {
                        $(".selectpicker").selectpicker('refresh');
                    },100)

                }
            });
        },

        consultaDoc(){
            $.toast({
                heading: 'INFORMACION',
                text: "Consultando documento",
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
            if (this.dataR.numDoc.length==11){
                this.dataR.tipoDoc="RUC";
                const ruc =this.dataR.numDoc;
                $.ajax({
                    type: "POST",
                    url: "../utils/libSunat/sunat/example/consultaRuc.php",
                    data: {ruc},
                    success: function (resp) {
                        console.log(resp)
                        if (resp.success){
                            $.toast({
                                heading: 'EXITOSO',
                                text: "Informacion encontrada",
                                icon: 'success',
                                position: 'top-right',
                                hideAfter: '2500',
                            });

                            const dataConsulta =resp.result
                            APP_clientes._data.dataR.nomRaz=dataConsulta.razon_social;
                            APP_clientes._data.dataR.estado=dataConsulta.estado;
                            APP_clientes._data.dataR.condicion=dataConsulta.condicion;
                            APP_clientes._data.dataR.direccion=dataConsulta.direccion;
                            APP_clientes._data.dataR.representantes=dataConsulta.representantes_legales;
                            APP_clientes._data.dataR.depart='';
                            APP_clientes._data.dataR.provin='';
                            APP_clientes._data.dataR.distrito='';

                            if (dataConsulta.direccion.trim()=='-'){
                                APP_clientes._data.editDirec=true;
                            }else{
                                APP_clientes._data.editDirec=false;
                                const dirFrag = dataConsulta.direccion.split("-")
                                const distriTemp = dirFrag[dirFrag.length-1].trim();
                                const proviTemp = dirFrag[dirFrag.length-2].trim();
                                var deparTemp = dirFrag[0].trim();
                                const depFrag = deparTemp.split(" ");
                                if (depFrag[depFrag.length-1].search("LIBERTAD") != -1){
                                    deparTemp = 'LA LIBERTAD';
                                }else if (depFrag[depFrag.length-1].search("DIOS") != -1){
                                    deparTemp = 'MADRE DE DIOS';
                                }else if (depFrag[depFrag.length-1].search("MARTIN") != -1){
                                    deparTemp = 'SAN MARTIN';
                                }else{
                                    deparTemp = depFrag[depFrag.length-1];
                                }
                                //console.log(deparTemp+' - '+proviTemp+' - '+distriTemp)


                                $.ajax({
                                    type: "POST",
                                    url: "../ajaxs/ajs_bd_datas.php",
                                    data: {tipo:'cons_ubigeo',depar:deparTemp,provin:proviTemp,distri:distriTemp,ind:0},
                                    success: function (resp) {
                                        //console.log(resp)
                                        resp = JSON.parse(resp);
                                        APP_clientes._data.dataR.depart = resp.depa;
                                        APP_clientes._data.dataR.provin=resp.provin;
                                        APP_clientes._data.dataR.distrito=resp.distri;
                                        APP_clientes._data.dataR.ubigeo=resp.ubigeo;

                                        $.ajax({
                                            type: "POST",
                                            url: "../ajaxs/ajs_bd_datas.php",
                                            data: {tipo:'list_prov',dep:resp.depa},
                                            success: function (respProv) {
                                                //console.log(respProv)
                                                APP_clientes._data.listProvincias=JSON.parse(respProv);
                                                setTimeout(function () {
                                                    $(".selectpicker").selectpicker('refresh');
                                                },100)

                                            }
                                        });


                                        $.ajax({
                                            type: "POST",
                                            url: "../ajaxs/ajs_bd_datas.php",
                                            data: {tipo:'list_distri',dep:resp.depa,pro:resp.provin},
                                            success: function (respDistr) {
                                                //console.log(respDistr)
                                                APP_clientes._data.listDistritos=JSON.parse(respDistr);
                                                setTimeout(function () {
                                                    $(".selectpicker").selectpicker('refresh');
                                                },100)
                                            }
                                        });

                                    }
                                });
                            }



                            APP_clientes._data.dataR.email1='';
                            APP_clientes._data.dataR.email2='';
                            APP_clientes._data.dataR.telf1='';
                            APP_clientes._data.dataR.telf2='';
                            APP_clientes._data.dataR.clas='';
                            APP_clientes._data.dataR.contac='1';
                            APP_clientes._data.dataR.vend=''
                            APP_clientes.dataR_setDirecciones(dataConsulta.establecimientos);

                        }else{
                            $.toast({
                                heading: 'INFORMACION',
                                text: "Informacion no encontrada",
                                icon: 'info',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr)
                        $.toast({
                            heading: 'ERROR',
                            text: "Error en la sunat",
                            icon: 'error',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                    }
                });
            }else if (this.dataR.numDoc.length==8){
                this.dataR.tipoDoc="DNI";
                this.editDirec=true;
                const dni =this.dataR.numDoc;
                $.ajax({
                    type: "POST",
                    url: "../ajaxs/consulta_DNI.php",
                    data: {dni},
                    success: function (resp) {
                        resp=JSON.parse(resp);
                        console.log(resp)
                        if (resp.success){
                            APP_clientes._data.dataR.nomRaz=resp.result.paterno +" "+resp.result.materno+" "+resp.result.nombre;
                            APP_clientes._data.dataR.estado='';
                            APP_clientes._data.dataR.condicion='';
                            APP_clientes._data.dataR.direccion='';
                            APP_clientes._data.dataR.depart='';
                            APP_clientes._data.dataR.provin='';
                            APP_clientes._data.dataR.distrito='';
                            APP_clientes._data.dataR.email1='';
                            APP_clientes._data.dataR.email2='';
                            APP_clientes._data.dataR.telf1='';
                            APP_clientes._data.dataR.telf2='';
                            APP_clientes._data.dataR.clas='';
                            APP_clientes._data.dataR.contac='1';
                            APP_clientes._data.dataR.vend='';
                            APP_clientes._data.dataR.direcciones=[];
                            APP_clientes._data.dataR.representantes=[];
                        }else{

                        }

                    },
                });

            }else{
                $.toast({
                    heading: 'ERROR',
                    text: "El numero ingresado no es un DNI o RUC",
                    icon: 'warning',
                    position: 'top-right',
                    hideAfter: '2500',
                });
            }



        },
        consultaDocE(){
            $.toast({
                heading: 'INFORMACION',
                text: "Consultando documento",
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
            if (this.dataE.numDoc.length==11){
                this.dataE.tipoDoc="RUC";
                const ruc =this.dataE.numDoc;
                $.ajax({
                    type: "POST",
                    url: "../utils/libSunat/sunat/example/consultaRuc.php",
                    data: {ruc},
                    success: function (resp) {
                        console.log(resp)
                        if (resp.success){
                            $.toast({
                                heading: 'EXITOSO',
                                text: "Informacion encontrada",
                                icon: 'success',
                                position: 'top-right',
                                hideAfter: '2500',
                            });

                            const dataConsulta =resp.result
                            APP_clientes._data.dataE.nomRaz=dataConsulta.razon_social;
                            APP_clientes._data.dataE.estado=dataConsulta.estado;
                            APP_clientes._data.dataE.condicion=dataConsulta.condicion;
                            APP_clientes._data.dataE.direccion=dataConsulta.direccion;
                            APP_clientes.dataE_setRepresentantes(dataConsulta.representantes_legales);


                            if (dataConsulta.direccion.trim()=='-'){
                                APP_clientes._data.editDirec=true;
                            }else{
                                APP_clientes._data.editDirec=false;
                                const dirFrag = dataConsulta.direccion.split("-")
                                const distriTemp = dirFrag[dirFrag.length-1].trim();
                                const proviTemp = dirFrag[dirFrag.length-2].trim();
                                var deparTemp = dirFrag[0].trim();
                                const depFrag = deparTemp.split(" ");
                                if (depFrag[depFrag.length-1].search("LIBERTAD") != -1){
                                    deparTemp = 'LA LIBERTAD';
                                }else if (depFrag[depFrag.length-1].search("DIOS") != -1){
                                    deparTemp = 'MADRE DE DIOS';
                                }else if (depFrag[depFrag.length-1].search("MARTIN") != -1){
                                    deparTemp = 'SAN MARTIN';
                                }else{
                                    deparTemp = depFrag[depFrag.length-1];
                                }
                                //console.log(deparTemp+' - '+proviTemp+' - '+distriTemp)


                                $.ajax({
                                    type: "POST",
                                    url: "../ajaxs/ajs_bd_datas.php",
                                    data: {tipo:'cons_ubigeo',depar:deparTemp,provin:proviTemp,distri:distriTemp,ind:0},
                                    success: function (resp) {
                                        //console.log(resp)
                                        resp = JSON.parse(resp);
                                        APP_clientes._data.dataE.depart = resp.depa;
                                        APP_clientes._data.dataE.provin=resp.provin;
                                        APP_clientes._data.dataE.distrito=resp.distri;
                                        APP_clientes._data.dataE.ubigeo=resp.ubigeo;

                                        $.ajax({
                                            type: "POST",
                                            url: "../ajaxs/ajs_bd_datas.php",
                                            data: {tipo:'list_prov',dep:resp.depa},
                                            success: function (respProv) {
                                                //console.log(respProv)
                                                APP_clientes._data.listProvincias=JSON.parse(respProv);
                                                setTimeout(function () {
                                                    $(".selectpicker").selectpicker('refresh');
                                                },100)

                                            }
                                        });


                                        $.ajax({
                                            type: "POST",
                                            url: "../ajaxs/ajs_bd_datas.php",
                                            data: {tipo:'list_distri',dep:resp.depa,pro:resp.provin},
                                            success: function (respDistr) {
                                                //console.log(respDistr)
                                                APP_clientes._data.listDistritos=JSON.parse(respDistr);
                                                setTimeout(function () {
                                                    $(".selectpicker").selectpicker('refresh');
                                                },100)
                                            }
                                        });

                                    }
                                });
                            }




                            APP_clientes.dataE_setDirecciones(dataConsulta.establecimientos);

                        }else{
                            $.toast({
                                heading: 'INFORMACION',
                                text: "Informacion no encontrada",
                                icon: 'info',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr)
                        $.toast({
                            heading: 'ERROR',
                            text: "Error en la sunat",
                            icon: 'error',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                    }
                });
            }else if (this.dataR.numDoc.length==8){
                this.dataE.tipoDoc="DNI";
                this.editDirec=true;
                const dni =this.dataE.numDoc;
                $.ajax({
                    type: "POST",
                    url: "../ajaxs/consulta_DNI.php",
                    data: {dni},
                    success: function (resp) {
                        resp=JSON.parse(resp);
                        console.log(resp)
                        if (resp.success){
                            APP_clientes._data.dataE.nomRaz=resp.result.paterno +" "+resp.result.materno+" "+resp.result.nombre;
                            APP_clientes._data.dataE.estado='';
                            APP_clientes._data.dataR.condicion='';
                            APP_clientes._data.dataE.direccion='';
                            APP_clientes._data.dataE.depart='';
                            APP_clientes._data.dataE.provin='';
                            APP_clientes._data.dataE.distrito='';
                            APP_clientes._data.dataE.email1='';
                            APP_clientes._data.dataE.email2='';
                            APP_clientes._data.dataE.telf1='';
                            APP_clientes._data.dataE.telf2='';
                            APP_clientes._data.dataE.clas='';
                            APP_clientes._data.dataE.contac='1';
                            APP_clientes._data.dataE.vend='';
                            APP_clientes._data.dataE.direcciones=[];
                            APP_clientes._data.dataE.representantes=[];
                        }else{

                        }

                    },
                });

            }else{
                $.toast({
                    heading: 'ERROR',
                    text: "El numero ingresado no es un DNI o RUC",
                    icon: 'warning',
                    position: 'top-right',
                    hideAfter: '2500',
                });
            }



        },
        guardarCliente(){
            var  dataR = {...this.dataR};
            dataR.direcciones = JSON.stringify(dataR.direcciones)
            dataR.representantes = JSON.stringify(dataR.representantes)
            dataR.tipo='i'

            $.ajax({
                type: "POST",
                url: "../ajaxs/ajs_cliente.php",
                data: {numDoc:dataR.numDoc,tipo:'verificar'},
                success: function (respu) {
                    respu = JSON.parse(respu)
                    console.log(respu)
                    if(respu.res){
                        swal('Este cliente ya fue registrado')
                    }else{
                        $.ajax({
                            type: "POST",
                            url: "../ajaxs/ajs_cliente.php",
                            data: dataR,
                            success:  function (resp) {
                                console.log(resp)
                                if (isJson(resp)){
                                    resp = JSON.parse(resp);
                                    if (resp.res){
                                        APP_clientes.ejecutarFun(resp);
                                        $("#modal_register_cliente").modal('hide');
                                        APP_clientes.limpiarDataR()
                                        $.toast({
                                            heading: 'EXITOSO',
                                            text: "Registrado",
                                            icon: 'success',
                                            position: 'top-right',
                                            hideAfter: '2500',
                                        });
                                    }else{
                                        $.toast({
                                            heading: 'ALERTA',
                                            text: "No se pudo guardar",
                                            icon: 'warning',
                                            position: 'top-right',
                                            hideAfter: '2500',
                                        });
                                    }
                                }else{
                                    $.toast({
                                        heading: 'ERROR',
                                        text: "Error, en el servidor",
                                        icon: 'error',
                                        position: 'top-right',
                                        hideAfter: '2500',
                                    });
                                }
                            }
                        });
                    }
                }
            });

            /*
            */

        },
        actualizarCliente(){
            var  dataE = {...this.dataE};
            dataE.direcciones = JSON.stringify(dataE.direcciones)
            dataE.representantes = JSON.stringify(dataE.representantes)
            dataE.tipo='u'
            console.log(dataE);

            $.ajax({
                type: "POST",
                url: "../ajaxs/ajs_cliente.php",
                data: dataE,
                success:  function (resp) {
                    console.log(resp)
                    if (isJson(resp)){
                        resp = JSON.parse(resp);
                        if (resp.res){
                            APP_clientes.ejecutarFun(resp);
                            $("#modal_aditar_cliente").modal('hide');
                            APP_clientes.limpiarDataR()
                            $.toast({
                                heading: 'EXITOSO',
                                text: "Registrado",
                                icon: 'success',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        }else{
                            $.toast({
                                heading: 'ALERTA',
                                text: "No se pudo guardar",
                                icon: 'warning',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        }
                    }else{
                        $.toast({
                            heading: 'ERROR',
                            text: "Error, en el servidor",
                            icon: 'error',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                    }
                }
            });


            /*
            */

        },
        dataR_setDirecciones(direc){
            console.log(direc);
            this.dataR.direcciones=[];
            for(var i =0; i<direc.length;i++){
                const dirFrag = direc[i].Direccion.split("-")
                const distriTemp = dirFrag[dirFrag.length-1].trim();
                const proviTemp = dirFrag[dirFrag.length-2].trim();
                var deparTemp = dirFrag[0].trim();
                const depFrag = deparTemp.split(" ");
                if (depFrag[depFrag.length-1].search("LIBERTAD") != -1){
                    deparTemp = 'LA LIBERTAD';
                }else if (depFrag[depFrag.length-1].search("DIOS") != -1){
                    deparTemp = 'MADRE DE DIOS';
                }else if (depFrag[depFrag.length-1].search("MARTIN") != -1){
                    deparTemp = 'SAN MARTIN';
                }else{
                    deparTemp = depFrag[depFrag.length-1];
                }
                console.log(deparTemp+' - '+proviTemp+' - '+distriTemp)
                this.dataR.direcciones.push({direc:direc[i].Direccion,depar:deparTemp,provin:proviTemp,distri:distriTemp,ubigeo:'',tipo:direc[i].tipo,codigo:direc[i].codigo});
                console.log({direc:direc[i].Direccion,depar:deparTemp,provin:proviTemp,distri:distriTemp,ubigeo:'',tipo:direc[i].tipo,
                    codigo:direc[i].codigo})
                $.ajax({
                    type: "POST",
                    url: "../ajaxs/ajs_bd_datas.php",
                    data: {tipo:'cons_ubigeo',depar:deparTemp,provin:proviTemp,distri:distriTemp,ind:i},
                    success: function (resp) {
                        console.log(resp)
                        resp=JSON.parse(resp);
                        const ubigeo=resp.ubigeo;
                        console.log( parseInt(resp.index))
                        APP_clientes._data.dataR.direcciones[parseInt(resp.index)].ubigeo = ubigeo
                    }
                });
            }

        },
        dataE_setRepresentantes(data){
            for (var i=0; i<this.dataE.representantes.length;i++){
                if (this.dataE.representantes[i].rep_id == '0'){
                    this.dataE.representantes.splice( i, 1 );
                }
            }
            for (var x=0; x<data.length;x++){
                const tipoDataResul = this.dataE.representantes.find(dir=>dir.rep_nomape == data[x].rep_nomape);

                if ( tipoDataResul == undefined){


                    this.dataE.representantes.push({
                        rep_cargo: data[x].cargo,
                        rep_desde: data[x].desde,
                        rep_id: "0",
                        rep_ndoc: data[x].numdoc,
                        rep_nomape: data[x].nombre,
                        rep_ruc: '',
                        rep_tdoc: data[x].tipodoc
                    })
                }
            }
        },
        dataE_setDirecciones(direc){
            console.log(direc);
            const lengDir =  this.dataR.direcciones.length;
            for (var ii=0;ii<lengDir;ii++){
                if (this.dataR.direcciones[ii].su_id == '0'){
                    this.dataR.direcciones.splice( ii, 1 );
                }
            }
            for(var i =0; i<direc.length;i++){
                const tipoDataResul = this.dataE.direcciones.find(dir=>dir.su_direccion == direc[i].Direccion);

                if ( tipoDataResul == undefined){
                    const dirFrag = direc[i].Direccion.split("-")
                    const distriTemp = dirFrag[dirFrag.length-1].trim();
                    const proviTemp = dirFrag[dirFrag.length-2].trim();
                    var deparTemp = dirFrag[0].trim();
                    const depFrag = deparTemp.split(" ");
                    if (depFrag[depFrag.length-1].search("LIBERTAD") != -1){
                        deparTemp = 'LA LIBERTAD';
                    }else if (depFrag[depFrag.length-1].search("DIOS") != -1){
                        deparTemp = 'MADRE DE DIOS';
                    }else if (depFrag[depFrag.length-1].search("MARTIN") != -1){
                        deparTemp = 'SAN MARTIN';
                    }else{
                        deparTemp = depFrag[depFrag.length-1];
                    }
                    //console.log(deparTemp+' - '+proviTemp+' - '+distriTemp)

                    this.dataE.direcciones.push({su_id:'0',su_direccion:direc[i].Direccion,depar:deparTemp,provin:proviTemp,distri:distriTemp,ubigeo:'',su_tipo:direc[i].tipo,su_codigo:direc[i].codigo});


                    $.ajax({
                        type: "POST",
                        url: "../ajaxs/ajs_bd_datas.php",
                        data: {tipo:'cons_ubigeo',depar:deparTemp,provin:proviTemp,distri:distriTemp,ind:i},
                        success: function (resp) {
                            console.log(resp)
                            resp=JSON.parse(resp);
                            const ubigeo=resp.ubigeo;
                            console.log( parseInt(resp.index))
                            APP_clientes._data.dataE.direcciones[parseInt(resp.index)].ubigeo = ubigeo
                        }
                    });
                }

            }

        },
        renderDataTableRdic(){
            if (typeof tableRdic !== 'undefined'){
                tableRdic.clear().draw(true);
                this.dataR.direcciones.forEach(function (item,index) {
                    tableRdic.row.add( [item.direc,item.ubigeo,item.tipo,
                        "<button class='btn btn-danger' onclick='APP_clientes.eliminarItemDataR("+index+")'><i class='fa fa-times'></i></button>"] ).draw( false );
                })
            }

        },
        renderDataTableEdic(){
            if (typeof tableEdic !== 'undefined'){
                tableEdic.clear().draw(true);
                this.dataE.direcciones.forEach(function (item,index) {
                    tableEdic.row.add( ['<span style="display: block;margin: auto;text-align: center;">'+item.su_direccion+'</span>',
                        '<span style="display: block;margin: auto;text-align: center;">'+item.ubigeo+'</span>',
                        '<span style="display: block;margin: auto;text-align: center;">'+item.su_tipo+'</span>'] ).draw( false );
                })
            }

        }
    },
    computed:{
        observadorDirR(){
            this.renderDataTableRdic();
            return  this.dataR.direcciones.length;
        },
        observadorDirE(){
            this.renderDataTableEdic();
            return  this.dataE.direcciones.length;
        },
        ubigeoSelect(){
            this.dataR.ubigeo = this.dataR.depart+""+this.dataR.provin+""+this.dataR.distrito
            return this.dataR.ubigeo
        },
        ubigeoSelectE(){
            this.dataE.ubigeo = this.dataE.depart+""+this.dataE.provin+""+this.dataE.distrito
            return this.dataE.ubigeo
        }
    }
});
var tableRdic;
var tableEdic;
$( document ).ready(function() {
    tableRdic = $('#tabaRDirec').DataTable({
        "pageLength": 4,
        language: {
            url: '../assets/Spanish.json'
        }
    });
    tableEdic = $('#tabaEDirec').DataTable({
        "pageLength": 4,
        language: {
            url: '../assets/Spanish.json'
        }
    });
});