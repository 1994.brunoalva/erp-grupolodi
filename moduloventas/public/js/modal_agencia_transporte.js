const APP_agencia_transporte = new Vue({
    el:"#content-agencia-transporte",
    data:{
        funt_exe:function (){},
        listaConducR:[],
        indexTemConR:-1,
        listaConducE:[],
        indexTemConE:-1,
        dataR:{
            ruc:'',
            razon:'',
            direccion:'',
            estado:'',
            condicion:'',
            telefono:'',
            listaCarrosCon:[]
        },
        temRC:{
            isReg:false,
            placa:'',
            marca:'',
            modelo:''
        },
        temRCon:{
            isReg:false,
            nombre:'',
            doc:'',
            nro_doc:'',
            nro_lice:''
        },
        temEC:{
            isReg:false,
            placa:'',
            marca:'',
            modelo:''
        },
        temECon:{
            isReg:false,
            nombre:'',
            doc:'',
            nro_doc:'',
            nro_lice:''
        },
        dataE:{
            id:'',
            ruc:'',
            razon:'',
            direccion:'',
            estado:'',
            condicion:'',
            telefono:'',
            listaCarrosCon:[]
        },
    },
    methods:{

        addConDataR(){
            const datar = {...this.temRCon};
            this.dataR.listaCarrosCon[this.indexTemConR].conductores.push(datar)
            this.temRCon={
                isReg:false,
                nombre:'',
                doc:'',
                nro_doc:'',
                nro_lice:''
            };
        },
        addConDataE(){
            const datar = {...this.temECon};
            datar.carro_id = this.dataE.listaCarrosCon[this.indexTemConE].carr_id;
            datar.tipo="in-con";
            $.ajax({
                type: "POST",
                url:  "../ajaxs/ajs_agencia_transporte.php",
                data: datar,
                success: function (resp) {
                    console.log(resp)
                    APP_agencia_transporte._data.dataE.listaCarrosCon[ APP_agencia_transporte._data.indexTemConE].conductores.push(JSON.parse(resp))
                }
            });


           // this.dataR.listaCarrosCon[this.indexTemConR].conductores.push(datar)
            this.temECon={
                isReg:false,
                nombre:'',
                doc:'',
                nro_doc:'',
                nro_lice:''
            };
        },
        elimanrRegConR(index){
            this.dataR.listaCarrosCon[this.indexTemConR].conductores.splice(index, 1);
        },
        elimanrRegConE(index){
            const datar = this.dataE.listaCarrosCon[this.indexTemConE].conductores[index];
            $.ajax({
                type: "POST",
                url:  "../ajaxs/ajs_agencia_transporte.php",
                data: {tipo:'del-con',conduc:datar.conductor_id},
                success: function (resp) {
                    console.log(resp);
                    resp=JSON.parse(resp);
                    if (resp.res){
                        APP_agencia_transporte._data.dataE.listaCarrosCon[APP_agencia_transporte._data.indexTemConE].conductores.splice(index, 1);
                    }
                }
            });
            //this.dataR.listaCarrosCon[this.indexTemConR].conductores.splice(index, 1);
        },
        regConDataR(){
            this.temRCon={
                isReg:true,
                nombre:'',
                doc:'',
                nro_doc:'',
                nro_lice:''
            };
        },
        regConDataE(){
            this.temECon={
                isReg:true,
                nombre:'',
                doc:'',
                nro_doc:'',
                nro_lice:''
            };
        },
        seleccionConducR(index){
            console.log(index)
            this.listaConducR=this.dataR.listaCarrosCon[index].conductores;
            this.indexTemConR=index;
        },
        seleccionConducE(index){
            console.log(index)
            this.listaConducE=this.dataE.listaCarrosCon[index].conductores;
            this.indexTemConE=index;
        },
        elimanrRegCarR(index){
            this.dataR.listaCarrosCon.splice(index, 1);
        },
        elimanrRegCarE(index){
            const dt = this.dataE.listaCarrosCon[index]
            console.log(dt);
            $.ajax({
                type: "POST",
                url:  "../ajaxs/ajs_agencia_transporte.php",
                data: {tipo:'del-car',carro:dt.carr_id},
                success: function (resp) {
                    console.log(resp);
                    resp=JSON.parse(resp);
                    if (resp.res){
                        APP_agencia_transporte._data.dataE.listaCarrosCon.splice(index, 1);
                    }
                }
            });


        },
        addCarDataE(){

            var data = {...this.temEC};
            data.agencia=this.dataE.id;
            data.tipo='in-car';

            $.ajax({
                type: "POST",
                url: "../ajaxs/ajs_agencia_transporte.php",
                data: data,
                success: function (resp) {
                    console.log(resp);
                    APP_agencia_transporte._data.dataE.listaCarrosCon.push(JSON.parse(resp));
                }
            });

            this.temEC={
                isReg:false,
                placa:'',
                marca:'',
                modelo:''
            };

        },
        addCarDataR(){

            var data = {...this.temRC};
            data.conductores=[];
            this.dataR.listaCarrosCon.push(data)
            this.temRC={
                isReg:false,
                placa:'',
                marca:'',
                modelo:''
            };

        },
        regCarDataE(){
            this.temEC={
                isReg:true,
                placa:'',
                marca:'',
                modelo:''
            };
        },
        regCarDataR(){
            this.temRC={
                isReg:true,
                placa:'',
                marca:'',
                modelo:''
            };
        },
        exeFuncion(){
            this.funt_exe();
        },
        setFuntionExe(fun){
            this.funt_exe = fun;
        },
        setDatoEdit(idA){
            this.listaConducE=[],
                this.indexTemConE=-1,
            //console.log({id:idA,tipo:'s'})
            this.dataE.id=idA;
            $.ajax({
                type: "POST",
                url: "../ajaxs/ajs_agencia_transporte.php",
                data: {id:idA,tipo:'s'},
                success: function (resp) {
                    resp=JSON.parse(resp);
                    console.log(resp)
                    APP_agencia_transporte._data.dataE.razon=resp.razon_social;
                    APP_agencia_transporte._data.dataE.direccion=resp.direccion;
                    APP_agencia_transporte._data.dataE.estado=resp.estado;
                    APP_agencia_transporte._data.dataE.condicion=resp.condicion;
                    APP_agencia_transporte._data.dataE.telefono=resp.telefono;
                    APP_agencia_transporte._data.dataE.ruc=resp.ruc;
                    APP_agencia_transporte._data.dataE.listaCarrosCon=resp.carros;
                }
            });

        },
        consultaRUC(st){
            const ruc = st==1?APP_agencia_transporte._data.dataR.ruc:APP_agencia_transporte._data.dataE.ruc;
            $.toast({
                heading: 'INFORMACION',
                text: "Consultando ruc",
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });

            $.ajax({
                type: "POST",
                url: "../utils/libSunat/sunat/example/consultaRuc.php",
                data: {ruc},
                success: function (resp) {
                    console.log(resp)
                    if (resp.success){
                        $.toast({
                            heading: 'EXITOSO',
                            text: "Informacion encontrada",
                            icon: 'success',
                            position: 'top-right',
                            hideAfter: '2500',
                        });

                        const dataConsulta =resp.result
                        if (st==1){

                            APP_agencia_transporte._data.dataR.razon=dataConsulta.razon_social;
                            APP_agencia_transporte._data.dataR.direccion=dataConsulta.direccion;
                            APP_agencia_transporte._data.dataR.estado=dataConsulta.estado;
                            APP_agencia_transporte._data.dataR.condicion=dataConsulta.condicion;

                        }else{
                            APP_agencia_transporte._data.dataE.razon=dataConsulta.razon_social;
                            APP_agencia_transporte._data.dataE.direccion=dataConsulta.direccion;
                            APP_agencia_transporte._data.dataE.estado=dataConsulta.estado;
                            APP_agencia_transporte._data.dataE.condicion=dataConsulta.condicion;
                        }
                    }else{
                        $.toast({
                            heading: 'INFORMACION',
                            text: "Informacion no encontrada",
                            icon: 'info',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr)
                    $.toast({
                        heading: 'ERROR',
                        text: "Error en la sunat",
                        icon: 'error',
                        position: 'top-right',
                        hideAfter: '2500',
                    });
                }
            });
        },
        actualizar(){
            const datos = {...this.dataE};
            datos.tipo="u";
            $.ajax({
                type: "POST",
                url: "../ajaxs/ajs_agencia_transporte.php",
                data: datos,
                success: function (resp) {
                    console.log(resp)
                    if (isJson(resp)){
                        resp= JSON.parse(resp);
                        if (resp.res){
                            APP_agencia_transporte._data.dataE.id='';
                            APP_agencia_transporte._data.dataE.razon='';
                            APP_agencia_transporte._data.dataE.ruc='';
                            APP_agencia_transporte._data.dataE.direccion='';
                            APP_agencia_transporte._data.dataE.estado='';
                            APP_agencia_transporte._data.dataE.condicion='';
                            APP_agencia_transporte._data.dataE.telefono='';

                            APP_agencia_transporte.exeFuncion();
                            $('#modal_edit_agencia_transporte').modal('hide');

                        }else{
                            $.toast({
                                heading: 'INFORMACION',
                                text: "No se pudo guardar",
                                icon: 'info',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        }
                    }else{
                        $.toast({
                            heading: 'ERROR',
                            text: "Error en la servidor",
                            icon: 'error',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                    }
                }
            });

        },
        gregistrar(){
            const datos = {...this.dataR};
            datos.listaCarrosCon = JSON.stringify(datos.listaCarrosCon);
            datos.tipo="i";
            $.ajax({
                type: "POST",
                url: "../ajaxs/ajs_agencia_transporte.php",
                data: datos,
                success: function (resp) {
                    console.log(resp)
                    if (isJson(resp)){
                        resp= JSON.parse(resp);
                        if (resp.res){
                            APP_agencia_transporte._data.dataR.razon='';
                            APP_agencia_transporte._data.dataR.direccion='';
                            APP_agencia_transporte._data.dataR.estado='';
                            APP_agencia_transporte._data.dataR.condicion='';
                            APP_agencia_transporte._data.dataR.telefono='';
                            APP_agencia_transporte._data.dataR.ruc='';

                            APP_agencia_transporte.exeFuncion();
                            $('#modal_agregar_agencia_transporte').modal('hide');

                        }else{
                            $.toast({
                                heading: 'INFORMACION',
                                text: "No se pudo guardar",
                                icon: 'info',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        }
                    }else{
                        $.toast({
                            heading: 'ERROR',
                            text: "Error en la servidor",
                            icon: 'error',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                    }
                }
            });

        }
    }
});