<?php
require "../dao/VentaFacturacionDao.php";
require "../dao/ClienteDao.php";
require "../dao/VentaDetalleDao.php";
require "../dao/EmpresaDao.php";
require "../dao/VentaSunatDao.php";
require "../dao/TasaCambioDao.php";
require  "../utils/Tools.php";
require_once('../../lib/mpdf/vendor/autoload.php');
require_once('../../lib/vendor/autoload.php');
use Endroid\QrCode\QrCode;
use Luecano\NumeroALetras\NumeroALetras;

$idVenta = $_GET['venta'];

$tools = new Tools();
$formatter = new NumeroALetras;


$ventaFacturacionDao = new VentaFacturacionDao();
$ventaDetalleDao = new VentaDetalleDao();
$clienteDao= new ClienteDao();
$empresaDao = new EmpresaDao();
$ventaSunatDao= new VentaSunatDao();
$tasaCambioDao= new TasaCambioDao();

$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);


$ventaFacturacionDao->setFacId($idVenta);
$ventaDetalleDao->setFacId($idVenta);
$ventaSunatDao->setIdVentaSunat($idVenta);


$listaProd = $ventaDetalleDao->getProductos();

$resultC = $ventaFacturacionDao->getdataVenta()->fetch_assoc();

$empresaDao->setEmpId($resultC['emp_id']);
$tasaCambioDao->setTasId($resultC['tas_id']);

$restEmpre = $empresaDao->getData()->fetch_assoc();

$resDataTasaCambio = $tasaCambioDao->getDatos()->fetch_assoc();
//echo  json_encode($resultC);
$direccionClientFac ='';


    $clienteDao->setCliId($resultC['cli_id']);
    $resultClie =$clienteDao->getdata()->fetch_assoc();

    if ($resultC['direccion']==0){
        //echo json_encode($resultClie);
        $direccionClientFac=$resultClie['cli_direc'];
    }else{
        $dirClieFa = $clienteDao->exeSQL("SELECT * FROM sys_ven_clientes_sucursales WHERE su_ruc='{$resultClie['cli_ndoc']}' AND su_id=".$resultC['direccion'])->fetch_assoc();
        $direccionClientFac=$dirClieFa['su_direccion'];
    }


//echo $resultC['direccion'].'    '.$direccionClientFac;
//echo json_encode($resultC);
$rowHTML ="";
$subtotal = 0;
$contador =1;

$totalOpGratuita=0;
$totalOpExonerada=0;
$totalOpinafec=0;
$totalOpgravado=0;
$totalDescuento=0;
$totalOpinafecta=0;
$SC=0;
$percepcion=0;

$totalOpGratuita = number_format($totalOpGratuita, 2, '.', ',');
$totalOpExonerada = number_format($totalOpExonerada, 2, '.', ',');
$totalOpinafec = number_format($totalOpinafec, 2, '.', ',');
$totalOpgravado = number_format($totalOpgravado, 2, '.', ',');
$totalDescuento = number_format($totalDescuento, 2, '.', ',');
$totalOpinafecta = number_format($totalOpinafecta, 2, '.', ',');
$SC = number_format($SC, 2, '.', ',');
$percepcion = number_format($percepcion, 2, '.', ',');


foreach ($listaProd as $prod ){

    $precio =  $prod['facd_preciou'];
    $importe = $precio * $prod['facd_cantidad'];
    $subtotal = $subtotal + $importe;
    $precio = number_format($precio, 2, '.', ',');
    $importe = number_format($importe, 2, '.', ',');
    $rowHTML = $rowHTML . "
      <tr>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;'>$contador</td>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;'>{$prod['facd_cantidad']}</td>
        <td class='' style=' font-size: 11px; text-align: left;border-left: 1px solid #363636;'>{$prod['produ_nombre']}</td>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;'>$precio</td>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;'>0.00</td>
        
        
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;border-right: 1px solid #363636;'>$importe</td>
      </tr>
    ";
    $contador++;
}
$total = $subtotal;
$subtotal = $subtotal /1.18;
$igv = $total-$subtotal ;
//$total = $subtotal+ $igv;

$TipoMoneda = $resultC['sun_id']==2?'DOLARES':'SOLES';
$totalLetras =   $formatter->toInvoice(number_format($total, 2, '.', ''), 2, $TipoMoneda);
$subtotal= number_format($subtotal, 2, '.', ',');
$igv =number_format($igv, 2, '.', ',');
$total =number_format($total, 2, '.', ',');

$dataDocumento = strlen($resultC['cli_ndoc'])>8?"RUC":"DNI";

$stylesheet = file_get_contents('../public/css/stylepdf.css');

$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);

$tipoDocNom = $resultC['tip_docu_sunat']==1?'FACTURA':'BOLETA';

$S_N = $resultC['fac_serie']. " - ". $tools->numeroParaDocumento($resultC['fac_num'],6);

$dataContQR = '';
$dataHashSunat = 'efsssssssssssssssssssss';
if ($rowSD = $ventaSunatDao->getData()->fetch_assoc()){
    $dataContQR = $rowSD['dataQR'];
    $dataHashSunat = $rowSD['hash_v'];
}



$qrCode = new QrCode($dataContQR);
$qrCode->setSize(150);
$image= $qrCode->writeString();//Salida en formato de texto
$imageData = base64_encode($image);
$qrImage='<img style="width: 130px;" src="data:image/png;base64,'.$imageData.'">';

$cntRowEE=37;
$rowHTMLTERT="";
for ($tert =0; $tert<$cntRowEE-$contador;$tert++){
    $rowHTMLTERT=$rowHTMLTERT." <tr>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636; color: white'>.</td>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636; '> </td>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636; '> </td>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636; '> </td>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636; '> </td>
        
        
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;border-right: 1px solid #363636;'> </td>
      </tr>";
}



/*$mpdf->SetDefaultBodyCSS("background","url('../public/img/cotilodi.png");
$mpdf->SetDefaultBodyCSS('background-image-resize', 6);*/

$htmlCuadroHead="<div style=' width: 34%;text-align: center; background-color: #ffffff ; float: right;'>

<div style='width: 100%; height: 100px; border: 2px solid #1e1e1e' class=''>
<div style='margin-top:10px'></div>
<span>RUC: {$restEmpre['emp_ruc']}</span><br>
<div style='margin-top: 10px'></div>
<span><strong>$tipoDocNom ELECTRONICA</strong></span><br>
<div style='margin-top: 10px'></div>
<span>Nro. $S_N </span>
</div>
</div>
</div>";

$mpdf->WriteFixedPosHTML("<img style='max-width: 300px' src='../public/img/empresalogo/{$restEmpre['emp_logo']}'>",15,5,150,120);
$mpdf->WriteFixedPosHTML($htmlCuadroHead,0,5,195,130);
$mpdf->WriteFixedPosHTML("<span style=' font-size: 12px'><strong>Central Telefonico: </strong> 203-1300</span>",15,27,210,130);
$mpdf->WriteFixedPosHTML("<span style=' font-size: 12px'><strong>Email: </strong> facturacion@grupolodi.com</span>",15,32,210,130);
$mpdf->WriteFixedPosHTML("<span style=' font-size: 12px'><strong>Direccion:</strong> {$restEmpre['emp_direccion']}</span>",15,37,210,130);

//$mpdf->WriteFixedPosHTML("<span style=' font-size: 13px'><strong>FECHA DE EMISION: {$resultC['fac_fechae']}</strong></span>",124,32,210,130);

$formaPago = $resultC['pag_id'] == 2?'CONTADO':'CREDITO';


$html= "<div style='width: 1000%;padding-top: 110px; overflow: hidden;clear: both;'>
<div style='width: 100%;border: 1px solid black'>
<div style='width: 55%; float: left; '>

<table style='width:100%'>
  <tr>
    <td style=' font-size: 11px;text-align: left'><strong>$dataDocumento:</strong></td>
    <td style=' font-size: 11px;'>{$resultC['cli_ndoc']}</td>
  </tr>
  <tr>
    <td style=' font-size: 11px;text-align: left'><strong>CLIENTE:</strong></td>
    <td style=' font-size: 11px;'>{$resultC['cli_nomape']}</td>
  </tr>
  <tr>
    <td style=' font-size: 11px;text-align: left'><strong>DIRECCION:</strong></td>
    <td style=' font-size: 11px;'>$direccionClientFac</td>
  </tr>
</table>
</div>
<div style='width: 45%; float: left'>
<table style='width:100%'>
  <tr>
    <td style=' font-size: 11px;text-align: left'><strong>TIPO DE PAGO:</strong></td>
    <td style=' font-size: 11px;'>$formaPago</td>
  </tr>
  <tr>
    <td style=' font-size: 11px;text-align: left'><strong>VENDEDOR:</strong></td>
    <td style=' font-size: 11px;'>BRUNO</td>
  </tr>
   <tr>
    <td style=' font-size: 11px;text-align: left'><strong>MONEDA:</strong></td>
    <td style=' font-size: 11px;'>$TipoMoneda</td>
  </tr>
</table>
</div>
</div>


</div>

<div style='width: 100%; padding-top: 20px;'>
<table style='width:100%;border-bottom: 1px solid #363636;border-collapse: collapse;'>
    <tr style='border-bottom: 1px solid #363636;border-collapse: collapse;'>
    <td style=' font-size: 12px;text-align: center; color: #000000;border: 1px solid #363636;border-collapse: collapse;'><strong>ITEM</strong></td>
    <td style=' font-size: 12px;text-align: center; color: #000000;border: 1px solid #363636;border-collapse: collapse;'><strong>CANT</strong></td>
    <td style=' font-size: 12px;text-align: center; color: #000000;border: 1px solid #363636;border-collapse: collapse;'><strong>DESCRIPCION</strong></td>
    <td style=' font-size: 12px;text-align: center; color: #000000;border: 1px solid #363636;border-collapse: collapse;'><strong>PRECIO U.</strong></td>
    <td style=' font-size: 12px;text-align: center; color: #000000;border: 1px solid #363636;border-collapse: collapse;'><strong>DESCUENTO.</strong></td>
    <td style=' font-size: 12px;text-align: center; color: #000000;border: 1px solid #363636;border-collapse: collapse;'><strong>IMPORTE</strong></td>
    
  </tr>
  $rowHTML
  $rowHTMLTERT
      <tr>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;border-bottom: 1px solid #363636;color: white'>.</td>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;border-bottom: 1px solid #363636;'> </td>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;border-bottom: 1px solid #363636;'> </td>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;border-bottom: 1px solid #363636;'> </td>
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;border-bottom: 1px solid #363636;'> </td>
        
        
        <td class='' style=' font-size: 11px; text-align: center;border-left: 1px solid #363636;border-right: 1px solid #363636;border-bottom: 1px solid #363636;'> </td>
      </tr>
 

</table>
</div>

";


$mpdf->SetHTMLFooter("
<!--div style='width: 100%; padding-bottom: 5px; font-size: 10px;'>Tipo cambio: {$resDataTasaCambio['tas_comercial_venta']} Valido solo {$resDataTasaCambio['tas_fecha']} || S/. $total</div-->
<div style='height: 10px;width: 100%; padding-bottom: 0px;font-size: 10px;border: 1px solid black;'>. SON: | $totalLetras</div>
<!--div style='width: 100%; padding-bottom: 0px;font-size: 10px;'>HASH SUNAT: $dataHashSunat</div-->

<div style='width: 100%; height: 10px;margin-top: 3px;'>
<div style='float: left; width: 20%;height: 10px '>
$qrImage

<div style='position: absolute; left: 80px; top: 90px;'></div>

</div>
 <div style='width: 50%; padding-bottom: 5px;font-size: 12px; float: left; padding-top: 10px;'>
    <div style='width: 100%'>Observaciones:</div>
    <div style='width: 95%; border: 1px solid black; padding: 3px; font-size: 10px;height: 90px '>{$resultC['observaciones']}</div>
 </div>
 <div style='width: 30%;'>
 <table style='width: 100%;border-top: 1px solid #363636;border-bottom: 1px solid #363636;border-right: 1px solid #363636;border-collapse: collapse;'>
  <tr>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px; text-align: right'>Total Op. Gratuita</td>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px;  text-align: right' >$totalOpGratuita</td>
  </tr>
  <tr>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px; text-align: right'>Total Op. Exonerada:</td>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px;  text-align: right' >$totalOpExonerada</td>
  </tr>
   <tr>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px; text-align: right'>Total Op. Inafecta</td>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px;  text-align: right' >$totalOpinafec</td>
  </tr>
  <tr>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px; text-align: right'>Total Op. Gravado:</td>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px;  text-align: right' >$subtotal</td>
  </tr>
  <tr>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px; text-align: right'>Total Descuento:</td>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px;  text-align: right' >$totalDescuento</td>
  </tr>
  ¿
  <tr>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px; text-align: right'>Total Op. Inafecta</td>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px;  text-align: right' >$totalOpinafecta</td>
  </tr>
  
  
  <tr>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px; text-align: right'>SC:</td>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px;  text-align: right' >$SC</td>
  </tr>
  <tr>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px; text-align: right'>IGV:</td>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px;  text-align: right' >$igv</td>
  </tr>
   <tr>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px; text-align: right'>Importe Total:</td>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px;  text-align: right' >$total</td>
  </tr>
  <tr>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px; text-align: right'>Percepción:</td>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px;  text-align: right' >$percepcion</td>
  </tr>
  <tr>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px; text-align: right'>Total a Pagar</td>
    <td style='border-left: 1px solid #363636;border-collapse: collapse; font-size: 12px;  text-align: right' >$total</td>
  </tr>
  
</table>
    </div>
</div> 
");
//echo "<style>$stylesheet</style>".$html;



$mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

$mpdf->Output();







