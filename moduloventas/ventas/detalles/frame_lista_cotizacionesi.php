<div id="exTab1" class="container-fluid">
    <div style="text-align: right">
       <a href="./registro_cotizacion.php?venta=" class="btn btn-lodi-green">Nueva Venta</a>
       <a href="./registro_cotizacion.php?cotizacion=" class="btn btn-primary">Nueva Cotizacion</a>
    </div>

    <table id="tabal-cotizaciones" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>

            <th>TD</th>
            <th>NUM. DOCUMENTO</th>
            <th>CLIENTE</th>
            <th>VENDEDOR</th>
            <th>MONTO</th>
            <th>FECHA</th>
            <th>ESTADO</th>
            <th>VER</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $listaCoti=$cotizacion->verLista();
        $contador=1;
        foreach ($listaCoti as $item){
            $simbolo ='';
            if ($item['coti_tp_moneda']==1){
                $simbolo ='$ ';
            }else{
                $simbolo ='S/. ';
            }
            ?>
            <tr>
                <td style="text-align: center"><?php echo  strlen($item['coti_ruc'])==8?'DNI':'RUC' ?></td>
                <td style="text-align: center"><?php echo  $item['coti_ruc'] ?></td>
                <td><?php echo  $item['coti_razon'] ?></td>
                <td><?php echo  $item['coti_ate'] ?></td>
                <td><?php echo $simbolo. $item['coti_total'] ?></td>
                <td><?php echo  $item['coti_fecha'] ?></td>
                <td style="background-color: #fff7a7; text-align: center"><?php echo  $item['estado_nombre'] ?></td>
                <td style="text-align: center">
                    <a title="Ver Cotizacion" href="registro_cotizacion.php?view=<?php echo  $item['coti_id'] ?>" class="btn btn-warning"> <i class="fa fa-eye"></i> </a>
                </td>
            </tr>
        <?php $contador++; }    ?>



    </table>
</div>


<style>
    .color-modal-header {
        background: #0866c6;
        color: #FFFFFF;
    }

    .row .pd-lodi {
        margin: 0.4em 0
    }

    .bootstrap-select {
        padding: 0;
    }

    .pd-lodi label {
        padding: 0;
    }

    .panel-folder {
        border: 1px solid rgba(3, 1, 214, 0.28);
        padding-top: 1em;
        padding-bottom: 1em;
        border-radius: 4px;
    }

    .panel-folder .row-filter {
        padding-top: 14px;
    }

    .panel-folder .serie {
        padding-top: 25px;
    }

    .panel-folder .row .pd-lodi > .ord-data {
        background: rgba(201, 255, 66, 0.32);
    }

    .orden-select .filter-option-inner-inner {
        color: #ed1404;
    }

    .form-horizontal .form-group {
        padding-left: 0;
        padding-right: 0;
    }

    .hpanel {
        background: #FFFFFF;
        border: 1px solid rgba(197, 156, 255, 0.5);
        margin-bottom: 12px;;
    }

    .btn-lodi-green {
        color: #FFFFFF !important;
        background: #14B721;
        border: 1px solid #109418;
    }

    .btn-lodi-green:hover {
        color: #FFFFFF !important;
        background: rgba(20, 183, 33, 0.82);
    }

    .btn-lodi-blue {
        color: #FFFFFF !important;
        background: #3379B6;
    }
</style>