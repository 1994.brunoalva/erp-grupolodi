<div id="exTab1" class="container-fluid">
    <ul class="nav nav-pills pilas">
        <li class="active" style="border:0px">
            <a href="#1a" data-toggle="tab">VENTAS</a>
        </li>
        <li><a href="#2a" data-toggle="tab">REPORTE DE VENTAS</a>
        </li>
        <li><a href="#3a" data-toggle="tab">UTILIDADES</a>
        </li>
        <li><a href="#4a" data-toggle="tab">CLIENTES</a>
        </li>
    </ul>
    <div class="tab-content clearfix contenedor-pilas bg-info" style="background: rgba(41,237,215,0.14);">
        <div class="tab-pane active panel-folder" id="1a">
            <div class="content animate-panel">
                <div class="container-fluid">
                    <div class="col-md-8" style="padding-left: 0;">
                        <!-- begin panel -->
                        <div class="hpanel ">
                            <div class="panel-heading btn-lodi-blue">
                                <h4 class="panel-title">Buscar Productos</h4>
                            </div>
                            <div class="panel-body" style="padding-right: 15px;">
                                <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8 pd-lodi">
                                    <label class="col-xs-12" for="">BUSCAR PRODUCTO :</label>

                                    <div class="input-group  col-xs-12 no-padding">
                                        <input id="venta-input-buscar-producto" type="text" class="form-control" value="">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    id="btn_modal_orden"
                                                    data-target="#modal_producto" data-dir="up">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </span>
                                        <?php
                                        /* include 'modal/add_orden_proveedor.php';*/
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-4 pd-lodi">
                                    <label class="col-xs-12 invisible" for="">AGREGAR ITEM :</label>
                                    <button id="btn-agregar" type="button" class="btn btn-lodi-green btn-sm form-control">
                                        <i class="fa fa-plus"></i> Agregar Item
                                    </button>
                                    <!--<input type="text" class="form-control" value="">-->
                                </div>
                                <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-3 pd-lodi">
                                    <label class="col-xs-12" for="">STOCK :</label>
                                    <input id="stock" type="text" disabled class="form-control" value="">
                                </div>
                                <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-3 pd-lodi">
                                    <label class="col-xs-12" for="">LOTE :</label>
                                    <input  id="lote" type="text" disabled class="form-control" value="">
                                </div>
                                <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-3 pd-lodi">
                                    <label class="col-xs-12" for="">PRECIO VENTA :</label>
                                    <input id="precio" type="text" disabled class="form-control" value="">
                                </div>
                                <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-3 pd-lodi">
                                    <label class="col-xs-12" for="">CANTIDAD :</label>
                                    <div class="input-group number-spinner col-xs-12 no-padding">
                                    <span class="input-group-btn">
                                        <a class="btn btn-lodi-green" data-dir="dwn">
                                            <span
                                                class="fa fa-minus">
                                            </span>
                                        </a>
                                    </span>
                                        <input id="cantidad" type="text" class="form-control text-center"
                                               value="1">
                                        <span class="input-group-btn">
                                        <a class="btn btn-lodi-green" data-dir="up">
                                            <span class="fa fa-plus"></span>
                                        </a>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->

                        <!-- begin panel -->
                        <div class="hpanel">
                            <div class="panel-heading btn-lodi-blue">
                                <h4 class="panel-title">Productos Agregados</h4>
                            </div>
                            <div class="panel-body">
                                <table id="tabla-detalle" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th width="4%">Item</th>
                                        <th width="40%">Producto</th>
                                        <th width="11%">Cant.</th>
                                        <th width="10%">Precio</th>
                                        <th width="10%">sub Total</th>
                                        <th width="11%">Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                    <div class="col-md-4 hpanel" style="background: #FFFFFF;padding-top: 10px;">
                        <div class="widget padding-0 white-bg">
                            <div class="bg-primary pv-15 text-center "
                                 style="height: 90px; text-align: center; padding-top: 3px">
                                <h1 class="mv-0 font-400" id="lbl_suma_pedido">S/ 0.00</h1>
                                <div class="text-uppercase">Suma Pedido</div>
                            </div>
                            <br>
                            <div class="padding-20 text-center">
                                <form role="form" class="form-horizontal" method="post">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Doc.</label>
                                        <div class="col-md-9">
                                            <select class="form-control" name="select_documento"
                                                    id="venta_select_serie_sunat">
                                                <option>-Seleccione-</option>
                                                <option>FACTURA</option>
                                                <option>BOLETA</option>
                                                <option>NOTA DE VENTA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">S - N</label>
                                        <div class="col-lg-3">
                                            <input id="venta_input_serie" type="text" class="form-control text-center"
                                                   value="B001"
                                                   readonly>
                                        </div>
                                        <div class="col-lg-5">
                                            <input id="venta_input_numero" type="text" class="form-control text-center"
                                                   value="508"
                                                   readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Fecha</label>
                                        <?php
                                        $fecha = date('Y-m-d');
                                        ?>
                                        <div class="col-lg-6">
                                            <input class="form-control text-center" value="<?php echo $fecha;?>" readonly>
                                        </div>
                                    </div>
                                    <div id="error_documento">
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-8 col-md-12 col-lg-12 pd-lodi text-left">
                                        <label class="col-xs-12 no-padding">CLIENTE:</label>
                                        <div class="input-group col-xs-12 no-padding">
                                            <div class="input-group col-xs-12 no-padding">
                                                <input id="venta-input_cliente" class="form-control" type="text"
                                                       placeholder="Haga click para agregar cliente">
                                                <span class="input-group-btn">
                                                        <button id="venta-btn-add-proveedor" type="button" class="btn btn-primary"
                                                                data-toggle="modal" data-target="#modal_empresa">
                                                            <i class="fa fa-plus"></i></button>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="form-group col-xs-12 col-sm-8 col-md-12 col-lg-12 pd-lodi">
                                        <label class="col-xs-12 text-left" for="">CLIENTE :</label>




                                        </div>
                                    </div>-->
                                    <div class="form-group col-xs-12 col-sm-8 col-md-12 col-lg-12 pd-lodi">
                                        <input id="ruc" class="form-control" placeholder="N° Ruc">
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-8 col-md-12 col-lg-12 pd-lodi">
                                        <input id="direccion" class="form-control" placeholder="Direccion">
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <input type="hidden" id="hidden_total" name="hidden_total">
                                            <button type="button" class="btn btn-md btn-primary"
                                                    id="btn_finalizar_pedido" disabled
                                                    onclick="enviar_formulario()">Guardar
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="tab-pane" id="2a">
            <h3>MI REPORTE VENTA EN CONSTRUCCION
            </h3>
        </div>
        <div class="tab-pane" id="3a">
            <h3>UTILIDADES EN CONSTRUCCION</h3>
        </div>
        <div class="tab-pane" id="4a">
            <div class="table-responsive form-group col-xs-12 no-padding">
                <table id="table-empresa" class="table table-striped table-bordered table-hover">
                    <thead class="bg-head-table">
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-left">NOMBRE</th>
                        <th class="text-left">RUC</th>
                        <th class="text-left">TELEFONO</th>
                        <th class="text-left">DIRECCION</th>
                        <th class="text-left">ESTADO</th>
                       <!-- <th class="text-left">OPCION</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $empresa = new Empresa('SELECT');
                    $resultEmp = $empresa->selectAll();
                    $indice = 0;
                    foreach ($resultEmp as $item) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo ++$indice; ?></td>
                            <td class="text-left">
                                <label><?php echo $item->emp_nombre; ?></label>
                            </td>
                            <td class="text-left">
                                <div id="emp_num_ruc" ><?php echo $item->emp_ruc; ?></div>
                            </td>
                            <td class="text-left"><?php echo $item->emp_tel; ?></td>
                            <td class="text-left"><?php echo $item->emp_dir; ?></td>
                            <td class="text-center"><?php echo ($item->emp_estado) ?'ACTIVO':'INACTIVO'; ?></td>
                            <!--<td class="text-center">
                                <a id="btn-" class="btn btn-sm btn-danger fa fa-check btn-option"
                                   title="Anadir item" data-dismiss="modal"></a>
                                <input class="emp_id no-display" type="text" value="<?php /*echo $item->emp_id; */?>">
                            </td>-->
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>


<style>
    .color-modal-header {
        background: #0866c6;
        color: #FFFFFF;
    }

    .row .pd-lodi {
        margin: 0.4em 0
    }

    .bootstrap-select {
        padding: 0;
    }

    .pd-lodi label {
        padding: 0;
    }

    .panel-folder {
        border: 1px solid rgba(3, 1, 214, 0.28);
        padding-top: 1em;
        padding-bottom: 1em;
        border-radius: 4px;
    }

    .panel-folder .row-filter {
        padding-top: 14px;
    }

    .panel-folder .serie {
        padding-top: 25px;
    }

    .panel-folder .row .pd-lodi > .ord-data {
        background: rgba(201, 255, 66, 0.32);
    }

    .orden-select .filter-option-inner-inner {
        color: #ed1404;
    }

    .form-horizontal .form-group {
        padding-left: 0;
        padding-right: 0;
    }

    .hpanel {
        background: #FFFFFF;
        border: 1px solid rgba(197, 156, 255, 0.5);
        margin-bottom: 12px;;
    }

    .btn-lodi-green {
        color: #FFFFFF !important;
        background: #14B721;
        border: 1px solid #109418;
    }

    .btn-lodi-green:hover {
        color: #FFFFFF !important;
        background: rgba(20, 183, 33, 0.82);
    }

    .btn-lodi-blue {
        color: #FFFFFF !important;
        background: #3379B6;
    }
</style>