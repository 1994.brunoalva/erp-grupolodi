<?php
$indexRuta=1;
require '../conexion/Conexion.php';
require '../model/TipoPago.php';
require_once '../model/model.php';
$tipoPago = new TipoPago();
$categorias= new Categoria('SELECT');

$listaPa= $tipoPago->lista();
$listaTemTP = [];

foreach ($listaPa as $item){
    $listaTemTP []= $item;
}


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <script src="../aConfig/plugins/sweetalert2/sweetalert2.min.css"></script>

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Productos</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <!--<div class="col-lg-6 text-right">
                                        <a href="new-folder.php" id="folder_btn_nuevo_folder" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Folder
                                        </a>

                                    </div>-->

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div id="conten-modales">
                                    <table id="table-productos" class="table table-striped table-bordered table-hover">
                                        <thead class="bg-head-table">
                                        <tr>

                                            <th class="text-left">PRODUCTO</th>
                                            <th class="text-left">SKU</th>
                                            <th class="text-left">STOCK</th>
                                            <th class="text-left">PRECIO</th>
                                            <th class="text-left">MARCA</th>
                                            <th class="text-left">PAIS</th>
                                            <th class="text-left">EMPRESA</th>
                                            <th class="text-left">OPCION</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr v-for="(pro, index) in productos.lista" >

                                            <td class="text-left">
                                                <label>{{pro.produ_nombre}}</label>
                                            </td>
                                            <td class="text-left">
                                                <div id="emp_num_ruc" >{{pro.produ_sku}}</div>
                                            </td>
                                            <td class="text-left">{{pro.cantidad}}</td>
                                            <td class="text-left">{{pro.precio}}</td>
                                            <td class="text-left">{{pro.mar_nombre}}</td>
                                            <td id="emp-dir" class="text-left">{{pro.pais_nombre}}</td>
                                            <td class="text-center">{{pro.emp_nombre}}</td>
                                            <td class="text-center">
                                                <button  class="btn btn-sm btn-info fa fa-edit btn-selector-cliente"  v-on:click="cargarDataEdit(index)" data-toggle="modal" data-target="#modal_editar_productos"></button>
                                            </td>
                                        </tr>

                                        </tbody>

                                    </table>


                                    <div class="modal fade" id="modal_editar_productos" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog" role="document" style="width: 60%;">
                                            <div class="modal-content">
                                                <div class="modal-header no-border no-padding">
                                                    <div class="modal-header text-center color-modal-header">
                                                        <h3 class="modal-title">Editar Productos</h3>
                                                    </div>
                                                </div>

                                                <div class="modal-body  no-border">
                                                    <form action="#" >
                                                        <div class="container-fluid">
                                                            <div class="row no-padding">
                                                                <div class="form-group col-xs-12 col-sm-4">
                                                                    <label>CATEGORIA:</label>
                                                                    <div class="input-group col-xs-12 no-padding">
                                                                        <select v-model="productos.dataRegistro.idCategoria"  @change="onChangeCategoriProduc($event)" id="select_modal_pro_categoria" required
                                                                                class="selectpicker form-control show-tick no-padding n1"
                                                                                data-live-search="true"
                                                                                data-size="5">
                                                                            <option value="0" selected>-Seleccione-</option>
                                                                            <?php
                                                                            $categoria = new Categoria('SELECT');
                                                                            $datos = $categoria->selectAll();
                                                                            foreach ($datos as $row) {
                                                                                echo '<option value="' . $row->cat_id . '">' . $row->cat_nombre . '</option>';
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                        <!--<span class="input-group-btn">
                                                                            <button  type="button" class="btn btn-primary" data-toggle="modal"
                                                                                    data-target="#modal_marca">
                                                                                <i class="fa fa-plus"></i>
                                                                            </button>
                                                                        </span>-->
                                                                    </div>
                                                                </div>

                                                                <div id="box-productos-inputs">
                                                                    <div class="form-group col-xs-12 col-sm-4">
                                                                        <label>MARCA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <select v-model="productos.dataRegistro.idMarca"  id="select_modal_prod_marca" required
                                                                                    class="selectpicker form-control show-tick no-padding n2"
                                                                                    data-live-search="true"
                                                                                    data-size="5">
                                                                            </select>
                                                                                                                    <span class="input-group-btn">
                                                                                <button id="producto_modal_btn_select_marca" disabled type="button"
                                                                                        class="btn btn-primary" data-toggle="modal"
                                                                                        data-target="#modal_marca">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </button>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>MODELO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <select v-model="productos.dataRegistro.idModelo" id="select_modal_prod_modelo"
                                                                                    class="selectpicker form-control show-tick no-padding n3"
                                                                                    data-live-search="true"
                                                                                    data-size="5">
                                                                                <option value="0" selected>-Seleccione-</option>
                                                                            </select>
                                                                            <span class="input-group-btn">
                                                                                <button id="producto_modal_btn_select_modelo" type="button" class="btn btn-primary"
                                                                                        data-toggle="modal"
                                                                                        data-target="#modal_modelo">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </button>
                                                                            </span>
                                                                        </div>
                                                                        <!--<div class="input-group col-xs-12 no-padding">
                                                                            <select disabled id="select_modal_prod_modelo" required
                                                                                    class="selectpicker form-control show-tick no-padding" data-live-search="true"
                                                                                    data-size="5">
                                                                                <option value="0" selected>-Seleccione-</option>
                                                                            </select>
                                                                            <span class="input-group-btn">
                                                                                <button id="producto_modal_btn_select_modelo" disabled type="button" class="btn btn-primary" data-toggle="modal"
                                                                                        data-target="#modal_marca">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </button>
                                                                            </span>
                                                                        </div>-->
                                                                    </div>
                                                                    <div hidden id="item-produ-detalle" class=" no-padding">
                                                                        <div class="form-group  col-xs-12 col-sm-4">
                                                                            <label class="col-xs-12 no-padding">COD. SUNAT:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <div class="input-group col-xs-12 no-padding">
                                                                                    <input v-model="productos.dataRegistro.CodSunat" id="modal-producto-input-codigo-sunat" class="form-control"
                                                                                           type="text"
                                                                                           required
                                                                                           placeholder="Click para gregar" title="">
                                                                                    <input v-model="productos.dataRegistro.idCodSunat" id="modal-producto-input-codigo-sunat-id"
                                                                                           class="form-control no-display"
                                                                                           type="text">
                                                                                    <!--<span class="input-group-btn">
                                                                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                                                                data-target="#modal_linea">
                                                                                            <i class="fa fa-plus"></i></button>
                                                                                    </span>-->
                                                                                </div>
                                                                                <!--<select id="select_codigo" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                                                                                /*                    $codSunat = new CodSunat('SELECT');
                                                                                                    $datos = $codSunat->selectAll();
                                                                                                    foreach ($datos as $row) {
                                                                                                        echo '<option value="' . $row->sunat_cod_id . '">' . $row->sunat_cod_codigo . '</option>';
                                                                                                    }
                                                                                                    */ ?>
                </select>-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-xs-12 col-sm-4">
                                                                            <label>SKU:</label>
                                                                            <div class="input-group col-xs-12 no-padding me-select">
                                                                                <div class="input-group col-xs-12 no-padding">
                                                                                    <input v-model="productos.dataRegistro.sku" id="modal-producto-input-sku" disabled class="form-control" type="text" required
                                                                                           placeholder="Codigo de sku">
                                                                                </div>
                                                                                <span class="input-group-btn">
                                            <button id="modal-producto-btn-view-sku" type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#modal_sku">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group  col-xs-12 col-sm-4">
                                                                            <label>U. DE MEDIDA :</label>
                                                                            <div class="input-group col-xs-12 no-padding me-select">
                                                                                <select v-model="productos.dataRegistro.idUndMedida" id="select_modal_medida" required
                                                                                        class="form-control no-padding"
                                                                                        data-size="5">
                                                                                    <option value="0" selected>-Seleccione-</option>
                                                                                    <?php
                                                                                    $unidad = new Unidad('SELECT');
                                                                                    $datos = $unidad->selectAll();
                                                                                    foreach ($datos as $row) {
                                                                                        echo '<option value="' . $row->unidad_id . '">' . $row->unidad_nombre . '</option>';
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                                <!--<span class="input-group-btn">
                                                                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                                                                            data-target="#modal_categoria">
                                                                                        <i class="fa fa-plus"></i>
                                                                                    </button>
                                                                                </span>-->
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group  col-xs-12 col-sm-4">
                                                                            <label>DESCRIPCCION :</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.descripcion" id="modal-producto-input-desc" class="form-control" type="text"
                                                                                       required
                                                                                       placeholder="Nombre de producto">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-xs-12 col-sm-4">
                                                                            <label>Nombre producto:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input disabled id="modal-producto-input-nombre" class="form-control" type="text"
                                                                                       required
                                                                                       placeholder="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-xs-12 col-sm-4">
                                                                            <label>P/N:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.pn" id="modal-producto-input-pn" class="form-control" type="text" required
                                                                                       placeholder="Numero de Parte">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-xs-12 col-sm-4 box-opc">
                                                                            <label class="col-xs-12 no-padding">TIPO:</label><!--OPC-->
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <select v-model="productos.dataRegistro.idTipo" id="select_opc" required
                                                                                        class="selectpicker form-control show-tick no-padding"
                                                                                        data-live-search="true"
                                                                                        data-size="5">
                                                                                    <option value="0" selected>-Seleccione-</option>
                                                                                    <?php
                                                                                    $tipoProducto = new TipoProducto('SELECT');
                                                                                    $datos = $tipoProducto->selectAll();
                                                                                    foreach ($datos as $row) {
                                                                                        echo '<option value="' . $row->tipro_id . '">' . $row->tipro_nombre . '</option>';
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div id="box-nomenclatura" class="form-group col-xs-12 col-sm-4">
                                                                            <label>NOMENGLATURA:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <select v-model="productos.dataRegistro.idnomenglatura" id="select_nomenglatura" required
                                                                                        class="selectpicker form-control show-tick no-padding"
                                                                                        data-live-search="true"
                                                                                        data-size="5">
                                                                                    <option value="0" selected>-Seleccione-</option>
                                                                                    <?php
                                                                                    $nomenglatura = new Nomenglatura('SELECT');
                                                                                    $datos = $nomenglatura->selectAll();
                                                                                    foreach ($datos as $row) {
                                                                                        echo '<option value="' . $row->nom_id . '">' . $row->nom_nombre . '</option>';
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-xs-12 col-sm-4">
                                                                            <label>PAIS:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <select v-model="productos.dataRegistro.isPais" id="select_prod_pais" required
                                                                                        class="selectpicker form-control show-tick no-padding"
                                                                                        data-live-search="true"
                                                                                        data-size="5">

                                                                                    <?php
                                                                                    $pais = new Pais('SELECT');
                                                                                    $datos = $pais->selectAll();
                                                                                    foreach ($datos as $row) {
                                                                                        echo '<option value="' . $row->pais_id . '">' . $row->pais_nombre . '</option>';
                                                                                    }
                                                                                    ?>
                                                                                    ?>
                                                                                </select>
                                                                                <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#modal_pais">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </span>
                                                                            </div>
                                                                        </div>

                                                                        <!--CAMARA-->
                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 CAMARA">
                                                                            <label>MEDIDA:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.medidaCamara" id="modal-producto-input-cam-medida" class="form-control" type="text" required
                                                                                       placeholder="Medida de camara">
                                                                            </div>
                                                                        </div>
                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 CAMARA">
                                                                            <label>ARO:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.camAro" id="modal-producto-input-cam-aro" class="form-control" type="text" required
                                                                                       placeholder="Aro de camara">
                                                                            </div>
                                                                        </div>
                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 CAMARA">
                                                                            <label> VALVULA:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.valvula" id="modal-producto-input-cam-valvula" class="form-control" type="text" required
                                                                                       placeholder="Tipo de valvula">
                                                                            </div>
                                                                        </div>
                                                                        <!--CAMARA-->



                                                                        <!--AROS-->
                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                                                            <label>MODELO:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.modeloAro" id="modal-producto-input-aro-modelo" class="form-control" type="text" required
                                                                                       placeholder="Modelo aro">
                                                                            </div>
                                                                        </div>

                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                                                            <label>MEDIDA:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.medidaAro" id="modal-producto-input-aro-medida" class="form-control" type="text" required
                                                                                       placeholder="medida aro">
                                                                            </div>
                                                                        </div>
                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                                                            <label>ESPESOR MM:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.espesorhueco" id="modal-producto-input-aro-espesor" class="form-control" type="text" required
                                                                                       placeholder="Espesor aro">
                                                                            </div>
                                                                        </div>

                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                                                            <label># HUECOS:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.huecos" id="modal-producto-input-aro-num-huecos" class="form-control" type="text" required
                                                                                       placeholder="# de huecos">
                                                                            </div>
                                                                        </div>
                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                                                            <label>ESPESOR HUECO:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input  v-model="productos.dataRegistro.huecos"  id="modal-producto-input-aro-espesor-hueco" class="form-control" type="text" required
                                                                                       placeholder="Espesor de hueco">
                                                                            </div>
                                                                        </div>
                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                                                            <label>C.B.D:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.CBD" id="modal-producto-input-aro-cbd" class="form-control" type="text" required
                                                                                       placeholder="C.B.D aro">
                                                                            </div>
                                                                        </div>
                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                                                            <label>P.C.D:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.PCD" id="modal-producto-input-aro-pcd" class="form-control" type="text" required
                                                                                       placeholder="P.C.D aro">
                                                                            </div>
                                                                        </div>
                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                                                            <label>OFF SET:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.offSet" id="modal-producto-input-aro-offset" class="form-control" type="text" required
                                                                                       placeholder="Off Set aro">
                                                                            </div>
                                                                        </div>

                                                                        <!--AROS-->

                                                                        <!--NEUMATICO-->
                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                                                            <label>ANCHO:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.ancho" id="modal-producto-input-neu-ancho" class="form-control" type="text" required
                                                                                       placeholder="Ancho neumatico">
                                                                            </div>
                                                                        </div>
                                                                        <div hidden class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                                                            <label>SERIE:</label>
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input v-model="productos.dataRegistro.serie" id="modal-producto-input-neu-serie" class="form-control" type="text" required
                                                                                       placeholder="Serie Neumatico">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <!--NEUMATICO-->
                                                                    <!--<div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>ANCHO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-ancho" class="form-control" type="text" required
                                                                                   placeholder="Ancho neumatico">
                                                                        </div>
                                                                    </div>-->
                                                                    <!--<div hidden class="form-group col-xs-12 col-sm-2 NEUMATICO">
                                                                        <label>SERIE:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input class="form-control" type="text" required
                                                                                   placeholder="Serie Neumatico">
                                                                        </div>
                                                                    </div>-->
                                                                    <div hidden class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>ARO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input v-model="productos.dataRegistro.aro" id="modal-producto-input-neu-aro" class="form-control" type="text" required
                                                                                   placeholder="Aro de Neumatico">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>PLIEGUES:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input v-model="productos.dataRegistro.pliegles" id="modal-producto-input-neu-pliege" class="form-control" type="text" required
                                                                                   placeholder="Numero de pliegues">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group  col-xs-12 col-sm-4">
                                                                        <label>SET:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-set" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>USO:</label>
                                                                        <div id="select_uso" class="input-group col-xs-12 no-padding">
                                                                            <select v-model=" this.productos.dataRegistro.Uso" id="select_modal_uso" class="selectpicker form-control show-tick no-padding"
                                                                                    data-live-search="true"
                                                                                    data-size="5">

                                                                                <option value="PPE-Pasajeros, uso permanente">PPE-Pasajeros, uso permanente</option>
                                                                                <option value="PTE-Pasajeros, uso temporal">PTE-Pasajeros, uso temporal</option>
                                                                                <option value="CLT-Comerciales, para camioneta de carga, microbuses o camiones ligeros">CLT-Comerciales, para camioneta de carga, microbuses o camiones ligeros</option>
                                                                                <option value="CTR-Comerciales, para camión y/o ómnibus">CTR-Comerciales, para camión y/o ómnibus</option>
                                                                                <option value="CML-Comerciales, para uso minero y forestal">CML-Comerciales, para uso minero y forestal</option>
                                                                                <option value="CMH-Comerciales, para casas rodantes">CMH-Comerciales, para casas rodantes</option>
                                                                                <option value="CST-Comerciales, remolcadores en carretera">CST-Comerciales, remolcadores en carretera</option>
                                                                                <option value="ALN-Agrícolas">ALN-Agrícolas</option>
                                                                                <option value="OTG-Maquinaria, tractores niveladores">OTG-Maquinaria, tractores niveladores</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group  col-xs-12 col-sm-4">
                                                                        <label>MATE:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-mate" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group  col-xs-12 col-sm-4">
                                                                        <label>ANCHO ADUANA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-ancho-adua" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4 ">
                                                                        <label>SERIE ADUANA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-serie-adua" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4 ">
                                                                        <label>TIPO CONSTANTE:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-serie-tipo-cons" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>CARGA:</label>
                                                                        <div class="input-group col-xs-12 no-padding ">
                                                                            <input id="modal-producto-input-neu-carga" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>PISA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-pisa" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>EXTERNO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-externo" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>VELOCIDAD:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-veloci" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>CONSTA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-consta" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>ITEM:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-item" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>VIGENCIA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-vigencia" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>CONFOR:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-confor" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4 box-siempre-show">
                                                                        <label>PARTIDA ARANCELARIA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input v-model="productos.dataRegistro.partiAran" id="modal-producto-input-neu-parti" class="form-control" type="text" required
                                                                                   placeholder="Partida Arancelaria">
                                                                        </div>
                                                                    </div>
                                                                    <!--NEUMATICO-->

                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>PRO MEDIDA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-produ-medida" class="form-control" type="text" required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="container-fluid">
                                                            <hr class="line-frame-modal">
                                                        </div>
                                                        <div class="container-fluid text-right">

                                                            <!-- <a type="submit" id="modal-buscar-empresa-btn-guardar" class="btn btn-primary">
                                                                 Guardar
                                                             </a>
                                                             <button type="button" id="modal-buscar-empresa-btn-limpiar" class="btn btn-default">
                                                                 Limpiar
                                                             </button>-->
                                                            <button type="submit"  class="btn btn-primary">
                                                                Guardar
                                                            </button>
                                                            <button type="button"  class="btn btn-success"
                                                                    data-dismiss="modal">
                                                                Cerrar
                                                            </button>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>




    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <script  type="module" src="../aConfig/scripts/producto.js"></script>
    <script  type="module" src="../aConfig/scripts/sku.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        $('#select_nomenglatura').change(function () {
            genNombreProducto();
        });
        $('#select_modal_prod_modelo').change(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-pliege").keyup(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-serie").keyup(function () {
            genNombreProducto();
        });
        $( document ).ready(function() {
            $("#select_modal_pro_categoria").change(function () {
                const cate =$("#select_modal_pro_categoria option:selected").text();
                console.log(cate+"<<<<<<<<<sssss")
            });
        });

        function genNombreProducto() {
            const cate =$("#select_modal_pro_categoria option:selected").text();
            var nombrePr=cate;

            $("#modal-producto-input-nombre").removeAttr('disabled');
            if (cate=="NEUMATICOS"){
                $("#modal-producto-input-nombre").prop( "disabled", true );
                const  nomen = $("#select_nomenglatura option:selected").text();
                const  tipo = $("#select_opc option:selected").text();
                const ancho =  $("#modal-producto-input-neu-ancho").val();
                nombrePr+=" "+ancho;
                if (nomen=="MILIMETRICA"){
                    const serie= $("#modal-producto-input-neu-serie").val();
                    nombrePr+="/"+serie;
                }
                if (tipo=="RADIAL"){
                    nombrePr+="R";
                }
                const aroNeu=$("#modal-producto-input-neu-aro").val();
                const plieges = $("#modal-producto-input-neu-pliege").val();
                const marca =$("#select_modal_prod_marca option:selected").text();
                const modelo =$("#select_modal_prod_modelo option:selected").text();
                const pais =$("#select_prod_pais option:selected").text();
                nombrePr+="-"+aroNeu+" "+plieges+"PR " +marca +" "+modelo+" "+pais;
                $("#modal-producto-input-nombre").val(nombrePr);
            }else {

              /*  const  nomen = $("#select_nomenglatura option:selected").text();
                if (nomen=="MILIMETRICA"){
                    const serie= $("#modal-producto-input-neu-serie").val();
                    nombrePr+="/"+serie;
                }
                if (tipo=="RADIAL"){
                    nombrePr+="R";
                }*/
            }

        }
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>

<script src="../aConfig/scripts/modal_controller.js"></script>

<script type="text/javascript">
    MODALES._data.productos.url_data='../ajax/Producto/get_all_data2.php';
    MODALES._data.productos.iniciarDatos= true;
/*
    const APP = new Vue({
        el:"#contenedorprincipal",
        data:{
            cliente:{
                id:0,
                nombre:'',
                ruc:'',
                direccion:'',
                telefono:'',
                atencion:'',
                formapago:'',
                tipopago:'',
                cantidaddias:'',
                agenciatrasporte:'',
                idagencia:'',
                observaciones:''
            },
            listaTipoPago:,
            listaDetallePago:[],
            producto:{
                predata:{},
                nombre:'',
                precio:0,
                cantidad:'',
                stock:0,
                descuento:'',
                total:0
            },
            index_producto:-1,
            productos:[]
        },
        methods:{
            seleccionarProducnto(index){
                this.index_producto=index;
                console.log('hhhhholaaaaaaaaaaa'+ index)
            },
            eliminarItemProducto(){
                if (this.index_producto!=-1){
                    this.productos.splice(  this.index_producto, 1 );
                    this.index_producto=-1;
                }else{
                    swal('Primero selecione Una fila');
                }

            },
            guardarCotizacion(){
                var datacoti = this.cliente;
                $.ajax({
                    type: "POST",
                    url: "../ajax/Cotizacion/set_new_cotizacion.php",
                    data: datacoti,
                    success: function (resp) {
                        if (isJson(resp)){
                            var json = JSON.parse(resp);
                            if (json.res){
                                $.ajax({
                                    type: "POST",
                                    url: '../ajax/DetalleCotizacion/set_new_data.php',
                                    data: {
                                        idCoti:json.idCoti,
                                        productos: JSON.stringify(APP._data.productos)
                                    },
                                    success: function (response) {
                                        console.log(response)
                                    }
                                });
                            }else{
                                console.log("error")
                            }
                        }else{
                            console.log(resp);
                        }
                    }
                });
            },
            agregarProducto(){
                var indexProd=-1;
                for (var i =0; i<this.productos.length;i++){
                    if (this.producto.predata.produ_id===this.productos[i].id){
                        indexProd=i;

                    }
                }
                if (indexProd==-1){
                    this.productos.push({
                        id:this.producto.predata.produ_id,
                        producto:this.producto.nombre,
                        empresa:this.producto.predata.emp_nombre,
                        marca:this.producto.predata.mar_nombre,
                        sku:this.producto.predata.produ_sku,
                        pais:this.producto.predata.pais_nombre,
                        cantidad:this.producto.cantidad,
                        precio:this.producto.precio,
                        subtotal:this.producto.total

                    });
                }else {
                    this.productos[indexProd].cantidad = parseInt( this.productos[indexProd].cantidad ) +parseInt(this.producto.cantidad);
                }

                $('#tabla-poductos-coti tbody tr').removeClass('bg-success');
                this.producto.predata = {};
                this.producto.nombre ='';
                this.producto.stock =0;
                this.producto.cantidad='';
                this.producto.precio=0;
                this.producto.descuento='';
            },
            setDataCliente(dat){
                this.cliente.id=dat.id;
                this.cliente.nombre=dat.razon_social;
                this.cliente.ruc=dat.ruc;
                this.cliente.direccion=dat.direccion;
                this.cliente.telefono=dat.telefono;
                this.cliente.atencion='';
                this.cliente.formapago='';
                this.cliente.tipopago='';
                this.cliente.cantidaddias='';
                this.cliente.agenciatrasporte='';
                this.cliente.idagencia='';
                this.cliente.observaciones='';
            },
            setDataTransporte(data){
                this.cliente.agenciatrasporte=data.razon_social;
                this.cliente.idagencia=data.id;
            },
            setDataProducto(data){
                this.producto.predata = data;
                this.producto.nombre =data.produ_nombre;
                this.producto.stock =data.cantidad;
                this.producto.cantidad='';
                this.producto.precio=NumeroAleatorio(50,300);
                this.producto.descuento='';


            },
            onformaPago(evt){
                var id = evt.target.value;
                $.ajax({
                    type: "POST",
                    url: '../ajax/PagoDetalle/getData.php',
                    data: {id},
                    success: function (resp) {
                        if (isJson(resp)){
                            APP._data.listaDetallePago = JSON.parse(resp);
                            setTimeout(function () {
                                $('#select-tipoPago').selectpicker('refresh');
                            },100)
                        }else{
                            console.log(resp)
                        }
                    }
                });
            }
        },
        computed:{
            totalpropetido(){
                var desc = (this.producto.descuento +"").length>0?this.producto.descuento:0;
                var cnt = (this.producto.cantidad +"").length>0?this.producto.cantidad:0;
                var toT = this.producto.precio * cnt;
                this.producto.total = toT - ((toT*desc)/100);
                return this.producto.total.toFixed(3);
            },
            totalTabla(){
                setTimeout(function () {
                    $('#tabla-poductos-coti tbody tr').click(function() {
                        //console.log('sasasasasasasasasa')
                        $(this).addClass('bg-success').siblings().removeClass('bg-success');
                    });
                },200);

                var total = 0;
                for (var i=0; i<this.productos.length; i++){
                    total += this.productos[i].subtotal;
                }
                return total.toFixed(3);
            }
        }
    });
*/
    $(document).ready(function() {



        $('#tttttttttttttttt').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
        $('#1111111111111').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });


        $("#input-coti-cliente").focus(function () {
            $("#modal_buscar_cliente").modal('show');
        });
        $("#input-producto-buscar").focus(function () {
            $("#modal_buscar_productos").modal('show');
        });

        $("#input-coti-agendia").focus(function () {
            $("#modal_buscar_Agencia_transporte").modal('show');
        });

        $('#modal_buscar_cliente').on('hidden.bs.modal', function () {
            if (MODALES._data.clientes.isSelected){
                APP.setDataCliente(MODALES._data.clientes.clienteSelected);
            }
        });
        $('#modal_buscar_Agencia_transporte').on('hidden.bs.modal', function () {
            if (MODALES._data.agenciaTansporte.isSelected){
                APP.setDataTransporte(MODALES._data.agenciaTansporte.agenciaSelected);
            }
        });
        $('#modal_buscar_productos').on('hidden.bs.modal', function () {
            if (MODALES._data.productos.isSelected){
                APP.setDataProducto(MODALES._data.productos.productoSelected);
            }
        });
    });

    function NumeroAleatorio(min, max) {
        var num = Math.round(Math.random() * (max - min) + min);
        return num;
    }


</script>

</html>
