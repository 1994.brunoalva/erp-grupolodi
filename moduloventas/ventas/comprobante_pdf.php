<?php
require "../dao/VentaFacturacionDao.php";
require "../dao/ClienteDao.php";
require "../dao/VentaDetalleDao.php";
require "../dao/EmpresaDao.php";
require "../dao/VentaSunatDao.php";
require "../dao/TasaCambioDao.php";
require  "../utils/Tools.php";
require_once('../../lib/mpdf/vendor/autoload.php');
require_once('../../lib/vendor/autoload.php');
use Endroid\QrCode\QrCode;
use Luecano\NumeroALetras\NumeroALetras;

$idVenta = $_GET['venta'];

$tools = new Tools();
$formatter = new NumeroALetras;


$ventaFacturacionDao = new VentaFacturacionDao();
$ventaDetalleDao = new VentaDetalleDao();
$clienteDao= new ClienteDao();
$empresaDao = new EmpresaDao();
$ventaSunatDao= new VentaSunatDao();
$tasaCambioDao= new TasaCambioDao();

$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);


$ventaFacturacionDao->setFacId($idVenta);
$ventaDetalleDao->setFacId($idVenta);
$ventaSunatDao->setIdVentaSunat($idVenta);


$listaProd = $ventaDetalleDao->getProductos();

$resultC = $ventaFacturacionDao->getdataVenta()->fetch_array();

$empresaDao->setEmpId($resultC['emp_id']);
$tasaCambioDao->setTasId($resultC['tas_id']);

$restEmpre = $empresaDao->getData()->fetch_assoc();

$resDataTasaCambio = $tasaCambioDao->getDatos()->fetch_assoc();



//$clienteDao->setCliId($resultC['id_cliente']);
//$resultClie =$clienteDao->getdata()->fetch_array();
$rowHTML ="";
$subtotal = 0;
foreach ($listaProd as $prod ){

    $precio =  $prod['facd_preciou'];
    $importe = $precio * $prod['facd_cantidad'];
    $subtotal = $subtotal + $importe;
    $precio = number_format($precio, 2, '.', ',');
    $importe = number_format($importe, 2, '.', ',');
    $rowHTML = $rowHTML . "
      <tr>
        <td class='borde-p' style=' font-size: 11px;'>{$prod['produ_nombre']}</td>
        <td class='borde-p' style=' font-size: 11px; text-align: center'>{$prod['produ_sku']}</td>
        <td class='borde-p' style=' font-size: 11px; text-align: center'>{$prod['facd_cantidad']}</td>
        <td class='borde-p' style=' font-size: 11px; text-align: center'>$precio</td>
        <td class='borde-p' style=' font-size: 11px; text-align: center'>$importe</td>
      </tr>
    ";
}

$igv = $subtotal *0.18;
$total = $subtotal+ $igv;

$totalLetras =   $formatter->toInvoice(number_format($total, 2, '.', ''), 2, $resultC['sun_id']==2?'DOLARES':'SOLES');
$subtotal= number_format($subtotal, 2, '.', ',');
$igv =number_format($igv, 2, '.', ',');
$total =number_format($total, 2, '.', ',');

$dataDocumento = strlen($resultC['cli_ndoc'])>8?"RUC":"DNI";

$stylesheet = file_get_contents('../public/css/stylepdf.css');

$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);

$tipoDocNom = $resultC['tip_docu_sunat']==1?'FACTURA':'BOLETA';

$S_N = $resultC['fac_serie']. " - ". $tools->numeroParaDocumento($resultC['fac_num'],6);

$dataContQR = '';
$dataHashSunat = '';
if ($rowSD = $ventaSunatDao->getData()->fetch_assoc()){
    $dataContQR = $rowSD['dataQR'];
    $dataHashSunat = $rowSD['hash_v'];
}



$qrCode = new QrCode($dataContQR);
$qrCode->setSize(150);
$image= $qrCode->writeString();//Salida en formato de texto
$imageData = base64_encode($image);
$qrImage='<img style="width: 130px;" src="data:image/png;base64,'.$imageData.'">';





/*$mpdf->SetDefaultBodyCSS("background","url('../public/img/cotilodi.png");
$mpdf->SetDefaultBodyCSS('background-image-resize', 6);*/
$mpdf->WriteFixedPosHTML("<img style='width: 100%' src='../public/img/headpdf/{$restEmpre['emp_logo']}'>",0,0,230,130);
$mpdf->WriteFixedPosHTML("<span style='color: white;  font-size: 27px'>{$restEmpre['emp_nombre']}</span>",8,8,210,130);
$mpdf->WriteFixedPosHTML("<span style='color: white;  font-size: 20px'><strong>$tipoDocNom:</strong> $S_N</span>",130,9,210,130);
$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>Direccion:</strong> {$restEmpre['emp_direccion']}</span>",8,22,210,130);
$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>RUC: {$restEmpre['emp_ruc']}</strong></span>",30,32,210,130);
$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>Telefono:</strong> {$restEmpre['emp_telefono']}   <strong>Fax:</strong> (+51) 000000000</span>",124,22,210,130);
$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>FECHA DE EMISION: {$resultC['fac_fechae']}</strong></span>",124,32,210,130);

$html= "<div style='width: 1000%;padding-top: 90px; overflow: hidden;clear: both;background-color: rgb(255,255,255)'>
<div style='width: 50%; float: left;'>

<table style='width:100%'>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>$dataDocumento:</strong></td>
    <td style=' font-size: 13px;'>{$resultC['cli_ndoc']}</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>CLIENTE:</strong></td>
    <td style=' font-size: 13px;'>{$resultC['cli_nomape']}</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>DIRECCION:</strong></td>
    <td style=' font-size: 13px;'></td>
  </tr>
</table>
</div>
<div style='width: 50%; float: left'>
<table style='width:100%'>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>TIPO DE PAGO:</strong></td>
    <td style=' font-size: 13px;'>COONTADO</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>VENDEDOR:</strong></td>
    <td style=' font-size: 13px;'>BRUNO</td>
  </tr>
</table>
</div>
</div>

<div style='width: 100%; padding-top: 20px;'>
<table style='width:100%'>
  <tr style='border-bottom: 1px solid #0071C1'>
    <td style=' font-size: 13px;text-align: left; color: #326582;border-bottom: 1px solid #0071C1'><strong>PRODUCTOS</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>SKU</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>CANT.</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>PRECIO U.</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>IMPORTE</strong></td>
    
  </tr>
  $rowHTML

<tr style=''>
<td colspan='2'></td>
 <td colspan='2' style=' font-size: 12px; text-align: right; font-weight: bold;background-color: #d3d3d3'>SUB TOTAL:</td>
 <td style=' font-size: 12px; text-align: center; font-weight: bold;background-color: #d3d3d3'>$subtotal</td>
</tr>
<tr>
 <td colspan='4' style=' font-size: 11px; text-align: right;'>IGV:</td>
 <td style=' font-size: 12px; text-align: center; '>18.00 %</td>
</tr>
<tr>
 <td colspan='4' style=' font-size: 11px; text-align: right; '>TOTAL IMPUESTOS:</td>
 <td style=' font-size: 12px; text-align: center; '>$igv</td>
</tr>
<tr>
<td colspan='2'></td>
 <td class='border-top' colspan='2' style=' font-size: 12px; text-align: right; font-weight: bold;background-color: #d3d3d3'>TOTAL</td>
 <td class='border-top' style=' font-size: 12px; text-align: center; font-weight: bold;background-color: #d3d3d3'>$total</td>
</tr>

</table>
</div>

";
$mpdf->SetHTMLFooter("
<div style='width: 100%; padding-bottom: 5px; font-size: 12px;'>Tipo cambio: {$resDataTasaCambio['tas_comercial_venta']} Valido solo {$resDataTasaCambio['tas_fecha']} || S/. $total</div>
<div style='width: 100%; padding-bottom: 0px;font-size: 12px;'>SON: $totalLetras</div>

<div style='width: 100%; '>
<div style='float: left; width: 140px'>
$qrImage
</div>
 <div style='width: 60%; padding-bottom: 5px;font-size: 12px; float: left; padding-top: 20px;'>HASH SUNAT: $dataHashSunat<br>
 </div>
</div>
<div style='width: 100%; text-align: center;color: #006ec2; padding-bottom: 10px;'>¡Gracias por su compra!</div>
");
//echo "<style>$stylesheet</style>".$html;

$mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

$mpdf->Output();







