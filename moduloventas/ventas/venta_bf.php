<?php
require_once('../../lib/mpdf/vendor/autoload.php');

$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);

$stylesheet = file_get_contents('../public/css/stylepdf.css');

$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);

/*$mpdf->SetDefaultBodyCSS("background","url('../public/img/cotilodi.png");
$mpdf->SetDefaultBodyCSS('background-image-resize', 6);*/
$mpdf->WriteFixedPosHTML("<img style='width: 100%' src='../public/img/cotilodi.png'>",0,0,230,130);
$mpdf->WriteFixedPosHTML("<span style='color: white;  font-size: 30px'>LODI IMPORT CENTER S.R.L.</span>",8,8,210,130);
$mpdf->WriteFixedPosHTML("<span style='color: white;  font-size: 20px'><strong>Cotización:</strong> #0000000</span>",145,9,210,130);
$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>Direccion:</strong> JR. EDGAR ZUÃ±IGA 165 - SAN LUIS - LIMA</span>",8,22,210,130);
$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>RUC: 20333336929</strong></span>",30,32,210,130);
$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>Telefono:</strong> (+51) 000000000   <strong>Fax:</strong> (+51) 000000000</span>",124,22,210,130);
$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>FECHA DE EMISION: 24/06/2016</strong></span>",124,32,210,130);

$html= "<div style='width: 1000%;padding-top: 90px; overflow: hidden;clear: both;background-color: rgb(255,255,255)'>
<div style='width: 50%; float: left;'>

<table style='width:100%'>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>RUC:</strong></td>
    <td style=' font-size: 13px;'>10706671817</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>CLIENTE:</strong></td>
    <td style=' font-size: 13px;'>BRUNO ALVA SAMUEL</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>DIRECCION:</strong></td>
    <td style=' font-size: 13px;'>JR. PERU 2020</td>
  </tr>
</table>
</div>
<div style='width: 50%; float: left'>
<table style='width:100%'>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>TIPO DE PAGO:</strong></td>
    <td style=' font-size: 13px;'>COONTADO</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>VENDEDOR:</strong></td>
    <td style=' font-size: 13px;'>BRUNO</td>
  </tr>
</table>
</div>
</div>

<div style='width: 100%; padding-top: 20px;'>
<table style='width:100%'>
  <tr style='border-bottom: 1px solid #0071C1'>
    <td style=' font-size: 13px;text-align: left; color: #326582;border-bottom: 1px solid #0071C1'><strong>PRODUCTOS</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>SKU</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>CANT.</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>PRECIO U.</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>IMPORTE</strong></td>
    
  </tr>
  <tr>
    <td class='borde-p' style=' font-size: 11px;'>12.00R24 18PR AURORA AM04 MIXTA KR NEUMATICO</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>NE-AUR-PTE-1231-243</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>100</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>500</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>5000</td>
  </tr>
  <tr>
    <td class='borde-p' style=' font-size: 11px;'>12.00R24 18PR AURORA AM04 MIXTA KR NEUMATICO</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>NE-AUR-PTE-1231-243</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>100</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>500</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>5000</td>
</tr>
<tr>
    <td class='borde-p' style=' font-size: 11px;'>12.00R24 18PR AURORA AM04 MIXTA KR NEUMATICO</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>NE-AUR-PTE-1231-243</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>100</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>500</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>5000</td>
</tr>
<tr>
    <td class='borde-p' style=' font-size: 11px;'>.www</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'></td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'></td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'></td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'></td>
</tr>
<tr style=''>
<td colspan='2'></td>
 <td colspan='2' style=' font-size: 12px; text-align: right; font-weight: bold;background-color: #d3d3d3'>SUB TOTAL:</td>
 <td style=' font-size: 12px; text-align: center; font-weight: bold;background-color: #d3d3d3'>10000</td>
</tr>
<tr>
 <td colspan='4' style=' font-size: 11px; text-align: right;'>IGV:</td>
 <td style=' font-size: 12px; text-align: center; '>18.00 %</td>
</tr>
<tr>
 <td colspan='4' style=' font-size: 11px; text-align: right; '>TOTAL IMPUESTOS:</td>
 <td style=' font-size: 12px; text-align: center; '>10000</td>
</tr>
<tr>
<td colspan='2'></td>
 <td class='border-top' colspan='2' style=' font-size: 12px; text-align: right; font-weight: bold;background-color: #d3d3d3'>TOTAL</td>
 <td class='border-top' style=' font-size: 12px; text-align: center; font-weight: bold;background-color: #d3d3d3'>10000</td>
</tr>

</table>
</div>

";
$mpdf->SetHTMLFooter("
<div style='width: 100%; text-align: center;color: #006ec2; padding-bottom: 10px;'>¡Gracias por hacer negocios!</div>
<div style='width: 100%; '>
<div style='float: left; width: 70%'>
    <div style='font-size: 12px;width: 100%;'> <strong>OTROS COMENTARIOS</strong></div>
    <div style='font-size: 12px;width: 100%'>1. el pago total a pagar en 30 dias.</div>
    <div style='font-size: 12px;width: 100%'>2. Por favor, incluya el numero de factura en su cheque.</div>
    <div style='font-size: 12px;width: 100%'>3. Por favor, envie su cheque a la direccion indicada mas arriba.</div>
    
</div>
<div style='float: left; width: 30%'>
<div style='font-size: 12px;width: 100%'>Haga todos los cheques a nombre de:</div>
</div>

</div>
");
//echo "<style>$stylesheet</style>".$html;

$mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

$mpdf->Output();







