<?php
$indexRuta=1;
require  '../conexion/Conexion.php';
/*require  '../model/Cotizacion.php';
require  '../model/Venta.php';
$cotizacion = new Cotizacion();
$ventas = new Venta();*/

$nombremodule = "Cotizacion";


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../public/css/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">
    <link href="../public/plugins/sweetalert2/sweetalert2.min.css">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>


</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/

    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Ventas</strong>
                                        </h2>
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <a href="./registro_venta.php?venta=" class="btn btn-primary">Nueva Venta</a>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id=""  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <table id="tabal-cotizaciones" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr style="background-color: rgb(0, 122, 195);">
                                        <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center"></th>
                                        <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">N-S</th>
                                        <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">DOCUMENTO</th>
                                        <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">CLIENTE</th>
                                        <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">MONTO</th>
                                        <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">FECHA/HORA</th>
                                        <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">VENTA</th>
                                        <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">ESTADO</th>
                                        <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">VER</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>



                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>



    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="module" src="../public/js/Input_validate.js"></script>
    <script src="../public/plugins/sweetalert2/vue-swal.js"></script>

    <script type="text/javascript">

        function lleganar0(num,rang){
            var numfinal="";
            if (num.length>=rang){
                numfinal=num;
            }else{
                for (var i = 0; i< (rang-num.length);i++){
                    numfinal+="0";
                }
                numfinal+=num;
            }
            return numfinal;
        }


        $(document).ready(function () {
            $("#tabal-cotizaciones").DataTable({
                "processing": true,
                "serverSide": true,
                "sAjaxSource": "../ServerSide/serversideVenta.php",
                order: [[ 0, "desc" ]],
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel'
                ],
                columnDefs: [
                    {
                        "targets": 0,
                        "data": "coti_id",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;color: white">'+row[0]+'</span>';

                        }
                    },
                    {
                        "targets": 1,
                        "data": "cli_tdoc",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[1]+' - '+lleganar0(row[2]+"",6)+'</span>';

                        }
                    },
                    {
                        "targets": 2,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[3]+'</span>';

                        }
                    },
                    {
                        "targets": 3,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[4]+'</span>';

                        }
                    },
                    {
                        "targets": 4,
                        "data": "",
                        "render": function (data, type, row, meta) {
                            const totalmon = row[5].split('-');
                            return '<span style="display: block;margin: auto;text-align: center;">'+(totalmon[0]==1?'S/. ':'$ ')+totalmon[1]+'</span>';

                        }
                    },
                    {
                        "targets": 5,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[6]+'</span>';

                        }
                    },
                    {
                        "targets": 6,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[7]+'</span>';

                        }
                    },

                    {
                        "targets": 7,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[8]+'</span>';

                        }
                    },

                    {
                        "targets": 8,
                        "data": "",
                        "render": function (data, type, row, meta) {
                            return '<div style="text-align: center"><a title="Ver Cotizacion" href="venta_procesada.php?view='+row[0]+'" class="btn btn-warning"> <i class="fa fa-eye"></i> </a></div>';

                        }
                    },
                ],
                language: {
                    url: '../assets/Spanish.json'
                }
            });

        })


    </script>


</body>

</html>
