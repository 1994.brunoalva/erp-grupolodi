<?php
$indexRuta=1;
require  '../dao/TipoPagoDao.php';
require  '../dao/TipoDePrecioDao.php';



$tipoPagoDao = new TipoPagoDao();
$tipoDePrecioDao = new TipoDePrecioDao();


$listaPa= $tipoPagoDao->getdata();
$listaTipoPrecio= $tipoDePrecioDao->getdata();
$listaTemTP = [];

$isCo='true';
$idCoti=0;
$cotizacionActual='';
foreach ($listaPa as $item){
    $listaTemTP []= $item;
}

if (isset($_GET['view'])){
    $isCo='false';
    $idCoti = $_GET['view'];
}

$nombremodule = "Cotizacion";

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../public/css/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../public/plugins/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <!--suppress JSAnnotator -->
    <script>
        var isRegister = <?php echo $isCo ?>;
        var idCoti = <?php echo $idCoti ?>;

        var idVenta = -1;
        var isventa=false;


        function openPrint() {
            if (idCoti >0){
                window.open("cotizacion_pdf2.php?coti="+idCoti)
            }else{
                swal('Primero Guarde la cotizacion')
            }

        }
    </script>
    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
        #loader{

            position:absolute;/*agregamos una posición absoluta para que nos permita mover la capa en el espacio del navegador*/

            top:0;/*posicionamiento en Y */

            left:0;/*pocisionamiento en X*/

            z-index:9999; /* Le asignamos la pocisión más alta en el DOM */

            background-color:#ffffff; /* le asignamos un color de fondo */

            width:100%; /* maximo ancho de la pantalla */

            height:100%; /* maxima altura de la pantalla */

            display:block; /* mostramos el layer */

        }

        .preloader {
            width: 70px;
            height: 70px;
            border: 10px solid #eee;
            border-top: 10px solid #666;
            border-radius: 50%;
            animation-name: girar;
            animation-duration: 2s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }
        @keyframes girar {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
    </style>

</head>

<body>

<!--div id="loader">Cargando.........</div-->
<div id="wrapper">
    <?php

    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <input type="hidden" id="input-id-empresa-user" value="3">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <?php
                                            if (isset($_GET['cotizacion'])){   ?>
                                                <strong id="tittle-header-body">Resumen de Venta</strong> <span>ANULADO</span>
                                            <?php   }
                                            ?>
                                            <?php
                                            if (isset($_GET['venta']) ){   ?>
                                                <strong id="tittle-header-body">Nueva Venta</strong>
                                            <?php   }
                                            ?>
                                            <?php
                                            if (isset($_GET['view']) ){   ?>
                                                <strong id="tittle-header-body">Venta:</strong><span id="doc-anulado" style="color: red;display: none">: <strong>DOCUMENTO ANULADO</strong></span>
                                            <?php   }
                                            ?>

                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">


                                        <button id="btn-anulador" onclick="APP.anularFactura(<?php echo $_GET['view'] ?>)" type="button" class="btn btn-danger"><i
                                                    class="fa fa-times-circle"></i> ANULAR VENTA</button>

                                        <button style="display: none" type="button" class="btn btn-success"><i
                                                    class="fa fa-map"></i> GUIA DE REMISION</button>

                                        <a href="comprobante2_pdf.php?venta=<?php echo $_GET['view'] ?>" target="_blank" type="button" class="btn btn-info"><i
                                                    class="glyphicon glyphicon-print"></i> IMPRIMIR COMPROBANTE
                                        </a>


                                        <span style="color: white">----------------------------</span>
                                        <a id="btn-salir" href="./" type="reset" class="btn btn-warning"><i
                                                class="glyphicon glyphicon-chevron-left"></i>Salir
                                        </a>
                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <!--<div class="col-lg-6 text-right">
                                        <a href="new-folder.php" id="folder_btn_nuevo_folder" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Folder
                                        </a>

                                    </div>-->

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div>
                                    <form id="frame-new-folder-form">
                                        <input id="fecha"  type="hidden" value="<?php echo date("Y-m-d") ?>" class="form-control  text-center">
                                        <div class="form-group">
                                            <div class="row">

                                                <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                                    <label class="col-xs-12 no-padding">EMPRESA:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  v-model="venta.empresa" type="text" class="form-control"
                                                               aria-describedby="basic-addon1"
                                                               value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">FECHA VENTA:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  v-model="venta.fecha" type="text" class="form-control"
                                                               aria-describedby="basic-addon1"
                                                               value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">HORA VENTA:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  v-model="venta.hora" type="text" class="form-control"
                                                               aria-describedby="basic-addon1"
                                                               value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">S/N:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  v-model="venta.s_n" type="text" class="form-control"
                                                               aria-describedby="basic-addon1"
                                                               value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">ESTADO:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  v-model="venta.estado" type="text" class="form-control"
                                                               aria-describedby="basic-addon1"
                                                               value="" placeholder="">
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="row">
                                                <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                                    <label class="col-xs-12 no-padding">CLIENTE:</label>

                                                    <div class="input-group col-xs-12 no-padding">
                                                        <input v-model="cliente.nombre" id="input-coti-cliente"  type="text" class="form-control" autocomplete="off"
                                                                 aria-describedby="basic-addon1"
                                                                    disabled
                                                                 value="" placeholder="">

                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">NUMERO DOCUMENTO:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  v-model="cliente.ruc" type="text" class="form-control"
                                                                 aria-describedby="basic-addon1"
                                                                 value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-5 col-sm-7 col-md-6">
                                                    <label class="col-xs-12 no-padding">DIRECCION FISICA / SUCURSAL:</label>
                                                        <select data-width="100%" disabled  v-model="cliente.idDireccion" class="form-control">
                                                            <option v-for="dir in cliente.direcciones" v-bind:value="dir.id" >{{dir.direccion}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">TELEFONO:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  v-model="cliente.telefono" type="text" class="form-control"
                                                                 aria-describedby="basic-addon1"
                                                                 value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                    <label class="col-xs-12 no-padding">Tipo de precios:</label>
                                                    <div class="input-group col-xs-12">
                                                        <select  disabled id="select-tipo-precio" v-model="cliente.tipreciopro" class="form-control">
                                                            <?php
                                                                foreach ($listaTipoPrecio as $row){
                                                                    echo "<option value='".$row['tipl_id']."'>".$row['tipl_categoria']."</option>";
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div style="display: none" class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">VENDEDOR:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input v-model="cliente.atencion" type="text" class="form-control"
                                                                 aria-describedby="basic-addon1"
                                                               disabled
                                                                 value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding"> FORMA DE PAGO:</label>
                                                    <div class="input-group col-xs-12  no-padding">
                                                        <select    disabled v-model="cliente.formapago"
                                                                 class="form-control no-padding"
                                                                data-live-search="true">
                                                            <option v-for="item in listaTipoPago" v-bind:value="item.pag_id">{{item.pag_nombre}}</option>
                                                        </select>
                                                        <!--span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                                    data-target="#modal_incoterm">
                                                                <i class="fa fa-plus"></i></button>
                                                        </span-->
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding"> TIPO DE PAGO:</label>
                                                    <div class="input-group col-xs-12 no-padding">
                                                        <select  id="select-tipoPago" disabled v-model="cliente.tipopago"
                                                                class="form-control no-padding"
                                                                data-live-search="true">
                                                            <option v-for="dpago in listaDetallePago" v-bind:value="dpago.tip_id">{{dpago.tip_descrip}}</option>
                                                        </select>
                                                        <!--span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                                    data-target="#modal_incoterm">
                                                                <i class="fa fa-plus"></i></button>
                                                        </span-->
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-1">
                                                    <label class="col-xs-12 no-padding"> MONEDA:</label>
                                                    <div class="input-group col-xs-12 no-padding">
                                                        <select  disabled v-model="cliente.moneda"
                                                                class="form-control no-padding"
                                                                data-live-search="true">
                                                            <option value="1">SOLES</option>
                                                            <option value="2">DOLARES</option>

                                                        </select>
                                                        <!--span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                                    data-target="#modal_incoterm">
                                                                <i class="fa fa-plus"></i></button>
                                                        </span-->
                                                    </div>
                                                </div>

                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">CNT. DIAS:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  v-model="cliente.cantidaddias" type="text" class="form-control input-number"
                                                                 aria-describedby="basic-addon1"
                                                                 value="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                                    <label class="col-xs-12 no-padding">Observaciones:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled   v-model="cliente.observaciones" type="text" class="form-control"
                                                                 aria-describedby="basic-addon1"
                                                                 value="" placeholder="">

                                                    </div>
                                                </div>


                                        </div>

                                            <div  style="width: 100%; text-align: center;">
                                                <span style="margin-right: 50px"><strong>Venta: </strong>{{tasaCambio.venta}}</span>
                                                <span><strong>Compra: </strong>{{tasaCambio.compra}}</span>
                                            </div>

                                            <div class="form-group ">
                                                <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    Datos de venta<!--Padding is optional-->
                                                  </span>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <table id="tabla-poductos-coti" class="table table-bordered table-hover" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th class="col-sm-3">PROODUCTO</th>
                                                        <th class="col-sm-1">MARCA</th>
                                                        <th class="col-sm-1">SET</th>
                                                        <th class="col-sm-1">PAIS</th>
                                                        <th class="col-sm-1">CANTIDAD</th>
                                                        <th class="col-sm-1">PRECIO</th>
                                                        <th class="col-sm-1">DESCUENTO</th>

                                                        <th class="col-sm-1">IMPORTE</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr v-for="(prod, index ) in productos" v-on:click="seleccionarProducnto(index)">

                                                        <td>{{prod.producto}}</td>
                                                        <td>{{prod.marca}}</td>
                                                        <td>{{prod.unidad_nombre}}</td>
                                                        <td>{{prod.pais}}</td>
                                                        <td  class="text-center">{{prod.cantidad}}</td>
                                                        <td  class="text-center">{{simboleModena}} {{prod.precio}}</td>
                                                        <td  class="text-center">{{prod.descuento}}%</td>
                                                        <td  class="text-center" style="background-color: #feffcb">{{simboleModena}} {{parseInt(prod.subtotal).toFixed(2)}}</td>
                                                    </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="7" style="text-align: right; font-weight: bold; font-size: 18px">Sub. Total</td>
                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{subTotal.toFixed(2)}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7" style="text-align: right; font-weight: bold; font-size: 18px">IGV</td>
                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{igv.toFixed(2)}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7" style="text-align: right; font-weight: bold; font-size: 18px">Descuento</td>
                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{descuento}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7" style="text-align: right; font-weight: bold; font-size: 18px">Total</td>
                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{totalTabla}}</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        <div hidden class="form-group text-right">


                                            <div class="col-md-12 no-padding">
                                                <div class="refresh-table">
                                                    <button  type="submit" class="btn btn-primary">Guardar</button>
                                                    <button id="frame-new-folder-btn-limpiar" type="button" class="btn btn-default">Limpiar</button>
                                                    <a href="import.php" id="frame-new-folder-btn-back" type="reset" class="btn btn-warning"><i
                                                            class="glyphicon glyphicon-chevron-left"></i>Salir
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>


    <div id="conten-modales">

       <?php // include 'modales/bucar_clientes.php' ?>
       <?php //include 'modales/registrar_cliente.php' ?>
       <?php //include 'modales/buscar_agensia.php' ?>
       <?php //include 'modales/registrar_agensia.php' ?>
       <?php //include 'modales/buscar_productos.php' ?>


    </div>

    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <!--script type="module" src="../public/alertToas.js"></script-->
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <!--script type="text/javascript" src="../public/js/contador_espinner.js"></script-->
    <script type="module" src="../public/js/Input_validate.js"></script>
    <script src="../public/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
       //parseFloat()
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>


<script type="text/javascript">

   // MODALES._data.clientes.iniciarDatos= true;
    //MODALES._data.agenciaTansporte.iniciarDatos= true;
    //MODALES._data.productos.iniciarDatos= true;


   function lleganar0(num,rang){
       var numfinal="";
       if (num.length>=rang){
           numfinal=num;
       }else{
           for (var i = 0; i< (rang-num.length);i++){
               numfinal+="0";
           }
           numfinal+=num;
       }
       return numfinal;
   }


   var  fecha =  new Date();
    const APP = new Vue({
        el:"#contenedorprincipal",
        data:{
            idcotizacion:0,
            clasificacion:[],
            funExeSavaCoti:function () {

            },
            venta:{
                empresa:'',
                fecha:'',
                hora:'',
                s_n:'',
                estado:''
            },
            cliente:{
                id:0,
                nombre:'',
                ruc:'',
                direcciones:[],
                idDireccion:'',
                telefono:'',
                atencion:'',
                formapago:'',
                moneda:4,
                tipopago:'',
                tipreciopro:'',
                cantidaddias:'',
                agenciatrasporte:'',
                idagencia:'',
                fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate(),
                observaciones:''
            },
            listaTipoPago:<?php echo json_encode($listaTemTP)?>,
            listaDetallePago:[],
            tasaCambio:{},
            igv:0,
            descuento:0,
            subTotal:0,
            totalcoti:0,
            producto:{
                predata:{},
                nombre:'',
                precio:0,
                cantidad:'',
                stock:0,
                descuento:0,
                total:0
            },
            index_producto:-1,
            productos:[]
        },
        methods:{
            anularFactura(venta){

                const fechaC = fecha.getFullYear() +'-'+((fecha.getMonth()+1)>=10?(fecha.getMonth()+1):'0'+(fecha.getMonth()+1) )
                    +'-'+(fecha.getDate()>=10?fecha.getDate():'0'+fecha.getDate());
                swal({
                    title: "¿Desea anular la venta?",
                    text: "",
                    icon: "warning",
                    dangerMode: false,
                    buttons: ["NO", "SI"],
                })
                    .then((ressss) => {
                        console.log(ressss);
                        if (ressss){
                            $.ajax({
                                type: "POST",
                                url: "../ajaxs/ajs_anular_venta.php",
                                data: {venta,fechaC},
                                success: function (resp) {
                                    console.log(resp)
                                    resp=JSON.parse(resp)
                                    if (resp.res){
                                        APP._data.venta.estado='ANULADO';
                                        APP.gestorReEvaluacion();
                                        $.toast({
                                            heading: 'EXITOSO',
                                            text: 'Documento anulado',
                                            icon: 'success',
                                            position: 'top-right',
                                            hideAfter: '2500',
                                        });
                                    }

                                }
                            });
                        }
                    });


            },
            gestorReEvaluacion(){
                if ( this.venta.estado=="ANULADO"){
                    $("#doc-anulado").show();
                    $("#btn-anulador").attr('disabled','disabled');
                }else{
                    $("#doc-anulado").hide();
                    $("#btn-anulador").removeAttr('disabled');
                }
            },
            realizarVenta(){
                var d = new Date();

                var datacoti = {... APP._data.cliente};
                datacoti.tipo= "i";
                datacoti.direcciones='';
                datacoti.fechaVenta=$("#fecha").val();
                datacoti.horaVenta= d.getHours()+":"+((d.getMinutes()<10)?"0"+d.getMinutes():d.getMinutes());
                datacoti.id_empre=$("#input-id-empresa-user").val();
                datacoti.idcotizacion = idCoti;
                datacoti.tasacambio= this.tasaCambio.idc;
                datacoti.totalcoti =this.totalcoti;
                $.ajax({
                    type: "POST",
                    url: "../ajaxs/ajs_ventas.php",
                    data: datacoti,
                    success: function (resp) {
                        console.log(resp)
                        if (isJson(resp)){
                            var json = JSON.parse(resp);
                            if (json.res){
                                $.ajax({
                                    type: "POST",
                                    url: '../ajaxs/ajs_venta_detalle.php',
                                    data: {
                                        idCoti:json.idCoti,
                                        tipo:'i',
                                        productos: JSON.stringify(APP._data.productos)
                                    },
                                    success: function (response) {
                                        console.log(response)
                                        if (isJson(response)){
                                            swal("Cotizacion Guardada", "", "success");

                                        }else{
                                            swal('Hubo un problema al guardar los productos de la venta')
                                        }
                                    }
                                });
                            }else{
                                console.log("error")
                                console.log(resp)
                                swal('Error, no de pudo guardar la venta');
                            }
                        }else{
                            console.log(resp);
                        }
                    }
                });

            },
            cargarDatoCotizacion(){

                $.ajax({
                    type: "POST",
                    url: '../ajaxs/ajs_ventas.php',
                    data:{
                        tipo:'s',
                        idVent:idCoti
                    },
                    success: function (data) {
                        console.log(data)
                        if (isJson(data)){
                            var json = JSON.parse(data);
                            console.log(json)
                            console.log('*----------------*')
                            $("#tittle-header-body").text("Resumen de Venta")
                            APP._data.cliente.id=json.cli_id;
                            APP._data.cliente.nombre=json.cli_nomape;
                            APP._data.cliente.ruc=json.cli_ndoc;
                            APP._data.cliente.idDireccion=json.direccion;
                            APP._data.cliente.telefono=json.cli_tele;
                            APP._data.cliente.atencion='';
                            APP._data.cliente.formapago=json.pag_id;
                            APP._data.cliente.tipopago=json.pag_forma;
                            APP._data.cliente.cantidaddias =json.fac_diasc==0?'':json.fac_diasc;
                            APP._data.cliente.moneda =json.sun_id;
                            APP._data.venta.empresa=json.emp_nombre;
                            APP._data.venta.fecha=json.fac_fechae;
                            APP._data.venta.hora=json.fac_hora;
                            APP._data.venta.s_n=json.fac_serie+" - "+ lleganar0(json.fac_num, 6);
                            APP._data.venta.estado=json.fac_estatus;
                            APP._data.cliente.observaciones=json.observaciones;

                            //APP._data.cliente.agenciatrasporte='';
                            //APP._data.cliente.idagencia='';
                            //APP._data.cliente.fecha=json.coti_fecha;
                            APP._data.cliente.tipreciopro=json.fac_cost_list_id;

                            APP.gestorReEvaluacion();

                            console.log(APP._data.cliente.cantidaddias + "<>" + json.coti_dias_credito);

                            json.productos.forEach(function (prod) {
                                var totalT =parseInt(prod.cantidad) *parseInt(prod.precio_unitario);
                                APP._data.productos.push({
                                    id:0,
                                    producto:prod.produ_nombre,
                                    empresa:prod.emp_nombre,
                                    idempresa:0,
                                    idProdEmpr:0,
                                    marca:prod.mar_nombre,
                                    sku:prod.produ_sku,
                                    pais:prod.pais_nombre,
                                    cantidad:prod.cantidad,
                                    descuento:0,
                                    unidad_nombre: prod.unidad_nombre,
                                    precio:prod.facd_preciou,
                                    subtotal: prod.facd_subtotal

                                });
                            });

                            $.ajax({
                                type: "POST",
                                url: "../ajaxs/ajs_cliente.php",
                                data: {tipo:'s',acc:"direcciones",idc:APP._data.cliente.id},

                                success: function (data) {
                                    if (isJson(data)){
                                        var direc =JSON.parse(data);
                                        APP._data.cliente.direcciones = direc;
                                    }else{
                                        console.log(data);
                                    }

                                }
                            });


                           /* setTimeout(function () {
                                $('select').selectpicker('refresh');
                            },100);*/
                            $.ajax({
                                type: "POST",
                                url: '../ajaxs/ajs_bd_datas.php',
                                data: {id:json.pag_id,tipo:"pagodetalle"},

                                success: function (resp) {
                                   // console.log(resp)
                                    if (isJson(resp)){
                                        APP._data.listaDetallePago = JSON.parse(resp);
                                       /* setTimeout(function () {
                                            $('#select-tipoPago').selectpicker('refresh');
                                        },100)*/
                                    }else{
                                        console.log(resp)
                                    }
                                }
                            });

                            //console.log(json)
                        }else{
                            console.log(data)
                        }
                    }
                });
            },
            seleccionarProducnto(index){
                this.index_producto=index;
                //console.log('hhhhholaaaaaaaaaaa'+ index)
            },
            eliminarItemProducto(){
                if (this.index_producto!=-1){
                    this.productos.splice(  this.index_producto, 1 );
                    this.index_producto=-1;
                }else{
                   swal('Primero selecione Una fila');
                }

            },
            guardarCotizacion(){
                var datacoti = {... APP._data.cliente};
                datacoti.tipo= isRegister?"i":'u';
                datacoti.direcciones='';
                datacoti.estcot='1';
                datacoti.idcotizacion = idCoti;
                datacoti.tasacambio= this.tasaCambio.idc;
                datacoti.totalcoti =this.totalcoti;
                console.log(datacoti)
                $.ajax({
                    type: "POST",
                    url: "../ajaxs/ajs_cotizacion.php",
                    data: datacoti,
                    success: function (resp) {
                        console.log(resp)
                        if (isJson(resp)){
                            var json = JSON.parse(resp);
                            if (json.res){
                                console.log(APP._data.productos);
                                $.ajax({
                                    type: "POST",
                                    url: '../ajaxs/ajs_detalle_cotizacion.php',
                                    data: {
                                        tipo:isRegister?'i':'u',
                                        idCoti:json.idCoti,
                                        productos: JSON.stringify(APP._data.productos)
                                    },
                                    success: function (response) {
                                        console.log(response)
                                        if (isJson(response)){
                                            const jsRe = JSON.parse(response)
                                            if(jsRe.res){
                                                swal("Cotizacion Guardada", "", "success");
                                                idCoti=json.idCoti;
                                                isRegister=false;
                                            }else{
                                                swal('Hubo un problema al guardar los productos de la cotización')
                                            }

                                        }else{
                                            swal('Hubo un problema al guardar los productos de la cotización EROOR:Server')
                                        }
                                    }
                                });
                            }else{
                                console.log("error")
                                console.log(resp)
                                swal('Error, no de pudo guardar la cotizacion');
                            }
                        }else{
                            console.log(resp);
                        }
                    }
                });
            },
            guardarYVenterCotizacion(){
                var d = new Date();

                var datacoti = {... APP._data.cliente};
                datacoti.tipo= "i";
                datacoti.direcciones='';
                datacoti.fechaVenta=$("#fecha").val();
                datacoti.horaVenta= d.getHours()+":"+((d.getMinutes()<10)?"0"+d.getMinutes():d.getMinutes());
                datacoti.id_empre=$("#input-id-empresa-user").val();
                datacoti.idcotizacion = null;
                datacoti.tasacambio= this.tasaCambio.idc;
                datacoti.totalcoti =this.totalcoti;

                console.log(datacoti)

                swal({
                    title: "¿Desea realizar la venta?",
                    text: "",
                    icon: "warning",
                    dangerMode: false,
                    buttons: ["NO", "SI"],
                })
                    .then((ressss) => {
                        console.log(ressss);
                        if (ressss){
                            $.ajax({
                                type: "POST",
                                url: "../ajaxs/ajs_ventas.php",
                                data: datacoti,
                                success: function (resp) {
                                    console.log(resp)
                                    if (isJson(resp)){
                                        var json = JSON.parse(resp);
                                        if (json.res){
                                            $.ajax({
                                                type: "POST",
                                                url: '../ajaxs/ajs_venta_detalle.php',
                                                data: {
                                                    idCoti:json.idCoti,
                                                    tipo:'i',
                                                    productos: JSON.stringify(APP._data.productos)
                                                },
                                                success: function (response) {
                                                    console.log(response)
                                                    if (isJson(response)){
                                                        swal("Venta Procesada ", "", "success");

                                                    }else{
                                                        swal('Hubo un problema al guardar los productos de la venta')
                                                    }
                                                }
                                            });
                                        }else{
                                            console.log("error")
                                            console.log(resp)
                                            swal('Error, no de pudo guardar la venta');
                                        }
                                    }else{
                                        console.log(resp);
                                    }
                                }
                            });

                        }

                    });


            },

            recalcularPrecioGeneral(){
                const idtipoprecio = this.cliente.tipreciopro;
                for(var i=0; i<this.productos.length;i++){
                    this.productos[i]
                    const resultado =   this.productos[i].preciostemp.find(function(elt){ return elt.tipl_id == idtipoprecio});
                    var precionuevo;
                    if (typeof resultado === 'undefined'){
                        precionuevo='0.00';
                    }else{
                        precionuevo = resultado.lisd_vdolar;
                    }
                    this.productos[i].precio=precionuevo;
                    this.productos[i].subtotal= parseFloat(precionuevo+"") * parseInt( this.productos[i].cantidad);

                }
                if(this.producto.nombre.length>6){
                    const resultado =this.producto.predata.precios.find(function(elt){ return elt.tipl_id == idtipoprecio});
                    var precionuevo;
                    if (typeof resultado === 'undefined'){
                        precionuevo='0.00';
                    }else{
                        precionuevo = resultado.lisd_vdolar;
                    }
                    this.producto.precio=precionuevo
                }

            },
            agregarProducto(){
                console.log(this.producto.predata);
                if (parseInt(this.producto.predata.cantidad)>=parseInt(this.producto.cantidad)){
                    var indexProd=-1;
                    var cambio = 1;
                    if (this.cliente.moneda==1){
                        cambio= this.tasaCambio.venta;
                    }
                    for (var i =0; i<this.productos.length;i++){
                        if (this.producto.predata.produ_id==this.productos[i].id && this.producto.predata.emp_id ==this.productos[i].idempresa){
                            indexProd=i;

                        }
                    }


                    if (indexProd==-1){
                        this.productos.push({
                            id:this.producto.predata.produ_id,
                            idProdEmpr:this.producto.predata.prod_empre_id,
                            preciostemp:this.producto.predata.precios,
                            producto:this.producto.nombre,
                            empresa:this.producto.predata.emp_nombre,
                            idempresa:this.producto.predata.emp_id,
                            marca:this.producto.predata.mar_nombre,
                            sku:this.producto.predata.produ_sku,
                            pais:this.producto.predata.pais_nombre,
                            cantidad:this.producto.cantidad,
                            precio:this.producto.precio,
                            descuento:this.producto.descuento.length>0?this.producto.descuento:0,
                            subtotal:this.producto.total

                        });
                    }else {
                        this.productos[indexProd].cantidad = parseInt( this.productos[indexProd].cantidad ) +parseInt(this.producto.cantidad);
                        var precioU = parseInt(this.producto.cantidad)*parseFloat(this.producto.precio);
                        var descuento = precioU*parseFloat( this.productos[indexProd].descuento)/100;
                        this.productos[indexProd].subtotal  = (parseFloat( this.productos[indexProd].subtotal ) +(precioU-descuento)).toFixed(2);
                    }

                    $('#tabla-poductos-coti tbody tr').removeClass('bg-success');
                    this.producto.predata = {};
                    this.producto.nombre ='';
                    this.producto.stock =0;
                    this.producto.cantidad='';
                    this.producto.precio=0;
                    this.producto.descuento='';
                }else{
                    swal('La cantidad sobrepasa el stock actual')
                }

            },
            setDataCliente(dat){
                console.log(dat);
                this.cliente.id=dat.cli_id;
                this.cliente.nombre=dat.cli_nomape;
                this.cliente.ruc=dat.cli_ndoc;
                this.cliente.direccion=dat.cli_direc;
                this.cliente.telefono=dat.cli_tele;
               // this.cliente.idDireccion=(typeof dat.direcciones[0]!=='undefined')?dat.direcciones[0].id:'';
                this.cliente.atencion='';
                this.cliente.formapago='';
                this.cliente.tipopago='';
                this.cliente.cantidaddias='';
                this.cliente.agenciatrasporte='';
                this.cliente.idagencia='';
                this.cliente.observaciones='';
            },
            setDataTransporte(data){
                this.cliente.agenciatrasporte=data.razon_social;
                this.cliente.idagencia=data.id;
            },
            setDataProducto(data){
                var cambio = 1;
                if (this.cliente.moneda==1){
                    cambio= this.tasaCambio.venta;
                }
                this.producto.predata = data;
                this.producto.nombre =data.produ_nombre;
                this.producto.stock = parseInt( data.cantidad);
                this.producto.cantidad='';
                this.producto.precio= (parseFloat( data.precio)/cambio).toFixed(2);
                this.producto.descuento=0;


            },
            onformaPago(evt){
                var id = evt.target.value;
                $.ajax({
                    type: "POST",
                    url: '../ajaxs/ajs_bd_datas.php',
                    data: {id,tipo:"pagodetalle"},
                    success: function (resp) {
                        console.log(resp)
                        if (isJson(resp)){
                            APP._data.listaDetallePago = JSON.parse(resp);
                            /*setTimeout(function () {
                                $('#select-tipoPago').selectpicker('refresh');
                            },100)*/
                        }
                    }
                });
            }
        },
        computed:{
            simboleModena(){
              return this.cliente.moneda==1?'$':'S/.';
            },
            isCredito(){
                if (this.cliente.formapago==2){
                    this.cliente.cantidaddias="";
                }

                return this.cliente.formapago!=1;
            },
            isDolar(){
                return this.cliente.moneda==1;
            },
            totalpropetido(){
                var desc = (this.producto.descuento +"").length>0?this.producto.descuento:0;
                var cnt = (this.producto.cantidad +"").length>0?this.producto.cantidad:0;
                var toT = this.producto.precio * cnt;
                this.producto.total = toT - ((toT*desc)/100);
                return this.producto.total.toFixed(2);
            },
            totalTabla(){
                setTimeout(function () {
                    $('#tabla-poductos-coti tbody tr').click(function() {
                        //console.log('sasasasasasasasasa')
                        $(this).addClass('bg-success').siblings().removeClass('bg-success');
                    });
                },200);

                var total = 0;
                for (var i=0; i<this.productos.length; i++){
                    total += parseFloat(this.productos[i].subtotal);
                }
                this.subTotal=total;
                this.igv = (total*0.18);
                //console.log((total+ this.igv)+'/*************************');
                this.totalcoti=(total+ this.igv);
                return this.totalcoti;
            }
        }
    });

    function geTasaCambio(){
        $.ajax({
            type: "POST",
            url: "../ajax/CambioMoneda/iniciartipoCambio.php",
            data: {
                fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate()
            },
            success: function (dat) {
                if (isJson(dat)){
                    APP._data.tasaCambio= JSON.parse(dat);
                    swal("Tasa de cambio: "+APP._data.tasaCambio.fecha, "Venta: "+APP._data.tasaCambio.venta+"\nCompra: "+APP._data.tasaCambio.compra)
                }else{
                    console.log(dat);
                }
            }
        });
    }

    $(document).ready(function() {

       // geTasaCambio();

        if (!isRegister){
            APP.cargarDatoCotizacion();
        }

       /* $('#tttttttttttttttt').DataTable({

            serverSide: true,
            processing: true,
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
        $('#1111111111111').DataTable({

            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });*/
        //$('select').selectpicker();

        $("#input-coti-cliente").autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "../ajaxs/buscar_cliente_dnumdoc.php",
                    data: { term: request.term},
                    success: function(data){
                        response(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        console.log(jqXHR.responseText)
                    },
                    dataType: 'json'
                });
            },
            minLength: 2,
            select: function (event, ui) {
                console.log(ui.item)
                event.preventDefault();
                APP.setDataCliente(ui.item);

                $.ajax({
                    type: "POST",
                    url: "../ajaxs/ajs_cliente.php",
                    data: {tipo:'s',acc:"direcciones",idc:APP._data.cliente.id},
                    success: function (data) {
                        if (isJson(data)){
                            var direc =JSON.parse(data);
                            console.log(direc);
                            APP._data.cliente.direcciones = direc;
                            APP._data.cliente.idDireccion = 0;
                            //APP._data.cliente.idDireccion=(typeof direc[0]!=='undefined')?direc[0].id:'';

                        }else{
                            console.log(data);
                        }

                    }
                });

               /* $('#hidden_id_proveedor').val(ui.item.id);
                $('#input_ruc_proveedor').val(ui.item.ruc);
                $('#input_razon_social').val(ui.item.razon_social);
                $('#input_direccion').val(ui.item.direccion);
                $('#input_producto').focus();*/
            }
        });
        $("#input-buscar-producto").keydown(function (evt) {
            if((APP._data.cliente.tipreciopro+"").length==0){
                evt.preventDefault();
                swal("Seleccione el tipo de precio ").then(function () {
                    $("#select-tipo-precio").focus();
                })
            }
        });
        $("#input-buscar-producto").autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "../ajaxs/buscar_productos.php",
                    data: { term: request.term,idemp :$("#input-id-empresa-user").val()},
                    success: function(data){
                        response(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        console.log(jqXHR.responseText)
                    },
                    dataType: 'json'
                });
            },
            minLength: 2,
            select: function (event, ui) {
                console.log(ui.item)
                ui.item.cantidad -= nuevoStock(ui.item.produ_id,ui.item.emp_id);
                APP._data.producto.predata = ui.item;
                APP._data.producto.nombre = ui.item.produ_nombre;
                const resultado =  ui.item.precios.find(function(elt){ return elt.tipl_id == APP._data.cliente.tipreciopro});
                if (typeof resultado === 'undefined'){
                    APP._data.producto.precio = 0.00;
                }else{
                    APP._data.producto.precio = resultado.lisd_vdolar;
                }
                //APP._data.producto.precio = ui.item.precio;

                APP._data.producto.cantidad = '';
                APP._data.producto.stock = ui.item.cantidad ;
                APP._data.producto.total = 0;
                console.log( nuevoStock(ui.item.produ_id,ui.item.prod_empre_id));

                event.preventDefault();

            }
        });

        $("#input-coti-cliente").focus(function () {
            //$("#modal_buscar_cliente").modal('show');
        });
      /*  $("#input-producto-buscar").focus(function () {
            $("#modal_buscar_productos").modal('show');
        });*/

        $("#input-coti-agendia").focus(function () {
            $("#modal_buscar_Agencia_transporte").modal('show');
        });

        $('#modal_buscar_cliente').on('hidden.bs.modal', function () {
            if (MODALES._data.clientes.isSelected){
                APP.setDataCliente(MODALES._data.clientes.clienteSelected);
            }
        });
        $('#modal_buscar_Agencia_transporte').on('hidden.bs.modal', function () {
            if (MODALES._data.agenciaTansporte.isSelected){
                APP.setDataTransporte(MODALES._data.agenciaTansporte.agenciaSelected);
            }
        });
        $('#modal_buscar_productos').on('hidden.bs.modal', function () {
            if (MODALES._data.productos.isSelected){
                APP.setDataProducto(MODALES._data.productos.productoSelected);
            }
        });
        //$("#loader").fadeOut("slow");
    });
   
    function nuevoStock(idProd, idEmpre) {
        const prods = APP._data.productos;
        var cnt =0;
        for (var i=0; i< prods.length;i++){
            if (prods[i].idempresa==idEmpre&&prods[i].id==idProd){
                cnt = prods[i].cantidad;
            }
        }
        return cnt;
    }
    function NumeroAleatorio(min, max) {
        var num = Math.round(Math.random() * (max - min) + min);
        return num;
    }







</script>

</html>
