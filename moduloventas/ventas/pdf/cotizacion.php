<?php
require '../../conexion/Conexion.php';
require '../../model/Cotizacion.php';
require '../../model/DetalleCotizacion.php';
require '../../model/Venta.php';

require_once('../../Lib/mpdf/vendor/autoload.php');
require_once('../../Lib/vendor/autoload.php');//Llamare el autoload de la clase que genera el QR
use Endroid\QrCode\QrCode;
use Luecano\NumeroALetras\NumeroALetras;

$formatter = new NumeroALetras;
$venta=new Venta();


$mpdf = new \Mpdf\Mpdf();
$stylesheet = file_get_contents('stylepdf.css');

$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);




$idCoti = $_GET['coti'];


$cotizacion = new Cotizacion();
$detalleCotizacion=new DetalleCotizacion();


$idCOtizacion = $idCoti;


$cotizacion->setCotiId($idCOtizacion);

$respuesta = $cotizacion->verCotizacion()->fetch_assoc();
$productos=[];

$detalleCotizacion->setIdCotizacion($idCOtizacion);
$res = $detalleCotizacion->verLista();

$prodHTML ="";
$contador =1;
$subtotal=0;
$descuento = 0;
$simb='';
$monedaNombre='';
if($respuesta['coti_tp_moneda']==1){
    $simb = '$';
    $monedaNombre='DOLARES';
}else{
    $simb = 'S/.';
    $monedaNombre='SOLES';
}


//echo $respuesta['coti_tp_moneda']. '................';
$fechaEmision=$respuesta['coti_fecha'];
$qrImage='';

if (isset($_GET['venta'])){
    $idventa= $_GET['venta'];
    $venta->setId($idventa);

    $resV = $venta->getData()->fetch_assoc();

    $qrCode = new QrCode("ruc empresa|tipoComprobante|seri numero|igv|total|{$resV['fecha']}|tipoDocumento|numDocumen|hash|firmadijital");
    $qrCode->setSize(150);
    $image= $qrCode->writeString();//Salida en formato de texto
    $imageData = base64_encode($image);
    $qrImage='<img src="data:image/png;base64,'.$imageData.'">';
    $fechaEmision=$resV['fecha'];
}
$productosSeparadosEmpresas=[];
foreach ($res as $row){
    if (count($productosSeparadosEmpresas)>0){
        $vali=true;
        for($in=0;$in<count($productosSeparadosEmpresas);$in++){
            if (count($productosSeparadosEmpresas[$in])>0){
                if ($productosSeparadosEmpresas[$in][0]['emp_id']==$row['emp_id']){
                    $productosSeparadosEmpresas[$in][]=$row;
                    $vali=false;
                }
            }

        }
        if ($vali){
            $productosSeparadosEmpresas[]=array($row);
        }
    }else{
        $productosSeparadosEmpresas[]=array($row);
    }
    /* $importe = $row['cantidad']*$row['precio_unitario'];
     $des = ($importe*$row['descuento'])/100;
     $descuento +=$des;
     $subtotal+= $importe;
     $importeConDes= $importe -$des;



     $prodHTML .= "
     <tr>
             <td class='td' style='text-align: center'>{$row['cantidad']}</td>
            <td class='td'>{$row['produ_sku']}</td>
            <td class='td'>{$row['produ_nombre']}</td>
            <td class='td' style='text-align: center'>$simb {$row['precio_unitario']}</td>

            <td class='td' style='text-align: center'> {$row['descuento']}</td>
            <td class='td' style='text-align: center'>$simb $importeConDes</td>

         </tr>
     ";
     $contador++;*/
}
//echo json_encode($productosSeparadosEmpresas);
/*$igv = $subtotal*0.19;
$total = $subtotal+$igv-$descuento;


$totalLetras =   $formatter->toInvoice($total, 2, 'SOLES');


$cabeza= "
 <div style='width: 100%; background-color: #ffffff' >
  <div style=' width: 65%;float: left; background-color: #ffffff'>
<div style='width: 47%;float: left; background-color: #ffffff' >
 <img style='max-width: 100%;' src='../../imagenes/logo/CHION_1.png'>
</div>

    <div style='width:53%;text-align: center; font-size: 13px; float: right; background-color: #ffffff'>
       <span><strong>LODI IMPORT CENTER S.R.L.</strong></span><br>
       <span>JR. EDGAR ZUNIGA NRO. 165 URB. LA VINA LIMA - LIMA - SAN LUIS</span><br>
       <span><strong>Telf. </strong>+51 1 2031300</span><br>
    </div>
</div>
<div style=' width: 34%;text-align: center; background-color: #ffffff ; float: right;'>

<div style='width: 100%; height: 130px; border: 2px solid #1e1e1e' class='contenedor'>
<div style='margin-top:20px'></div>
<span>RUC: 2147483647</span><br>
<div style='margin-top: 10px'></div>
<span><strong>FACTURA ELECTRONICA</strong></span><br>
<div style='margin-top: 10px'></div>
<span>Nro. F001-00023</span>
</div>
</div>
</div>

<div style='background-color: #ffffff; width: 100%;margin-top: 10px;border: 2px solid #1e1e1e; padding: 8px;'>
<table width='100%'>
 <tr>
    <td style='width: 60%'><strong>Cliente</strong></td>
    <td><strong>Fecha de emision</strong></td>
    <td><strong>Condicion de pago</strong></td>
  </tr>
  <tr>
    <td style='width: 60%;padding-left: 15px;padding-right: 10px;'>{$respuesta['coti_razon']}</td>
    <td>$fechaEmision</td>
    <td>{$respuesta['formapagonom']}</td>
  </tr>
  <tr style=''>
    <td style='width: 60%;padding-top: 10px;'><strong>RUC:</strong>  {$respuesta['coti_ruc']}</td>
    <td><strong>Vendedor</strong></td>
    <td><strong></strong></td>
  </tr>
   <tr>
    <td style='width: 60%;padding-left: 15px;padding-right: 10px;'>{$respuesta['cliente_direc']}</td>
    <td style='padding-right: 10px;' valign='top'>{$respuesta['coti_ate']}</td>
    <td></td>
  </tr>
</table>
</div>
<div style='background-color: #ffffff; width: 100%;margin-top: 10px;'>
<table style='width:100%'>
  <tr>
    <th style='color: white; background-color: #404040;padding: 5px;'>CANT.</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>SKU</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>DESCRIPCION</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>VALOR U.</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>DSCT %</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>IMPORTE</th>
  </tr>
  $prodHTML
  <tr>
  <td colspan='5' style='text-align: right;border-right: 1px solid black'>SUB TOTAL  </td>
  <td  style='text-align: center;border: 1px solid black'>11111</td>
</tr>
 <tr>

  <td colspan='5' style='text-align: right;border-right: 1px solid black'>IGV</td>
  <td  style='text-align: center;border: 1px solid black'>11111</td>
</tr>
 <tr>
 <td colspan='3' style='text-align: left;'>Importe en letras</td>
  <td colspan='2' style='text-align: right;border-right: 1px solid black'>DESCUENTO</td>
  <td  style='text-align: center;border: 1px solid black'>11111</td>
</tr>
 <tr>
 <td colspan='3' style='text-align: left;font-size: 13px'>$totalLetras</td>
  <td colspan='2' style='text-align: right;border-right: 1px solid black'>TOTAL</td>
  <td  style='text-align: center;border: 1px solid black'>$total</td>
</tr>
</table>
</div>
$qrImage
  ";*/

$contHoja=0;
foreach ($productosSeparadosEmpresas as $hoja){
    $prodHTML ="";
    $contador =1;
    $subtotal=0;
    $descuento = 0;
    $idRmpresaHoja=0;
    foreach ($hoja as $row){
        $idRmpresaHoja = $row['emp_id'];
        $importe = $row['cantidad']*$row['precio_unitario'];
        $des = ($importe*$row['descuento'])/100;
        $descuento +=$des;
        $subtotal+= $importe;
        $importeConDes= $importe -$des;

        $prodHTML .= "
    <tr>
            <td class='td' style='text-align: center'>{$row['cantidad']}</td>
           <td class='td'>{$row['produ_sku']}</td>
           <td class='td'>{$row['produ_nombre']}</td>
           <td class='td' style='text-align: center'>$simb {$row['precio_unitario']}</td>
           
           <td class='td' style='text-align: center'>$simb $importeConDes</td>
           
        </tr>
    ";
        $contador++;
    }

    $empresaData = $venta->getEmpres($idRmpresaHoja)->fetch_assoc();

    $total =  number_format($subtotal-$descuento,2,'.','');
    $igv = number_format($subtotal*0.19, 2, '.', '');
    $subtotalPrin =$total;

    $total=$subtotalPrin+$igv;
  /*  $subtotalPrin= number_format($total/1.19,2,'.','');
    $igvPrin =  number_format($subtotalPrin*0.19,2,'.','');*/
    $descuento=  number_format($descuento,2,'.','');
    $total -= $descuento;
    $totalLetras =   $formatter->toInvoice($total, 2, $monedaNombre);


    $cabeza= "
 <div style='width: 100%; background-color: #ffffff' >
  <div style=' width: 65%;float: left; background-color: #ffffff'>
<div style='width: 47%;float: left; background-color: #ffffff' >
 <img style='max-width: 100%;' src='../../imagenes/logo/{$empresaData['emp_logo']}'>
</div>
   
    <div style='width:53%;text-align: center; font-size: 13px; float: right; background-color: #ffffff'>
       <span><strong>{$empresaData['emp_nombre']}</strong></span><br>
       <span>{$empresaData['emp_dir']}</span><br>
       <span><strong>Telf. </strong>{$empresaData['emp_tel']}</span><br>
    </div>
</div>
<div style=' width: 34%;text-align: center; background-color: #ffffff ; float: right;'>

<div style='width: 100%; height: 130px; border: 2px solid #1e1e1e' class='contenedor'>
<div style='margin-top:20px'></div>
<span>RUC: {$empresaData['emp_ruc']}</span><br>
<div style='margin-top: 10px'></div>
<span><strong>COTIZACION</strong></span><br>
<div style='margin-top: 10px'></div>
</div>
</div>
</div>

<div style='background-color: #ffffff; width: 100%;margin-top: 10px;border: 2px solid #1e1e1e; padding: 8px;'>
<table width='100%'>
 <tr>
    <td style='width: 60%'><strong>Cliente</strong></td>
    <td><strong>Fecha de emision</strong></td>
    <td><strong>Condicion de pago</strong></td>
  </tr>
  <tr>
    <td style='width: 60%;padding-left: 15px;padding-right: 10px;'>{$respuesta['coti_razon']}</td>
    <td>$fechaEmision</td>
    <td>{$respuesta['formapagonom']}</td>
  </tr>
  <tr style=''>
    <td style='width: 60%;padding-top: 10px;'><strong>RUC:</strong>  {$respuesta['coti_ruc']}</td>
    <td><strong>Vendedor</strong></td>
    <td><strong></strong></td>
  </tr>
   <tr>
    <td style='width: 60%;padding-left: 15px;padding-right: 10px;'>{$respuesta['cliente_direc']}</td>
    <td style='padding-right: 10px;' valign='top'>{$respuesta['coti_ate']}</td>
    <td></td>
  </tr>
</table>
</div>
<div style='background-color: #ffffff; width: 100%;margin-top: 10px;'>
<table style='width:100%'>
  <tr>
    <th style='color: white; background-color: #404040;padding: 5px;'>CANT.</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>SKU</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>DESCRIPCION</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>VALOR U.</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>IMPORTE</th>
  </tr>
  $prodHTML
  <tr>
  <td colspan='4' style='text-align: right;border-right: 1px solid black'>SUB TOTAL  </td>
  <td  style='text-align: center;border: 1px solid black'>$simb $subtotalPrin</td>
</tr>
 <tr>
  
  <td colspan='4' style='text-align: right;border-right: 1px solid black'>IGV</td>
  <td  style='text-align: center;border: 1px solid black'>$simb $igv</td>
</tr>
 <tr>
 <td colspan='3' style='text-align: left;'>Importe en letras</td>
  <td  style='text-align: right;border-right: 1px solid black'>DESCUENTO</td>
  <td  style='text-align: center;border: 1px solid black'>$simb $descuento</td>
</tr>
 <tr>
 <td colspan='3' style='text-align: left;font-size: 13px'>$totalLetras</td>
  <td  style='text-align: right;border-right: 1px solid black'>TOTAL</td>
  <td  style='text-align: center;border: 1px solid black'>$simb $total</td>
</tr>
</table>
</div>
$qrImage
  ";

    $mpdf->WriteHTML($cabeza,\Mpdf\HTMLParserMode::HTML_BODY);
    $contHoja++;
    if ($contHoja<count($productosSeparadosEmpresas)){
        $mpdf->AddPage();
    }

}


$mpdf->Output();