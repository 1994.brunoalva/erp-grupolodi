<?php
require '../../conexion/Conexion.php';
require '../../model/Cotizacion.php';
require '../../model/DetalleCotizacion.php';
require '../../model/Venta.php';
require '../../model/DetalleVenta.php';
require '../../utils/Tools';

require_once('../../Lib/mpdf/vendor/autoload.php');
require_once('../../Lib/vendor/autoload.php');//Llamare el autoload de la clase que genera el QR
use Endroid\QrCode\QrCode;
use Luecano\NumeroALetras\NumeroALetras;

$formatter = new NumeroALetras;
$venta=new Venta();
$detalleVenta=new DetalleVenta();
$tools = new Tools();

$mpdf = new \Mpdf\Mpdf();
$stylesheet = file_get_contents('stylepdf.css');

$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);




$idCoti = $_GET['coti'];


$cotizacion = new Cotizacion();
$detalleCotizacion=new DetalleCotizacion();


$idCOtizacion = $idCoti;

$cotizacion->setCotiId($idCOtizacion);

$respuesta = $cotizacion->verCotizacion()->fetch_assoc();
$productos=[];
$venta->setIdCoti($idCOtizacion);

$res_ventas = $venta->listaProCoti();

$detalleCotizacion->setIdCotizacion($idCOtizacion);
$res = $detalleCotizacion->verLista();
$prodHTML ="";
$contador =1;
$subtotal=0;
$descuento = 0;
$simb = $respuesta['coti_tp_moneda']==1?'$':'S/.';
//echo $respuesta['coti_tp_moneda']. '................';
$fechaEmision=$respuesta['coti_fecha'];
$contHoja=0;
foreach ($res_ventas as $vnRow){
    $prodHTML ="";
    $contador =1;
    $subtotal=0;
    $descuento = 0;
    $idRmpresaHoja=0;

    $seriDoc = $vnRow['serie'];
    $numeroDoc = $tools->numeroParaDocumento($vnRow['numero'],5);

    $qrImage='';

    $detalleVenta->setIdVenta($vnRow['id']);
    $res_proDeta = $detalleVenta->listaProVenta();

    if (isset($_GET['venta'])){
        $idventa= $_GET['venta'];
        $venta->setId($idventa);

        $resV = $venta->getData()->fetch_assoc();

        $qrCode = new QrCode("ruc empresa|tipoComprobante|seri numero|igv|total|{$resV['fecha']}|tipoDocumento|numDocumen|hash|firmadijital");
        $qrCode->setSize(150);
        $image= $qrCode->writeString();//Salida en formato de texto
        $imageData = base64_encode($image);
        $qrImage='<img src="data:image/png;base64,'.$imageData.'">';
        $fechaEmision=$resV['fecha'];
    }

    foreach ($res_proDeta as $dproRow){

        $idRmpresaHoja = $dproRow['id_empresa'];
        $importe = $dproRow['cantidad']*$dproRow['precio_unitario'];

        $subtotal+= $importe;
        $importeConDes= $importe ;

        $prodHTML .= "
    <tr>
            <td class='td' style='text-align: center'>{$dproRow['cantidad']}</td>
           <td class='td'>{$dproRow['produ_sku']}</td>
           <td class='td'>{$dproRow['producto_desc']}</td>
           <td class='td' style='text-align: center'>$simb {$dproRow['precio_unitario']}</td>
           <td class='td' style='text-align: center'>$simb $importeConDes</td>
           
        </tr>
    ";

    }

    $empresaData = $venta->getEmpres($vnRow['id_empresa'])->fetch_assoc();
    $igv = number_format($subtotal*0.18, 2, '.', '');


    $total =  $subtotal+$igv;

    /*  $subtotalPrin= number_format($total/1.19,2,'.','');
      $igvPrin =  number_format($subtotalPrin*0.19,2,'.','');*/

    $totalLetras =   $formatter->toInvoice($total, 2, $respuesta['coti_tp_moneda']==1?'DOLARES':'SOLES');


    $cabeza= "
 <div style='width: 100%; background-color: #ffffff' >
  <div style=' width: 65%;float: left; background-color: #ffffff'>
<div style='width: 47%;float: left; background-color: #ffffff' >
 <img style='max-width: 100%;' src='../../imagenes/logo/{$empresaData['emp_logo']}'>
</div>
   
    <div style='width:53%;text-align: center; font-size: 13px; float: right; background-color: #ffffff'>
       <span><strong>{$empresaData['emp_nombre']}</strong></span><br>
       <span>{$empresaData['emp_dir']}</span><br>
       <span><strong>Telf. </strong>{$empresaData['emp_tel']}</span><br>
    </div>
</div>
<div style=' width: 34%;text-align: center; background-color: #ffffff ; float: right;'>

<div style='width: 100%; height: 130px; border: 2px solid #1e1e1e' class='contenedor'>
<div style='margin-top:20px'></div>
<span>RUC: {$empresaData['emp_ruc']}</span><br>
<div style='margin-top: 10px'></div>
<span><strong>FACTURA ELECTRONICA</strong></span><br>
<div style='margin-top: 10px'></div>
<span>Nro. $seriDoc-$numeroDoc</span>
</div>
</div>
</div>

<div style='background-color: #ffffff; width: 100%;margin-top: 10px;border: 2px solid #1e1e1e; padding: 8px;'>
<table width='100%'>
 <tr>
    <td style='width: 60%'><strong>Cliente</strong></td>
    <td><strong>Fecha de emision</strong></td>
    <td><strong>Condicion de pago</strong></td>
  </tr>
  <tr>
    <td style='width: 60%;padding-left: 15px;padding-right: 10px;'>{$respuesta['coti_razon']}</td>
    <td>$fechaEmision</td>
    <td>{$respuesta['formapagonom']}</td>
  </tr>
  <tr style=''>
    <td style='width: 60%;padding-top: 10px;'><strong>RUC:</strong>  {$respuesta['coti_ruc']}</td>
    <td><strong>Vendedor</strong></td>
    <td><strong></strong></td>
  </tr>
   <tr>
    <td style='width: 60%;padding-left: 15px;padding-right: 10px;'>{$respuesta['coti_ruc']}</td>
    <td style='padding-right: 10px;' valign='top'>{$respuesta['coti_ate']}</td>
    <td></td>
  </tr>
</table>
</div>
<div style='background-color: #ffffff; width: 100%;margin-top: 10px;'>
<table style='width:100%'>
  <tr>
    <th style='color: white; background-color: #404040;padding: 5px;'>CANT.</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>SKU</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>DESCRIPCION</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>VALOR U.</th>
    <th style='color: white; background-color: #404040;padding: 5px;'>IMPORTE</th>
  </tr>
  $prodHTML
  <tr>
  <td colspan='4' style='text-align: right;border-right: 1px solid black'>SUB TOTAL  </td>
  <td  style='text-align: center;border: 1px solid black'>$simb $subtotal</td>
</tr>
 <tr>
  
  <td colspan='4' style='text-align: right;border-right: 1px solid black'>IGV</td>
  <td  style='text-align: center;border: 1px solid black'>$simb $igv</td>
</tr>
 <tr>
 <td colspan='3' style='text-align: left;'>Importe en letras</td>
  <td  style='text-align: right;border-right: 1px solid black'>DESCUENTO</td>
  <td  style='text-align: center;border: 1px solid black'>$simb $descuento</td>
</tr>
 <tr>
 <td colspan='3' style='text-align: left;font-size: 13px'>$totalLetras</td>
  <td style='text-align: right;border-right: 1px solid black'>TOTAL</td>
  <td  style='text-align: center;border: 1px solid black'>$simb $total</td>
</tr>
</table>
</div>
$qrImage
  ";

    $mpdf->WriteHTML($cabeza,\Mpdf\HTMLParserMode::HTML_BODY);
    $contHoja++;
    if ($contHoja<$res_ventas->num_rows){
        $mpdf->AddPage();
    }


}

/*
$productosSeparadosEmpresas=[];
foreach ($resDetalleCoti as $row){
    if (count($productosSeparadosEmpresas)>0){
        $vali=true;
        for($in=0;$in<count($productosSeparadosEmpresas);$in++){
            if (count($productosSeparadosEmpresas[$in]["productos"])>0){
                if ($productosSeparadosEmpresas[$in]['idEmpre']==$row['emp_id']){
                    $productosSeparadosEmpresas[$in]["monto"]+=$row['cantidad']*$row['precio_unitario'];
                    $productosSeparadosEmpresas[$in]["productos"][]=$row;
                    $vali=false;
                }
            }

        }
        if ($vali){
            //$productosSeparadosEmpresas[]=array($row);
            $productosSeparadosEmpresas[]=array(
                "idEmpre"=>$row['emp_id'],
                "monto"=>$row['cantidad']*$row['precio_unitario'],
                "productos"=>array($row)
            );
        }
    }else{
        $productosSeparadosEmpresas[]=array(
            "idEmpre"=>$row['emp_id'],
            "monto"=>$row['cantidad']*$row['precio_unitario'],
            "productos"=>array($row)
        );
    }
}
*/




$mpdf->Output();