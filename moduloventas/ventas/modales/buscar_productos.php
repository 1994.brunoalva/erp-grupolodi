<div class="modal fade" id="modal_buscar_productos" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Buscar Productos</h3>
                </div>
            </div>

            <div class="modal-body  no-border">
                <form action="#">
                    <div class="container-fluid">

                        <div class="table-responsive form-group col-xs-12 no-padding">
                            <table id="table-productos" class="table table-striped table-bordered table-hover">
                                <thead class="bg-head-table">
                                <tr>

                                    <th class="text-left">PRODUCTO</th>
                                    <th class="text-left">SKU</th>
                                    <th class="text-left">STOCK</th>
                                    <th class="text-left">PRECRIO</th>
                                    <th class="text-left">MARCA</th>
                                    <th class="text-left">PAIS</th>
                                    <th class="text-left">EMPRESA</th>
                                    <th class="text-left">OPCION</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="(pro, index) in productos.lista" >

                                    <td class="text-left">
                                        <label>{{pro.produ_nombre}}</label>
                                    </td>
                                    <td class="text-left">
                                        <div id="emp_num_ruc" >{{pro.produ_sku}}</div>
                                    </td>
                                    <td class="text-left">{{pro.cantidad}}</td>
                                    <td class="text-left">{{pro.precio}}</td>
                                    <td class="text-left">{{pro.mar_nombre}}</td>
                                    <td id="emp-dir" class="text-left">{{pro.pais_nombre}}</td>
                                    <td class="text-center">{{pro.emp_nombre}}</td>
                                    <td class="text-center">
                                        <button v-on:click="setSelecteditemProductoIN(index)" class="btn btn-sm btn-success fa fa-check btn-selector-cliente"
                                                title="Anadir item" data-dismiss="modal"></button>

                                    </td>
                                </tr>

                                </tbody>

                            </table>
                        </div>

                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <!-- <a type="submit" id="modal-buscar-empresa-btn-guardar" class="btn btn-primary">
                             Guardar
                         </a>
                         <button type="button" id="modal-buscar-empresa-btn-limpiar" class="btn btn-default">
                             Limpiar
                         </button>-->
                        <button type="button" id="modal-buscar-empresa-btn-agregar" class="btn btn-primary"
                                data-toggle="modal" data-target="#modal_agregar_cliente">
                            Agregar
                        </button>
                        <button type="button" id="modal-buscar-empresa-btn-cerrar" class="btn btn-danger"
                                data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>