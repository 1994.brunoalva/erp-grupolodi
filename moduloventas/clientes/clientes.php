<?php
$indexRuta=1;
require '../conexion/Conexion.php';
$conexion = (new Conexion())->getConexion();
$departamentos = $conexion->query("SELECT * FROM sys_ven_departamento");
$clasificaciones = $conexion->query("SELECT * FROM sys_cos_tipo_lista");
$contactos = $conexion->query("SELECT * FROM sys_cliente_contacto");

$nombremodule="clientes";

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../public/css/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../public/plugins/sweetalert2/sweetalert2.min.css">

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Clientes</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <button onclick="" id="folder_btn_nuevo_folder" class="btn btn-primary" data-toggle="modal" data-target="#modal_register_cliente">
                                            <i class="fa fa-plus "></i> Nuevo Cliente
                                        </button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div class="col-md-12 text-right">
                                   <!--span>Buscar:</span> <input-->
                                </div>
                                <div id="conten-modales">

                                    <table id="table-clientes" class="table table-striped table-bordered table-hover">
                                        <thead class="bg-head-table">
                                        <tr style="background-color: #007ac3; color: white">
                                            <th  style="border-right-color: #007ac3"  class="text-center"></th>
                                            <th  style="border-right-color: #007ac3"  class="text-center">TP</th>
                                            <th style="border-right-color: #007ac3"  class="text-center">NUMERO</th>
                                            <th style="border-right-color: #007ac3"  class="text-center">NOMBRE / RAZON SOCIAL</th>
                                            <th style="border-right-color: #007ac3"  class="text-center">TELEFONO 1</th>
                                            <th style="border-right-color: #007ac3"  class="text-center">TELEFONO 2</th>
                                            <th class="text-center">ACCION</th>
                                        </tr>
                                        </thead>



                                    </table>



                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>

    <div id="container-cliente">
        <div class="modal fade" id="modal_register_cliente" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document" style="width: 80%;">
                <div class="modal-content">
                    <div class="modal-header no-border no-padding">
                        <div class="modal-header text-center color-modal-header">
                            <h3 class="modal-title">Registrar <!--Empresa-->Cliente</h3>
                        </div>
                    </div>


                    <div class="modal-body  no-border">
                        <form  v-on:submit.prevent="guardarCliente">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                        <label class="col-xs-12 no-padding">RUC / DNI:</label>

                                        <div class="input-group col-xs-12 no-padding">
                                            <input v-model="dataR.numDoc" @keypress="onlyNumber" required   type="text" class="form-control">
                                            <span
                                                    class="input-group-btn"><button
                                                        v-on:click="consultaDoc(1)"
                                                        type="button"
                                                        class="btn btn-primary"><i
                                                            class="fa fa-search"></i></button></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-5 col-sm-5 col-md-5">
                                        <label class="col-xs-12 no-padding">NOMBRE / RAZON SOCIAL :</label>
                                        <div class="input-group col-xs-12">
                                            <input  v-model="dataR.nomRaz" required disabled   type="text" class="form-control input-number"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">ESTADO:</label>
                                        <div class="input-group col-xs-12">
                                            <input disabled   v-model="dataR.estado"  type='text' class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">CONDICION:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataR.condicion" disabled type='text' class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                        <label class="col-xs-12 no-padding">DIRECCION</label>
                                        <div class="input-group col-xs-12">
                                            <input :disabled="!editDirec" v-model="dataR.direccion" required   type="text" class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">DEPARTAMENTO:</label>
                                        <div class="input-group col-xs-12">
                                            <select @change="selectDepart($event)" v-model="dataR.depart" data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                                <?php
                                                foreach ($departamentos as $dep){
                                                    echo "<option value='" .$dep['dep_cod']. "'>" .$dep['dep_nombre']. "</option>";
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">PROVINCIA:</label>
                                        <div class="input-group col-xs-12">
                                            <select @change="selectProvin($event)"  v-model="dataR.provin" data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                                <option v-for="item in listProvincias" :value="item.pro_cod">{{item.pro_nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">DISTRITO:</label>
                                        <div class="input-group col-xs-12">
                                            <select  v-model="dataR.distrito" data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                                <option v-for="item in listDistritos" :value="item.dis_codigo">{{item.dis_nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">UBIGEO:</label>
                                        <div class="input-group col-xs-12">
                                            <input disabled v-model="ubigeoSelect"  type="text" class="form-control input-number"
                                                    aria-describedby="basic-addon1"
                                                    value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                        <label class="col-xs-12 no-padding">EMAIL 1:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataR.email1" type="email" class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                        <label class="col-xs-12 no-padding">EMAIL 2:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataR.email2"  type="email" class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">TELEFONO 1:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataR.telf1" type="text" class="form-control input-number"
                                                    aria-describedby="basic-addon1"
                                                    value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">Telefono 2:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataR.telf2" type="text" class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                        <label class="col-xs-12 no-padding"> CLASIFICACION:</label>
                                        <div class="input-group col-xs-12 no-padding">
                                            <select v-model="dataR.clas"  required
                                                     class="form-control no-padding"
                                                     data-live-search="true">
                                                <?php
                                                foreach ($clasificaciones as $rowcls){
                                                    if ($rowcls['tipl_estatus']!=0){
                                                        echo "<option value='{$rowcls['tipl_id']}'>{$rowcls['tipl_categoria']}</option>";
                                                    }

                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                        <label class="col-xs-12 no-padding">CONTACTO:</label>
                                        <div class="input-group col-xs-12">
                                            <select  v-model="dataR.contac"
                                                     data-live-search="true" data-size="5" class="form-control no-padding selectpicker lista-contacto"

                                                     >
                                                <?php
                                                foreach ($contactos as $rowCon){
                                                    echo "<option value='{$rowCon['cli_con_id']}'>{$rowCon['nombre_cont']}</option>";
                                                }
                                                ?>
                                            </select>
                                            <span
                                                    class="input-group-btn"><button
                                                        type="button"
                                                        data-toggle="modal" data-target="#modal-contacto"

                                                        class="btn btn-primary"><i
                                                            class="fa fa-plus"></i></button></span>

                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                        <label class="col-xs-12 no-padding">VENDEDOR:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataR.vend" type=text class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div v-if="dataR.tipoDoc=='RUC'||true" class="col-md-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home">DIRECCIONES ({{observadorDirR}})</a> </li>
                                    <li><a data-toggle="tab" href="#menu1">REPRESENTANTES LEGALES </a></li>
                                </ul>

                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <button v-on:click="addSwiDir(2)" data-toggle="modal" data-target="#modal-direccion" type="button" class="btn btn-success"><i class="fa fa-plus"></i> Agregar</button>
                                        <table id="tabaRDirec" class="table table-bordered table-condensed">
                                            <thead>
                                            <tr  style="background-color: rgb(0, 122, 195);" >
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">Direccion</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">Ubigeo</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">Tipo</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center"></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                    <div id="menu1" class="tab-pane fade">
                                        <table class="table table-bordered table-condensed">
                                            <tr  style="background-color: rgb(0, 122, 195);" >
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">TI. DOC</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">NRO. DOC</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">NOMBRE</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">CARGO</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">DESDE</th>

                                            </tr>
                                            <tr v-for="item in dataR.representantes">
                                                <td class="text-center">{{item.tipodoc}}</td>
                                                <td class="text-center">{{item.numdoc}}</td>
                                                <td class="text-center">{{item.nombre}}</td>
                                                <td class="text-center">{{item.cargo}}</td>
                                                <td class="text-center">{{item.desde}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    </div>
                            </div>


                            <div class="container-fluid">
                                <hr class="line-frame-modal">
                            </div>
                            <div class="container-fluid text-right">

                                <!-- <a type="submit" id="modal-buscar-empresa-btn-guardar" class="btn btn-primary">
                                     Guardar
                                 </a>
                                 <button type="button" id="modal-buscar-empresa-btn-limpiar" class="btn btn-default">
                                     Limpiar
                                 </button>-->
                                <button type="submit"  class="btn btn-primary">
                                    Guardar
                                </button>
                                <button type="button"  class="btn btn-success"
                                        data-dismiss="modal">
                                    Cerrar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal_aditar_cliente" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document" style="width: 80%;">
                <div class="modal-content">
                    <div class="modal-header no-border no-padding">
                        <div class="modal-header text-center color-modal-header">
                            <h3 class="modal-title"><!--Empresa-->Cliente</h3>
                        </div>
                    </div>


                    <div class="modal-body  no-border">
                        <form  v-on:submit.prevent="actualizarCliente">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                        <label class="col-xs-12 no-padding">RUC / DNI:</label>

                                        <div class="input-group col-xs-12 no-padding">
                                            <input v-model="dataE.numDoc"  required @keypress="onlyNumber"  type="text" class="form-control">
                                            <span
                                                    class="input-group-btn"><button
                                                        v-on:click="consultaDocE(1)"
                                                        type="button"
                                                        class="btn btn-primary"><i
                                                            class="fa fa-search"></i></button></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-5 col-sm-5 col-md-5">
                                        <label class="col-xs-12 no-padding">NOMBRE / RAZON SOCIAL :</label>
                                        <div class="input-group col-xs-12">
                                            <input  v-model="dataE.nomRaz" required disabled   type="text" class="form-control input-number"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">ESTADO:</label>
                                        <div class="input-group col-xs-12">
                                            <input disabled   v-model="dataE.estado"  type='text' class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">CONDICION:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataE.condicion" disabled type='text' class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                        <label class="col-xs-12 no-padding">DIRECCION</label>
                                        <div class="input-group col-xs-12">
                                            <input :disabled="!editDirec" v-model="dataE.direccion" required   type="text" class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">DEPARTAMENTO:</label>
                                        <div class="input-group col-xs-12">
                                            <select @change="selectDepart($event)" v-model="dataE.depart" data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                                <?php
                                                foreach ($departamentos as $dep){
                                                    echo "<option value='" .$dep['dep_cod']. "'>" .$dep['dep_nombre']. "</option>";
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">PROVINCIA:</label>
                                        <div class="input-group col-xs-12">
                                            <select @change="selectProvin($event)"  v-model="dataE.provin" data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                                <option v-for="item in listProvincias" :value="item.pro_cod">{{item.pro_nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">DISTRITO:</label>
                                        <div class="input-group col-xs-12">
                                            <select  v-model="dataE.distrito" data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                                <option v-for="item in listDistritos" :value="item.dis_codigo">{{item.dis_nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">UBIGEO:</label>
                                        <div class="input-group col-xs-12">
                                            <input disabled v-model="ubigeoSelectE"  type="text" class="form-control input-number"
                                                    aria-describedby="basic-addon1"
                                                    value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                        <label class="col-xs-12 no-padding">EMAIL 1:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataE.email1" type="email" class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                        <label class="col-xs-12 no-padding">EMAIL 2:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataE.email2"  type="email" class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">TELEFONO 1:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataE.telf1" type="text" class="form-control input-number"
                                                    aria-describedby="basic-addon1"
                                                    value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                        <label class="col-xs-12 no-padding">Telefono 2:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataE.telf2" type="text" class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                        <label class="col-xs-12 no-padding"> CLASIFICACION:</label>
                                        <div class="input-group col-xs-12 no-padding">
                                            <select v-model="dataE.clas"  required
                                                     class="form-control no-padding"
                                                     data-live-search="true">
                                                <?php
                                                foreach ($clasificaciones as $rowcls){
                                                    if ($rowcls['tipl_estatus']!=0){
                                                        echo "<option value='{$rowcls['tipl_id']}'>{$rowcls['tipl_categoria']}</option>";
                                                    }

                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                        <label class="col-xs-12 no-padding">CONTACTO:</label>
                                        <div class="input-group col-xs-12">
                                            <select  v-model="dataE.contac"
                                                     data-live-search="true" data-size="5" class="form-control no-padding selectpicker lista-contacto"

                                            >
                                                <?php
                                                foreach ($contactos as $rowCon){
                                                    echo "<option value='{$rowCon['cli_con_id']}'>{$rowCon['nombre_cont']}</option>";
                                                }
                                                ?>
                                            </select>
                                            <span
                                                    class="input-group-btn"><button
                                                        type="button"
                                                        data-toggle="modal" data-target="#modal-contacto"
                                                        class="btn btn-primary"><i
                                                            class="fa fa-plus"></i></button></span>

                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                        <label class="col-xs-12 no-padding">VENDEDOR:</label>
                                        <div class="input-group col-xs-12">
                                            <input v-model="dataE.vend" type=text class="form-control"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div v-if="dataE.tipoDoc=='RUC'||true" class="col-md-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home2">DIRECCIONES ({{observadorDirE}})</a></li>
                                    <li><a data-toggle="tab" href="#menu2">REPRESENTANTES LEGALES </a></li>
                                </ul>

                                <div class="tab-content">
                                    <div id="home2" class="tab-pane fade in active">
                                        <button v-on:click="addSwiDir(1)" data-toggle="modal" data-target="#modal-direccion" type="button" class="btn btn-success"><i class="fa fa-plus"></i> Agregar</button>
                                        <table id="tabaEDirec" class="table table-bordered table-condensed">
                                            <thead>
                                            <tr  style="background-color: rgb(0, 122, 195);" >
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">Direccion</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">Ubigeo</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">Tipo</th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                    <div id="menu2" class="tab-pane fade">
                                        <table  class="table table-bordered table-condensed">
                                            <thead>
                                            <tr  style="background-color: rgb(0, 122, 195);" >
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">TI. DOC</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">NRO. DOC</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">NOMBRE</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">CARGO</th>
                                                <th style="border-right-color: rgb(0, 122, 195); color: white; text-align: center">DESDE</th>

                                            </tr>
                                            </thead>
                                            <tbody>


                                            <tr v-for="item in dataE.representantes">
                                                <td class="text-center">{{item.rep_tdoc}}</td>
                                                <td class="text-center">{{item.rep_ndoc}}</td>
                                                <td class="text-center">{{item.rep_nomape}}</td>
                                                <td class="text-center">{{item.rep_cargo}}</td>
                                                <td class="text-center">{{item.rep_desde}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                            </div>


                            <div class="container-fluid">
                                <hr class="line-frame-modal">
                            </div>
                            <div class="container-fluid text-right">

                                <!-- <a type="submit" id="modal-buscar-empresa-btn-guardar" class="btn btn-primary">
                                     Guardar
                                 </a>
                                 <button type="button" id="modal-buscar-empresa-btn-limpiar" class="btn btn-default">
                                     Limpiar
                                 </button>-->
                                <button type="submit"  class="btn btn-primary">
                                    Guardar
                                </button>
                                <button type="button"  class="btn btn-success"
                                        data-dismiss="modal">
                                    Cerrar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-direccion" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <form v-on:submit.prevent="agregarDireccion">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0866c6;text-align: center">
                            <h3 class="modal-title" id="exampleModalLabel" style="color: white"> Agregar Direccion</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="form-group col-xs-12">
                                        <label class="col-xs-12 no-padding">Direccion:</label>
                                        <div class="input-group col-xs-12">
                                            <input   type='text' class="form-control"
                                                     v-model="direcData.direccion"
                                                     aria-describedby="basic-addon1"
                                                     required
                                                     value="" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-4">
                                        <label class="col-xs-12 no-padding">DEPARTAMENTO:</label>
                                        <div class="input-group col-xs-12">
                                            <select @change="selectDepart2($event)" v-model="direcData.depart" required data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                                <?php
                                                foreach ($departamentos as $dep){
                                                    echo "<option value='" .$dep['dep_cod']. "'>" .$dep['dep_nombre']. "</option>";
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4 ">
                                        <label class="col-xs-12 no-padding">PROVINCIA:</label>
                                        <div class="input-group col-xs-12">
                                            <select required @change="selectProvin2($event)"  v-model="direcData.provin" data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                                <option v-for="item in listProvincias2" :value="item.pro_cod">{{item.pro_nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-4">
                                        <label class="col-xs-12 no-padding">DISTRITO:</label>
                                        <div class="input-group col-xs-12">
                                            <select required v-model="direcData.distrito" data-live-search="true" data-size="5" class="form-control no-padding selectpicker" >
                                                <option v-for="item in listDistritos2" :value="item.dis_codigo">{{item.dis_nombre}}</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">

                            <button  type="submit" class="btn btn-primary">Cuardar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>


    <div class="modal fade" id="modal-contacto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #0866c6;text-align: center">
                    <h3 class="modal-title" id="exampleModalLabel" style="color: white"> Nuevo Contacto</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <label class="col-xs-12 no-padding">Nombre:</label>
                                <div class="input-group col-xs-12">
                                    <input  id="nom-contac"  type='text' class="form-control"
                                             aria-describedby="basic-addon1"
                                             value="" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <button onclick="guardarContacto() " type="button" class="btn btn-primary">Cuardar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="module" src="../public/js/Input_validate.js"></script>
    <script src="../public/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="../public/js/modal_clientes.js" ></script>

    <script>

        function guardarContacto() {
            const nombre = $("#nom-contac").val();
            if (nombre.length>0){
                $.ajax({
                    type: "POST",
                    url: "../ajaxs/ajs_bd_datas.php",
                    data: {nombre,tipo:'reg-cont'},
                    success: function (resp) {
                        resp=JSON.parse(resp);
                        if (resp.res){
                            $(".lista-contacto").append("<option value='"+resp.id+"'>"+nombre+"</option>")
                            $("#nom-contac").val('')
                            $('#modal-contacto').modal('hide');
                            $(".selectpicker").selectpicker('refresh');
                        } else{
                            $.toast({
                                heading: 'ERROR',
                                text: "Error en el servidor",
                                icon: 'error',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        }
                        console.log(resp)
                    }
                });

            }else{
                $.toast({
                    heading: 'INFORMACION',
                    text: "Escriba un nombre",
                    icon: 'info',
                    position: 'top-right',
                    hideAfter: '2500',
                });
                $("#nom-contac").focus();
            }
        }

        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>


<script type="text/javascript">
    var tablaCliente;

    APP_clientes.setFuntExe(function () {
        tablaCliente.ajax.reload();
    });
    $(document).ready(function() {



        tablaCliente = $('#table-clientes').DataTable({
            "processing": true,
            "serverSide": true,
            "sAjaxSource": "../ServerSide/serversideClientes.php",
            order: [[ 0, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel'
            ],
            columnDefs: [
                {
                    "targets": 0,
                    "data": "coti_id",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;color: white">'+row[0]+'</span>';

                    }
                },
                {
                    "targets": 1,
                    "data": "cli_tdoc",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[1]+'</span>';

                    }
                },
                {
                    "targets": 2,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[2]+'</span>';

                    }
                },
                {
                    "targets": 3,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[3]+'</span>';

                    }
                },
                {
                    "targets": 4,
                    "data": "",
                    "render": function (data, type, row, meta) {
                        return '<span style="display: block;margin: auto;text-align: center;">'+row[4]+'</span>';

                    }
                },
                {
                    "targets": 5,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[5]+'</span>';

                    }
                },
                {
                    "targets": 6,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<button onclick="APP_clientes.getDataCliente('+row[0]+')" data-toggle="modal" data-target="#modal_aditar_cliente" class="btn btn-info" style="display: block;margin: auto;text-align: center;"><i class="fa fa-edit"></i></button>';

                    }
                },
            ],
            language: {
                url: '../assets/Spanish.json'
            }
        });
        $('#1111111111111').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });


    });



</script>

</html>
