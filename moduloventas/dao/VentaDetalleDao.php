<?php

require "../models/VentaDetalle.php";
require_once "../conexion/Conexion.php";

class VentaDetalleDao extends  VentaDetalle
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function getProductos(){
        $sql ="
            SELECT 
               fac_deta.*,
               prod_empre.cantidad,
               empr.emp_nombre,
               prod.produ_nombre,
               prod.produ_sku,
               prod.peso,
               marca.mar_nombre,
               pais.pais_nombre,
               unidad.unidad_nombre
               
            FROM
              sys_ven_facturacion_detalle AS fac_deta 
              INNER JOIN sys_producto_empresa AS prod_empre 
                ON fac_deta.prod_id = prod_empre.prod_empre_id 
                INNER JOIN sys_producto AS prod ON prod_empre.id_prod=prod.produ_id 
                INNER JOIN sys_empresas AS empr ON prod_empre.id_empresa=empr.emp_id
                INNER JOIN sys_com_marca AS marca ON prod.mar_id=marca.mar_id
                INNER JOIN sys_pais AS pais ON prod.pais_id = pais.pais_id
                INNER JOIN sys_unidad AS unidad ON prod.unidad_id = unidad.unidad_id
                WHERE fac_deta.fac_id=". $this->getFacId();
        return $this->conexion->query($sql);
    }

    public function insertar(){
        $sql = "INSERT INTO  sys_ven_facturacion_detalle VALUES (null,?,?,?,?,?,?,?,?);";

        $prod_id=$this->getProdId();
        $facd_cantidad=$this->getFacdCantidad();
        $facd_preciou=$this->getFacdPreciou();
        $facd_descuento=$this->getFacdDescuento();
        $facd_subtotal=$this->getFacdSubtotal();
        $fac_id=$this->getFacId();
        $origen=$this->getOrigen();
        $estado = $this->getEstado();

        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param("ssssssss",$prod_id ,$facd_cantidad,$facd_preciou,$facd_descuento,$facd_subtotal,$fac_id,$origen,$estado);

        $res = $stmt->execute();
        //echo  "INSERT INTO  sys_ven_facturacion_detalle VALUES (null,$prod_id,$facd_cantidad,$facd_preciou,$facd_descuento,$facd_subtotal,$fac_id,'$origen','$estado');";
        $stmt->close();
        return $res;
    }

    public function exeSql($sql){
        return $this->conexion->query($sql);

    }
}