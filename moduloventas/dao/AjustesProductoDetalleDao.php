<?php
require "../models/AjustesProductoDetalle.php";
require_once "../conexion/Conexion.php";

class AjustesProductoDetalleDao extends  AjustesProductoDetalle
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        $sql="INSERT INTO sys_prod_ajustes_detalle VALUES (null,?,?,?,?,?);";
        $stmt = $this->conexion->prepare($sql);

             $ajus_id=$this->getAjusId();
             $prod_empr_id=$this->getProdEmprId();
             $cnt_actual=$this->getCntActual();
             $nuevo_cnt=$this->getNuevoCnt();
             $estado=$this->getEstado();


        $stmt->bind_param("sssss",$ajus_id, $prod_empr_id, $cnt_actual, $nuevo_cnt, $estado);
        $res = $stmt->execute();

        $this->setAjusId($stmt->insert_id);
        $stmt->close();
        return $res;
    }
    public function getData(){
        $sql ="SELECT 
                  aj_deta.ajus_det_id,
                  aj_deta.ajus_id,
                  aj_deta.prod_empr_id,
                  aj_deta.cnt_actual,
                  aj_deta.nuevo_cnt,
                  aj_deta.estado,
                  prod.produ_nombre,
                  prod.produ_sku
                FROM
                  sys_prod_ajustes_detalle AS aj_deta 
                  INNER JOIN sys_producto_empresa AS prod_emp 
                    ON aj_deta.prod_empr_id = prod_emp.prod_empre_id 
                    INNER JOIN sys_producto AS prod ON prod_emp.prod_empre_id = prod.produ_id
                WHERE aj_deta.ajus_id = " . $this->getAjusId();

        return $this->conexion->query($sql);
    }

}