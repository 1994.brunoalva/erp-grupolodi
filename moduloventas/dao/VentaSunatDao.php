<?php
require "../models/VentaSunat.php";
require_once "../conexion/Conexion.php";

class VentaSunatDao extends VentaSunat
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }
    public function getData(){
        $sql ="SELECT * FROM sys_venta_sunat WHERE id_venta_sunat=". $this->getIdVentaSunat();
        return $this->conexion->query($sql);
    }
}