<?php
require "../models/TipoPago.php";
require_once "../conexion/Conexion.php";

class TipoPagoDao extends TipoPago
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function getdata(){
        $sql ="SELECT * FROM sys_cob_tip_pagos";
        return $this->conexion->query($sql);
    }

}