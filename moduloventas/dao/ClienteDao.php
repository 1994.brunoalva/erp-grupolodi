<?php
require "../models/Cliente.php";
require_once "../conexion/Conexion.php";

class ClienteDao extends Cliente
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }
    public function verificar(){
        $sql ="SELECT * FROM sys_ven_clientes WHERE cli_ndoc ='{$this->getCliNdoc()}'";
        return $this->conexion->query($sql);
    }
    public function actualizar(){
        $sql="UPDATE sys_ven_clientes
SET 
  cli_tdoc = ?,cli_ndoc = ?,cli_nomape = ?,cli_direc = ?,cli_tele = ?,cli_tele2 = ?,cli_conta = ?,cli_mail1 = ?,
  cli_mail2 = ?,cli_clasi = ?,cli_vende = ?,cli_estad = ?,cli_condi = ?,dep_cod = ?,pro_cod = ?,dis_cod = ?,ubi_cod = ?
WHERE cli_id = ?;";
        $stmt = $this->conexion->prepare($sql);

        $cli_tdoc=$this->getCliTdoc();
        $cli_ndoc=$this->getCliNdoc();
        $cli_nomape=$this->getCliNomape();
        $cli_direc=$this->getCliDirec();
        $cli_tele=$this->getCliTele();
        $cli_tele2=$this->getCliTele2();
        $cli_conta=$this->getCliConta();
        $cli_mail1=$this->getCliMail1();
        $cli_mail2=$this->getCliMail2();
        $cli_clasi=$this->getCliClasi();
        $cli_vende=$this->getCliVende();
        $cli_estad=$this->getCliEstad();
        $cli_condi=$this->getCliCondi();
        $dep_cod=$this->getDepCod();
        $pro_cod=$this->getProCod();
        $dis_cod=$this->getDisCod();
        $ubi_cod=$this->getUbiCod();
        $cli_id = $this->getCliId();

        $stmt->bind_param("ssssssssssssssssss", $cli_tdoc,$cli_ndoc,$cli_nomape,$cli_direc,$cli_tele,$cli_tele2,$cli_conta,
            $cli_mail1,$cli_mail2,$cli_clasi,$cli_vende,$cli_estad,$cli_condi,$dep_cod,$pro_cod,$dis_cod,$ubi_cod,$cli_id);



        $res = $stmt->execute();
        $stmt->close();
        return $res;
    }
    public function insertar(){
        $sql="INSERT INTO sys_ven_clientes VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $stmt = $this->conexion->prepare($sql);

        $cli_tdoc=$this->getCliTdoc();
        $cli_ndoc=$this->getCliNdoc();
        $cli_nomape=$this->getCliNomape();
        $cli_direc=$this->getCliDirec();
        $cli_tele=$this->getCliTele();
        $cli_tele2=$this->getCliTele2();
        $cli_conta=$this->getCliConta();
        $cli_mail1=$this->getCliMail1();
        $cli_mail2=$this->getCliMail2();
        $cli_clasi=$this->getCliClasi();
        $cli_vende=$this->getCliVende();
        $cli_estad=$this->getCliEstad();
        $cli_condi=$this->getCliCondi();
        $dep_cod=$this->getDepCod();
        $pro_cod=$this->getProCod();
        $dis_cod=$this->getDisCod();
        $ubi_cod=$this->getUbiCod();

        $stmt->bind_param("sssssssssssssssss", $cli_tdoc,$cli_ndoc,$cli_nomape,$cli_direc,$cli_tele,$cli_tele2,$cli_conta,
            $cli_mail1,$cli_mail2,$cli_clasi,$cli_vende,$cli_estad,$cli_condi,$dep_cod,$pro_cod,$dis_cod,$ubi_cod);



        $res = $stmt->execute();
        $stmt->close();
        return $res;
    }
    public function getdataBuscar($ter){
        $sql ="SELECT
                  cli_id,
                  cli_ndoc,
                  cli_tele,
                  cli_direc,
                  cli_nomape
                FROM sys_ven_clientes
                WHERE cli_ndoc  LIKE '$ter%'  OR cli_nomape LIKE '$ter%' OR cli_direc LIKE '$ter%'";

        return $this->conexion->query($sql);
    }
    public function getdata(){
        $sql ="SELECT
                  *
                FROM sys_ven_clientes
                WHERE cli_id = ".$this->getCliId();

        return $this->conexion->query($sql);
    }public function getdataContac(){
    $sql ="SELECT * FROM sys_ven_clientes WHERE  sys_ven_clientes.cli_conta = ".$this->getCliConta();
    return $this->conexion->query($sql);

}
    public function getdataByNumDc(){
        $sql ="SELECT
                  *
                FROM sys_ven_clientes WHERE cli_ndoc='".$this->getCliNdoc()."'";
        //echo $sql;

        return $this->conexion->query($sql);
    }
    public function getDireciones(){
        $sql ="SELECT
                  su_id,
                  su_ruc,
                  su_direccion,
                  su_tipo
                FROM sys_ven_clientes_sucursales
                WHERE su_ruc = '" . $this->getCliNdoc()."'";

        return $this->conexion->query($sql);
    }
    public function getDirecion($direcion){
        $sql ="SELECT
                  su_id,
                  su_ruc,
                  su_direccion
                  
                FROM sys_ven_clientes_sucursales
                WHERE su_id = '" . $direcion."'";

        return $this->conexion->query($sql);
    }
    public function exeSQL($sql){
        return $this->conexion->query($sql);
    }
}