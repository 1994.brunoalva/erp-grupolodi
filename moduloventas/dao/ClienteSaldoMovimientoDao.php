<?php
require "../models/ClienteSaldoMovimiento.php";
require_once "../conexion/Conexion.php";


class ClienteSaldoMovimientoDao extends ClienteSaldoMovimiento
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        $sql="INSERT INTO sys_clie_sal_movimiento
VALUES (null,
        '{$this->getClieSalId()}',
        '{$this->getDetalle()}',
        '{$this->getTipoMov()}',
        '{$this->getMonto()}',
        '{$this->getNumRef()}',
        '{$this->getTipoDoc()}');";
        return $this->conexion->query($sql);
    }

}