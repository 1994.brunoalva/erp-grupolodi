<?php
require "../models/GuiaConductor.php";
require_once "../conexion/Conexion.php";

class GuiaConductorDao extends GuiaConductor
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        $sql = "INSERT INTO sys_ven_guia_conductores
                VALUES (null,
                        '{$this->getGuiaRemiId()}',
                        '{$this->getTipoDoc()}',
                        '{$this->getNumeroDocumento()}',
                        '{$this->getNumeroLicencia()}');";
        return $this->conexion->query($sql);
    }

    public function getDatas(){
        $sql="SELECT 
  gui_con.* 
FROM
  sys_ven_guia_conductores  AS gui_con
WHERE gui_con.guia_remi_id =" . $this->getGuiaRemiId();
       // echo $sql;
        return $this->conexion->query($sql);
    }
}