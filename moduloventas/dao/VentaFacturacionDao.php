<?php
require "../models/VentaFacturacion.php";
require_once "../conexion/Conexion.php";

class VentaFacturacionDao extends VentaFacturacion
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function getdataVenta(){
        $sql = "SELECT 
clie.cli_id,
clie.cli_tele,
  clie.cli_nomape,
  empr.emp_nombre,
  vent.* 
FROM
  sys_ven_facturacion AS vent 
  INNER JOIN sys_ven_clientes AS clie 
    ON vent.cli_ndoc = clie.cli_ndoc
    INNER JOIN sys_empresas AS empr ON vent.emp_id = empr.emp_id
    WHERE  vent.fac_id = ". $this->getFacId();
        return $this->conexion->query($sql);
    }
    public function anularVenta(){
        $sql="UPDATE sys_ven_facturacion
SET 
  fecha_a = '{$this->getFechaA()}',
  fac_estatus = 'ANULADO'
WHERE fac_id = " . $this->getFacId();
        return $this->conexion->query($sql);
    }
    public function insertar(){
        $sql ="INSERT INTO sys_ven_facturacion VALUES (null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        $fac_num=$this->getFacNum();
        $fac_ncontrol=$this->getFacNcontrol();
        $fac_fechae=$this->getFacFechae();
        $fac_hora=$this->getFacHora();
        $fac_fechav=$this->getFacFechav();
        $fac_diasc=$this->getFacDiasc();
        $fecha_a=$this->getFechaA();
        $tas_id=$this->getTasId();
        $sun_id=$this->getSunId();
        $cli_ndoc=$this->getCliNdoc();
        $pag_id=$this->getPagId();
        $pag_forma=$this->getPagForma();
        $emp_id=$this->getEmpId();
        $fac_total=$this->getFacTotal();
        $coti_id=$this->getCotiId();
        $fac_serie=$this->getFacSerie();
        $fac_estatu=$this->getFacEstatus();
        $tip_docu_sunat = $this->getTipDocuSunat();
        $fac_cost_list_id = $this->getFacCostListId();
        $observaciones = $this->getObservaciones();
        $direccion= $this->getDireccion();

        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param("sssssssssssssssssssss", $fac_num,$fac_ncontrol,$fac_fechae,$fac_hora,$tip_docu_sunat,$fac_fechav,$fac_diasc,$fecha_a,$tas_id,$sun_id,$cli_ndoc,$pag_id,$pag_forma,$emp_id,$fac_total,$coti_id,$fac_serie,$direccion,$observaciones,$fac_cost_list_id,$fac_estatu);

        $res = $stmt->execute();
        $this->setFacId($stmt->insert_id);
        //echo $stmt->error;
        $stmt->close();
        return $res;
    }
    public function exeSql($sql){
        return $this->conexion->query($sql);
    }
}