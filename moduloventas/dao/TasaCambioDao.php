<?php
require "../models/TasaCambio.php";
require_once "../conexion/Conexion.php";

class TasaCambioDao extends TasaCambio
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function getDatos(){
        $sql = "SELECT * FROM sys_tasa_cambio WHERE tas_id= '{$this->getTasId()}'";

        return $this->conexion->query($sql);
    }
}