<?php
require "../models/Producto.php";
require_once "../conexion/Conexion.php";

class ProductoDao extends Producto
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function getPrecioProducto(){
        $sql = "SELECT  *  FROM sys_cos_lista_detalle WHERE plis_codprod = '{$this->getProduCodprod()}'";
        return $this->conexion->query($sql);
    }
    public function getdataBuscar($ter){
       // $sql ="SELECT * FROM vista_productos_almacen WHERE produ_nombre  LIKE '%$ter%';";
        $idEmpresa = $this->getProdEmpreId();
        $sql ="
SELECT
  `prod_empre`.`prod_empre_id` AS `prod_empre_id`,
  `prod`.`produ_id`            AS `produ_id`,
  prod.produ_codprod ,
  `emp`.`emp_nombre`           AS `emp_nombre`,
  `emp`.`emp_id`               AS `emp_id`,
  `prod`.`produ_nombre`        AS `produ_nombre`,
  `unidad`.`unidad_nombre`     AS `unidad_nombre`,
  `prod`.`produ_sku`           AS `produ_sku`,
  `cate`.`cat_nombre`          AS `cat_nombre`,
  `marc`.`mar_nombre`          AS `mar_nombre`,
  `modelo`.`mod_nombre`        AS `mod_nombre`,
  `pais`.`pais_nombre`         AS `pais_nombre`,
  `prod_empre`.`cantidad` AS `cantidad`
FROM (((((((`sys_producto_empresa` `prod_empre`
         JOIN `sys_producto` `prod`
           ON (`prod_empre`.`id_prod` = `prod`.`produ_id`))
        JOIN `sys_empresas` `emp`
          ON (`prod_empre`.`id_empresa` = `emp`.`emp_id`))
       JOIN `sys_com_categoria` `cate`
         ON (`prod`.`cat_id` = `cate`.`cat_id`))
      JOIN `sys_com_marca` `marc`
        ON (`prod`.`mar_id` = `marc`.`mar_id`))
     JOIN `sys_pais` `pais`
       ON (`prod`.`pais_id` = `pais`.`pais_id`))
    JOIN `sys_com_modelo` `modelo`
      ON (`prod`.`mod_id` = `modelo`.`mod_id`))
   JOIN `sys_unidad` `unidad`
     ON (`prod`.`unidad_id` = `unidad`.`unidad_id`))
WHERE `prod`.`cat_id` != 6  AND prod.produ_nombre LIKE '%$ter%' LIMIT 13";
        //echo $sql;
        return $this->conexion->query($sql);
    }
    public function getdataBuscar2($ter){
        // $sql ="SELECT * FROM vista_productos_almacen WHERE produ_nombre  LIKE '%$ter%';";
        $idEmpresa = $this->getProdEmpreId();
        $sql ="
SELECT
  `prod_empre`.`prod_empre_id` AS `prod_empre_id`,
  `prod`.`produ_id`            AS `produ_id`,
  prod.produ_codprod ,
  `emp`.`emp_nombre`           AS `emp_nombre`,
  `emp`.`emp_id`               AS `emp_id`,
  `prod`.`produ_nombre`        AS `produ_nombre`,
  `unidad`.`unidad_nombre`     AS `unidad_nombre`,
  `prod`.`produ_sku`           AS `produ_sku`,
  `cate`.`cat_nombre`          AS `cat_nombre`,
  `marc`.`mar_nombre`          AS `mar_nombre`,
  `modelo`.`mod_nombre`        AS `mod_nombre`,
  `pais`.`pais_nombre`         AS `pais_nombre`,
  `prod_empre`.`cantidad` AS `cantidad`
FROM (((((((`sys_producto_empresa` `prod_empre`
         JOIN `sys_producto` `prod`
           ON (`prod_empre`.`id_prod` = `prod`.`produ_id`))
        JOIN `sys_empresas` `emp`
          ON (`prod_empre`.`id_empresa` = `emp`.`emp_id`))
       JOIN `sys_com_categoria` `cate`
         ON (`prod`.`cat_id` = `cate`.`cat_id`))
      JOIN `sys_com_marca` `marc`
        ON (`prod`.`mar_id` = `marc`.`mar_id`))
     JOIN `sys_pais` `pais`
       ON (`prod`.`pais_id` = `pais`.`pais_id`))
    JOIN `sys_com_modelo` `modelo`
      ON (`prod`.`mod_id` = `modelo`.`mod_id`))
   JOIN `sys_unidad` `unidad`
     ON (`prod`.`unidad_id` = `unidad`.`unidad_id`))
WHERE `prod`.`cat_id` != 6  AND prod_empre.id_empresa = $idEmpresa AND prod.produ_nombre LIKE '%$ter%' LIMIT 13";
        //echo $sql;
        return $this->conexion->query($sql);
    }

    public function actualizarStock(){
        $sql ="UPDATE sys_producto_empresa
                    SET 
                      cantidad = {$this->getCantidad()}
                    WHERE prod_empre_id = " . $this->getProdEmpreId();
        return$this->conexion->query($sql);
    }
    public function addStock(){
        $sql ="UPDATE sys_producto_empresa
                    SET 
                      cantidad = cantidad + {$this->getCantidad()}
                    WHERE prod_empre_id = " . $this->getProdEmpreId();
        return$this->conexion->query($sql);
    }
}