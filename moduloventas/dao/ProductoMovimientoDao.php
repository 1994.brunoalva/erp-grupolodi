<?php
require "../models/ProductoMovimiento.php";
require_once "../conexion/Conexion.php";


class ProductoMovimientoDao extends ProductoMovimiento
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        $sql="INSERT INTO sys_producto_movimiento VALUES (null,?,?,?,?,?,?);";


             $pro_emp_id=$this->getProEmpId();
             $mov_tipo=$this->getMovTipo();
             $mov_fecha=$this->getMovFecha();
             $mov_cantidad=$this->getMovCantidad();
             $mov_descripcion=$this->getMovDescripcion();
             $estado=$this->getEstado();

        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param("ssssss",$pro_emp_id, $mov_tipo, $mov_fecha, $mov_cantidad, $mov_descripcion, $estado);

        $res = $stmt->execute();
        $stmt->close();
        return $res;
    }

}