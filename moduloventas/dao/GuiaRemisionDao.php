<?php
require "../models/GuiaRemision.php";
require_once "../conexion/Conexion.php";

class GuiaRemisionDao extends GuiaRemision
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }
    public function insertar(){
        $sql = "INSERT INTO sys_ven_guia_remision VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";


             $cliente_id=$this->getClienteId();
             $direccion=$this->getDireccion();
             $fecha = $this->getFecha();
            $serie=$this->getSerie();
        $empresa_id=$this->getEmpresaId();
            $numero=$this->getNumero();
             $motivo=$this->getMotivo();
             $agencia_id=$this->getAgenciaId();
             $tipo_transporte=$this->getTipoTransporte();
             $peso_cargamento=$this->getPesoCargamento();
             $fact_rela=$this->getFactRela();
             $num_placa=$this->getNumPlaca();
             $marca=$this->getMarca();
             $modelo=$this->getModelo();
             $estado=$this->getEstado();

        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param("sssssssssssssss",$empresa_id,$fecha,$serie, $numero,$cliente_id, $direccion, $motivo, $agencia_id, $tipo_transporte,
            $peso_cargamento, $fact_rela, $num_placa, $marca, $modelo, $estado);

        $res = $stmt->execute();
        $this->setGuiReId($stmt->insert_id);
        //echo $stmt->error;
        $stmt->close();
        return $res;
    }

    public function getData(){
        $sql="SELECT 
guia.*,
  emp.emp_logo,
  emp.emp_nombre,
  emp.emp_ruc,
  emp.emp_direccion,
  clie.cli_direc,
  clie.cli_nomape,
  clie.cli_ndoc 
FROM
  sys_ven_guia_remision AS guia 
  INNER JOIN sys_empresas AS emp 
    ON guia.empresa_id = emp.emp_id 
  INNER JOIN sys_ven_clientes AS clie 
    ON guia.cliente_id = clie.cli_id 
WHERE guia.gui_re_id =".$this->getGuiReId();

        return $this->conexion->query($sql);
    }
}