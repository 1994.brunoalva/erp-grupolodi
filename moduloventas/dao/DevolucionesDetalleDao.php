<?php
require "../models/DevolucionesDetalle.php";
require_once "../conexion/Conexion.php";


class DevolucionesDetalleDao extends DevolucionesDetalle
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }
    public function insertar(){
        $sql ="INSERT INTO sys_prod_devo_detalle VALUES (null,?,?,?,?,?);";

         $devo_id= $this->getDevoId();
         $prod_empre_id= $this->getProdEmpreId();
         $cantidad= $this->getCantidad();
         $estado= $this->getEstado();
         $ubica=$this->getUbica();
        // echo "INSERT INTO sys_prod_devo_detalle VALUES (null, $devo_id, $prod_empre_id, $cantidad, $estado);";
        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param("sssss",  $devo_id, $prod_empre_id, $cantidad, $estado,$ubica);

        $res = $stmt->execute();
        //echo $stmt->error;
        $stmt->close();
        return $res;
    }

    public function getdata(){
        $sql ="SELECT 
  prod_devo.*,
  prod.produ_nombre 
FROM
  sys_prod_devo_detalle AS prod_devo 
  INNER JOIN sys_producto_empresa AS prod_emp 
    ON prod_devo.prod_empre_id = prod_emp.prod_empre_id
    INNER JOIN sys_producto AS prod ON prod_emp.id_prod = prod.produ_id
    WHERE prod_devo.devo_id = ".$this->getDevoId();

        return $this->conexion->query($sql);
    }
}