<?php
require "../models/ProductoPromocion.php";
require_once "../conexion/Conexion.php";

class ProductoPromocionDao extends ProductoPromocion
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        $sql="INSERT INTO sys_prod_promocion VALUES (null,
        '{$this->getProdEmpId()}',
        {$this->getProdId()},
        {$this->getEmpId()},
        '0',
        '{$this->getCantidad()}',
        '{$this->getEstado()}');";

        return $this->conexion->query($sql);
    }
    public function verificar(){
        $sql="SELECT * FROM sys_prod_promocion WHERE prod_emp_id = {$this->getProdEmpId()} AND estado='{$this->getEstado()}'";
        return $this->conexion->query($sql);
    }

    public function addStock(){
        $sql="UPDATE sys_prod_promocion
SET 
  cantidad = cantidad + {$this->getCantidad()}
WHERE prod_promo_id = ".$this->getProdPromoId();
        return $this->conexion->query($sql);
    }

}