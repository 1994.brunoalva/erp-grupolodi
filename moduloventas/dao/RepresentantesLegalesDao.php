<?php
require "../models/RepresentantesLegales.php";
require_once "../conexion/Conexion.php";

class RepresentantesLegalesDao extends RepresentantesLegales
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }
    public function insertar(){
        $sql="INSERT INTO sys_ven_clientes_representantes VALUES (NULL,?,?,?,?,?,?);";
        $stmt = $this->conexion->prepare($sql);

        $rep_ruc=$this->getRepRuc();
        $rep_tdoc=$this->getRepTdoc();
        $rep_ndoc=$this->getRepNdoc();
        $rep_nomape=$this->getRepNomape();
        $rep_cargo=$this->getRepCargo();
        $rep_desde=$this->getRepDesde();

        $stmt->bind_param("ssssss", $rep_ruc, $rep_tdoc, $rep_ndoc, $rep_nomape, $rep_cargo, $rep_desde );

        $res = $stmt->execute();
        $stmt->close();
        return $res;
    }
    public function getDatos(){
        $sql="SELECT * FROM sys_ven_clientes_representantes WHERE rep_ruc = '{$this->getRepRuc()}'";
        //echo $sql;
        return $this->conexion->query($sql);
    }
}