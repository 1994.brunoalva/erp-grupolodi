<?php

require "../models/Egresos.php";
require_once "../conexion/Conexion.php";


class EgresosDao extends Egresos
{

    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }
    public function insertar(){
        $sql = "INSERT INTO sys_cob_egreso  VALUES (NULL,?,?,?,?,?,?,?,?,?,?);";

        $stmt = $this->conexion->prepare($sql);

             $egr_descrip=$this->getEgrDescrip();
             $egr_monto=$this->getEgrMonto();
             $sun_id=$this->getSunId();
             $egr_fecha=$this->getEgrFecha();
             $egr_numope=$this->getEgrNumope();
             $egr_tipo=$this->getEgrTipo();
             $egr_estatus=$this->getEgrEstatus();
             $egr_numref=$this->getEgrNumref();
             $egr_tref=$this->getEgrTref();
            $emp_id=$this->getEmpId();

        $stmt->bind_param("ssssssssss",$egr_descrip,
            $egr_monto,
            $sun_id,
            $egr_fecha,
            $egr_numope,
            $egr_tipo,
            $egr_estatus,
            $egr_numref,
            $egr_tref,
            $emp_id);

        $res = $stmt->execute();
        $stmt->close();
        return $res;
    }
    public function getData(){
        $sql="SELECT * FROM sys_cob_egreso WHERE egr_id =".$this->getEgrId();
        return $this->conexion->query($sql);
    }
    public function eliminar(){
        $sql="DELETE FROM sys_cob_egreso WHERE egr_id =".$this->getEgrId();
        return $this->conexion->query($sql);
    }
}