<?php
require "../models/TipoDePrecio.php";
require_once "../conexion/Conexion.php";

class TipoDePrecioDao extends TipoDePrecio
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }
    public function getdata(){
        $sql ="SELECT * FROM sys_cos_tipo_lista";
        return $this->conexion->query($sql);
    }
}