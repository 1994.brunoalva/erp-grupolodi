<?php
require "../models/AjustesProducto.php";
require_once "../conexion/Conexion.php";

class AjustesProductoDao extends AjustesProducto
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }
    function insertar(){
        $sql="INSERT INTO sys_prod_ajustes VALUES (null,?,?,?,?);";
        $stmt = $this->conexion->prepare($sql);

             $emp_id=$this->getEmpId();
             $fecha=$this->getFecha();
             $motivo=$this->getMotivo();
             $cnt_prod=$this->getCntProd();

        $stmt->bind_param("ssss",$emp_id, $fecha, $motivo, $cnt_prod);
        $res = $stmt->execute();

        $this->setAjusId($stmt->insert_id);
        $stmt->close();
        return $res;
    }
    public function getData(){
        $sql ="SELECT
              ajus_id,
              emp_id,
              fecha,
              motivo,
              cnt_prod
            FROM sys_prod_ajustes
            WHERE ajus_id = " . $this->getAjusId();

        return $this->conexion->query($sql);
    }

}

