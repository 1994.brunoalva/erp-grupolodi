<?php
require "../models/NotaElectronica.php";
require_once "../conexion/Conexion.php";

class NotaElectronicaDao extends NotaElectronica
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        $sql = "INSERT INTO sys_cob_nota_credito VALUES (null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

             $not_tipo_doc=$this->getNotTipoDoc();
             $not_sn=$this->getNotSn();
             $notc_tipo=$this->getNotcTipo();
             $not_cod_motivo= $this->getNotCodMotivo();
             $notc_descripcion=$this->getNotcDescripcion();
             $notc_monto=$this->getNotcMonto();
             $notc_disponible=$this->getNotcDisponible();
             $notc_usado=$this->getNotcUsado();
             $sun_id=$this->getSunId();
             $fac_id=$this->getFacId();
             $cli_ndoc=$this->getCliNdoc();
             $notc_fecha=$this->getNotcFecha();
             $notc_estatus=$this->getNotcEstatus();
             $emp_id=$this->getEmpId();
             $asig_id=$this->getAsigId();
        $nota_sustento=$this->getNotaSustento();

             $sql = "INSERT INTO sys_cob_nota_electronica
VALUES (NULL,
        '$not_sn',
        '$not_tipo_doc',
        '$notc_tipo',
        '$notc_descripcion',
        '$not_cod_motivo',
        '$nota_sustento',
        '$notc_monto',
        '$notc_disponible',
        '$notc_usado',
        '$sun_id',
        '$fac_id',
        '$cli_ndoc',
        '$notc_fecha',
        '$notc_estatus',
        '$emp_id',
        '$asig_id');";
        //echo $sql;
       /*$stmt = $this->conexion->prepare($sql);
        $stmt->bind_param("sssssssssssssssss", $not_sn, $not_tipo_doc,$notc_tipo,$notc_descripcion,$not_cod_motivo,$notc_monto,$notc_disponible,$notc_usado,$sun_id,$fac_id,$cli_ndoc,$notc_fecha, $notc_estatus,$emp_id,$asig_id);

        $res = $stmt->execute();
        $this->setFacId($stmt->insert_id);
        //echo $sql;
        $stmt->close();*/
        $res =  $this->conexion->query($sql);
        $this->setNotcId( $this->conexion->insert_id);
        return $res;
    }

    public function getNotasE(){
        $sql ="SELECT 
  noel.*,
  adm_doc.doc_nombre,
  clie.cli_nomape
FROM
  sys_cob_nota_electronica AS noel 
  INNER JOIN sys_adm_documentos AS adm_doc 
  ON adm_doc.doc_id = noel.not_tipo_doc
  INNER JOIN sys_ven_clientes AS clie ON noel.cli_ndoc = clie.cli_ndoc";
        return $this->conexion->query($sql);

    }
    public function getDato(){
        $sql ="SELECT 
  noel.*,
  adm_doc.doc_nombre,
  clie.cli_nomape,
  clie.cli_direc 
FROM
  sys_cob_nota_electronica AS noel 
  INNER JOIN sys_adm_documentos AS adm_doc 
    ON adm_doc.doc_id = noel.not_tipo_doc 
  INNER JOIN sys_ven_clientes AS clie 
    ON noel.cli_ndoc = clie.cli_ndoc 
    WHERE noel.notc_id = " . $this->getNotcId();
        return $this->conexion->query($sql);
    }

}