<?php
require "../models/ClientesSucursales.php";
require_once "../conexion/Conexion.php";

class ClientesSucursalesDao extends ClientesSucursales
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }
    public function insertar(){
        $sql="INSERT INTO sys_ven_clientes_sucursales VALUES (NULL,?,?,?,?,?,?);";
        $stmt = $this->conexion->prepare($sql);

        $su_ruc=$this->getSuRuc();
        $su_codigo=$this->getSuCodigo();
        $su_tipo=$this->getSuTipo();
        $su_direccion=$this->getSuDireccion();
        $id_agencia=$this->getIdAgencia();
        $ubigeo=$this->getUbigeo();

        $stmt->bind_param("ssssss", $su_ruc, $su_codigo, $su_tipo, $su_direccion, $id_agencia,$ubigeo);

        $res = $stmt->execute();
        $stmt->close();
        return $res;
    }
    public function getDatos(){
        $sql="SELECT * FROM sys_ven_clientes_sucursales WHERE su_ruc= '{$this->getSuRuc()}'";
        return $this->conexion->query($sql);
    }
}