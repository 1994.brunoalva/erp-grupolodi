<?php
require "../models/DocumentoVentaNumero.php";
require_once "../conexion/Conexion.php";

class DocumentoVentaNumeroDao extends DocumentoVentaNumero
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function getPrecioProducto(){
        $sql = "SELECT  *  FROM sys_cos_lista_detalle WHERE plis_codprod = '{$this->getProduCodprod()}'";
        return $this->conexion->query($sql);
    }

    public function getData(){
        $sql = "SELECT *
        FROM sys_ven_documento_empresa
        WHERE id_empre = {$this->getIdEmpre()} AND id_doc_emp=".$this->getIdDocEmp();
        return $this->conexion->query($sql);
    }
}