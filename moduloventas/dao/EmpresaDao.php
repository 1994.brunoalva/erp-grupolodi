<?php
require "../models/Empresa.php";
require_once "../conexion/Conexion.php";


class EmpresaDao extends  Empresa
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function getLista(){
        $sql = "SELECT  *  FROM sys_empresas ";
        return $this->conexion->query($sql);
    }
    public function getData(){
        $sql = "SELECT  *  FROM sys_empresas WHERE emp_id = '{$this->getEmpId()}'";
        return $this->conexion->query($sql);
    }
    public function exeSQL($sql){
        return $this->conexion->query($sql);
    }

}