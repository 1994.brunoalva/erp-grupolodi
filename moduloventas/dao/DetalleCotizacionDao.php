<?php
require "../models/DetalleCotizacion.php";
require_once "../conexion/Conexion.php";

class DetalleCotizacionDao extends DetalleCotizacion
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        $sql="INSERT INTO sys_coti_detalle VALUES (null, ?, ?, ?, ?, ?, ?, ?,?, '1');";
        $stmt = $this->conexion->prepare($sql);

        $id_cotizacion=$this->getIdCotizacion();
        $id_producto=$this->getIdProducto();
        $id_empresa=$this->getIdEmpresa();
        $id_prod_empre =$this->getIdProdEmpre();
        $cantidad=$this->getCantidad();
        $descuento=$this->getDescuento();
        $precio_unitario=$this->getPrecioUnitario();
        $producto_desc=$this->getProductoDesc();

        //echo "INSERT INTO sys_coti_detalle VALUES (null, $id_cotizacion, $id_producto, $id_empresa, $cantidad,$descuento, $precio_unitario, $producto_desc, '1');";

        $stmt->bind_param("ssssssss", $id_cotizacion,$id_producto,$id_empresa,$id_prod_empre,$cantidad,$descuento,$precio_unitario,$producto_desc);

        $res = $stmt->execute();
        if (!$res){
           echo  $stmt->error;
        }
        $this->setId($stmt->insert_id);
        $stmt->close();
        return $res;
    }
    public function eliminarByCotiId(){
        $sql ="DELETE
FROM sys_coti_detalle
WHERE id_cotizacion=" . $this->getIdCotizacion();
        //echo $sql;
        return $this->conexion->query($sql);
    }
    public function getListaProducto(){
        $sql="SELECT 
sys_coti_detalle.id_cotizacion,
  sys_coti_detalle.id,
  sys_producto.produ_id,
  sys_producto.produ_codprod,
  sys_producto.produ_desc,
  sys_producto.produ_nombre,
  sys_com_marca.mar_nombre,
  sys_producto.produ_sku,
  sys_pais.pais_nombre,
  sys_empresas.emp_nombre,
  sys_empresas.emp_id,
  sys_coti_detalle.cantidad,
  sys_coti_detalle.id_prod_empre,
  sys_coti_detalle.precio_unitario,
  sys_coti_detalle.descuento,
  sys_unidad.unidad_nombre as 'setProd'
FROM
  sys_producto 
  INNER JOIN sys_produ_detalle 
    ON sys_producto.produ_deta_id = sys_produ_detalle.produ_deta_id 
  INNER JOIN sys_com_marca 
    ON sys_producto.mar_id = sys_com_marca.mar_id 
  INNER JOIN sys_pais 
    ON sys_producto.pais_id = sys_pais.pais_id 
  INNER JOIN sys_coti_detalle 
    ON sys_producto.produ_id = sys_coti_detalle.id_producto 
  INNER JOIN sys_empresas 
    ON sys_coti_detalle.id_empresa = sys_empresas.emp_id 
    INNER JOIN sys_unidad ON sys_producto.unidad_id = sys_unidad.unidad_id
WHERE sys_coti_detalle.id_cotizacion = ".$this->getIdCotizacion();

        return $this->conexion->query($sql);

    }





    public function getdataBuscar($ter){
       // $sql ="SELECT * FROM vista_productos_almacen WHERE produ_nombre  LIKE '%$ter%';";
        $sql ="SELECT 
                  prod_emp.id,
                  prod_emp.cantidad,
                  prod_emp.precio,
                  emp.emp_nombre,
                  prod.produ_nombre 
                FROM
                  sys_producto_empresa AS prod_emp 
                  INNER JOIN sys_empresas AS emp 
                    ON prod_emp.id_empresa = emp.emp_id 
                  INNER JOIN sys_producto AS prod 
                    ON prod_emp.id_producto = prod.produ_id 
                WHERE  prod.produ_nombre LIKE '%$ter%';";
        //echo $sql;
        return $this->conexion->query($sql);
    }
}