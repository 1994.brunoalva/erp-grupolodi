<?php
require "../models/NotaDetalle.php";
require_once "../conexion/Conexion.php";

class NotaDetalleDao extends NotaDetalle
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }
    public function insertar(){

        $sql = "INSERT INTO sys_cob_nota_detalle VALUES (null,?,?,?,?,?);";

        $stmt = $this->conexion->prepare($sql);

         $nota_id= $this->getNotaId();
         $descripcion= $this->getDescripcion();
         $medida= $this->getMedida();
         $catidad= $this->getCatidad();
         $precio= $this->getPrecio();

        $stmt->bind_param("sssss", $nota_id,$descripcion,$medida,$catidad,$precio);

        $res = $stmt->execute();
        echo $stmt->error;
        $stmt->close();
        return $res;
    }

    public function getDatos()
    {
         $sql = "SELECT * FROM sys_cob_nota_detalle WHERE nota_id = ". $this->getNotaId();
         return $this->conexion->query($sql);
    }

}