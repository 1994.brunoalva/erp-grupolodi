<?php
require "../models/ClienteSaldo.php";
require_once "../conexion/Conexion.php";


class ClienteSaldoDao extends ClienteSaldo
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function addSaldo(){

        $sql="UPDATE sys_cliente_saldo
SET 
  clie_saldo = clie_saldo + '{$this->getClieSaldo()}'
WHERE cli_sal_id = " . $this->getCliSalId() ;

        return $this->conexion->query($sql);
    }

    public function insertar(){

        $sql="INSERT INTO sys_cliente_saldo VALUES (null,
        '{$this->getClieteId()}',
        '{$this->getClieSaldo()}',
        'ACTIVO');";
        $res = $this->conexion->query($sql);
        if ($res){
            $this->setCliSalId($this->conexion->insert_id);
        }
        return $res;
    }
    public function getData(){

        $sql="SELECT 
              clie_sal.* 
            FROM
              sys_cliente_saldo AS clie_sal 
              INNER JOIN sys_ven_clientes AS clien 
                ON clie_sal.cliete_id = clien.cli_id 
                WHERE clien.cli_ndoc =  '" . $this->getClieteId()."'";

        return $this->conexion->query($sql);

    }

}