<?php
require "../models/Devoluciones.php";
require_once "../conexion/Conexion.php";

class DevolucionesDao extends Devoluciones
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        $sql ="INSERT INTO sys_prod_devolucion VALUES (null,?,?,?,?,?,?,?);";

         $emp_id=$this->getEmpId();
         $fac_id=$this->getFacId();
         $tipo_doc=$this->getTipoDoc();
         $num_doc=$this->getNumDoc();
         $fecha=$this->getFecha();
         $observaciones=$this->getObservaciones();
         $estado=$this->getEstado();

        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param("sssssss",  $emp_id, $fac_id, $tipo_doc, $num_doc, $fecha, $observaciones, $estado);

        $res = $stmt->execute();
        echo $stmt->error;
        $this->setDevoId($stmt->insert_id);
        $stmt->close();
        return $res;
    }

    public function getdata(){
        $sql ="SELECT
                  *
                FROM sys_prod_devolucion
                WHERE devo_id = ".$this->getDevoId();

        return $this->conexion->query($sql);
    }

}