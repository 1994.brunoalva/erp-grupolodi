<?php
require "../models/Cotizacion.php";
require_once "../conexion/Conexion.php";

class CotizacionDao extends Cotizacion
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        //$sql="INSERT INTO sys_cotizacion VALUES (null, ?, ?, null, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, null, ?, ?);";
        $sql="INSERT INTO sys_cotizacion VALUES (null,?,?,null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $stmt = $this->conexion->prepare($sql);

        $id_cliente=$this->getIdCliente();

  /*$id_cliente=$this->getIdCliente();
  $id_direccion=$this->getIdDireccion();
  $coti_tp_docu=$this->getCotiTpDocu();
  $coti_ruc=$this->getCotiRuc();
  $coti_razon=$this->getCotiRazon();
  $coti_ate=$this->getCotiAte();
  $coti_telf=$this->getCotiTelf();
  $coti_pago=$this->getCotiPago();
  $coti_tp_pago=$this->get;
  $coti_dias_credito=$this->get;
  $coti_tp_cambio=$this->get;
  $coti_tp_moneda=$this->get;
  $coti_obs=$this->get;
  $coti_llega=$this->get;
  $coti_estatus=$this->get;
  $coti_fecha=$this->get;
  $coti_total=$this->get;
  $tipo_costo_prod=$this->get;
  $estado=$this->get;*/
        $id_cliente=$this->getIdCliente();
        $id_direccion=$this->getIdDireccion();
        $coti_tp_docu=$this->getCotiTpCambio();
        $coti_ruc=$this->getCotiRuc();
        $coti_razon=$this->getCotiRazon();
        $coti_ate=$this->getCotiAte();
        $coti_telf=$this->getCotiTelf();
        $coti_pago=$this->getCotiPago();
        $coti_tp_pago=$this->getCotiTpPago();
        $coti_dias_credito=$this->getCotiDiasCredito();
        $coti_tp_cambio=$this->getCotiTpCambio();
        $coti_tp_moneda=$this->getCotiTpMoneda();
        $coti_obs=$this->getCotiObs();
        $coti_llega=$this->getCotiLlega();
        $coti_estatus=$this->getCotiEstatus();
        $coti_fecha=$this->getCotiFecha();
        $coti_total=$this->getCotiTotal();
        $coti_fecha_venta=$this->getCotiFechaVenta();
        $tipo_costo_prod=$this->getTipoCostoProd();
        $estado_coti=$this->getEstado();

        $stmt->bind_param("ssssssssssssssssssss",   $id_cliente,$id_direccion,$coti_tp_docu, $coti_ruc, $coti_razon, $coti_ate, $coti_telf,
            $coti_pago, $coti_tp_pago, $coti_dias_credito, $coti_tp_cambio, $coti_tp_moneda, $coti_obs, $coti_llega, $coti_estatus,
            $coti_fecha, $coti_total, $coti_fecha_venta, $tipo_costo_prod, $estado_coti);

        $res = $stmt->execute();

        $this->setCotiId($stmt->insert_id);
        $stmt->close();
        return $res;
    }
    public function actualizar(){
        $sql="UPDATE sys_cotizacion
                SET 
                  id_cliente = ?,
                  id_direccion = ?,
                  coti_tp_docu = ?,
                  coti_ruc = ?,
                  coti_razon = ?,
                  coti_ate = ?,
                  coti_telf = ?,
                  coti_pago = ?,
                  coti_tp_pago = ?,
                  coti_dias_credito = ?,
                  coti_tp_cambio = ?,
                  coti_tp_moneda = ?,
                  coti_obs = ?,
                  coti_llega = ?,
                  coti_estatus = ?,
                  coti_fecha = ?,
                  coti_total = ?,
                  coti_fecha_venta = ?,
                  tipo_costo_prod = ?,
                  estado = ?
                  
                WHERE coti_id = ?;";
        $stmt = $this->conexion->prepare($sql);

        $id_cliente=$this->getIdCliente();
        $id_direccion=$this->getIdDireccion();
        $coti_tp_docu=$this->getCotiTpCambio();
        $coti_ruc=$this->getCotiRuc();
        $coti_razon=$this->getCotiRazon();
        $coti_ate=$this->getCotiAte();
        $coti_telf=$this->getCotiTelf();
        $coti_pago=$this->getCotiPago();
        $coti_tp_pago=$this->getCotiTpPago();
        $coti_dias_credito=$this->getCotiDiasCredito();
        $coti_tp_cambio=$this->getCotiTpCambio();
        $coti_tp_moneda=$this->getCotiTpMoneda();
        $coti_obs=$this->getCotiObs();
        $coti_llega=$this->getCotiLlega();
        $coti_estatus=$this->getCotiEstatus();
        $coti_fecha=$this->getCotiFecha();
        $coti_total=$this->getCotiTotal();
        $coti_fecha_venta=$this->getCotiFechaVenta();
        $coti_id = $this->getCotiId();
        $tipo_costo_prod=$this->getTipoCostoProd();
        $estado_coti=$this->getEstado();

        $stmt->bind_param("sssssssssssssssssssss", $id_cliente,  $id_direccion,  $coti_tp_docu,  $coti_ruc,  $coti_razon,  $coti_ate,  $coti_telf,  $coti_pago,  $coti_tp_pago,  $coti_dias_credito,  $coti_tp_cambio,  $coti_tp_moneda,  $coti_obs,  $coti_llega,  $coti_estatus,  $coti_fecha,  $coti_total,  $coti_fecha_venta,$tipo_costo_prod,$estado_coti,$coti_id);

        $res = $stmt->execute();
        $stmt->close();
        //echo $stmt->error;
        //echo $this->conexion->error;
        return $res;
    }



    public function getData(){
        $sql ="SELECT *
                FROM sys_cotizacion
                WHERE coti_id = ". $this->getCotiId();
        //echo $sql;
        return $this->conexion->query($sql);
    }


    public function getdataBuscar($ter){
       // $sql ="SELECT * FROM vista_productos_almacen WHERE produ_nombre  LIKE '%$ter%';";
        $sql ="SELECT 
                  prod_emp.id,
                  prod_emp.cantidad,
                  prod_emp.precio,
                  emp.emp_nombre,
                  prod.produ_nombre 
                FROM
                  sys_producto_empresa AS prod_emp 
                  INNER JOIN sys_empresas AS emp 
                    ON prod_emp.id_empresa = emp.emp_id 
                  INNER JOIN sys_producto AS prod 
                    ON prod_emp.id_producto = prod.produ_id 
                WHERE  prod.produ_nombre LIKE '%$ter%';";
        //echo $sql;
        return $this->conexion->query($sql);
    }
}