<?php
require "../models/GuiaRemisionDetalle.php";
require_once "../conexion/Conexion.php";

class GuiaRemisionDetalleDao extends GuiaRemisionDetalle
{
    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }
    public function insertar(){
        $sql = "INSERT INTO sys_ven_guia_detalle VALUES (NULL,?,?,?,?);";

        $stmt = $this->conexion->prepare($sql);

         $gui_re_id=$this->getGuiReId();
         $produ_id=$this->getProduId();
         $unidad=$this->getUnidad();
         $cantidad=$this->getCantidad();

        $stmt->bind_param("ssss", $gui_re_id, $produ_id,$unidad,$cantidad);

        $res = $stmt->execute();
        //echo $stmt->error;
        $stmt->close();
        return $res;
    }
    public function getDatos(){
        $sql="SELECT 
  guia_deta.*,
  prod.produ_nombre,
  prod.produ_sku
FROM
  sys_ven_guia_detalle AS guia_deta 
  INNER JOIN sys_producto_empresa AS emp_prod 
    ON guia_deta.produ_id = emp_prod.prod_empre_id 
  INNER JOIN sys_producto AS prod 
    ON emp_prod.id_prod = prod.produ_id 
    WHERE guia_deta.gui_re_id = ".$this->getGuiReId();

        return $this->conexion->query($sql);
    }
}