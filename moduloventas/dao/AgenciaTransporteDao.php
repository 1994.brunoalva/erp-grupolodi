<?php
require "../models/AgenciaTransporte.php";
require_once "../conexion/Conexion.php";

class AgenciaTransporteDao extends AgenciaTransporte
{
    private $conexion;
    private $dataRetor;

    /**
     * @return mixed
     */
    public function getDataRetor()
    {
        return $this->dataRetor;
    }

    /**
     * @param mixed $dataRetor
     */
    public function setDataRetor($dataRetor)
    {
        $this->dataRetor = $dataRetor;
    }


    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        $sql = "INSERT INTO sys_agencia_transporte VALUES (NULL,?,?,?,?,?,?);";

        $ruc=$this->getRuc();
        $razon_social=$this->getRazonSocial();
        $direccion=$this->getDireccion();
        $telefono=$this->getTelefono();
        $condicion=$this->getCondicion();
        $estado=$this->getEstado();

        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param("ssssss",$ruc,$razon_social, $direccion, $telefono, $condicion, $estado);

        $res = $stmt->execute();
        $this->setId($stmt->insert_id);
        //echo $stmt->error;
        $stmt->close();
        return $res;
    }
    public function actualizar(){
        $sql = "UPDATE sys_agencia_transporte
                SET 
                  ruc = ?,
                  razon_social = ?,
                  direccion = ?,
                  telefono = ?,
                  condicion = ?,
                  estado = ?
                WHERE id = ?;";

        $ruc=$this->getRuc();
        $razon_social=$this->getRazonSocial();
        $direccion=$this->getDireccion();
        $telefono=$this->getTelefono();
        $condicion=$this->getCondicion();
        $estado=$this->getEstado();
        $id=$this->getId();

        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param("sssssss",$ruc,$razon_social, $direccion, $telefono, $condicion, $estado,$id);

        $res = $stmt->execute();
        //echo $stmt->error;
        $stmt->close();
        return $res;
    }

    public function exeSql($sql){
        $res = $this->conexion->query($sql);
        $this->setDataRetor($this->conexion->insert_id);
        return $res;
    }
    public function buscar($ter){
        $sql="SELECT * FROM sys_agencia_transporte WHERE ruc LIKE '%$ter%' OR razon_social LIKE '%$ter%'";
        return $this->conexion->query($sql);
    }

    public function getDato(){
        $sql="SELECT * FROM sys_agencia_transporte WHERE id =". $this->getId();

        return $this->conexion->query($sql);
    }

}