<?php
$indexRuta=0;

$nombremodule = "Ventas";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="./public/css/lodi-css.css" rel="stylesheet">
    <link href="./assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="./assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="./assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="./assets/datatables.css" rel="stylesheet">
    <link href="./assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">
    <link href="./public/plugins/sweetalert2/sweetalert2.min.css">


    <script src="./assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="./assets/datatables.js"></script>
    <script src="./assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="./assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <style>

        .contenedor-boder-r{
            border: 1px solid #d4cfcf;
            border-radius: 10px;
            -webkit-box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
            -moz-box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
            box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
        }
        .onliborder{
            border: 1px solid #d4cfcf;
            border-radius: 10px;
        }

        .contenedor-cal{
            clear: both;
            overflow: hidden;
        }
        .cont-ds-cal{

        }
        .headcontenedor-cal{
            background-color: #0866c6;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            padding-bottom: 5px;
            padding-top: 5px;
        }

        .cal-cont-se{
            border-bottom: 1px  solid rgba(6, 33, 73, 0.94);
            clear: both;
            overflow: hidden;
            background-color: #0866c61f;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .box-d-cal{
            width: 14.284%;
            text-align: center;

            display: flex ;
            position: relative ;
            float: left ;
            min-height: 1px ;
            height: 16.5%;

        }
        .box-day-cal:hover{
            border: 1px  solid rgb(194, 194, 194);
            cursor: pointer;
            color: white;
            background-color: rgba(8, 102, 198, 0.53);
        }
        .elemt-box-cal{
            display: block;
            margin: auto;
            font-size: 14px;
        }
        .send-left{
            float: left;
        }
        .send-right{
            float: right;
        }

        .box-unit{


            clear: both;
            overflow: hidden;

            height: 120px;
        }
        .box-footer-unic{

            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
            text-align: center;
            padding: 5px;
        }
        .headbox{
            clear: both;
            overflow: hidden;
            padding: 15px;
            margin-bottom: 13px;
        }
        .boxclice:hover{
            cursor: pointer;
            -webkit-box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
            -moz-box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
            box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    include 'componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include 'componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Ventas</strong>
                                        </h2>
                                    </div>
                                    <!--BOTONES-->
                                    <!--<div class="col-lg-6 text-right">
                                        <a href="new-folder.php" id="folder_btn_nuevo_folder" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Folder
                                        </a>

                                    </div>-->

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id=""  class="col-xs-12 col-sm-12 col-md-12 no-padding">

                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-6" >
                                            <a href="cotizaciones/cotizaciones.php">
                                                <div class="onliborder box-unit boxclice" style="background-color: #0073b6;">
                                                    <div class="headbox" style="">
                                                        <p style="color: white; font-size: 25px;" class="send-left">Cotizaciones </p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-asterisk fa-lg"></i>

                                                    </div>
                                                    <div style="background-color: #0066a4;" class="box-footer-unic" >
                                                        <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                    </div>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-lg-2 col-md-4 col-sm-6" >
                                            <a href="ventas/index.php">
                                                <div class="onliborder box-unit boxclice" style="background-color: #dd4c39;">
                                                    <div class="headbox" style="">
                                                        <p style="color: white; font-size: 25px;" class="send-left">Ventas </p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-asterisk fa-lg"></i>

                                                    </div>
                                                    <div style="background-color: #c64233;" class="box-footer-unic" >
                                                        <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                    </div>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-lg-2 col-md-4 col-sm-6" >
                                            <a href="productos/">
                                                <div class="onliborder box-unit boxclice" style="background-color: #00a65a;">
                                                    <div class="headbox" style="">
                                                        <p style="color: white; font-size: 25px;" class="send-left">Productos </p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-asterisk fa-lg"></i>

                                                    </div>
                                                    <div style="background-color: #009551;" class="box-footer-unic" >
                                                        <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                    </div>
                                                </div>
                                            </a>

                                        </div>

                                        <div class="col-lg-2 col-md-4 col-sm-6" >
                                            <a href="clientes/clientes.php">
                                                <div class="onliborder box-unit boxclice" style="background-color: #8683bc;">
                                                    <div class="headbox" style="">
                                                        <p style="color: white; font-size: 25px;" class="send-left">Clientes </p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-asterisk fa-lg"></i>

                                                    </div>
                                                    <div style="background-color: #5d5b9a;" class="box-footer-unic" >
                                                        <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                    </div>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-lg-2 col-md-4 col-sm-6" >
                                            <a href="productos/index.php">
                                                <div class="onliborder box-unit boxclice" style="background-color: #f39c11;">
                                                    <div class="headbox" style="">
                                                        <p style="color: white; font-size: 25px;" class="send-left">Inventario </p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-asterisk fa-lg"></i>

                                                    </div>
                                                    <div style="background-color: #da8c10;" class="box-footer-unic" >
                                                        <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                    </div>
                                                </div>
                                            </a>

                                        </div>

                                        <div class="col-lg-2 col-md-4 col-sm-6" >
                                            <a href="cajaChica/flujoCaja.php">
                                                <div class="onliborder box-unit boxclice" style="background-color: #00c0ef;">
                                                    <div class="headbox" style="">
                                                        <p style="color: white; font-size: 25px;" class="send-left">Caja Chica </p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-asterisk fa-lg"></i>

                                                    </div>
                                                    <div style="background-color: #00abd7;" class="box-footer-unic" >
                                                        <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                    </div>
                                                </div>
                                            </a>

                                        </div>

                                    </div>
                                </div>
                                <div style="" class="col-md-12">
                                    <div class="col-md-12" style="text-align: center;">
                                        <h2 style="color: #0866C6;margin-top: 30px;font-weight: bold">Venta del <?php echo date('Y')?></h2>
                                    </div>
                                    <canvas style="height: 500px; width: 100%;" id="char-data-general"></canvas>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_tc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

                <div class="modal-content">
                    <div class="modal-header" style="background-color: #0866c6">
                        <h3 style="color: white" class="modal-title" id="exampleModalLabel">Tasa de cambio Comercial</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tasa de Cambio para Ventas</label>
                            <input autocomplete="off" type="text" class="form-control money-temp" id="tasaCambio" aria-describedby="" placeholder=" ">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button onclick="registrarTasa()" type="button" class="btn btn-primary">Registar</button>
                    </div>
                </div>


        </div>
    </div>
    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="public/js/Input_validate.js"></script>
    <script src="public/plugins/sweetalert2/vue-swal.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="assets/Toast/build/jquery.toast.min.js"></script>

    <script type="text/javascript">


        function  registrarTasa(){
            const  tasa = $("#tasaCambio").val();
            if (tasa.length>0){
                const dataR = {tipo:'reg',ts:tasa,fecha:getFormatYear()};
                console.log(dataR);
                $.toast({
                    heading: 'INFORMACION',
                    text: 'Guardando Tasa de cambio',
                    icon: 'info',
                    position: 'top-right',
                    hideAfter: '2500',
                });
                $.ajax({
                    type: "POST",
                    url: './ajaxs/ajs_tasa_cambio.php',
                    data: dataR,
                    success: function (resp) {
                        console.log(resp)
                        resp=JSON.parse(resp);
                        if (resp.res){
                            swal("Registrado","","success")
                                .then(function () {
                                    $('#modal_tc').modal('hide');
                                });

                        }
                    }
                });

            }else{
                $("#tasaCambio").focus();
            }
        }

        var ctx_general = document.getElementById('char-data-general').getContext('2d');
        var char_general = new Chart(ctx_general, {
            type: 'line',
            data: {
                labels: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'],
                datasets: [
                ]
            },
            options: {

            }
        });
        var dataTEmp ;
        const  Colores = ['rgb(227,222,80)',
            'rgb(228,90,95)',
            'rgb(73,224,81)',
            'rgb(81,215,221)',
            'rgb(68,159,226)',
            'rgb(223,4,135)',
            'rgb(58,55,228)',
            'rgb(182,69,228)',
            'rgb(225,0,191)',
        ];
        const  ColoresFondo = ['rgb(173,169,156)',
            'rgb(173,147,150)',
            'rgb(132,173,140)',
            'rgb(147,166,173)',
            'rgb(120,131,173)',
            'rgb(173,130,152)',
            'rgb(128,122,173)',
            'rgb(154,124,173)',
            'rgb(173,116,168)',
        ];
       function getDataGrafico(){
           var today = new Date();
           $.ajax({
               type: "GET",
               url: "ajaxs/ajs_graficos.php?anio="+today.getFullYear(),
               success: function (data) {
                   console.log(data)
                   data= JSON.parse(data);
                   var dataSET =[];

                   for (var i=0; i<data.length;i++){
                       char_general.data.datasets.push({
                           pointRadius: 0,
                           spanGaps: false,
                           label:data[i].empresa ,
                           data: data[i].ventas,
                           backgroundColor: [
                               'rgba(0, 0, 0, 0.0)'
                           ],
                           borderColor: [
                               Colores[i]
                           ],
                           borderWidth: 2
                       })

                   }

                   char_general.update();
                   //char_general.data.datasets=dataSET
               }
           });
       }

        function getFormatYear(){
           const fecha = new Date();
           const dia = (fecha.getDate()+"").length==1?"0"+fecha.getDate():fecha.getDate();
           const mes = ((fecha.getMonth()+1)+"").length==1?"0"+(fecha.getMonth()+1):(fecha.getMonth()+1);
           const anio = fecha.getFullYear();
           return anio+"-"+mes+"-"+dia;
        }

       function verificarTasa(){
           $.ajax({
               type: "POST",
               url: './ajaxs/ajs_tasa_cambio.php',
               data: {tipo:'vr',fecha:getFormatYear()},
               success: function (resp) {
                   resp =JSON.parse(resp);
                   if (!resp.res){
                       $('#modal_tc').modal('show');
                   }
                   console.log(resp)
               }
           });

       }

        var conuntChart=0;
        $(document).ready(function () {
            $('.money-temp').keypress(function (e) {
                var regex = new RegExp("^[0-9]+$");
                var letra = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                var moneyText = $(this).val();
                if ((moneyText.length) === 0) {
                    if (letra === '.' || letra === '0') {
                        e.preventDefault();
                        return false;
                    }
                } else {
                    var tienePunto = moneyText.toString().includes('.');
                    if (tienePunto === false && letra === '.') {
                        $(this).val(moneyText + letra);
                    }
                }
                if (conuntChart>=2) {
                    if(conuntChart===3){
                        $(this).val(moneyText.substring(0,moneyText.length-1));
                    }
                    e.preventDefault();
                    return false;
                }
                if (!regex.test(letra)) {
                    e.preventDefault();
                    return false;
                }
            });

            setTimeout(function () {
                getDataGrafico()
            },100)

            verificarTasa()

            $('.numeros').keyup(function ($event) {
                let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
                if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                    $event.preventDefault();
                }
            });
        });

    </script>


</body>

</html>
