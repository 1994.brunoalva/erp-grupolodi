<?php
require "../dao/CotizacionDao.php";
require "../dao/ClienteDao.php";
require "../dao/DetalleCotizacionDao.php";
require "../dao/EmpresaDao.php";
require "../dao/TasaCambioDao.php";
require  "../utils/Tools.php";

require_once('../../lib/mpdf/vendor/autoload.php');


$idCoti = $_GET['coti'];


$cotizacionDao = new CotizacionDao();
$detalleCotizacionDao = new DetalleCotizacionDao();
$clienteDao= new ClienteDao();
$empresaDao = new EmpresaDao();
$tasaCambioDao= new TasaCambioDao();
$tools = new Tools();

$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);




$cotizacionDao->setCotiId($idCoti);
$detalleCotizacionDao->setIdCotizacion($idCoti);

$listaProduc=$detalleCotizacionDao->getListaProducto();

$resultC = $cotizacionDao->getData()->fetch_array();

$clienteDao->setCliId($resultC['id_cliente']);
$resultClie =$clienteDao->getdata()->fetch_array();

$numCotiCeros  = $tools->numeroParaDocumento($idCoti, 6);

//$empresaDao->setEmpId($resultC['emp_id']);
//$tasaCambioDao->setTasId($resultC['tas_id']);

//$restEmpre = $empresaDao->getData()->fetch_assoc();

//$resDataTasaCambio = $tasaCambioDao->getDatos()->fetch_assoc();



$dataDocumento = strlen($resultC['coti_ruc'])>8?"RUC":"DNI";

$stylesheet = file_get_contents('../public/css/stylepdf.css');

$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);

$tempoProdArr =[];

foreach ($listaProduc as $pr){
    if(count($tempoProdArr)>0){
        $vali=true;
        for($in=0;$in<count($tempoProdArr);$in++){
            if (count($tempoProdArr[$in])>0){
                if ($tempoProdArr[$in][0]['emp_id']==$pr['emp_id']){
                    $tempoProdArr[$in][]=$pr;
                    $vali=false;
                }
            }

        }
        if ($vali){
            $tempoProdArr[]=array($pr);
        }
    }else{
        $tempoProdArr[]=array($pr);
    }
}

//echo json_encode($tempoProdArr);



$rowHTML="";
$subToal=0;
$tempARRRRRRRRRRRRRRR=[];
$contadorPRima=1;
foreach ($listaProduc as $prod){
    $tempARRRRRRRRRRRRRRR[]=$prod;
    $importe = $prod['precio_unitario']*$prod['cantidad'];
    $subToal= $subToal+ $importe;
    $importe = number_format($importe, 2, '.', ',');
    $rowHTML=$rowHTML. "<tr>
    <td class='borde-p' style='text-align: center; font-size: 11px;'>$contadorPRima</td>
    <td class='borde-p' style=' font-size: 11px;'>{$prod['produ_nombre']}</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>{$prod['setProd']}</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>{$prod['cantidad']}</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>{$prod['precio_unitario']}</td>
    <td class='borde-p' style=' font-size: 11px; text-align: center'>$importe</td>
  </tr>";
    $contadorPRima++;
}
//echo  json_encode($tempARRRRRRRRRRRRRRR);
$total =number_format($subToal, 2, '.', ',');
$subToal = $subToal/1.18;
$igv = $subToal *0.18;
//$total = $subToal+ $igv;
$subToal= number_format($subToal, 2, '.', ',');
$igv =number_format($igv, 2, '.', ',');








/*$mpdf->SetDefaultBodyCSS("background","url('../public/img/cotilodi.png");
$mpdf->SetDefaultBodyCSS('background-image-resize', 6);*/
$mpdf->WriteFixedPosHTML("<img style='width: 100%' src='../public/img/cotilodi.png'>",0,0,230,130);
$mpdf->WriteFixedPosHTML("<span style='color: white;  font-size: 30px'>RESUMEN GENERAL DE LA COTIZACION</span>",8,8,210,130);
$mpdf->WriteFixedPosHTML("<span style='color: white;  font-size: 20px'><strong>Cotización:</strong> #$numCotiCeros</span>",145,20,210,130);
//$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>Direccion:</strong> JR. EDGAR ZUÃ±IGA 165 - SAN LUIS - LIMA</span>",8,22,210,130);
//$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>RUC: 20333336929</strong></span>",30,32,210,130);
//$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>Telefono:</strong> (+51) 000000000   <strong>Fax:</strong> (+51) 000000000</span>",124,22,210,130);
//$mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>FECHA DE EMISION: {$resultC['coti_fecha']}</strong></span>",124,32,210,130);

$html= "<div style='width: 1000%;padding-top: 90px; overflow: hidden;clear: both;background-color: rgb(255,255,255)'>
<div style='width: 50%; float: left;'>

<table style='width:100%'>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>$dataDocumento:</strong></td>
    <td style=' font-size: 13px;'>{$resultC['coti_ruc']}</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>CLIENTE:</strong></td>
    <td style=' font-size: 13px;'>{$resultC['coti_razon']}</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>DIRECCION:</strong></td>
    <td style=' font-size: 13px;'>{$resultClie['cli_direc']}</td>
  </tr>
</table>
</div>
<div style='width: 50%; float: left'>
<table style='width:100%'>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>TIPO DE PAGO:</strong></td>
    <td style=' font-size: 13px;'>CONTADO</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>VENDEDOR:</strong></td>
    <td style=' font-size: 13px;'>BRUNO</td>
  </tr>
</table>
</div>
</div>

<div style='width: 100%; padding-top: 20px;'>
<table style='width:100%'>
  <tr style='border-bottom: 1px solid #0071C1'>
    <td style=' font-size: 13px;text-align: left; color: #326582;border-bottom: 1px solid #0071C1'><strong>ITEM</strong></td>
    <td style=' font-size: 13px;text-align: left; color: #326582;border-bottom: 1px solid #0071C1'><strong>PRODUCTOS</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>SET</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>CANT.</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>PRECIO U.</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>IMPORTE</strong></td>
    
  </tr>
  $rowHTML



<tr style=''>
<td colspan='2'></td>
 <td colspan='3' style=' font-size: 12px; text-align: right; font-weight: bold;background-color: #d3d3d3'>SUB TOTAL:</td>
 <td style=' font-size: 12px; text-align: center; font-weight: bold;background-color: #d3d3d3'>$subToal</td>
</tr>
<tr>
 <td colspan='5' style=' font-size: 11px; text-align: right;'>IGV:</td>
 <td style=' font-size: 12px; text-align: center; '>18.00 %</td>
</tr>
<tr>
 <td colspan='5' style=' font-size: 11px; text-align: right; '>TOTAL IMPUESTOS:</td>
 <td style=' font-size: 12px; text-align: center; '>$igv</td>
</tr>
<tr>
<td colspan='3'></td>
 <td class='border-top' colspan='2' style=' font-size: 12px; text-align: right; font-weight: bold;background-color: #d3d3d3'>TOTAL</td>
 <td class='border-top' style=' font-size: 12px; text-align: center; font-weight: bold;background-color: #d3d3d3'>$total</td>
</tr>

</table>
</div>

";
$mpdf->SetHTMLFooter("
<div style='width: 100%; text-align: center;color: #006ec2; padding-bottom: 10px;'>¡Gracias por hacer negocios!</div>
<div style='width: 100%; '>
<div style='float: left; width: 70%'>
    <div style='font-size: 12px;width: 100%;'> <strong>OTROS COMENTARIOS</strong></div>
    <div style='font-size: 12px;width: 100%'>1. el pago total a pagar en 30 dias.</div>
    <div style='font-size: 12px;width: 100%'>2. Por favor, incluya el numero de factura en su cheque.</div>
    <div style='font-size: 12px;width: 100%'>3. Por favor, envie su cheque a la direccion indicada mas arriba.</div>
    
</div>
<div style='float: left; width: 30%'>
<div style='font-size: 12px;width: 100%'></div>
</div>

</div>
");
//echo "<style>$stylesheet</style>".$html;

$mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

if(count($tempoProdArr)>1){

    foreach ($tempoProdArr as $cotR){
        $mpdf->AddPage();
        $idEmpresaByCot=0;
        $rowHTML="";
        $subToal=0;
        $contadorTempEmp =1;
        foreach ($cotR as $rowProC){
            $idEmpresaByCot=$rowProC['emp_id'];

            $importe = $rowProC['precio_unitario']*$rowProC['cantidad'];
            $subToal= $subToal+ $importe;
            $importe = number_format($importe, 2, '.', ',');
            $rowHTML=$rowHTML. "<tr>
                <td class='borde-p' style=' font-size: 11px;'>$contadorTempEmp</td>
                <td class='borde-p' style=' font-size: 11px;'>{$rowProC['produ_nombre']}</td>
                <td class='borde-p' style=' font-size: 11px; text-align: center'>{$rowProC['setProd']}</td>
                <td class='borde-p' style=' font-size: 11px; text-align: center'>{$rowProC['cantidad']}</td>
                <td class='borde-p' style=' font-size: 11px; text-align: center'>{$rowProC['precio_unitario']}</td>
                <td class='borde-p' style=' font-size: 11px; text-align: center'>$importe</td>
              </tr>";

        }


        $total =number_format($subToal, 2, '.', ',');
        $subToal=$subToal/1.18;
        $igv = $subToal *0.18;
        //$total = $subToal+ $igv;
        $subToal= number_format($subToal, 2, '.', ',');
        $igv =number_format($igv, 2, '.', ',');



        $empresaDao->setEmpId($idEmpresaByCot);

        $restEmpre = $empresaDao->getData()->fetch_assoc();
        $resBanc = $empresaDao->exeSQL("SELECT 
                                              banco.ban_id,
                                              banco.ban_nombre 
                                            FROM
                                              sys_empresas_bancos AS cuentas 
                                              INNER JOIN sys_adm_bancos AS banco 
                                                ON cuentas.ban_id = banco.ban_id 
                                            WHERE cuentas.emp_id = $idEmpresaByCot GROUP BY banco.ban_id");

        /*$mpdf->SetDefaultBodyCSS("background","url('../public/img/cotilodi.png");
        $mpdf->SetDefaultBodyCSS('background-image-resize', 6);*/
        $mpdf->WriteFixedPosHTML("<img style='width: 100%' src='../public/img/headpdf/{$restEmpre['emp_logo']}'>",0,0,230,130);
        $mpdf->WriteFixedPosHTML("<span style='color: white;  font-size: 30px'>{$restEmpre['emp_nombre']}</span>",8,8,210,130);
        $mpdf->WriteFixedPosHTML("<span style='color: white;  font-size: 20px'><strong>Cotización:</strong> #$numCotiCeros</span>",145,9,210,130);
        $mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>Direccion:</strong> {$restEmpre['emp_direccion']}</span>",8,22,210,130);
        $mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>RUC: {$restEmpre['emp_ruc']}</strong></span>",30,32,210,130);
        $mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>Telefono:</strong> {$restEmpre['emp_telefono']}   <strong>Fax:</strong> (+51) 000000000</span>",124,22,210,130);
        $mpdf->WriteFixedPosHTML("<span style='color: white; font-size: 13px'><strong>FECHA DE EMISION: {$resultC['coti_fecha']}</strong></span>",124,32,210,130);

        $html= "<div style='width: 1000%;padding-top: 90px; overflow: hidden;clear: both;background-color: rgb(255,255,255)'>
<div style='width: 50%; float: left;'>

<table style='width:100%'>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>$dataDocumento:</strong></td>
    <td style=' font-size: 13px;'>{$resultC['coti_ruc']}</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>CLIENTE:</strong></td>
    <td style=' font-size: 13px;'>{$resultC['coti_razon']}</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>DIRECCION:</strong></td>
    <td style=' font-size: 13px;'>{$resultClie['cli_direc']}</td>
  </tr>
</table>
</div>
<div style='width: 50%; float: left'>
<table style='width:100%'>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>TIPO DE PAGO:</strong></td>
    <td style=' font-size: 13px;'>CONTADO</td>
  </tr>
  <tr>
    <td style=' font-size: 13px;text-align: right'><strong>VENDEDOR:</strong></td>
    <td style=' font-size: 13px;'>BRUNO</td>
  </tr>
</table>
</div>
</div>

<div style='width: 100%; padding-top: 20px;'>
<table style='width:100%'>
  <tr style='border-bottom: 1px solid #0071C1'>
    <td style=' font-size: 13px;text-align: left; color: #326582;border-bottom: 1px solid #0071C1'><strong>ITEM</strong></td>
    <td style=' font-size: 13px;text-align: left; color: #326582;border-bottom: 1px solid #0071C1'><strong>PRODUCTOS</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>SET</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>CANT.</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>PRECIO U.</strong></td>
    <td style=' font-size: 13px;text-align: center; color: #326582;border-bottom: 1px solid #0071C1'><strong>IMPORTE</strong></td>
    
  </tr>
  $rowHTML



<tr style=''>
<td colspan='3'></td>
 <td colspan='2' style=' font-size: 12px; text-align: right; font-weight: bold;background-color: #d3d3d3'>SUB TOTAL:</td>
 <td style=' font-size: 12px; text-align: center; font-weight: bold;background-color: #d3d3d3'>$subToal</td>
</tr>
<tr>
 <td colspan='5' style=' font-size: 11px; text-align: right;'>IGV:</td>
 <td style=' font-size: 12px; text-align: center; '>18.00 %</td>
</tr>
<tr>
 <td colspan='5' style=' font-size: 11px; text-align: right; '>TOTAL IMPUESTOS:</td>
 <td style=' font-size: 12px; text-align: center; '>$igv</td>
</tr>
<tr>
<td colspan='3'></td>
 <td class='border-top' colspan='2' style=' font-size: 12px; text-align: right; font-weight: bold;background-color: #d3d3d3'>TOTAL</td>
 <td class='border-top' style=' font-size: 12px; text-align: center; font-weight: bold;background-color: #d3d3d3'>$total</td>
</tr>

</table>
</div>

";
        $htmlBanc="";
        foreach ($resBanc as $rowBanc){
            $htmlBanc = $htmlBanc."<div style='font-size: 10px;width: 100%'>{$rowBanc['ban_nombre']}</div>";
            $respNroCuen = $empresaDao->exeSQL("SELECT 
  cuentas.emp_cuenta,
  cuentas.emp_moneda
FROM
  sys_empresas_bancos AS cuentas 
  INNER JOIN sys_adm_bancos AS banco 
    ON cuentas.ban_id = banco.ban_id 
WHERE cuentas.emp_id =$idEmpresaByCot AND cuentas.ban_id =".$rowBanc['ban_id']);
            foreach ($respNroCuen as $rowCuen){
                $temMon= $rowCuen['emp_moneda']==2?'DOLARES':'SOLES';
                $htmlBanc = $htmlBanc."<div style='font-size: 10px;width: 100%;padding-left: 5px;'>$temMon: {$rowCuen['emp_cuenta']}</div>";
            }


        }

        $mpdf->SetHTMLFooter("
            <div style='width: 100%; text-align: center;color: #006ec2; padding-bottom: 10px;'>¡Gracias por hacer negocios!</div>
            <div style='width: 100%; '>
            <div style='float: left; width: 60%'>
                <div style='font-size: 12px;width: 100%;'> <strong>OTROS COMENTARIOS</strong></div>
                <div style='font-size: 12px;width: 100%'>1. el pago total a pagar en 30 dias.</div>
                <div style='font-size: 12px;width: 100%'>2. Por favor, incluya el numero de factura en su cheque.</div>
                <div style='font-size: 12px;width: 100%'>3. Por favor, envie su cheque a la direccion indicada mas arriba.</div>
                
            </div>
            <div style='float: left; width: 40%'>
            <div style='font-size: 12px;width: 100%;'> <strong>Cuentas Bancarias:</strong></div>
            $htmlBanc
            </div>
            
            </div>
            ");

        $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    }
}

$mpdf->Output();







