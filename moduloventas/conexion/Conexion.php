<?php

define("HOST_SS","localhost");
define("DATABASE_SS","db_lodi2");
define("USER_SS","root");
define("PASSWORD_SS","");

class Conexion
{
    private $servername = HOST_SS;
    private $username = USER_SS;
    private $password = PASSWORD_SS;
    private $bd=DATABASE_SS;
    private $conn;


   /* private $servername = "lightning.servidoresph.com";
    private $username = "serverca_bd_erp_lodi";
    private $password = "b*oEtXQ[(SN+";
    private $bd = "serverca_erp_lodi";
    private $conn;*/


    function getConexion()
    {
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->bd);
        $this->conn->set_charset("utf8");
        return $this->conn;
    }

    function closeConexion()
    {
        $this->conn->close();
    }
}

?>

