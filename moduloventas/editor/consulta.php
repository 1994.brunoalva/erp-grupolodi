<?php
require "../conexion/Conexion.php";
$conexion = (new Conexion())->getConexion();

$query=$_POST['query'];
$tipo=$_POST['tipo'];

$rsult = $conexion->query($query);

$respuesta = array("res"=>false);

if ($tipo==1){
    $respuesta=[];
    foreach ($rsult as $row){
        $respuesta[] = $row;
    }
}else{
    if ($rsult){
        $respuesta['res']=true;
    }
}

echo json_encode($respuesta);
