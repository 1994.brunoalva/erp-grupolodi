<?php


$indexRuta=1;

$nombremodule="editor";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../public/css/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../public/plugins/sweetalert2/sweetalert2.min.css">

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
        .box-shadow{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .box-shadow:hover{
            box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .fade-enter-active, .fade-leave-active {
            transition: opacity .2s;
        }
        .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
            opacity: 0;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Upload</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <a  href="index2.php"  class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Consulta
                                        </a>
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <button  data-toggle="modal" data-target="#modal_caja" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Abrir Caja
                                        </button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <!--div class="col-md-12 text-right" style="margin-bottom: 20px">
                                    <button  class="btn btn-primary"  data-toggle="modal" data-target="#modal_proveedor"><i class="fa fa-plus"></i> Nuevo</button>

                                </div-->
                                <div class="row">
                                    <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                        <label class="col-xs-12 no-padding">Ruta:</label>

                                        <div class="input-group col-xs-12 no-padding">
                                            <input  v-model="ruta"  type="text" class="form-control" autocomplete="off"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                        <label class="col-xs-12 no-padding">Archivo:</label>

                                        <div class="input-group col-xs-12 no-padding">
                                            <input id="input-file"  type="file" class="form-control" autocomplete="off"
                                                   aria-describedby="basic-addon1"
                                                   value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                        <label class="col-xs-12 no-padding">:</label>

                                        <div class="input-group col-xs-12 no-padding text-center">
                                            <button v-on:click="guardar()" class="btn btn-primary">Guardar</button>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12  col-sm-12 col-md-12">
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" :style="'width:'+ porcentaje+'%'" :aria-valuenow="porcentaje" aria-valuemin="0" aria-valuemax="100">{{porcentaje}}</div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>

    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="module" src="../public/js/Input_validate.js"></script>
    <script src="../public/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>

        function guaardaCA() {
            console.log("ingresoooo")
        }


        const APP =  new Vue({
            el:"#contenedorprincipal",
            data:{
                porcentaje:0,
                ruta:''
            },
            methods:{
                guardar(){
                    var frmData = new FormData();
                    frmData.append('file', $("#input-file")[0].files[0]);
                    frmData.append('folder', this.ruta);
                    $.ajax({
                        xhr: function() {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function(evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = ((evt.loaded / evt.total) * 100);
                                    APP._data.porcentaje=percentComplete;
                                }
                            }, false);
                            return xhr;
                        },
                        type: 'POST',
                        url: '../ajaxs/upd_files_m.php',
                        data: frmData,
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function(){
                            // app._data.progreso=0;
                            // app._data.starProgres=true;
                            $.toast({
                                heading: 'INFORMACION',
                                text: "guardando archivo",
                                icon: 'info',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        },
                        error:function(err){
                            console.log(err.responseText)
                            //alert('File upload failed, please try again.');
                            $.toast({
                                heading: 'ERROR',
                                text: "error al guardar el archivo",
                                icon: 'error',
                                position: 'top-right',
                                hideAfter: '2500',
                            });

                        },
                        success: function(resp){
                            $.toast({
                                heading: 'INFORMACION',
                                text: "archivo guardado",
                                icon: 'info',
                                position: 'top-right',
                                hideAfter: '2500',
                            });

                            console.log(resp)

                            setTimeout(function () {
                                APP._data.porcentaje=0;
                            },1500)


                        }
                    });
                }
            }
        });




        $( document ).ready(function() {

        });
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }

        function alerInfo(msg) {
            $.toast({
                heading: 'INFORMACION',
                text: msg,
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
        }
    </script>



</body>


</html>
