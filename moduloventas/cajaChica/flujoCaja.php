<?php
$indexRuta=1;

require '../conexion/Conexion.php';
require "../utils/Tools.php";


$tools= new Tools();

$conexion = (new Conexion())->getConexion();

$sql="SELECT * FROM sys_ven_facturacion  WHERE fac_estatus != 'TRASPASO'";

$resul_conceptos = $conexion->query("SELECT * FROM sys_caja_conceptos");
$resulFact = $conexion->query($sql);

$sql="SELECT * FROM sys_cob_egreso WHERE egr_tipo='CC'";
$resulEgres = $conexion->query($sql);
$nombremodule='Caja Chica';

$ingresoS=0;
$ingresoD=0;

$egresoS=0;
$egresod=0;

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../public/css/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../public/plugins/sweetalert2/sweetalert2.min.css">

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
        .box-shadow{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .box-shadow:hover{
            box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .fade-enter-active, .fade-leave-active {
            transition: opacity .2s;
        }
        .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
            opacity: 0;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <input id="idCaja" type="hidden" value="<?php echo $_GET['caja']?>">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">flujo - Caja Chica</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <!--<div class="col-lg-6 text-right">
                                        <a href="new-folder.php" id="folder_btn_nuevo_folder" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Folder
                                        </a>

                                    </div>-->

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div class="col-md-12 text-right" style="margin-bottom: 20px">
                                    <button  class="btn btn-primary" onclick="APP.limpiar()"  data-toggle="modal" data-target="#modal_caja_flujo"><i class="fa fa-plus"></i> Nuevo</button>

                                </div>

                                <table id="tabla-caja-flujo" class="table table-striped table-bordered">
                                    <thead>
                                    <tr  style="background-color: #007ac3; color: white">
                                        <th class="text-center" style="border-right-color: #007ac3" >FECHA</th>
                                        <th class="text-center" style="border-right-color: #007ac3" >DETALLE</th>
                                        <th class="text-center" style="border-right-color: #007ac3" >MONEDA</th>
                                        <th class="text-center" style="border-right-color: #007ac3" >INGRESO</th>
                                        <th class="text-center" style="border-right-color: #007ac3" >EGRESO</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php


                                    foreach ($resulFact as $fac){
                                        $facSN=$tools->numeroParaDocumento($fac['fac_num'],6);

                                        if ($fac['sun_id']==2){
                                            $ingresoD +=  $fac['fac_total'];
                                        }else{
                                            $ingresoS += $fac['fac_total'];
                                        }

                                        ?>

                                        <tr>
                                            <td class="text-center"><?= $fac['fac_fechae']?></td>
                                            <td class="text-center"><?= $fac['fac_serie']?> - <?= $facSN?></td>
                                            <td class="text-center"><?= $fac['sun_id']==2?'DOLARES':'SOLES'?></td>
                                            <td class="text-center" style="color: #2a5eed;" ><?= $fac['fac_total']?></td>
                                            <td class="text-center" style="color: #da384c;"></td>
                                            <td class="text-center"><a target="_blank" href="../ventas/comprobante_pdf.php?venta=<?= $fac['fac_id']?>" class="btn btn-info"><i class="fa fa-file"></i></a></td>
                                        </tr>

                                        <?php    }
                                    ?>
 <?php
                                    foreach ($resulEgres as $egre){
                                        if ($egre['sun_id']==2){
                                            $egresod +=  $egre['egr_monto'];
                                        }else{
                                            $egresoS += $egre['egr_monto'];
                                        }

                                        ?>

                                        <tr>
                                            <td class="text-center"><?= $egre['egr_fecha']?></td>
                                            <td class="text-center"><?= $egre['egr_descrip']?>  <?= $egre['egr_numref']?></td>
                                            <td class="text-center"><?= $egre['sun_id']==2?'DOLARES':'SOLES'?></td>
                                            <td class="text-center" style="color: #2a5eed;" ></td>
                                            <td class="text-center" style="color: #da384c;"><?= $egre['egr_monto']?></td>
                                            <td class="text-center">
                                                <button onclick="APP.getDataInfoEgreso(<?= $egre['egr_id']?>)"  data-toggle="modal" data-target="#modal_caja_flujo" class="btn btn-info"><i class="fa fa-eye"></i></button>
                                                <button onclick="APP.eliminar(<?= $egre['egr_id']?>)"  class="btn btn-danger"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>

                                        <?php    }
                                    ?>



                                    </tbody>
                                </table>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <table  class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                            <tr  style="background-color: #007ac3; color: white">
                                                <th  style="border-right-color: #007ac3">TOTAL INGRESOS</th>
                                                <th  style="border-right-color: #007ac3">TOTAL EGRESOS</th>
                                                <th  style="border-right-color: #007ac3">SALDO</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="text-center"><span style=" text-align: center ;color: #2a5eed; font-size: 20px; font-weight: bold">S/. <?=$ingresoS?></span></td>
                                                <td class="text-center"><span style=" text-align: center ;color: #da384c; font-size: 20px; font-weight: bold">S/. <?=$egresoS?></span></td>
                                                <td  style=" text-align: center ;font-size: 20px; ">S/. <?= ($ingresoS-$egresoS)?></td>
                                            </tr>
                                            <tr>
                                                <td  class="text-center" ><span style=" text-align: center ;color: #2a5eed; font-size: 20px; font-weight: bold">$ <?=$ingresoD?></span></td>
                                                <td class="text-center"><span style=" text-align: center ;color: #da384c; font-size: 20px; font-weight: bold">$ <?=$egresod?></span></td>
                                                <td style=" text-align: center ;font-size: 20px; ">$ <?=($ingresoD-$egresod)?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>


    <div class="modal fade" id="modal_caja_flujo" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Egreso</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form v-on:submit.prevent="guardarEgreso()" >
                        <div class="container-fluid">
                            <div class="row no-padding">
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>FECHA:</label>
                                    <div   class="input-group col-xs-12 no-padding">
                                        <input :disabled="!isEdit" v-model="egreso.fecha"
                                                type="date"
                                                required
                                            class="form-control">

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>MONEDA:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <select :disabled="!isEdit" id="moneda-id"
                                            class="form-control show-tick no-padding"
                                            data-live-search="true"
                                                required
                                                v-model="egreso.moneda"
                                            data-size="5">
                                            <option value="2">DOLAR</option>
                                            <option value="1">SOLES</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4" style="display: none">
                                    <label>HORA:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="hora-input" required disabled class="form-control" value="16:50">

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>TIPO DOCUMENTO:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <select :disabled="!isEdit" required v-model="egreso.doc" class="form-control">
                                            <option selected value="1">FACTURA</option>
                                            <option value="3">BOLETA</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-2">
                                    <label>NUMERO:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input :disabled="!isEdit" autocomplete="off" required  v-model="egreso.numero" class="form-control" value="">

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-2">
                                    <label>MONTO:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input :disabled="!isEdit" autocomplete="off"  @keypress="onlyNumber" required v-model="egreso.monto" id="monto-egreso" class="form-control" value="">

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-8">
                                    <label>DETALLE:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input :disabled="!isEdit" autocomplete="off" v-model="egreso.detalle" id="detalle-egreso" class="form-control"  value="">

                                    </div>
                                </div>






                            </div>

                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">

                            <button  type="submit" :disabled="!isEdit" class="btn btn-primary">
                                Guardar
                            </button>
                            <button type="button"  class="btn btn-success"
                                    data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>



    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script src="../public/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>

        function registrar() {
            var idCaja = $("#idCaja").val();
            var hora = $("#hora-input").val();
            var idconcepto = $("#consepto-id").val();
            var idmoneda = $("#moneda-id").val();
            var detalle = $("#detalle-egreso").val();
            var egreso = $("#monto-egreso").val();
            $.ajax({
                type: "POST",
                url: "../ajax/CajaChicaFlujo/newCajaFlujo.php",
                data: {
                    idCaja, hora,idconcepto,idmoneda,detalle,egreso
                },
                success: function (data) {
                    console.log(data)
                    location.reload();
                }
            });
        }

        const APP =  new Vue({
            el:"#modal_caja_flujo",
            data:{
                isEdit:true,
                egreso:{
                    empresa:1,
                    fecha:'',
                    moneda:'',
                    doc:'',
                    numero:'',
                    monto:'',
                    detalle:''
                }
            },
            methods:{
                eliminar(egreso){
                    $.ajax({
                        type: "POST",
                        url:  "../ajaxs/ajs_egreso.php",
                        data: {egreso,tipo:'d'},
                        success: function (resp) {
                            resp=JSON.parse(resp);
                            console.log(resp)
                            if (resp.res){
                                location.reload();
                            }else{
                                $.toast({
                                    heading: 'ERROR',
                                    text: "No se pudo eliminar",
                                    icon: 'error',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });
                            }
                        }
                    });

                },
                limpiar(){
                    APP.isEdit=true;
                    this.egreso={
                        empresa:1,
                            fecha:'',
                            moneda:'',
                            doc:'',
                            numero:'',
                            monto:'',
                            detalle:''
                    }
                },
                getDataInfoEgreso(egreso){
                    APP.isEdit=false;
                    $.ajax({
                        type: "POST",
                        url: "../ajaxs/ajs_egreso.php",
                        data: {egreso,tipo:'s'},
                        success: function (resp) {
                            resp = JSON.parse(resp);
                            console.log(resp)
                            APP._data.egreso={
                                        empresa:resp.emp_id,
                                        fecha:resp.egr_fecha,
                                        moneda:resp.sun_id,
                                        doc:resp.egr_tref,
                                        numero:resp.egr_numref,
                                        monto:resp.egr_monto,
                                        detalle:resp.egr_descrip
                                    }
                        }
                    });

                },
                onlyNumber ($event) {
                    //console.log($event.keyCode); //keyCodes value
                    let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
                    if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                        $event.preventDefault();
                    }
                },
                guardarEgreso(){
                    var data = {...this.egreso};
                    data.tipo="i"
                    console.log(data);
                    $.ajax({
                        type: "POST",
                        url: "../ajaxs/ajs_egreso.php",
                        data: data,
                        success: function (resp) {
                            console.log(resp)
                            if (isJson(resp)){
                                resp = JSON.parse(resp)
                                if (resp.res){
                                    location.reload();
                                }
                            }
                        }
                    });

                }
            }
        });


        $( document ).ready(function() {
            $('#tabla-caja-flujo').DataTable({
                "searching": false,
                "order": [[ 0, "desc" ]]
            });
            $("#tabla-Caja").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'csvHtml5',
                ]
            });
        });
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }

        function alerInfo(msg) {
            $.toast({
                heading: 'INFORMACION',
                text: msg,
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
        }
    </script>



</body>


</html>
