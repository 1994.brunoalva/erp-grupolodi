<?php

require '../conexion/Conexion.php';


$conexion = (new Conexion())->getConexion();



$indexRuta=1;

$nombremodule="Caja Chica";

$resul_tipoDoc = $conexion->query("SELECT *
FROM sys_tipo_documento");

$res_empresa = $conexion->query("SELECT * FROM sys_empresas");

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../public/css/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link src="../public/plugins/sweetalert2/sweetalert2.min.css">

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
        .box-shadow{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .box-shadow:hover{
            box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .fade-enter-active, .fade-leave-active {
            transition: opacity .2s;
        }
        .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
            opacity: 0;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Caja Chica</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <button  data-toggle="modal" data-target="#modal_caja" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Abrir Caja
                                        </button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <!--div class="col-md-12 text-right" style="margin-bottom: 20px">
                                    <button  class="btn btn-primary"  data-toggle="modal" data-target="#modal_proveedor"><i class="fa fa-plus"></i> Nuevo</button>

                                </div-->
                                <table id="tabla-caja" class="table table-striped table-bordered">
                                    <thead>
                                    <tr  style="background-color: #007ac3; color: white">
                                        <th style="border-right-color: #007ac3" >FECHA</th>
                                        <th style="border-right-color: #007ac3" > - </th>
                                        <th style="border-right-color: #007ac3" >EMPRESA</th>
                                        <th style="border-right-color: #007ac3" >TOTAL SOLES</th>
                                        <th style="border-right-color: #007ac3" >TOTAL DOLARES</th>
                                        <th>VER</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>


    <div class="modal fade" id="modal_caja" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width: 80%;">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Abrir Caja Chica</h3>
                    </div>
                </div>

                <div class="modal-body  no-border">
                    <form action="#" >
                        <div class="container-fluid">
                            <div class="row no-padding">
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>Empresa:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <select id="id-empresa"
                                                class="form-control show-tick no-padding"
                                                data-live-search="true"
                                                data-size="5">
                                            <?php
                                            foreach ($res_empresa as $row_emp){
                                                echo "<option value='{$row_emp['emp_id']}' >{$row_emp['emp_nombre']}</option>";
                                            }

                                            ?>

                                        </select>

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>Fecha</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="fecha" disabled class="form-control " value="<?php echo date("Y-m-d")?>">

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>Motivo</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input  disabled  class="form-control " value="Apertura de Caja">

                                    </div>
                                </div>

                                <div class="form-group col-xs-12 col-sm-3">
                                    <label>Monto soles</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="montosoles" class="form-control " >

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-3">
                                    <label>Monto dolares</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="montodolares" class="form-control " >

                                    </div>
                                </div>

                                <div class="form-group col-xs-12 col-sm-6">
                                    <label>Descripcion</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="descripcion" class="form-control " >

                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">

                            <button onclick="guardarCajaApertura()"  type="button"  class="btn btn-primary">
                                Guardar
                            </button>
                            <button  type="button"  class="btn btn-success"
                                    data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>



    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script src="../public/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>

        function guaardaCA() {
            console.log("ingresoooo")
        }


        const APP =  new Vue({
            el:"#contenedorprincipal",
            data:{

            },
            methods:{
            }
        });


        function guardarCajaApertura(){
            console.log("ingresoooo")
            var idEmpresa = $("#id-empresa").val();
            var fecha = $("#fecha").val();
            var soles = $("#montosoles").val();
            var dolares = $("#montodolares").val();
            var descrip = $("#descripcion").val();

            $.ajax({
                type: "POST",
                url: "../ajax/CajaChicaFlujo/newCaja.php",
                data: {
                    idEmpresa,  fecha,soles,dolares,descrip
                },
                success: function (data) {
                    console.log(data)
                    const jso = JSON.parse(data)
                    location.reload();
                    $("#id-empresa").val('');
                    $("#fecha").val('');
                    $("#montosoles").val('');
                    $("#montodolares").val('');
                    $("#descripcion").val('');
                }
            });


        }


        $( document ).ready(function() {
            $("#tabla-Caja").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'csvHtml5',
                ]
            });
        });
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }

        function alerInfo(msg) {
            $.toast({
                heading: 'INFORMACION',
                text: msg,
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
        }
    </script>



</body>


</html>
