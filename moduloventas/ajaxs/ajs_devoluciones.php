<?php

require "../dao/DevolucionesDao.php";
require "../dao/DevolucionesDetalleDao.php";
require "../dao/ProductoDao.php";
require "../dao/ProductoPromocionDao.php";

$devolucionesDao = new DevolucionesDao();
$devolucionesDetalleDao= new DevolucionesDetalleDao();
$productoDao= new ProductoDao();
$productoPromocionDao=new ProductoPromocionDao();

$tipo = filter_input(INPUT_POST, 'tipo');

$respuesta = array("res"=>false);



if ($tipo=="i"){

    $produtosDv = json_decode($_POST['productos']);

    $devolucionesDao->setEstado(1);
    $devolucionesDao->setFacId($_POST['docVentaId']);
    $devolucionesDao->setEmpId($_POST['empresa']);
    $devolucionesDao->setFecha($_POST['fecha']);
    $devolucionesDao->setNumDoc($_POST['numDoc']);
    $devolucionesDao->setObservaciones($_POST['obs']);
    $devolucionesDao->setTipoDoc($_POST['tipoDoc']);
    $devolucionesDao->setUbicacion("1");
    if ($devolucionesDao->insertar()){
        $respuesta["res"]=true;
        $respuesta["id"]=$devolucionesDao->getDevoId();
        $devolucionesDetalleDao->setDevoId($devolucionesDao->getDevoId());
       // echo "1";
        foreach ($produtosDv as $pordDv){
           // echo "2";
            $devolucionesDetalleDao->setEstado($pordDv->estado);
            $devolucionesDetalleDao->setProdEmpreId($pordDv->idProdEmpr);
            $devolucionesDetalleDao->setCantidad($pordDv->cnt);
            $devolucionesDetalleDao->setUbica($pordDv->ubica);
            if (!$devolucionesDetalleDao->insertar()){
                $respuesta["res"]=false;
            }
        }

        if ($respuesta["res"]){
            foreach ($produtosDv as $pordDv){
                if ($pordDv->ubica==1){
                    $productoDao->setProdEmpreId($pordDv->idProdEmpr);
                    $productoDao->setCantidad($pordDv->cnt);
                    $productoDao->addStock();
                }else{
                    $productoPromocionDao->setProdEmpId($pordDv->idProdEmpr);
                    $productoPromocionDao->setEstado($pordDv->estado);
                    $resProm = $productoPromocionDao->verificar();

                    if ($rowPro = $resProm->fetch_assoc()){
                        $productoPromocionDao->setProdPromoId($rowPro['prod_promo_id']);
                        $productoPromocionDao->setCantidad($pordDv->cnt);
                        $productoPromocionDao->addStock();
                    }else{
                        $productoPromocionDao->setCantidad($pordDv->cnt);
                        $productoPromocionDao->setEstado($pordDv->estado);
                        $productoPromocionDao->setProdEmpId($pordDv->idProdEmpr);
                        $productoPromocionDao->setEmpId('null');
                        $productoPromocionDao->setPrecio(0);
                        $productoPromocionDao->setProdId('null');
                        $productoPromocionDao->insertar();

                    }
                }
            }
        }

    }

}elseif ($tipo=="s"){
    $devolucionesDao->setDevoId($_POST['devo']);
    $devolucionesDetalleDao->setDevoId($_POST['devo']);
    $res = $devolucionesDao->getdata();

    if ($rowD = $res->fetch_assoc()){

        $prod = array();
        $resD = $devolucionesDetalleDao->getdata();

        foreach ($resD as $rowDD){
            $prod[]=$rowDD;
        }

        $rowD['productos'] = $prod;
        $respuesta["res"]=true;
        $respuesta["data"]=$rowD;
    }
}

echo  json_encode($respuesta);