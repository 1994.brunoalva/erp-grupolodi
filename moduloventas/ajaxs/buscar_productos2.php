<?php
require "../dao/ProductoDao.php";

$productoDao = new ProductoDao();

$searchTerm = filter_input(INPUT_GET, 'term');
$idEmpresa = filter_input(INPUT_GET, 'empresa');

$productoDao->setProdEmpreId($idEmpresa);
$result = $productoDao->getdataBuscar2($searchTerm);

$arrRes=array();
//echo $searchTerm;
foreach ($result as $row){
    $row['value']=$row['produ_nombre'] ." | SKU:".$row['produ_sku'];

    $arrRes []= $row;
}
echo  json_encode($arrRes);


