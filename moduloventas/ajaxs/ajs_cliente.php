<?php
require "../dao/ClienteDao.php";
require "../dao/ClientesSucursalesDao.php";
require "../dao/RepresentantesLegalesDao.php";

$clienteDao = new ClienteDao();
$clientesSucursalesDao = new ClientesSucursalesDao();
$representantesLegalesDao = new RepresentantesLegalesDao();

$tipo = filter_input(INPUT_POST, 'tipo');

$resulJS = array("res"=>false);

if ($tipo =="s"){
    $aacion = filter_input(INPUT_POST, 'acc');
    if ($aacion=="direcciones"){
        $clienteDao->setCliId(filter_input(INPUT_POST, 'idc'));
        $resul = $clienteDao->getdata();
        $resulJS= array();
        if ($row  = $resul->fetch_array()){
            $resulJS[]= array("id"=>"0","direccion"=>$row['cli_direc'],'tipo'=>'SU.  SUCURSAL');
            $clienteDao->setCliNdoc($row['cli_ndoc']);
            $resulDirecciones = $clienteDao->getDireciones();
            foreach ($resulDirecciones as $dir){
                $resulJS[]= array("id"=>$dir['su_id'],"direccion"=>$dir['su_direccion'],'tipo'=>$dir['su_tipo']);
            }
        }

    }elseif ($aacion=="clie"){
        $clienteDao->setCliNdoc(filter_input(INPUT_POST, 'ruc'));
        $resulJS= array();
        $resCli = $clienteDao->getdataByNumDc();
        if ($row = $resCli->fetch_assoc()){
            $resulJS= $row;
        }
    }
}elseif ($tipo =="i"){

    //echo '1111';
    $direcciones = json_decode($_POST['direcciones']);
    $representantes = json_decode($_POST['representantes']);

    $clienteDao->setCliNdoc($_POST['numDoc']);
    $clienteDao->setCliClasi($_POST['clas']);
    $clienteDao->setCliCondi($_POST['condicion']);
    $clienteDao->setCliConta($_POST['contac']);
    $clienteDao->setCliDirec($_POST['direccion']);
    $clienteDao->setCliEstad($_POST['estado']);
    $clienteDao->setCliMail1($_POST['email1']);
    $clienteDao->setCliMail2($_POST['email2']);
    $clienteDao->setCliNomape($_POST['nomRaz']);
    $clienteDao->setCliTdoc($_POST['tipoDoc']);
    $clienteDao->setCliTele($_POST['telf1']);
    $clienteDao->setCliTele2($_POST['telf2']);
    $clienteDao->setCliVende($_POST['vend']);
    $clienteDao->setDepCod($_POST['depart']);
    $clienteDao->setDisCod($_POST['distrito']);
    $clienteDao->setProCod($_POST['provin']);
    $clienteDao->setUbiCod($_POST['ubigeo']);

    if ($clienteDao->insertar()){
        $resulJS['res']=true;
        $clientesSucursalesDao->setSuRuc($_POST['numDoc']);
        foreach ($direcciones as $row){
            $clientesSucursalesDao->setIdAgencia(0);
            $clientesSucursalesDao->setSuCodigo($row->codigo);
            $clientesSucursalesDao->setSuDireccion($row->direc);
            $clientesSucursalesDao->setSuTipo($row->tipo);
            $clientesSucursalesDao->setUbigeo($row->ubigeo);
            if (!$clientesSucursalesDao->insertar()){
                $resulJS['res']=false;
            }
        }
        foreach ($representantes as $repr){
            $fechaFr = explode("/", $repr->desde);
            $representantesLegalesDao->setRepCargo($repr->cargo);
            $representantesLegalesDao->setRepDesde($fechaFr[2]."-".$fechaFr[1].'-'.$fechaFr[0]);
            $representantesLegalesDao->setRepNdoc($repr->numdoc);
            $representantesLegalesDao->setRepNomape($repr->nombre);
            $representantesLegalesDao->setRepRuc($_POST['numDoc']);
            $representantesLegalesDao->setRepTdoc($repr->tipodoc);
            if (!$representantesLegalesDao->insertar()){
                $resulJS['res']=false;
            }
        }
    }

}elseif ($tipo =="verificar"){
    $clienteDao->setCliNdoc($_POST['numDoc']);
    if ($row=$clienteDao->verificar()->fetch_assoc()){
        $resulJS['res']=true;
    }
}elseif ($tipo =="select-contac"){
    $contac = $_POST['contac'];
    $clienteDao->setCliConta($contac);
    $resp  = $clienteDao->getdataContac();
    $resulJS=[];
    foreach ($resp as $row){
        $resulJS[] = $row;
    }
}elseif ($tipo =="get_data"){

    $cliente = $_POST['clien'];

    $clienteDao->setCliId($cliente);
    $res=$clienteDao->getdata()->fetch_assoc();

    $clientesSucursalesDao->setSuRuc($res['cli_ndoc']);
    $representantesLegalesDao->setRepRuc($res['cli_ndoc']);

    $direcciones=[];
    $sucursales=[];

    $resSucur = $clientesSucursalesDao->getDatos();
    $resRepre = $representantesLegalesDao->getDatos();

    foreach ($resSucur as $rowSu){
        $direcciones[]=$rowSu;
    }
    foreach ($resRepre as $rowre){
        $sucursales[]=$rowre;
    }
    $res['direcciones']=$direcciones;
    $res['representantes']=$sucursales;
    $resulJS = $res;
}elseif ($tipo =="u"){
    $direcciones = json_decode($_POST['direcciones']);
    $representantes = json_decode($_POST['representantes']);

    $clienteDao->setCliId($_POST['idCli']);
    $clienteDao->setCliNdoc($_POST['numDoc']);
    $clienteDao->setCliClasi($_POST['clas']);
    $clienteDao->setCliCondi($_POST['condicion']);
    $clienteDao->setCliConta($_POST['contac']);
    $clienteDao->setCliDirec($_POST['direccion']);
    $clienteDao->setCliEstad($_POST['estado']);
    $clienteDao->setCliMail1($_POST['email1']);
    $clienteDao->setCliMail2($_POST['email2']);
    $clienteDao->setCliNomape($_POST['nomRaz']);
    $clienteDao->setCliTdoc($_POST['tipoDoc']);
    $clienteDao->setCliTele($_POST['telf1']);
    $clienteDao->setCliTele2($_POST['telf2']);
    $clienteDao->setCliVende($_POST['vend']);
    $clienteDao->setDepCod($_POST['depart']);
    $clienteDao->setDisCod($_POST['distrito']);
    $clienteDao->setProCod($_POST['provin']);
    $clienteDao->setUbiCod($_POST['ubigeo']);

    if ($clienteDao->actualizar()){
        $resulJS['res']=true;
        $clientesSucursalesDao->setSuRuc($_POST['numDoc']);
        foreach ($direcciones as $row){

            if ($row->su_id=='0'){
                $clientesSucursalesDao->setIdAgencia(0);
                $clientesSucursalesDao->setSuCodigo($row->su_codigo);
                $clientesSucursalesDao->setSuDireccion($row->su_direccion);
                $clientesSucursalesDao->setSuTipo($row->su_tipo);
                $clientesSucursalesDao->setUbigeo($row->ubigeo);
                if (!$clientesSucursalesDao->insertar()){
                    $resulJS['res']=false;
                }
            }


        }
        foreach ($representantes as $repr){

            if ($repr->rep_id=='0'){
                $fechaFr = explode("/", $repr->rep_desde);

                $representantesLegalesDao->setRepCargo($repr->rep_cargo);
                $representantesLegalesDao->setRepDesde($fechaFr[2]."-".$fechaFr[1].'-'.$fechaFr[0]);
                $representantesLegalesDao->setRepNdoc($repr->rep_ndoc);
                $representantesLegalesDao->setRepNomape($repr->rep_nomape);
                $representantesLegalesDao->setRepRuc($_POST['numDoc']);
                $representantesLegalesDao->setRepTdoc($repr->rep_tdoc);
                if (!$representantesLegalesDao->insertar()){
                    $resulJS['res']=false;
                }
            }


        }
    }
}

echo  json_encode($resulJS);