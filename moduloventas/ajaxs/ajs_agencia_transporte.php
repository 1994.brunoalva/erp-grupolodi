<?php
require "../dao/AgenciaTransporteDao.php";

$agenciaTransporteDao= new AgenciaTransporteDao();

$tipo = filter_input(INPUT_POST, 'tipo');

$respuesta = array("res"=>false);


if ($tipo=="i"){

    $carrosDatta = json_decode($_POST['listaCarrosCon']);

    $agenciaTransporteDao->setEstado($_POST['estado']);
    $agenciaTransporteDao->setDireccion($_POST['direccion']);
    $agenciaTransporteDao->setCondicion($_POST['condicion']);
    $agenciaTransporteDao->setRazonSocial($_POST['razon']);
    $agenciaTransporteDao->setRuc($_POST['ruc']);
    $agenciaTransporteDao->setTelefono($_POST['telefono']);
    if ($agenciaTransporteDao->insertar()){
        $respuesta['res']=true;
        foreach ($carrosDatta as $rowCar){
            $sql ="INSERT INTO sys_agen_trans_carro VALUES (null,'{$agenciaTransporteDao->getId()}','{$rowCar->placa}','{$rowCar->marca}','{$rowCar->modelo}','1');";
            $resCar = $agenciaTransporteDao->exeSql($sql);
            if ($resCar){
                $temidCon = $agenciaTransporteDao->getDataRetor();
                foreach ($rowCar->conductores as $rowCon){
                    $sql="INSERT INTO sys_carro_conductores
                            VALUES (null,
                                    '$temidCon',
                                    '{$rowCon->nombre}',
                                    '{$rowCon->doc}',
                                    '{$rowCon->nro_doc}',
                                    '{$rowCon->nro_lice}');";
                    $agenciaTransporteDao->exeSql($sql);
                }
            }
        }
    }
}elseif ($tipo=="s"){
    $agenciaTransporteDao->setId($_POST['id']);
    $respuesta=[];
    $res = $agenciaTransporteDao->getDato();
    if ($row = $res->fetch_assoc()){
        $carros =[];
        $sql="SELECT * FROM sys_agen_trans_carro WHERE agensia_id =".$_POST['id'];
        $resss = $agenciaTransporteDao->exeSql($sql);
        foreach ($resss as $rowCar){

            $sql ="SELECT * FROM sys_carro_conductores WHERE carro_id =".$rowCar['carr_id'];
            $resssCon = $agenciaTransporteDao->exeSql($sql);
            $rowCar['conductores']=[];
            foreach ($resssCon as $rowCon){
                $rowCar['conductores'][]=$rowCon;
            }
            $carros[]=$rowCar;
        }

        $row['carros']= $carros;
        $respuesta=$row;
    }
}elseif ($tipo=="u"){
    $agenciaTransporteDao->setId($_POST['id']);
    $agenciaTransporteDao->setEstado($_POST['estado']);
    $agenciaTransporteDao->setDireccion($_POST['direccion']);
    $agenciaTransporteDao->setCondicion($_POST['condicion']);
    $agenciaTransporteDao->setRazonSocial($_POST['razon']);
    $agenciaTransporteDao->setRuc($_POST['ruc']);
    $agenciaTransporteDao->setTelefono($_POST['telefono']);
    if ($agenciaTransporteDao->actualizar()){
        $respuesta['res']=true;
    }
}elseif($tipo =="in-car"){
    $sql ="INSERT INTO sys_agen_trans_carro VALUES (null,'{$_POST['agencia']}','{$_POST['placa']}','{$_POST['marca']}','{$_POST['modelo']}','1');";
    $resCar = $agenciaTransporteDao->exeSql($sql);

    if ($resCar){
        $sql="SELECT * FROM sys_agen_trans_carro WHERE carr_id =".$agenciaTransporteDao->getDataRetor();
        $resss = $agenciaTransporteDao->exeSql($sql);
        if ($rowCa = $resss->fetch_assoc()){
            $rowCa['conductores']=[];
            $respuesta=$rowCa;
        }
    }
}elseif($tipo =="del-car"){
    $sql = " DELETE
FROM  sys_carro_conductores
WHERE carro_id = '{$_POST['carro']}';";
    $agenciaTransporteDao->exeSql($sql);
    $sql = " DELETE
FROM sys_agen_trans_carro
WHERE carr_id = '{$_POST['carro']}';";
    //echo $sql;
    $resss = $agenciaTransporteDao->exeSql($sql);
    if ($resss){
        $respuesta['res'] =true;
    }
}elseif($tipo =="del-con"){
    $sql = " DELETE
FROM  sys_carro_conductores
WHERE conductor_id = '{$_POST['conduc']}';";

    $resss = $agenciaTransporteDao->exeSql($sql);
    if ($resss){
        $respuesta['res'] =true;
    }
}elseif($tipo =="in-con"){
    $sql="INSERT INTO sys_carro_conductores
                            VALUES (null,
                                    '{$_POST['carro_id']}',
                                    '{$_POST['nombre']}',
                                    '{$_POST['doc']}',
                                    '{$_POST['nro_doc']}',
                                    '{$_POST['nro_lice']}');";
    $resCar = $agenciaTransporteDao->exeSql($sql);

    if ($resCar){
        $sql="SELECT * FROM sys_carro_conductores WHERE conductor_id =".$agenciaTransporteDao->getDataRetor();
        $resss = $agenciaTransporteDao->exeSql($sql);
        if ($rowCa = $resss->fetch_assoc()){
            $respuesta=$rowCa;
        }
    }
}

echo  json_encode($respuesta);

