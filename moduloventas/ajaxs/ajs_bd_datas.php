<?php
require_once "../conexion/Conexion.php";
$conexion = (new Conexion())->getConexion();

$tipo = filter_input(INPUT_POST, 'tipo');

if ($tipo =="pagodetalle"){
    $sql="SELECT * FROM sys_cob_tip_pagos_detalle WHERE pag_id =" . $_POST['id'];
    $rs = $conexion->query($sql);
    $resp = array();

    foreach ($rs as $row){
        $resp[] = $row;
    }
    echo json_encode($resp);
}elseif ($tipo=="carros_agen_conduc"){
    $sql = "SELECT * FROM sys_carro_conductores WHERE carro_id=".$_POST['carro'];
    $rs = $conexion->query($sql);
    $resp = array();

    foreach ($rs as $row){
        $resp[] = $row;
    }
    echo json_encode($resp);
}elseif ($tipo=="carros_agen"){
    $sql = "SELECT * FROM sys_agen_trans_carro WHERE agensia_id =".$_POST['agencia'];
    $rs = $conexion->query($sql);
    $resp = array();

    foreach ($rs as $row){
        $resp[] = $row;
    }
    echo json_encode($resp);
}elseif ($tipo=="numdocsunat"){
    $sql = "SELECT * FROM sys_ven_documento_empresa WHERE id_doc_emp ={$_POST['idtipono']}  AND id_empre = ".$_POST['idEmpre'];
    $rs = $conexion->query($sql);
    $resp = array();

    foreach ($rs as $row){
        $resp = $row;
    }
    echo json_encode($resp);
}elseif ($tipo=="reg-cont"){
    $sql = "INSERT INTO sys_cliente_contacto VALUE(null,'{$_POST['nombre']}','1');";
    //echo $sql;
    $rs = $conexion->query($sql);
    $resp = array("res" =>false);

    if ($rs){
        $resp["res"] = true;
        $resp["id"] = $conexion->insert_id;
    }
    echo json_encode($resp);
}elseif ($tipo=="prodvents"){
    $idVent = $_POST['idVentasPro'];
    $venIds =explode("-", $idVent);

    $resp = array();
    foreach ($venIds as $idv){
        $sql = "SELECT 
  fac.*,
  emp.emp_nombre 
FROM
  sys_ven_facturacion AS fac 
  INNER JOIN sys_empresas AS emp 
    ON fac.emp_id = emp.emp_id 
    WHERE fac.fac_id = ".$idv;
        $rs = $conexion->query($sql);
        if ($rowVen = $rs->fetch_assoc()){
            $sql ="SELECT 
               fac_deta.*,
               prod_empre.cantidad,
               empr.emp_nombre,
               prod.produ_nombre,
               prod.produ_sku,
               marca.mar_nombre,
               pais.pais_nombre,
               unidad.unidad_nombre
               
            FROM
              sys_ven_facturacion_detalle AS fac_deta 
              INNER JOIN sys_producto_empresa AS prod_empre 
                ON fac_deta.prod_id = prod_empre.prod_empre_id 
                INNER JOIN sys_producto AS prod ON prod_empre.id_prod=prod.produ_id 
                INNER JOIN sys_empresas AS empr ON prod_empre.id_empresa=empr.emp_id
                INNER JOIN sys_com_marca AS marca ON prod.mar_id=marca.mar_id
                INNER JOIN sys_pais AS pais ON prod.pais_id = pais.pais_id
                INNER JOIN sys_unidad AS unidad ON prod.unidad_id = unidad.unidad_id
                WHERE fac_deta.fac_id=".$idv;
            $rsProdVent = $conexion->query($sql);
            $rowVen['productos']=[];

            $subTotalVen =0;

            foreach ($rsProdVent as $roPVD){
                $subTotalVen += $roPVD['facd_subtotal'];
                $rowVen['productos'][] = $roPVD;
            }
            $igvPro = $subTotalVen * 0.18;
            $montoTotal = $subTotalVen+$igvPro;
            $rowVen['subTotal']=$subTotalVen;
            $rowVen['igv']=$igvPro;
            $rowVen['montoToal']=$montoTotal;
            $resp[]=$rowVen;
        }

    }


    echo json_encode($resp);
}elseif ($tipo=="tasaCambio"){


    $sql = "SELECT * FROM sys_tasa_cambio ORDER BY tas_id  DESC LIMIT 1";
    $rs = $conexion->query($sql);
    $resp = array();

    foreach ($rs as $row){
        $resp = $row;
    }
    echo json_encode($resp);
}elseif ($tipo=="egresoI"){


    $sql = "SELECT * FROM sys_tasa_cambio ORDER BY tas_id  DESC LIMIT 1";
    $rs = $conexion->query($sql);
    $resp = array();

    foreach ($rs as $row){
        $resp = $row;
    }
    echo json_encode($resp);
}elseif ($tipo=="cons_ubigeo"){
    $depar=$_POST['depar'];
    $distr=$_POST['distri'];
    $provin=$_POST['provin'];
    $sql = "SELECT * FROM sys_ven_departamento WHERE dep_nombre ='$depar'";
    $rs = $conexion->query($sql);
    $resp = array("depa"=>'0','provin'=>'0','distri'=>'0','ubigeo'=>'','index'=>$_POST['ind']);

    if ($rowDep = $rs->fetch_assoc()){
        $resp['depa']=$rowDep['dep_cod'];
        $resp['ubigeo'] .= $rowDep['dep_cod'];
        $sql="SELECT * FROM sys_ven_provincia WHERE pro_nombre='$provin' AND dep_codigo='{$rowDep['dep_cod']}'";
        $rsPro = $conexion->query($sql);
        if ($rowPro = $rsPro->fetch_assoc()){
            $resp['provin']=$rowPro['pro_cod'];
            $resp['ubigeo'] .= $rowPro['pro_cod'];
            $sql="SELECT * FROM sys_ven_distrito WHERE dis_nombre='$distr' AND dep_codigo='{$rowDep['dep_cod']}' AND pro_codigo='{$rowPro['pro_cod']}'";
            $rsDist = $conexion->query($sql);
            if ($rowDist = $rsDist->fetch_assoc()){
                $resp['distri']=$rowDist['dis_codigo'];
                $resp['ubigeo'] .= $rowDist['dis_codigo'];

            }
        }
    }
    echo json_encode($resp);
}elseif ($tipo=="list_prov"){
    $codDep = $_POST['dep'];
    $sql="SELECT * FROM sys_ven_provincia WHERE   dep_codigo='$codDep'";
    $rsPro = $conexion->query($sql);
    $resp = array();
    foreach ($rsPro as $row){
        $resp[]=$row;
    }
    echo json_encode($resp);
}elseif ($tipo=="list_distri"){
    $codDep = $_POST['dep'];
    $codPro=$_POST['pro'];
    $sql="SELECT * FROM sys_ven_distrito WHERE  dep_codigo='$codDep' AND pro_codigo='$codPro'";
    $rsPro = $conexion->query($sql);
    $resp = array();
    foreach ($rsPro as $row){
        $resp[]=$row;
    }
    echo json_encode($resp);
}elseif ($tipo=="get_promo"){

    $promo = $_POST['prodI'];

    $sql="SELECT * FROM vista_productos_promo WHERE prod_promo_id= ".$promo;
    $rsPro = $conexion->query($sql);
    $resp = array();
    if ($row = $rsPro->fetch_assoc()){
        $resp = $row;
    }
    echo json_encode($resp);
}











