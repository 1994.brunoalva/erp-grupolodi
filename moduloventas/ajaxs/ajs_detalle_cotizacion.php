<?php
require "../dao/DetalleCotizacionDao.php";

$detalleCotizacionDao = new DetalleCotizacionDao();

$tipo = filter_input(INPUT_POST, 'tipo');

$respuesta = array("res"=>false);

if ($tipo=="i"){
    $idCoti= filter_input(INPUT_POST, 'idCoti');
    $productos= json_decode(filter_input(INPUT_POST, 'productos'));

    //print_r($productos);
    $respuesta['res'] = true;
    $detalleCotizacionDao->setIdCotizacion($idCoti);
    foreach ($productos as $prod){
        $detalleCotizacionDao->setCantidad($prod->cantidad);
        $detalleCotizacionDao->setDescuento($prod->descuento);
        $detalleCotizacionDao->setIdEmpresa($prod->idempresa);
        $detalleCotizacionDao->setIdProducto($prod->id);
        $detalleCotizacionDao->setPrecioUnitario($prod->precio);
        $detalleCotizacionDao->setIdProdEmpre($prod->idProdEmpr);
        $detalleCotizacionDao->setProductoDesc($prod->producto);
        if (!$detalleCotizacionDao->insertar()){
            $respuesta['res'] = false;
        }
    }


}elseif ($tipo=="u"){
    $idCoti= filter_input(INPUT_POST, 'idCoti');
    $productos= json_decode(filter_input(INPUT_POST, 'productos'));

    //print_r($productos);
    $respuesta['res'] = true;
    $detalleCotizacionDao->setIdCotizacion($idCoti);
    if ($detalleCotizacionDao->eliminarByCotiId()){
        foreach ($productos as $prod){
            $detalleCotizacionDao->setCantidad($prod->cantidad);
            $detalleCotizacionDao->setDescuento($prod->descuento);
            $detalleCotizacionDao->setIdEmpresa($prod->idempresa);
            $detalleCotizacionDao->setIdProdEmpre($prod->idProdEmpr);
            $detalleCotizacionDao->setIdProducto($prod->id);
            $detalleCotizacionDao->setPrecioUnitario($prod->precio);
            $detalleCotizacionDao->setProductoDesc($prod->producto);
            if (!$detalleCotizacionDao->insertar()){
                $respuesta['res'] = false;
            }
        }
    }

}


echo json_encode($respuesta);
