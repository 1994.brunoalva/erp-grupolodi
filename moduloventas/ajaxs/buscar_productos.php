<?php
require "../dao/ProductoDao.php";

$productoDao = new ProductoDao();

$searchTerm = filter_input(INPUT_GET, 'term');


$result = $productoDao->getdataBuscar($searchTerm);

$arrRes=array();
//echo $searchTerm;
foreach ($result as $row){
    $row['value']=$row['produ_nombre'] ." | ".$row['unidad_nombre']." | ".$row['emp_nombre']. ' | CNT:' . $row['cantidad'];
    $productoDao->setProduCodprod($row['produ_codprod']);
    $resPre = $productoDao->getPrecioProducto();
    $preciosp=[];
    foreach ($resPre as $row2){
        $preciosp[] = $row2;
    }
    $row['precios']=$preciosp;
    $arrRes []= $row;
}
echo  json_encode($arrRes);


