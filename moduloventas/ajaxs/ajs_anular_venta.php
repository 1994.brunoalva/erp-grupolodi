<?php

require "../dao/VentaFacturacionDao.php";
require "../dao/VentaDetalleDao.php";
require "../dao/NotaElectronicaDao.php";
require "../dao/NotaDetalleDao.php";
require "../dao/DocumentoVentaNumeroDao.php";
require "../utils/Tools.php";


$idVenta = $_POST['venta'];
$fecha = $_POST['fechaC'];

$ventaFacturacionDao= new VentaFacturacionDao();
$ventaDetalleDao= new VentaDetalleDao();
$notaElectronicaDao= new NotaElectronicaDao();
$notaDetalleDao= new NotaDetalleDao();
$documentoVentaNumeroDao= new DocumentoVentaNumeroDao();
$tools= new Tools();

$ventaFacturacionDao->setFacId($idVenta);
$ventaDetalleDao->setFacId($idVenta);

$ventaFacturacionDao->setFechaA($fecha);

$resVent = $ventaFacturacionDao->getdataVenta();
$productosVenta = $ventaDetalleDao->getProductos();
$respuesta = array("res"=>false);
if ($rowV = $resVent->fetch_assoc()){
    $respuesta['res'] = true;
    $productos=[];
    foreach ($productosVenta as $prod){
        $productos[]=$prod;
    }
    $rowV['productos'] = $productos;
    $respuesta['data'] = $rowV;
}


if ($respuesta['res']){
    $datos= $respuesta['data'];

    $documentoVentaNumeroDao->setIdEmpre($datos['emp_id']);
    $documentoVentaNumeroDao->setIdDocEmp("5");

    $resDataND = $documentoVentaNumeroDao->getData()->fetch_assoc();

    $numeroSerie = $resDataND['serie']."-".$tools->numeroParaDocumento($resDataND['numero'],5);

    $notaElectronicaDao->setFacId($idVenta);
    $notaElectronicaDao->setCliNdoc($datos['cli_ndoc']);
    $notaElectronicaDao->setNotaSustento('ANULACION DE LA OPERACION');
    $notaElectronicaDao->setEmpId($datos['emp_id']);
    $notaElectronicaDao->setSunId($datos['sun_id']);
    $notaElectronicaDao->setNotSn($numeroSerie);
    $notaElectronicaDao->setNotCodMotivo('01');
    $notaElectronicaDao->setNotTipoDoc('5');
    $notaElectronicaDao->setNotcUsado('0');
    $notaElectronicaDao->setNotcTipo(strlen($datos['cli_ndoc'])==8?"3":"1");
    $notaElectronicaDao->setNotcMonto($datos['fac_total']);
    $notaElectronicaDao->setNotcDisponible('0');
    $notaElectronicaDao->setNotcFecha($fecha);
    $notaElectronicaDao->setNotcEstatus('APROBADO');
    $notaElectronicaDao->setNotcDescripcion('ANULACION DE LA OPERACION');
    $notaElectronicaDao->setAsigId('');

    if ($notaElectronicaDao->insertar()){
        $notaDetalleDao->setNotaId($notaElectronicaDao->getNotcId());
        foreach ($datos['productos'] as $prod){

            $notaDetalleDao->setPrecio($prod['facd_preciou']);
            $notaDetalleDao->setMedida('UND');
            $notaDetalleDao->setDescripcion($prod['produ_nombre']);
            $notaDetalleDao->setCatidad($prod['facd_cantidad']);

            if (!$notaDetalleDao->insertar()){
                $respuesta['res'] = false;
            }

        }
    }else{
        $respuesta['res'] = false;
    }

}

if ( $respuesta['res']){
    $ventaFacturacionDao->anularVenta();
}
$respuesta['data']="";
echo json_encode($respuesta);