<?php

require "../dao/ClientesSucursalesDao.php";
$clientesSucursalesDao = new ClientesSucursalesDao();
$tipo = $_POST['tipo'];

$resultado=array("res"=>false);

if ($tipo == 'in'){
    $clientesSucursalesDao->setSuRuc($_POST['cliente']);
    $clientesSucursalesDao->setUbigeo($_POST['ubigeo']);
    $clientesSucursalesDao->setSuTipo($_POST['tipo_dir']);
    $clientesSucursalesDao->setSuDireccion($_POST['direccion']);
    $clientesSucursalesDao->setSuCodigo('');
    $clientesSucursalesDao->setIdAgencia(0);
    if ($clientesSucursalesDao->insertar()){
        $resultado['res']=true;
    }
}


echo json_encode($resultado);

