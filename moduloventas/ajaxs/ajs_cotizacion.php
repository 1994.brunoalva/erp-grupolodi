<?php
require "../dao/CotizacionDao.php";
require "../dao/DetalleCotizacionDao.php";
require "../dao/ProductoDao.php";

$cotizacionDao = new CotizacionDao();
$productoDao = new ProductoDao();
$detalleCotizacionDao = new DetalleCotizacionDao();

$tipo = filter_input(INPUT_POST, 'tipo');

$respuesta = array("res"=>false);

if ($tipo=="i"){

    $cotizacionDao->setCotiAte($_POST['atencion']);
    $cotizacionDao->setCotiDiasCredito($_POST['cantidaddias']);
    $cotizacionDao->setCotiEstatus(1);
    $cotizacionDao->setCotiFecha($_POST['fecha']);
    $cotizacionDao->setCotiFechaVenta("");
    $cotizacionDao->setCotiLlega("");
    $cotizacionDao->setCotiObs($_POST['observaciones']);
    $cotizacionDao->setCotiPago($_POST['formapago']);
    $cotizacionDao->setCotiRazon($_POST['nombre']);
    $cotizacionDao->setCotiRuc($_POST['ruc']);
    $cotizacionDao->setCotiTelf($_POST['telefono']);
    $cotizacionDao->setCotiTotal($_POST['totalcoti']);
    $cotizacionDao->setCotiTpCambio(1);
    $cotizacionDao->setCotiTpDocu(strlen ($_POST['ruc'])==8?'DNI':"RUC");
    $cotizacionDao->setCotiTpMoneda($_POST['moneda']);
    $cotizacionDao->setCotiTpPago($_POST['tipopago']);
    $cotizacionDao->setIdAgencia('');
    $cotizacionDao->setIdCliente($_POST['id']);
    $cotizacionDao->setIdDireccion($_POST['idDireccion']);
    $cotizacionDao->setTipoCostoProd($_POST['tipreciopro']);
    $cotizacionDao->setEstado($_POST['estcot']);
    //echo $cotizacionDao->sqlInsert();
    if ($cotizacionDao->insertar()){
        $respuesta['res'] = true;
        $respuesta['idCoti'] = $cotizacionDao->getCotiId();
    }

}elseif($tipo=="s"){
    $cotizacionDao->setCotiId(filter_input(INPUT_POST, 'idCoti'));
    $detalleCotizacionDao->setIdCotizacion($cotizacionDao->getCotiId());
    $resul = $cotizacionDao->getData();
    $respuesta = [];
    if ($row = $resul->fetch_assoc()){
        $row['productos']=[];
        $rs= $detalleCotizacionDao->getListaProducto();

        foreach ($rs as $pro){

            $productoDao->setProduCodprod($pro['produ_codprod']);
            $resPre = $productoDao->getPrecioProducto();
            $preciosp=[];
            foreach ($resPre as $row2){
                $preciosp[] = $row2;
            }
            $pro['precios']=$preciosp;
            $row['productos'][] = $pro;
        }

        $respuesta = $row;

    }


}elseif($tipo=="u"){
    $cotizacionDao->setCotiId($_POST['idcotizacion']);
    $cotizacionDao->setCotiAte($_POST['atencion']);
    $cotizacionDao->setCotiDiasCredito($_POST['cantidaddias']);
    $cotizacionDao->setCotiEstatus(1);
    $cotizacionDao->setCotiFecha($_POST['fecha']);
    $cotizacionDao->setCotiFechaVenta("");
    $cotizacionDao->setCotiLlega("");
    $cotizacionDao->setCotiObs($_POST['observaciones']);
    $cotizacionDao->setCotiPago($_POST['formapago']);
    $cotizacionDao->setCotiRazon($_POST['nombre']);
    $cotizacionDao->setCotiRuc($_POST['ruc']);
    $cotizacionDao->setCotiTelf($_POST['telefono']);
    $cotizacionDao->setCotiTotal($_POST['totalcoti']);
    $cotizacionDao->setCotiTpCambio(1);
    $cotizacionDao->setCotiTpDocu(strlen ($_POST['ruc'])==8?'DNI':"RUC");
    $cotizacionDao->setCotiTpMoneda($_POST['moneda']);
    $cotizacionDao->setCotiTpPago($_POST['tipopago']);
    $cotizacionDao->setIdAgencia('');
    $cotizacionDao->setIdCliente($_POST['id']);
    $cotizacionDao->setIdDireccion($_POST['idDireccion']);
    $cotizacionDao->setTipoCostoProd($_POST['tipreciopro']);
    $cotizacionDao->setEstado($_POST['estcot']);
    if ($cotizacionDao->actualizar()){
        $respuesta['res'] = true;
        $respuesta['idCoti'] = $cotizacionDao->getCotiId();
    }
}



echo json_encode($respuesta);
