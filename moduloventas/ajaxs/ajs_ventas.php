<?php
require "../dao/VentaFacturacionDao.php";
require "../dao/VentaDetalleDao.php";
require "../dao/DocumentoVentaNumeroDao.php";


$ventaFacturacionDao = new VentaFacturacionDao();
$ventaDetalleDao= new VentaDetalleDao();
$documentoVentaNumeroDao= new DocumentoVentaNumeroDao();

$tipo = filter_input(INPUT_POST, 'tipo');

$respuesta = array("res"=>false);


if ($tipo=="i"){

    $ListIdFacturas = "";

    $productosVenta = json_decode($_POST['productosV']);
    $productoEmpresa = json_decode($_POST['prodEmpresas']);
   // $obsEmpresa = json_decode($_POST['obsVenta']);

    $tempoProdArr =[];
    //print_r($productoEmpresa);
    $respuesta["res"]=true;
    foreach ($productoEmpresa as $emprPr){

        $documentoVentaNumeroDao->setIdDocEmp(strlen($_POST['ruc'])==8?3:1);
        $documentoVentaNumeroDao->setIdEmpre($emprPr->idempresa);
        $res_doc_venta = $documentoVentaNumeroDao->getData()->fetch_assoc();

        $ventaFacturacionDao->setCotiId($_POST['idcotizacion']);
        $ventaFacturacionDao->setCliNdoc($_POST['ruc']);
        $ventaFacturacionDao->setDireccion($_POST['idDireccion']);
        // $ventaFacturacionDao->setCliNdoc('');
        $ventaFacturacionDao->setEmpId($emprPr->idempresa);
        $ventaFacturacionDao->setTipDocuSunat(strlen($_POST['ruc'])==8?3:1);
        $ventaFacturacionDao->setFacDiasc($_POST['cantidaddias']);
        $ventaFacturacionDao->setFacEstatus($_POST['formapago']==2?"PAGADA":"PENDIENTE");
        $ventaFacturacionDao->setFacFechae($_POST['fechaVenta']);
        $ventaFacturacionDao->setFacFechav($_POST['fechaVentaVen']);
        $ventaFacturacionDao->setFacHora($_POST['horaVenta']);
        $ventaFacturacionDao->setFacNcontrol('');
        $ventaFacturacionDao->setFacNum($res_doc_venta['numero']);
        $ventaFacturacionDao->setFacSerie($res_doc_venta['serie']);
        $ventaFacturacionDao->setFacTotal($emprPr->totalVen);
        $ventaFacturacionDao->setFechaA('');
        $ventaFacturacionDao->setPagForma($_POST['tipopago']);
        $ventaFacturacionDao->setPagId($_POST['formapago']);
        $ventaFacturacionDao->setSunId($_POST['moneda']);
        $ventaFacturacionDao->setTasId($_POST['tasacambio']);
        $ventaFacturacionDao->setFacCostListId($_POST['tipreciopro']);
        $ventaFacturacionDao->setObservaciones($emprPr->obs);

        if ($ventaFacturacionDao->insertar()){

            $ListIdFacturas =$ListIdFacturas. $ventaFacturacionDao->getFacId() . "-";

            $ventaDetalleDao->setFacId($ventaFacturacionDao->getFacId() );

            foreach ($emprPr->productos as $rowPro){
                $ventaDetalleDao->setProdId($rowPro->idProdEmpr);
                $ventaDetalleDao->setFacdCantidad($rowPro->cantidad);
                $ventaDetalleDao->setFacdDescuento($rowPro->descuento);
                $ventaDetalleDao->setFacdPreciou($rowPro->precio);
                $ventaDetalleDao->setFacdSubtotal($rowPro->subtotal);
                $ventaDetalleDao->setEstado($rowPro->estado);
                $ventaDetalleDao->setOrigen($rowPro->origen);
                if (!$ventaDetalleDao->insertar()){
                    $respuesta['res'] = false;

                }
            }


        }else{

            $respuesta["res"]=false;
        }

        if ($respuesta['res']){
            if ($_POST['idcotizacion']!=0){
                $ventaFacturacionDao->exeSql("UPDATE  sys_cotizacion SET estado = '2' WHERE coti_id =".$_POST['idcotizacion']);
            }
        }
    }
   /* foreach ($productosVenta as $pr){
        if(count($tempoProdArr)>0){
            $vali=true;
            for($in=0;$in<count($tempoProdArr);$in++){
                if (count($tempoProdArr[$in])>0){
                    if ($tempoProdArr[$in]['empresa']==$pr->idempresa){
                        $tempoProdArr[$in]['prductos'][]=$pr;
                        $vali=false;
                    }
                }

            }
            if ($vali){
                $tempoProdArr[]=array("empresa"=>$pr->idempresa,"prductos"=>array($pr));
            }
        }else{
            $tempoProdArr[]=array("empresa"=>$pr->idempresa,"prductos"=>array($pr));
        }
    }
    $respuesta["res"]=true;
    for($i=0; $i<count($tempoProdArr);$i++){
        $subTotal =0;
        foreach ($tempoProdArr[$i]['prductos'] as $rowTepPRod){
            $subTotal+= $rowTepPRod->subtotal;
        }
        $igvPRo= $subTotal * 0.18;

        $tempoProdArr[$i]['total']=number_format($igvPRo+$subTotal, 2, '.', '');

    }

    foreach ($tempoProdArr as $worRegister){
         $documentoVentaNumeroDao->setIdDocEmp(strlen($_POST['ruc'])==8?3:1);
            $documentoVentaNumeroDao->setIdEmpre($worRegister['empresa']);
            $res_doc_venta = $documentoVentaNumeroDao->getData()->fetch_assoc();
            $ventaFacturacionDao->setCotiId($_POST['idcotizacion']);
            $ventaFacturacionDao->setCliNdoc($_POST['ruc']);
           // $ventaFacturacionDao->setCliNdoc('');
            $ventaFacturacionDao->setEmpId($worRegister['empresa']);
            $ventaFacturacionDao->setTipDocuSunat(strlen($_POST['ruc'])==8?3:1);
            $ventaFacturacionDao->setFacDiasc($_POST['cantidaddias']);
            $ventaFacturacionDao->setFacEstatus($_POST['formapago']==2?"PAGADA":"PENDIENTE");
            $ventaFacturacionDao->setFacFechae($_POST['fechaVenta']);
            $ventaFacturacionDao->setFacFechav($_POST['fechaVentaVen']);
            $ventaFacturacionDao->setFacHora($_POST['horaVenta']);
            $ventaFacturacionDao->setFacNcontrol('');
            $ventaFacturacionDao->setFacNum($res_doc_venta['numero']);
            $ventaFacturacionDao->setFacSerie($res_doc_venta['serie']);
            $ventaFacturacionDao->setFacTotal($worRegister['total']);
            $ventaFacturacionDao->setFechaA('');
            $ventaFacturacionDao->setPagForma($_POST['tipopago']);
            $ventaFacturacionDao->setPagId($_POST['formapago']);
            $ventaFacturacionDao->setSunId($_POST['moneda']);
            $ventaFacturacionDao->setTasId($_POST['tasacambio']);
            $ventaFacturacionDao->setFacCostListId($_POST['tipreciopro']);

            if ($ventaFacturacionDao->insertar()){

                $ListIdFacturas =$ListIdFacturas. $ventaFacturacionDao->getFacId() . "-";

                $ventaDetalleDao->setFacId($ventaFacturacionDao->getFacId() );

                foreach ($worRegister['prductos'] as $rowPro){
                    $ventaDetalleDao->setProdId($rowPro->idProdEmpr);
                    $ventaDetalleDao->setFacdCantidad($rowPro->cantidad);
                    $ventaDetalleDao->setFacdDescuento($rowPro->descuento);
                    $ventaDetalleDao->setFacdPreciou($rowPro->precio);
                    $ventaDetalleDao->setFacdSubtotal($rowPro->subtotal);
                    $ventaDetalleDao->setEstado($rowPro->estado);
                    $ventaDetalleDao->setOrigen($rowPro->origen);
                    if (!$ventaDetalleDao->insertar()){
                        $respuesta['res'] = false;

                    }
                }


            }else{
                $respuesta["res"]=false;
            }
    }
*/
    $respuesta['idVentas'] = $ListIdFacturas;

    if (isset($_POST['isempre'])){
        if ($_POST['isempre']){
            $NumDoc= $_POST['empre'];
            $sql = "SELECT
                      emp_id
                    FROM sys_empresas WHERE emp_ruc = '$NumDoc'";
            $resulEmpreSe = $ventaDetalleDao->exeSql($sql)->fetch_assoc();
            $idEmpresaVe=$resulEmpreSe['emp_id'];
            foreach ($productosVenta as $pr2){
                $sql = "SELECT 
                          * 
                        FROM
                          sys_producto_empresa 
                        WHERE     id_empresa = '".$idEmpresaVe."'  AND id_prod = ".$pr2->id;
                $res = $ventaDetalleDao->exeSql($sql);
                if ($row = $res->fetch_assoc()){

                    $sql ="UPDATE sys_producto_empresa
                            SET 
                              cantidad = cantidad + ".$pr2->cantidad."
                            WHERE prod_empre_id = ".$row['prod_empre_id'];
                }else{

                    $idEMpredaP = 6;

                    $sql = "INSERT INTO sys_producto_empresa
                        VALUES (null,
                                '".$pr2->id."',
                                '',
                                '$idEmpresaVe',
                                '".$pr2->cantidad."',
                                '1'); ";
                    $ventaDetalleDao->exeSql($sql);
                }

            }

        }
    }




}elseif ($tipo=="s"){
    $ventaFacturacionDao->setFacId(filter_input(INPUT_POST, 'idVent'));
    $ventaDetalleDao->setFacId($ventaFacturacionDao->getFacId());
    $resulventa=$ventaFacturacionDao->getdataVenta();
    $respuesta = [];
    if ($rowVenta = $resulventa->fetch_assoc()){
        $rowVenta['productos']=[];
        $respProd = $ventaDetalleDao->getProductos();
        foreach ($respProd as $rowPr){
            $rowVenta['productos'][] = $rowPr;
        }
        $respuesta = $rowVenta;
    }


}elseif ($tipo=="ifv"){
    $ventaFacturacionDao->setFacId(filter_input(INPUT_POST, 'idCoti'));
    $resulventa=$ventaFacturacionDao->getdataVenta();
    $respuesta = [];
    if ($rowVenta = $resulventa->fetch_assoc()){
        $respuesta = $rowVenta;
    }
}


echo  json_encode($respuesta);



