<?php
require "../dao/GuiaRemisionDao.php";
require "../dao/GuiaRemisionDetalleDao.php";
require "../dao/DocumentoVentaNumeroDao.php";
require "../dao/GuiaConductorDao.php";

$guiaRemisionDao=new GuiaRemisionDao();
$guiaRemisionDetalleDao=new GuiaRemisionDetalleDao();
$documentoVentaNumeroDao= new DocumentoVentaNumeroDao();
$guiaConductorDao= new GuiaConductorDao();


$tipo = filter_input(INPUT_POST, 'tipo');

$respuesta = array("res"=>false);

if ($tipo =="i"){
    $documentoVentaNumeroDao->setIdDocEmp(7);
    $documentoVentaNumeroDao->setIdEmpre($_POST['empresa']);
    $res_doc_venta = $documentoVentaNumeroDao->getData()->fetch_assoc();

    $productos = json_decode($_POST['productosG']);
    $conductores = json_decode($_POST['conductoresG']);

    $guiaRemisionDao->setClienteId($_POST['idClie']);
    $guiaRemisionDao->setEmpresaId($_POST['empresa']);
    $guiaRemisionDao->setFecha($_POST['fecha']);
    $guiaRemisionDao->setSerie($res_doc_venta['serie']);
    $guiaRemisionDao->setNumero($res_doc_venta['numero']);
    $guiaRemisionDao->setDireccion($_POST['idDireccion']);
    $guiaRemisionDao->setMotivo($_POST['motivo']);
    $guiaRemisionDao->setAgenciaId($_POST['agenciaid']);
    $guiaRemisionDao->setTipoTransporte($_POST['tpTrans']);
    $guiaRemisionDao->setPesoCargamento($_POST['peso']);
    $guiaRemisionDao->setFactRela($_POST['factura']);
    $guiaRemisionDao->setNumPlaca($_POST['numPlaca']);
    $guiaRemisionDao->setMarca($_POST['marcaCar']);
    $guiaRemisionDao->setModelo($_POST['modeloCar']);
    $guiaRemisionDao->setDocConduc(1);
    $guiaRemisionDao->setNunDocConduc('');
    $guiaRemisionDao->setNumLicen('');
    $guiaRemisionDao->setEstado('1');

    if ($guiaRemisionDao->insertar()){
        $respuesta['res']=true;
        $respuesta['gia']=$guiaRemisionDao->getGuiReId();
        $guiaRemisionDetalleDao->setGuiReId($guiaRemisionDao->getGuiReId());
        $guiaConductorDao->setGuiaRemiId($guiaRemisionDetalleDao->getGuiReId());
        foreach ($conductores as $prod){
            $guiaConductorDao->setTipoDoc($prod->docAgencia);
            $guiaConductorDao->setNumeroDocumento($prod->numDocConduc);
            $guiaConductorDao->setNumeroLicencia($prod->numLicenConduc);
            $guiaConductorDao->insertar();
        }
        foreach ($productos as $prod){
            $guiaRemisionDetalleDao->setCantidad($prod->cantidad);
            $guiaRemisionDetalleDao->setProduId($prod->idProdEmpr);
            $guiaRemisionDetalleDao->setUnidad($prod->medida);
            if (!$guiaRemisionDetalleDao->insertar()){
                $respuesta['res']=false;
            }
        }
    }

}

echo json_encode($respuesta);
