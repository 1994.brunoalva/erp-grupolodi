<?php
require "../dao/ClienteDao.php";

$clienteDao = new ClienteDao();

$searchTerm = filter_input(INPUT_GET, 'term');

$result = $clienteDao->getdataBuscar($searchTerm);

$arrRes=array();

foreach ($result as $row){
    $row['value']=$row['cli_ndoc'] . ' | ' . $row['cli_nomape'];
    $arrRes []= $row;
}
echo  json_encode($arrRes);


