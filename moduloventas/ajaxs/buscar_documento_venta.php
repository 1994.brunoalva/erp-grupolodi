<?php
require_once "../conexion/Conexion.php";
require_once "../dao/VentaDetalleDao.php";
$conexion = (new Conexion())->getConexion();

$ventaDetalleDao= new VentaDetalleDao();
$idDoc= $_POST['doc'];
$idEmpr= $_POST['nunDoc'];
$numDoc= $_POST['empre'];


$sql="SELECT * FROM sys_ven_facturacion WHERE tip_docu_sunat = $idDoc AND fac_num=$idEmpr AND emp_id=$numDoc";

$resul = $conexion->query($sql);
$resultado = array('res'=>false);
if ($row = $resul->fetch_assoc()){
    $ventaDetalleDao->setFacId($row['fac_id']);

    $respProd = $ventaDetalleDao->getProductos();
    $row['productos']=[];
    foreach ($respProd as $rowPr){
        $row['productos'][] = $rowPr;
    }


    $resultado['res']=true;
    $resultado['data']=$row;
}

echo json_encode($resultado);
