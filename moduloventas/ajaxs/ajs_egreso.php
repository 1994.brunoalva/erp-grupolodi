<?php

require "../dao/EgresosDao.php";

$egresosDao= new EgresosDao();

$tipo = filter_input(INPUT_POST, 'tipo');

$respuesta = array("res"=>false);

if ($tipo =="i"){
    $egresosDao->setEmpId($_POST['empresa']);
    $egresosDao->setSunId($_POST['moneda']);
    $egresosDao->setEgrDescrip($_POST['detalle']);
    $egresosDao->setEgrEstatus('RECIBIDO');
    $egresosDao->setEgrFecha($_POST['fecha']);
    $egresosDao->setEgrMonto($_POST['monto']);
    $egresosDao->setEgrNumope('0');
    $egresosDao->setEgrNumref($_POST['numero']);
    $egresosDao->setEgrTipo('CC');
    $egresosDao->setEgrTref($_POST['doc']);
    if ($egresosDao->insertar()){
        $respuesta['res'] = true;
    }

}elseif ($tipo =="s"){
    $egresosDao->setEgrId($_POST['egreso']);
    $res = $egresosDao->getData();
    if ($row= $res->fetch_assoc()){
        $respuesta = $row;
    }
}elseif ($tipo =="d"){
    $egresosDao->setEgrId($_POST['egreso']);
    if ($egresosDao->eliminar()){
        $respuesta['res']=true;
    }
}




echo  json_encode($respuesta);







