<?php
require "../dao/NotaElectronicaDao.php";
require "../dao/NotaDetalleDao.php";
require "../dao/VentaFacturacionDao.php";
require "../dao/VentaDetalleDao.php";
require "../dao/ProductoDao.php";
require "../dao/ClienteSaldoDao.php";
require "../dao/ClienteSaldoMovimientoDao.php";
require "../dao/ClienteDao.php";

$notaElectronicaDao=new NotaElectronicaDao();
$notaDetalleDao= new NotaDetalleDao();
$ventaFacturacionDao = new VentaFacturacionDao();
$ventaDetalleDao= new VentaDetalleDao();
$productoDao = new ProductoDao();
$clienteSaldoDao= new ClienteSaldoDao();
$clienteSaldoMovimientoDao= new ClienteSaldoMovimientoDao();
$clienteDao=new ClienteDao();

$tipo = filter_input(INPUT_POST, 'tipo');

$respuesta = array("res"=>false);

if ($tipo =="i"){

    $productos = json_decode($_POST['productos']);

    $notaElectronicaDao->setEmpId($_POST['idEmpresa']);
    $notaElectronicaDao->setFacId($_POST['idVenta']);
    $notaElectronicaDao->setNotSn($_POST['serieNumero']);
    $notaElectronicaDao->setSunId($_POST['moneda']);
    $notaElectronicaDao->setCliNdoc($_POST['cli_ndoc']);
    $notaElectronicaDao->setNotCodMotivo($_POST['motivo']);
    $notaElectronicaDao->setAsigId('0');
    $notaElectronicaDao->setNotcDescripcion($_POST['nombreMotivo']);

    $notaElectronicaDao->setNotcFecha($_POST['fecha']);
    $notaElectronicaDao->setNotcDisponible(0);
    $notaElectronicaDao->setNotcMonto($_POST['total']);
    $notaElectronicaDao->setNotcTipo($_POST['tipDoc']);
    $notaElectronicaDao->setNotcUsado('0');
    $notaElectronicaDao->setNotTipoDoc($_POST['tipoD']);
    $notaElectronicaDao->setNotaSustento($_POST['sustento']);
    if ($notaElectronicaDao->getNotTipoDoc()==5){
        $notaElectronicaDao->setNotcEstatus('APROBADO');
    }else{
        $notaElectronicaDao->setNotcEstatus('POR COBRAR');
    }

    if ($notaElectronicaDao->insertar()){
        $respuesta['res'] = true;
        $respuesta['id'] = $notaElectronicaDao->getNotcId();
        $notaDetalleDao->setNotaId( $notaElectronicaDao->getNotcId());
        foreach ($productos as $pro){
            $notaDetalleDao->setCatidad($pro->cantidad);
            $notaDetalleDao->setDescripcion($pro->descrip);
            $notaDetalleDao->setMedida($pro->medida);
            $notaDetalleDao->setPrecio($pro->precio_unitario);
            if (!$notaDetalleDao->insertar()){
                $respuesta['res'] = false;
            }
        }
    }

    if ($notaElectronicaDao->getNotTipoDoc()==5){
        $tipoNota=$notaElectronicaDao->getNotCodMotivo();
        if ($tipoNota == '01' || $tipoNota == '02' ){
            $idFact = $notaElectronicaDao->getFacId();
            $ventaFacturacionDao->setFacId($idFact);
            $ventaFacturacionDao->setFechaA($_POST['fecha']);

            if ($ventaFacturacionDao->anularVenta()){
                $ventaDetalleDao->setFacId($idFact);

                $resulDP = $ventaDetalleDao->getProductos();
                foreach ($resulDP as $pr){
                    $productoDao->setCantidad($pr['facd_cantidad']);
                    $productoDao->setProdEmpreId($pr['prod_id']);
                    $productoDao->addStock();
                }
            }

        }elseif ($tipoNota == '08' ){
            $clienteSaldoDao->setClieteId($notaElectronicaDao->getCliNdoc());
            $resulCli = $clienteSaldoDao->getData();
            $clienteSaldoDao->setClieSaldo($notaElectronicaDao->getNotcMonto());
            if ($rowC = $resulCli->fetch_assoc()){
                $clienteSaldoDao->setCliSalId($rowC['cli_sal_id']);
                $clienteSaldoDao->addSaldo();

                $clienteSaldoMovimientoDao->setClieSalId($clienteSaldoDao->getCliSalId());
                $clienteSaldoMovimientoDao->setTipoDoc('NC');
                $clienteSaldoMovimientoDao->setDetalle("Ingreso de saldo por Nota de Crédito: " . $_POST['serieNumero']);
                $clienteSaldoMovimientoDao->setMonto($notaElectronicaDao->getNotcMonto());
                $clienteSaldoMovimientoDao->setNumRef($_POST['serieNumero']);
                $clienteSaldoMovimientoDao->setTipoMov("E");
                $clienteSaldoMovimientoDao->insertar();

            }else{
                $clienteDao->setCliNdoc($notaElectronicaDao->getCliNdoc());
                $resulClie= $clienteDao->getdataByNumDc();
                if ($rowClie = $resulClie->fetch_assoc()){
                    $clienteSaldoDao->setClieteId($rowClie['cli_id']);
                    $clienteSaldoDao->setEstado("ACTIVO");
                    if ($clienteSaldoDao->insertar()){
                        $clienteSaldoMovimientoDao->setClieSalId($clienteSaldoDao->getCliSalId());
                        $clienteSaldoMovimientoDao->setTipoDoc('NC');
                        $clienteSaldoMovimientoDao->setDetalle("Ingreso de saldo por Nota de Crédito: " . $_POST['serieNumero']);
                        $clienteSaldoMovimientoDao->setMonto($notaElectronicaDao->getNotcMonto());
                        $clienteSaldoMovimientoDao->setNumRef($_POST['serieNumero']);
                        $clienteSaldoMovimientoDao->setTipoMov("E");
                        $clienteSaldoMovimientoDao->insertar();
                    }
                }

            }

        }else{

        }

    }



}

echo  json_encode($respuesta);

