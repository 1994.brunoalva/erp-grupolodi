<?php
require "../dao/AgenciaTransporteDao.php";

$agenciaTransporteDao= new AgenciaTransporteDao();

$searchTerm = filter_input(INPUT_GET, 'term');

$resul = $agenciaTransporteDao->buscar($searchTerm);

$arrRes=array();

foreach ($resul as $row){
    $row['value']=$row['ruc'] . ' | ' . $row['razon_social'];
    $arrRes []= $row;
}
echo  json_encode($arrRes);



