<?php
require "../dao/AjustesProductoDao.php";
require "../dao/AjustesProductoDetalleDao.php";
require "../dao/ProductoDao.php";
require "../dao/ProductoMovimientoDao.php";

$ajustesProductoDao=new AjustesProductoDao();
$ajustesProductoDetalleDao= new AjustesProductoDetalleDao();
$productoDao= new ProductoDao();
$productoMovimientoDao = new ProductoMovimientoDao();

$tipo = filter_input(INPUT_POST, 'tipo');

$respuesta = array("res"=>false);

if ($tipo =="i"){

    $produtosAfec = json_decode($_POST['productos']);

    $ajustesProductoDao->setFecha($_POST['fecha']);
    $ajustesProductoDao->setMotivo($_POST['motivo']);
    $ajustesProductoDao->setCntProd($_POST['cnt_prod']);
    $ajustesProductoDao->setEmpId($_POST['empresa']);
    if ($ajustesProductoDao->insertar()){
        $respuesta['res'] = true;

        $ajustesProductoDetalleDao->setAjusId($ajustesProductoDao->getAjusId());
        foreach ($produtosAfec as $prod){

            $ajustesProductoDetalleDao->setEstado('1');
            $ajustesProductoDetalleDao->setCntActual($prod->cnt);
            $ajustesProductoDetalleDao->setNuevoCnt($prod->catidad_nueva);
            $ajustesProductoDetalleDao->setProdEmprId($prod->idProdEmpr);
            if (!$ajustesProductoDetalleDao->insertar()){
                $respuesta['res'] = false;
            }

        }
    }

    if ($respuesta['res']){
        $respuesta['resSub'] = true;
        foreach ($produtosAfec as $prod){
            $productoDao->setCantidad($prod->catidad_nueva);
            $productoDao->setProdEmpreId($prod->idProdEmpr);
            if (!$productoDao->actualizarStock()){
                $respuesta['resSub'] = false;
            }
            $cantidadIS = $prod->catidad_nueva - $prod->cnt;
            $tipoMov = "E";
            if ($cantidadIS<0){
                $cantidadIS = $cantidadIS * -1;
                $tipoMov = "S";
            }
            $productoMovimientoDao->setEstado(1);
            $productoMovimientoDao->setMovCantidad($cantidadIS);
            $productoMovimientoDao->setMovDescripcion("Movimiento por ajuste de inventario motivo: ". $_POST['motivo']);
            $productoMovimientoDao->setMovFecha($_POST['fecha']);
            $productoMovimientoDao->setMovTipo($tipoMov);
            $productoMovimientoDao->setProEmpId($prod->idProdEmpr);
            $productoMovimientoDao->insertar();
        }
    }
}elseif ($tipo =="s"){
    $idajuste= $_POST['idajuste'];

    $ajustesProductoDao->setAjusId($idajuste);
    $ajustesProductoDetalleDao->setAjusId($idajuste);
    $respuesta=[];
    $resulA = $ajustesProductoDao->getData();
    if ($rowA = $resulA->fetch_assoc()){
       $arrayTemp = [];
        $resAPD = $ajustesProductoDetalleDao->getData();
       foreach ($resAPD as $rowAPD){
           $arrayTemp[]= $rowAPD;
       }
        $rowA['productos'] = $arrayTemp;
        $respuesta = $rowA;
    }

}

echo json_encode($respuesta);