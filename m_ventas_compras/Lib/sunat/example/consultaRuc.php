<?php
	header('Content-Type: application/json');

	require ("../src/autoload.php");
	$cookie = array(
		'cookie' 		=> array(
			'use' 		=> true,
			'file' 		=> __DIR__ . "/cookie.txt"
		)
	);
	$config = array(
		'representantes_legales' 	=> true,
		'cantidad_trabajadores' 	=> true,
		'establecimientos' 			=> true,
		'cookie' 					=> $cookie
	);
	$company = new \Sunat\ruc( $config );
	
	$ruc =$_POST['valor'];

	$result = $company->consulta( $ruc );
	
	echo json_encode($result,JSON_PRETTY_PRINT);
?>
