<?php


class Pagos extends DataBase
{
    private $pago_id;
    private $folder_id;
    private $monto_total;
    private $monto_pendiente;

    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_pago';
        parent::__construct($this->accion, $this->table, 'pago_id');
    }

    /**
     * @return mixed
     */
    public function getPagoId()
    {
        return $this->pago_id;
    }

    /**
     * @param mixed $pago_id
     */
    public function setPagoId($pago_id)
    {
        $this->pago_id = $pago_id;
    }

    /**
     * @return mixed
     */
    public function getFolderId()
    {
        return $this->folder_id;
    }

    /**
     * @param mixed $folder_id
     */
    public function setFolderId($folder_id)
    {
        $this->folder_id = $folder_id;
    }

    /**
     * @return mixed
     */
    public function getMontoTotal()
    {
        return $this->monto_total;
    }

    /**
     * @param mixed $monto_total
     */
    public function setMontoTotal($monto_total)
    {
        $this->monto_total = $monto_total;
    }

    /**
     * @return mixed
     */
    public function getMontoPendiente()
    {
        return $this->monto_pendiente;
    }

    /**
     * @param mixed $monto_pendiente
     */
    public function setMontoPendiente($monto_pendiente)
    {
        $this->monto_pendiente = $monto_pendiente;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}