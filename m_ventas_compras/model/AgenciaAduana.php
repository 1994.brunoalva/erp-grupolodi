<?php


class AgenciaAduana extends DataBase
{
    private $agencia_id;
    private $codigo;
    private $nombre;
    private $jurisdiccion;
    private $jurisdicion;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_agencia_aduana';
        parent::__construct($this->accion, $this->table, 'age_adua_id');
    }

    /**
     * @return mixed
     */
    public function getAgenciaId()
    {
        return $this->agencia_id;
    }

    /**
     * @param mixed $agencia_id
     */
    public function setAgenciaId($agencia_id)
    {
        $this->agencia_id = $agencia_id;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getJurisdiccion()
    {
        return $this->jurisdiccion;
    }

    /**
     * @param mixed $jurisdiccion
     */
    public function setJurisdiccion($jurisdiccion)
    {
        $this->jurisdiccion = $jurisdiccion;
    }

    /**
     * @return mixed
     */
    public function getJurisdicion()
    {
        return $this->jurisdicion;
    }

    /**
     * @param mixed $jurisdicion
     */
    public function setJurisdicion($jurisdicion)
    {
        $this->jurisdicion = $jurisdicion;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}