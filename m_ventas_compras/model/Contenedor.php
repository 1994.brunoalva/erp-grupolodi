<?php


class Contenedor extends DataBase
{
    private $contenedor_id;
    private $folder_id;
    private $codigo;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_contenedor';
        parent::__construct($this->accion, $this->table, 'con_id');
    }

    /**
     * @return mixed
     */
    public function getContenedorId()
    {
        return $this->contenedor_id;
    }

    /**
     * @param mixed $contenedor_id
     */
    public function setContenedorId($contenedor_id)
    {
        $this->contenedor_id = $contenedor_id;
    }

    /**
     * @return mixed
     */
    public function getFolderId()
    {
        return $this->folder_id;
    }

    /**
     * @param mixed $folder_id
     */
    public function setFolderId($folder_id)
    {
        $this->folder_id = $folder_id;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}