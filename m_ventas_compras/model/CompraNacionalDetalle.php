<?php


class CompraNacionalDetalle
{
    private $com_detalle_id;
    private $com_naci_id;
    private $prod_id;
    private $cantidad;
    private $precio_u;
    private $importe;


    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        $sql ="INSERT INTO sys_com_nac_detalle
VALUES (null,
        '$this->com_naci_id',
        '$this->prod_id',
        '$this->cantidad',
        '$this->precio_u',
        '$this->importe');";
        return $this->conexion->query($sql);
    }
    public function getDatos(){

        $sql ="SELECT 
              nac_det.*,
              prod.produ_nombre,
               marca.mar_nombre
            FROM
              sys_com_nac_detalle AS nac_det 
              INNER JOIN sys_producto AS prod ON nac_det.prod_id = prod.produ_id
              INNER JOIN sys_com_marca AS marca ON prod.mar_id = marca.mar_id
            WHERE nac_det.com_naci_id =" . $this->com_naci_id;
        return $this->conexion->query($sql);

    }
    /**
     * @return mixed
     */
    public function getComDetalleId()
    {
        return $this->com_detalle_id;
    }

    /**
     * @param mixed $com_detalle_id
     */
    public function setComDetalleId($com_detalle_id)
    {
        $this->com_detalle_id = $com_detalle_id;
    }

    /**
     * @return mixed
     */
    public function getComNaciId()
    {
        return $this->com_naci_id;
    }

    /**
     * @param mixed $com_naci_id
     */
    public function setComNaciId($com_naci_id)
    {
        $this->com_naci_id = $com_naci_id;
    }

    /**
     * @return mixed
     */
    public function getProdId()
    {
        return $this->prod_id;
    }

    /**
     * @param mixed $prod_id
     */
    public function setProdId($prod_id)
    {
        $this->prod_id = $prod_id;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return mixed
     */
    public function getPrecioU()
    {
        return $this->precio_u;
    }

    /**
     * @param mixed $precio_u
     */
    public function setPrecioU($precio_u)
    {
        $this->precio_u = $precio_u;
    }

    /**
     * @return mixed
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * @param mixed $importe
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    }

}