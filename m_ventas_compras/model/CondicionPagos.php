<?php


class CondicionPagos extends DataBase
{
    private $condicion_id;
    private $nombre;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_condicion_pago';
        parent::__construct($this->accion, $this->table, 'conpa_id');
    }

    /**
     * @return mixed
     */
    public function getCondicionId()
    {
        return $this->condicion_id;
    }

    /**
     * @param mixed $condicion_id
     */
    public function setCondicionId($condicion_id)
    {
        $this->condicion_id = $condicion_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}