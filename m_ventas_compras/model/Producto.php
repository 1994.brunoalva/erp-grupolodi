<?php


class Producto extends DataBase
{
    private $produ_id;
    private $proveedor_id;
    private $unidad_id;
    private $cod_sunat_id;
    private $nombre;
    private $sku;
    private $part_number;
    private $tipo;
    private $nomenglatura;
    private $categoria_id;
    private $marca_id;
    private $modelo_id;
    private $pais_id;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_producto';
        parent::__construct($this->accion, $this->table, 'produ_id');
    }

    /**
     * @return mixed
     */
    public function getProduId()
    {
        return $this->produ_id;
    }

    /**
     * @param mixed $produ_id
     */
    public function setProduId($produ_id)
    {
        $this->produ_id = $produ_id;
    }

    /**
     * @return mixed
     */
    public function getProveedorId()
    {
        return $this->proveedor_id;
    }

    /**
     * @param mixed $proveedor_id
     */
    public function setProveedorId($proveedor_id)
    {
        $this->proveedor_id = $proveedor_id;
    }

    /**
     * @return mixed
     */
    public function getUnidadId()
    {
        return $this->unidad_id;
    }

    /**
     * @param mixed $unidad_id
     */
    public function setUnidadId($unidad_id)
    {
        $this->unidad_id = $unidad_id;
    }

    /**
     * @return mixed
     */
    public function getCodSunatId()
    {
        return $this->cod_sunat_id;
    }

    /**
     * @param mixed $cod_sunat_id
     */
    public function setCodSunatId($cod_sunat_id)
    {
        $this->cod_sunat_id = $cod_sunat_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getPartNumber()
    {
        return $this->part_number;
    }

    /**
     * @param mixed $part_number
     */
    public function setPartNumber($part_number)
    {
        $this->part_number = $part_number;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getNomenglatura()
    {
        return $this->nomenglatura;
    }

    /**
     * @param mixed $nomenglatura
     */
    public function setNomenglatura($nomenglatura)
    {
        $this->nomenglatura = $nomenglatura;
    }

    /**
     * @return mixed
     */
    public function getCategoriaId()
    {
        return $this->categoria_id;
    }

    /**
     * @param mixed $categoria_id
     */
    public function setCategoriaId($categoria_id)
    {
        $this->categoria_id = $categoria_id;
    }

    /**
     * @return mixed
     */
    public function getMarcaId()
    {
        return $this->marca_id;
    }

    /**
     * @param mixed $marca_id
     */
    public function setMarcaId($marca_id)
    {
        $this->marca_id = $marca_id;
    }

    /**
     * @return mixed
     */
    public function getModeloId()
    {
        return $this->modelo_id;
    }

    /**
     * @param mixed $modelo_id
     */
    public function setModeloId($modelo_id)
    {
        $this->modelo_id = $modelo_id;
    }

    /**
     * @return mixed
     */
    public function getPaisId()
    {
        return $this->pais_id;
    }

    /**
     * @param mixed $pais_id
     */
    public function setPaisId($pais_id)
    {
        $this->pais_id = $pais_id;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}