<?php


class Puerto extends DataBase
{
    private $puerto_id;
    private $nombre;
    private $pais_id;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_puerto';
        parent::__construct($this->accion, $this->table, 'puerto_id');
    }

    /**
     * @return mixed
     */
    public function getPuertoId()
    {
        return $this->puerto_id;
    }

    /**
     * @param mixed $puerto_id
     */
    public function setPuertoId($puerto_id)
    {
        $this->puerto_id = $puerto_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getPaisId()
    {
        return $this->pais_id;
    }

    /**
     * @param mixed $pais_id
     */
    public function setPaisId($pais_id)
    {
        $this->pais_id = $pais_id;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}