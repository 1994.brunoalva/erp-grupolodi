<?php


class TipoDocumento extends DataBase
{
    private $documento_id;
    private $nombre;
    private $descripccion;
    private $estado;


    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_adm_documentos';
        parent::__construct($this->accion, $this->table, 'doc_id');
    }

    /**
     * @return mixed
     */
    public function getDocumentoId()
    {
        return $this->documento_id;
    }

    /**
     * @param mixed $documento_id
     */
    public function setDocumentoId($documento_id)
    {
        $this->documento_id = $documento_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getDescripccion()
    {
        return $this->descripccion;
    }

    /**
     * @param mixed $descripccion
     */
    public function setDescripccion($descripccion)
    {
        $this->descripccion = $descripccion;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}