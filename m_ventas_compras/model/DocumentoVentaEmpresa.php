<?php


class DocumentoVentaEmpresa
{
    private $id;
    private $id_doc_emp;
    private $id_empre;
    private $serie;
    private $numero;


    private $conexion;

    /**
     * DocumentoVentaEmpresa constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdDocEmp()
    {
        return $this->id_doc_emp;
    }

    /**
     * @param mixed $id_doc_emp
     */
    public function setIdDocEmp($id_doc_emp)
    {
        $this->id_doc_emp = $id_doc_emp;
    }

    /**
     * @return mixed
     */
    public function getIdEmpre()
    {
        return $this->id_empre;
    }

    /**
     * @param mixed $id_empre
     */
    public function setIdEmpre($id_empre)
    {
        $this->id_empre = $id_empre;
    }

    /**
     * @return mixed
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * @param mixed $serie
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    public function getData(){
        $sql = "SELECT *
        FROM sys_documento_empresa
        WHERE id_empre = $this->id_empre AND id_doc_emp=".$this->id_doc_emp;
        return $this->conexion->query($sql);
    }



}