<?php


class DetalleVenta
{
    public $id;
    public $id_venta;
    public $id_coti_deta;
    public $estado;

    private $conexion;


    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdVenta()
    {
        return $this->id_venta;
    }

    /**
     * @param mixed $id_venta
     */
    public function setIdVenta($id_venta)
    {
        $this->id_venta = $id_venta;
    }

    /**
     * @return mixed
     */
    public function getIdCotiDeta()
    {
        return $this->id_coti_deta;
    }

    /**
     * @param mixed $id_coti_deta
     */
    public function setIdCotiDeta($id_coti_deta)
    {
        $this->id_coti_deta = $id_coti_deta;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }
    public function listaProVentaresumen(){
        $sql ="SELECT 
  sys_coti_detalle.*,
  sys_producto.produ_sku,
  sys_com_marca.mar_nombre,
  sys_pais.pais_nombre
FROM
  sys_detalle_venta 
  INNER JOIN sys_coti_detalle 
    ON sys_detalle_venta.id_coti_deta = sys_coti_detalle.id 
  INNER JOIN sys_producto 
    ON sys_coti_detalle.id_producto = sys_producto.produ_id  
  INNER JOIN sys_pais 
    ON sys_producto.pais_id = sys_pais.pais_id 
  INNER JOIN sys_com_marca 
    ON sys_producto.mar_id = sys_com_marca.mar_id 
WHERE sys_detalle_venta.id_venta = ".$this->getIdVenta();
        return $this->conexion->query($sql);
    }
    public function listaProVenta(){
        $sql ="SELECT 
  sys_coti_detalle.*,
  sys_producto.produ_sku
FROM
  sys_detalle_venta 
  INNER JOIN sys_coti_detalle 
    ON sys_detalle_venta.id_coti_deta = sys_coti_detalle.id 
    INNER JOIN sys_producto ON sys_coti_detalle.id_producto = sys_producto.produ_id
WHERE sys_detalle_venta.id_venta = ".$this->getIdVenta();
        return $this->conexion->query($sql);
    }

    public function insertar(){
        $sql ="INSERT INTO sys_detalle_venta
VALUES (NULL,
        '$this->id_venta',
        '$this->id_coti_deta',
        '$this->estado');";
        return $this->conexion->query($sql);
    }


}