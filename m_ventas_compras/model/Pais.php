<?php


class Pais extends DataBase
{
    private $pais_id;
    private $nombre;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_pais';
        parent::__construct($this->accion, $this->table, 'pais_id');
    }

    /**
     * @return mixed
     */
    public function getPaisId()
    {
        return $this->pais_id;
    }

    /**
     * @param mixed $pais_id
     */
    public function setPaisId($pais_id)
    {
        $this->pais_id = $pais_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}