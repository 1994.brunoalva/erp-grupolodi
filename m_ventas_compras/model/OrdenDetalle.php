<?php


class OrdenDetalle extends DataBase
{
    private $detalle_id;
    private $orden_id;
    private $producto_id;
    private $producto_nombre;
    private $cantidad;
    private $precio;
    private $descuento;
    private $total;
    private $estado;


    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_orden_detalle';
        parent::__construct($this->accion, $this->table, 'orddeta_id');
    }

    /**
     * @return mixed
     */
    public function getDetalleId()
    {
        return $this->detalle_id;
    }

    /**
     * @param mixed $detalle_id
     */
    public function setDetalleId($detalle_id)
    {
        $this->detalle_id = $detalle_id;
    }

    /**
     * @return mixed
     */
    public function getOrdenId()
    {
        return $this->orden_id;
    }

    /**
     * @param mixed $orden_id
     */
    public function setOrdenId($orden_id)
    {
        $this->orden_id = $orden_id;
    }

    /**
     * @return mixed
     */
    public function getProductoId()
    {
        return $this->producto_id;
    }

    /**
     * @param mixed $producto_id
     */
    public function setProductoId($producto_id)
    {
        $this->producto_id = $producto_id;
    }

    /**
     * @return mixed
     */
    public function getProductoNombre()
    {
        return $this->producto_nombre;
    }

    /**
     * @param mixed $producto_nombre
     */
    public function setProductoNombre($producto_nombre)
    {
        $this->producto_nombre = $producto_nombre;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @return mixed
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * @param mixed $descuento
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}