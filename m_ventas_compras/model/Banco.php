<?php


class Banco
{
    private $ban_id;
    private $ban_sunat_id;
    private $ban_nombre;
    private $ban_estatus;

    private $conexion;

    /**
     * Banco constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getBanId()
    {
        return $this->ban_id;
    }

    /**
     * @param mixed $ban_id
     */
    public function setBanId($ban_id)
    {
        $this->ban_id = $ban_id;
    }

    /**
     * @return mixed
     */
    public function getBanSunatId()
    {
        return $this->ban_sunat_id;
    }

    /**
     * @param mixed $ban_sunat_id
     */
    public function setBanSunatId($ban_sunat_id)
    {
        $this->ban_sunat_id = $ban_sunat_id;
    }

    /**
     * @return mixed
     */
    public function getBanNombre()
    {
        return $this->ban_nombre;
    }

    /**
     * @param mixed $ban_nombre
     */
    public function setBanNombre($ban_nombre)
    {
        $this->ban_nombre = $ban_nombre;
    }

    /**
     * @return mixed
     */
    public function getBanEstatus()
    {
        return $this->ban_estatus;
    }

    /**
     * @param mixed $ban_estatus
     */
    public function setBanEstatus($ban_estatus)
    {
        $this->ban_estatus = $ban_estatus;
    }


    public function getLista(){
        $sql ="SELECT * FROM sys_adm_bancos";
        //echo $sql;
        return $this->conexion->query($sql);
    }


}