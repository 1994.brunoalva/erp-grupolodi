<?php


class DetallePagos extends DataBase
{
    private $detalle_id;
    private $condicion_id;
    private $monto_pago;
    private $fecha_pago;
    private $pago_id;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_detalle_pago';
        parent::__construct($this->accion, $this->table, 'detpa_id');
    }

    /**
     * @return mixed
     */
    public function getDetalleId()
    {
        return $this->detalle_id;
    }

    /**
     * @param mixed $detalle_id
     */
    public function setDetalleId($detalle_id)
    {
        $this->detalle_id = $detalle_id;
    }

    /**
     * @return mixed
     */
    public function getCondicionId()
    {
        return $this->condicion_id;
    }

    /**
     * @param mixed $condicion_id
     */
    public function setCondicionId($condicion_id)
    {
        $this->condicion_id = $condicion_id;
    }

    /**
     * @return mixed
     */
    public function getMontoPago()
    {
        return $this->monto_pago;
    }

    /**
     * @param mixed $monto_pago
     */
    public function setMontoPago($monto_pago)
    {
        $this->monto_pago = $monto_pago;
    }

    /**
     * @return mixed
     */
    public function getFechaPago()
    {
        return $this->fecha_pago;
    }

    /**
     * @param mixed $fecha_pago
     */
    public function setFechaPago($fecha_pago)
    {
        $this->fecha_pago = $fecha_pago;
    }

    /**
     * @return mixed
     */
    public function getPagoId()
    {
        return $this->pago_id;
    }

    /**
     * @param mixed $pago_id
     */
    public function setPagoId($pago_id)
    {
        $this->pago_id = $pago_id;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}