<?php


class Despacho extends DataBase
{
    private $despacho_id;
    private $nombre;
    private $descripccion;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_despacho';
        parent::__construct($this->accion, $this->table, 'desp_id');
    }
}