<?php


class Venta
{
    private $id;
    private $id_coti;
    private $id_cliente;
    private $id_empresa;
    private $tipo_pago;
    private $fecha;
    private $hora;
    private $monto;
    private $moneda;
    private $total;
    private $tipo_venta;
    private $serie;
    private $numero;
    private $estado;


    private $conexion;

    /**
     *  constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * @param mixed $serie
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdEmpresa()
    {
        return $this->id_empresa;
    }

    /**
     * @param mixed $id_empresa
     */
    public function setIdEmpresa($id_empresa)
    {
        $this->id_empresa = $id_empresa;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdCoti()
    {
        return $this->id_coti;
    }

    /**
     * @param mixed $id_coti
     */
    public function setIdCoti($id_coti)
    {
        $this->id_coti = $id_coti;
    }

    /**
     * @return mixed
     */
    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    /**
     * @param mixed $id_cliente
     */
    public function setIdCliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;
    }

    /**
     * @return mixed
     */
    public function getTipoPago()
    {
        return $this->tipo_pago;
    }

    /**
     * @param mixed $tipo_pago
     */
    public function setTipoPago($tipo_pago)
    {
        $this->tipo_pago = $tipo_pago;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * @param mixed $hora
     */
    public function setHora($hora)
    {
        $this->hora = $hora;
    }

    /**
     * @return mixed
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * @param mixed $monto
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;
    }

    /**
     * @return mixed
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * @param mixed $moneda
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getTipoVenta()
    {
        return $this->tipo_venta;
    }

    /**
     * @param mixed $tipo_venta
     */
    public function setTipoVenta($tipo_venta)
    {
        $this->tipo_venta = $tipo_venta;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }



    public function getDato(){
        $sql ="SELECT
                *
                FROM sys_ventas
                WHERE id="+$this->id;
        $res = $this->conexion->query($sql);
        if ($row = $res->fetch_row()){
            $this->id=$row['id'];
              $this->id_coti=$row['id_coti'];
              $this->id_cliente=$row['id_cliente'];
              $this->tipo_pago=$row['tipo_pago'];
              $this->fecha=$row['fecha'];
              $this->hora=$row['hora'];
              $this->monto=$row['monto'];
              $this->moneda=$row['moneda'];
              $this->total=$row['total'];
              $this->tipo_venta=$row['tipo_venta'];
              $this->estado=$row['estado'];
        }
        return $res;
    }

    public function insertar(){
        $sql ="INSERT INTO sys_ventas
VALUES (null,
        '$this->id_coti',
        '$this->id_cliente',
        '$this->id_empresa',
        '$this->tipo_pago',
        '$this->fecha',
        '$this->hora',
        '$this->monto',
        '$this->moneda',
        '$this->total',
        '$this->tipo_venta',
        '$this->serie',
        '$this->numero',
        '$this->estado');";
        ///echo $sql;
                $res = $this->conexion->query($sql);
                $this->id = $this->conexion->insert_id;
        return $res;
    }
    public function descontadorPro(){
        $estado = true;
        $sql1="SELECT 
                id_producto,
                id_empresa,
                cantidad 
                FROM
                sys_coti_detalle 
                WHERE id_cotizacion = ". $this->getIdCoti();
        $resulG = $this->conexion->query($sql1);
        foreach ($resulG as $row){
            $sql2= "UPDATE 
                      sys_producto_empresa 
                    SET
                      cantidad = cantidad - {$row['cantidad']}
                    WHERE id_empresa = {$row['id_empresa']} 
                      AND id_producto = {$row['id_producto']} ;";
            if (!$this->conexion->query($sql2)){
                $estado=false;
            }
        }

        return $estado;
    }
    public function listaProCoti(){
        $sql ="SELECT sys_ventas.*, sys_empresas.emp_nombre AS 'nomempre'
FROM sys_ventas
INNER JOIN sys_empresas ON sys_ventas.id_empresa=sys_empresas.emp_id
WHERE sys_ventas.id_coti=".$this->getIdCoti();
        return $this->conexion->query($sql);
    }
    public function lista(){
        $sql ="SELECT 
              coti.*,
              cotest.nombre AS 'estado_nombre' 
            FROM
              sys_cotizacion AS coti 
              INNER JOIN sys_coti_estado AS cotest 
                ON coti.coti_estatus = cotest.id 
            WHERE estado = 2 ";
        return $this->conexion->query($sql);
    }

    public function anular(){
        $sql ="UPDATE sys_ventas
                SET 
                  estado = '$this->estado'
                WHERE id = '$this->id';";
        return $this->conexion->query($sql);
    }
    public function getData(){
        $sql ="SELECT * FROM sys_ventas WHERE id =" . $this->getId();
        //echo $sql;
        return $this->conexion->query($sql)->fetch_assoc();
    }

    public function getEmpres($idEmpresa){
        $sql ="SELECT * FROM sys_empresas WHERE emp_id=".$idEmpresa;
        return $this->conexion->query($sql);
    }


}