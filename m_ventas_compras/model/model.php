<?php
$file = '../entidadDB/DataBase.php';
if (file_exists($file)) {
    require_once "" . $file . "";
} else {
    $file = '../' . $file;
    if (file_exists($file)) {
        require_once "" . $file . "";
    }
}
require_once 'Incoterm.php';
require_once 'Categoria.php';
require_once 'Pais.php';
require_once 'Proveedores.php';
require_once 'TipoDocumento.php';
require_once 'Folder.php';
require_once 'Empresa.php';
require_once 'TipoContenedor.php';
require_once 'Marca.php';
require_once 'Puerto.php';
require_once 'Aduana.php';
require_once 'AgenciaAduana.php';
require_once 'Canal.php';
require_once 'Orden.php';
require_once 'Almacen.php';
require_once 'Despacho.php';

require_once 'Forwarder.php';
require_once 'Linea.php';
require_once 'Nave.php';
require_once 'Embarque.php';
require_once 'Unidad.php';
require_once 'TipoProducto.php';
require_once 'CodSunat.php';
require_once 'Nomenglatura.php';
require_once 'Producto.php';
require_once 'Modelo.php';
require_once 'ProductoDetalle.php';
require_once 'Precentacion.php';
require_once 'Moneda.php';
require_once 'CambioMoneda.php';

require_once 'OrdenDetalle.php';
require_once 'TasaPoliza.php';
require_once 'Poliza.php';
require_once 'Pagos.php';
require_once 'CondicionPagos.php';
require_once 'DetallePagos.php';
require_once 'DetalleEstado.php';
require_once 'Estado.php';
require_once 'Contenedor.php';
require_once 'Packing.php';



