<?php


class Clientes
{
    private $id;
    private $ruc;
    private $id_tipo_documento;
    private $razon_social;
    private $telefono;
    private $contacto;
    private $email1;
    private $email2;
    private $telefono2;
    private $id_clasificacion;
    private $vendedor_responsable;
    private $estado_sunat;
    private $condicion_sunat;
    private $estado;

    private $conexion;

    /**
     * Clientes constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * @param mixed $ruc
     */
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;
    }

    /**
     * @return mixed
     */
    public function getIdTipoDocumento()
    {
        return $this->id_tipo_documento;
    }

    /**
     * @param mixed $id_tipo_documento
     */
    public function setIdTipoDocumento($id_tipo_documento)
    {
        $this->id_tipo_documento = $id_tipo_documento;
    }

    /**
     * @return mixed
     */
    public function getRazonSocial()
    {
        return $this->razon_social;
    }

    /**
     * @param mixed $razon_social
     */
    public function setRazonSocial($razon_social)
    {
        $this->razon_social = $razon_social;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * @param mixed $contacto
     */
    public function setContacto($contacto)
    {
        $this->contacto = $contacto;
    }

    /**
     * @return mixed
     */
    public function getEmail1()
    {
        return $this->email1;
    }

    /**
     * @param mixed $email1
     */
    public function setEmail1($email1)
    {
        $this->email1 = $email1;
    }

    /**
     * @return mixed
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * @param mixed $email2
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;
    }

    /**
     * @return mixed
     */
    public function getTelefono2()
    {
        return $this->telefono2;
    }

    /**
     * @param mixed $telefono2
     */
    public function setTelefono2($telefono2)
    {
        $this->telefono2 = $telefono2;
    }

    /**
     * @return mixed
     */
    public function getIdClasificacion()
    {
        return $this->id_clasificacion;
    }

    /**
     * @param mixed $id_clasificacion
     */
    public function setIdClasificacion($id_clasificacion)
    {
        $this->id_clasificacion = $id_clasificacion;
    }

    /**
     * @return mixed
     */
    public function getVendedorResponsable()
    {
        return $this->vendedor_responsable;
    }

    /**
     * @param mixed $vendedor_responsable
     */
    public function setVendedorResponsable($vendedor_responsable)
    {
        $this->vendedor_responsable = $vendedor_responsable;
    }

    /**
     * @return mixed
     */
    public function getEstadoSunat()
    {
        return $this->estado_sunat;
    }

    /**
     * @param mixed $estado_sunat
     */
    public function setEstadoSunat($estado_sunat)
    {
        $this->estado_sunat = $estado_sunat;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mysqli
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    /**
     * @param mysqli $conexion
     */
    public function setConexion($conexion)
    {
        $this->conexion = $conexion;
    }


    /**
     * @param mixed $condicion_sunat
     */
    public function setCondicionSunat($condicion_sunat)
    {
        $this->condicion_sunat = $condicion_sunat;
    }


    function getArrayData(){
        return array(
            'id'=>$this->id,
            'ruc'=>$this->ruc,
            'id_tipo_documento'=>$this->id_tipo_documento,
            'razon_social'=>$this->razon_social,
            'telefono'=>$this->telefono,
            'contacto'=>$this->contacto,
            'email1'=>$this->email1,
            'email2'=>$this->email2,
            'telefono2'=>$this->telefono2,
            'id_clasificacion'=>$this->id_clasificacion,
            'vendedor_responsable'=>$this->vendedor_responsable,
            'estado_sunat'=>$this->estado_sunat,
            'condicion_sunat'=>$this->condicion_sunat,
            'estado'=>$this->estado
        );
    }


    function insertar(){
        $sql = "INSERT INTO sys_clientes
                VALUES ('$this->id',
                        '$this->ruc',
                        '$this->id_tipo_documento',
                        '$this->razon_social',
                        '$this->telefono',
                        '$this->contacto',
                        '$this->email1',
                        '$this->email2',
                        '$this->telefono2',
                        '$this->id_clasificacion',
                        '$this->vendedor_responsable',
                        '$this->estado_sunat',
                        '$this->condicion_sunat',
                        '$this->estado');";

        $res = $this->conexion->query($sql);
        $this->id= $this->conexion->insert_id;
        return $res;
    }
    function verLista(){
        $sql="SELECT * FROM sys_clientes";
        return $this->conexion->query($sql);
    }
    function getData(){
        $sql="SELECT * FROM sys_clientes where id = ". $this->getId();
        $res = $this->conexion->query($sql);
        if ($row = $res->fetch_assoc()){

        }
        return $res;
    }

    function verCliente(){
        $sql="SELECT * FROM sys_clientes where id = ". $this->getId();
        //echo $sql;
        return $this->conexion->query($sql);
    }
    function update(){
        $sql="UPDATE sys_clientes
                SET 
                  ruc = '$this->ruc',
                  id_tipo_documento = '$this->id_tipo_documento',
                  razon_social = '$this->razon_social',
                  telefono = '$this->telefono',
                  contacto = '$this->contacto',
                  email1 = '$this->email1',
                  email2 = '$this->email2',
                  telefono2 = '$this->telefono2',
                  id_clasificacion = '$this->id_clasificacion',
                  vendedor_responsable = '$this->vendedor_responsable',
                  estado_sunat = '$this->estado_sunat',
                  condicion_sunat = '$this->condicion_sunat',
                  estado = '$this->estado'
                WHERE id = '$this->id';";
        echo $sql;
        return $this->conexion->query($sql);
    }
    function buscarCliente($term){
        $sql ="SELECT *
                FROM sys_clientes
                WHERE ruc  LIKE '%$term%'  LIMIT 20";
        return $this->conexion->query($sql);
    }




}