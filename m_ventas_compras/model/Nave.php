<?php

class Nave extends DataBase
{
    private $nave_id;
    private $nombre;
    private $linea_id;
    private $estado;


    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_nave';
        parent::__construct($this->accion, $this->table, 'nave_id');
    }

    /**
     * @return mixed
     */
    public function getNaveId()
    {
        return $this->nave_id;
    }

    /**
     * @param mixed $nave_id
     */
    public function setNaveId($nave_id)
    {
        $this->nave_id = $nave_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getLineaId()
    {
        return $this->linea_id;
    }

    /**
     * @param mixed $linea_id
     */
    public function setLineaId($linea_id)
    {
        $this->linea_id = $linea_id;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }



}