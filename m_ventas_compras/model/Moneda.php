<?php


class Moneda extends DataBase
{
    private $aduana_id;
    private $nombre;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_adm_sunat';
        parent::__construct($this->accion, $this->table, 'sun_id');
    }

    /**
     * @return mixed
     */
    public function getAduanaId()
    {
        return $this->aduana_id;
    }

    /**
     * @param mixed $aduana_id
     */
    public function setAduanaId($aduana_id)
    {
        $this->aduana_id = $aduana_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}