<?php


class Folder extends DataBase
{
    private $folder_id;
    private $empresa_id;
    private $categoria_id;
    private $serie;
    private $numerdo;
    private $incoterm_id;
    private $contenedor_id;
    private $cantidad_contenedor;
    private $marca_id;
    private $pais_id;
    private $puerto;
    private $proveedor_id;
    private $serie_proforma;
    private $fecha;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_impor_folder';
        parent::__construct($this->accion, $this->table, 'impor_id');
    }

    /**
     * @return mixed
     */
    public function getFolderId()
    {
        return $this->folder_id;
    }

    /**
     * @param mixed $folder_id
     */
    public function setFolderId($folder_id)
    {
        $this->folder_id = $folder_id;
    }

    /**
     * @return mixed
     */
    public function getEmpresaId()
    {
        return $this->empresa_id;
    }

    /**
     * @param mixed $empresa_id
     */
    public function setEmpresaId($empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return mixed
     */
    public function getCategoriaId()
    {
        return $this->categoria_id;
    }

    /**
     * @param mixed $categoria_id
     */
    public function setCategoriaId($categoria_id)
    {
        $this->categoria_id = $categoria_id;
    }

    /**
     * @return mixed
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * @param mixed $serie
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;
    }

    /**
     * @return mixed
     */
    public function getNumerdo()
    {
        return $this->numerdo;
    }

    /**
     * @param mixed $numerdo
     */
    public function setNumerdo($numerdo)
    {
        $this->numerdo = $numerdo;
    }

    /**
     * @return mixed
     */
    public function getIncotermId()
    {
        return $this->incoterm_id;
    }

    /**
     * @param mixed $incoterm_id
     */
    public function setIncotermId($incoterm_id)
    {
        $this->incoterm_id = $incoterm_id;
    }

    /**
     * @return mixed
     */
    public function getContenedorId()
    {
        return $this->contenedor_id;
    }

    /**
     * @param mixed $contenedor_id
     */
    public function setContenedorId($contenedor_id)
    {
        $this->contenedor_id = $contenedor_id;
    }

    /**
     * @return mixed
     */
    public function getCantidadContenedor()
    {
        return $this->cantidad_contenedor;
    }

    /**
     * @param mixed $cantidad_contenedor
     */
    public function setCantidadContenedor($cantidad_contenedor)
    {
        $this->cantidad_contenedor = $cantidad_contenedor;
    }

    /**
     * @return mixed
     */
    public function getMarcaId()
    {
        return $this->marca_id;
    }

    /**
     * @param mixed $marca_id
     */
    public function setMarcaId($marca_id)
    {
        $this->marca_id = $marca_id;
    }

    /**
     * @return mixed
     */
    public function getPaisId()
    {
        return $this->pais_id;
    }

    /**
     * @param mixed $pais_id
     */
    public function setPaisId($pais_id)
    {
        $this->pais_id = $pais_id;
    }

    /**
     * @return mixed
     */
    public function getPuerto()
    {
        return $this->puerto;
    }

    /**
     * @param mixed $puerto
     */
    public function setPuerto($puerto)
    {
        $this->puerto = $puerto;
    }

    /**
     * @return mixed
     */
    public function getProveedorId()
    {
        return $this->proveedor_id;
    }

    /**
     * @param mixed $proveedor_id
     */
    public function setProveedorId($proveedor_id)
    {
        $this->proveedor_id = $proveedor_id;
    }

    /**
     * @return mixed
     */
    public function getSerieProforma()
    {
        return $this->serie_proforma;
    }

    /**
     * @param mixed $serie_proforma
     */
    public function setSerieProforma($serie_proforma)
    {
        $this->serie_proforma = $serie_proforma;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}