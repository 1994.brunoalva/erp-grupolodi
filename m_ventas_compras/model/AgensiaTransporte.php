<?php


class AgensiaTransporte
{
private $id;
private $ruc;
private $razon_social;
private $direccion;
private $telefono;
private $condicion;
private $estado;


    private $conexion;

    /**
     * Clientes constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * @param mixed $ruc
     */
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;
    }

    /**
     * @return mixed
     */
    public function getRazonSocial()
    {
        return $this->razon_social;
    }

    /**
     * @param mixed $razon_social
     */
    public function setRazonSocial($razon_social)
    {
        $this->razon_social = $razon_social;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getCondicion()
    {
        return $this->condicion;
    }

    /**
     * @param mixed $condicion
     */
    public function setCondicion($condicion)
    {
        $this->condicion = $condicion;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    function  getArrayData(){
        return array(
            'id'=>$this->id,
            'ruc'=>$this->ruc,
            'razon_social'=>$this->razon_social,
            'direccion'=>$this->direccion,
            'telefono'=>$this->telefono,
            'condicion'=>$this->condicion,
            'estado'=>$this->estado
        );
    }


    function insertar(){
        $sql = "INSERT sys_agencia_transporte
                VALUES (null,
                    '$this->ruc',
                    '$this->razon_social',
                    '$this->direccion',
                    '$this->telefono',
                    '$this->condicion',
                    '$this->estado');";
        $res = $this->conexion->query($sql);
        $this->id= $this->conexion->insert_id;
        return $res;
    }

    function lista(){
        $sql="SELECT * FROM sys_agencia_transporte";
        return $this->conexion->query($sql);
    }

    function update(){
        $sql="UPDATE sys_agencia_transporte
            SET 
              ruc = '$this->ruc',
              razon_social = '$this->razon_social',
              direccion = '$this->direccion',
              telefono = '$this->telefono',
              condicion = '$this->condicion',
              estado = '$this->estado'
            WHERE id = '$this->id';
            ";
        return $this->conexion->query($sql);
    }

}