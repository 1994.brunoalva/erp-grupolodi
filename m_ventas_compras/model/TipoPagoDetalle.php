<?php


class TipoPagoDetalle
{

    private $tip_id;
    private $tip_descrip;
    private $tip_siglas;
    private $pag_id;


    private $conexion;

    /**
     * Clientes constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getTipId()
    {
        return $this->tip_id;
    }

    /**
     * @param mixed $tip_id
     */
    public function setTipId($tip_id)
    {
        $this->tip_id = $tip_id;
    }

    /**
     * @return mixed
     */
    public function getTipDescrip()
    {
        return $this->tip_descrip;
    }

    /**
     * @param mixed $tip_descrip
     */
    public function setTipDescrip($tip_descrip)
    {
        $this->tip_descrip = $tip_descrip;
    }

    /**
     * @return mixed
     */
    public function getTipSiglas()
    {
        return $this->tip_siglas;
    }

    /**
     * @param mixed $tip_siglas
     */
    public function setTipSiglas($tip_siglas)
    {
        $this->tip_siglas = $tip_siglas;
    }

    /**
     * @return mixed
     */
    public function getPagId()
    {
        return $this->pag_id;
    }

    /**
     * @param mixed $pag_id
     */
    public function setPagId($pag_id)
    {
        $this->pag_id = $pag_id;
    }

    function insertar(){
        $sql="";
        return $this->conexion->query($sql);
    }

    function listaPorTipo(){
        $sql="SELECT * FROM sys_cob_tip_pagos_detalle WHERE pag_id =" . $this->getPagId();
        return $this->conexion->query($sql);
    }

}