<?php


class CompraNacional
{
    private $com_id;
    private $proveedor_id;
    private $empresa_id;
    private $tipo_doc;
    private $com_fecha;
    private $num_ref;
    private $moneda;
    private $cambio;
    private $monto;
    private $archivo;
    private $estado;

    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public function insertar(){
        $sql ="INSERT INTO sys_compras_nacionales
VALUES (null,
        '$this->proveedor_id',
        '$this->empresa_id',
        '$this->tipo_doc',
        '$this->com_fecha',
        '$this->num_ref',
        '$this->moneda',
        '$this->cambio',
        '$this->monto',
        '$this->archivo',
        '$this->estado');";
        $res = $this->conexion->query($sql);
        if ($res){
            $this->com_id = $this->conexion->insert_id;
        }
        return $res;
    }
    public function getDato(){
        $sql ="SELECT 
  com_nac.*,
  provee.provee_desc 
FROM
  sys_compras_nacionales AS com_nac 
  INNER JOIN sys_com_proveedor AS provee  ON com_nac.proveedor_id  = provee.provee_id
WHERE com_nac.com_id =".$this->com_id;
        $res = $this->conexion->query($sql);

        return $res;
    }
    /**
     * @return mixed
     */
    public function getComId()
    {
        return $this->com_id;
    }

    /**
     * @param mixed $com_id
     */
    public function setComId($com_id)
    {
        $this->com_id = $com_id;
    }

    /**
     * @return mixed
     */
    public function getProveedorId()
    {
        return $this->proveedor_id;
    }

    /**
     * @param mixed $proveedor_id
     */
    public function setProveedorId($proveedor_id)
    {
        $this->proveedor_id = $proveedor_id;
    }

    /**
     * @return mixed
     */
    public function getEmpresaId()
    {
        return $this->empresa_id;
    }

    /**
     * @param mixed $empresa_id
     */
    public function setEmpresaId($empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return mixed
     */
    public function getTipoDoc()
    {
        return $this->tipo_doc;
    }

    /**
     * @param mixed $tipo_doc
     */
    public function setTipoDoc($tipo_doc)
    {
        $this->tipo_doc = $tipo_doc;
    }

    /**
     * @return mixed
     */
    public function getComFecha()
    {
        return $this->com_fecha;
    }

    /**
     * @param mixed $com_fecha
     */
    public function setComFecha($com_fecha)
    {
        $this->com_fecha = $com_fecha;
    }

    /**
     * @return mixed
     */
    public function getNumRef()
    {
        return $this->num_ref;
    }

    /**
     * @param mixed $num_ref
     */
    public function setNumRef($num_ref)
    {
        $this->num_ref = $num_ref;
    }

    /**
     * @return mixed
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * @param mixed $moneda
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;
    }

    /**
     * @return mixed
     */
    public function getCambio()
    {
        return $this->cambio;
    }

    /**
     * @param mixed $cambio
     */
    public function setCambio($cambio)
    {
        $this->cambio = $cambio;
    }

    /**
     * @return mixed
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * @param mixed $monto
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;
    }

    /**
     * @return mixed
     */
    public function getArchivo()
    {
        return $this->archivo;
    }

    /**
     * @param mixed $archivo
     */
    public function setArchivo($archivo)
    {
        $this->archivo = $archivo;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}