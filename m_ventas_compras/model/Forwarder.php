<?php


class Forwarder extends DataBase
{
    private $forwarder_id;
    private $codigo;
    private $nombre;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_forwarder';
        parent::__construct($this->accion, $this->table, 'forwa_id');
    }

    /**
     * @return mixed
     */
    public function getForwarderId()
    {
        return $this->forwarder_id;
    }

    /**
     * @param mixed $forwarder_id
     */
    public function setForwarderId($forwarder_id)
    {
        $this->forwarder_id = $forwarder_id;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }



}