<?php


class Canal extends DataBase
{
    private $canal_id;
    private $nombre;
    private $descipccion;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_aduana_canal';
        parent::__construct($this->accion, $this->table, 'canal_id');
    }

    /**
     * @return mixed
     */
    public function getCanalId()
    {
        return $this->canal_id;
    }

    /**
     * @param mixed $canal_id
     */
    public function setCanalId($canal_id)
    {
        $this->canal_id = $canal_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getDescipccion()
    {
        return $this->descipccion;
    }

    /**
     * @param mixed $descipccion
     */
    public function setDescipccion($descipccion)
    {
        $this->descipccion = $descipccion;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}