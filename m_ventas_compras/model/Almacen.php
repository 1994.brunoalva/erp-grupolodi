<?php


class Almacen extends DataBase
{
    private $almacen_id;
    private $razon_social;
    private $jurisdiccion;
    private $representante;
    private $direccion;
    private $telefono1;
    private $telefono2;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_almacen';
        parent::__construct($this->accion, $this->table, 'alm_id');
    }

    /**
     * @return mixed
     */
    public function getAlmacenId()
    {
        return $this->almacen_id;
    }

    /**
     * @param mixed $almacen_id
     */
    public function setAlmacenId($almacen_id)
    {
        $this->almacen_id = $almacen_id;
    }

    /**
     * @return mixed
     */
    public function getRazonSocial()
    {
        return $this->razon_social;
    }

    /**
     * @param mixed $razon_social
     */
    public function setRazonSocial($razon_social)
    {
        $this->razon_social = $razon_social;
    }

    /**
     * @return mixed
     */
    public function getJurisdiccion()
    {
        return $this->jurisdiccion;
    }

    /**
     * @param mixed $jurisdiccion
     */
    public function setJurisdiccion($jurisdiccion)
    {
        $this->jurisdiccion = $jurisdiccion;
    }

    /**
     * @return mixed
     */
    public function getRepresentante()
    {
        return $this->representante;
    }

    /**
     * @param mixed $representante
     */
    public function setRepresentante($representante)
    {
        $this->representante = $representante;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getTelefono1()
    {
        return $this->telefono1;
    }

    /**
     * @param mixed $telefono1
     */
    public function setTelefono1($telefono1)
    {
        $this->telefono1 = $telefono1;
    }

    /**
     * @return mixed
     */
    public function getTelefono2()
    {
        return $this->telefono2;
    }

    /**
     * @param mixed $telefono2
     */
    public function setTelefono2($telefono2)
    {
        $this->telefono2 = $telefono2;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}