<?php


class Embarque extends DataBase
{
    private $embarque_id;
    private $orden_id;
    private $factura_serie;
    private $factura_fecha;
    private $monto_embarque;
    private $flete_embarque;
    private $thc_embarque;
    private $total_flete_embarque;
    private $embarque_serie;
    private $forwarder_id;
    private $despacho_id;
    private $linea_id;
    private $nave_id;
    private $embarque_etd;
    private $embarque_eta;
    private $embarque_fecha_sobrestadia;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_embarque';
        parent::__construct($this->accion, $this->table, 'emb_id');
    }

    /**
     * @return mixed
     */
    public function getEmbarqueId()
    {
        return $this->embarque_id;
    }

    /**
     * @param mixed $embarque_id
     */
    public function setEmbarqueId($embarque_id)
    {
        $this->embarque_id = $embarque_id;
    }

    /**
     * @return mixed
     */
    public function getOrdenId()
    {
        return $this->orden_id;
    }

    /**
     * @param mixed $orden_id
     */
    public function setOrdenId($orden_id)
    {
        $this->orden_id = $orden_id;
    }

    /**
     * @return mixed
     */
    public function getFacturaSerie()
    {
        return $this->factura_serie;
    }

    /**
     * @param mixed $factura_serie
     */
    public function setFacturaSerie($factura_serie)
    {
        $this->factura_serie = $factura_serie;
    }

    /**
     * @return mixed
     */
    public function getFacturaFecha()
    {
        return $this->factura_fecha;
    }

    /**
     * @param mixed $factura_fecha
     */
    public function setFacturaFecha($factura_fecha)
    {
        $this->factura_fecha = $factura_fecha;
    }


    /**
     * @return mixed
     */
    public function getMontoEmbarque()
    {
        return $this->monto_embarque;
    }

    /**
     * @param mixed $monto_embarque
     */
    public function setMontoEmbarque($monto_embarque)
    {
        $this->monto_embarque = $monto_embarque;
    }

    /**
     * @return mixed
     */
    public function getFleteEmbarque()
    {
        return $this->flete_embarque;
    }

    /**
     * @param mixed $flete_embarque
     */
    public function setFleteEmbarque($flete_embarque)
    {
        $this->flete_embarque = $flete_embarque;
    }

    /**
     * @return mixed
     */
    public function getThcEmbarque()
    {
        return $this->thc_embarque;
    }

    /**
     * @param mixed $thc_embarque
     */
    public function setThcEmbarque($thc_embarque)
    {
        $this->thc_embarque = $thc_embarque;
    }

    /**
     * @return mixed
     */
    public function getTotalFleteEmbarque()
    {
        return $this->total_flete_embarque;
    }

    /**
     * @param mixed $total_flete_embarque
     */
    public function setTotalFleteEmbarque($total_flete_embarque)
    {
        $this->total_flete_embarque = $total_flete_embarque;
    }

    /**
     * @return mixed
     */
    public function getEmbarqueSerie()
    {
        return $this->embarque_serie;
    }

    /**
     * @param mixed $embarque_serie
     */
    public function setEmbarqueSerie($embarque_serie)
    {
        $this->embarque_serie = $embarque_serie;
    }

    /**
     * @return mixed
     */
    public function getForwarderId()
    {
        return $this->forwarder_id;
    }

    /**
     * @param mixed $forwarder_id
     */
    public function setForwarderId($forwarder_id)
    {
        $this->forwarder_id = $forwarder_id;
    }

    /**
     * @return mixed
     */
    public function getDespachoId()
    {
        return $this->despacho_id;
    }

    /**
     * @param mixed $despacho_id
     */
    public function setDespachoId($despacho_id)
    {
        $this->despacho_id = $despacho_id;
    }

    /**
     * @return mixed
     */
    public function getLineaId()
    {
        return $this->linea_id;
    }

    /**
     * @param mixed $linea_id
     */
    public function setLineaId($linea_id)
    {
        $this->linea_id = $linea_id;
    }

    /**
     * @return mixed
     */
    public function getNaveId()
    {
        return $this->nave_id;
    }

    /**
     * @param mixed $nave_id
     */
    public function setNaveId($nave_id)
    {
        $this->nave_id = $nave_id;
    }

    /**
     * @return mixed
     */
    public function getEmbarqueEtd()
    {
        return $this->embarque_etd;
    }

    /**
     * @param mixed $embarque_etd
     */
    public function setEmbarqueEtd($embarque_etd)
    {
        $this->embarque_etd = $embarque_etd;
    }

    /**
     * @return mixed
     */
    public function getEmbarqueEta()
    {
        return $this->embarque_eta;
    }

    /**
     * @param mixed $embarque_eta
     */
    public function setEmbarqueEta($embarque_eta)
    {
        $this->embarque_eta = $embarque_eta;
    }

    /**
     * @return mixed
     */
    public function getEmbarqueFechaSobrestadia()
    {
        return $this->embarque_fecha_sobrestadia;
    }

    /**
     * @param mixed $embarque_fecha_sobrestadia
     */
    public function setEmbarqueFechaSobrestadia($embarque_fecha_sobrestadia)
    {
        $this->embarque_fecha_sobrestadia = $embarque_fecha_sobrestadia;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}