<?php


class TipoPago
{

    private $pag_id;
    private $pag_nombre;
    private $pag_estatus;


    private $conexion;

    /**
     * Clientes constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getPagId()
    {
        return $this->pag_id;
    }

    /**
     * @param mixed $pag_id
     */
    public function setPagId($pag_id)
    {
        $this->pag_id = $pag_id;
    }

    /**
     * @return mixed
     */
    public function getPagNombre()
    {
        return $this->pag_nombre;
    }

    /**
     * @param mixed $pag_nombre
     */
    public function setPagNombre($pag_nombre)
    {
        $this->pag_nombre = $pag_nombre;
    }

    /**
     * @return mixed
     */
    public function getPagEstatus()
    {
        return $this->pag_estatus;
    }

    /**
     * @param mixed $pag_estatus
     */
    public function setPagEstatus($pag_estatus)
    {
        $this->pag_estatus = $pag_estatus;
    }

    /**
     * @return mysqli
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    /**
     * @param mysqli $conexion
     */
    public function setConexion($conexion)
    {
        $this->conexion = $conexion;
    }


    function insertar(){
        $sql="INSERT INTO sys_cob_tip_pagos
                VALUES ('pag_id',
                        'pag_nombre',
                        'pag_estatus');";
        return $this->conexion->query($sql);
    }

    function lista(){
        $sql="SELECT * FROM sys_cob_tip_pagos";
        return $this->conexion->query($sql);
    }

}