<?php


class EmbarqueDetalle
{
private $emb_det_id;
    private $emb_id;
    private $linea_id;
    private $almacen_id;
    private $nave_id;
    private $desp_id;
    private $emb_bl_serie;
    private $emb_etd;
    private $emb_eta;
    private $emb_libre_sobre;
    private $emb_libre_almacen;
    private $conexion;
    /**
     * EmbarqueDetalle constructor.
     */
    public function __construct()
    {$this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getEmbDetId()
    {
        return $this->emb_det_id;
    }

    /**
     * @param mixed $emb_det_id
     */
    public function setEmbDetId($emb_det_id)
    {
        $this->emb_det_id = $emb_det_id;
    }

    /**
     * @return mixed
     */
    public function getEmbId()
    {
        return $this->emb_id;
    }

    /**
     * @param mixed $emb_id
     */
    public function setEmbId($emb_id)
    {
        $this->emb_id = $emb_id;
    }

    /**
     * @return mixed
     */
    public function getLineaId()
    {
        return $this->linea_id;
    }

    /**
     * @param mixed $linea_id
     */
    public function setLineaId($linea_id)
    {
        $this->linea_id = $linea_id;
    }

    /**
     * @return mixed
     */
    public function getAlmacenId()
    {
        return $this->almacen_id;
    }

    /**
     * @param mixed $almacen_id
     */
    public function setAlmacenId($almacen_id)
    {
        $this->almacen_id = $almacen_id;
    }

    /**
     * @return mixed
     */
    public function getNaveId()
    {
        return $this->nave_id;
    }

    /**
     * @param mixed $nave_id
     */
    public function setNaveId($nave_id)
    {
        $this->nave_id = $nave_id;
    }

    /**
     * @return mixed
     */
    public function getDespId()
    {
        return $this->desp_id;
    }

    /**
     * @param mixed $desp_id
     */
    public function setDespId($desp_id)
    {
        $this->desp_id = $desp_id;
    }

    /**
     * @return mixed
     */
    public function getEmbBlSerie()
    {
        return $this->emb_bl_serie;
    }

    /**
     * @param mixed $emb_bl_serie
     */
    public function setEmbBlSerie($emb_bl_serie)
    {
        $this->emb_bl_serie = $emb_bl_serie;
    }

    /**
     * @return mixed
     */
    public function getEmbEtd()
    {
        return $this->emb_etd;
    }

    /**
     * @param mixed $emb_etd
     */
    public function setEmbEtd($emb_etd)
    {
        $this->emb_etd = $emb_etd;
    }

    /**
     * @return mixed
     */
    public function getEmbEta()
    {
        return $this->emb_eta;
    }

    /**
     * @param mixed $emb_eta
     */
    public function setEmbEta($emb_eta)
    {
        $this->emb_eta = $emb_eta;
    }

    /**
     * @return mixed
     */
    public function getEmbLibreSobre()
    {
        return $this->emb_libre_sobre;
    }

    /**
     * @param mixed $emb_libre_sobre
     */
    public function setEmbLibreSobre($emb_libre_sobre)
    {
        $this->emb_libre_sobre = $emb_libre_sobre;
    }

    /**
     * @return mixed
     */
    public function getEmbLibreAlmacen()
    {
        return $this->emb_libre_almacen;
    }

    /**
     * @param mixed $emb_libre_almacen
     */
    public function setEmbLibreAlmacen($emb_libre_almacen)
    {
        $this->emb_libre_almacen = $emb_libre_almacen;
    }

    public function toArrayData(){
        return array('iddetaemb'=>$this->getEmbDetId(),
                    'idemp'=>$this->getEmbId(),
                    'idLinea'=>$this->getLineaId(),
                    'nombrelinea'=>"",
                    'idalmacen'=>$this->getAlmacenId(),
                    'nomalmacen'=>"",
                    'bl'=>$this->getEmbBlSerie(),
                    'idnave'=>$this->getNaveId(),
                    'etd'=>$this->getEmbEtd(),
                    'eta' => $this->getEmbEta(),
                    'libres'=>$this->getEmbLibreSobre(),
                    'iddepacho'=>$this->getDespId(),
                    'diasalma'=>$this->getEmbLibreAlmacen());
    }



    public function insertar(){
        $sql ="INSERT INTO sys_com_embarque_detalle
VALUES (null,
        $this->emb_id,
        $this->linea_id,
        $this->almacen_id,
        $this->nave_id,
        $this->desp_id,
        '$this->emb_bl_serie',
        '$this->emb_etd',
        '$this->emb_eta',
        '$this->emb_libre_sobre',
        '$this->emb_libre_almacen');";
        //echo $sql;
        $res =  $this->conexion->query($sql);
        $this->emb_det_id = $this->conexion->insert_id;
        return $res;
    }

    public function listar(){
        $sql ="SELECT *
        FROM sys_com_embarque_detalle
        WHERE emb_id= '$this->emb_id'";
        //echo $sql;
        return $this->conexion->query($sql);
    }

    public function actualizar(){
        $sql ="UPDATE sys_com_embarque_detalle
SET
  linea_id = $this->linea_id,
  almacen_id = $this->almacen_id,
  nave_id = $this->nave_id,
  desp_id = $this->desp_id,
  emb_bl_serie = '$this->emb_bl_serie',
  emb_etd = '$this->emb_etd',
  emb_eta = '$this->emb_eta',
  emb_libre_sobre = '$this->emb_libre_sobre',
  emb_libre_almacen = '$this->emb_libre_almacen'
WHERE emb_det_id = '$this->emb_det_id';";

        return $this->conexion->query($sql);
    }



}