<?php


class Orden extends DataBase
{
    private $orden_id;
    private $folder_id;
    private $total;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_orden';
        parent::__construct($this->accion, $this->table, 'ord_id');
    }

    /**
     * @return mixed
     */
    public function getOrdenId()
    {
        return $this->orden_id;
    }

    /**
     * @param mixed $orden_id
     */
    public function setOrdenId($orden_id)
    {
        $this->orden_id = $orden_id;
    }

    /**
     * @return mixed
     */
    public function getFolderId()
    {
        return $this->folder_id;
    }

    /**
     * @param mixed $folder_id
     */
    public function setFolderId($folder_id)
    {
        $this->folder_id = $folder_id;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }



}