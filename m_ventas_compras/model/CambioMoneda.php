<?php


class CambioMoneda extends DataBase
{
    private $cambio_id;
    private $moneda_id;
    private $compra;
    private $venta;
    private $fecha;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_tasa_cambio';
        parent::__construct($this->accion, $this->table, 'tas_id');
    }

    /**
     * @return mixed
     */
    public function getCambioId()
    {
        return $this->cambio_id;
    }

    /**
     * @param mixed $cambio_id
     */
    public function setCambioId($cambio_id)
    {
        $this->cambio_id = $cambio_id;
    }

    /**
     * @return mixed
     */
    public function getMonedaId()
    {
        return $this->moneda_id;
    }

    /**
     * @param mixed $moneda_id
     */
    public function setMonedaId($moneda_id)
    {
        $this->moneda_id = $moneda_id;
    }

    /**
     * @return mixed
     */
    public function getCompra()
    {
        return $this->compra;
    }

    /**
     * @param mixed $compra
     */
    public function setCompra($compra)
    {
        $this->compra = $compra;
    }

    /**
     * @return mixed
     */
    public function getVenta()
    {
        return $this->venta;
    }

    /**
     * @param mixed $venta
     */
    public function setVenta($venta)
    {
        $this->venta = $venta;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}