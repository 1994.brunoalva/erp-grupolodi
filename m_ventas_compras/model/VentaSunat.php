<?php


class VentaSunat
{
    public $id_venta_sunat;
    public $hash_v;
    public $nombreXML;
    public $dataQR;
    public $estado;

    private $conexion;

    /**
     *  constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getIdVentaSunat()
    {
        return $this->id_venta_sunat;
    }

    /**
     * @param mixed $id_venta_sunat
     */
    public function setIdVentaSunat($id_venta_sunat)
    {
        $this->id_venta_sunat = $id_venta_sunat;
    }

    /**
     * @return mixed
     */
    public function getHashV()
    {
        return $this->hash_v;
    }

    /**
     * @param mixed $hash_v
     */
    public function setHashV($hash_v)
    {
        $this->hash_v = $hash_v;
    }

    /**
     * @return mixed
     */
    public function getNombreXML()
    {
        return $this->nombreXML;
    }

    /**
     * @param mixed $nombreXML
     */
    public function setNombreXML($nombreXML)
    {
        $this->nombreXML = $nombreXML;
    }

    /**
     * @return mixed
     */
    public function getDataQR()
    {
        return $this->dataQR;
    }

    /**
     * @param mixed $dataQR
     */
    public function setDataQR($dataQR)
    {
        $this->dataQR = $dataQR;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function getData(){
        $sql ="SELECT *
        FROM sys_venta_sunat
        WHERE id_venta_sunat = " . $this->getIdVentaSunat();
        $res =  $this->conexion->query($sql);

        if ($row = $res->fetch_assoc()){
            $this->hash_v=$row['hash_v'];
            $this->nombreXML=$row['nombreXML'];
            $this->dataQR=$row['dataQR'];
            $this->estado=$row['estado'];

        }

        return $res;

    }

    public function insertar(){
        $sql ="INSERT INTO sys_venta_sunat
VALUES ('$this->id_venta_sunat',
        '$this->hash_v',
        '$this->nombreXML',
        '$this->dataQR',
        '$this->estado');";
        return $this->conexion->query($sql);
    }


}