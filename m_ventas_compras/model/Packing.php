<?php


class Packing extends DataBase
{
    private $packing_id;
    private $detalle_id;
    private $cont_id;
    private $cantidad;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_packing';
        parent::__construct($this->accion, $this->table, 'pack_id');
    }

    /**
     * @return mixed
     */
    public function getPackingId()
    {
        return $this->packing_id;
    }

    /**
     * @param mixed $packing_id
     */
    public function setPackingId($packing_id)
    {
        $this->packing_id = $packing_id;
    }

    /**
     * @return mixed
     */
    public function getDetalleId()
    {
        return $this->detalle_id;
    }

    /**
     * @param mixed $detalle_id
     */
    public function setDetalleId($detalle_id)
    {
        $this->detalle_id = $detalle_id;
    }

    /**
     * @return mixed
     */
    public function getContId()
    {
        return $this->cont_id;
    }

    /**
     * @param mixed $cont_id
     */
    public function setContId($cont_id)
    {
        $this->cont_id = $cont_id;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

   }