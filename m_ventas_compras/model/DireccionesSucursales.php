<?php


class DireccionesSucursales
{
    private $id;
    private $id_agencia;
    private $id_cliente;
    private $direccion;
    private $departamento;
    private $provincia;
    private $distrito;
    private $ubigeo;
    private $tipo;
    private $estado;

    private $conexion;

    /**
     * Clientes constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdAgencia()
    {
        return $this->id_agencia;
    }

    /**
     * @param mixed $id_agencia
     */
    public function setIdAgencia($id_agencia)
    {
        $this->id_agencia = $id_agencia;
    }

    /**
     * @return mixed
     */
    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    /**
     * @param mixed $id_cliente
     */
    public function setIdCliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * @param mixed $departamento
     */
    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;
    }

    /**
     * @return mixed
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * @param mixed $provincia
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;
    }

    /**
     * @return mixed
     */
    public function getDistrito()
    {
        return $this->distrito;
    }

    /**
     * @param mixed $distrito
     */
    public function setDistrito($distrito)
    {
        $this->distrito = $distrito;
    }

    /**
     * @return mixed
     */
    public function getUbigeo()
    {
        return $this->ubigeo;
    }

    /**
     * @param mixed $ubigeo
     */
    public function setUbigeo($ubigeo)
    {
        $this->ubigeo = $ubigeo;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mysqli
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    /**
     * @param mysqli $conexion
     */
    public function setConexion($conexion)
    {
        $this->conexion = $conexion;
    }

    public function getArrayDirec(){
        $dire=[];
        $sql ="SELECT
                 *
                FROM sys_clie_dir_sucur WHERE id_cliente=".$this->getIdCliente();
        $res =  $this->conexion->query($sql);
        foreach ($res as $row){
            $dire []=array(
                'id'=>$row['id'],
                'idagencia'=>is_null($row['id_agencia'])?-1:$row['id_agencia'],
                'nomagencia'=>'',
                'idcliente'=>$row['id_cliente'],
                'direccion'=>$row['direccion'],
                'departamento'=>$row['departamento'],
                'distrito'=>$row['distrito'],
                'provincia'=>$row['provincia'],
                'tipo'=>$row['tipo'],
                'ubigeo'=>$row['ubigeo'],
                "estado"=>$row['estado'],
                'st'=>false
            );
        }
        return $dire;
    }
    public function actualizar(){
        $sql ="UPDATE sys_clie_dir_sucur
                SET
                  id_agencia = $this->id_agencia,
                  id_cliente = '$this->id_cliente',
                  direccion = '$this->direccion',
                  departamento = '$this->departamento',
                  provincia = '$this->provincia',
                  distrito = '$this->distrito',
                  ubigeo = '$this->ubigeo',
                  tipo = '$this->tipo'
                WHERE id = '$this->id';";
        return $this->conexion->query($sql);
    }

    public function insertar(){
        $sql ="INSERT INTO sys_clie_dir_sucur
VALUES (null,
        $this->id_agencia,
        '$this->id_cliente',
        '$this->direccion',
        '$this->departamento',
        '$this->provincia',
        '$this->distrito',
        '$this->ubigeo',
        '$this->tipo',
        '$this->estado');";
        return $this->conexion->query($sql);
    }



}