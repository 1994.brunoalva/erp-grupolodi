<?php


class Modelo extends DataBase
{
    private $modelo_id;
    private $nombre;
    private $marca_id;
    private $logo;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_modelo';
        parent::__construct($this->accion, $this->table, 'mod_id');
    }

    /**
     * @return mixed
     */
    public function getModeloId()
    {
        return $this->modelo_id;
    }

    /**
     * @param mixed $modelo_id
     */
    public function setModeloId($modelo_id)
    {
        $this->modelo_id = $modelo_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getMarcaId()
    {
        return $this->marca_id;
    }

    /**
     * @param mixed $marca_id
     */
    public function setMarcaId($marca_id)
    {
        $this->marca_id = $marca_id;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}