<?php


class TipoProducto extends DataBase
{
    private $tipo_prod_id;
    private $nombre;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_tipo_producto';
        parent::__construct($this->accion, $this->table, 'tipro_id');
    }

    /**
     * @return mixed
     */
    public function getTipoProdId()
    {
        return $this->tipo_prod_id;
    }

    /**
     * @param mixed $tipo_prod_id
     */
    public function setTipoProdId($tipo_prod_id)
    {
        $this->tipo_prod_id = $tipo_prod_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}