<?php


class DetalleEstado extends DataBase
{
    private $detalle_di;
    private $estado_id;
    private $impor_id;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_deta_estado';
        parent::__construct($this->accion, $this->table, 'de_id');
    }

    /**
     * @return mixed
     */
    public function getDetalleDi()
    {
        return $this->detalle_di;
    }

    /**
     * @param mixed $detalle_di
     */
    public function setDetalleDi($detalle_di)
    {
        $this->detalle_di = $detalle_di;
    }

    /**
     * @return mixed
     */
    public function getEstadoId()
    {
        return $this->estado_id;
    }

    /**
     * @param mixed $estado_id
     */
    public function setEstadoId($estado_id)
    {
        $this->estado_id = $estado_id;
    }

    /**
     * @return mixed
     */
    public function getImporId()
    {
        return $this->impor_id;
    }

    /**
     * @param mixed $impor_id
     */
    public function setImporId($impor_id)
    {
        $this->impor_id = $impor_id;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}