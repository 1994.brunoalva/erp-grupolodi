<?php


class FlujoCajaChica
{
    private $id;
    private $id_caja;
    private $hora;
    private $id_concepto;
    private $id_moneda;
    private $detalle;
    private $ingreso;
    private $egreso;


    private $conexion;

    /**
     * FlujoCajaChica constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdCaja()
    {
        return $this->id_caja;
    }

    /**
     * @param mixed $id_caja
     */
    public function setIdCaja($id_caja)
    {
        $this->id_caja = $id_caja;
    }

    /**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * @param mixed $hora
     */
    public function setHora($hora)
    {
        $this->hora = $hora;
    }

    /**
     * @return mixed
     */
    public function getIdConcepto()
    {
        return $this->id_concepto;
    }

    /**
     * @param mixed $id_concepto
     */
    public function setIdConcepto($id_concepto)
    {
        $this->id_concepto = $id_concepto;
    }

    /**
     * @return mixed
     */
    public function getIdMoneda()
    {
        return $this->id_moneda;
    }

    /**
     * @param mixed $id_moneda
     */
    public function setIdMoneda($id_moneda)
    {
        $this->id_moneda = $id_moneda;
    }

    /**
     * @return mixed
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * @param mixed $detalle
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;
    }

    /**
     * @return mixed
     */
    public function getIngreso()
    {
        return $this->ingreso;
    }

    /**
     * @param mixed $ingreso
     */
    public function setIngreso($ingreso)
    {
        $this->ingreso = $ingreso;
    }

    /**
     * @return mixed
     */
    public function getEgreso()
    {
        return $this->egreso;
    }

    /**
     * @param mixed $egreso
     */
    public function setEgreso($egreso)
    {
        $this->egreso = $egreso;
    }
    public function lista(){
        $sql="SELECT 
  sys_caja_chica_fujo.*,
  sys_caja_conceptos.nombre_concepto,
  sys_moneda.mone_nombre 
FROM
  sys_caja_chica_fujo 
  INNER JOIN sys_caja_conceptos 
    ON sys_caja_chica_fujo.id_concepto = sys_caja_conceptos.id 
  INNER JOIN sys_moneda 
    ON sys_caja_chica_fujo.id_moneda = sys_moneda.mone_id 
    WHERE sys_caja_chica_fujo.id_caja = " . $this->id_caja;
        return $this->conexion->query($sql);
    }

    public function insertar(){
        $sql="    INSERT INTO sys_caja_chica_fujo
VALUES (null,
        '$this->id_caja',
        '$this->hora',
        '$this->id_concepto',
        '$this->id_moneda',
        '$this->detalle',
        '$this->ingreso',
        '$this->egreso');  ";
        return $this->conexion->query($sql);
    }
    public function actualizar(){
        $sql="UPDATE sys_caja_chica_fujo
SET 
  id_caja = '$this->id_caja',
  hora = '$this->hora',
  id_concepto = '$this->id_concepto',
  id_moneda = '$this->id_moneda',
  detalle = '$this->detalle',
  ingreso = '$this->ingreso',
  egreso = '$this->egreso'
WHERE id = '$this->id';";
        return $this->conexion->query($sql);
    }


}