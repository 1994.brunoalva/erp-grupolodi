<?php


class Aduana extends DataBase
{
    private $aduana_id;
    private $nombre;
    private $jurisdiccion;
    private $aduana_estado;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_impor_aduana';
        parent::__construct($this->accion, $this->table, 'impor_aduana_id');
    }

    /**
     * @return mixed
     */
    public function getAduanaId()
    {
        return $this->aduana_id;
    }

    /**
     * @param mixed $aduana_id
     */
    public function setAduanaId($aduana_id)
    {
        $this->aduana_id = $aduana_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getJurisdiccion()
    {
        return $this->jurisdiccion;
    }

    /**
     * @param mixed $jurisdiccion
     */
    public function setJurisdiccion($jurisdiccion)
    {
        $this->jurisdiccion = $jurisdiccion;
    }

    /**
     * @return mixed
     */
    public function getAduanaEstado()
    {
        return $this->aduana_estado;
    }

    /**
     * @param mixed $aduana_estado
     */
    public function setAduanaEstado($aduana_estado)
    {
        $this->aduana_estado = $aduana_estado;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}