<?php


class DetalleCotizacion
{
    private $id;
    private $id_cotizacion;
    private $id_producto;
    private $cantidad;
    private $precio_unitario;
    private $producto_desc;
    private $estado;
    private $id_empresa;
    private $descuento;

    private $conexion;

    /**
     * Clientes constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getIdEmpresa()
    {
        return $this->id_empresa;
    }

    /**
     * @param mixed $id_empresa
     */
    public function setIdEmpresa($id_empresa)
    {
        $this->id_empresa = $id_empresa;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdCotizacion()
    {
        return $this->id_cotizacion;
    }

    /**
     * @param mixed $id_cotizacion
     */
    public function setIdCotizacion($id_cotizacion)
    {
        $this->id_cotizacion = $id_cotizacion;
    }

    /**
     * @return mixed
     */
    public function getIdProducto()
    {
        return $this->id_producto;
    }

    /**
     * @param mixed $id_producto
     */
    public function setIdProducto($id_producto)
    {
        $this->id_producto = $id_producto;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return mixed
     */
    public function getPrecioUnitario()
    {
        return $this->precio_unitario;
    }

    /**
     * @param mixed $precio_unitario
     */
    public function setPrecioUnitario($precio_unitario)
    {
        $this->precio_unitario = $precio_unitario;
    }

    /**
     * @return mixed
     */
    public function getProductoDesc()
    {
        return $this->producto_desc;
    }

    /**
     * @param mixed $producto_desc
     */
    public function setProductoDesc($producto_desc)
    {
        $this->producto_desc = $producto_desc;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * @param mixed $descuento
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;
    }

    /**
     * @return mysqli
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    /**
     * @param mysqli $conexion
     */
    public function setConexion($conexion)
    {
        $this->conexion = $conexion;
    }

    function insertar(){
        $sql = "INSERT INTO sys_coti_detalle
VALUES (null,
        '$this->id_cotizacion',
        '$this->id_producto',
        '$this->id_empresa',
        '$this->cantidad',
        '$this->descuento',
        '$this->precio_unitario',
        '$this->producto_desc',
        '$this->estado');";
        //echo $sql;
        $res = $this->conexion->query($sql);
        $this->id= $this->conexion->insert_id;
        return $res;
    }
    function verLista(){
        $sql="SELECT 
  sys_coti_detalle.id,
  sys_producto.produ_id,
  sys_producto.produ_desc,
  sys_producto.produ_nombre,
  sys_com_marca.mar_nombre,
  sys_producto.produ_sku,
  sys_pais.pais_nombre,
  sys_empresas.emp_nombre,
  sys_empresas.emp_id,
  sys_coti_detalle.cantidad,
  sys_coti_detalle.precio_unitario,
  sys_coti_detalle.descuento
FROM
  sys_producto 
  INNER JOIN sys_produ_detalle 
    ON sys_producto.produ_deta_id = sys_produ_detalle.produ_deta_id 
  INNER JOIN sys_com_marca 
    ON sys_producto.mar_id = sys_com_marca.mar_id 
  INNER JOIN sys_pais 
    ON sys_producto.pais_id = sys_pais.pais_id 
  INNER JOIN sys_coti_detalle 
    ON sys_producto.produ_id = sys_coti_detalle.id_producto 
  INNER JOIN sys_empresas 
    ON sys_coti_detalle.id_empresa = sys_empresas.emp_id 
WHERE sys_coti_detalle.id_cotizacion =  $this->id_cotizacion ";
       // echo  $sql."<br>";
        return $this->conexion->query($sql);
    }

    function elimarPorCotizacion(){
        $sql ="DELETE FROM sys_coti_detalle WHERE id_cotizacion = " . $this->getIdCotizacion();
        //echo $sql;
        return $this->conexion->query($sql);
    }

}