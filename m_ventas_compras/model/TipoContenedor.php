<?php


class TipoContenedor extends DataBase
{
    private $tipo_id;
    private $nombre;
    private $descripccion;
    private $estado;


    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_tipo_contenedor';
        parent::__construct($this->accion, $this->table, 'ticon_id');
    }

    /**
     * @return mixed
     */
    public function getTipoId()
    {
        return $this->tipo_id;
    }

    /**
     * @param mixed $tipo_id
     */
    public function setTipoId($tipo_id)
    {
        $this->tipo_id = $tipo_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getDescripccion()
    {
        return $this->descripccion;
    }

    /**
     * @param mixed $descripccion
     */
    public function setDescripccion($descripccion)
    {
        $this->descripccion = $descripccion;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}