<?php


class Linea extends DataBase
{
    private $linea_id;
    private $codigo;
    private $nombre;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_linea';
        parent::__construct($this->accion, $this->table, 'linea_id');
    }

    /**
     * @return mixed
     */
    public function getLineaId()
    {
        return $this->linea_id;
    }

    /**
     * @param mixed $linea_id
     */
    public function setLineaId($linea_id)
    {
        $this->linea_id = $linea_id;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}