<?php

class Incoterm extends DataBase
{
    private $incoterm_id;
    private $nombre;
    private $descripccion;
    private $estado;


    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_incoterm';
        parent::__construct($this->accion, $this->table, 'inco_id');
    }

    /**
     * @return mixed
     */
    public function getIncotermId()
    {
        return $this->incoterm_id;
    }

    /**
     * @param mixed $incoterm_id
     */
    public function setIncotermId($incoterm_id)
    {
        $this->incoterm_id = $incoterm_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getDescripccion()
    {
        return $this->descripccion;
    }

    /**
     * @param mixed $descripccion
     */
    public function setDescripccion($descripccion)
    {
        $this->descripccion = $descripccion;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}