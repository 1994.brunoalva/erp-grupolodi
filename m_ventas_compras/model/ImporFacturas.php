<?php


class ImporFacturas
{

    private $impor_fac_id;
    private $impor_id;
    private $fecha;
    private $tipo_doc;
    private $numero;
    private $proveedor_id;
    private $ruc;
    private $taza_cambio;
    private $monto;
    private $archivo;
    private $estado;

    private $conexion;

    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    public  function getDatos(){
        $sql="SELECT * FROM  sys_impor_facturas WHERE impor_id = " . $this->impor_id;
        return $this->conexion->query($sql);
    }

    public function insertar(){
        $sql="INSERT INTO sys_impor_facturas
                VALUES (null,
                        '$this->impor_id',
                        '$this->fecha',
                        '$this->tipo_doc',
                        '$this->numero',
                        '$this->proveedor_id',
                        '$this->ruc',
                        '$this->taza_cambio',
                        '$this->monto',
                        '$this->archivo',
                        '1');";
        return $this->conexion->query($sql);
    }

    public function eliminar(){
        $sql="DELETE FROM sys_impor_facturas WHERE impor_fac_id = '$this->impor_fac_id';";
        return $this->conexion->query($sql);
    }

}