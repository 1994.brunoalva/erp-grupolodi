<?php


class CodSunat extends DataBase
{
    private $codigo_id;
    private $codigo_sunat;
    private $descripccion;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_sunat_codigo_prod';
        parent::__construct($this->accion, $this->table, 'sunat_cod_id');
    }

    /**
     * @return mixed
     */
    public function getCodigoId()
    {
        return $this->codigo_id;
    }

    /**
     * @param mixed $codigo_id
     */
    public function setCodigoId($codigo_id)
    {
        $this->codigo_id = $codigo_id;
    }

    /**
     * @return mixed
     */
    public function getCodigoSunat()
    {
        return $this->codigo_sunat;
    }

    /**
     * @param mixed $codigo_sunat
     */
    public function setCodigoSunat($codigo_sunat)
    {
        $this->codigo_sunat = $codigo_sunat;
    }

    /**
     * @return mixed
     */
    public function getDescripccion()
    {
        return $this->descripccion;
    }

    /**
     * @param mixed $descripccion
     */
    public function setDescripccion($descripccion)
    {
        $this->descripccion = $descripccion;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}