<?php


class TasaPoliza extends DataBase
{
    private $tasa_id;
    private $tporcentaje;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_tasa_poliza';
        parent::__construct($this->accion, $this->table, 'tasa_poliza_id');
    }

    /**
     * @return mixed
     */
    public function getTasaId()
    {
        return $this->tasa_id;
    }

    /**
     * @param mixed $tasa_id
     */
    public function setTasaId($tasa_id)
    {
        $this->tasa_id = $tasa_id;
    }

    /**
     * @return mixed
     */
    public function getTporcentaje()
    {
        return $this->tporcentaje;
    }

    /**
     * @param mixed $tporcentaje
     */
    public function setTporcentaje($tporcentaje)
    {
        $this->tporcentaje = $tporcentaje;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}