<?php


class Poliza extends DataBase
{
    private $poliza_id;
    private $suma_asegurada;
    private $referencia;
    private $vigencia_desde;
    private $vigencia_hasta;
    private $num_polica_id;
    private $aplicacion;
    private $taza_id;
    private $prima_neta;
    private $prima_total;
    private $cif_total;
    private $folder_id;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_com_poliza';
        parent::__construct($this->accion, $this->table, 'pol_id');
    }

    /**
     * @return mixed
     */
    public function getPolizaId()
    {
        return $this->poliza_id;
    }

    /**
     * @param mixed $poliza_id
     */
    public function setPolizaId($poliza_id)
    {
        $this->poliza_id = $poliza_id;
    }

    /**
     * @return mixed
     */
    public function getSumaAsegurada()
    {
        return $this->suma_asegurada;
    }

    /**
     * @param mixed $suma_asegurada
     */
    public function setSumaAsegurada($suma_asegurada)
    {
        $this->suma_asegurada = $suma_asegurada;
    }

    /**
     * @return mixed
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * @param mixed $referencia
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;
    }

    /**
     * @return mixed
     */
    public function getVigenciaDesde()
    {
        return $this->vigencia_desde;
    }

    /**
     * @param mixed $vigencia_desde
     */
    public function setVigenciaDesde($vigencia_desde)
    {
        $this->vigencia_desde = $vigencia_desde;
    }

    /**
     * @return mixed
     */
    public function getVigenciaHasta()
    {
        return $this->vigencia_hasta;
    }

    /**
     * @param mixed $vigencia_hasta
     */
    public function setVigenciaHasta($vigencia_hasta)
    {
        $this->vigencia_hasta = $vigencia_hasta;
    }

    /**
     * @return mixed
     */
    public function getNumPolicaId()
    {
        return $this->num_polica_id;
    }

    /**
     * @param mixed $num_polica_id
     */
    public function setNumPolicaId($num_polica_id)
    {
        $this->num_polica_id = $num_polica_id;
    }

    /**
     * @return mixed
     */
    public function getAplicacion()
    {
        return $this->aplicacion;
    }

    /**
     * @param mixed $aplicacion
     */
    public function setAplicacion($aplicacion)
    {
        $this->aplicacion = $aplicacion;
    }

    /**
     * @return mixed
     */
    public function getTazaId()
    {
        return $this->taza_id;
    }

    /**
     * @param mixed $taza_id
     */
    public function setTazaId($taza_id)
    {
        $this->taza_id = $taza_id;
    }

    /**
     * @return mixed
     */
    public function getPrimaNeta()
    {
        return $this->prima_neta;
    }

    /**
     * @param mixed $prima_neta
     */
    public function setPrimaNeta($prima_neta)
    {
        $this->prima_neta = $prima_neta;
    }

    /**
     * @return mixed
     */
    public function getPrimaTotal()
    {
        return $this->prima_total;
    }

    /**
     * @param mixed $prima_total
     */
    public function setPrimaTotal($prima_total)
    {
        $this->prima_total = $prima_total;
    }

    /**
     * @return mixed
     */
    public function getCifTotal()
    {
        return $this->cif_total;
    }

    /**
     * @param mixed $cif_total
     */
    public function setCifTotal($cif_total)
    {
        $this->cif_total = $cif_total;
    }

    /**
     * @return mixed
     */
    public function getFolderId()
    {
        return $this->folder_id;
    }

    /**
     * @param mixed $folder_id
     */
    public function setFolderId($folder_id)
    {
        $this->folder_id = $folder_id;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}