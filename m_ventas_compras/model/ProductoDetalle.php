<?php


class ProductoDetalle extends DataBase
{
    private $detalle_id;
    private $producto_id;
    private $camara_medida;
    private $camara_aro;
    private $camara_valvula;
    private $aro_modelo;
    private $aro_medida;
    private $aro_espesor;
    private $aro_huecos;
    private $aro_espesor_huecos;
    private $aro_cbd;
    private $aro_pcd;
    private $aro_offset;
    private $neumatico_ancho;
    private $neumatico_serie;
    private $neumatico_aro;
    private $neumatico_pliegue;
    private $neumatico_set;
    private $neumatico_uso;
    private $neumatico_material;



    private $neumatico_ancho_adua;
    private $neumatico_serie_adua;
    private $neumatico_tp_constante;
    private $neumatico_carga;
    private $neumatico_pisa;
    private $neumatico_externo;
    private $neumatico_velocidad;
    private $neumatico_constante;
    private $neumatico_item;
    private $neumatico_vigencia;
    private $neumatico_confor;
    private $neumatico_partida;
    private $neumatico_medida;

    private $estado;


    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_produ_detalle';
        parent::__construct($this->accion, $this->table, 'produ_deta_id');
    }

    /**
     * @return mixed
     */
    public function getDetalleId()
    {
        return $this->detalle_id;
    }

    /**
     * @param mixed $detalle_id
     */
    public function setDetalleId($detalle_id)
    {
        $this->detalle_id = $detalle_id;
    }

    /**
     * @return mixed
     */
    public function getProductoId()
    {
        return $this->producto_id;
    }

    /**
     * @param mixed $producto_id
     */
    public function setProductoId($producto_id)
    {
        $this->producto_id = $producto_id;
    }

    /**
     * @return mixed
     */
    public function getCamaraMedida()
    {
        return $this->camara_medida;
    }

    /**
     * @param mixed $camara_medida
     */
    public function setCamaraMedida($camara_medida)
    {
        $this->camara_medida = $camara_medida;
    }

    /**
     * @return mixed
     */
    public function getCamaraAro()
    {
        return $this->camara_aro;
    }

    /**
     * @param mixed $camara_aro
     */
    public function setCamaraAro($camara_aro)
    {
        $this->camara_aro = $camara_aro;
    }

    /**
     * @return mixed
     */
    public function getCamaraValvula()
    {
        return $this->camara_valvula;
    }

    /**
     * @param mixed $camara_valvula
     */
    public function setCamaraValvula($camara_valvula)
    {
        $this->camara_valvula = $camara_valvula;
    }

    /**
     * @return mixed
     */
    public function getAroModelo()
    {
        return $this->aro_modelo;
    }

    /**
     * @param mixed $aro_modelo
     */
    public function setAroModelo($aro_modelo)
    {
        $this->aro_modelo = $aro_modelo;
    }

    /**
     * @return mixed
     */
    public function getAroMedida()
    {
        return $this->aro_medida;
    }

    /**
     * @param mixed $aro_medida
     */
    public function setAroMedida($aro_medida)
    {
        $this->aro_medida = $aro_medida;
    }

    /**
     * @return mixed
     */
    public function getAroEspesor()
    {
        return $this->aro_espesor;
    }

    /**
     * @param mixed $aro_espesor
     */
    public function setAroEspesor($aro_espesor)
    {
        $this->aro_espesor = $aro_espesor;
    }

    /**
     * @return mixed
     */
    public function getAroHuecos()
    {
        return $this->aro_huecos;
    }

    /**
     * @param mixed $aro_huecos
     */
    public function setAroHuecos($aro_huecos)
    {
        $this->aro_huecos = $aro_huecos;
    }

    /**
     * @return mixed
     */
    public function getAroEspesorHuecos()
    {
        return $this->aro_espesor_huecos;
    }

    /**
     * @param mixed $aro_espesor_huecos
     */
    public function setAroEspesorHuecos($aro_espesor_huecos)
    {
        $this->aro_espesor_huecos = $aro_espesor_huecos;
    }

    /**
     * @return mixed
     */
    public function getAroCbd()
    {
        return $this->aro_cbd;
    }

    /**
     * @param mixed $aro_cbd
     */
    public function setAroCbd($aro_cbd)
    {
        $this->aro_cbd = $aro_cbd;
    }

    /**
     * @return mixed
     */
    public function getAroPcd()
    {
        return $this->aro_pcd;
    }

    /**
     * @param mixed $aro_pcd
     */
    public function setAroPcd($aro_pcd)
    {
        $this->aro_pcd = $aro_pcd;
    }

    /**
     * @return mixed
     */
    public function getAroOffset()
    {
        return $this->aro_offset;
    }

    /**
     * @param mixed $aro_offset
     */
    public function setAroOffset($aro_offset)
    {
        $this->aro_offset = $aro_offset;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoAncho()
    {
        return $this->neumatico_ancho;
    }

    /**
     * @param mixed $neumatico_ancho
     */
    public function setNeumaticoAncho($neumatico_ancho)
    {
        $this->neumatico_ancho = $neumatico_ancho;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoSerie()
    {
        return $this->neumatico_serie;
    }

    /**
     * @param mixed $neumatico_serie
     */
    public function setNeumaticoSerie($neumatico_serie)
    {
        $this->neumatico_serie = $neumatico_serie;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoAro()
    {
        return $this->neumatico_aro;
    }

    /**
     * @param mixed $neumatico_aro
     */
    public function setNeumaticoAro($neumatico_aro)
    {
        $this->neumatico_aro = $neumatico_aro;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoPliegue()
    {
        return $this->neumatico_pliegue;
    }

    /**
     * @param mixed $neumatico_pliegue
     */
    public function setNeumaticoPliegue($neumatico_pliegue)
    {
        $this->neumatico_pliegue = $neumatico_pliegue;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoSet()
    {
        return $this->neumatico_set;
    }

    /**
     * @param mixed $neumatico_set
     */
    public function setNeumaticoSet($neumatico_set)
    {
        $this->neumatico_set = $neumatico_set;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoUso()
    {
        return $this->neumatico_uso;
    }

    /**
     * @param mixed $neumatico_uso
     */
    public function setNeumaticoUso($neumatico_uso)
    {
        $this->neumatico_uso = $neumatico_uso;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoMaterial()
    {
        return $this->neumatico_material;
    }

    /**
     * @param mixed $neumatico_material
     */
    public function setNeumaticoMaterial($neumatico_material)
    {
        $this->neumatico_material = $neumatico_material;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoAnchoAdua()
    {
        return $this->neumatico_ancho_adua;
    }

    /**
     * @param mixed $neumatico_ancho_adua
     */
    public function setNeumaticoAnchoAdua($neumatico_ancho_adua)
    {
        $this->neumatico_ancho_adua = $neumatico_ancho_adua;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoSerieAdua()
    {
        return $this->neumatico_serie_adua;
    }

    /**
     * @param mixed $neumatico_serie_adua
     */
    public function setNeumaticoSerieAdua($neumatico_serie_adua)
    {
        $this->neumatico_serie_adua = $neumatico_serie_adua;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoTpConstante()
    {
        return $this->neumatico_tp_constante;
    }

    /**
     * @param mixed $neumatico_tp_constante
     */
    public function setNeumaticoTpConstante($neumatico_tp_constante)
    {
        $this->neumatico_tp_constante = $neumatico_tp_constante;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoCarga()
    {
        return $this->neumatico_carga;
    }

    /**
     * @param mixed $neumatico_carga
     */
    public function setNeumaticoCarga($neumatico_carga)
    {
        $this->neumatico_carga = $neumatico_carga;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoPisa()
    {
        return $this->neumatico_pisa;
    }

    /**
     * @param mixed $neumatico_pisa
     */
    public function setNeumaticoPisa($neumatico_pisa)
    {
        $this->neumatico_pisa = $neumatico_pisa;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoExterno()
    {
        return $this->neumatico_externo;
    }

    /**
     * @param mixed $neumatico_externo
     */
    public function setNeumaticoExterno($neumatico_externo)
    {
        $this->neumatico_externo = $neumatico_externo;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoVelocidad()
    {
        return $this->neumatico_velocidad;
    }

    /**
     * @param mixed $neumatico_velocidad
     */
    public function setNeumaticoVelocidad($neumatico_velocidad)
    {
        $this->neumatico_velocidad = $neumatico_velocidad;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoConstante()
    {
        return $this->neumatico_constante;
    }

    /**
     * @param mixed $neumatico_constante
     */
    public function setNeumaticoConstante($neumatico_constante)
    {
        $this->neumatico_constante = $neumatico_constante;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoItem()
    {
        return $this->neumatico_item;
    }

    /**
     * @param mixed $neumatico_item
     */
    public function setNeumaticoItem($neumatico_item)
    {
        $this->neumatico_item = $neumatico_item;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoVigencia()
    {
        return $this->neumatico_vigencia;
    }

    /**
     * @param mixed $neumatico_vigencia
     */
    public function setNeumaticoVigencia($neumatico_vigencia)
    {
        $this->neumatico_vigencia = $neumatico_vigencia;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoConfor()
    {
        return $this->neumatico_confor;
    }

    /**
     * @param mixed $neumatico_confor
     */
    public function setNeumaticoConfor($neumatico_confor)
    {
        $this->neumatico_confor = $neumatico_confor;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoPartida()
    {
        return $this->neumatico_partida;
    }

    /**
     * @param mixed $neumatico_partida
     */
    public function setNeumaticoPartida($neumatico_partida)
    {
        $this->neumatico_partida = $neumatico_partida;
    }

    /**
     * @return mixed
     */
    public function getNeumaticoMedida()
    {
        return $this->neumatico_medida;
    }

    /**
     * @param mixed $neumatico_medida
     */
    public function setNeumaticoMedida($neumatico_medida)
    {
        $this->neumatico_medida = $neumatico_medida;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}