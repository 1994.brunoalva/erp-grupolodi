<?php


class CajaChica
{
    private $id;
    private $fecha;
    private $vendedor;
    private $id_empresa;
    private $total_soles;
    private $total_dolares;
    private $estado;

    private $conexion;


    /**
     * FlujoCajaChica constructor.
     */
    public function __construct()
    {
        $this->conexion = (new Conexion())->getConexion();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor($vendedor)
    {
        $this->vendedor = $vendedor;
    }

    /**
     * @return mixed
     */
    public function getIdEmpresa()
    {
        return $this->id_empresa;
    }

    /**
     * @param mixed $id_empresa
     */
    public function setIdEmpresa($id_empresa)
    {
        $this->id_empresa = $id_empresa;
    }

    /**
     * @return mixed
     */
    public function getTotalSoles()
    {
        return $this->total_soles;
    }

    /**
     * @param mixed $total_soles
     */
    public function setTotalSoles($total_soles)
    {
        $this->total_soles = $total_soles;
    }

    /**
     * @return mixed
     */
    public function getTotalDolares()
    {
        return $this->total_dolares;
    }

    /**
     * @param mixed $total_dolares
     */
    public function setTotalDolares($total_dolares)
    {
        $this->total_dolares = $total_dolares;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function lista(){
        $sql="SELECT sys_caja_chica.*, sys_empresas.emp_nombre FROM  sys_caja_chica 
INNER JOIN sys_empresas ON sys_caja_chica.id_empresa= sys_empresas.emp_id";
        return $this->conexion->query($sql);
    }

    public function insertar(){
        $sql="INSERT INTO sys_caja_chica
VALUES (null,
        '$this->fecha',
        '$this->vendedor',
        '$this->id_empresa',
        '$this->total_soles',
        '$this->total_dolares',
        '$this->estado');";
        $res =  $this->conexion->query($sql);
        if ($res){
            $this->id = $this->conexion->insert_id;
        }

        return $res;
    }
    public function actualizar(){
        $sql="UPDATE sys_caja_chica
SET 
  fecha = '$this->fecha',
  vendedor = '$this->vendedor',
  id_empresa = '$this->id_empresa',
  total_soles = '$this->total_soles',
  total_dolares = '$this->total_dolares'
WHERE id = '$this->id';";
        return $this->conexion->query($sql);
    }


}