<?php


class Nomenglatura extends DataBase
{
    private $nomen_id;
    private $nombre;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_nomen';
        parent::__construct($this->accion, $this->table, 'nom_id');
    }

    /**
     * @return mixed
     */
    public function getNomenId()
    {
        return $this->nomen_id;
    }

    /**
     * @param mixed $nomen_id
     */
    public function setNomenId($nomen_id)
    {
        $this->nomen_id = $nomen_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}