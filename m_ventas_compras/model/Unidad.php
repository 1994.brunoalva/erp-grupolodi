<?php


class Unidad extends DataBase
{
    private $unidad_id;
    private $nombre;
    private $estado;

    private $sql;
    private $accion;
    private $table;

    function __construct($accion)
    {
        $this->accion = $accion;
        $this->table = 'sys_unidad';
        parent::__construct($this->accion, $this->table, 'unidad_id');
    }

    /**
     * @return mixed
     */
    public function getUnidadId()
    {
        return $this->unidad_id;
    }

    /**
     * @param mixed $unidad_id
     */
    public function setUnidadId($unidad_id)
    {
        $this->unidad_id = $unidad_id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


}