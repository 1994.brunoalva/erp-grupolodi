<?php

require '../../conexion/Conexion.php';
require '../../model/AgensiaTransporte.php';

$agensiaTransporte= new AgensiaTransporte();

$agensiaTransporte->setId($_POST['id']);
$agensiaTransporte->setTelefono($_POST['telefono']);
$agensiaTransporte->setRuc($_POST['ruc']);
$agensiaTransporte->setEstado($_POST['estado']);
$agensiaTransporte->setCondicion($_POST['condicion']);
$agensiaTransporte->setRazonSocial($_POST['nombresocial']);
$agensiaTransporte->setDireccion($_POST['direccion']);

$arrRes = array('res' => false);
if ($agensiaTransporte->update()){
    $arrRes['res']=true;
    $arrRes['data']=$agensiaTransporte->getArrayData();

}

echo json_encode($arrRes);