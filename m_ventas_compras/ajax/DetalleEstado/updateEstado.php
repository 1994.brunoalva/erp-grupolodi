<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/DetalleEstado.php';

$detalleEstado = new DetalleEstado('UPDATE');
$datos = json_decode($_POST['array']);
$resultSet = $detalleEstado->updateById($datos);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

