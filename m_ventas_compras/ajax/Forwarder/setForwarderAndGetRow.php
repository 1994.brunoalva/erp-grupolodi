<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Forwarder.php';

$forwarder = new Forwarder('INSERT');
$datos = json_decode($_POST['array']);
$id = $forwarder->insertAllAndGetId($datos);

$forwarder = new Forwarder('SELECT');
$resultSet = $forwarder->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

