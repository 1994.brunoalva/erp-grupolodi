<?php

require '../../conexion/Conexion.php';
require '../../model/Venta.php';
require '../../model/DetalleVenta.php';
require '../../model/Cotizacion.php';
require '../../model/DetalleCotizacion.php';
require '../../model/DocumentoVentaEmpresa.php';
require '../../utils/Tools.php';




$venta = new Venta();
$detalleVenta=new DetalleVenta();
$cotizacion = new Cotizacion();
$detalleCotizacion=new DetalleCotizacion();
$documentoVentaEmpresa= new DocumentoVentaEmpresa();
$tools = new Tools();

$idCoti = $_POST['idcoti'];

$cotizacion->setCotiId($idCoti);

$res_coti = $cotizacion->verCotizacion()->fetch_assoc();

$venta->setIdCoti($idCoti);

$detalleCotizacion->setIdCotizacion($idCoti);
$resDetalleCoti = $detalleCotizacion->verLista();
$res = array("res"=>true);
$res_ventas = $venta->listaProCoti();

$productosSeparadosEmpresas=[];

foreach ($res_ventas as $row){
    $detalleVenta->setIdVenta($row['id']);
    $res_proDeta = $detalleVenta->listaProVentaresumen();
    $row['numero']= $tools->numeroParaDocumento($row['numero'],5);
    $row['totalpro']=0;
    $row['igv']=0;
    $row['subtotal']=0;
    $row['productosVe']=[];
    foreach ($res_proDeta as $roRR){
        $row['productosVe'][]=$roRR;
        $row['subtotal']+= $roRR['cantidad'] * $roRR['precio_unitario'];
    }
    $row['igv']=$row['subtotal']*0.18;
    $row['totalpro']=$row['igv'] + $row['subtotal'];

    $productosSeparadosEmpresas[]=$row;

}

echo json_encode($productosSeparadosEmpresas);
