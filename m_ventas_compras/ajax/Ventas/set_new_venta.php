<?php

require '../../conexion/Conexion.php';
require '../../model/Venta.php';
require '../../model/DetalleVenta.php';
require '../../model/Cotizacion.php';
require '../../model/DetalleCotizacion.php';
require '../../model/DocumentoVentaEmpresa.php';
require '../../utils/SendCurlVenta.php';


$venta = new Venta();
$detalleVenta=new DetalleVenta();
$cotizacion = new Cotizacion();
$detalleCotizacion=new DetalleCotizacion();
$documentoVentaEmpresa= new DocumentoVentaEmpresa();
$sendCurlVenta= new SendCurlVenta();

$sendCurlVenta->setUrlCurl("http://localhost/proyecto/generadorBF/generates/");

$idCoti = $_POST['idcoti'];

$cotizacion->setCotiId($idCoti);

$res_coti = $cotizacion->verCotizacion()->fetch_assoc();

$detalleCotizacion->setIdCotizacion($idCoti);
$resDetalleCoti = $detalleCotizacion->verLista();
$res = array("res"=>true,'extra'=>array());

$productosSeparadosEmpresas=[];
foreach ($resDetalleCoti as $row){
    if (count($productosSeparadosEmpresas)>0){
        $vali=true;
        for($in=0;$in<count($productosSeparadosEmpresas);$in++){
            if (count($productosSeparadosEmpresas[$in]["productos"])>0){
                if ($productosSeparadosEmpresas[$in]['idEmpre']==$row['emp_id']){
                    $productosSeparadosEmpresas[$in]["monto"]+=$row['cantidad']*$row['precio_unitario'];
                    $productosSeparadosEmpresas[$in]["productos"][]=$row;
                    $vali=false;
                }
            }

        }
        if ($vali){
            //$productosSeparadosEmpresas[]=array($row);
            $productosSeparadosEmpresas[]=array(
                "idEmpre"=>$row['emp_id'],
                "monto"=>$row['cantidad']*$row['precio_unitario'],
                "productos"=>array($row)
            );
        }
    }else{
        $productosSeparadosEmpresas[]=array(
            "idEmpre"=>$row['emp_id'],
            "monto"=>$row['cantidad']*$row['precio_unitario'],
            "productos"=>array($row)
        );
    }
}


foreach ($productosSeparadosEmpresas as $emprVent){

    $documentoVentaEmpresa->setIdDocEmp($_POST['tipoventa']);
    $documentoVentaEmpresa->setIdEmpre($emprVent['idEmpre']);
    $res_doc_venta = $documentoVentaEmpresa->getData()->fetch_assoc();
    $venta->setIdCliente($_POST['idcliente']);
    $venta->setEstado('1');
    $venta->setIdCoti($_POST['idcoti']);
    $venta->setMoneda($_POST['moneda']);
    $venta->setMonto($emprVent['monto']);
    $venta->setFecha($_POST['fecha']);
    $venta->setTipoVenta($_POST['tipoventa']);
    $venta->setTipoPago($_POST['idpago']);
    $venta->setIdEmpresa($emprVent['idEmpre']);
    $venta->setHora($_POST['hora']);
    $venta->setSerie($res_doc_venta['serie']);
    $venta->setNumero($res_doc_venta['numero']);
    $venta->setTotal(0);

    if ($venta->insertar()){

        foreach ($emprVent['productos'] as $row){
            $detalleVenta->setEstado(0);
            $detalleVenta->setIdCotiDeta($row['id']);
            $detalleVenta->setIdVenta($venta->getId());
            if (!$detalleVenta->insertar()){
                $res['res']=false;

            }
        }
        //if ($res['res']){}
        $sendCurlVenta->setTipo(3);
        $sendCurlVenta->setDataSend(array('id_venta'=>$venta->getId()));
        $res['extra'][]=$sendCurlVenta->enviar();
    }else{
        $res['res']=false;
    }

}

if ($res['res']){
    $cotizacion->setEstado(2);
    $cotizacion->setCotiFechaVenta($_POST['fecha']);
    $cotizacion->actualizarEstadoVenta();

}

//echo json_encode($productosSeparadosEmpresas);
/*
$res = array("res"=>true);

if ($venta->insertar()){
    $res['res']=true;
    $res['idVenta'] = $venta->getId();
}*/

echo json_encode($res);

/*$venta->setEstado('1');
$venta->setFecha($_POST['fecha']);
$venta->setMonto($_POST['monto']);
$venta->setMoneda($_POST['moneda']);
$venta->setIdCoti($_POST['idcoti']);
$venta->setTipoPago($_POST['idpago']);
$venta->setIdCliente($_POST['idcliente']);
$venta->setTipoVenta($_POST['tipoventa']);*/
/*
$venta->setIdCliente();
$venta->setEstado('1');
$venta->setIdCoti($_POST['idcoti']);
$venta->setMoneda($_POST['moneda']);
$venta->setMonto($_POST['monto']);
$venta->setFecha($_POST['fecha']);
$venta->setTipoVenta($_POST['tipoventa']);
$venta->setTipoPago($_POST['idpago']);
$venta->setIdEmpresa();
$venta->setHora();
$venta->setTotal(0);

$detalleVenta->setEstado();
$detalleVenta->setIdCotiDeta();
$detalleVenta->setIdVenta();*/

/*
$res = array("res"=>false);

if ($venta->insertar()){
    $res['res']=true;
    $res['idVenta'] = $venta->getId();
}

echo json_encode($res);
*/