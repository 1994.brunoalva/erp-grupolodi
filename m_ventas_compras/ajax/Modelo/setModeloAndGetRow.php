<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Modelo.php';

$modelo = new Modelo('INSERT');
$datos = json_decode($_POST['array']);
$id = $modelo->insertAllAndGetId($datos);
$modelo = new Modelo('SELECT');
$resultSet = $modelo->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

