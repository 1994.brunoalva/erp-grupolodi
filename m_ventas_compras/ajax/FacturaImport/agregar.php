<?php

require "../../conexion/Conexion.php";

$conexion = (new Conexion())->getConexion();

$sql =" INSERT INTO sys_impor_facturas
VALUES (NULL,
        '{$_POST['import']}',
        '{$_POST['fecha']}',
        '{$_POST['tipo']}',
        '{$_POST['empresa']}',
        '{$_POST['num']}',
        '{$_POST['ruc']}',
        '{$_POST['tc']}',
        '{$_POST['monto']}',
        '{$_POST['nomFile']}',
        '1');";

$res = $conexion->query($sql);
$resultado= array("res" =>false);
$resultado['data'] = array('impor_fac_id'=>0,
        'impor_id'=>$_POST['import'],
        'fecha'=>$_POST['fecha'],
        'tipo_doc'=>$_POST['tipo'],
        'proveedor'=>$_POST['empresa'],
        'numero'=>$_POST['num'],
        'ruc'=>$_POST['ruc'],
        'taza_cambio'=>$_POST['tc'],
        'monto'=>$_POST['monto'],
        'archivo'=>$_POST['nomFile'],
        'estado'=>'1');

if ($res){
    $resultado['res'] = true;
    $resultado['data']['impor_fac_id'] = $conexion->insert_id;
}


echo json_encode($resultado);