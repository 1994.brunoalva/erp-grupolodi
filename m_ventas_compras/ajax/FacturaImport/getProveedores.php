<?php
require "../../conexion/Conexion.php";
$conexion = (new Conexion())->getConexion();

$idImport=$_POST['inport'];


$listaProvee=[];
$listaProvee[]=array("nombre"=>'MAPFRE PERU', "ruc"=>'',"tipo"=>"SEGURO");


$sql="SELECT 
  adua.age_adua_nombre,
  adua.age_adua_ruc,
  adua.age_adua_id 
FROM
  sys_impor_aduana AS impor_adua 
  INNER JOIN sys_agencia_aduana AS adua 
    ON impor_adua.age_adua_id = adua.age_adua_id
    WHERE impor_adua.impor_id = ".$idImport;
$resulAdua = $conexion->query($sql);


foreach ($resulAdua as $rowadua){
    $listaProvee[]=array("nombre"=>$rowadua['age_adua_nombre'], "ruc"=>$rowadua['age_adua_ruc'],"tipo"=>"ADUANAS");
}


$sql="SELECT 
  folwar.forwa_nombre,
  folwar.forwa_codigo  
FROM
  sys_com_embarque AS embar 
  INNER JOIN sys_com_orden AS orden 
    ON embar.ord_id = orden.ord_id 
    INNER JOIN sys_com_forwarder AS folwar ON embar.forwa_id = folwar.forwa_id
WHERE orden.impor_id =".$idImport;
$resulAdua = $conexion->query($sql);
foreach ($resulAdua as $rowadua3){
    $listaProvee[]=array("nombre"=>$rowadua3['forwa_nombre'], "ruc"=>$rowadua3['forwa_codigo'],"tipo"=>"FORWARDER");
}

$sql ="SELECT 
  almacen.alm_razon_social,
  almacen.alm_ruc
FROM
  sys_com_embarque AS embar 
  INNER JOIN sys_com_orden AS orden 
    ON embar.ord_id = orden.ord_id 
  INNER JOIN sys_com_embarque_detalle AS emb_det 
    ON embar.emb_id = emb_det.emb_id 
  INNER JOIN sys_com_almacen AS almacen 
    ON emb_det.almacen_id = almacen.alm_id 
WHERE orden.impor_id =".$idImport;
$resulAdua = $conexion->query($sql);
foreach ($resulAdua as $rowadua4){
    $listaProvee[]=array("nombre"=>$rowadua4['alm_razon_social'], "ruc"=>$rowadua4['alm_ruc'],"tipo"=>"ALMACEN");
}

$sql ="SELECT 
  linea.linea_nombre
FROM
  sys_com_embarque AS embar 
  INNER JOIN sys_com_orden AS orden 
    ON embar.ord_id = orden.ord_id 
  INNER JOIN sys_com_embarque_detalle AS emb_det 
    ON embar.emb_id = emb_det.emb_id 
  INNER JOIN sys_com_linea AS linea 
    ON emb_det.linea_id = linea.linea_id 
WHERE orden.impor_id =".$idImport;
$resulAdua = $conexion->query($sql);
foreach ($resulAdua as $rowadua5){
    $listaProvee[]=array("nombre"=>$rowadua5['linea_nombre'], "ruc"=>'',"tipo"=>"LINEA");
}

echo json_encode($listaProvee);