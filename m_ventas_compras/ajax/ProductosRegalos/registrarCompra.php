<?php
require "../../conexion/Conexion.php";
require "../../model/CompraNacional.php";
require "../../model/CompraNacionalDetalle.php";

$conexion = (new Conexion())->getConexion();
$compraNacional= new CompraNacional();
$compraNacionalDetalle=new CompraNacionalDetalle();

$productos= json_decode($_POST['productos']);

$compraNacional->setEstado('APROBADO');
$compraNacional->setNumRef($_POST['numDoc']);
$compraNacional->setMonto($_POST['monto']);
$compraNacional->setTipoDoc($_POST['tido']);
$compraNacional->setEmpresaId($_POST['empresa']);
$compraNacional->setArchivo($_POST['docUrl']);
$compraNacional->setCambio($_POST['tc']);
$compraNacional->setComFecha($_POST['fecha']);
$compraNacional->setMoneda($_POST['moneda']);
$compraNacional->setProveedorId($_POST['idProvee']);
$respuesta = array("res"=>false);
if ($compraNacional->insertar()){
    $respuesta['res']=true;
    $compraNacionalDetalle->setComNaciId($compraNacional->getComId());
    foreach ($productos as $prod){
        $compraNacionalDetalle->setCantidad($prod->cantidad);
        $compraNacionalDetalle->setProdId($prod->idProd);
        $compraNacionalDetalle->setImporte($prod->importe);
        $compraNacionalDetalle->setPrecioU($prod->precio);
        if (!$compraNacionalDetalle->insertar()){
            $respuesta['res']=false;
        }
    }

    if ($respuesta['res']){
        foreach ($productos as $prod){
            //echo "SELECT * FROM sys_producto_empresa WHERE id_prod=" .$prod->idProd." AND id_empresa = ".$_POST['empresa'];
            $resEmOr = $conexion->query("SELECT * FROM sys_producto_empresa WHERE id_prod=" .$prod->idProd." AND id_empresa = ".$_POST['empresa']);
            if ($rowEo= $resEmOr->fetch_array() ){

                $sql ="SELECT *
                    FROM sys_prod_promocion
                    WHERE prod_emp_id = ".$rowEo['prod_empre_id'];
                $respPRomo = $conexion->query($sql);

                if ($rowPromo = $respPRomo->fetch_assoc()){
                    $sql = "UPDATE sys_prod_promocion
                        SET 
                          cantidad =  cantidad + $prod->cantidad
                        WHERE prod_emp_id = ". $rowEo['prod_empre_id'];
                    $conexion->query($sql);

                }else{
                    $sql = "INSERT INTO sys_prod_promocion
                        VALUES (null,
                                '{$rowEo['prod_empre_id']}',
                                '$prod->idProd',
                                '{$_POST['empresa']}',
                                '0',
                                '$prod->cantidad',
                                'REGALO');";

                    $conexion->query($sql);
                }


            }else{

                $conexion->query("INSERT INTO sys_producto_empresa
                                    VALUES (null,
                                            '$prod->idProd',
                                            '',
                                            '{$_POST['empresa']}',
                                            '0',
                                            'a');");
                $idProdEmpresa =$conexion->insert_id;
                $sql = "INSERT INTO sys_prod_promocion
                        VALUES (null,
                                '$idProdEmpresa',
                                '$prod->idProd',
                                '{$_POST['empresa']}',
                                '0',
                                '$prod->cantidad',
                                'REGALO');";

                $conexion->query($sql);

            }
        }
    }
}
echo json_encode($respuesta);





