<?php

require "../../conexion/Conexion.php";
require "../../model/CompraNacional.php";
require "../../model/CompraNacionalDetalle.php";

$compraNacional = new CompraNacional();
$compraNacionalDetalle = new CompraNacionalDetalle();
$compraNacional->setComId($_POST['compra']);
$compraNacionalDetalle->setComNaciId($_POST['compra']);
$respuesta = array();
$resCom = $compraNacional->getDato();
$resPR= $compraNacionalDetalle->getDatos();
if ($rowC = $resCom->fetch_assoc()){
    $productos=[];
    foreach ($resPR as $prod){
        $productos[] = $prod;
    }
    $rowC['productos'] = $productos;
    $respuesta = $rowC;
}
echo json_encode($respuesta);