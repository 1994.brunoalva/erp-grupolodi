<?php
require "../../conexion/Conexion.php";

$conexion = (new Conexion())->getConexion();

$termino = $_GET['term'];

$sql = "SELECT 
  prod.*,
  marca.mar_nombre 
FROM
  sys_producto AS prod 
  INNER JOIN sys_com_marca AS marca 
    ON prod.mar_id = marca.mar_id 
WHERE prod.cat_id = 6  AND prod.produ_nombre LIKE '%$termino%'";

$resul = $conexion->query($sql);
$arrRes=array();
foreach ($resul as $row){
    $row['value']=$row['produ_nombre'] ." | ".$row['mar_nombre'];
    $arrRes []= $row;
}

echo  json_encode($arrRes);

