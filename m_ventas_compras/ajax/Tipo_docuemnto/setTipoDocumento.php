<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/TipoDocumento.php';

$tipoDocumento = new TipoDocumento('INSERT');
$datos = json_decode($_POST['array']);
$resultSet = $tipoDocumento->insertAll($datos);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

