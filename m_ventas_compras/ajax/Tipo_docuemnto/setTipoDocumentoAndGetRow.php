<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/TipoDocumento.php';

$tipoDocumento = new TipoDocumento('INSERT');
$datos = json_decode($_POST['array']);
$id = $tipoDocumento->insertAllAndGetId($datos);
$tipoDocumento = new TipoDocumento('SELECT');
$resultSet = $tipoDocumento->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

