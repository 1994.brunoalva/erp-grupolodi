<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Nave.php';

$nave = new Nave('INSERT');
$datos = json_decode($_POST['array']);
$id = $nave->insertAllAndGetId($datos);

$nave = new Nave('SELECT');
$resultSet = $nave->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

