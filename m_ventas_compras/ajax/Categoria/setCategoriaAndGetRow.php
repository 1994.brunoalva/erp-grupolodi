<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Categoria.php';

$categoria = new Categoria('INSERT');
$datos = json_decode($_POST['array']);
$id = $categoria->insertAllAndGetId($datos);
$categoria = new Categoria('SELECT');
$resultSet = $categoria->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

