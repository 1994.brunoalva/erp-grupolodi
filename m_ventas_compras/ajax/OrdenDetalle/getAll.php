<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/OrdenDetalle.php';
require '../../model/Orden.php';
require '../../model/Producto.php';
require '../../model/Moneda.php';
require '../../model/Unidad.php';
require '../../model/Modelo.php';
require '../../model/ProductoDetalle.php';
require '../../model/Categoria.php';

$id = $_POST['id'];
$ordenDetalle = new OrdenDetalle('SELECT');
$resultDeta = $ordenDetalle->selectAllByColumn('ord_id', $id);
$row = Array();
if ($resultDeta) {
    foreach ($resultDeta as $item) {
        $orden = new Orden('SELECT');
        $resultOrd = $orden->selectById($item->ord_id);

        /*$moneda = new Moneda('SELECT');
        $resultMod = $moneda->selectById($resultOrd->mone_id);
        $resultOrd->ord_id=$resultMod;*/

        $item->ord_id = $resultOrd;


        $producto = new Producto('SELECT');
        $resultProd = $producto->selectById($item->produ_id);


        $unidad = new Unidad('SELECT');
        $resultUnidad = $unidad->selectById($resultProd->unidad_id);
        $resultProd->unidad_id = $resultUnidad;


        $modelo = new Modelo('SELECT');
        if (isset($resultProd->mod_id)){
            $resultMod = $modelo->selectById($resultProd->mod_id);
            $resultProd->mod_id = $resultMod;
        }



        $categoria = new Categoria('SELECT');
        $resultCate = $categoria->selectById($resultProd->cat_id);
        $resultProd->cat_id = $resultCate;


        $productoDetalle = new ProductoDetalle('SELECT');
        $resultDetPro = $productoDetalle->selectById($resultProd->produ_deta_id);
        $datos = Array();
        if ($resultCate->cat_nombre == 'NEUMATICOS') {

            $datos[] = $resultDetPro->produ_neu_ancho_interno;
            $datos[] = $resultDetPro->produ_neu_aro;
            $datos[] = $resultDetPro->produ_neu_pliegue;
        }
        if ($resultCate->cat_nombre == 'CAMARAS') {
            $datos[] = $resultDetPro->produ_cam_ancho;
            $datos[] = $resultDetPro->produ_cam_aro;
            $datos[] = $resultDetPro->produ_cam_valvula;
        }
        if ($resultCate->cat_nombre == 'AROS') {

            $datos[] = $resultDetPro->produ_aro_modelo;
            $datos[] = $resultDetPro->produ_aro_medida;
            $datos[] = $resultDetPro->produ_aro_espesor;


        }
        if ($resultCate->cat_nombre == 'ACCESORIOS') {
            $datos[] = $resultProd->produ_desc;
            $datos[] = $resultProd->produ_pn;
            $datos[] = $resultUnidad->unidad_nombre;
        }
        if ($resultCate->cat_nombre == 'PROTECTORES') {
            $datos[] = $resultProd->produ_desc;
            $datos[] = $resultProd->produ_pn;
            $datos[] = $resultUnidad->unidad_nombre;
        }
        $resultProd->detalle = $datos;
        /* $resultProd->detalle12 =$resultDetPro;*/


        $item->produ_id = $resultProd;


        $row[] = $item;
    }
}

echo json_encode($row, JSON_PRETTY_PRINT);
?>