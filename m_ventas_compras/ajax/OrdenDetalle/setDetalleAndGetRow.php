<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/OrdenDetalle.php';
require '../../model/Producto.php';
require '../../model/Unidad.php';

$ordenDetalle = new OrdenDetalle('INSERT');

$datos = json_decode($_POST['array']);
//echo json_encode($datos);
$id = $ordenDetalle->insertAllAndGetId($datos);

$ordenDetalle = new OrdenDetalle('SELECT');
$resultSet = $ordenDetalle->selectById($id);

$producto = new Producto('SELECT');
$resultProd = $producto->selectById($resultSet->produ_id);


$unidad = new Unidad('SELECT');
$resultUnidad = $unidad->selectById($resultProd->unidad_id);
$resultProd->unidad_id =$resultUnidad;


$resultSet->produ_id=$resultProd;

echo json_encode($resultSet, JSON_PRETTY_PRINT);





