<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/OrdenDetalle.php';
require '../../model/Orden.php';
require '../../model/Producto.php';
require '../../model/Moneda.php';
require '../../model/ProductoDetalle.php';

$id=$_GET['id'];
$ordenDetalle = new OrdenDetalle('SELECT');
$resultDeta = $ordenDetalle->selectAllByColumn('ord_id',$id);
$row = Array();
if($resultDeta){
    foreach ($resultDeta as $item) {
        $orden = new Orden('SELECT');
        $resultOrd = $orden->selectById($item->ord_id);

        /*$moneda = new Moneda('SELECT');
        $resultMod = $moneda->selectById($resultOrd->mone_id);
        $resultOrd->ord_id=$resultMod;*/

        $item->ord_id=$resultOrd;



        $producto = new Producto('SELECT');
        $resultProd = $producto->selectById($item->produ_id);
        $item->produ_id=$resultProd;


        /*$productoDetalle = new ProductoDetalle('SELECT');
        $resultPdeta = $productoDetalle->selectById($resultProd->produ_deta_id);
        $resultProd->produ_deta_id=$resultPdeta;*/



        $item->produ_id=$resultProd;


        $row[]=$item;
    }
}

echo json_encode($row, JSON_PRETTY_PRINT);
?>