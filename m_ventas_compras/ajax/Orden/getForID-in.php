<?php

function getAllOrdenForId($id){
    $orden = new Orden('SELECT');
    $resultOrd = $orden->selectByColumn('impor_id',$id);
    if ($resultOrd){
        $folder = new Folder('SELECT');
        $resultFol = $folder->selectById($resultOrd->impor_id);
        $resultOrd->impor_id=$resultFol;

        if(isset($resultOrd->mone_id)){
            $moneda = new Moneda('SELECT');
            //echo $resultOrd->mone_id . "<<<<<<<<";
            $resultMo = $moneda->selectById($resultOrd->mone_id);

            $resultOrd->mone_id=$resultMo;
        }

    }

    return $resultOrd;
}
?>