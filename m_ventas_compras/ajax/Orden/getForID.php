<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Orden.php';
require '../../model/Folder.php';
require '../../model/Moneda.php';

$id=$_GET['id'];
$orden = new Orden('SELECT');
$resultOrd = $orden->selectByColumn('impor_id',$id);

$folder = new Folder('SELECT');
$resultFol = $folder->selectById($resultOrd->impor_id);
$resultOrd->impor_id=$resultFol;
if(isset($resultOrd->mone_id)){
    $moneda = new Moneda('SELECT');
    $resultMo = $moneda->selectById($resultOrd->mone_id);

    $resultOrd->mone_id=$resultMo;
}


echo json_encode($resultOrd, JSON_PRETTY_PRINT);
?>