<?php

require '../../conexion/Conexion.php';
require '../../model/Cotizacion.php';

$cotizacion = new Cotizacion();

$cotizacion->setCotiId($_POST['idcotizacion']);
$cotizacion->setCotiAte($_POST['atencion']);
$cotizacion->setCotiEstatus(1);
$cotizacion->setCotiObs($_POST['observaciones']);
$cotizacion->setCotiLlega('');
$cotizacion->setCotiPago('');
$cotizacion->setCotiDiasCredito($_POST['cantidaddias']);
$cotizacion->setCotiRazon($_POST['nombre']);
$cotizacion->setCotiRuc($_POST['ruc']);
$cotizacion->setCotiTelf($_POST['telefono']);
$cotizacion->setCotiTpCambio($_POST['tasacambio']);
$cotizacion->setCotiTpCredito($_POST['tipopago']);
$cotizacion->setCotiTpDocu(3);
$cotizacion->setCotiTpMoneda($_POST['moneda']);
$cotizacion->setCotiTotal($_POST['totalcoti']);
$cotizacion->setIdCliente($_POST['id']);
$cotizacion->setIdAgencia('null');
$cotizacion->setCotiFecha($_POST['fecha']);
$cotizacion->setEstado('1');

$arrRe = array("res" => false);

if ($cotizacion->actualizar()) {
    $arrRe['res'] = true;
    $arrRe['idCoti'] = $cotizacion->getCotiId();

}
echo json_encode($arrRe);
