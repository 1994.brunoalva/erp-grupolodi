<?php
require '../../conexion/Conexion.php';
require '../../model/Cotizacion.php';
require '../../model/DetalleCotizacion.php';
$cotizacion = new Cotizacion();
$detalleCotizacion=new DetalleCotizacion();


$idCOtizacion = $_POST['idCoti'];


$cotizacion->setCotiId($idCOtizacion);

$respuesta = $cotizacion->verCotizacion()->fetch_assoc();
$respuesta['productos']=[];

$detalleCotizacion->setIdCotizacion($idCOtizacion);
$res = $detalleCotizacion->verLista();

foreach ($res as $row){
    $respuesta['productos'][]= $row;
}

echo json_encode($respuesta);