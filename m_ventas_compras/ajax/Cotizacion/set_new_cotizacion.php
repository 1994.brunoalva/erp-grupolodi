<?php
require '../../conexion/Conexion.php';
require '../../model/Cotizacion.php';

$cotizacion= new Cotizacion();

$cotizacion->setCotiAte($_POST['atencion']);
$cotizacion->setCotiEstatus(1);
$cotizacion->setCotiObs($_POST['observaciones']);
$cotizacion->setCotiLlega('');
$cotizacion->setCotiPago('');
$cotizacion->setCotiDiasCredito($_POST['cantidaddias']);
$cotizacion->setCotiRazon($_POST['nombre']);
$cotizacion->setCotiRuc($_POST['ruc']);
$cotizacion->setCotiTelf($_POST['telefono']);
$cotizacion->setCotiTpCambio($_POST['tasacambio']);
$cotizacion->setCotiTpCredito($_POST['tipopago']);
$cotizacion->setCotiTpDocu(3);
$cotizacion->setCotiTpMoneda($_POST['moneda']);
$cotizacion->setIdCliente($_POST['id']);
$cotizacion->setIdAgencia('null');
$cotizacion->setCotiFecha($_POST['fecha']);
$cotizacion->setCotiTotal($_POST['totalcoti']);
$cotizacion->setEstado('1');
$cotizacion->setIdDireccion($_POST['idDireccion']);

$arrRe= array("res" => false);

if ($cotizacion->insertar()){
    $arrRe['res'] = true;
    $arrRe['idCoti'] = $cotizacion->getCotiId();

}
echo json_encode($arrRe);
