<?php

function getAllPolizaForId($id){
    $poliza = new Poliza('SELECT');
    $resultPol= $poliza->selectByColumn('impor_id',$id);

    if(isset($resultPol->tasa_poliza_id)){
        $tasaPoliza = new TasaPoliza('SELECT');
        $resultTaz = $tasaPoliza->selectById($resultPol->tasa_poliza_id);
        $resultPol->tasa_poliza_id=$resultTaz;
    }
    return $resultPol;
}
?>