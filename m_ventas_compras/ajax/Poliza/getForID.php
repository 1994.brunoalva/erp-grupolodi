<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Orden.php';
require '../../model/Poliza.php';
require '../../model/TasaPoliza.php';

$id=$_GET['id'];
$poliza = new Poliza('SELECT');
$resultPol= $poliza->selectByColumn('impor_id',$id);

if(isset($resultPol->tasa_poliza_id)){
    $tasaPoliza = new TasaPoliza('SELECT');
    $resultTaz = $tasaPoliza->selectById($resultPol->tasa_poliza_id);
    $resultPol->tasa_poliza_id=$resultTaz;
}



echo json_encode($resultPol, JSON_PRETTY_PRINT);
?>