<?php
/*header('Content-Type: application/json');

require ("../src/autoload.php");
$cookie = array(
    'cookie' 		=> array(
        'use' 		=> true,
        'file' 		=> __DIR__ . "/cookie.txt"
    )
);
$config = array(
    'representantes_legales' 	=> true,
    'cantidad_trabajadores' 	=> true,
    'establecimientos' 			=> true,
    'cookie' 					=> $cookie
);
$company = new \Sunat\tipo_cambio( $config );

$mes =12;
$anio ='2020';

$result = $company->consulta( $mes , $anio);

echo json_encode($result,JSON_PRETTY_PRINT);*/

//$html = file_get_contents('https://e-consulta.sunat.gob.pe/cl-at-ittipcam/tcS01Alias'); //Convierte la información de la URL en cadena
function file_get_contents_curl($url){
   /* $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, TRUE);
    curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $head = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
*/

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url );
    curl_setopt($ch, CURLOPT_POST, 1);// set post data to true
    curl_setopt($ch, CURLOPT_POSTFIELDS,"");   // post data
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $json = curl_exec($ch);
    curl_close($ch);
    return $json;
}

$sitioweb = file_get_contents_curl("https://e-consulta.sunat.gob.pe/cl-at-ittipcam/tcS01Alias");  // Ejecuta la función curl escrapeando el sitio web https://devcode.la and regresa el valor a la variable $sitioweb
echo $sitioweb;
