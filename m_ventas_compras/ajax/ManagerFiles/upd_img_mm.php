<?php
require "../../utils/Tools.php";

$tools = new Tools();

/* Getting file name */
$filename = $_FILES['file']['name'];
$carpeta =$_POST['folder'];

$path_parts = pathinfo($filename, PATHINFO_EXTENSION);
$newName =$tools->getToken(35);
/* Location */
$location = "../../imagenes/$carpeta/" . $newName .'.'. $path_parts;


$arr = array( 'res' => false);
if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
    $arr['res']=true;
    $arr['datos']= $newName;

}
echo json_encode($arr);