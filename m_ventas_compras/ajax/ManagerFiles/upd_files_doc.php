<?php
require "../../utils/Tools.php";

$tools = new Tools();

/* Getting file name */
$filename = $_FILES['file']['name'];

$path_parts = pathinfo($filename, PATHINFO_EXTENSION);
$newName =$tools->getToken(35);
/* Location */
$location = "../../imagenes/doc_compra/" . $newName .'.'. $path_parts;


$arr = array( 'res' => false);
if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
    $arr['res']=true;
    $arr['dstos']= $newName.'.' . $path_parts;

}
echo json_encode($arr);