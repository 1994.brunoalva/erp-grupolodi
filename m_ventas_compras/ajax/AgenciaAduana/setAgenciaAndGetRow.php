<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/AgenciaAduana.php';

$agenciaAduana = new AgenciaAduana('INSERT');
$datos = json_decode($_POST['array']);
$id = $agenciaAduana->insertAllAndGetId($datos);

$agenciaAduana = new AgenciaAduana('SELECT');
$resultSet = $agenciaAduana->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

