<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Proveedores.php';
require '../../model/TipoDocumento.php';
require '../../model/Pais.php';

$id=$_POST['id'];
$proveedores = new Proveedores('SELECT');
$resultPro = $proveedores->selectById($id);
$idD = strval($resultPro->tipodoc_id);
$idP = strval($resultPro->pais_id);
$tipoDocumento = new TipoDocumento('SELECT');
$resultDoc = $tipoDocumento->selectById("'" . $idD . "'");
$resultPro->tipodoc_id = $resultDoc;
$pais = new Pais('SELECT');
$resultPais = $pais->selectById("'" . $idP . "'");
$resultPro->pais_id = $resultPais;
echo  json_encode($resultPro,JSON_PRETTY_PRINT);
?>