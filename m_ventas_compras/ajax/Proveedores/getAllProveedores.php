<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Proveedores.php';
require '../../model/TipoDocumento.php';
require '../../model/Pais.php';
$proveedores = new Proveedores('SELECT');
$resultPro = $proveedores->selectAll();
$row = Array();
if($resultPro){
    foreach ($resultPro as $item) {
        $idD =  strval($item->tipodoc_id);
        $idP =  strval($item->pais_id);
        $tipoDocumento = new TipoDocumento('SELECT');
        $resultDoc = $tipoDocumento->selectById("'".$idD."'");
        $item->tipodoc_id=$resultDoc;

        $pais = new Pais('SELECT');
        $resultPais = $pais->selectById("'".$idP."'");
        $item->pais_id=$resultPais;

        $row[]=$item;
    }
}
echo  json_encode($row,JSON_PRETTY_PRINT);
?>