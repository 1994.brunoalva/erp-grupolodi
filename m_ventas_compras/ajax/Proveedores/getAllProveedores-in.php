<?php

function getAllProveedores(){
    $proveedores = new Proveedores('SELECT');
    $resultPro = $proveedores->selectAll();
    $row = Array();
    if($resultPro){
        foreach ($resultPro as $item) {
            
            $idD =  strval($item->tipodoc_id);
            $idP =  strval($item->pais_id);
            $tipoDocumento = new TipoDocumento('SELECT');
            $resultDoc = $tipoDocumento->selectById("'".$idD."'");
            $item->tipodoc_id=$resultDoc;

            $pais = new Pais('SELECT');
            $resultPais = $pais->selectById("'".$idP."'");
            $item->pais_id=$resultPais;

            $row[]=$item;
        }
    }
    return $row;
}

?>