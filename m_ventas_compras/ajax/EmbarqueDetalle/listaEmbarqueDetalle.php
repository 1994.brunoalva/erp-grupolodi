<?php

require "../../conexion/Conexion.php";
require "../../model/EmbarqueDetalle.php";

$conexion = (new Conexion())->getConexion();
$embarqueDetalle = new EmbarqueDetalle();

$embarqueDetalle->setEmbId($_POST['idembarque']);
$arritems=[];
foreach ( $embarqueDetalle->listar() as $row){
    $sql="SELECT *
        FROM sys_com_linea
        WHERE linea_id= '".$row['linea_id']."'";
    $nombreLinea = "";
    if($lin=$conexion->query($sql)->fetch_assoc()){
        $nombreLinea = $lin["linea_nombre"];
    }

    $sql="SELECT
        *
        FROM sys_com_almacen
        WHERE alm_id = '".$row['almacen_id']."'";
    $nombreAlma= "";
    if($alm=$conexion->query($sql)->fetch_assoc()){
        $nombreAlma = $alm["alm_razon_social"];
    }

    $arritems[]=  array(
        'iddetaemb'=>$row['emb_det_id'],
        'idemp'=>$row['emb_id'],
        'idLinea'=>$row['linea_id'],
        'nombrelinea'=>$nombreLinea,
        'idalmacen'=>$row['almacen_id'],
        'nomalmacen'=>$nombreAlma,
        "naves"=>array(),
        'bl'=>$row['emb_bl_serie'],
        'idnave'=>$row['nave_id'],
        'etd'=>$row['emb_etd'],
        'eta' =>$row['emb_eta'],
        'libres'=>$row['emb_libre_sobre'],
        'iddepacho'=>$row['desp_id'],
        'diasalma'=>$row['emb_libre_almacen']);
}

echo json_encode($arritems);