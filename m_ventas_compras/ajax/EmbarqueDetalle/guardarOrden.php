<?php

require "../../conexion/Conexion.php";
require "../../model/EmbarqueDetalle.php";

$conexion = (new Conexion())->getConexion();
$embarqueDetalle = new EmbarqueDetalle();

$listaEmbarque  = json_decode($_POST['embarques']);

$idorden=$_POST['idorden'];
$idembarque=$_POST['idembarque'];
$proforma=$_POST['proforma'];
$fecha=$_POST['fecha'];
$idfolwa=$_POST['idfolwa']==""?'null':$_POST['idfolwa'];
$fobtotal=$_POST['fobtotal'];
$fletecont=$_POST['fletecont'];
$thc=$_POST['thc'];
$cost_adicional=$_POST['cost_ad'];
$totalflete=$_POST['totalflete'];

$sql ="INSERT INTO sys_com_embarque
VALUES (null,
        '$idorden',
        '$proforma',
        '$fecha',
        '$fobtotal',
        '$fletecont',
        '$thc',
        '$cost_adicional',
        '$totalflete',
        '',
        $idfolwa,
        null,
        null,
        null,
        null,
        null,
       null,
        null,
        b'1');";

if ($idembarque==""){
    $conexion->query($sql);
    $idembarque=$conexion->insert_id;
}else{
    $sql ="UPDATE sys_com_embarque
SET
  emb_factura = '$proforma',
  emb_fech_factura = '$fecha',
  emb_monto = '$fobtotal',
  emb_flete = '$fletecont',
  emb_thc = '$thc',
  emb_cost_origen = '$cost_adicional',
  emb_total_flete = '$totalflete',
  forwa_id = $idfolwa
WHERE emb_id = '$idembarque';";
    $conexion->query($sql);
}

//print_r($listaEmbarque);
$tempArra=[];
foreach ($listaEmbarque as $item){
    $embarqueDetalle->setAlmacenId($item->idalmacen==""?'null':$item->idalmacen);
    $embarqueDetalle->setDespId($item->iddepacho==""?'null':$item->iddepacho);
    $embarqueDetalle->setEmbBlSerie($item->bl);
    $embarqueDetalle->setEmbEta($item->eta);
    $embarqueDetalle->setEmbId($idembarque);
    $embarqueDetalle->setEmbEtd($item->etd);
    $embarqueDetalle->setEmbDetId($item->iddetaemb==""?'null':$item->iddetaemb);
    $embarqueDetalle->setEmbLibreAlmacen($item->diasalma);
    $embarqueDetalle->setEmbLibreSobre($item->libres);
    $embarqueDetalle->setLineaId($item->idLinea==""?'null':$item->idLinea);
    $embarqueDetalle->setNaveId($item->idnave==""?'null':$item->idnave);

    if ($item->iddetaemb==''){
        $embarqueDetalle->insertar();
    }else{
        $embarqueDetalle->actualizar();
    }
    $itemarr = $embarqueDetalle->toArrayData();
    $itemarr['nombrelinea']=$item->nombrelinea;
    $itemarr['nomalmacen']=$item->nomalmacen;

    $tempArra[]=$itemarr;
}
echo json_encode(array("res"=>true,"ideb"=>$idembarque,"eb"=>$tempArra));