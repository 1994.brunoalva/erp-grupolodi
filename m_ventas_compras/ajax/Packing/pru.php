<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Packing.php';
require '../../model/OrdenDetalle.php';
require '../../model/Producto.php';
require '../../model/Unidad.php';
require '../../model/Modelo.php';



$id = $_GET['id'];
$packing = new Packing('SELECT');
$result = $packing->selectAllByColumn('con_id',$id);

$row = Array();
if ($result) {
    foreach ($result as $item) {

        $ordenDetalle = new OrdenDetalle('SELECT');
        $resultDet = $ordenDetalle->selectById($item->orddeta_id);
        $item->orddeta_id = $resultDet;


        $producto = new Producto('SELECT');
        $resultPro = $producto->selectById($item->orddeta_id->produ_id);


        $unidad = new Unidad('SELECT');
        $resultUni = $unidad->selectById($resultPro->unidad_id);

        $resultPro->unidad_id=$resultUni;


        $modelo = new Modelo('SELECT');
        $resultMod = $modelo->selectById($resultPro->mod_id);
        $resultPro->mod_id = $resultMod;
        $resultPro->unidad_id=$resultUni;


        $resultDet->produ_id=$resultPro;

        $item->orddeta_id = $resultDet;

        $row[] = $item;
    }
}
echo  json_encode($row,JSON_PRETTY_PRINT);





?>