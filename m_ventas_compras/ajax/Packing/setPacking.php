<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Packing.php';
require '../../model/OrdenDetalle.php';
require '../../model/Producto.php';
require '../../model/Unidad.php';
require '../../model/Modelo.php';



$packing = new Packing('INSERT');
$datos = json_decode($_POST['array']);
$id = $packing->insertAllAndGetId($datos);


$packing = new Packing('SELECT');
$resultSet = $packing->selectById($id);

if ($resultSet) {

        $ordenDetalle = new OrdenDetalle('SELECT');
        $resultDet = $ordenDetalle->selectById($resultSet->orddeta_id);
        $resultSet->orddeta_id = $resultDet;


        $producto = new Producto('SELECT');
        $resultPro = $producto->selectById($resultSet->orddeta_id->produ_id);


        $unidad = new Unidad('SELECT');
        $resultUni = $unidad->selectById($resultPro->unidad_id);

        $resultPro->unidad_id=$resultUni;


        if ($resultPro->mod_id){
            $modelo = new Modelo('SELECT');
            $resultMod = $modelo->selectById($resultPro->mod_id);
            $resultPro->mod_id = $resultMod;
            $resultPro->unidad_id=$resultUni;
        }



        $resultDet->produ_id=$resultPro;

        $resultSet->orddeta_id = $resultDet;
}

echo  json_encode($resultSet,JSON_PRETTY_PRINT);

