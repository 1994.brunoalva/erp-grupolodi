<?php

require '../../conexion/Conexion.php';
require '../../model/DetalleCotizacion.php';

$detalleCotizacion=new DetalleCotizacion();

$idCoti= $_POST['idCoti'];

$listaProductos = json_decode($_POST['productos']);
$arrRe= array("res" => true);
foreach ($listaProductos as $pro){

    $detalleCotizacion->setEstado(1);
    $detalleCotizacion->setIdProducto($pro->id);
    $detalleCotizacion->setCantidad($pro->cantidad);
    $detalleCotizacion->setPrecioUnitario($pro->precio);
    $detalleCotizacion->setIdEmpresa($pro->idempresa);
    $detalleCotizacion->setIdCotizacion($idCoti);
    $detalleCotizacion->setDescuento($pro->descuento);
    $detalleCotizacion->setProductoDesc($pro->producto);
    if (!$detalleCotizacion->insertar()){
        $arrRe['res'] = false;
    }
}
echo json_encode($arrRe);