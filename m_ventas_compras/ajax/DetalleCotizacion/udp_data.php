<?php


require '../../conexion/Conexion.php';
require '../../model/DetalleCotizacion.php';

$detalleCotizacion = new DetalleCotizacion();

$idCoti = $_POST['idCoti'];
$detalleCotizacion->setIdCotizacion($idCoti);
$listaProductos = json_decode($_POST['productos']);
$arrRe = array("res" => true);
if ($detalleCotizacion->elimarPorCotizacion()){
    foreach ($listaProductos as $pro) {
        $detalleCotizacion->setEstado(1);
        $detalleCotizacion->setIdProducto($pro->id);
        $detalleCotizacion->setCantidad($pro->cantidad);
        $detalleCotizacion->setPrecioUnitario($pro->precio);
        $detalleCotizacion->setIdEmpresa($pro->idempresa);
        $detalleCotizacion->setProductoDesc($pro->producto);
        $detalleCotizacion->setDescuento($pro->descuento);
        if (!$detalleCotizacion->insertar()) {
            $arrRe['res'] = false;
        }
    }
}else{
    $arrRe['res'] = false;
}

echo json_encode($arrRe);
