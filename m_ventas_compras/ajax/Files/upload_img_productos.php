 <?php


 /* Getting file name */
 $filename = $_FILES['file']['name'];

 $path_parts = pathinfo($filename, PATHINFO_EXTENSION);
 $newName = getToken(30);
 /* Location */
 $location = "../public/img/pictures/" . $newName . '.' . $path_parts;
 $uploadOk = 1;
 $imageFileType = pathinfo($location, PATHINFO_EXTENSION);

 /* Valid Extensions */
 $valid_extensions = array("jpg", "jpeg", "png");
 /* Check file extension */
 if (!in_array(strtolower($imageFileType), $valid_extensions)) {
     $uploadOk = 0;
 }

 if ($uploadOk == 0) {
     echo 0;
 } else {
     /* Upload file */
     $arr = array('res' => false);
     if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
         $arr['res'] = true;
         $arr['dstos'] = array('name' => $newName, 'extencion' => $path_parts);
         echo json_encode($arr);
     } else {
         echo json_encode($arr);
     }
 }

 function getToken($length)
 {
     $token = "";
     $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
     $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
     $codeAlphabet .= "0123456789";
     $max = strlen($codeAlphabet);

     for ($i = 0; $i < $length; $i++) {
         $token .= $codeAlphabet[random_int(0, $max - 1)];
     }

     return $token;
 }