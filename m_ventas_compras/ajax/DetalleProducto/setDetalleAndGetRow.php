<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/ProductoDetalle.php';
$datos = json_decode($_POST['array']);

if ($datos[0]==0){
    $datos[0]='';
    $productoDetalle = new ProductoDetalle('INSERT');

    $id = $productoDetalle->insertAllAndGetId($datos);
    echo  json_encode($id,JSON_PRETTY_PRINT);
}else{
    $productoDetalle = new ProductoDetalle('UPDATE');
    //$productoDetalle->updateById($datos);
    $sql= "UPDATE sys_produ_detalle
SET 
  produ_cam_ancho = '{$datos[1]}',
  produ_cam_aro = '{$datos[2]}',
  produ_cam_valvula = '{$datos[3]}',
  produ_aro_modelo = '{$datos[4]}',
  produ_aro_medida = '{$datos[5]}',
  produ_aro_espesor = '{$datos[6]}',
  produ_aro_huecos = '{$datos[7]}',
  produ_aro_espe_hueco = '{$datos[8]}',
  produ_aro_cbd = '{$datos[9]}',
  produ_aro_pcd = '{$datos[10]}',
  produ_aro_offset = '{$datos[11]}',
  produ_neu_ancho_interno = '{$datos[12]}',
  produ_neu_serie = '{$datos[13]}',
  produ_neu_aro = '{$datos[14]}',
  produ_neu_pliegue = '{$datos[15]}',
  produ_neu_uso_comercial = '{$datos[16]}',
  produ_neu_set = '{$datos[17]}',
  produ_neu_mate = '{$datos[18]}',
  produ_neu_ancho_adua = '{$datos[19]}',
  produ_neu_serie_adua = '{$datos[20]}',
  produ_neu_tp_const = '{$datos[21]}',
  produ_neu_carga = '{$datos[22]}',
  produ_neu_pisa = '{$datos[23]}',
  produ_neu_exte = '{$datos[24]}',
  produ_neu_vel = '{$datos[25]}',
  produ_neu_consta = '{$datos[26]}',
  produ_neu_item = '{$datos[27]}',
  produ_neu_vigencia = '{$datos[28]}',
  produ_neu_confor = '{$datos[29]}',
  produ_neu_indice = '{$datos[30]}',
  produ_neu_suce = '{$datos[31]}',
  produ_partida = '{$datos[32]}',
  produ_medida = '{$datos[33]}',
  estado = '{$datos[34]}'
WHERE produ_deta_id = '{$datos[0]}';";
    $productoDetalle->existData($sql);

    //echo $sql;

    echo  json_encode($datos[0],JSON_PRETTY_PRINT);
}



