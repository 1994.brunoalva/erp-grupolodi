<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Linea.php';

$linea = new Linea('INSERT');
$datos = json_decode($_POST['array']);
$id = $linea->insertAllAndGetId($datos);

$linea = new Linea('SELECT');
$resultSet = $linea->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

