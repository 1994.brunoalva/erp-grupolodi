<?php


require '../../conexion/Conexion.php';
require '../../model/Clientes.php';
require '../../model/DireccionesSucursales.php';
require '../../model/RepresentanteLegal.php';

$clientes=new Clientes();
$direcciones = json_decode( $_POST['direciones']);
$represen = json_decode( $_POST['representantes']);
$driSur = new DireccionesSucursales();
$representanteLegal=new RepresentanteLegal();


$respuesta =array('res'=>false);
$clientes->setContacto($_POST['contacto']);
$clientes->setCondicionSunat($_POST['condicion']);
$clientes->setEmail1($_POST['email1']);
$clientes->setEmail2($_POST['email2']);
$clientes->setEstadoSunat($_POST['estado']);
$clientes->setIdClasificacion($_POST['clasificacion']);
$clientes->setIdTipoDocumento(1);
$clientes->setTelefono2($_POST['email3']);
$clientes->setRazonSocial($_POST['nombresocial']);
$clientes->setRuc($_POST['ruc']);
$clientes->setTelefono($_POST['telefono']);
$clientes->setVendedorResponsable($_POST['vendedor']);
if ($clientes->insertar()){
    $contador=1;
foreach ($direcciones as $dir){
    $driSur->setEstado($contador==1?'p':'s');
    $driSur->setIdCliente($clientes->getId());
    $driSur->setIdAgencia($dir->idagencia==-1?'null':$dir->idagencia);
    $driSur->setDistrito($dir->distrito);
    $driSur->setUbigeo($dir->ubigeo);
    $driSur->setDireccion($dir->direccion);
    $driSur->setProvincia($dir->provincia);
    $driSur->setDepartamento($dir->departamento);
    $driSur->setTipo($dir->tipo);
    if ($driSur->insertar()){}
    $contador++;
}
foreach ($represen as $repre){
    $representanteLegal->setRepCargo($repre->cargo);
    $representanteLegal->setRepDesde($repre->desde);
    $representanteLegal->setRepIdClie($clientes->getId());
    $representanteLegal->setRepNdoc($repre->numdoc);
    $representanteLegal->setRepTdoc($repre->tipodoc);
    $representanteLegal->setRepNomape($repre->nombre);
    if ($representanteLegal->insertar()){}
}
    $respuesta['res']=true;
    $respuesta['data']=$clientes->getArrayData();
}

echo json_encode($respuesta);
