<?php


require '../../conexion/Conexion.php';
require '../../model/Clientes.php';
require '../../model/DireccionesSucursales.php';

$clientes=new Clientes();
$driSur = new DireccionesSucursales();

$direcciones = json_decode( $_POST['direciones']);

$respuesta =array('res'=>false);
$clientes->setId($_POST['id']);
$clientes->setContacto($_POST['contacto']);
$clientes->setCondicionSunat($_POST['condicion']);
$clientes->setEmail1($_POST['email1']);
$clientes->setEmail2($_POST['email2']);
$clientes->setEstadoSunat($_POST['estado']);
$clientes->setIdClasificacion($_POST['clasificacion']);
$clientes->setIdTipoDocumento(1);
$clientes->setTelefono2($_POST['email3']);
$clientes->setRazonSocial($_POST['nombresocial']);
$clientes->setRuc($_POST['ruc']);
$clientes->setTelefono($_POST['telefono']);
$clientes->setVendedorResponsable($_POST['vendedor']);

if ($clientes->update()){
    $contador=1;
    foreach ($direcciones as $dir){
        $driSur->setId($dir->id);
        $driSur->setEstado($contador==1?'p':'s');
        $driSur->setIdCliente($clientes->getId());
        $driSur->setIdAgencia($dir->idagencia==-1?'null':$dir->idagencia);
        $driSur->setDistrito($dir->distrito);
        $driSur->setUbigeo($dir->ubigeo);
        $driSur->setDireccion($dir->direccion);
        $driSur->setProvincia($dir->provincia);
        $driSur->setDepartamento($dir->departamento);
        $driSur->setTipo($dir->tipo);
        if (strlen($dir->direccion)>3){
            if ($dir->id==-1){
                if ($driSur->insertar()){}
            }else{
                if ($driSur->actualizar()){}
            }
        }


        $contador++;
    }
    $respuesta['res']=true;
    $respuesta['data']=$clientes->getArrayData();
}

echo json_encode($respuesta);
