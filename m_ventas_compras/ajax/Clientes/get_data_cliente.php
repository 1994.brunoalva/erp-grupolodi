<?php
require '../../conexion/Conexion.php';
require '../../model/Clientes.php';
require '../../model/DireccionesSucursales.php';

$clientes=new Clientes();
$dirSuc=new DireccionesSucursales();

$clientes->setId($_POST['id']);
$dirSuc->setIdCliente($_POST['id']);
$resul = $clientes->verCliente()->fetch_assoc();
$resul['direcciones'] = $dirSuc->getArrayDirec();

/*
$arrOjt= [];
foreach ($resul as $row){
    $arrOjt []= $row;
}*/
echo json_encode($resul);
