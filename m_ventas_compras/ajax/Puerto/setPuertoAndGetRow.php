<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Puerto.php';

$puerto = new Puerto('INSERT');
$datos = json_decode($_POST['array']);
$id = $puerto->insertAllAndGetId($datos);
$puerto = new Puerto('SELECT');
$resultSet = $puerto->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

