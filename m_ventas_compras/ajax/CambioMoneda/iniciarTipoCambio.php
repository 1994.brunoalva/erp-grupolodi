<?php

require '../../conexion/Conexion.php';
require ("../libSunat/sunat/src/autoload.php");
$conecion = (new Conexion())->getConexion();
$fecha=$_POST['fecha'];
//$fecha='2020-08-03';
//echo  $fecha;
$sql="SELECT *
FROM sys_tipo_cambio
WHERE fecha_consulta = '$fecha'";
$resp =$conecion->query($sql);

$respuesta = array();
if ($row = $resp->fetch_assoc()){
    $respuesta['idc']=$row['ticam_id'];
    $respuesta['venta']=$row['ticam_venta'];
    $respuesta['compra']=$row['ticam_compra'];
    $respuesta['fecha']=$row['fecha_consulta'];
}else{
    $cookie = array(
        'cookie' 		=> array(
            'use' 		=> true,
            'file' 		=> __DIR__ . "/cookie.txt"
        )
    );
    $config = array(
        'representantes_legales' 	=> true,
        'cantidad_trabajadores' 	=> true,
        'establecimientos' 			=> true,
        'cookie' 					=> $cookie
    );
    $company = new \Sunat\tipo_cambio( $config );
    $div = explode("-", $fecha);
    $mes =$div[1];
    $anio =$div[0];
    //print_r($div);
    $result = $company->consulta( ($mes<10)? "0".$mes: $mes, $anio);
    //print_r($result);
    $camb = $result->result[count($result->result)-1];

   //echo $camb->fecha;

    $sql="INSERT INTO sys_tipo_cambio
VALUES (null,
        1,
        '".$camb->compra."',
        '".$camb->venta."',
        '".$camb->fecha."',
        '$fecha',
        '1');";
    $conecion->query($sql);

    $respuesta['idc']=  $conecion->insert_id;
    $respuesta['venta']=$camb->venta;
    $respuesta['compra']=$camb->compra;
    $respuesta['fecha']=$fecha;

}

echo json_encode($respuesta);
