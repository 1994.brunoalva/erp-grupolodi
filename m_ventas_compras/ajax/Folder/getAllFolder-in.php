<?php

function getAllFolder(){
    $folder = new Folder('SELECT');
    $resultFol = $folder->selectAll();
    $row = Array();
    if ($resultFol) {
        foreach ($resultFol as $item) {
         //   print_r($item);
            $idE = strval($item->emp_id);
            $idC = strval($item->cat_id);
            $idI = strval($item->inco_id);
            $idT = strval($item->ticon_id);
            $idM = strval($item->mar_id);
            $idP = strval($item->pais_id);
            $idPu = strval($item->puerto_id);
            $idPr = strval($item->provee_id);
            $empresa = new Empresa('SELECT');
            $resultEmp = $empresa->selectById("'" . $idE . "'");
            $item->emp_id = $resultEmp;
            $categoria = new Categoria('SELECT');
            $resultCat = $categoria->selectById("'" . $idC . "'");
            $item->cat_id = $resultCat;
            $incoterm = new Incoterm('SELECT');
            $resultInco = $incoterm->selectById("'" . $idI . "'");
            $item->inco_id = $resultInco;
            $tipoContenedor = new TipoContenedor('SELECT');
            $resultTico = $tipoContenedor->selectById("'" . $idT . "'");
            $item->ticon_id = $resultTico;
            $marca = new Marca('SELECT');
            $resultMar = $marca->selectById("'" . $idM . "'");
            $resultCat = $categoria->selectById($resultMar->cat_id);
            $resultMar->cat_id=$resultCat;
            $item->mar_id = $resultMar;
            $pais = new Pais('SELECT');
            $resultPais = $pais->selectById("'" . $idP . "'");
            $item->pais_id = $resultPais;
            $puerto = new Puerto('SELECT');
            $resultPuerto = $puerto->selectById("'" . $idPu . "'");
            $item->puerto_id = $resultPuerto;
            $proveedores = new Proveedores('SELECT');
            $resultPro = $proveedores->selectById("'" . $idPr . "'");

            $tipoDocumento = new TipoDocumento('SELECT');
            $resultDoc = $tipoDocumento->selectById("'" . $resultPro->tipodoc_id . "'");
            $resultPro->tipodoc_id = $resultDoc;

            $resultPais = $pais->selectById("'" . $resultPro->pais_id . "'");
            $resultPro->pais_id = $resultPais;
            $item->provee_id = $resultPro;
            $row[] = $item;

        }
    }
    return $row;
}