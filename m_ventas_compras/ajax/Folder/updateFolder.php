<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Folder.php';
require '../../model/Contenedor.php';
require '../../model/Packing.php';


$folder = new Folder('UPDATE');
$datos = json_decode($_POST['array']);
$resultSet = $folder->updateById($datos);

//print_r($datos);

$id_fol=$datos[0][1];
$can =$datos[7][1];

$contenedor = new Contenedor('SELECT');
$resultCon = $contenedor->selectAllByColumn('impor_id', $id_fol);
$row = Array();

$num = 0;
if ($resultCon) {
    foreach ($resultCon as $item) {
        $row[] = $item->con_id;
        ++$num;
    }
}
$data=Array();
$data[]=['codigo',''];
$contenedor = new Contenedor('UPDATE');
$resultPac = $contenedor->updateByColumnAndID($data,'impor_id',$id_fol);

if ($can < $num) {
    $can = $num - $can;
    $row = array_reverse($row);
    $ind = 0;
    while ($ind < $can) {
        $contenedor = new Contenedor('DELETE');
        $resultCon = $contenedor->deleteById($row[$ind]);
        $packing = new Packing('DELETE');
        $resultPac = $packing->deleteByColumn('con_id', $row[$ind]);
        unset($row[$ind]);
        ++$ind;
    }
    $row = array_reverse($row);


    foreach ($row as $item) {
        $packing = new Packing('DELETE');
        $resultPac = $packing->deleteByColumn('con_id', $item);
    }
} else {
    $can = $can - $num;
    $ind = 1;
    while ($ind <= $can) {
        $conte = Array();
        $conte[] = '';
        $conte[] = $id_fol;
        $conte[] = '';
        $conte[] = '';
        $conte[] = '';
        $conte[] = 'b1';
        $contenedor = new Contenedor('INSERT');
        $res = $contenedor->insertAll($conte);
        ++$ind;;
    }
}

/*if($resultSet){
    $id_fol=$datos[0][1];
    $qty=$datos[7][1];
    $contenedor = new Contenedor('DELETE');
    $contenedor->deleteByColumn('impor_id',$id_fol);
    while ($qty){
        $conte = Array();
        $conte[]='';
        $conte[]=$id_fol;
        $conte[]='';
        $conte[]='b1';
        $contenedor = new Contenedor('INSERT');
        $res = $contenedor->insertAll($conte);
        --$qty;
    }
}*/
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

