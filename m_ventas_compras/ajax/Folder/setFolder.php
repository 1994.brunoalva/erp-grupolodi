<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Folder.php';
require '../../model/Orden.php';
require '../../model/Poliza.php';
require '../../model/Pagos.php';
require '../../model/DetallePagos.php';
require '../../model/DetalleEstado.php';
require '../../model/Contenedor.php';

require_once "../../model/EmbarqueDetalle.php";
require_once "../../conexion/Conexion.php";

$enbarqueDetalle = new EmbarqueDetalle();


$folder = new Folder('INSERT');
$datos = json_decode($_POST['array']);
$resultSet = $folder->insertAllAndGetId($datos);

$orden = Array();
$orden[]='';
$orden[]=$resultSet;
$orden[]='';
$orden[]='';
$orden[]='b1';
$ord = new Orden('INSERT');
$res = $ord->insertAll($orden);


$poli = Array();
$poli[]='';
$poli[]='';
$poli[]='';
$poli[]='';
$poli[]='';
$poli[]='';
$poli[]='';
$poli[]='';
$poli[]='';
$poli[]='';
$poli[]='';
$poli[]=$resultSet;
$poli[]='b1';
$poli[]='';

$poliza = new Poliza('INSERT');
$res = $poliza->insertAll($poli);



$pag = Array();
$pag[]='';
$pag[]=$resultSet;
$pag[]='';
$pag[]='';
$pag[]='b1';
$pagos = new Pagos('INSERT');
$res = $pagos->insertAllAndGetId($pag);

$dees = Array();
$dees[]='';
$dees[]='';
$dees[]=$resultSet;
$dees[]='b1';
$detalleEstado = new DetalleEstado('INSERT');
$res = $detalleEstado->insertAll($dees);

/*COMPROBAR SU USO*/
$qty=$datos[7];
while ($qty){
    $conte = Array();
    $conte[]='';
    $conte[]=$resultSet;
    $conte[]=$qty;
    $conte[]='';
    $conte[]='';
    $conte[]='b1';
    $contenedor = new Contenedor('INSERT');
    $res = $contenedor->insertAll2($conte);
    --$qty;
}

echo  json_encode($resultSet,JSON_PRETTY_PRINT);

