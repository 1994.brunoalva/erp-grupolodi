<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Marca.php';

$marca = new Marca('INSERT');
$datos = json_decode($_POST['array']);
$id = $marca->insertAllAndGetId($datos);
$marca = new Marca('SELECT');
$resultSet = $marca->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

