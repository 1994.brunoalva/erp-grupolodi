<?php
require '../../conexion/Conexion.php';
$conexion = (new Conexion())->getConexion();
$res = $conexion->query("SELECT *  FROM sys_com_marca");

$lista = [];
while ($row = $res->fetch_assoc()){
    $isuse=false;
    if ($temp = $conexion->query("SELECT * FROM sys_producto WHERE mar_id = {$row['mar_id']} LIMIT 1;")->fetch_array()){
        $isuse=true;
    }
    $row['isUse']= $isuse;
    $lista  []= $row;
}

echo json_encode($lista);