<?php

require '../../conexion/Conexion.php';
$conexion = (new Conexion())->getConexion();

$sql = "SELECT 
  sys_producto.*,
   sys_produ_detalle.*,
   snco.sunat_cod_codigo
   
   FROM  sys_producto
   INNER JOIN sys_produ_detalle 
    ON sys_producto.produ_deta_id = sys_produ_detalle.produ_deta_id 
    INNER JOIN sys_sunat_codigo_prod AS snco  ON sys_producto.sunat_cod_id = snco.sunat_cod_id
    WHERE sys_producto.produ_id =  {$_POST['idprod']}
    ;

";
$resp = $conexion->query($sql);
$arrRe = array();
if ($row = $resp->fetch_assoc() ){
    $arrRe= $row;
}
echo json_encode($arrRe);