<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Producto.php';
require '../../model/Unidad.php';
require '../../model/CodSunat.php';
require '../../model/Categoria.php';
require '../../model/Marca.php';
require '../../model/Modelo.php';
require '../../model/Pais.php';
require '../../model/ProductoDetalle.php';

function insertAllAndGetId($datos){
    $valores="";
    foreach ($datos as $dato) {
        if($dato == ''){
            $valores .= "NULL,";
        }else{
            $valores .= "'".$dato."',";
        }
    }
    $valores = "(".substr($valores, 0, -1).")";
    $sql =" INSERT INTO sys_productos VALUES $valores".';';
     echo $sql;
}

$datos = json_decode($_POST['array']);
//echo "--------------------------------";
//echo insertAllAndGetId($datos);
//print_r($datos);
if ($datos[0]==0){
    $datos[0]='';

$producto = new Producto('INSERT');

$id = $producto->insertAllAndGetId($datos);
$producto = new Producto('SELECT');
$resultSet = $producto->selectById($id);

if ($resultSet) {
        $idU = strval($resultSet->unidad_id);
        $idDeta = strval($resultSet->produ_deta_id);
        $idC = strval($resultSet->sunat_cod_id);
        $idCa = strval($resultSet->cat_id);
        $idMa = strval($resultSet->mar_id);
        $idMo = strval($resultSet->mod_id);
        $idPa = strval($resultSet->pais_id);

        $unidad = new Unidad('SELECT');
        $resultUni = $unidad->selectById("'" . $idU . "'");
        $resultSet->unidad_id = $resultUni;

        $productoDetalle = new ProductoDetalle('SELECT');
        $resultDeta = $productoDetalle->selectById("'" . $idDeta . "'");
        $resultSet->produ_deta_id = $resultDeta;

        $codSunat = new CodSunat('SELECT');
        $resultCod = $codSunat->selectById("'" . $idC . "'");
        $resultSet->sunat_cod_id = $resultCod;

        $categoria = new Categoria('SELECT');
        $resultCat = $categoria->selectById("'" . $idCa . "'");
        $resultSet->cat_id = $resultCat;

        $marca = new Marca('SELECT');
        $resultMar = $marca->selectById("'" . $idMa . "'");
        $resultSet->mar_id = $resultMar;

        $modelo = new Modelo('SELECT');
        $resultMod = $modelo->selectById("'" . $idMo . "'");
        $resultSet->mod_id = $resultMod;

        $pais = new Pais('SELECT');
        $resultPai = $pais->selectById("'" . $idPa . "'");
        $resultSet->pais_id = $resultPai;
}







echo  json_encode($resultSet,JSON_PRETTY_PRINT);
}else{
    $producto = new Producto('UPDATE');

    $modelo = $datos[13]==''?1:$datos[13];
    $nomen = $datos[10]==''?6:$datos[10];
    $tipoprod = $datos[9]==''?'NULL':$datos[9];
    $sql="UPDATE sys_producto
SET 
  produ_deta_id = '{$datos[2]}',
  provee_id = null,
  unidad_id = '{$datos[4]}',
  sunat_cod_id = '{$datos[5]}',
  produ_desc = '{$datos[6]}',
  produ_sku = '{$datos[7]}',
  produ_pn = '{$datos[8]}',
  tipro_id =$tipoprod,
  nom_id =$nomen,
  cat_id = '{$datos[11]}',
  mar_id = '{$datos[12]}',
  mod_id =$modelo,
  pais_id = '{$datos[14]}',
  produ_nombre = '{$datos[15]}',
  estado = '{$datos[16]}',
  peso = '{$datos[17]}'
WHERE produ_id = '{$datos[0]}';";
    $producto->existData($sql);

    //echo $sql;
    echo  json_encode(array("id"=>$datos[0]),JSON_PRETTY_PRINT);
}
