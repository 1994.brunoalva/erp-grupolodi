<?php

require '../../conexion/Conexion.php';
$conexion = (new Conexion())->getConexion();

$sql = "SELECT 
  sys_producto.*,
  sys_producto_empresa.precio,
  sys_com_marca.mar_nombre,
  sys_producto.produ_sku,
  sys_pais.pais_nombre,
  sys_empresas.emp_nombre,
  sys_empresas.emp_id,
  sys_producto_empresa.cantidad,
  sys_produ_detalle.*,
  sys_sunat_codigo_prod.sunat_cod_codigo
FROM
  sys_producto 
  INNER JOIN sys_produ_detalle 
    ON sys_producto.produ_deta_id = sys_produ_detalle.produ_deta_id 
  INNER JOIN sys_producto_empresa 
    ON sys_producto.produ_id = sys_producto_empresa.id_producto 
  INNER JOIN sys_empresas 
    ON sys_producto_empresa.id_empresa = sys_empresas.emp_id 
  INNER JOIN sys_com_marca 
    ON sys_producto.mar_id = sys_com_marca.mar_id 
  INNER JOIN sys_pais 
    ON sys_producto.pais_id = sys_pais.pais_id 
    INNER JOIN sys_sunat_codigo_prod ON sys_producto.sunat_cod_id=sys_sunat_codigo_prod.sunat_cod_id;

";
$resp = $conexion->query($sql);
$arrRe = array();
foreach ($resp as $row){
    $arrRe[]= $row;
}
echo json_encode($arrRe);