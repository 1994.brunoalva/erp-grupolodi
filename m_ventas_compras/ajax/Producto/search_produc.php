<?php

require '../../conexion/Conexion.php';
$conexion = (new Conexion())->getConexion();

$searchTerm = filter_input(INPUT_GET, 'term');



$sql = " 
  SELECT 
  sys_producto.*,
  sys_producto_empresa.precio,
  sys_com_marca.mar_nombre,
  sys_producto.produ_sku,
  sys_pais.pais_nombre,
  sys_empresas.emp_nombre,
  sys_empresas.emp_id,
  sys_producto_empresa.cantidad
FROM
  sys_producto 
  INNER JOIN sys_produ_detalle 
    ON sys_producto.produ_deta_id = sys_produ_detalle.produ_deta_id 
  INNER JOIN sys_producto_empresa 
    ON sys_producto.produ_id = sys_producto_empresa.id_producto 
  INNER JOIN sys_empresas 
    ON sys_producto_empresa.id_empresa = sys_empresas.emp_id 
  INNER JOIN sys_com_marca 
    ON sys_producto.mar_id = sys_com_marca.mar_id 
  INNER JOIN sys_pais 
    ON sys_producto.pais_id = sys_pais.pais_id 
   WHERE sys_producto.produ_nombre LIKE '%$searchTerm%' LIMIT 12;
";
//echo $sql;
$respuesta = $conexion->query($sql);
$arrRes=array();
$a_json = array();
$a_json_row = array();

foreach ($respuesta as $row){
    $row['value'] = $row['produ_nombre']." | CNT:".$row['cantidad'] ." | PRECIO: " . $row['precio'];
    $arrRe[]= $row;
}
echo json_encode($arrRe);