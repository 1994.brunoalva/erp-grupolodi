<?php


function getAllProducto()
{
    $producto = new Producto('SELECT');
    $resultProd = $producto->selectAll();
    $row = Array();

    if ($resultProd) {
        foreach ($resultProd as $item) {
            $idU = strval($item->unidad_id);
            $idDeta = strval($item->produ_deta_id);
            $idC = strval($item->sunat_cod_id);
            $idCa = strval($item->cat_id);
            $idMa = strval($item->mar_id);
            $idMo = strval($item->mod_id);
            $idPa = strval($item->pais_id);

            $unidad = new Unidad('SELECT');
            $resultUni = $unidad->selectById("'" . $idU . "'");
            $item->unidad_id = $resultUni;

            $productoDetalle = new ProductoDetalle('SELECT');
            $resultDeta = $productoDetalle->selectById("'" . $idDeta . "'");
            $item->produ_deta_id = $resultDeta;

            $codSunat = new CodSunat('SELECT');
            $resultCod = $codSunat->selectById("'" . $idC . "'");
            $item->sunat_cod_id = $resultCod;

            $categoria = new Categoria('SELECT');
            $resultCat = $categoria->selectById("'" . $idCa . "'");
            $item->cat_id = $resultCat;

            $marca = new Marca('SELECT');
            $resultMar = $marca->selectById("'" . $idMa . "'");
            $item->mar_id = $resultMar;

            $modelo = new Modelo('SELECT');
            $resultMod = $modelo->selectById("'" . $idMo . "'");
            $item->mod_id = $resultMod;

            $pais = new Pais('SELECT');
            $resultPai = $pais->selectById("'" . $idPa . "'");
            $item->pais_id = $resultPai;


            $row[] = $item;
        }
    }
    return $row;
}


?>