<?php
require '../../conexion/Conexion.php';
$conexion = (new Conexion())->getConexion();

$searchTerm = filter_input(INPUT_GET, 'term');



$sql = " 
SELECT 
  prod.produ_id,
  prod.produ_nombre,
  prod.produ_sku,
  prod.unidad_id,
  cate.cat_nombre,
  marc.mar_nombre
FROM
  sys_producto AS prod 
  INNER JOIN sys_com_categoria AS cate 
    ON prod.cat_id = cate.cat_id 
  INNER JOIN sys_com_marca AS marc 
    ON prod.mar_id = marc.mar_id 
WHERE prod.produ_nombre LIKE '%$searchTerm%' LIMIT 10;
";
//echo $sql;
$respuesta = $conexion->query($sql);
$arrRes=array();
$a_json = array();
$a_json_row = array();

foreach ($respuesta as $row){
    $row['value'] = $row['produ_nombre']." | SKU:".$row['produ_sku'] ." | " . $row['cat_nombre']." | " . $row['mar_nombre'];
    $arrRe[]= $row;
}
echo json_encode($arrRe);