<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Producto.php';
require '../../model/Unidad.php';
require '../../model/CodSunat.php';
require '../../model/TipoProducto.php';
require '../../model/Nomenglatura.php';
require '../../model/Categoria.php';
require '../../model/Marca.php';
require '../../model/Modelo.php';
require '../../model/Pais.php';
require '../../model/ProductoDetalle.php';

$id = $_POST['id'];
$producto = new Producto('SELECT');
$resultSet = $producto->selectById($id);

if ($resultSet) {
    $idU = strval($resultSet->unidad_id);
    $idDeta = strval($resultSet->produ_deta_id);
    $idC = strval($resultSet->sunat_cod_id);
    $idTi = strval($resultSet->tipro_id);
    $idNo = strval($resultSet->nom_id);
    $idCa = strval($resultSet->cat_id);
    $idMa = strval($resultSet->mar_id);
    $idMo = strval($resultSet->mod_id);
    $idPa = strval($resultSet->pais_id);

    $unidad = new Unidad('SELECT');
    $resultUni = $unidad->selectById("'" . $idU . "'");
    $resultSet->unidad_id = $resultUni;

    $productoDetalle = new ProductoDetalle('SELECT');
    $resultDeta = $productoDetalle->selectById("'" . $idDeta . "'");
    $resultSet->produ_deta_id = $resultDeta;

    $codSunat = new CodSunat('SELECT');
    $resultCod = $codSunat->selectById("'" . $idC . "'");
    $resultSet->sunat_cod_id = $resultCod;

    $tipoProducto = new TipoProducto('SELECT');
    $resultTip = $tipoProducto->selectById("'" . $idTi . "'");
    $resultSet->tipro_id = $resultTip;

    $nomenglatura = new Nomenglatura('SELECT');
    $resultNom = $nomenglatura->selectById("'" . $idNo . "'");
    $resultSet->nom_id = $resultNom;

    $categoria = new Categoria('SELECT');
    $resultCat = $categoria->selectById("'" . $idCa . "'");
    $resultSet->cat_id = $resultCat;

    $marca = new Marca('SELECT');
    $resultMar = $marca->selectById("'" . $idMa . "'");
    $resultSet->mar_id = $resultMar;

    $modelo = new Modelo('SELECT');
    $resultMod = $modelo->selectById("'" . $idMo . "'");
    $resultSet->mod_id = $resultMod;

    $pais = new Pais('SELECT');
    $resultPai = $pais->selectById("'" . $idPa . "'");
    $resultSet->pais_id = $resultPai;
}

echo  json_encode($resultSet,JSON_PRETTY_PRINT);

