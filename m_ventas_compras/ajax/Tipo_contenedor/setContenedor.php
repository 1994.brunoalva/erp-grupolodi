<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/TipoContenedor.php';

$tipoContenedor = new TipoContenedor('INSERT');
$datos = json_decode($_POST['array']);
$resultSet = $tipoContenedor->insertAll($datos);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

