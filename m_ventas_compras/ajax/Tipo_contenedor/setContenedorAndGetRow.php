<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/TipoContenedor.php';

$tipoContenedor = new TipoContenedor('INSERT');
$datos = json_decode($_POST['array']);
$id = $tipoContenedor->insertAllAndGetId($datos);
$tipoContenedor = new TipoContenedor('SELECT');
$resultSet = $tipoContenedor->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

