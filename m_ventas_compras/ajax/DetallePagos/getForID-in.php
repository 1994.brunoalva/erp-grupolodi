<?php

function getAllDetaPagosForId($id){
    $detallePagos = new DetallePagos('SELECT');
    $resultDeta= $detallePagos->selectAllByColumn('pago_id',$id);
    $row = Array();

    if ($resultDeta) {
        foreach ($resultDeta as $item) {
            $idC = strval($item->conpa_id);
            $condicionPagos = new CondicionPagos('SELECT');
            $resultCon = $condicionPagos->selectById("'".$idC."'");
            $item->conpa_id=$resultCon;
            $row[]=$item;
        }
    }
    return $row;
}
?>