<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/DetallePagos.php';
require '../../model/CondicionPagos.php';

$id=$_GET['id'];
$detallePagos = new DetallePagos('SELECT');
$resultDeta= $detallePagos->selectAllByColumn('pago_id',$id);

$row = Array();

if ($resultDeta) {
    foreach ($resultDeta as $item) {
        $idC = strval($item->conpa_id);
        $condicionPagos = new CondicionPagos('SELECT');
        $resultCon = $condicionPagos->selectById("'".$idC."'");
        $item->conpa_id=$resultCon;
        $row[]=$item;
    }
}


echo json_encode($resultDeta, JSON_PRETTY_PRINT);
?>