<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Almacen.php';

$almacen = new Almacen('INSERT');
$datos = json_decode($_POST['array']);
$id = $almacen->insertAllAndGetId($datos);

$almacen = new Almacen('SELECT');
$resultSet = $almacen->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

