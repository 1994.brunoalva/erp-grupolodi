<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Contenedor.php';

$contenedor = new Contenedor('UPDATE');
$datos = json_decode($_POST['array']);
$resultSet = $contenedor->updateById($datos);

echo  json_encode($resultSet,JSON_PRETTY_PRINT);

