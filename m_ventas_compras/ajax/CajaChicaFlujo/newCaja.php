<?php
require "../../conexion/Conexion.php";
require "../../model/CajaChica.php";
require "../../model/FlujoCajaChica.php";

$cajaChica = new CajaChica();
$flujoCajaChica= new FlujoCajaChica();

$cajaChica->setEstado("1");
$cajaChica->setIdEmpresa($_POST['idEmpresa']);
$cajaChica->setFecha($_POST["fecha"]);
$cajaChica->setVendedor(0);
$cajaChica->setTotalSoles($_POST['soles']);
$cajaChica->setTotalDolares($_POST['dolares']);

$ress = array("res"=>false);
if ($cajaChica->insertar()){
    $ress['res']=true;

    $flujoCajaChica->setIdCaja($cajaChica->getId());
    $flujoCajaChica->setHora('10:11');
    $flujoCajaChica->setDetalle($_POST['descrip']);
    $flujoCajaChica->setIngreso($_POST['dolares']);
    $flujoCajaChica->setIdMoneda(1);
    $flujoCajaChica->setIdConcepto(1);
    $flujoCajaChica->insertar();


    $flujoCajaChica->setIngreso($_POST['soles']);
    $flujoCajaChica->setIdMoneda(4);
    $flujoCajaChica->insertar();


}

echo json_encode($ress);
