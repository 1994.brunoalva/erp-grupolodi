<?php
require "../../conexion/Conexion.php";
require "../../model/FlujoCajaChica.php";

$flujoCajaChica = new FlujoCajaChica();
//idCaja
//hora,idconcepto,idmoneda,detalle,egreso

$flujoCajaChica->setIdCaja($_POST['idCaja']);
$flujoCajaChica->setHora($_POST['hora']);
$flujoCajaChica->setDetalle($_POST['detalle']);
$flujoCajaChica->setEgreso($_POST['egreso']);
$flujoCajaChica->setIdMoneda($_POST['idmoneda']);
$flujoCajaChica->setIdConcepto($_POST['idconcepto']);
$ress = array("res"=>false);
if ($flujoCajaChica->insertar()){
    $ress['res']=true;
}
echo json_encode($ress);