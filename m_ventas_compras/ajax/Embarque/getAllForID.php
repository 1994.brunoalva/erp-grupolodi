<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Embarque.php';
require '../../model/Orden.php';
require '../../model/Forwarder.php';
require '../../model/Despacho.php';
require '../../model/Linea.php';
require '../../model/Nave.php';
require '../../model/Almacen.php';



$id=$_POST['id'];
$embarque = new Embarque('SELECT');
$resultEmb = $embarque->selectByColumn('ord_id',$id);

$idO = strval($resultEmb->ord_id);
$idF = strval($resultEmb->forwa_id);
$idD = strval($resultEmb->desp_id);
$idL = strval($resultEmb->linea_id);
$idN = strval($resultEmb->nave_id);
$idA = strval($resultEmb->alm_id);

$orden = new Orden('SELECT');
$resultOrd = $orden->selectById("'".$idO."'");
$resultEmb->ord_id=$resultOrd;
/*echo "'".$idO."'\n";*/

$forwarder = new Forwarder('SELECT');
$resultFor = $forwarder->selectById("'".$idF."'");
$resultEmb->forwa_id=$resultFor;
/*echo "'".$idF."'\n";*/


$despacho = new Despacho('SELECT');
$resultDes = $despacho->selectById("'".$idD."'");
$resultEmb->desp_id=$resultDes;
/*echo "'".$idD."'\n";*/


$linea = new Linea('SELECT');
$resultLin = $linea->selectById("'".$idL."'");
$resultEmb->linea_id=$resultLin;
/*echo "'".$idL."'\n";*/


$nave = new Nave('SELECT');
$resultNav = $nave->selectById("'".$idN."'");
$resultEmb->nave_id=$resultNav;
/*echo "'".$idN."'\n";*/


$almacen = new Almacen('SELECT');
$resultAlm = $almacen->selectById("'".$idA."'");
$resultEmb->alm_id=$resultAlm;
/*echo "'".$idA."'\n";*/


echo json_encode($resultEmb, JSON_PRETTY_PRINT);
?>