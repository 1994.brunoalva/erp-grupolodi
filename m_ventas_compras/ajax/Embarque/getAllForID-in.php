<?php

function getEmbarqueForId($id){
    $embarque = new Embarque('SELECT');
    $resultEmb = $embarque->selectByColumn('ord_id',$id);

    if($resultEmb){
        $idO = strval($resultEmb->ord_id);
        $idF = strval($resultEmb->forwa_id);
        $idD = strval($resultEmb->desp_id);
        $idL = strval($resultEmb->linea_id);
        $idN = strval($resultEmb->nave_id);
        $idA = strval($resultEmb->alm_id);

        $orden = new Orden('SELECT');
        $resultOrd = $orden->selectById("'".$idO."'");
        $resultEmb->ord_id=$resultOrd;

        $forwarder = new Forwarder('SELECT');
        $resultFor = $forwarder->selectById("'".$idF."'");
        $resultEmb->forwa_id=$resultFor;

        $despacho = new Despacho('SELECT');
        $resultDes = $despacho->selectById("'".$idD."'");
        $resultEmb->desp_id=$resultDes;

        $linea = new Linea('SELECT');
        $resultLin = $linea->selectById("'".$idL."'");
        $resultEmb->linea_id=$resultLin;

        $nave = new Nave('SELECT');
        $resultNav = $nave->selectById("'".$idN."'");
        $resultEmb->nave_id=$resultNav;

        $almacen = new Almacen('SELECT');
        $resultAlm = $almacen->selectById("'".$idA."'");
        $resultEmb->alm_id=$resultAlm;
    }
    return $resultEmb;
}


?>