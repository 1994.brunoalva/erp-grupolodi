<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Incoterm.php';

$incoterm = new Incoterm('INSERT');
$datos = json_decode($_POST['array']);
$id = $incoterm->insertAllAndGetId($datos);
$incoterm = new Incoterm('SELECT');
$resultSet = $incoterm->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

