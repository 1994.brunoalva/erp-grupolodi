<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Empresa.php';

$empresa = new Empresa('INSERT');
$datos = json_decode($_POST['array']);

$id = $empresa->insertAllAndGetId($datos);
$empresa = new Empresa('SELECT');
$resultSet = $empresa->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

