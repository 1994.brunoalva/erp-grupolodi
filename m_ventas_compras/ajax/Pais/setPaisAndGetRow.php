<?php
header('Content-Type: application/json');
require '../../entidadDB/DataBase.php';
require '../../model/Pais.php';

$pais = new Pais('INSERT');
$datos = json_decode($_POST['array']);
$id = $pais->insertAllAndGetId($datos);
$pais = new Pais('SELECT');
$resultSet = $pais->selectById($id);
echo  json_encode($resultSet,JSON_PRETTY_PRINT);

