<?php
$indexRuta = 1;
require '../conexion/Conexion.php';
require '../model/TipoPago.php';
require_once '../model/model.php';
$tipoPago = new TipoPago();
$categorias = new Categoria('SELECT');
/*
$listaPa = $tipoPago->lista();
$listaTemTP = [];

foreach ($listaPa as $item) {
    $listaTemTP [] = $item;
}*/


$conexionp = (new Conexion())->getConexion();

$sql = "SELECT * FROM sys_com_almacen";
$result_almceb = $conexionp->query($sql);


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../aConfig/plugins/sweetalert2/sweetalert2.min.css">

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }


        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="../"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Almacenes Aduaneros</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <button onclick="" type="button"
                                                data-toggle="modal" data-target="#modal_almacen"
                                                class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo
                                        </button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal" class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <table id="table-almacen-adua-lista"
                                       class="table table-striped table-bordered table-hover">
                                    <thead class="bg-head-table">
                                    <tr style="background-color: #007ac3; color: white">
                                        <th style="border-right-color: #007ac3" class="text-center"></th>
                                        <th style="border-right-color: #007ac3" class="text-center">RUC</th>
                                        <th style="border-right-color: #007ac3" class="text-center">RAZON SOCIAL</th>

                                        <th style="border-right-color: #007ac3" class="text-center">DIRECCION</th>
                                        <th style="border-right-color: #007ac3" class="text-center">TELEFONO</th>
                                        <th class="text-center">OPCION</th>
                                    </tr>
                                    </thead>
                                        <?php

                                        foreach ($result_almceb as $row){ ?>
                                            <tr>
                                                <td style="color: white"><?=$row['alm_id']?></td>
                                                <td class="text-center" ><?=$row['alm_ruc']?></td>
                                                <td class="text-center"><?=$row['alm_razon_social']?></td>

                                                <td class="text-center"><?=$row['alm_dir']?></td>
                                                <td class="text-center"><?=$row['alm_tel_1']?></td>
                                                <td class="text-center"><button data-toggle="modal" data-target="#modal_almacen_edit" onclick="getddataalama(<?=$row['alm_id']?>)" class="btn btn-info"><i class="fa fa-eye"></i></button></td>
                                            </tr>
                                        <?php     }

                                        ?>
                                    <tbody>


                                    </tbody>

                                </table>
                            </div>


                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>


    <div class="modal fade" id="modal_almacen" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Nuevo almacen</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <div id="form-modal-almacen" >
                        <div class="container-fluid">
                            <div class="row text-left no-padding">
                                <div class="form-group col-xs-12 col-sm-5 col-md-5">

                                    <label label class="col-xs-12 no-padding">BUSCAR POR N° RUC :</label>
                                    <div class="input-group  col-xs-12 no-padding ">
                                        <input id="modal-almacen-input-ruc" name="folder_input_ruc_emp" maxlength="11"
                                               required
                                               placeholder="Ingrese numero de Ruc" data-date-format="yyyy-dd-mm"
                                               class='form-control'/>
                                        <span class="input-group-btn">
                                            <a class="btn btn-primary" id="modal-almacen-btn-buscar"><i
                                                        class="fa fa-search"></i></a>
                                    </span>
                                    </div>

                                </div>
                                <div class="form-group col-xs-12 col-sm-7 col-md-7">
                                    <label class="col-xs-12 no-padding">RAZON SOCIAL :</label>
                                    <input id="modal-almacen-input-razon-social" disabled type="text" required
                                           class="form-control col-xs-12 bg-blanco" placeholder="Razon social">
                                </div>
                                <!--OCULTO  Y  POR APROBAR SU UTILIZACION-->
                                <div class="form-group col-xs-12 col-sm-12 col-md-6 no-display">
                                    <label class="col-xs-12 no-padding">JURISDICCION :</label>
                                    <input id="modal-almacen-input-juris" disabled type="text" required
                                           class="form-control col-xs-12 bg-blanco" placeholder="Direccion fiscal">
                                </div>
                                <!--OCULTO  Y  POR APROBAR SU UTILIZACION-->
                                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                    <label class="col-xs-12 no-padding">REPRESENTANTE :</label>
                                    <input id="modal-almacen-input-represen" disabled type="text" required
                                           class="form-control col-xs-12 bg-blanco" placeholder="Direccion fiscal">
                                </div>
                                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                    <label class="col-xs-12 no-padding">CARGO :</label>
                                    <input id="modal-almacen-input-cargo" disabled type="text" required
                                           class="form-control col-xs-12 bg-blanco" placeholder="Cargo de representante">
                                </div>
                                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                    <label class="col-xs-12 no-padding">DIRECCION :</label>
                                    <input id="modal-almacen-input-direccion" disabled type="text" required
                                           class="form-control col-xs-12 bg-blanco" placeholder="Direccion fiscal">
                                </div>
                                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                    <label class="col-xs-12 no-padding">TELEFONO 2 :</label>
                                    <input id="modal-almacen-input-telefono-1" type="text" required
                                           class="form-control col-xs-12 input-number" placeholder="Telef. N° ">
                                </div>
                                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                    <label class="col-xs-12 no-padding">TELEFONO 2 :</label>
                                    <input id="modal-almacen-input-telefono-2" type="text" required
                                           class="form-control col-xs-12 input-number" placeholder="Telef. N° ">
                                </div>

                            </div>
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">

                            <button type="button" id="modal-almacen-btn-guardar" class="btn btn-primary">
                                Guardar
                            </button>
                            <button type="button" id="modal-almacen-btn-limpiar" class="btn btn-default">
                                Limpiar
                            </button>
                            <button type="button" id="modal-almacen-btn-cerrar" class="btn btn-success"
                                    data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_almacen_edit" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Almacen Aduanero</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <div id="form-modal-almacen" >
                        <div class="container-fluid">
                            <div class="row text-left no-padding">
                                <div class="form-group col-xs-12 col-sm-5 col-md-5">

                                    <label label class="col-xs-12 no-padding">BUSCAR POR N° RUC :</label>
                                    <div class="input-group  col-xs-12 no-padding ">
                                        <input id="modal-almacen-input-id_edit" type="hidden">
                                        <input id="modal-almacen-input-ruc_edit" name="folder_input_ruc_emp" maxlength="11"
                                               disabled
                                               placeholder="Ingrese numero de Ruc" data-date-format="yyyy-dd-mm"
                                               class='form-control'/>
                                        
                                    </div>

                                </div>
                                <div class="form-group col-xs-12 col-sm-7 col-md-7">
                                    <label class="col-xs-12 no-padding">RAZON SOCIAL :</label>
                                    <input id="modal-almacen-input-razon-social_edit" disabled type="text" required
                                           class="form-control col-xs-12 bg-blanco" placeholder="Razon social">
                                </div>
                                <!--OCULTO  Y  POR APROBAR SU UTILIZACION-->
                                <div class="form-group col-xs-12 col-sm-12 col-md-6 no-display">
                                    <label class="col-xs-12 no-padding">JURISDICCION :</label>
                                    <input id="modal-almacen-input-juris_edit" disabled type="text" required
                                           class="form-control col-xs-12 bg-blanco" placeholder="Direccion fiscal">
                                </div>
                                <!--OCULTO  Y  POR APROBAR SU UTILIZACION-->
                                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                    <label class="col-xs-12 no-padding">REPRESENTANTE :</label>
                                    <input id="modal-almacen-input-represen_edit" disabled type="text" required
                                           class="form-control col-xs-12 bg-blanco" placeholder="Direccion fiscal">
                                </div>
                                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                    <label class="col-xs-12 no-padding">CARGO :</label>
                                    <input id="modal-almacen-input-cargo_edit" disabled type="text" required
                                           class="form-control col-xs-12 bg-blanco" placeholder="Cargo de representante">
                                </div>
                                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                    <label class="col-xs-12 no-padding">DIRECCION :</label>
                                    <input id="modal-almacen-input-direccion_edit" disabled type="text" required
                                           class="form-control col-xs-12 bg-blanco" placeholder="Direccion fiscal">
                                </div>
                                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                    <label class="col-xs-12 no-padding">TELEFONO 2 :</label>
                                    <input id="modal-almacen-input-telefono-1_edit" type="text" required
                                           class="form-control col-xs-12 input-number" placeholder="Telef. N° ">
                                </div>
                                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                    <label class="col-xs-12 no-padding">TELEFONO 2 :</label>
                                    <input id="modal-almacen-input-telefono-2_edit" type="text" required
                                           class="form-control col-xs-12 input-number" placeholder="Telef. N° ">
                                </div>

                            </div>
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">

                            <button type="button" onclick="guardaralmaadua()" class="btn btn-primary">
                                Guardar
                            </button>

                            <button type="button"  class="btn btn-success"
                                    data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <style>
        #img-file-preview-zone {
            -webkit-box-shadow: 1px 1px 4px 1px rgba(75, 87, 209, 1);
            -moz-box-shadow: 1px 1px 4px 1px rgba(75, 87, 209, 1);
            box-shadow: 1px 1px 4px 1px rgba(75, 87, 209, 1);
            border-radius: 3px;
        }
    </style>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <!--script  type="module" src="../aConfig/scripts/sku.js"></script-->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        $('#select_nomenglatura').change(function () {
            genNombreProducto();
        });
        $('#select_modal_prod_modelo').change(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-pliege").keyup(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-serie").keyup(function () {
            genNombreProducto();
        });

        function genNombreProducto2() {
            const cate = $("#select_modal_pro_categoria option:selected").text();
            var nombrePr = cate;
            if (cate == "NEUMATICOS") {
                const nomen = $("#select_nomenglatura option:selected").text();
                const tipo = $("#select_opc option:selected").text();
                const ancho = $("#modal-producto-input-neu-ancho").val();
                nombrePr += " " + ancho;
                if (nomen == "MILIMETRICA") {
                    const serie = $("#modal-producto-input-neu-serie").val();
                    nombrePr += "/" + serie;
                }
                if (tipo == "RADIAL") {
                    nombrePr += "R";
                }
                const aroNeu = $("#modal-producto-input-neu-aro").val();
                const plieges = $("#modal-producto-input-neu-pliege").val();
                const marca = $("#select_modal_prod_marca option:selected").text();
                const modelo = $("#select_modal_prod_modelo option:selected").text();
                const pais = $("#select_prod_pais option:selected").text();
                nombrePr += "-" + aroNeu + " " + plieges + "PR " + marca + " " + modelo + " " + pais;

            } else if (cate == "CAMARAS") {
                /*  const  nomen = $("#select_nomenglatura option:selected").text();
                  if (nomen=="MILIMETRICA"){
                      const serie= $("#modal-producto-input-neu-serie").val();
                      nombrePr+="/"+serie;
                  }
                  if (tipo=="RADIAL"){
                      nombrePr+="R";
                  }*/
            }
            $("#modal-producto-input-nombre").val(nombrePr);
        }
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function removeItemFromArr(arr, index) {

            arr.splice(index, 1);
        }

        function addPais() {
            const pais = $("#modal-pais-input-nombre").val();

            $.ajax({
                type: "POST",
                url: "../ajax/Pais/add_pais.php",
                data: {pais},
                success: function (data) {

                    console.log(data);
                    if (isJson(data)) {
                        const jso = JSON.parse(data);
                        if (jso.res) {
                            $("#select_prod_pais").append('<option value="' + jso.data.id + '">' + jso.data.nombre + '</option>');
                            $('#producto-select-presentacion2').selectpicker('refresh');
                            $("#modal-pais-input-nombre").val('');
                            $("#modal_pais").modal('hide');
                            $.toast({
                                heading: 'EXITOSO',
                                text: "Se agrego",
                                icon: 'success',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        } else {
                            console.log(data);
                            $.toast({
                                heading: 'ERROR',
                                text: "Error No se pudo agregar",
                                icon: 'error',
                                position: 'top-center',
                                hideAfter: '2500',
                            });
                        }
                    } else {
                        $.toast({
                            heading: 'ERROR',
                            text: "Error",
                            icon: 'error',
                            position: 'top-center',
                            hideAfter: '2500',
                        });
                        console.log(data);
                    }
                }
            });

        }

        $(document).ready(function () {
            $('#table-codigo-sunat').DataTable({
                /*scrollY: false,*/
                /*scrollX: true,*/
                paging: false,
                /* lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],*/
                language: {
                    url: '../assets/Spanish.json'
                }
            });
        });
    </script>


</body>



<script type="text/javascript">
    var idproducto=0;
    var idproductoDeta=0;

    //MODALES._data.productos.iniciarDatos= true;

    function getddataalama(id) {

        $.ajax({
            type: "POST",
            url:  '../ajax/Almacen/getdatabyID.php',
            data: {idalma:id},
            success: function (data) {
                data = JSON.parse(data);
                $('#modal-almacen-input-id_edit').val(data.alm_id);
                $('#modal-almacen-input-ruc_edit').val(data.alm_ruc);
                $('#modal-almacen-input-razon-social_edit').val(data.alm_razon_social);
                $('#modal-almacen-input-represen_edit').val(data.alm_rep);
                $('#modal-almacen-input-cargo_edit').val(data.alm_cargo);
                $('#modal-almacen-input-direccion_edit').val(data.alm_dir);
                $('#modal-almacen-input-telefono-1_edit').val(data.alm_tel_1);
                $('#modal-almacen-input-telefono-2_edit').val(data.alm_tel_2);

                console.log(data)
            }
        });

    }
    function guardaralmaadua() {
       const idal =$('#modal-almacen-input-id_edit').val();
        const ruc= $('#modal-almacen-input-ruc_edit').val();
        const razon = $('#modal-almacen-input-razon-social_edit').val();
        const respo=$('#modal-almacen-input-represen_edit').val();
        const cargo=$('#modal-almacen-input-cargo_edit').val();
        const direc=$('#modal-almacen-input-direccion_edit').val();
        const tel1=$('#modal-almacen-input-telefono-1_edit').val();
        const tel2 = $('#modal-almacen-input-telefono-2_edit').val();
        $.ajax({
            type: "POST",
            url: '../ajax/Almacen/actualizarAlmacen.php',
            data: {idal,ruc,razon,respo,cargo,direc,tel1,tel2},
            success: function (data) {

                $('#modal_almacen_edit').modal('hide');

                alerSuccess('Se ha actualizado la informacion');
                setTimeout(function () {
                    location.reload();
                },50)

            }
        });

    }

    function setAlmacen(almacenData) {
        $.ajax({
            data: {'array': JSON.stringify(almacenData)},
            url: '../ajax/Almacen/setAlmacenAndGetRow.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json.length!==0) {
                    console.log('AGREGANDO ALMACEN');

                    $('#modal_almacen').modal('hide');

                    alerSuccess('Se ha creado nueva Almacen');
                    setTimeout(function () {
                        location.reload();
                    },50)
                } else {
                    alerInfo('No se pudo crear Almacen ingrese todos los campos');
                }
            },
            error: function () {
                alerError('Error al crear Almacen<br>Reporte este error!!');
            }
        });
    }

    $(document).ready(function () {
        $('#modal-almacen-btn-guardar').click(function () {
            var almacenData = new Array();
            almacenData.push('');
            almacenData.push($('#modal-almacen-input-ruc').val());
            almacenData.push($('#modal-almacen-input-razon-social').val());
            almacenData.push($('#modal-almacen-input-juris').val());
            almacenData.push($('#modal-almacen-input-represen').val());
            almacenData.push($('#modal-almacen-input-cargo').val());
            almacenData.push($('#modal-almacen-input-direccion').val());
            almacenData.push($('#modal-almacen-input-telefono-1').val());
            almacenData.push($('#modal-almacen-input-telefono-2').val());
            almacenData.push('b1');
            console.log(almacenData);
            setAlmacen(almacenData);

        });

        $('#modal-almacen-btn-buscar').click(function () {
            ruc =$('#modal-almacen-input-ruc').val();
          //  $('body').css('cursor', 'wait');
            $.ajax({
                data: {ruc},
                url: '../ajax/libSunat/sunat/example/consultaRuc.php',
                type: 'POST',
                /* async: false,*/
                beforeSend: function() {
                    alerInfo('Buscando RUC en Sunat, Porfavor espere ..');
                },
                success: function (response) {
                    var json = JSON.parse(JSON.stringify(response));
                    console.log(json);
                    if (json.success) {
                        console.log(json);
                        $('body').css('cursor', 'default');
                        $('#modal-almacen-input-razon-social').val(json.result.razon_social);
                        $('#modal-almacen-input-direccion').val(json.result.direccion);
                        $(json.result.representantes_legales).each(function (i,v) {
                            if(v.cargo ==='GERENTE GENERAL'){
                                $('#modal-almacen-input-represen').val(v.nombre);
                                $('#modal-almacen-input-cargo').val(v.cargo);
                            }
                        });
                        /*$('#modal-almacen-input-represen').val(json.result.representantes_legales[]);*/
                        alerSuccess('Cargando Datos de empresa');
                    } else {
                        $('body').css('cursor', 'default');
                        alerWar(json.message);
                    }

                },
                error: function () {
                    alerError('Error al en el servidor de SUNAT<br>Reporte este error!!');
                },
            });
        });

        $("#table-almacen-adua-lista").DataTable({
            "dom": 'Bfrtip',
            "order": [[ 0, "desc" ]],
            language: {
                url: '../assets/Spanish.json'
            },
        });


        $('#tttttttttttttttt').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
        $('#1111111111111').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });


    });

    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-file-preview-zone').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-file-image").change(function () {
        readImage(this);
        var url = '';
        var valor = $('#input-file-image').val();
        for (var i = 0; i < valor.length; i++) {
            if (i >= 12) {
                url += valor[i];
            }
        }

        $("#label-file-image").text(url);
    });

    function readImage2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-file-preview-zone-modelo').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-file-image-modelo").change(function () {
        readImage2(this);
        var url = '';
        var valor = $('#input-file-image-modelo').val();
        for (var i = 0; i < valor.length; i++) {
            if (i >= 12) {
                url += valor[i];
            }
        }

        // $("#label-file-image").text(url);
    });
    function alerError(msg) {
        $.toast({
            heading: 'ERROR',
            text: msg,
            icon: 'error',
            position: 'top-right',
            hideAfter: '2500',
        });
    }
    function alerSuccess(msg) {
        $.toast({
            heading: 'EXITOSO',
            text: msg,
            icon: 'success',
            position: 'top-right',
            hideAfter: '2500',
        });
    }
    function alerInfo(msg) {
        $.toast({
            heading: 'INFORMACION',
            text: msg,
            icon: 'info',
            position: 'top-right',
            hideAfter: '2500',
        });
    }
    function alerWar(msg) {
        $.toast({
            heading: 'ALERTA',
            text: msg,
            icon: 'warning',
            position: 'top-right',
            hideAfter: '2500',
        });
    }
</script>

</html>
