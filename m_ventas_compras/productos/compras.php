<?php
$indexRuta=1;
require  '../conexion/Conexion.php';


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">
    <link href="../aConfig/plugins/sweetalert2/sweetalert2.min.css">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../public/plugins/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">


</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Compras Nacionales</strong>
                                        </h2>
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <a href="registro_compra.php" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Agregar
                                        </a>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id=""  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <table id="tabla-compras-nac" class="table table-striped table-bordered table-hover" style="width:100%">
                                    <thead class="bg-head-table">
                                    <tr  style="background-color: #007ac3; color: white">
                                        <th  style="border-right-color: #007ac3; text-align: center"></th>
                                        <th  style="border-right-color: #007ac3; text-align: center">EMPRESA</th>
                                        <th  style="border-right-color: #007ac3; text-align: center">PROVEEDOR</th>
                                        <th  style="border-right-color: #007ac3; text-align: center">FECHA</th>
                                        <th  style="border-right-color: #007ac3; text-align: center">NUM.</th>
                                        <th  style="border-right-color: #007ac3; text-align: center">MONEDA</th>
                                        <th  style="border-right-color: #007ac3; text-align: center">MONTO</th>
                                        <th  style="border-right-color: #007ac3; text-align: center">DOC.</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../aConfig/scripts/venta.js"></script>
    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <<script type="text/javascript">
        function procesar(id) {
            swal({
                title: "¿Desea procesar esta Cotizacion?",
                text: "",
                icon: "warning",
                dangerMode: false,
                buttons: ["NO", "SI"],
            })
                .then((ressss) => {
                    console.log(ressss);
                    if (ressss){
                        window.locationf="procesar_cotizacion.php?coti="+id;
                    }

                });

        }
        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [ day, month, year].join('-');
        }

        $(document).ready(function () {

            $("#tabla-compras-nac").DataTable({
                "processing": true,
                "serverSide": true,
                "sAjaxSource": "../ajax/ServerSide/serversideCompras.php",
                order: [[0, "desc" ]],
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel'
                ],
                columnDefs: [
                    {
                        "targets": 0,
                        "data": "coti_id",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;color: white">'+row[0]+'</span>';

                        }
                    },
                    {
                        "targets": 1,
                        "data": "cli_tdoc",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[1]+'</span>';

                        }
                    },
                    {
                        "targets": 2,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[2]+'</span>';

                        }
                    },
                    {
                        "targets": 3,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+formatDate(row[3])+'</span>';

                        }
                    },
                    {
                        "targets": 4,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[4]+'</span>';

                        }
                    },
                    {
                        "targets": 5,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+(row[5]==1?"SOLES":"DOLARES")+'</span>';

                        }
                    },
                    {
                        "targets": 6,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[6]+'</span>';

                        }
                    },
                    {
                        "targets": 7,
                        "data": "",
                        "render": function (data, type, row, meta) {
                            if (row[7].length>0){
                                return '<div class="text-center" style="width: 100%"><a target="_blank" class="btn btn-info" href="../imagenes/doc_compra/'+row[7]+'" ><i class="fa fa-file-alt"></i></a></div>';
                            }else{
                                return "";
                            }


                        }
                    },
                    {
                        "targets": 8,
                        "data": "",
                        "render": function (data, type, row, meta) {
                            return '<div class="text-center" style="width: 100%"><a  class="btn btn-primary" href="view_compra.php?compra='+row[8]+'" ><i class="fa fa-eye"></i></a></div>';

                        }
                    },
                ],
                language: {
                    url: '../assets/Spanish.json'
                }
            });




        });

    </script>


</body>

</html>
