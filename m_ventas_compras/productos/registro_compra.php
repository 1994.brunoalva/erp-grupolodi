<?php
$indexRuta=1;
require  '../conexion/Conexion.php';

$conexion = (new Conexion())->getConexion();
$sql="SELECT
  emp_id, 
  emp_nombre 
FROM sys_empresas ";
$resEmpre = $conexion->query($sql);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">
    <link href="../aConfig/plugins/sweetalert2/sweetalert2.min.css">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../aConfig/plugins/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">


</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Compras Nacionales</strong>
                                        </h2>
                                    </div>
                                    <!--BOTONES-->

                                    <div class="col-lg-6 text-right">
                                        <button onclick="$( '#boton-submit' ).click();"  type="button" class="btn btn-primary"><i
                                                    class="glyphicon glyphicon-floppy-save"></i> Guardar Compra
                                        </button>
                                        <span style="color: white">----------------------------</span>
                                        <a href="compras.php" class="btn btn-warning">
                                            <i class="glyphicon glyphicon-chevron-left"></i> Salir
                                        </a>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>
                            <div id="contenedor-principal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">

                                <div class="row">
                                    <form id="form-guardar" v-on:submit.prevent="guardar">
                                        <input type="submit" id="boton-submit" style="display: none">
                                        <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                            <label class="col-xs-12 no-padding">EMPRESA:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <select required v-model="compra.empresa" class="form-control"  >
                                                    <?php
                                                    foreach ($resEmpre as $emp){
                                                        echo "<option value='{$emp['emp_id']}'>{$emp['emp_nombre']}</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                            <label class="col-xs-12 no-padding">PROVEEDOR:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <input required v-model="compra.nombreProvee"  type="text" id="input-proveedor-buscar" class="form-control"  >
                                                <span class="input-group-btn">
                                                <button   type="button" class="btn btn-primary"
                                                        data-toggle="modal" data-target="#modal_reg_proveedor">
                                                    <i class="fa fa-plus"></i></button>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">FECHA:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <input required v-model="compra.fecha"  type="date" class="form-control"  >
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">DOCUMENTO:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <select required v-model="compra.tido"  class="form-control"  >
                                                    <option value="1">FACTURA</option>
                                                    <option value="3">BOLETA</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">NUN. REF.:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <input required v-model="compra.numDoc"  type="text" class="form-control"  placeholder="ejem.  FF02-0001">
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">MONEDA:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <select required @change="onChangeMoneda($event)"  v-model="compra.moneda"  class="form-control"  >
                                                    <option value="1">SOLES</option>
                                                    <option value="2">DOLARES</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">TAZA CAMBIO:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <input required :disabled="compra.moneda!=2" @keypress="onlyNumber" v-model="compra.tc"  type="text" class="form-control"  placeholder="">
                                            </div>
                                        </div>

                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">ARCHIVO:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <input id="input-file-doc" type="file" class="form-control-file" >

                                            </div>
                                        </div>
                                    </form>


                                </div>
                                <div class="form-group ">
                                    <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    PRODUCTOS DE LA COMPRA<!--Padding is optional-->
                                                  </span>

                                    </div>
                                </div>
                                <div class="row">
                                    <form  v-on:submit.prevent="agregar">
                                        <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                            <label class="col-xs-12 no-padding">BUSCAR PRODUCTO:</label>
                                            <div class="input-group col-xs-12">
                                                <input required v-model="dataRegistro.nombre" id="input-producto-buscar"  type="text" class="form-control"
                                                       aria-describedby="basic-addon1"
                                                       value="" placeholder="">

                                                <span class="input-group-btn">
                                                <button  type="button" class="btn btn-primary"
                                                        data-toggle="modal" data-target="#modal_reg_prducto">
                                                    <i class="fa fa-plus"></i></button>
                                            </span>

                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">PRECIO/UND:</label>
                                            <div class="input-group col-xs-12">
                                                <input required v-model="dataRegistro.precio" @keypress="onlyNumber" style="text-align: center"   type="text" class="form-control"
                                                       aria-describedby="basic-addon1"
                                                       value="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">CANTIDAD:</label>
                                            <div class="input-group col-xs-12">
                                                <input required v-model="dataRegistro.cantidad" @keypress="onlyNumber" style="text-align: center"   type="text" class="form-control"
                                                       aria-describedby="basic-addon1"
                                                       value="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">TOTAL: </label>
                                            <div class="input-group col-xs-12">
                                                <input :value="dataRegistro.cantidad*dataRegistro.precio" style="text-align: center" disabled  type="text" class="form-control"
                                                       aria-describedby="basic-addon1"
                                                       value="" placeholder="">
                                            </div>
                                        </div>

                                        <div class="form-group col-xs-7 col-sm-7 col-md-1">
                                            <label style="color: white" class="col-xs-12 no-padding"></label>
                                            <div class="input-group col-xs-12">
                                                <label     class="col-xs-12 " style="color: white">.</label>
                                                <button  style="margin-bottom: 5px;" id="" type="submit"  class="btn btn-primary">AGREGAR</button>


                                            </div>
                                        </div>
                                    </form>


                                </div>

                                <div class="row">
                                    <table  class="table table-striped table-bordered table-hover" style="width:100%">
                                        <thead class="bg-head-table">
                                        <tr  style="background-color: #007ac3; color: white">
                                            <th  style="border-right-color: #007ac3; text-align: center">#</th>
                                            <th  style="border-right-color: #007ac3; text-align: center">PRODUCTO</th>
                                            <th  style="border-right-color: #007ac3; text-align: center">MARCA</th>
                                            <th  style="border-right-color: #007ac3; text-align: center">CANTIDAD</th>
                                            <th  style="border-right-color: #007ac3; text-align: center">PRECIO</th>
                                            <th  style="border-right-color: #007ac3; text-align: center">IMPORTE</th>
                                            <th></th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr v-for="(item, index) in productos">

                                            <td class="text-center">{{index+1}}</td>
                                            <td class="text-center">{{item.producto}}</td>
                                            <td class="text-center">{{item.marca}}</td>
                                            <td class="text-center">{{item.cantidad}}</td>
                                            <td class="text-center">{{item.precio}}</td>
                                            <td class="text-center">{{item.importe.toFixed(2)}}</td>
                                            <td class="text-center"><button v-on:click="eliminarItem(index)" class="btn btn-danger"><i class="fa fa-times"></i></button></td>

                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">Sub. Total</td>
                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{subTotal.toFixed(2)}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">IGV</td>
                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{igv.toFixed(2)}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">Total</td>
                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{compra.monto.toFixed(2)}}</td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_reg_proveedor" tabindex="-1" role="dialog" aria-hidden="true"
         style="z-index: 1800; display: none;">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Registrar Proveedor</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form action="#" v-on:submit.prevent="guardarProvedor()">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="form-group col-xs-7 col-sm-7 col-md-6">
                                    <label class="col-xs-12 no-padding">RUC: </label>
                                    <div class="input-group col-xs-12">
                                        <input @keypress="onlyNumber"   v-model="ruc"  type="text" class="form-control"
                                               aria-describedby="basic-addon1"
                                               value="" placeholder="">
                                        <span class="input-group-btn">
                                        <button v-on:click=" buscarDocumento()" id="frame-new-btn-add-importador" type="button" class="btn btn-primary" >
                                            <i class="fa fa-search"></i></button>
                                    </span>
                                    </div>
                                </div>
                                <div class="form-group col-xs-7 col-sm-7 col-md-6">
                                    <label class="col-xs-12 no-padding">Razon Social: </label>
                                    <div class="input-group col-xs-12">
                                        <input v-model="razon"  required   type="text" class="form-control"
                                               aria-describedby="basic-addon1"
                                               value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-xs-7 col-sm-7 col-md-12">
                                    <label class="col-xs-12 no-padding">Direccion: </label>
                                    <div class="input-group col-xs-12">
                                        <input v-model="dir"    type="text" class="form-control"
                                                aria-describedby="basic-addon1"
                                                value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-xs-7 col-sm-7 col-md-6">
                                    <label class="col-xs-12 no-padding">Telefono 1: </label>
                                    <div class="input-group col-xs-12">
                                        <input v-model="tel1"     type="text" class="form-control"
                                                aria-describedby="basic-addon1"
                                                value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-xs-7 col-sm-7 col-md-6">
                                    <label class="col-xs-12 no-padding">Telefono 2: </label>
                                    <div class="input-group col-xs-12">
                                        <input v-model="tel2"     type="text" class="form-control"
                                                aria-describedby="basic-addon1"
                                                value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-xs-7 col-sm-7 col-md-12">
                                    <label class="col-xs-12 no-padding">Email: </label>
                                    <div class="input-group col-xs-12">
                                        <input v-model="email"    type="text" class="form-control"
                                               aria-describedby="basic-addon1"
                                               value="" placeholder="">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">
                            <button type="submit" class="btn btn-primary">
                                Guardar
                            </button>

                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_reg_prducto" tabindex="-1" role="dialog" aria-hidden="true"
         style="z-index: 1800; display: none;">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Registrar Producto</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form action="#" v-on:submit.prevent="guardarProducto()">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="form-group col-xs-7 col-sm-7 col-md-6">
                                    <label class="col-xs-12 no-padding">Nombre Producto: </label>
                                    <div class="input-group col-xs-12">
                                        <input    v-model="dataR.nombre" required type="text" class="form-control"
                                               aria-describedby="basic-addon1"
                                               value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-xs-7 col-sm-7 col-md-6">
                                    <label class="col-xs-12 no-padding">Marca: </label>
                                    <div class="input-group col-xs-12">
                                        <select v-model="dataR.marca"  required  class="form-control selectpicker show-tick no-padding "
                                                data-live-search="true"
                                                data-size="5">
                                            <option v-for="item in listaMarcas" :value="item.mar_id" >{{item.mar_nombre}}</option>
                                        </select>
                                        <span class="input-group-btn">
                                        <button  type="button" class="btn btn-primary"  data-toggle="modal" data-target="#modal_marca">
                                            <i class="fa fa-plus"></i></button>
                                    </span>
                                    </div>
                                </div>



                            </div>

                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">
                            <button type="submit" class="btn btn-primary">
                                Guardar
                            </button>

                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_marca" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800; display: none;">
        <div class="modal-dialog modal-xs " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Agregar Nueva Marca</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <div class="container-fluid">
                        <div class="form-group col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Nombre:</label>
                            <input id="nombre-marca-input"  class="form-control" type="text" placeholder="" required="">
                        </div>

                    </div>

                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <button type="button" onclick="AppProd.guardarMarca()"   class="btn btn-primary">
                            Guardar
                        </button>
                        <button  type="button"   class="btn btn-success" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../aConfig/scripts/venta.js"></script>
    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <<script type="text/javascript">
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }



        const AppProd = new Vue({
            el:"#modal_reg_prducto",
            data:{
                dataR:{
                  nombre:'',
                  marca: '',
                  unidad:"10",
                  pais:'80'
                },
                listaMarcas:[],


            },
            methods:{
                guardarMarca(){
                    const marca = $("#nombre-marca-input").val();
                    if (marca.length>0){
                        $.ajax({
                            type: "POST",
                            url: "../ajax/Marca/setNewMarca.php",
                            data: {idc:6,nom:marca,log:''},
                            success: function (data) {
                                data = JSON.parse(data)
                                console.log(data)
                                AppProd._data.listaMarcas.push(data.data)
                                $.toast({
                                    heading: 'EXITOSO',
                                    text: "Guardado",
                                    icon: 'success',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });
                                $('#modal_marca').modal('hide');
                                setTimeout(function () {
                                    $(".selectpicker").selectpicker('refresh');
                                },300)
                            }
                        });

                    }else{
                        $.toast({
                            heading: 'INFORMACION',
                            text: "Complete el campo",
                            icon: 'info',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                    }
                },

                getMarcas(){
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Marca/getMarcaForCategoria.php",
                        data: {id:6},
                        success: function (data) {
                            console.log(data)
                            AppProd._data.listaMarcas = data
                            setTimeout(function () {
                                $(".selectpicker").selectpicker('refresh');
                            },300)
                        }
                    });

                },
                guardarProducto(){
                    const data = {... this.dataR}
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Producto/setProdExtra.php",
                        data: data,
                        success: function (resp) {
                            console.log(resp);
                            swal("Producto Guardado", "", "success")
                                .then(function () {
                                    AppProd._data.dataR.nombre="";
                                    AppProd._data.dataR.marca="";
                                    $('#modal_reg_prducto').modal('hide');
                                });
                        }
                    });



                }
            }
        });

        const AppProvee = new Vue({
            el:"#modal_reg_proveedor",
            data:{
                ruc:'',
                razon:'',
                dir:'',
                tel1:'',
                tel2:'',
                email:''
            },
            methods: {
                guardarProvedor(){

                    $.ajax({
                        type: "POST",
                        url: "../ajax/Proveedores/newProveedornacional.php",
                        data: {... AppProvee._data},
                        success: function (data) {
                            console.log(data)
                            data = JSON.parse(data)
                            if (data.res){
                                swal("Proveedor Guardado", "", "success")
                                    .then(function () {
                                        AppProvee._data.ruc="";
                                        AppProvee._data.razon="";
                                        AppProvee._data.dir="";
                                        AppProvee._data.tel1="";
                                        AppProvee._data.tel2="";
                                        AppProvee._data.email="";
                                        $('#modal_reg_proveedor').modal('hide');
                                    });
                            }else{
                                $.toast({
                                    heading: 'INFORMACION',
                                    text: "Error al guardar",
                                    icon: 'error',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });
                            }
                        }
                    });

                },
                onlyNumber ($event) {
                    //console.log($event.keyCode); //keyCodes value
                    let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
                    if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                        $event.preventDefault();
                    }
                },
                buscarDocumento(){
                      $.toast({
                            heading: 'INFORMACION',
                            text: "Buscando Documento",
                            icon: 'info',
                            position: 'top-right',
                            hideAfter: '2500',
                        });

                    $.ajax({
                        type: "POST",
                        url:"../ajax/libSunat/sunat/example/consultaRuc.php",
                        data: {ruc:AppProvee._data.ruc},
                        success: function (data) {
                            console.log(data)
                            if (data.success){
                                $.toast({
                                    heading: 'EXITOSO',
                                    text: "Documento encontrado",
                                    icon: 'success',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });
                                AppProvee._data.razon = data.result.razon_social
                                AppProvee._data.dir = data.result.direccion
                            }else{
                                $.toast({
                                    heading: 'ERROR',
                                    text: "Documento no encontrado",
                                    icon: 'error',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });
                            }
                        }
                    });

                }
            }
        });


        const APP = new Vue({
            el:"#contenedor-principal",
            data:{
                simboleModena:'',
                igv:0,
                totalTabla:0,
                dataRegistro:{
                    preData:{},
                    nombre:'',
                    cantidad:'',
                    precio:''
                },
                compra:{
                    empresa:'',
                    idProvee:'',
                    nombreProvee:'',
                    fecha:'',
                    tido:'',
                    numDoc:'',
                    moneda:'',
                    tc:'0.00',
                    monto:0
                },
                productos:[]
            },
            methods:{
                guardar(){
                    console.log("ssssssssss")
                    if (this.productos.length>0){
                        this.guardarCompra();
                    }else{
                        swal("No sé ha agregado productos a la lista")
                    }
                },
                onChangeMoneda(event){
                    if(event.target.value==2){
                        this.compra.tc='';
                    }else{
                        this.compra.tc='0.00';
                    }
                },
                guardarCompra(){
                    var data = {...this.compra}
                    data.docUrl="";
                    data.productos = JSON.stringify(this.productos);
                    console.log(data)
                    var dataFile = $('#input-file-doc').val();

                    if (dataFile.length > 0){
                        var frmData = new FormData();
                        frmData.append('file', $("#input-file-doc")[0].files[0]);


                        $.ajax({
                            xhr: function() {
                                var xhr = new window.XMLHttpRequest();
                                xhr.upload.addEventListener("progress", function(evt) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = ((evt.loaded / evt.total) * 100);
                                        //app._data.progreso=percentComplete;
                                    }
                                }, false);
                                return xhr;
                            },
                            type: 'POST',
                            url: '../ajax/ManagerFiles/upd_files_doc.php',
                            data: frmData,
                            contentType: false,
                            cache: false,
                            processData:false,
                            beforeSend: function(){
                                // app._data.progreso=0;
                                // app._data.starProgres=true;
                                $.toast({
                                    heading: 'INFORMACION',
                                    text: "Guardando archivo",
                                    icon: 'info',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });
                            },
                            error:function(err){
                                console.log(err.responseText)
                                //alert('File upload failed, please try again.');
                                $.toast({
                                    heading: 'ERROR',
                                    text: "El archivo no se pudo guardar",
                                    icon: 'error',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });

                            },
                            success: function(resp){
                                console.log(resp)
                                resp = JSON.parse(resp);
                                data.docUrl=resp.dstos;
                                $.ajax({
                                    type: "POST",
                                    url: "../ajax/ProductosRegalos/registrarCompra.php",
                                    data: data,
                                    success: function (resp) {
                                        if (isJson(resp)){
                                            resp=JSON.parse(resp);
                                            if (resp.res){
                                                swal("Compra Guardada", "", "success");
                                            }else{
                                                swal("Error al Guardar la compra", "", "error");
                                            }
                                        }else{
                                            swal("Error en el servidor", "", "error");
                                        }

                                    }
                                });

                            }
                        });
                    }else{
                        $.ajax({
                            type: "POST",
                            url: "../ajax/ProductosRegalos/registrarCompra.php",
                            data: data,
                            success: function (resp) {
                                console.log(resp);
                                if (isJson(resp)){
                                    resp=JSON.parse(resp);
                                    if (resp.res){
                                        swal("Compra Guardada", "", "success")
                                            .then(function () {
                                                //location.href ="compras.php";
                                            });
                                    }else{
                                        swal("Error al Guardar la compra", "", "error");
                                    }
                                }else{
                                    swal("Error en el servidor", "", "error");
                                }
                            }
                        });
                    }






                },
                eliminarItem(index){
                    this.productos.splice(index, 1);
                },
                agregar(){
                    this.productos.push({
                        idProd:this.dataRegistro.preData.produ_id,
                        marca:this.dataRegistro.preData.mar_nombre,
                        producto:this.dataRegistro.preData.produ_nombre,
                        cantidad:this.dataRegistro.cantidad,
                        precio: this.dataRegistro.precio,
                        importe: this.dataRegistro.precio * this.dataRegistro.cantidad,
                    });
                    this.dataRegistro={
                        preData:{},
                        nombre:'',
                            cantidad:'',
                            precio:''
                    };
                },
                onlyNumber ($event) {
                    //console.log($event.keyCode); //keyCodes value
                    let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
                    if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                        $event.preventDefault();
                    }
                }
            },
            computed:{
                subTotal(){
                    var subtotal=0;
                    var monto=0;
                    for (var  i = 0; i< this.productos.length; i++){
                        monto += this.productos[i].importe;
                    }
                    subtotal=monto/1.18;
                    this.igv = monto - subtotal;
                    this.compra.monto = monto;
                    return subtotal;
                }
            }
        });

        $(document).ready(function () {
            AppProd.getMarcas()
           // $(".selectpicker").selectpicker('refresh');
            $("#input-producto-buscar").autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "../ajax/ProductosRegalos/search_produc.php",
                        data: { term: request.term},
                        success: function(data){
                            response(data);
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log(errorThrown)
                        },
                        dataType: 'json'
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    console.log(ui.item)
                    APP._data.dataRegistro.preData = ui.item;
                    APP._data.dataRegistro.nombre=ui.item.produ_nombre
                    APP._data.dataRegistro.cantidad=''
                    APP._data.dataRegistro.precio=''
                    event.preventDefault();

                }
            });
            $("#input-proveedor-buscar").autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "../ajax/Proveedores/buscarProveedorNacional.php",
                        data: { term: request.term},
                        success: function(data){
                            response(data);
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log(errorThrown)
                        },
                        dataType: 'json'
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    console.log(ui.item)
                    APP._data.compra.idProvee=ui.item.provee_id
                    APP._data.compra.nombreProvee=ui.item.provee_desc

                    event.preventDefault();

                }
            });



        });

    </script>


</body>

</html>
