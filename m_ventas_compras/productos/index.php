<?php
$indexRuta = 1;
require '../conexion/Conexion.php';
require '../model/TipoPago.php';
require_once '../model/model.php';
$tipoPago = new TipoPago();
$categorias = new Categoria('SELECT');
/*
$listaPa = $tipoPago->lista();
$listaTemTP = [];

foreach ($listaPa as $item) {
    $listaTemTP [] = $item;
}*/


$conexionp = (new Conexion())->getConexion();

$sql = "SELECT prod.*, cate.cat_nombre, mar.mar_nombre, model.mod_nombre, pais.pais_nombre
FROM sys_producto AS prod
INNER JOIN sys_com_categoria AS cate ON prod.cat_id = cate.cat_id
INNER JOIN sys_com_marca AS mar ON prod.mar_id = mar.mar_id 
INNER JOIN sys_com_modelo AS model ON prod.mod_id = model.mod_id
INNER JOIN sys_pais AS pais  ON prod.pais_id = pais.pais_id";
//$result_productos = $conexionp->query($sql);


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../aConfig/plugins/sweetalert2/sweetalert2.min.css"/>

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }


        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="../"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Productos</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <button onclick="MODALES. resetDataRegisterProducto(false)" type="button"
                                                data-toggle="modal" data-target="#modal_editar_productos"
                                                class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Producto
                                        </button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal" class="col-xs-12 col-sm-12 col-md-12 no-padding">

                                <table id="table-productos-lista"
                                       class="table table-striped table-bordered table-hover">
                                    <thead class="bg-head-table">
                                    <tr style="background-color: #007ac3; color: white">
                                        <th style="border-right-color: #007ac3" class="text-center">SKU</th>
                                        <th style="border-right-color: #007ac3" class="text-center">NOMBRE</th>

                                        <th style="border-right-color: #007ac3" class="text-center">CATEGORIA</th>
                                        <th style="border-right-color: #007ac3" class="text-center">MARCA</th>
                                        <th style="border-right-color: #007ac3" class="text-center">MODELO</th>
                                        <th style="border-right-color: #007ac3" class="text-center">PAIS</th>
                                        <th class="text-center">OPCION</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    </tbody>

                                </table>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div id="conten-modales">


                                    <div class="modal fade" id="modal_editar_productos" tabindex="-1" role="dialog"
                                         aria-hidden="true" style="z-index: 1600;">
                                        <div class="modal-dialog" role="document" style="width: 80%;">
                                            <div class="modal-content">
                                                <div class="modal-header no-border no-padding">
                                                    <div class="modal-header text-center color-modal-header">
                                                        <h3 class="modal-title">Producto</h3>
                                                    </div>
                                                </div>

                                                <div class="modal-body  no-border">
                                                    <form action="#">
                                                        <div class="container-fluid">
                                                            <div class="no-padding">
                                                                <div class="form-group col-xs-12 col-sm-4">
                                                                    <label>CATEGORIA:</label>
                                                                    <div class="input-group col-xs-12 no-padding">
                                                                        <select :disabled="!productos.view_std"
                                                                                @change="onChangeCategoriProduc($event)"
                                                                                v-model="productos.dataRegistro.idCategoria"
                                                                                id="select_modal_pro_categoria" required
                                                                                class="form-control show-tick no-padding n1"
                                                                                data-live-search="true"
                                                                                data-size="5">

                                                                            <?php
                                                                            $categoria = new Categoria('SELECT');
                                                                            $datos = $categoria->selectAll();
                                                                            foreach ($datos as $row) {
                                                                                if ($row->cat_id != 6) {
                                                                                    echo '<option value="' . $row->cat_id . '">' . $row->cat_nombre . '</option>';
                                                                                }

                                                                            }
                                                                            ?>
                                                                        </select>
                                                                        <!--<span class="input-group-btn">
                                                                            <button  type="button" class="btn btn-primary" data-toggle="modal"
                                                                                    data-target="#modal_marca">
                                                                                <i class="fa fa-plus"></i>
                                                                            </button>
                                                                        </span>-->
                                                                    </div>
                                                                </div>

                                                                <div id="box-productos-inputs">


                                                                    <div class="form-group col-xs-12 col-sm-4"
                                                                         :style="productos.moduladores.marcaSS?'':'display: none;'">
                                                                        <label>MARCA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <select :disabled="!productos.view_std"
                                                                                    @change="onChangeMarcaiProduc($event)"
                                                                                    v-model="productos.dataRegistro.idMarca"
                                                                                    id="select_modal_prod_marca"
                                                                                    required
                                                                                    class=" form-control  no-padding selectpicker"
                                                                                    data-live-search="true"
                                                                                    data-size="5">
                                                                                <option v-for="(item, indes ) in marcasProducto.lista"
                                                                                        v-bind:value="item.mar_id">
                                                                                    {{item.mar_nombre}}
                                                                                </option>
                                                                            </select>
                                                                            <span class="input-group-btn">
                                                                                <button :disabled="productos.dataRegistro.idCategoria==0"
                                                                                        id="producto_modal_btn_select_marca"
                                                                                        type="button"
                                                                                        class="btn btn-primary"
                                                                                        data-toggle="modal"
                                                                                        data-target="#modal_marca">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </button>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isNeumatico?'':'display: none;'"
                                                                         class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>MODELO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <select
                                                                                    @change="onChangeModeloProduc($event)"
                                                                                    v-model="productos.dataRegistro.idModelo"
                                                                                    :disabled="!productos.view_std"
                                                                                    id="select_modal_prod_modelo"
                                                                                    class="form-control no-padding selectpicker"
                                                                                    data-live-search="true"
                                                                                    data-size="5">
                                                                                <option v-for="(item, index) in modeloProducto.lista"
                                                                                        v-bind:value="item.mod_id">
                                                                                    {{item.mod_nombre}}
                                                                                </option>
                                                                            </select>
                                                                            <span class="input-group-btn">
                                                                                <button id="producto_modal_btn_select_modelo"
                                                                                        type="button"
                                                                                        class="btn btn-primary"
                                                                                        data-toggle="modal"
                                                                                        data-target="#modal_modelo">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </button>
                                                                            </span>
                                                                        </div>
                                                                        <!--<div class="input-group col-xs-12 no-padding">
                                                                            <select disabled id="select_modal_prod_modelo" required
                                                                                    class="selectpicker form-control show-tick no-padding" data-live-search="true"
                                                                                    data-size="5">
                                                                                <option value="0" selected>-Seleccione-</option>
                                                                            </select>
                                                                            <span class="input-group-btn">
                                                                                <button id="producto_modal_btn_select_modelo" disabled type="button" class="btn btn-primary" data-toggle="modal"
                                                                                        data-target="#modal_marca">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </button>
                                                                            </span>
                                                                        </div>-->
                                                                    </div>

                                                                    <div v-if="productos.moduladores.global"
                                                                         class="form-group  col-xs-12 col-sm-4">
                                                                        <label class="col-xs-12 no-padding">COD.
                                                                            SUNAT:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input :disabled="!productos.view_std"
                                                                                       v-model="productos.dataRegistro.CodSunat"
                                                                                       id="modal-producto-input-codigo-sunat"
                                                                                       class="form-control"
                                                                                       type="text"
                                                                                       required
                                                                                       placeholder="Click para gregar"
                                                                                       title="">
                                                                                <input v-model="productos.dataRegistro.idCodSunat"
                                                                                       id="modal-producto-input-codigo-sunat-id"
                                                                                       class="form-control no-display"
                                                                                       type="text">
                                                                                <!--<span class="input-group-btn">
                                                                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                                                                            data-target="#modal_linea">
                                                                                        <i class="fa fa-plus"></i></button>
                                                                                </span>-->
                                                                            </div>
                                                                            <!--<select id="select_codigo" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                                                                            /*                    $codSunat = new CodSunat('SELECT');
                                                                                                $datos = $codSunat->selectAll();
                                                                                                foreach ($datos as $row) {
                                                                                                    echo '<option value="' . $row->sunat_cod_id . '">' . $row->sunat_cod_codigo . '</option>';
                                                                                                }
                                                                                                */ ?>
                </select>-->
                                                                        </div>
                                                                    </div>
                                                                    <div v-if="productos.moduladores.global"
                                                                         class="form-group col-xs-12 col-sm-4">
                                                                        <label>SKU:</label>
                                                                        <div class="input-group col-xs-12 no-padding me-select">
                                                                            <div class="input-group col-xs-12 no-padding">
                                                                                <input  v-if="productos.view_std"
                                                                                       v-model="getSKU"
                                                                                        id="skutempoRe"
                                                                                       disabled class="form-control"
                                                                                       type="hidden" required
                                                                                       placeholder="Codigo de sku">
                                                                                <input  v-if="productos.view_std"
                                                                                        id="modal-producto-input-sku"
                                                                                        v-model="productos.dataRegistro.sku"
                                                                                        disabled class="form-control"
                                                                                        type="text" required
                                                                                        placeholder="Codigo de sku">
                                                                                <input v-if="!productos.view_std"
                                                                                       v-model="productos.dataRegistro.sku"
                                                                                       id="modal-producto-input-sku"
                                                                                       disabled class="form-control"
                                                                                       type="text" required
                                                                                       placeholder="Codigo de sku">
                                                                            </div>
                                                                            <span class="input-group-btn">
                                            <button id="modal-producto-btn-view-sku" type="button"
                                                    class="btn btn-primary" data-toggle="modal"
                                                    data-target="#modal_sku">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.global2?'':'display: none;'"
                                                                         class="form-group col-xs-12 col-sm-4">
                                                                        <label>U. DE MEDIDA :</label>
                                                                        <div class="input-group col-xs-12 no-padding me-select">
                                                                            <select :disabled="!productos.view_std"
                                                                                    v-model="productos.dataRegistro.idUndMedida"
                                                                                    id="select_modal_medida" required
                                                                                    class="form-control no-padding"
                                                                                    data-size="5">
                                                                                <?php
                                                                                $unidad = new Unidad('SELECT');
                                                                                $datos = $unidad->selectAll();
                                                                                foreach ($datos as $row) {
                                                                                    if ($row->unidad_nombre == 'UND') {
                                                                                        echo '<option  v-if="!productos.moduladores.isNeumatico"  value="' . $row->unidad_id . '">' . $row->unidad_nombre . '</option>';
                                                                                    } elseif($row->unidad_id>=6&&$row->unidad_id<=9) {
                                                                                        echo '<option  v-if="productos.moduladores.isNeumatico"  value="' . $row->unidad_id . '">' . $row->unidad_nombre . '</option>';
                                                                                    }

                                                                                }
                                                                                ?>
                                                                            </select>
                                                                            <!--<span class="input-group-btn">
                                                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                                                        data-target="#modal_categoria">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </button>
                                                                            </span>-->
                                                                        </div>
                                                                    </div>

                                                                    <div v-if="productos.moduladores.isProtectores || productos.moduladores.isAccesorios"
                                                                         class="form-group  col-xs-12 col-sm-4">
                                                                        <label>DESCRIPCCION:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.descripcion"
                                                                                   id="modal-producto-input-desc"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   placeholder="Descripccion">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.global2?'':'display: none;'"
                                                                         class="form-group col-xs-12 col-sm-4">
                                                                        <label>NOMBRE PRODUCTO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input disabled
                                                                                   id="modal-producto-input-nombre"
                                                                                   v-model="productos.dataRegistro.nombre"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isAccesorios?'':'display: none;'"
                                                                         class="form-group col-xs-12 col-sm-4">
                                                                        <label>P/N:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.pn"
                                                                                   id="modal-producto-input-pn"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="(productos.moduladores.isNeumatico || productos.moduladores.isCamara||productos.moduladores.isAros)?'':'display: none;'"
                                                                         class="form-group col-xs-12 col-sm-4 box-opc">
                                                                        <label class="col-xs-12 no-padding">TIPO:</label>
                                                                        <!--OPC-->
                                                                        <div class="input-group col-xs-12 no-padding">

                                                                            <select :disabled="!productos.view_std"
                                                                                    @change="onChangeTipoProduc($event)"
                                                                                    v-model="productos.dataRegistro.idTipo"
                                                                                    id="select_opc" required
                                                                                    class=" form-control show-tick no-padding "
                                                                                    data-live-search="true"
                                                                                    data-size="5">
                                                                                <?php
                                                                                $tipoProducto = new TipoProducto('SELECT');
                                                                                $datos = $tipoProducto->selectAll();
                                                                                foreach ($datos as $row) {
                                                                                    if ($row->tipro_id <= 2) {
                                                                                        echo '<option v-if="productos.moduladores.isNeumatico||productos.moduladores.isCamara" value="' . $row->tipro_id . '">' . $row->tipro_nombre . '</option>';
                                                                                    } elseif ($row->tipro_id <= 6) {
                                                                                        echo '<option v-if="productos.moduladores.isAros" value="' . $row->tipro_id . '">' . $row->tipro_nombre . '</option>';
                                                                                    }

                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div :style="productos.moduladores.isNeumatico?'':'display: none;'"
                                                                         id="box-nomenclatura"
                                                                         class="form-group col-xs-12 col-sm-4">
                                                                        <label>NOMENGLATURA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <select :disabled="!productos.view_std"
                                                                                    @change="onChangeNomengProduc($event)"
                                                                                    v-model="productos.dataRegistro.idnomenglatura"
                                                                                    id="select_nomenglatura" required
                                                                                    class=" form-control show-tick no-padding"
                                                                                    data-live-search="true"
                                                                                    data-size="5">
                                                                                <?php
                                                                                $nomenglatura = new Nomenglatura('SELECT');
                                                                                $datos = $nomenglatura->selectAll();
                                                                                foreach ($datos as $row) {
                                                                                    if ($row->estado!=0){
                                                                                        echo '<option value="' . $row->nom_id . '">' . $row->nom_nombre . '</option>';
                                                                                    }

                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.global?'':'display: none;'"
                                                                         class="form-group col-xs-12 col-sm-4">
                                                                        <label>PAIS:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <select :disabled="!productos.view_std"
                                                                                    @change="onChangePaisProduc($event)"
                                                                                    v-model="productos.dataRegistro.isPais"
                                                                                    id="select_prod_pais" required
                                                                                    class="form-control show-tick no-padding selectpicker"
                                                                                    data-live-search="true"
                                                                                    data-size="5">

                                                                                <?php
                                                                                $pais = new Pais('SELECT');
                                                                                $datos = $pais->selectAll();
                                                                                foreach ($datos as $row) {
                                                                                    if ($row->estado != '0') {
                                                                                        echo '<option value="' . $row->pais_id . '">' . $row->pais_nombre . '</option>';
                                                                                    }

                                                                                }
                                                                                ?>
                                                                                ?>
                                                                            </select>
                                                                            <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#modal_pais">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </span>
                                                                        </div>
                                                                    </div>

                                                                    <!--CAMARA-->
                                                                    <div :style="productos.moduladores.isCamara?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 CAMARA">
                                                                        <label>MEDIDA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.medidaCamara"
                                                                                   id="modal-producto-input-cam-medida"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   placeholder="Medida de camara">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isCamara?'':'display: none;'"
                                                                         class="form-group col-xs-12 col-sm-4 CAMARA">
                                                                        <label>ARO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.camAro"
                                                                                   id="modal-producto-input-cam-aro"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   placeholder="Aro de camara">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isCamara?'':'display: none;'"
                                                                         class="form-group col-xs-12 col-sm-4 CAMARA">
                                                                        <label> VALVULA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-model="productos.dataRegistro.valvula"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   id="modal-producto-input-cam-valvula"
                                                                                   class="form-control" type="text"
                                                                                   autocomplete="off"
                                                                                   required
                                                                                   placeholder="Tipo de valvula">
                                                                        </div>
                                                                    </div>
                                                                    <!--CAMARA-->

                                                                    <!--AROS-->
                                                                    <div :style="productos.moduladores.isAros?'':'display: none;'"
                                                                         class="form-group col-xs-12 col-sm-4 ARO">
                                                                        <label>MODELO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.modeloAro"
                                                                                   id="modal-producto-input-aro-modelo"
                                                                                   class="form-control" type="text"
                                                                                   autocomplete="off"
                                                                                   required
                                                                                   placeholder="Modelo aro">
                                                                        </div>
                                                                    </div>

                                                                    <div :style="productos.moduladores.isAros?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 ARO">
                                                                        <label>MEDIDA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.medidaAro"
                                                                                   id="modal-producto-input-aro-medida"
                                                                                   class="form-control" type="text"
                                                                                   autocomplete="off"
                                                                                   required
                                                                                   placeholder="medida aro">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isAros?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 ARO">
                                                                        <label>ESPESOR MM:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-model="productos.dataRegistro.espesormm"
                                                                                   id="modal-producto-input-aro-espesor"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   placeholder="Espesor aro">
                                                                        </div>
                                                                    </div>

                                                                    <div :style="productos.moduladores.isAros?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 ARO">
                                                                        <label># HUECOS:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.huecos"
                                                                                   id="modal-producto-input-aro-num-huecos"
                                                                                   class="form-control" type="text"
                                                                                   autocomplete="off"
                                                                                   required
                                                                                   placeholder="# de huecos">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isAros?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 ARO">
                                                                        <label>ESPESOR HUECO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.espesorhueco"
                                                                                   id="modal-producto-input-aro-espesor-hueco"
                                                                                   class="form-control" type="text"
                                                                                   autocomplete="off"
                                                                                   required
                                                                                   placeholder="Espesor de hueco">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isAros?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 ARO">
                                                                        <label>C.B.D:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.CBD"
                                                                                   id="modal-producto-input-aro-cbd"
                                                                                   class="form-control" type="text"
                                                                                   autocomplete="off"
                                                                                   required
                                                                                   placeholder="C.B.D aro">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isAros?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 ARO">
                                                                        <label>P.C.D:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.PCD"
                                                                                   id="modal-producto-input-aro-pcd"
                                                                                   class="form-control" type="text"
                                                                                   autocomplete="off"
                                                                                   required
                                                                                   placeholder="P.C.D aro">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isAros?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 ARO">
                                                                        <label>OFF SET:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.offSet"
                                                                                   id="modal-producto-input-aro-offset"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   placeholder="Off Set aro">
                                                                        </div>
                                                                    </div>

                                                                    <!--AROS-->

                                                                    <!--NEUMATICO-->
                                                                    <div :style="productos.moduladores.isNeumatico?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>ANCHO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-model="productos.dataRegistro.ancho"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   id="modal-producto-input-neu-ancho"
                                                                                   class="form-control" type="text"
                                                                                   autocomplete="off"
                                                                                   required
                                                                                   placeholder="Ancho neumatico">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isNeumatico?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>SERIE:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   v-model="productos.dataRegistro.serie"
                                                                                   id="modal-producto-input-neu-serie"
                                                                                   class="form-control" type="text"
                                                                                   autocomplete="off"
                                                                                   required
                                                                                   placeholder="Serie Neumatico">
                                                                        </div>
                                                                    </div>


                                                                    <!--NEUMATICO-->
                                                                    <!--<div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>ANCHO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-ancho" class="form-control" type="text" required
                                                                                   placeholder="Ancho neumatico">
                                                                        </div>
                                                                    </div>-->
                                                                    <!--<div hidden class="form-group col-xs-12 col-sm-2 NEUMATICO">
                                                                        <label>SERIE:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input class="form-control" type="text" required
                                                                                   placeholder="Serie Neumatico">
                                                                        </div>
                                                                    </div>-->
                                                                    <div :style="productos.moduladores.isNeumatico?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>ARO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-model="productos.dataRegistro.aro"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   id="modal-producto-input-neu-aro"
                                                                                   class="form-control" type="text"
                                                                                   autocomplete="off"
                                                                                   required
                                                                                   placeholder="Aro de Neumatico">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isNeumatico?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>PLIEGUES:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-model="productos.dataRegistro.pliegles"
                                                                                   id="modal-producto-input-neu-pliege"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   placeholder="Numero de pliegues">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-6">
                                                                        <label>SET:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   id="modal-producto-input-neu-set"
                                                                                   class="form-control" type="text"
                                                                                   v-on:keyup="eventoNombreProduct()"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.isNeumatico?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                                                        <label>USO:</label>
                                                                        <div id="select_uso"
                                                                             class="input-group col-xs-12 no-padding">
                                                                            <input v-if="!productos.view_std" class="form-control" disabled v-model="productos.dataRegistro.Uso">
                                                                            <select v-if="productos.view_std"
                                                                                    v-model="productos.dataRegistro.Uso"
                                                                                    id="select_modal_uso"
                                                                                    class="form-control show-tick no-padding"
                                                                                    data-live-search="true"
                                                                                    data-size="5">

                                                                                <option value="PPE-Pasajeros, uso permanente">PPE-Pasajeros, uso permanente</option>
                                                                                <option value="PTE-Pasajeros, uso temporal">PTE-Pasajeros, uso temporal</option>
                                                                                <option value="CLT-Comerciales, para camioneta de carga, microbuses o camiones ligeros">CLT-Comerciales, para camioneta de carga, microbuses o camiones ligeros</option>
                                                                                <option value="CTR-Comerciales, para camión y/o ómnibus">CTR-Comerciales, para camión y/o ómnibus</option>
                                                                                <option value="CML-Comerciales, para uso minero y forestal">CML-Comerciales, para uso minero y forestal</option>
                                                                                <option value="CMH-Comerciales, para casas rodantes">CMH-Comerciales, para casas rodantes</option>
                                                                                <option value="CST-Comerciales, remolcadores en carretera">CST-Comerciales, remolcadores en carretera</option>
                                                                                <option value="ALN-Agrícolas">ALN-Agrícolas</option>
                                                                                <option value="OTG-Maquinaria, tractores niveladores">OTG-Maquinaria, tractores niveladores</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div :style="productos.moduladores.global?'':'display: none;'"
                                                                         class="form-group  col-xs-12 col-sm-4 box-siempre-show">
                                                                        <label>PARTIDA ARANCELARIA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   v-model="productos.dataRegistro.partiAran"
                                                                                   id="modal-producto-input-neu-parti"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   placeholder="Partida Arancelaria">
                                                                        </div>
                                                                    </div>

                                                                    <div  class="form-group col-xs-12 col-sm-4 " :style="productos.moduladores.isNeumatico?'':'display: none;'">
                                                                        <label>MATERIAL:</label>
                                                                        <div class="input-group col-xs-12 no-padding">

                                                                            <select id="modal-producto-input-neu-mate" class="form-control show-tick no-padding"
                                                                                    data-live-search="true"
                                                                                    :disabled="!productos.view_std"
                                                                                    v-model="productos.dataRegistro.material"
                                                                                    data-size="5">
                                                                                <option value="NYLON">NYLON</option>
                                                                                <option value="ACERO">ACERO</option>
                                                                                <option value="POLIÉSTER">POLIÉSTER</option>
                                                                                <option value="RAYON">RAYON</option>
                                                                                <option value="POLIAMIDA">POLIAMIDA</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div  class="form-group col-xs-12 col-sm-4 " :style="productos.moduladores.isNeumatico?'':'display: none;'">
                                                                        <label>ANCHO ADUANA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   id="modal-producto-input-neu-ancho-adua"
                                                                                   class="form-control"
                                                                                   type="text"
                                                                                   v-model="productos.dataRegistro.ancho_aduana"
                                                                                   autocomplete="off"
                                                                                   required
                                                                                   placeholder="Ancho aduana">
                                                                        </div>
                                                                    </div>
                                                                    <div  class="form-group col-xs-12 col-sm-4 NEUMATICO" :style="productos.moduladores.isNeumatico?'':'display: none;'">
                                                                        <label>SERIE ADUANA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std" id="modal-producto-input-neu-serie-adua" autocomplete="off" class="form-control" type="text" required
                                                                                   v-model="productos.dataRegistro.serie_aduana"
                                                                                   placeholder="Serie aduana">
                                                                        </div>
                                                                    </div>
                                                                    <div  class="form-group col-xs-12 col-sm-4 NEUMATICO" :style="productos.moduladores.isNeumatico?'':'display: none;'">
                                                                        <label>TIPO CONSTRUCCION:</label>
                                                                        <div class="input-group col-xs-12 no-padding">

                                                                            <select id="modal-producto-input-neu-serie-tipo-cons" class=" form-control show-tick no-padding"
                                                                                    data-live-search="true"
                                                                                    v-model="productos.dataRegistro.tipo_construccion"
                                                                                    :disabled="!productos.view_std"
                                                                                    data-size="5">
                                                                                <option value="CTT-CONVENCIONAL CON CAMARA">CTT-CONVENCIONAL CON CAMARA</option>
                                                                                <option value="CTL-CONVENCIONAL SIN CAMARA">CTL-CONVENCIONAL SIN CAMARA</option>
                                                                                <option value="RTT-RADIAL CON CAMARA">RTT-RADIAL CON CAMARA</option>
                                                                                <option value="RTL-RADIAL SIN CAMARA">RTL-RADIAL SIN CAMARA</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4 " >
                                                                        <label>CARGA:</label>
                                                                        <div class="input-group col-xs-12 no-padding ">
                                                                            <input :disabled="!productos.view_std"
                                                                                   id="modal-producto-input-neu-carga"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   :disabled="!productos.view_std"
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>PISA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   id="modal-producto-input-neu-pisa"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>EXTERNO:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   id="modal-producto-input-neu-externo"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                    <div  class="form-group col-xs-12 col-sm-4" :style="productos.moduladores.isNeumatico?'':'display: none;'">
                                                                        <label>INDICE DE CARGA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input id="modal-producto-input-neu-indice-carga" autocomplete="off" class="form-control" type="text" required
                                                                                   v-model="productos.dataRegistro.indice_carga"
                                                                                   :disabled="!productos.view_std"
                                                                                   placeholder="Indice de carga">
                                                                        </div>
                                                                    </div>
                                                                    <div  class="form-group col-xs-12 col-sm-4" :style="productos.moduladores.isNeumatico?'':'display: none;'">
                                                                        <label>VELOCIDAD:</label>
                                                                        <div class="input-group col-xs-12 no-padding">

                                                                            <select id="modal-producto-input-neu-veloci" class="form-control selectpicker no-padding"
                                                                                    data-live-search="true"
                                                                                    :disabled="!productos.view_std"
                                                                                    v-model="productos.dataRegistro.velocidad"
                                                                                    data-size="5">
                                                                                <option value="E-70KM/H">E-70KM/H</option>
                                                                                <option value="F-80KM/H">F-80KM/H</option>
                                                                                <option value="G-90KM/H">G-90KM/H</option>
                                                                                <option value="J-100KM/H">J-100KM/H</option>
                                                                                <option value="K-110KM/H">K-110KM/H</option>
                                                                                <option value="L-120KM/H">L-120KM/H</option>
                                                                                <option value="M-130KM/H">M-130KM/H</option>
                                                                                <option value="N-140KM/H">N-140KM/H</option>
                                                                                <option value="P-150KM/H">P-150KM/H</option>
                                                                                <option value="Q-160KM/H">Q-160KM/H</option>
                                                                                <option value="R-170KM/H">R-170KM/H</option>
                                                                                <option value="S-180KM/H">S-180KM/H</option>
                                                                                <option value="T-190KM/H">T-190KM/H</option>
                                                                                <option value="U-200KM/H">U-200KM/H</option>
                                                                                <option value="H-210KM/H">H-210KM/H</option>
                                                                                <option value="V-240KM/H">V-240KM/H</option>
                                                                                <option value="W-270KM/H">W-270KM/H</option>
                                                                                <option value="Y-300KM/H">Y-300KM/H</option>
                                                                                <option value="Z-MAYOR A 300KM/H">Z-MAYOR A 300KM/H</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div  class="form-group col-xs-12 col-sm-4" :style="productos.moduladores.isNeumatico?'':'display: none;'">
                                                                        <label>CONSTANCIA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   id="modal-producto-input-neu-consta"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   v-model="productos.dataRegistro.constancia"
                                                                                   placeholder="Constancia de cumplimiento">
                                                                        </div>
                                                                    </div>
                                                                    <div  class="form-group col-xs-12 col-sm-4" :style="productos.moduladores.isNeumatico?'':'display: none;'">
                                                                        <label>ITEM:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   id="modal-producto-input-neu-item"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   autocomplete="off"
                                                                                   v-model="productos.dataRegistro.item_const"
                                                                                   placeholder="Item de la constancia">
                                                                        </div>
                                                                    </div>
                                                                    <div  class="form-group col-xs-12 col-sm-4" :style="productos.moduladores.isNeumatico?'':'display: none;'">
                                                                        <label>VIGENCIA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   id="modal-producto-input-neu-vigencia"
                                                                                   class="form-control" type="date"
                                                                                   autocomplete="off"
                                                                                   v-model="productos.dataRegistro.vigencia"
                                                                                   required >
                                                                        </div>
                                                                    </div>
                                                                    <div  class="form-group col-xs-12 col-sm-4" :style="productos.moduladores.isNeumatico?'':'display: none;'">
                                                                        <label>CONFORMIDAD:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input autocomplete="off" :disabled="!productos.view_std"
                                                                                   id="modal-producto-input-neu-confor"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   v-model="productos.dataRegistro.conformidad"
                                                                                   placeholder="Declaracion de conformidad">
                                                                        </div>
                                                                    </div>
                                                                    <div  class="form-group  col-xs-6 col-sm-2 NEUMATICO"  :style="productos.moduladores.isNeumatico?'':'display: none;'">
                                                                        <label>SUCE:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input autocomplete="off" v-model="productos.dataRegistro.suce" id="modal-producto-input-neu-suce" autocomplete="off" class="form-control" type="text" required
                                                                                   :disabled="!productos.view_std"
                                                                                   placeholder="Ancho neumatico">
                                                                        </div>
                                                                    </div>
                                                                    <div  :class="productos.moduladores.isNeumatico?'form-group  col-xs-6 col-sm-2':'form-group  col-xs-12 col-sm-4'"  :style="productos.moduladores.global?'':'display: none;'">
                                                                        <label>PESO (KGM):</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input autocomplete="off" v-model="productos.dataRegistro.peso" id="modal-producto-input-peso" autocomplete="off" class="form-control" type="text" required

                                                                                   @keypress="onlyNumber"
                                                                                   placeholder="Ancho neumatico">
                                                                        </div>
                                                                    </div>
                                                                    <!--NEUMATICO-->

                                                                    <div hidden class="form-group col-xs-12 col-sm-4">
                                                                        <label>PRO MEDIDA:</label>
                                                                        <div class="input-group col-xs-12 no-padding">
                                                                            <input :disabled="!productos.view_std"
                                                                                   id="modal-producto-input-produ-medida"
                                                                                   class="form-control" type="text"
                                                                                   required
                                                                                   placeholder="Numero de Parte">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="container-fluid">
                                                            <hr class="line-frame-modal">
                                                        </div>
                                                        <div class="container-fluid text-right">

                                                            <!-- <a type="submit" id="modal-buscar-empresa-btn-guardar" class="btn btn-primary">
                                                                 Guardar
                                                             </a>
                                                             <button type="button" id="modal-buscar-empresa-btn-limpiar" class="btn btn-default">
                                                                 Limpiar
                                                             </button>-->
                                                            <button   type="button" onclick="guardarProductoModalSoloPeso()"
                                                                    class="btn btn-primary">
                                                                Guardars
                                                            </button>
                                                            <button v-if="productos.view_std" type="button" onclick="guardarProductoModal()"
                                                                    class="btn btn-primary">
                                                                Guardar
                                                            </button>
                                                            <button type="button" class="btn btn-success"
                                                                    data-dismiss="modal">
                                                                Cerrar
                                                            </button>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>

                            <div class="modal fade" id="modal_marca" tabindex="-1" role="dialog" aria-hidden="true"
                                 style="z-index: 1800; display: none;">
                                <div class="modal-dialog modal-xs " role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-border no-padding">
                                            <div class="modal-header text-center color-modal-header">
                                                <h3 class="modal-title">Agregar Nueva Marca</h3>
                                            </div>
                                        </div>
                                        <div class="modal-body  no-border">
                                            <div class="container-fluid">

                                                <div class="form-group col-xs-12 no-padding">
                                                    <label class="col-xs-12 no-padding">Nombre:</label>
                                                    <input id="modal-marca-input-nombre" class="form-control"
                                                           type="text" placeholder="ejem. CHINA TYRE" required="">
                                                </div>

                                                <div class="form-group col-xs-12 no-padding text-center"
                                                     style="max-height: 275px;padding: 15px; border: 1px solid rgba(10,107,206,0.6);border-radius: 1px;">
                                                    <div class="col-xs-12" style="padding: 0;">

                                                        <div class="md-12" width="100%"
                                                             style="max-height: 167px; display: flex; padding: 15px;">
                                                            <img src="../imagenes/logo/img-icon.svg"
                                                                 id="img-file-preview-zone" width="auto"
                                                                 style="margin: auto" height="170">
                                                        </div>

                                                        <div class="col-xs-12 text-center" style="margin-top: 25px;">
                                                            <div class="form-group col-xs-12 no-margin no-padding">
                                                                <h5 id="label-file-image" class="col-xs-12">
                                                                    ( Ninguna imagen seleccionada.... )
                                                                </h5>
                                                            </div>
                                                            <input type="file" accept=".png" id="input-file-image"
                                                                   name="input-file-image" class="no-display">
                                                            <div class="form-group col-xs-6">
                                                                <label type="button" for="input-file-image"
                                                                       class="btn btn-sm btn-primary col-xs-12"
                                                                       title="Subir logotipo de importador">
                                                                    <i class="fa fa-file"></i> Subir Logo
                                                                </label>
                                                            </div>
                                                            <div class="form-group col-xs-6">
                                                                <label id="modal-marca-btn-eliminar-logo" type="button"
                                                                       class="btn btn-sm btn-danger col-xs-12"
                                                                       title="Subir logotipo de importador">
                                                                    <i class="fa fa-remove"></i> Eliminar
                                                                </label>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="container-fluid">
                                                <hr class="line-frame-modal">
                                            </div>
                                            <div class="container-fluid text-right">

                                                <button  type="button" onclick="MODALES.guardarMarca()"
                                                        id="modal-marca-btn-guardar" class="btn btn-primary">
                                                    Guardar
                                                </button>

                                                <button onclick="limpiarRegMar()" type="button"
                                                        id="modal-marca-btn-cerrar" class="btn btn-success"
                                                        data-dismiss="modal">
                                                    Cerrar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>


    <div class="modal fade" id="modal_buscar_codigo_sunat" tabindex="-1" role="dialog" aria-hidden="true"
         style="z-index: 1800;">
        <div class="modal-dialog modal-xs " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Buscar codigo sunat</h3>
                    </div>
                </div>
                <style>
                    .bg-head-table tr th {
                        padding: 0;
                    }

                    div.dataTables_wrapper div.dataTables_info {
                        display: none;
                    }

                    div.dataTables_wrapper div.dataTables_length {
                        display: none;
                    }

                    /*


                                    table.dataTable thead > tr > th.sorting {
                                        padding-right: 13px;
                                    }*/
                    #table-codigo-sunat_filter {
                        display: none;
                    }
                </style>
                <div class="modal-body  no-border">
                    <div class="container-fluid">
                        <div class="col-xs-12 no-padding">
                            <div id="" class="dataTables_filter">
                                <label>Buscar:
                                    <input class="form-control input-sm input-search-cods">
                                </label>
                            </div>
                        </div>


                        <div class="form-group col-xs-12 no-padding" style="max-height: 320px;overflow-x:hidden;">
                            <table id="table-codigo-sunat" class="table table-striped table-bordered table-hover"
                                   style="width: 100%;">
                                <thead class="bg-head-table">
                                <tr>
                                    <th class="text-center"></th>
                                    <th class="text-left">CODIGO</th>
                                    <th class="text-left">DESCRIPCCION</th>
                                    <th class="text-left">OPCION</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $codSunat = new CodSunat('SELECT');
                                $result = $codSunat->selectAll();
                                $index = 0;
                                foreach ($result as $item) {
                                    ?>
                                    <tr>
                                        <td style="color: white" class="text-center"><?php echo ++$index; ?></td>
                                        <td class="text-left">
                                            <label><?php echo $item->sunat_cod_codigo; ?></label>
                                        </td>
                                        <td class="text-left">
                                            <div id="desc"><?php echo $item->sunat_cod_desc; ?></div>
                                        </td>
                                        <td class="text-center">
                                            <button onclick="MODALES.setCodigoSunatRegister(<?php echo $item->sunat_cod_id; ?> , '<?php echo $item->sunat_cod_codigo; ?>')"
                                                    class="btn btn-sm btn-success fa fa-check btn-option"
                                                    title="Anadir item" data-dismiss="modal"></button>
                                        </td>

                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>

                            </table>
                        </div>


                        <!--<div class="form-group  col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Descripccion:</label>
                            <textarea id="modal-buscar-codigo-sunat-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                        </div>-->
                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <!-- <a type="submit" id="modal-buscar-codigo-sunat-btn-guardar" class="btn btn-primary">
                             Guardar
                         </a>
                         <button type="button" id="modal-buscar-codigo-sunat-btn-limpiar" class="btn btn-default">
                             Limpiar
                         </button>-->
                        <!--<button type="button" id="modal-buscar-codigo-sunat-btn-agregar" class="btn btn-primary"
                                data-toggle="modal" data-target="#modal_aduanas">
                            Agregar
                        </button>-->
                        <button type="button" id="modal-buscar-codigo-sunat-btn-cerrar" class="btn btn-success"
                                data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_modelo" tabindex="-1" role="dialog" aria-hidden="true"
         style="z-index: 1800; display: none;">
        <div class="modal-dialog modal-xs " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Agregar Nueva Modelo</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <div class="container-fluid">

                        <div class="form-group col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Nombre:</label>
                            <input id="modal-modelo-input-nombre" class="form-control" type="text"
                                   placeholder="ejem. CHINA TYRE" required="">
                        </div>
                        <div class="form-group col-xs-12 no-padding text-center"
                             style="max-height: 275px;padding: 15px; border: 1px solid rgba(10,107,206,0.6);border-radius: 1px;">
                            <div class="col-xs-12" style="padding: 0;">

                                <div class="md-12" width="100%"
                                     style="max-height: 167px; display: flex; padding: 15px;">
                                    <img src="" id="img-file-preview-zone-modelo"
                                         width="auto" style="margin: auto" height="170">
                                </div>

                                <div class="col-xs-12 text-center" style="margin-top: 25px;">
                                    <div class="form-group col-xs-12 no-margin no-padding">
                                        <h5 id="label-file-image-modelo" class="col-xs-12">

                                        </h5>
                                    </div>
                                    <input type="file" accept=".png" id="input-file-image-modelo"
                                           name="input-file-image-modelo" class="no-display">
                                    <div class="form-group col-xs-6">
                                        <label type="button" for="input-file-image-modelo"
                                               class="btn btn-sm btn-primary col-xs-12"
                                               title="Subir logotipo de importador">
                                            <i class="fa fa-file"></i> Subir Logo
                                        </label>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <label type="button" class="btn btn-sm btn-danger col-xs-12"
                                               title="Subir logotipo de importador">
                                            <i class="fa fa-remove"></i> Eliminar
                                        </label>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <button type="button" onclick="MODALES.guardarModelo()" class="btn btn-primary">
                            Guardar
                        </button>

                        <button type="button" class="btn btn-success" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_pais" tabindex="-1" role="dialog" aria-hidden="true"
         style="z-index: 1800; display: none;">
        <div class="modal-dialog modal-xs " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Agregar Nuevo Pais</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form action="#">
                        <div class="container-fluid">
                            <div class="form-group col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Nombre:</label>
                                <input id="modal-pais-input-nombre" class="form-control" type="text" placeholder=""
                                       required="">
                            </div>
                            <!--<div class="form-group  col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Descripccion:</label>
                                <textarea id="modal-pais-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                            </div>-->
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">

                            <button type="button" onclick=" addPais()" class="btn btn-primary">
                                Guardar
                            </button>

                            <button type="button" class="btn btn-success" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <style>
        #img-file-preview-zone {
            -webkit-box-shadow: 1px 1px 4px 1px rgba(75, 87, 209, 1);
            -moz-box-shadow: 1px 1px 4px 1px rgba(75, 87, 209, 1);
            box-shadow: 1px 1px 4px 1px rgba(75, 87, 209, 1);
            border-radius: 3px;
        }
    </style>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <!--script  type="module" src="../aConfig/scripts/sku.js"></script-->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        $('#select_nomenglatura').change(function () {
            genNombreProducto();
        });
        $('#select_modal_prod_modelo').change(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-pliege").keyup(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-serie").keyup(function () {
            genNombreProducto();
        });

        function genNombreProducto2() {
            const cate = $("#select_modal_pro_categoria option:selected").text();
            var nombrePr = cate;
            if (cate == "NEUMATICOS") {
                const nomen = $("#select_nomenglatura option:selected").text();
                const tipo = $("#select_opc option:selected").text();
                const ancho = $("#modal-producto-input-neu-ancho").val();
                nombrePr += " " + ancho;
                if (nomen == "MILIMETRICA") {
                    const serie = $("#modal-producto-input-neu-serie").val();
                    nombrePr += "/" + serie;
                }
                if (tipo == "RADIAL") {
                    nombrePr += "R";
                }
                const aroNeu = $("#modal-producto-input-neu-aro").val();
                const plieges = $("#modal-producto-input-neu-pliege").val();
                const marca = $("#select_modal_prod_marca option:selected").text();
                const modelo = $("#select_modal_prod_modelo option:selected").text();
                const pais = $("#select_prod_pais option:selected").text();
                nombrePr += "-" + aroNeu + " " + plieges + "PR " + marca + " " + modelo + " " + pais;

            } else if (cate == "CAMARAS") {
                /*  const  nomen = $("#select_nomenglatura option:selected").text();
                  if (nomen=="MILIMETRICA"){
                      const serie= $("#modal-producto-input-neu-serie").val();
                      nombrePr+="/"+serie;
                  }
                  if (tipo=="RADIAL"){
                      nombrePr+="R";
                  }*/
            }
            $("#modal-producto-input-nombre").val(nombrePr);
        }
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function removeItemFromArr(arr, index) {

            arr.splice(index, 1);
        }

        function addPais() {
            const pais = $("#modal-pais-input-nombre").val();

            $.ajax({
                type: "POST",
                url: "../ajax/Pais/add_pais.php",
                data: {pais},
                success: function (data) {

                    console.log(data);
                    if (isJson(data)) {
                        const jso = JSON.parse(data);
                        if (jso.res) {
                            $("#select_prod_pais").append('<option value="' + jso.data.id + '">' + jso.data.nombre + '</option>');
                            //
                            // $('#producto-select-presentacion2').selectpicker('refresh');
                            $("#modal-pais-input-nombre").val('');
                            $("#modal_pais").modal('hide');
                            setTimeout(function () {
                                $(".selectpicker").selectpicker('refresh');
                            },100)

                            $.toast({
                                heading: 'EXITOSO',
                                text: "Se agrego",
                                icon: 'success',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        } else {
                            console.log(data);
                            $.toast({
                                heading: 'ERROR',
                                text: "Error No se pudo agregar",
                                icon: 'error',
                                position: 'top-center',
                                hideAfter: '2500',
                            });
                        }
                    } else {
                        $.toast({
                            heading: 'ERROR',
                            text: "Error",
                            icon: 'error',
                            position: 'top-center',
                            hideAfter: '2500',
                        });
                        console.log(data);
                    }
                }
            });

        }

        $(document).ready(function () {
            $('#table-codigo-sunat').DataTable({
                /*scrollY: false,*/
                /*scrollX: true,*/
                paging: false,
                /* lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],*/
                language: {
                    url: '../assets/Spanish.json'
                }
            });
        });
    </script>


</body>

<script src="../aConfig/scripts/modal_controller.js?red="></script>

<script type="text/javascript">
    var idproducto=0;
    var idproductoDeta=0;
    var tabla_producto_alma;

    MODALES._data.productos.url_data = '../ajax/Producto/get_all_data2.php';
    //MODALES._data.productos.iniciarDatos= true;

    $(document).ready(function () {

        tabla_producto_alma = $("#table-productos-lista").DataTable({
            "processing": true,
            "serverSide": true,
            language: {
                url: '../assets/Spanish.json'
            },
            "sAjaxSource": "../ajax/ServerSide/serversideProductos.php",
            "dom": 'Bfrtip',
            "order": [[ 2, "asc" ]],
            columnDefs: [
                {
                    "targets": 0,
                    "data": "produ_id",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[0]+'</span>';

                    }
                },
                {
                    "targets": 1,
                    "data": "produ_id",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[1]+'</span>';

                    }
                },
                {
                    "targets": 2,
                    "data": "produ_id",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[2]+'</span>';

                    }
                },
                {
                    "targets": 3,
                    "data": "produ_id",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[3]+'</span>';

                    }
                },
                {
                    "targets": 4,
                    "data": "produ_id",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[4]+'</span>';

                    }
                },
                {
                    "targets": 5,
                    "data": "produ_id",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[5]+'</span>';

                    }
                },
                {
                    "targets": 6,
                    "data": "produ_id",
                    "render": function (data, type, row, meta) {
                        //console.log(row[6])
                        const datArr = row[6].split("-");
                        if (datArr[1] == "m") {
                            return '<button style="display: block;margin: auto"  class="btn btn-sm btn-warning fa fa-edit btn-selector-cliente"  onclick="MODALES.getDataDBProducto(' + datArr[0] + ',true);MODALES.reLoadSkau()" data-toggle="modal" data-target="#modal_editar_productos"></button>';
                        } else {
                            return ' <button  style="display: block;margin: auto"  class="btn btn-sm btn-info fa fa-eye btn-selector-cliente"  onclick="MODALES.getDataDBProducto(' + datArr[0] + ',false)" data-toggle="modal" data-target="#modal_editar_productos"></button>';
                        }

                    }
                }

            ]

        });


        $('#tttttttttttttttt').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
        $('#1111111111111').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });


    });

    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-file-preview-zone').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-file-image").change(function () {
        readImage(this);
        var url = '';
        var valor = $('#input-file-image').val();
        for (var i = 0; i < valor.length; i++) {
            if (i >= 12) {
                url += valor[i];
            }
        }

        $("#label-file-image").text(url);
    });

    function readImage2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-file-preview-zone-modelo').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-file-image-modelo").change(function () {
        readImage2(this);
        var url = '';
        var valor = $('#input-file-image-modelo').val();
        for (var i = 0; i < valor.length; i++) {
            if (i >= 12) {
                url += valor[i];
            }
        }

        // $("#label-file-image").text(url);
    });

</script>

</html>
