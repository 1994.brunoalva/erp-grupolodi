<?php
$indexRuta=2;

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../../assets/datatables.css" rel="stylesheet">
    <link href="../../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../../assets/datatables.js"></script>
    <script src="../../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <script src="../../aConfig/plugins/sweetalert2/sweetalert2.min.css"></script>

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
        .box-shadow{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .box-shadow:hover{
            box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .fade-enter-active, .fade-leave-active {
            transition: opacity .2s;
        }
        .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
            opacity: 0;
        }
        .box-inavilitado{
            background-color: #bdbdbd;
        }
        .box-modelo-data{
            background-color: #fff4c4; text-align: left; padding: 5px; border-radius: 10px;margin-bottom: 5px;
        }
        .box-modelo-data-h{
            background-color: #bbb390; text-align: left; padding: 5px; border-radius: 10px;margin-bottom: 5px;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    include '../../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Categorias</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <!--<div class="col-lg-6 text-right">
                                        <a href="new-folder.php" id="folder_btn_nuevo_folder" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Folder
                                        </a>

                                    </div>-->

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding" style="min-height: 300px">
                                <div class="col-md-12 text-right" style="margin-bottom: 20px">
                                    <button v-if="seeMarcas"  class="btn btn-primary"  data-toggle="modal" data-target="#modal_marca"><i class="fa fa-plus"></i> Nuevo</button>
                                    <button v-if="seeModelos"  class="btn btn-primary"  data-toggle="modal" data-target="#modal_model"><i class="fa fa-plus"></i> Nuevo</button>
                                    <button v-on:click="irAtrasBtn()" v-if="!seeCategoria"  class="btn btn-warning"><i class="fa fa-chevron-left"></i> Atras</button>
                                </div>

                                <transition name="fade">

                                    <div v-if="seeMarcas" class="col-md-12" >

                                        <div v-for="(item , index) in listaHoja" class="col-md-2">
                                            <div :class="'panel  box-shadow ' + (item.estado==0?'box-inavilitado':'')" style="height: 175px;">
                                                <div v-if="item.estado==0" class="text-center">Inhabilitado</div>
                                                <div class="panel-body text-center">
                                                    <div style=" height: 70px;">
                                                        <img style="max-height: 70px;max-width: 100%" alt="Sin Logo" class="img-lg  mar-btm" v-bind:src="'../../imagenes/logo/'+item.mar_logo +'.png'">
                                                    </div>

                                                    <p class="text-lg text-semibold mar-no text-main">{{item.mar_nombre}}</p>
                                                    <div class="mar-top">
                                                        <!--button v-if="isNeumatico"  v-on:click="selectMarcaa(item.mar_id)" class="btn btn-info">Ver Modelos</button-->
                                                        <button title="Ver Modelos" v-if="isNeumatico"  v-on:click="selectMarcaa(item.mar_id,item.mar_nombre)" class="btn btn-info"><i class="fa fa-eye"></i></button>
                                                        <button title="Editar" v-on:click="editarMarca(index)"  data-toggle="modal" data-target="#modal_marca_editar" class="btn btn-success"><i class="fa fa-edit"></i></button>
                                                        <button title="Eliminar" v-if="item.estado==1" v-on:click="eliminarMarca(index)" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                                        <button title="Habilitar modelo" v-if="item.estado==0" v-on:click="eliminarMarca(index)" class="btn btn-info"><i class="fa fa-check"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </transition>

                                <transition name="fade">
                                <div  v-if="seeCategoria"  class="col-md-12" >



                                    <div v-for="(item , index) in listaCategoria"  class="col-md-2">
                                        <div class="panel box-shadow">
                                            <div class="panel-body text-center">
                                                <p style="font-size: 20px" class="text-semibold mar-no text-main">{{item.cat_nombre}}</p>
                                                <div class="mar-top">
                                                    <button v-on:click="selectCategoria(item.cat_id,index)" class="btn btn-info">Ver Marcas</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </transition>

                                <transition name="fade">

                                    <div v-if="seeModelos" class="col-md-12" >

                                        <div v-for="(item , index) in listaHoja" class="col-md-2">
                                            <div :class="'panel  box-shadow ' + (item.estado==0?'box-inavilitado':'')" style="height: 260px;">
                                                <div class="panel-body text-center">
                                                    <div style=" height: 70px;">
                                                        <img style="max-height: 70px;max-width: 100%" alt="Sin Logo" class="img-lg mar-btm" v-bind:src="'../../imagenes/modelo/'+item.mod_img +'.png'">
                                                    </div>

                                                    <p class="text-lg text-semibold mar-no text-main">{{item.mod_nombre}}</p>
                                                    <div  :class="(item.estado==0?'box-modelo-data-h':'box-modelo-data')">
                                                        <p><strong>Aro: </strong>{{item.datos.aro}}</p>
                                                        <p><strong>Plieges: </strong>{{item.datos.ancho}}</p>
                                                        <p><strong>Ancho interno: </strong>{{item.datos.plieges}}</p>

                                                    </div>
                                                    <div class="mar-top">

                                                        <button v-on:click="seledtModel(index)"  data-toggle="modal" data-target="#modal_marca_editar" class="btn btn-success"><i class="fa fa-edit"></i></button>
                                                        <button v-if="item.estado==1" v-on:click="eliminarModelo(index)" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                                        <button v-if="item.estado==0" v-on:click="eliminarModelo(index)" class="btn btn-info"><i class="fa fa-check"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </transition>
                                <div v-if="seeModelos||seeMarcas" class="col-md-12" style="text-align: center">
                                    <nav aria-label="...">
                                        <ul class="pagination">
                                            <li class="page-item">
                                                <a  v-on:click="setSwithHoja(-1)"  class="page-link" href="javaScript: void(0)" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                            <li v-for="(item , index ) in listaPagination" v-bind:class=" item == hojaActual? 'page-item active':'page-item' ">
                                                <a v-if="item != hojaActual" v-on:click="setNumHoja(item)" class="page-link" href="javaScript: void(0)" >{{item}}</a>
                                                <a  v-if="item == hojaActual" class="page-link" href="javaScript: void(0)" >{{item}} <span class="sr-only">(current)</span></a>
                                            </li>

                                            <li class="page-item">
                                                <a v-on:click="setSwithHoja(1)" class="page-link" href="javaScript: void(0)" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>

                            </div>
                            <div  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div>

                                    <div class="modal fade" id="modal_marca" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800; display: none;">
                                        <div class="modal-dialog modal-xs " role="document">
                                            <div class="modal-content">
                                                <div class="modal-header no-border no-padding">
                                                    <div class="modal-header text-center color-modal-header">
                                                        <h3 class="modal-title">Agregar Nueva Marca</h3>
                                                    </div>
                                                </div>
                                                <div class="modal-body  no-border">
                                                    <div class="container-fluid">
                                                        <div class="form-group col-xs-12 no-padding">
                                                            <label class="col-xs-12 no-padding">Nombre:</label>
                                                            <input id="modal-marca-input-nombre" class="form-control" type="text" placeholder="ejem. CHINA TYRE" required="">
                                                        </div>
                                                        <div class="form-group col-xs-12 no-padding text-center" style="max-height: 275px;padding: 15px; border: 1px solid rgba(10,107,206,0.6);border-radius: 1px;">
                                                            <div class="col-xs-12" style="padding: 0;">

                                                                <div class="md-12" width="100%" style="max-height: 167px; display: flex; padding: 15px;">
                                                                    <img src="../../imagenes/logo/img-icon.svg" id="img-file-preview-zone" width="auto" style="margin: auto" height="170">
                                                                </div>

                                                                <div class="col-xs-12 text-center" style="margin-top: 25px;">
                                                                    <div class="form-group col-xs-12 no-margin no-padding">
                                                                        <h5 id="label-file-image" class="col-xs-12">
                                                                            ( Ninguna imagen seleccionada.... )
                                                                        </h5>
                                                                    </div>
                                                                    <input type="file" accept=".png" id="input-file-image" name="input-file-image" class="no-display">
                                                                    <div class="form-group col-xs-6">
                                                                        <label type="button" for="input-file-image" class="btn btn-sm btn-primary col-xs-12" title="Subir logotipo de importador">
                                                                            <i class="fa fa-file"></i> Subir Logo
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-group col-xs-6">
                                                                        <label onclick="limpiarmodelo()"  id="modal-marca-btn-eliminar-logo" type="button" class="btn btn-sm btn-danger col-xs-12" title="Subir logotipo de importador">
                                                                            <i class="fa fa-remove"></i> Eliminar
                                                                        </label>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="container-fluid">
                                                        <hr class="line-frame-modal">
                                                    </div>
                                                    <div class="container-fluid text-right">

                                                        <button type="button" onclick="APP.guardarMarca()" id="modal-marca-btn-guardar" class="btn btn-primary">
                                                            Guardar
                                                        </button>
                                                        <button onclick="limpiarRegMar()" type="button" id="modal-marca-btn-limpiar" class="btn btn-default">
                                                            Limpiar
                                                        </button>
                                                        <button onclick="limpiarRegMar()" type="button" id="modal-marca-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                                                            Cerrar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="modal_model" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800; display: none;">
                                        <div class="modal-dialog modal-xs " role="document">
                                            <div class="modal-content">
                                                <div class="modal-header no-border no-padding">
                                                    <div class="modal-header text-center color-modal-header">
                                                        <h3 class="modal-title">Agregar Nueva Modelo</h3>
                                                    </div>
                                                </div>
                                                <div class="modal-body  no-border">
                                                    <div class="container-fluid">
                                                        <div class="form-group col-xs-12 no-padding">
                                                            <label class="col-xs-12 no-padding">Nombre:</label>
                                                            <input id="modal-modelo-input-nombre" class="form-control" type="text" placeholder="ejem. CHINA TYRE" required="">
                                                        </div>
                                                        <div class="form-group col-xs-12 no-padding text-center" style="max-height: 275px;padding: 15px; border: 1px solid rgba(10,107,206,0.6);border-radius: 1px;">
                                                            <div class="col-xs-12" style="padding: 0;">

                                                                <div class="md-12" width="100%" style="max-height: 167px; display: flex; padding: 15px;">
                                                                    <img src="../../imagenes/logo/img-icon.svg" id="img-file-preview-zone2" width="auto" style="margin: auto" height="170">
                                                                </div>

                                                                <div class="col-xs-12 text-center" style="margin-top: 25px;">
                                                                    <div class="form-group col-xs-12 no-margin no-padding">
                                                                        <h5 id="label-file-image2" class="col-xs-12">
                                                                            ( Ninguna imagen seleccionada.... )
                                                                        </h5>
                                                                    </div>
                                                                    <input type="file" accept=".png" id="input-file-image2" name="input-file-image" class="no-display">
                                                                    <div class="form-group col-xs-6">
                                                                        <label type="button" for="input-file-image2" class="btn btn-sm btn-primary col-xs-12" title="Subir logotipo de importador">
                                                                            <i class="fa fa-file"></i> Subir Logo
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-group col-xs-6">
                                                                        <label onclick="limpiarmodelo()" id="modal-marca-btn-eliminar-logo2" type="button" class="btn btn-sm btn-danger col-xs-12" title="Subir logotipo de importador">
                                                                            <i class="fa fa-remove"></i> Eliminar
                                                                        </label>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="container-fluid">
                                                        <hr class="line-frame-modal">
                                                    </div>
                                                    <div class="container-fluid text-right">

                                                        <button type="button" onclick="APP.guardarModelo()" id="modal-marca-btn-guardar" class="btn btn-primary">
                                                            Guardar
                                                        </button>

                                                        <button onclick="limpiarRegModelo()" type="button" id="modal-marca-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                                                            Cerrar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="modal_agregar_categoria" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog" role="document" style="width: 80%;">
                                            <div class="modal-content">
                                                <div class="modal-header no-border no-padding">
                                                    <div class="modal-header text-center color-modal-header">
                                                        <h3 class="modal-title">Editar Productos</h3>
                                                    </div>
                                                </div>

                                                <div class="modal-body  no-border">


                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="modal_marca_editar" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800; display: none;">
                                        <div class="modal-dialog modal-xs " role="document">
                                            <div class="modal-content">
                                                <div class="modal-header no-border no-padding">
                                                    <div class="modal-header text-center color-modal-header">
                                                        <h3 class="modal-title">Editar Marca</h3>
                                                    </div>
                                                </div>
                                                <div class="modal-body  no-border">
                                                    <div class="container-fluid">
                                                        <input type="hidden" id="idmarca">
                                                        <div class="form-group col-xs-12 no-padding">
                                                            <label class="col-xs-12 no-padding">Nombre:</label>
                                                            <input disabled id="modal-marca-input-nombre_edt" class="form-control" type="text" placeholder="ejem. CHINA TYRE" required="">
                                                        </div>
                                                        <div class="form-group col-xs-12 no-padding text-center" style="max-height: 275px;padding: 15px; border: 1px solid rgba(10,107,206,0.6);border-radius: 1px;">
                                                            <div class="col-xs-12" style="padding: 0;">

                                                                <div class="md-12" width="100%" style="max-height: 167px; display: flex; padding: 15px;">
                                                                    <img src="../../imagenes/logo/img-icon.svg" id="img-file-preview-zone_edt" width="auto" style="margin: auto" height="170">
                                                                </div>

                                                                <div class="col-xs-12 text-center" style="margin-top: 25px;">
                                                                    <div class="form-group col-xs-12 no-margin no-padding">

                                                                    </div>
                                                                    <input type="file" accept=".png" id="input-file-image_edt" name="input-file-image_edt" class="no-display">
                                                                    <div class="form-group col-xs-6">
                                                                        <label type="button" for="input-file-image_edt" class="btn btn-sm btn-primary col-xs-12" title="Subir logotipo de importador">
                                                                            <i class="fa fa-file"></i> Subir Logo
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-group col-xs-6">
                                                                        <label onclick="limpiarmodelo()" id="modal-marca-btn-eliminar-logo" type="button" class="btn btn-sm btn-danger col-xs-12" title="Subir logotipo de importador">
                                                                            <i class="fa fa-remove"></i> Eliminar
                                                                        </label>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="container-fluid">
                                                        <hr class="line-frame-modal">
                                                    </div>
                                                    <div class="container-fluid text-right">

                                                        <button type="button" onclick="APP.guardarMarcaEdit()" id="modal-marca-btn-guardar" class="btn btn-primary">
                                                            Guardar
                                                        </button>

                                                        <button onclick="limpiarRegMar()" type="button" id="modal-marca-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                                                            Cerrar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="modal_modelo_editar" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800; display: none;">
                                        <div class="modal-dialog modal-xs " role="document">
                                            <div class="modal-content">
                                                <div class="modal-header no-border no-padding">
                                                    <div class="modal-header text-center color-modal-header">
                                                        <h3 class="modal-title">Editar Modelo</h3>
                                                    </div>
                                                </div>
                                                <div class="modal-body  no-border">
                                                    <div class="container-fluid">
                                                        <input type="hidden" id="idmarca">
                                                        <div class="form-group col-xs-12 no-padding">
                                                            <label class="col-xs-12 no-padding">Nombre:</label>
                                                            <input  id="modal-modelo-input-nombre_edt" class="form-control" type="text" placeholder="ejem. CHINA TYRE" required="">
                                                        </div>
                                                        <div class="form-group col-xs-12 no-padding text-center" style="max-height: 275px;padding: 15px; border: 1px solid rgba(10,107,206,0.6);border-radius: 1px;">
                                                            <div class="col-xs-12" style="padding: 0;">

                                                                <div class="md-12" width="100%" style="max-height: 167px; display: flex; padding: 15px;">
                                                                    <img src="../../imagenes/logo/img-icon.svg" id="img-file-preview-zone_edt-modelo" width="auto" style="margin: auto" height="170">
                                                                </div>

                                                                <div class="col-xs-12 text-center" style="margin-top: 25px;">
                                                                    <div class="form-group col-xs-12 no-margin no-padding">

                                                                    </div>
                                                                    <input type="file" accept=".png" id="input-file-image_edt-modelo" name="input-file-image_edt" class="no-display">
                                                                    <div class="form-group col-xs-6">
                                                                        <label type="button" for="input-file-image_edt-modelo" class="btn btn-sm btn-primary col-xs-12" title="Subir logotipo de importador">
                                                                            <i class="fa fa-file"></i> Subir Logo
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-group col-xs-6">
                                                                        <label onclick='limpiarmodelo()'  id="modal-modelo-btn-eliminar-logo" type="button" class="btn btn-sm btn-danger col-xs-12" title="Subir logotipo de importador">
                                                                            <i class="fa fa-remove"></i> Eliminar
                                                                        </label>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="container-fluid">
                                                        <hr class="line-frame-modal">
                                                    </div>
                                                    <div class="container-fluid text-right">

                                                        <button type="button" onclick="APP.guardarModeloEdit()" id="modal-marca-btn-guardar" class="btn btn-primary">
                                                            Guardar
                                                        </button>

                                                        <button onclick="limpiarRegMar()" type="button" id="modal-marca-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                                                            Cerrar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>




    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="module" src="../../aConfig/Input_validate.js"></script>
    <script src="../../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>
        function  limpiarmodelo(){
            $("#input-file-image_edt-modelo").val('');
            $("#input-file-image2").val('');
            $("#input-file-image").val('');
            $("#input-file-image_edt").val('');

            $("#img-file-preview-zone_edt-modelo").attr("src","");
            $("#img-file-preview-zone2").attr("src","");
            $("#img-file-preview-zone").attr("src","");
            $("#img-file-preview-zone_edt").attr("src","");
        }
        function getNameImage() {
            var name = '';
            name = $("#modal-marca-input-nombre").val();
            name += '_' + APP._data.idCategoria;
            /*var name = valor.substring(valor.length - 4, valor.length);*/
            console.log(name);
            return name;
        }
        function getNameImage2() {
            var name = '';
            name = $("#modal-modelo-input-nombre").val();
            name += '_' + APP._data.idMarca;
            /*var name = valor.substring(valor.length - 4, valor.length);*/
            console.log(name);
            return name;
        }
        function  limpiarRegMar() {
            $('#modal-marca-input-nombre').val("");
            $('#input-file-image').val("");
            $("#img-file-preview-zone").attr("src","../../imagenes/logo/img-icon.svg")
        }

        function  limpiarRegModelo() {
            $('#modal-modelo-input-nombre').val("");
            $('#input-file-image2').val("");
            $("#img-file-preview-zone2").attr("src","../../imagenes/logo/img-icon.svg")
        }
        function setImage(frmData) {
            $.ajax({
                data: frmData,
                url: '../../ajax/SetImage/uploadImage.php',
                type: 'POST',
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    var json = JSON.parse(JSON.stringify(response));
                    /*if (json) {
                        alerta.alerSuccess('Cargando nueva imagen');
                    } else {
                        alerta.alerInfo('no se pudo cargar nueva imagen');
                    }*/
                },
                error: function (err) {
                    console.log(err)
                    //alerta.alerError('Error al cargar imagen<br>Reporte este error!!');
                }
            });
        }
        const APP =  new Vue({
            el:"#contenedorprincipal",
            data:{
                seeCategoria:true,
                seeMarcas:false,
                seeModelos:false,
                isNeumatico:false,

                idCategoria:0,
                indexCatego:0,
                idMarca:0,

                nombreCatego:'',


                listaCategoria:[],
                listaMarca:[],
                listaModelos:[],

                tempMarca:{},
                tempModelo:{},

                listaTemporalItem:[],

                listaHoja:[],
                cantidadHojas:0,
                cantidadItem:24,
                hojaActual:1,


            },
            methods:{
                guardarMarcaEdit(){
                    //var dataInput = $('#modal-marca-input-nombre_edt').val();
                    var dataFile = $('#input-file-image_edt').val();

                    var dat = this.tempMarca;


                    var nombrelog= dat.mar_logo;

                    if (dataFile.length > 0) {
                        nombrelog=getNameImage();
                        var frmData = new FormData();
                        frmData.append('file', $("#input-file-image_edt")[0].files[0]);
                        frmData.append('folder', 'logo');

                        $.ajax({
                            xhr: function() {
                                var xhr = new window.XMLHttpRequest();
                                xhr.upload.addEventListener("progress", function(evt) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = ((evt.loaded / evt.total) * 100);
                                        //app._data.progreso=percentComplete;
                                    }
                                }, false);
                                return xhr;
                            },
                            type: 'POST',
                            url: '../../ajax/ManagerFiles/upd_img_mm.php',
                            data: frmData,
                            contentType: false,
                            cache: false,
                            processData:false,
                            beforeSend: function(){
                                // app._data.progreso=0;
                                // app._data.starProgres=true;
                            },
                            error:function(err){
                                console.log(err.responseText)
                                //alert('File upload failed, please try again.');


                            },
                            success: function(resp){
                                console.log(resp)
                                var jsp = JSON.parse(resp);
                                nombrelog = jsp.datos;
                                dat.imgt =nombrelog;
                                $.ajax({
                                    type: "POST",
                                    url: "../../ajax/Marca/actualizarMarca.php",
                                    data: dat,
                                    success: function (resp) {
                                        console.log(resp)
                                        $("#modal_marca_editar").modal("toggle")
                                        APP.selectCategoria(APP._data.idCategoria,APP._data.indexCatego);
                                        setTimeout(function () {
                                            APP.startPagination();
                                            APP.llenarHoja()
                                        },500)
                                    }
                                });

                            }
                        });

                    }else{
                        $.toast({
                            heading: 'INFORMACION',
                            text: "Seleccione una Imagen",
                            icon: 'info',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                    }


                },
                guardarModeloEdit(){
                    var dataInput = $('#modal-modelo-input-nombre_edt').val();
                    var dataFile = $('#input-file-image_edt-modelo').val();

                    var dat = this.tempModelo;
                    dat.mod_nombre=dataInput;
                    if (dataInput.length > 0) {
                        var nombrelog= dat.mod_img;
                        if (dataFile.length > 0) {
                            nombrelog=getNameImage();
                            var frmData = new FormData();
                            frmData.append('file', $("#input-file-image_edt-modelo")[0].files[0]);
                            frmData.append('folder', 'modelo');

                            $.ajax({
                                xhr: function() {
                                    var xhr = new window.XMLHttpRequest();
                                    xhr.upload.addEventListener("progress", function(evt) {
                                        if (evt.lengthComputable) {
                                            var percentComplete = ((evt.loaded / evt.total) * 100);
                                            //app._data.progreso=percentComplete;
                                        }
                                    }, false);
                                    return xhr;
                                },
                                type: 'POST',
                                url: '../../ajax/ManagerFiles/upd_img_mm.php',
                                data: frmData,
                                contentType: false,
                                cache: false,
                                processData:false,
                                beforeSend: function(){
                                    // app._data.progreso=0;
                                    // app._data.starProgres=true;
                                },
                                error:function(err){
                                    console.log(err.responseText)
                                    //alert('File upload failed, please try again.');


                                },
                                success: function(resp){
                                    console.log(resp)
                                    var jsp = JSON.parse(resp);
                                    nombrelog = jsp.datos;
                                    dat.imgt =nombrelog;
                                    $.ajax({
                                        type: "POST",
                                        url: "../../ajax/Modelo/actualizarModelo.php",
                                        data: dat,
                                        success: function (resp) {
                                            console.log(resp)
                                            $("#modal_modelo_editar").modal("toggle")

                                            APP.selectMarcaa(APP._data.idMarca,APP._data.nombreCatego);

                                        }
                                    });

                                }
                            });

                        }else{
                            dat.imgt =nombrelog;
                            $.ajax({
                                type: "POST",
                                url: "../../ajax/Modelo/actualizarModelo.php",
                                data: dat,
                                success: function (resp) {
                                    console.log(resp)
                                    $("#modal_modelo_editar").modal("toggle")
                                    APP.selectMarcaa(APP._data.idMarca,APP._data.nombreCatego);
                                }
                            });
                        }
                    } else {
                        alerInfo('Porfavor asigne un nombre a esta marca');
                    }

                },
                eliminarMarca(index){
                    var mar = this.listaHoja[index];
                    this.indexMarca = index;
                    const stado = mar.estado==1?0:1;
                    $.toast({
                        heading: 'INFORMACION',
                        text: "Procesando los cambios",
                        icon: 'info',
                        position: 'top-right',
                        hideAfter: '2500',
                    });
                    if (mar.isUse){
                        console.log("si")
                        console.log({idma:mar.mar_id, estado:stado})
                        $.ajax({
                            type: "POST",
                            url: "../../ajax/Marca/abilitador_marca.php",
                            data: {idma:mar.mar_id, estado:stado},
                            success: function (data) {
                                console.log(data);
                                APP.selectCategoria(APP._data.idCategoria,APP._data.indexCatego);
                            }
                        });
                    }else{
                        console.log("no")
                        $.ajax({
                            type: "POST",
                            url: "../../ajax/Marca/del_marca.php",
                            data: {idma:mar.mar_id},
                            success: function (data) {
                                console.log(data);
                                APP.selectCategoria(APP._data.idCategoria,APP._data.indexCatego);
                                //APP.selectMarcaa(APP._data.idMarca,APP._data.nombreCatego);
                            }
                        });


                    }

                },
                editarMarca(index){
                    var mar = this.listaHoja[index];
                    this.tempMarca = mar;
                    $("#select-categoria_edt").val(mar.cat_id)
                    $("#modal-marca-input-nombre_edt").val(mar.mar_nombre)
                    $("#img-file-preview-zone_edt").attr('src', "../../imagenes/logo/"+mar.mar_logo+".png");
                    $('#input-file-image_edt').val('');

                },
                eliminarModelo(index){
                    var mar = this.listaModelos[index];
                    //this.indexMarca = index;
                    const stado = mar.estado==1?0:1;

                    $.toast({
                        heading: 'INFORMACION',
                        text: "Procesando los cambios",
                        icon: 'info',
                        position: 'top-right',
                        hideAfter: '2500',
                    });

                    if (mar.isUse){
                        console.log("si")
                        console.log({idma:mar.mod_id, estado:stado})
                        $.ajax({
                            type: "POST",
                            url: "../../ajax/Modelo/abilitador_modelo.php",
                            data: {idma:mar.mod_id, estado:stado},
                            success: function (data) {
                                console.log(data);
                                APP.selectMarcaa(APP._data.idMarca,APP._data.nombreCatego);
                            }
                        });
                    }else{
                        console.log("no")
                        $.ajax({
                            type: "POST",
                            url: "../../ajax/Modelo/del_madelo.php",
                            data: {idma:mar.mod_id},
                            success: function (data) {
                                console.log(data);
                                APP.selectMarcaa(APP._data.idMarca,APP._data.nombreCatego);
                            }
                        });


                    }

                },
                seledtModel(index){
                    var mar = this.listaHoja[index];
                    this.tempModelo = mar;
                    $("#modal-modelo-input-nombre_edt").val(mar.mod_nombre)
                   // $("#modal-marca-input-nombre_edt").val(mar.mod_nombre)
                    $("#img-file-preview-zone_edt-modelo").attr('src', "../../imagenes/modelo/"+mar.mod_img+".png");
                    $('#input-file-image_edt-modelo').val('');

                },
                setNumHoja(num){
                    this.hojaActual=num;
                    this.llenarHoja();
                },
                setSwithHoja(num){
                    console.log(num);
                    if ( this.hojaActual+num > 0&& this.hojaActual+num <= this.cantidadHojas){
                        this.hojaActual+=num;
                        this.llenarHoja();
                    }

                },
                startPagination(){
                    if (this.seeMarcas){
                        this.cantidadHojas = Math.ceil(this.listaMarca.length/this.cantidadItem);
                        this.listaTemporalItem= this.listaMarca
                    }else if (this.seeModelos){
                        this.cantidadHojas = Math.ceil(this.listaModelos.length/this.cantidadItem);
                        this.listaTemporalItem= this.listaModelos
                    }


                },
                llenarHoja(){
                    this.listaHoja=[];

                    for (var i = (this.hojaActual-1)*this.cantidadItem; i < this.cantidadItem * this.hojaActual; i++){
                        if (i<this.listaTemporalItem.length){
                            this.listaHoja.push(this.listaTemporalItem[i]);
                        }else{
                            break;
                        }
                    }

                },
                guardarMarca(){
                    var dataInput = $('#modal-marca-input-nombre').val();
                    var dataFile = $('#input-file-image').val();
                    if (dataInput.length > 0) {
                        var nombrelog= "";
                        if (dataFile.length > 0) {
                            nombrelog=getNameImage();
                            var frmData = new FormData();
                            frmData.append('image-file', $("#input-file-image")[0].files[0]);
                            frmData.append('image-name', nombrelog);
                            frmData.append('image-set-ruta', '../../imagenes/logo/');
                            setImage(frmData)

                        }
                        $.ajax({
                            type: "POST",
                            url: "../../ajax/Marca/setNewMarca.php",
                            data: {
                                idc:APP._data.idCategoria,nom:dataInput,log:nombrelog
                            },
                            success: function (data) {
                                console.log(data)
                               if (isJson(data)){
                                   var jsn=JSON.parse(data)
                                   if (jsn.res){
                                       $('#modal_marca').modal('toggle');
                                       APP._data.listaMarca.push(jsn.data)
                                       limpiarRegMar()
                                       APP.startPagination();
                                       APP.llenarHoja()
                                   }
                               }
                            }
                        });

                    } else {
                        alerInfo('Porfavor asigne un nombre a esta marca');
                    }
                },
                guardarModelo(){
                    var dataInput = $('#modal-modelo-input-nombre').val();
                    var dataFile = $('#input-file-image2').val();
                    if (dataInput.length > 0) {
                        var nombrelog= "";
                        if (dataFile.length > 0) {
                            nombrelog=getNameImage2();
                            var frmData = new FormData();
                            frmData.append('image-file', $("#input-file-image2")[0].files[0]);
                            frmData.append('image-name', nombrelog);
                            frmData.append('image-set-ruta', '../../imagenes/modelo/');
                            setImage(frmData)

                        }
                        $.ajax({
                            type: "POST",
                            url: "../../ajax/Modelo/setNewModelo.php",
                            data: {
                                idc:APP._data.idMarca,nom:dataInput,log:nombrelog
                            },
                            success: function (data) {
                                console.log(data)
                               if (isJson(data)){
                                   var jsn=JSON.parse(data)
                                   if (jsn.res){
                                       $('#modal_model').modal('toggle');
                                       APP._data.listaModelos.push(jsn.data)
                                       limpiarRegModelo()
                                       APP.startPagination();
                                       APP.llenarHoja()
                                   }
                               }
                            }
                        });

                    } else {
                        alerInfo('Porfavor asigne un nombre a esta marca');
                    }
                },

                irAtrasBtn(){
                    if (this.seeMarcas){
                        this.seeMarcas=false;
                        this.seeModelos=false;
                        $("#tittle-header-body").text("Categorias")
                        setTimeout(function () {
                            APP._data.seeCategoria=true;

                        },1000)
                    }else if (this.seeModelos){
                        const nombre =  this._data.listaCategoria[this.indexCatego].cat_nombre;
                        this.seeModelos=false;
                        $("#tittle-header-body").text("Modelos de "+nombre)
                        setTimeout(function () {
                            APP._data.seeMarcas=true;
                            APP.startPagination();
                            APP.setNumHoja(1)
                        },1000)
                    }
                },
                selectMarcaa(idmodelo,nom){
                    this.idMarca=idmodelo;
                    this.seeMarcas=false;
                    this.nombreCatego = nom;
                    const nombre = this._data.listaCategoria[this.indexCatego].cat_nombre +" marcas: "+nom;
                    $.ajax({
                        type: "POST",
                        url: "../../ajax/Modelo/getAllModeloMarca.php",
                        data:{
                            id:idmodelo
                        },
                        success: function (data) {
                            console.log(data)
                            $("#tittle-header-body").text("Modelos de "+nombre)
                            APP._data.listaModelos =JSON.parse(data)
                            setTimeout(function () {
                                APP._data.seeModelos=true;
                                APP.startPagination();
                                APP.setNumHoja(1)
                            },1000)

                        },
                        error: function (error) {
                            console.log(error)
                        }
                    });

                },

                selectCategoria(idcate,index){

                    console.log("sssssssssss")

                    this.indexCatego = index;

                    this.seeCategoria=false;
                    this.isNeumatico = this._data.listaCategoria[index].cat_id ==1;
                    this.idCategoria= this._data.listaCategoria[index].cat_id;
                    const nombre =  this._data.listaCategoria[index].cat_nombre;
                    $.ajax({
                        type: "POST",
                        url: "../../ajax/Marca/getMarcaForCategoria2.php",
                        data:{
                            id:idcate
                        },
                        success: function (data) {
                            console.log(data)

                            $("#tittle-header-body").text("Marcas de "+nombre)
                            APP._data.listaMarca=JSON.parse(data);
                            setTimeout(function () {
                                APP._data.seeMarcas=true;
                                APP.startPagination();
                                APP.setNumHoja(1)
                            },1000)

                        },
                        error: function (err) {
                            console.log(err.responseText)
                        }
                    });

                },
                cargarDataCategoria(){
                    $.ajax({
                        type: "POST",
                        url: "../../ajax/Categoria/getAllCategoria.php",
                        success: function (data) {
                            APP._data.listaCategoria=data;
                        }
                    });
                }
            },

            computed:{
            listaPagination(){
                var lista = [];
                for (var i =1 ; i<= this. cantidadHojas;i++){
                    lista.push(i);
                }
                return lista;
            }
        }
        });
        $( document ).ready(function() {
            APP.cargarDataCategoria();

            function readImage(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#img-file-preview-zone').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            function readImage2(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#img-file-preview-zone2').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }function readImage3(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#img-file-preview-zone_edt').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#input-file-image").change(function () {
                readImage(this);
                var url = '';
                var valor = $('#input-file-image').val();
                for (var i = 0; i < valor.length; i++) {
                    if (i >= 12) {
                        url += valor[i];
                    }
                }

                $("#label-file-image").text(url);
            });

            $("#input-file-image2").change(function () {
                readImage2(this);
                var url = '';
                var valor = $('#input-file-image2').val();
                for (var i = 0; i < valor.length; i++) {
                    if (i >= 12) {
                        url += valor[i];
                    }
                }

                $("#label-file-image2").text(url);
            });

            function readImage4(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#img-file-preview-zone_edt-modelo').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#input-file-image_edt-modelo").change(function () {
                readImage4(this);
                var url = '';
                var valor = $('#input-file-image_edt-modelo').val();
                for (var i = 0; i < valor.length; i++) {
                    if (i >= 12) {
                        url += valor[i];
                    }
                }

               // $("#label-file-image2").text(url);
            });
            function readImage5(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#img-file-preview-zone_edt').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#input-file-image_edt").change(function () {
                readImage5(this);
                var url = '';
                var valor = $('#input-file-image_edt').val();
                for (var i = 0; i < valor.length; i++) {
                    if (i >= 12) {
                        url += valor[i];
                    }
                }

               // $("#label-file-image2").text(url);
            });

        });
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }

        function alerInfo(msg) {
            $.toast({
                heading: 'INFORMACION',
                text: msg,
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
        }
    </script>



</body>


</html>
