<?php
$indexRuta=1;
require  '../conexion/Conexion.php';
$compraid= $_GET['compra'];
$conexion = (new Conexion())->getConexion();
$sql="SELECT
  emp_id, 
  emp_nombre 
FROM sys_empresas ";
$resEmpre = $conexion->query($sql);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">
    <link href="../aConfig/plugins/sweetalert2/sweetalert2.min.css">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../aConfig/plugins/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script>
        const compra='<?=$compraid ?>';
    </script>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Compras Nacionales</strong>
                                        </h2>
                                    </div>
                                    <!--BOTONES-->

                                    <div class="col-lg-6 text-right">

                                        <span style="color: white">----------------------------</span>
                                        <a href="compras.php" class="btn btn-warning">
                                            <i class="glyphicon glyphicon-chevron-left"></i> Salir
                                        </a>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>
                            <div id="contenedor-principal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">

                                <div class="row">
                                        <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                            <label class="col-xs-12 no-padding">EMPRESA:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <select disabled v-model="compra.empresa" class="form-control"  >
                                                    <?php
                                                    foreach ($resEmpre as $emp){
                                                        echo "<option value='{$emp['emp_id']}'>{$emp['emp_nombre']}</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                            <label class="col-xs-12 no-padding">PROVEEDOR:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <input placeholder="Escriba el RUC para buscar" disabled v-model="compra.nombreProvee"  type="text" id="input-proveedor-buscar" class="form-control"  >
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">FECHA:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <input disabled v-model="compra.fecha"  type="date" class="form-control"  >
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">DOCUMENTO:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <select disabled v-model="compra.tido"  class="form-control"  >
                                                    <option value="1">FACTURA</option>
                                                    <option value="3">BOLETA</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">NUN. REF.:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <input disabled v-model="compra.numDoc"  type="text" class="form-control"  placeholder="ejem.  FF02-0001">
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">MONEDA:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <select disabled @change="onChangeMoneda($event)"  v-model="compra.moneda"  class="form-control"  >
                                                    <option value="1">SOLES</option>
                                                    <option value="2">DOLARES</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">TAZA CAMBIO:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <input disabled :disabled="compra.moneda!=2" @keypress="onlyNumber" v-model="compra.tc"  type="text" class="form-control"  placeholder="">
                                            </div>
                                        </div>

                                        <div v-if="archivo.length>0" class="form-group col-xs-7 col-sm-7 col-md-2">
                                            <label class="col-xs-12 no-padding">ARCHIVO:</label>

                                            <div class="input-group col-xs-12 no-padding">
                                                <a target="_blank" :href="'../imagenes/doc_compra/'+archivo" type="button" class="btn btn-info">Ver Archivo</a>

                                            </div>
                                        </div>



                                </div>
                                <div class="form-group ">
                                    <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    PRODUCTOS DE LA COMPRA<!--Padding is optional-->
                                                  </span>

                                    </div>
                                </div>

                                <div class="row">
                                    <table  class="table table-striped table-bordered table-hover" style="width:100%">
                                        <thead class="bg-head-table">
                                        <tr  style="background-color: #007ac3; color: white">
                                            <th  style="border-right-color: #007ac3; text-align: center">#</th>
                                            <th  style="border-right-color: #007ac3; text-align: center">PRODUCTO</th>
                                            <th  style="border-right-color: #007ac3; text-align: center">MARCA</th>
                                            <th  style="border-right-color: #007ac3; text-align: center">CANTIDAD</th>
                                            <th  style="border-right-color: #007ac3; text-align: center">PRECIO</th>
                                            <th  style="border-right-color: #007ac3; text-align: center">IMPORTE</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr v-for="(item, index) in productos">

                                            <td class="text-center">{{index+1}}</td>
                                            <td class="text-center">{{item.produ_nombre}}</td>
                                            <td class="text-center">{{item.mar_nombre}}</td>
                                            <td class="text-center">{{item.cantidad}}</td>
                                            <td class="text-center">{{item.precio_u}}</td>
                                            <td class="text-center">{{parseFloat(item.importe).toFixed(2)}}</td>

                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">Sub. Total</td>
                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{subTotal.toFixed(2)}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">IGV</td>
                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{igv.toFixed(2)}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">Total</td>
                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{compra.monto.toFixed(2)}}</td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="text/javascript" src="../aConfig/scripts/venta.js"></script>
    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <<script type="text/javascript">
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


        const APP = new Vue({
            el:"#contenedor-principal",
            data:{
                simboleModena:'',
                igv:0,
                totalTabla:0,
                dataRegistro:{
                    preData:{},
                    nombre:'',
                    cantidad:'',
                    precio:''
                },
                compra:{
                    empresa:'',
                    idProvee:'',
                    nombreProvee:'',
                    fecha:'',
                    tido:'',
                    numDoc:'',
                    moneda:'',
                    tc:'0.00',
                    monto:0
                },
                productos:[],
                archivo:''
            },
            methods:{
                 getDataCompra(){
                     $.ajax({
                         type: "POST",
                         url: '../ajax/ProductosRegalos/getData.php',
                         data: {compra},
                         success: function (data) {
                             data= JSON.parse(data)
                             console.log(data)
                             APP._data.compra={
                                        empresa:data.empresa_id,
                                     idProvee:'',
                                     nombreProvee:data.provee_desc,
                                     fecha:data.com_fecha,
                                     tido:data.tipo_doc,
                                     numDoc:data.num_ref,
                                     moneda:data.moneda,
                                     tc:data.cambio,
                                     monto:0
                             }
                             APP._data.archivo = data.archivo;
                             APP._data.productos = data.productos;
                         }
                     });

                 },
                onChangeMoneda(event){

                },
                onlyNumber ($event) {
                    //console.log($event.keyCode); //keyCodes value
                    let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
                    if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                        $event.preventDefault();
                    }
                }
            },
            computed:{
                subTotal(){
                    var subtotal=0;
                    var monto=0;
                    for (var  i = 0; i< this.productos.length; i++){
                        monto += parseFloat(this.productos[i].importe);
                    }
                    subtotal=monto/1.18;
                    this.igv = monto - subtotal;
                    this.compra.monto = monto;
                    return subtotal;
                }
            }
        });

        $(document).ready(function () {
            APP.getDataCompra();
            $("#input-producto-buscar").autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "../ajax/ProductosRegalos/search_produc.php",
                        data: { term: request.term},
                        success: function(data){
                            response(data);
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log(errorThrown)
                        },
                        dataType: 'json'
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    console.log(ui.item)
                    APP._data.dataRegistro.preData = ui.item;
                    APP._data.dataRegistro.nombre=ui.item.produ_nombre
                    APP._data.dataRegistro.cantidad=''
                    APP._data.dataRegistro.precio=''
                    event.preventDefault();

                }
            });
            $("#input-proveedor-buscar").autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "../ajax/Proveedores/buscarProveedorNacional.php",
                        data: { term: request.term},
                        success: function(data){
                            response(data);
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log(errorThrown)
                        },
                        dataType: 'json'
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    console.log(ui.item)
                    APP._data.compra.idProvee=ui.item.provee_id
                    APP._data.compra.nombreProvee=ui.item.provee_desc

                    event.preventDefault();

                }
            });



        });

    </script>


</body>

</html>
