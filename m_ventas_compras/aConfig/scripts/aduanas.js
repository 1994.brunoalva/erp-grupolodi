import alerta from "../alertToas.js";

$(document).ready(function () {

    $('#modal-aduana-btn-ruc-num').click(function () {
        var numRuc= $('#modal-aduana-input-ruc-num').val();
        consultarRuc(numRuc);
    });
    function consultarRuc(ruc) {
        var json =[];

        $.ajax({
            data: {ruc},
            url: '../ajax/libSunat/sunat/example/consultaRuc.php',
            type: 'POST',
            async: true,
            beforeSend: function() {
                alerta.alerInfo('Buscando RUC en Sunat, Porfavor espere ..');

            },
            success: function (response) {
                json = JSON.parse(JSON.stringify(response));
                if (json.success) {
                    $("#modal-aduanas-input-nombre").val(json.result.razon_social);
                    alerta.alerSuccess('Cargando Datos de empresa');
                } else {
                    alerta.alerWar('El numero RUC es incorrecto ..');
                }

            },
            error: function () {
                alerta.alerError('Error al en el servidor de SUNAT<br>Reporte este error!!');
            },
        });
        return json;
    }

    $('#aduana-input-nombre').focus(function () {
        $('#modal_buscar_aduanas').modal('show');
    });


    $('#aduana-btn-guardar').click(function () {
        var impor_id =$('#folio_input_id').val();
        console.log(impor_id);
        searchAduana(impor_id);
    });

    $('.input-search-adu').focus(function () {
        $('.input-search-adu').keyup(function (e) {
            var letra = $('.input-search-adu').val();
            $('#table-agencia-aduanera_filter label input').val(letra);
            $('#table-agencia-aduanera_filter label input').keyup();
        });

    });


    var updateAgenciaData=[];
    var setAgenciaData=[];
    function searchAduana(id) {
        $.ajax({
            data: {id},
            url: '../ajax/Aduana/getForID.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json.length!==0) {
                    updateAgenciaData.push(['impor_aduana_id',$('#aduana-input_id').val()]);
                    updateAgenciaData.push(['age_adua_id',$('#aduana-input-agencia_id').val()]);
                    updateAgenciaData.push(['impor_dua',$('#aduana-input-dua').val()]);
                    updateAgenciaData.push(['impor_fecha_num',$('#aduana-input-fecha-numeracion').val()]);
                    updateAgenciaData.push(['canal_id',$('#select_canal').val()]);
                    updateAgenciaData.push(['impor_fecha_ingreso',$('#aduana-input-fecha-ingreso').val()]);
                    /*updateAgenciaData.push(['contai_id','']);*/
                    updateAgenciaData.push(['impor_id',$('#folio_input_id').val()]);
                    updateAgenciaData.push(['estado','b1']);
                    updateAduana(updateAgenciaData);
                } else {
                    setAgenciaData.push('');
                    setAgenciaData.push($('#aduana-input-agencia_id').val());
                    setAgenciaData.push($('#aduana-input-dua').val());
                    setAgenciaData.push($('#aduana-input-fecha-numeracion').val());
                    setAgenciaData.push($('#select_canal').val());
                    //setAgenciaData.push($('#aduana-input-fecha-ingreso').val());
                    setAgenciaData.push('2020-08-05');
                    setAgenciaData.push('');
                    setAgenciaData.push($('#folio_input_id').val());
                    setAgenciaData.push('b1');
                    setAduana(setAgenciaData);

                }
                setAgenciaData=[];
                updateAgenciaData=[];
            },
            error: function (err) {
                console.log(err)
                alerta.alerError('Error al Buscar aduanas<br>Reporte este error!!');
            }
        });
    }
    function setAduana(AgenciaData) {
        $.ajax({
            data: {'array': JSON.stringify(AgenciaData)},
            url: '../ajax/Aduana/setAduana.php',
            type: 'POST',
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json) {
                    alerta.alerSuccess('Se ha guardado aduanas');
                } else {
                    alerta.alerInfo('No se pudo guardar aduanas asegurese de completar todos los campos');
                }
            },
            error: function (err) {
                console.log(err)
                alerta.alerError('Error al guardar aduanas<br>Reporte este error!!');
            }
        });
    }

    function updateAduana(AgenciaData) {
        $.ajax({
            data: {'array': JSON.stringify(AgenciaData)},
            url: '../ajax/Aduana/updateAduana.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json) {
                    alerta.alerSuccess('Se ha actualizado aduanas');
                } else {
                    alerta.alerInfo('No se pudo actualizar aduanas asegurese de completar todos los campos');
                }
            },
            error: function () {
                alerta.alerError('Error al actualizar aduanas<br>Reporte este error!!');
            }
        });
    }



    /*-----------------------------------------------------AJAX LISTA DE AGENCIAS----------------------------------------------------------*/
    /*MODAL AGENCIA ADUANA*/
    $('#modal-aduanas-btn-guardar').click(function () {
        var aduaData = new Array();
        aduaData.push('');
        aduaData.push($('#modal-aduanas-input-codigo').val());
        aduaData.push($('#modal-aduanas-input-nombre').val());
        aduaData.push($('#modal-aduana-input-ruc-num').val());
        aduaData.push($('#modal-aduanas-input-juris').val());
        aduaData.push('b1');
        console.log(aduaData);
        setAgenciaAduana(aduaData);

    });

    $('#modal-aduanas-btn-limpiar').click(function () {
        limpiarModalAduana();
    });
    $('#modal-aduanas-btn-cerrar').click(function () {
        limpiarModalAduana();
    });

    function limpiarModalAduana() {
        $('#modal_aduanas input').val('');
    }
    /*MODAL AGENCIA ADUANA*/

    $('#table-agencia-aduanera').on('click', 'tr td a', function (evt) {
        var id = $(this).parents('tr').eq(0).find('input').val();
        var name = $(this).parents('tr').eq(0).find('label').text();
        $('#aduana-input-nombre').val(name);
        $('#aduana-input-agencia_id').val(id);
    });

    function setAgenciaAduana(agenciaData) {
        $.ajax({
            data: {'array': JSON.stringify(agenciaData)},
            url: '../ajax/AgenciaAduana/setAgenciaAndGetRow.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json) {
                    limpiarModalAduana();
                    addAgenciaAduana(json);
                    $('#modal_aduanas').modal('hide');
                    alerta.alerSuccess('Se ha creado nueva agencia de aduana');
                } else {
                    alerta.alerInfo('No se pudo crear agencia de aduana');
                }
            },
            error: function (err) {
                console.log(err)
                alerta.alerError('Error al crear agencia de aguana<br>Reporte este error!!');
            }
        });
    }

    function addAgenciaAduana(json) {
        var t = $('#table-agencia-aduanera').DataTable();
        var info = t.page.info();
        var count = info.recordsTotal;
        console.log(count);
        if(json){
            t.row.add([
                '<div class="text-left">' +json.age_adua_cod+'</div>',
                '<div class="text-left">' +
                    '<label id="num_ruc">'+json.age_adua_nombre+'</label>' +
                '</div>',
                '<div class="text-left">'+json.age_adua_juris+'</div>',
                '<div class="text-left">'+json.age_adua_cod+'</div>',
                '<div class="text-center">' +
                    '<a id="btn-"  class="btn btn-sm btn-danger fa fa-check btn-option" title="Anadir item" data-dismiss="modal"></a>' +
                    '<input class="emp_id no-display" type="text" value="' + json.age_adua_id + '">' +
                '</div>'
            ]).draw(false);
        }
    }


    /*-----------------------------------------------------AJAX LISTA DE AGENCIAS----------------------------------------------------------*/






});
