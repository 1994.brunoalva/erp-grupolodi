import alerta from "../alertToas.js";
import images from "../Myjs/Images.js";

$(document).ready(function () {

    /*MODAL AGREGAR PRODUCTOS*/
    var nombre ='';
    $('#modal-producto-btn-guardar').click(function () {

        //get_existe_nombre
        $.ajax({
            type: "POST",
            url: "../ajax/Producto/get_existe_nombre.php",
            data: {nombre:$("#modal-producto-input-nombre").val()},
            success: function (res) {
                console.log(res)
                const jsm= JSON.parse(res);
                if (!jsm.res){
                    var detalleData = [];
                    detalleData.push('');                                               /*0*/
                    /*CAMARA*/
                    detalleData.push($('#modal-producto-input-cam-medida').val());      /*1*/
                    detalleData.push($('#modal-producto-input-cam-aro').val());         /*2*/
                    detalleData.push($('#modal-producto-input-cam-valvula').val());     /*3*/
                    /*CAMARA*/

                    /*AROS*/
                    detalleData.push($('#modal-producto-input-aro-modelo').val());      /*4*/
                    detalleData.push($('#modal-producto-input-aro-medida').val());      /*5*/
                    detalleData.push($('#modal-producto-input-aro-espesor').val());     /*6*/
                    detalleData.push($('#modal-producto-input-aro-num-huecos').val());  /*7*/
                    detalleData.push($('#modal-producto-input-aro-espesor-hueco').val());/*8*/
                    detalleData.push($('#modal-producto-input-aro-cbd').val());         /*9*/
                    detalleData.push($('#modal-producto-input-aro-pcd').val());         /*10*/
                    detalleData.push($('#modal-producto-input-aro-offset').val());      /*11*/
                    /*AROS*/

                    /*NEUMATICOS*/
                    detalleData.push($('#modal-producto-input-neu-ancho').val());       /*12*/
                    detalleData.push($('#modal-producto-input-neu-serie').val());       /*13*/
                    detalleData.push($('#modal-producto-input-neu-aro').val());         /*14*/
                    detalleData.push($('#modal-producto-input-neu-pliege').val());      /*15*/
                    detalleData.push($('#select_uso').find('option:selected').text()=='-Seleccione-'?'':$('#select_uso').find('option:selected').text());  /*16*/


                    detalleData.push(' ');/*17*/    /*detalleData.push($('#modal-producto-input-neu-set').val());*/
                    detalleData.push($('#modal-producto-input-neu-mate').find('option:selected').text()=='-Seleccione-'?'':$('#modal-producto-input-neu-mate').find('option:selected').text());
                    detalleData.push($('#modal-producto-input-neu-ancho-adua').val());
                    detalleData.push($('#modal-producto-input-neu-serie-adua').val());
                    detalleData.push($('#modal-producto-input-neu-serie-tipo-cons').find('option:selected').text()=='-Seleccione-'?'':$('#modal-producto-input-neu-serie-tipo-cons').find('option:selected').text());
                    detalleData.push(' ');/*22*/    /*detalleData.push($('#modal-producto-input-neu-carga').val());*/
                    detalleData.push(' ');/*23*/    /*detalleData.push($('#modal-producto-input-neu-pisa').val());*/
                    detalleData.push(' ');/*24*/    /*detalleData.push($('#modal-producto-input-neu-externo').val());*/
                    detalleData.push($('#modal-producto-input-neu-veloci').find('option:selected').text()=='-Seleccione-'?'':$('#modal-producto-input-neu-veloci').find('option:selected').text()); /*25*/    /*detalleData.push($('#modal-producto-input-neu-veloci').val());*/
                    detalleData.push($('#modal-producto-input-neu-consta').val());/*26*/    /**/
                    detalleData.push($('#modal-producto-input-neu-item').val());/*27*/    /**/
                    detalleData.push($('#modal-producto-input-neu-vigencia').val());/*28*/    /**/
                    detalleData.push($('#modal-producto-input-neu-confor').val());/*29*/    /**/
                    detalleData.push($('#modal-producto-input-neu-indice-carga').val());/*29*/    /**/
                    detalleData.push($('#modal-producto-input-neu-suce').val());/*29*/    /**/


                    detalleData.push($('#modal-producto-input-neu-parti').val());        /*30*/


                    detalleData.push($('#modal-producto-input-produ-medida').val());/*31*/
                    detalleData.push('b1');/*32*/

                    console.log('detalleData');
                    console.log(detalleData);
                    /*NEUMATICOS*/
                    /*    setDetalleProducto(detalleData);*/


                    var c = $('#select_modal_pro_categoria').val();
                    var m = $('#select_modal_prod_marca').val();
                    var mar = $('#select_modal_prod_marca option:selected').text();
                    var mo = $('#select_modal_prod_modelo').val();
                    var mode = $('#select_modal_prod_modelo option:selected').text();

                    var co = $('#modal-producto-input-codigo-sunat-id').val();
                    var sk = $('#modal-producto-input-sku').val();
                    var me = $('#select_modal_medida').val();
                    var pn = $('#modal-producto-input-pn').val();
                    var pa = $('#select_prod_pais').val();
                    var pai = $('#select_prod_pais option:selected').val();

                    var sTCAT = (c==7);


                    if (c > 0 && m > 0 ||sTCAT) {
                        //console.log(co + ' - ' + sk + ' - ' + me + ' - ' + de + ' - ' + pn + ' - ' + pa);
                        if ( me > 0 ) {
                            var ti = $('#select_opc').val();
                            var tip = $('#select_opc option:selected').val();
                            setDetalleProducto(detalleData);
                           /* if (ti > 0) {
                                var no = $('#select_nomenglatura').val();
                                if (cate === 'NEUMATICOS') {
                                    //console.log('ES NEUMATICOS ESTO'+ cate);
                                    if (no > 0) {
                                        var nanc = $('#modal-producto-input-neu-ancho').val();
                                        var nser = $('#modal-producto-input-neu-serie').val();
                                        var naro = $('#modal-producto-input-neu-aro').val();
                                        var npli = $('#modal-producto-input-neu-pliege').val();
                                        var nuso = $('#select_modal_uso').val();
                                        var uso = $('#select_modal_uso option:selected').text();
                                        uso =uso.substr(0, 3);
                                        if (nanc !== '' && nser !== '' && naro !== '' && npli !== '' && nuso > 0) {
                                            console.log('ES NEUMATICOS ESTO'+ cate);
                                            alerta.alerSuccess('guardando datos NEUMATICO');
                                            //nombre=cate+' - '+mar+' - '+uso+' - '+mode+' ('+naro+' - '+nanc+')';
                                            setDetalleProducto(detalleData);
                                        } else {
                                            alerta.alerWar('ingrese los datos del nuevo producto');
                                            console.log('-n4');
                                        }
                                    } else {
                                        alerta.alerWar('ingrese los datos del nuevo producto NEUMATICO');
                                        console.log('-n3');
                                    }
                                }
                                if (cate === 'CAMARAS') {
                                    console.log('ES CAMARAS ESTO'+ cate);
                                    var cmed = $('#modal-producto-input-cam-medida').val();
                                    var caro = $('#modal-producto-input-cam-aro').val();
                                    var cval = $('#modal-producto-input-cam-valvula').val();
                                    if (cmed !== '' && caro !== '' && cval !== '') {
                                        console.log('ES CAMARAS ESTO'+ cate);
                                        //nombre=cate+' - '+mar+' - '+uso+' - '+mode+'('+cmed+' - '+caro+' - '+cval+')';
                                        //nombre=cate+' - '+mar+'('+cmed+' - '+caro+' - '+cval+')';
                                        alerta.alerSuccess('guardando datos CAMARA');
                                        setDetalleProducto(detalleData);
                                    } else {
                                        alerta.alerWar('ingrese los datos del nuevo producto CAMARA');
                                        console.log('-c4');
                                    }
                                }
                                if (cate === 'AROS') {
                                    var amo = $('#modal-producto-input-aro-modelo').val();
                                    var ame = $('#modal-producto-input-aro-medida').val();
                                    var aes = $('#modal-producto-input-aro-espesor').val();
                                    var anhu = $('#modal-producto-input-aro-num-huecos').val();
                                    var aesh = $('#modal-producto-input-aro-espesor-hueco').val();
                                    var acbd = $('#modal-producto-input-aro-cbd').val();
                                    var apcd = $('#modal-producto-input-aro-pcd').val();
                                    var aose = $('#modal-producto-input-aro-offset').val();
                                    console.log('ES AROS ESTO'+ cate);
                                    if (amo !== '' && ame !== '' && aes !== ''&&anhu!== '' &&aesh!== '' &&acbd!== '' &&apcd!== '' && aose) {
                                        console.log('ES AROS ESTO'+ cate);
                                       // nombre=cate+' '+mar+' - '+tip+' - '+mode+'('+ame+' - '+aes+' - '+anhu+')';
                                        alerta.alerSuccess('guardando datos AROS');
                                        setDetalleProducto(detalleData);
                                    } else {
                                        alerta.alerWar('ingrese los datos del nuevo producto AROS');
                                        console.log('-c4');
                                    }
                                }

                            }

                            if (cate === 'PROTECTORES') {
                                console.log('ES PROTECTORES ESTO'+ cate);
                                alerta.alerSuccess('guardando datos PROTECTORES');
                                //nombre=cate+' - '+mar+' - '+tip+' - '+mode+' - '+de+' - '+pai;
                                setDetalleProducto(detalleData);
                            }
                            if (cate === 'ACCESORIOS') {
                                console.log('ES ACCESORIOS ESTO'+ cate);
                                //nombre=cate+' - '+mar+' - '+tip+' - '+mode+' - '+de+' - '+pai;
                                alerta.alerSuccess('guardando datos ACCESORIOS');
                                setDetalleProducto(detalleData);
                            }*/
                        } else {
                            alerta.alerWar('ingrese los datos del nuevo producto');
                            console.log('-1');
                        }
                    } else {
                        alerta.alerWar('Seleccione marca y modelo del producto');
                    }
                }else{
                    $.toast({
                        heading: 'ALERTA',
                        text: "Este producto ya existe",
                        icon: 'warning',
                        position: 'top-right',
                        hideAfter: '2500',
                    });
                }

            }
        });






    });
    var cate = '';
    $('#select_modal_pro_categoria').change(function () {
        cate = $(this).find('option:selected').text();
        $('#modal-producto-btn-limpiar').click();
    });


    $('#modal-producto-btn-cerrar').click(function () {
        $('#modal_producto').modal('hide');
        $('#select_modal_pro_categoria').val('0').change();
        $('#modal-producto-btn-limpiar').click();
    });

    function setDetalleProducto(detalleData) {
        $.ajax({
            data: {'array': JSON.stringify(detalleData)},
            url: '../ajax/DetalleProducto/setDetalleAndGetRow.php',
            type: 'POST',
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log('RESPUESTA DE DETALLE');
                console.log(response);
                if (json) {

                    var productoData = []
                    productoData.push('');/*0*/
                    productoData.push('');/*0*/
                    productoData.push(json);/*1*/
                    productoData.push('');/*2*/
                    productoData.push($('#select_modal_medida').val());/*3*/
                    productoData.push($('#modal-producto-input-codigo-sunat-id').val());/*4*/
                    productoData.push($('#modal-producto-input-desc').val());/*5*/
                    productoData.push($('#modal-producto-input-sku').val());/*6*/
                    productoData.push($('#modal-producto-input-pn').val());/*7*/
                    var o = $('#select_opc').val();
                    var n = $('#select_nomenglatura').val();
                    if (o === '0') {
                        productoData.push('');
                    } else {
                        productoData.push($('#select_opc').val());/*8*/
                    }
                    if (n === '0') {
                        productoData.push('');
                    } else {
                        productoData.push($('#select_nomenglatura').val());/*9*/
                    }
                    productoData.push($('#select_modal_pro_categoria').val());/*10*/
                    productoData.push($('#select_modal_prod_marca').val()===null?'159':$('#select_modal_prod_marca').val());/*11*/
                    var modelo= $('#select_modal_prod_modelo').val();
                    productoData.push(modelo==null? '1':modelo);/*12*/
                    productoData.push($('#select_prod_pais').val()=='0'?'348':$('#select_prod_pais').val());/*13*/
                    //productoData.push(nombre);/*14*/
                    productoData.push($("#modal-producto-input-nombre").val());/*14*/

                    productoData.push('n');/*15*/
                    productoData.push('0');/*15*/

                    console.log('productoData');
                    console.log(productoData);
                    setProducto(productoData);

                    alerta.alerSuccess('Se ha guardado el detalle');

                } else {
                    alerta.alerInfo('No se pudo guardar el detalle, porfavor ingrese todos los campos necesarios');
                }
            },
            error: function (err) {
                console.log(err)
                alerta.alerError('Error al guardado el detalle<br>Reporte este error!!');
            }
        });
    }

    function setProducto(productoData) {
        $.ajax({
            data: {'array': JSON.stringify(productoData)},
            url: '../ajax/Producto/setProductoAndGetRow.php',
            type: 'POST',
            success: function (response) {
                console.log('RESPUESTA DEL PRODUCTO');
                console.log(response);
                var json = JSON.parse(JSON.stringify(response));




                if (json) {
                    //addProducto(json);
                    /*$('#modal-producto-btn-cerrar').click();*/
                    $('#modal-producto-btn-limpiar').click();
                    $('#modal-producto-btn-cerrar').click();
                    alerta.alerSuccess('Se ha guardado el producto');
                } else {
                    alerta.alerInfo('No se pudo guardar el producto, porfavor ingrese todos los campos necesarios');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText)
                alerta.alerError('Error al guardado el producto<br>Reporte este error!!');
            }
        });
    }

    function addProducto(json) {
        var t = $('#table-productos').DataTable();
        var info = t.page.info();
        var count = info.recordsTotal;
        console.log(count);
        console.log(json);
        if (json) {
            t.row.add([
                '<div class="text-center">' + (++count) + '</div>',
                '<div class="text-left">' +
                    '<a id="btn-sku" href="#">'+ json.produ_sku +'</a>' +
                '</div>',
                '<div class="text-left">' +
                    '<label>' +
                        '<a id="btn-nombre" href="#">'+ json.produ_nombre +'</a>' +
                    '</label>' +
                '</div>',
                '<div id="btn-cat"  class="text-left">' + json.cat_id.cat_nombre + '</div>',
                '<div class="text-left">' + json.mar_id.mar_nombre + '</div>',
                '<div class="text-left">' + json.mod_id.mod_nombre + '</div>',
                '<div class="text-left">' +
                        json.pais_id.pais_nombre +
                        '<a id="med-data" class="no-display" href="#">'+ json.unidad_id.unidad_id +'</a>' +
                '</div>',
                '<div class="text-center">' +
                    '<a id="btn-a" class="btn btn-sm btn-danger fa fa-check btn-option" title="Anadir item" data-dismiss="modal"></a>' +
                    '<input class="emp_id no-display" type="text" value="' + json.produ_id + '">' +
                '</div>'
            ]).draw(false);
        }
    }

    /*MODAL AGREGAR PRODUCTOS*/


    /*   function validateInput(){
           var indice= 0;
           $('#modal_producto input').each(function(){
               var data = $(this).val('');
               console.log('data');
               console.log(data);
               if(data ===''){
                  /!* ++indice;*!/
               }
               ++indice;
           });
           console.log('--------------indice------------------');
           console.log(indice);
       }*/


    $('#select_modal_pro_categoria').change(function () {
        var categoria = $(this).find('option:selected').text();
        if (categoria === 'NEUMATICOS' || categoria === 'CAMARAS') {
            $('.box-opc').show();
            $('#select_opc option').each(function () {
                var valor = $(this).val();
                $('#select_opc option').eq(0).show();
                if (valor > 0 && valor < 3) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });

        } else if (categoria === 'AROS') {
            $('.box-opc').show();
            $('#select_opc option').each(function () {
                var valor = $(this).val();
                $('#select_opc option').eq(0).show();
                if (valor > 0 && valor < 3) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
                if (valor==8){
                    $(this).hide();
                }
            });
        } else {
            $('.box-opc').hide();
        }

        $('#select_opc').val('0').change();
        $('#select_opc').selectpicker('refresh');
    });

    function clearImputFilter() {
        $('.' + filter).val('');
        $('.' + filter).keyup();
    }

});