import alerta from "../alertToas.js";


$(document).ready(function () {

    var p =[];

    var nom_cate='';

    $('#table-productos').on('click', 'tr td label', function (evt) {
        var nombre_prod = $(this).parents('tr').eq(0).find('#btn-nombre').text();
        var id = $(this).parents('tr').eq(0).find('input').val();
        nom_cate = $(this).parents('tr').eq(0).find('#btn-cat').text();
        getProducto(id);
        /*alert(nombre_prod);*/
        console.log(nombre_prod);
        console.log(id);
        console.log(nom_cate);
        var title = 'Detalle de ';


        if(nom_cate==='NEUMATICOS'){
            title +='Neumatico';
            $('.text-title-producto').text(title);
        }
        if(nom_cate==='CAMARAS'){
            title+='Camara';
            $('.text-title-producto').text(title);
        }
        if(nom_cate==='AROS'){
            title+='Aros';
            $('.text-title-producto').text(title);
        }
        if(nom_cate==='PROTECTORES'){
            title+='Protectores';
            $('.text-title-producto').text(title);
        }
        if(nom_cate==='ACCESORIOS'){
            title+='Accesorios';
            $('.text-title-producto').text(title);
        }

        setTimeout(cargarModal,500);


    });

    function cargarModal() {
        console.log(p[0].cat_id.cat_nombre);
        $('#p-marca').val(p[0].cat_id.cat_nombre);
        $('#p-modelo').val(p[0].mod_id.mod_nombre);
        $('#p-nombre').val(p[0].produ_nombre);
        $('#p-sku').val(p[0].produ_sku);
        $('#p-cod').val(p[0].sunat_cod_id.sunat_cod_codigo);
        $('#p-cod').prop('title',p[0].sunat_cod_id.sunat_cod_codigo+' - '+p[0].sunat_cod_id.sunat_cod_desc);
        $('#p-unidad').val(p[0].unidad_id.unidad_nombre);
        $('#p-desc').val(p[0].produ_desc);
        $('#p-pn').val(p[0].produ_pn);
        $('#p-pais').val(p[0].pais_id.pais_nombre);



        $('#p_c').find('.n1').remove();
        if(nom_cate==='NEUMATICOS'){
            $('#p_c').append(getHtml('Nomenglatura',p[0].nom_id.nom_nombre,'1','6','4'));
            $('#p_c').append(getHtml('Ancho',p[0].produ_deta_id.produ_neu_ancho_interno,'1','4','3'));
            $('#p_c').append(getHtml('Serie',p[0].produ_deta_id.produ_neu_serie,'1','4','3'));
            $('#p_c').append(getHtml('Pliegue',p[0].produ_deta_id.produ_neu_pliegue,'1','4','2'));
            $('#p_c').append(getHtml('Uso Comercial',p[0].produ_deta_id.produ_neu_uso_comercial,'1','12','12'));
        }
        if(nom_cate==='CAMARAS'){
            $('#p_c').append(getHtml('Ancho',p[0].produ_deta_id.produ_cam_ancho,'1','6','4'));
            $('#p_c').append(getHtml('# Aro',p[0].produ_deta_id.produ_cam_aro,'1','6','4'));
            $('#p_c').append(getHtml('Tipo Valvula',p[0].produ_deta_id.produ_cam_valvula,'1','6','4'));
        }
        if(nom_cate==='AROS'){
            $('#p_c').append(getHtml('Ancho',p[0].produ_deta_id.produ_cam_ancho,'1','6','4'));
            $('#p_c').append(getHtml('# Aro',p[0].produ_deta_id.produ_cam_aro,'1','6','4'));
            $('#p_c').append(getHtml('Tipo Valvula',p[0].produ_deta_id.produ_cam_valvula,'1','6','4'));
        }


        $('#modal_neumatico').modal('show');
    }
    function getHtml(t,dato,i,a,x) {
        var cod ='<div class="form-group col-xs-'+a+' col-sm-'+x+' n'+i+'">' +
                    '<label class="col-xs-12 no-padding">'+t+' :</label>' +
                    '<input disabled type="text" class="form-control col-xs-12" value="'+dato+'">'+
                '</div>';
        return cod;
    }

    function getProducto(id) {
        $.ajax({
            data: {id},
            url: '../ajax/Producto/getProductoForId.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                p=[];
                p.push(json);
                /*console.log(p[0].mar_id.mar_nombre);*/
                if (json) {
                    alerta.alerSuccess('Cargando datos del prodcuto');

                } else {
                    alerta.alerInfo('no se encontraron datos de este producto');
                }
            },
            error: function () {
                alerta.alerError('Error al cargar datos de este producto<br>Reporte este error!!');
            }
        });
    }

});