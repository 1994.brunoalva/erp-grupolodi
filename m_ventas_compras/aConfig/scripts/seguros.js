import alerta from "../alertToas.js";

$(document).ready(function () {


    setTimeout(readSeguro,500);
    function readSeguro(){
        var qty=$('#folio-folder-qty').val();
        var fto=$('#orden-input-fob-total').val();
        var tfl=$('#orden-input-flete-contenedor').val();
        fto=fto.replace(/,/g, '');
        tfl=tfl.replace(/,/g, '');

        if(fto==='')
        {
            fto='0.00';
        }
        if(tfl==='')
        {
            tfl='0.00';
        }
        if(parseFloat(tfl)>0){
            tfl=parseFloat(tfl)*qty;
            $('#seguro-input-total-flete').val(tfl.toFixed(2));
        }else{
            $('#seguro-input-total-flete').val(tfl);
        }
        $('#seguro-input-total-fob').val(fto);

        TransformDecimal('seguro-input-total-flete');
        TransformDecimal('seguro-input-total-fob');

    }


    $('#seguro-btn-guardar').click(function () {
        var dataPoliza =[];

        dataPoliza.push(['pol_id',$('#seguro-input-poliza-id').val()]);
        dataPoliza.push(['pol_suma_aseg',$('#seguro-input-suma-asegurada').val()]);
        dataPoliza.push(['pol_referencia',$('#seguro-input-referencia').val()]);
        dataPoliza.push(['pol_vig_desde',$('#seguro-input-vigencia-desde').val()]);
        dataPoliza.push(['pol_vig_hasta',$('#seguro-input-vigencia-hasta').val()]);
        dataPoliza.push(['poliza_id',$('#seguro-input-poliza').val()]);
        dataPoliza.push(['pol_aplicacion',$('#seguro-input-aplicacion').val()]);
        dataPoliza.push(['tasa_poliza_id',$('#select_tasa').val()]);
        dataPoliza.push(['pol_prima_neta',$('#seguro-input-prima-neta').val()]);
        dataPoliza.push(['tasa_camb_id',idUtlimaTasaCambio]);
        dataPoliza.push(['pol_prima_total',$('#seguro-input-prima-total').val()]);
        dataPoliza.push(['pol_cif',$('#seguro-input-monto-total').val()]);
        dataPoliza.push(['impor_id',$('#seguro-input-folder-id').val()]);
        dataPoliza.push(['estado','b1']);
       /* console.log(dataPoliza);
        varlidarForm('form-seguro');
        if(numVacio===1){
            runAjaxArray(dataPoliza,
                'Poliza',
                'updatePoliza',
                'Poliza',
                updateSeguro);
        }else{
            alerta.alerInfo('Complete todos los campos.');
        }*/


            runAjaxArray(dataPoliza,
                'Poliza',
                'updatePoliza',
                'Poliza',
                updateSeguro);


    });
    var numVacio =0;
    function varlidarForm(ele) {
        numVacio=0;
        $('#'+ele+' input').each(function () {
            var vacio= $(this).val();
            if(vacio===''){
                ++numVacio;
            }
            if(vacio==='0.00'){
                ++numVacio;
            }
        });
    }
    function runAjaxArray(array, file, method, title, func) {
        $.ajax({
            data: {'array': JSON.stringify(array)},
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json) {
                    func(json);
                } else {
                    /*clearSelect(puerto);*/
                }
            },
            error: function () {
                alerta.alerInfo('No se pudo cargar ' + title + '<br>Reporte este error!!');
            }
        });
    }
    function updateSeguro(json) {
        alerta.alerSuccess('Se ha Guardado El seguro.');
    }


    $('#seguro-btn-limpiar').click(function () {
        limpiar();
    });
    function limpiar() {
        $('#box-input-seguro input[type=text]').each(function () {
            $(this).val('');
        });
        $('#box-input-seguro select').val(0).change();
    }



    $('#select_tasa').change(function () {
        var index = $(this).val();
        var sas = $('#seguro-input-suma-asegurada').val();
        var costo_ad = $('#orden-input-cost-adicional').val();
        if(index>0&&sas.length>0){

            sas = sas.replace(/,/g, '');
            costo_ad = costo_ad.replace(/,/g, '');
            if (costo_ad==""){
                costo_ad='0';
            }

            var por =$(this).find('option:selected').text();
            por= por.substring(0,por.length-2);
            por=por/100;
            por=por.toFixed(4);
            var primaNeta = parseFloat(sas)*(por);

            $('#seguro-input-prima-neta').val(primaNeta.toFixed(2));

            por = 3/100 ;
            var primaTotal = ((primaNeta*por)+primaNeta)+(((primaNeta*por)+primaNeta)*0.18);

            $('#seguro-input-prima-total').val(primaTotal.toFixed(2));

            var mt = parseFloat(sas)  + parseFloat(primaNeta) + parseFloat(costo_ad);
            /*console.log();*/
            $('#seguro-input-monto-total').val(mt.toFixed(2));

            TransformDecimal('seguro-input-monto-total');
        }else{
            $('#seguro-input-prima-neta').val('');
            $('#seguro-input-prima-total').val('');
            $('#seguro-input-monto-total').val('');
        }







    });






    var index = 0;
    function TransformDecimal(ele) {
        var numero ='';
        var moneyText = '';
        moneyText =  $('#'+ele).val();

        var decimal='';
        index = moneyText.indexOf('.');
        decimal='.'+moneyText.substring(index+1);
        moneyText=moneyText.replace(/,/g, '');
        moneyText= moneyText.substring(0, index);
        /*console.log('moneyText');
        console.log(moneyText);*/
        numero='';
        moneyText = moneyText.split('').reverse().join('');
        for (let i = moneyText.length-1; i >= 0; --i) {
            numero += moneyText.charAt(i);
            if(i%3===0&&i>0){
                numero += ',';
            }
        }
        $('#'+ele).val(numero+decimal);
    }
});