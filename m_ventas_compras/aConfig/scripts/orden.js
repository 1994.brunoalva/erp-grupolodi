import alerta from "../alertToas.js";

$(document).ready(function () {
    var ruc='';
    var filter='';
    $('#select_nave').prop('disabled', true);

    var linea_id = $('#orden-input-linea-id').val();
    if(linea_id!==''){
        $('#select_nave').prop('disabled', false);
        $('#orden-btn-modal-nave').prop('disabled', false);

    }

    $('#orden-btn-guardar').click(function () {
        var ord_id = $('#orden-input-orden-id').val();
        searchEmbarque(ord_id);
    });

    var updateOrdenData=[];
    var setOrdenData=[];

    $('#orden-input-flete-contenedor').focusout(function () {
        var fob_total = $('#orden-input-fob-total').val();
        var flete =$('#orden-input-flete-contenedor').val();
        flete =flete.replace(/,/g, '');
        fob_total =fob_total.replace(/,/g, '');
        if(flete===''){
            flete=0.00;
        }
        var conqty = $('#folio-folder-qty').val();
        if(conqty===''){
            conqty=0.00;
        }
        fob_total = (parseFloat(flete) * conqty) + parseFloat(fob_total);
        $('#orden-input-total-flete').val(fob_total.toFixed(2));
       /* TransformDecimal('orden-input-fob-total');*/
        TransformDecimal('orden-input-total-flete');


        var f=parseFloat(flete) * conqty;
        $('#seguro-input-total-flete').val(f.toFixed(2));

        $('#seguro-input-suma-asegurada').val(fob_total.toFixed(2));

        console.log(fob_total.toFixed(2));
        TransformDecimal('seguro-input-total-flete');
        TransformDecimal('seguro-input-suma-asegurada');
        $('#select_tasa').val(0).change();
    });





    function searchEmbarque(id) {
        $.ajax({
            data: {id},
            url: '../ajax/Embarque/getForID.php',
            type: 'POST',
            /*async: false,*/
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json.length!==0) {
                    updateOrdenData.push(['emb_id',$('#orden-input-embarque-id').val()]);
                    updateOrdenData.push(['ord_id',$('#orden-input-orden-id').val()]);
                    updateOrdenData.push(['emb_factura',$('#orden-input-fecha-factura').val()]);
                    updateOrdenData.push(['emb_fech_factura',$('#orden-input-fecha-factura').val()]);
                    updateOrdenData.push(['emb_monto',$('#orden-input-fob-total').val()]);
                    updateOrdenData.push(['emb_flete',$('#orden-input-flete-contenedor').val()]);
                    updateOrdenData.push(['emb_thc',$('#orden-input-thc-contenedor').val()]);
                    updateOrdenData.push(['emb_total_flete',$('#orden-input-total-flete').val()]);
                    updateOrdenData.push(['emb_serie_bl',$('#orden-input-orden-bl').val()]);
                    updateOrdenData.push(['forwa_id',$('#orden-input-Forwarder-id').val()]);
                    updateOrdenData.push(['desp_id',$('#select_despacho').val()]);
                    updateOrdenData.push(['linea_id',$('#orden-input-linea-id').val()]);
                    updateOrdenData.push(['nave_id',$('#select_nave').val()]);
                    updateOrdenData.push(['emb_etd',$('#orde-input-etd').val()]);
                    updateOrdenData.push(['emb_eta',$('#orde-input-eta').val()]);
                    updateOrdenData.push(['emb_fech_sobre',$('#orde-input-esta').val()]);
                    updateOrdenData.push(['alm_id',$('#orden-input-almacen-id').val()]);
                    updateOrdenData.push(['estado','b1']);
                    console.log(updateOrdenData);
                    updateEmbarque(updateOrdenData);
                } else {
                    setOrdenData.push('');
                    setOrdenData.push($('#orden-input-orden-id').val());
                    setOrdenData.push($('#orden-input-serie-factura').val());
                    setOrdenData.push($('#orden-input-fecha-factura').val());
                    setOrdenData.push($('#orden-input-fob-total').val());
                    setOrdenData.push($('#orden-input-flete-contenedor').val());
                    setOrdenData.push($('#orden-input-thc-contenedor').val());
                    setOrdenData.push($('#orden-input-total-flete').val());
                    setOrdenData.push($('#orden-input-orden-bl').val());
                    setOrdenData.push($('#orden-input-Forwarder-id').val());
                    setOrdenData.push($('#select_despacho').val());
                    setOrdenData.push($('#orden-input-linea-id').val());
                    setOrdenData.push($('#select_nave').val());
                    setOrdenData.push($('#orde-input-etd').val());
                    setOrdenData.push($('#orde-input-eta').val());
                    setOrdenData.push($('#orde-input-esta').val());
                    setOrdenData.push($('#orden-input-almacen-id').val());
                    setOrdenData.push('b1');
                    console.log(setOrdenData);
                    setEmbarque(setOrdenData);

                }
                updateOrdenData=[];
                setOrdenData=[];
            },
            error: function () {
                alerta.alerError('Error al Buscar Embarque<br>Reporte este error!!');
            }
        });
    }
    function setEmbarque(setOrdenData) {
        $.ajax({
            data: {'array': JSON.stringify(setOrdenData)},
            url: '../ajax/Embarque/setEmbarque.php',
            type: 'POST',
            /*async: true,*/
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json) {
                    alerta.alerSuccess('Se ha guardado Embarque');
                } else {
                    alerta.alerInfo('No se pudo guardar Embarque asegurese de completar todos los campos');
                }
            },
            error: function () {
                alerta.alerError('Error al guardar Embarque<br>Reporte este error!!');
            }
        });
    }
    function updateEmbarque(updateOrdenData) {
        $.ajax({
            data: {'array': JSON.stringify(updateOrdenData)},
            url: '../ajax/Embarque/updateEmbarque.php',
            type: 'POST',
           /* async: true,*/
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json) {
                    alerta.alerSuccess('Se ha actualizado Embarque');
                } else {
                    alerta.alerInfo('No se pudo actualizar Embarque asegurese de completar todos los campos');
                }
            },
            error: function () {
                alerta.alerError('Error al actualizar Embarque<br>Reporte este error!!');
            }
        });
    }


















    /*----------------------------------------------------------AJAX LISTA DE ALMACEN----------------------------------------------------------*/
    /*MODAL BUSCAR ALMACEN*/
    $('#orden-input-almacen').focus(function () {
        $('#modal_buscar_almacen').modal('show');
    });
    $('.input-search-alm').focus(function () {
        $('.input-search-alm').keyup(function () {
            var letra = $('.input-search-alm').val();
            $('#table-almacen_filter label input').val(letra);
            $('#table-almacen_filter label input').keyup();
        });
    });
    $('#table-almacen').on('click', 'tr td a', function (evt) {
        var id = $(this).parents('tr').eq(0).find('input').val();
        var name = $(this).parents('tr').eq(0).find('label').text();
        EMBARQUE._data.listaEmbarque[EMBARQUE._data.indexareb].idalmacen=id;
        EMBARQUE._data.listaEmbarque[EMBARQUE._data.indexareb].nomalmacen=name;

        $('#orden-input-almacen').val(name);
        $('#orden-input-almacen-id').val(id);
        setTimeout(clearImputFilter,200);
        filter='input-search-alm';
    });
    /*MODAL BUSCAR ALMACEN*/

    /*MODAL ALMACEN*/

    $('#modal-almacen-btn-buscar').click(function () {
        ruc =$('#modal-almacen-input-ruc').val();
        $('body').css('cursor', 'wait');
        buscarRucAlmacen();
    });

    $('#modal-almacen-btn-guardar').click(function () {
        var almacenData = new Array();
        almacenData.push('');
        almacenData.push($('#modal-almacen-input-ruc').val());
        almacenData.push($('#modal-almacen-input-razon-social').val());
        almacenData.push($('#modal-almacen-input-juris').val());
        almacenData.push($('#modal-almacen-input-represen').val());
        almacenData.push($('#modal-almacen-input-cargo').val());
        almacenData.push($('#modal-almacen-input-direccion').val());
        almacenData.push($('#modal-almacen-input-telefono-1').val());
        almacenData.push($('#modal-almacen-input-telefono-2').val());
        almacenData.push('b1');
        console.log(almacenData);
        setAlmacen(almacenData);

    });
    $('#modal-almacen-btn-limpiar').click(function () {
        limpiarModalAlmacen();
    });
    $('#modal-almacen-btn-cerrar').click(function () {
        limpiarModalAlmacen();
    });

    function limpiarModalAlmacen() {
        $('#modal_almacen input').val('');
    }
    function setAlmacen(almacenData) {
        $.ajax({
            data: {'array': JSON.stringify(almacenData)},
            url: '../ajax/Almacen/setAlmacenAndGetRow.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json.length!==0) {
                    console.log('AGREGANDO ALMACEN');
                    addAlmacen(json);
                    $('#modal_almacen').modal('hide');
                    limpiarModalAlmacen();
                    alerta.alerSuccess('Se ha creado nueva Almacen');
                } else {
                    alerta.alerInfo('No se pudo crear Almacen ingrese todos los campos');
                }
            },
            error: function () {
                alerta.alerError('Error al crear Almacen<br>Reporte este error!!');
            }
        });
    }
    function addAlmacen(json) {
        var t = $('#table-almacen').DataTable();
        var info = t.page.info();
        var count = info.recordsTotal;
        console.log(count);
        if(json){
            t.row.add([
                '<div class="text-center">'+(++count)+'</div>',
                '<div class="text-left">' +
                    '<label id="num_for">'+json.alm_razon_social+'</label>' +
                '</div>',
                '<div class="text-left">' +json.alm_rep+'</div>',
                '<div class="text-left">' +json.alm_dir+'</div>',
                '<div class="text-center">'+
                    '<a id="btn-"  class="btn btn-sm btn-danger fa fa-check btn-option" title="Anadir item" data-dismiss="modal"></a>' +
                    '<input class="alm_id no-display" type="text" value="' + json.alm_id + '">' +
                '</div>'
            ]).draw(false);
        }
    }
    function buscarRucAlmacen() {
        $.ajax({
            data: {ruc},
            url: '../ajax/libSunat/sunat/example/consultaRuc.php',
            type: 'POST',
            /* async: false,*/
            beforeSend: function() {
                alerta.alerInfo('Buscando RUC en Sunat, Porfavor espere ..');
            },
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.success) {
                    console.log(json);
                    $('body').css('cursor', 'default');
                    $('#modal-almacen-input-razon-social').val(json.result.razon_social);
                    $('#modal-almacen-input-direccion').val(json.result.direccion);
                    $(json.result.representantes_legales).each(function (i,v) {
                        if(v.cargo ==='GERENTE GENERAL'){
                            $('#modal-almacen-input-represen').val(v.nombre);
                            $('#modal-almacen-input-cargo').val(v.cargo);
                        }
                    });
                    /*$('#modal-almacen-input-represen').val(json.result.representantes_legales[]);*/
                    alerta.alerSuccess('Cargando Datos de empresa');
                } else {
                    $('body').css('cursor', 'default');
                    alerta.alerWar(json.message);
                }

            },
            error: function () {
                alerta.alerError('Error al en el servidor de SUNAT<br>Reporte este error!!');
            },
        });
    }
    /*MODAL LINEA*/

    /*----------------------------------------------------------AJAX LISTA DE ALMACEN----------------------------------------------------------*/













    /*----------------------------------------------------------AJAX LISTA DE NAVE----------------------------------------------------------*/

    /*MODAL NAVE*/
    $('#modal-nave-btn-guardar').click(function () {
        var naveData = [];
        naveData.push('');
        naveData.push($('#modal-nave-input-nombre').val());
        naveData.push(EMBARQUE._data.listaEmbarque[EMBARQUE._data.indexareb].idLinea);
        naveData.push('b1');
        setNave(naveData, 'nave');

    });
    $('#modal-nave-btn-limpiar').click(function () {
        limpiarModalNave();
    });
    $('#modal-nave-btn-cerrar').click(function () {
        $('#modal_nave').modal('hide');
        limpiarModalNave();
    });

    function limpiarModalNave() {
        $('#modal_nave input').val('');
    }

    /*MODAL NAVE*/
    function chargeNave(id, Nave) {
        $.ajax({
            data: {id},
            url: '../ajax/Nave/getAllNaveForLinea.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length > 0) {
                    alerta.alerSuccess('Cargando Naves por Linea');
                    selectNave(json, Nave);
                } else {
                    alerta.alerInfo('Agregue naves para esta linea');
                    clearSelect(Nave);
                }
            },
            error: function () {
                alerta.alerError('Error al cargar Naves por Linea<br>Reporte este error!!');
            }
        });
    }
    function setNave(puertoData, nave) {
        $.ajax({
            data: {'array': JSON.stringify(puertoData)},
            url: '../ajax/Nave/setNaveAndGetRow.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length !== 0) {
                    alerta.alerSuccess('Se ha añadido una nueva nave');
                    EMBARQUE._data.listaEmbarque[EMBARQUE._data.indexareb].naves.push(json);
                   // $('#select_' + nave).append('<option value="' + json.nave_id + '">' + json.nave_nombre + '</option>');
                    setTimeout(function () {
                        $('#select_' + nave).selectpicker('refresh');
                        $('#select_' + nave).val('0').change();
                    },150);
                    $('#modal_nave').modal('hide');
                    limpiarModalNave();

                } else {
                    alerta.alerInfo('No se pudo añadir un nueva nave');
                }
            },
            error: function () {
                alerta.alerError('Error al crear nueva nave<br>Reporte este error!!');
            }
        });
    }
    function selectNave(json, Nave) {
        clearSelect(Nave);
        $(json).each(function (i, v) { // indice, valor
            $('#select_' + Nave).append('<option value="' + v.nave_id + '">' + v.nave_nombre + '</option>');
        });
        $('#select_' + Nave).selectpicker('refresh');
    }

    /*----------------------------------------------------------AJAX LISTA DE NAVE----------------------------------------------------------*/



    /*----------------------------------------------------------AJAX LISTA DE LINEA----------------------------------------------------------*/
    /*MODAL BUSCAR LINEA*/
    $('#orden-input-linea').focus(function () {
        $('#modal_buscar_linea').modal('show');
    });
    $('.input-search-lin').focus(function () {
        $('.input-search-lin').keyup(function () {
            var letra = $('.input-search-lin').val();
            $('#table-linea_filter label input').val(letra);
            $('#table-linea_filter label input').keyup();
        });
    });
    $('#table-linea').on('click', 'tr td a', function (evt) {
        var id = $(this).parents('tr').eq(0).find('input').val();
        var name = $(this).parents('tr').eq(0).find('label').text();

        EMBARQUE._data.listaEmbarque[EMBARQUE._data.indexareb].idLinea=id;
        EMBARQUE._data.listaEmbarque[EMBARQUE._data.indexareb].nombrelinea=name;

        EMBARQUE.rellenarNavesAll();

        $('#orden-input-linea').val(name);
        $('#orden-input-linea-id').val(id);
        chargeNave(id,'nave');
        $('#select_nave').prop('disabled', false);
        $('#orden-btn-modal-nave').prop('disabled', false);
        setTimeout(clearImputFilter,200);
        filter='input-search-lin';
    });
    /*MODAL BUSCAR LINEA*/

    /*MODAL LINEA*/
    $('#modal-linea-btn-buscar').click(function () {
        ruc = $('#modal-linea-input-ruc').val();
        $('body').css('cursor', 'wait');
        buscarRucLinea();
    });
    $('#modal-linea-btn-guardar').click(function () {
        var lineaData = new Array();
        lineaData.push('');
        lineaData.push($('#modal-linea-input-codigo').val());
        lineaData.push($('#modal-linea-input-nombre').val());
        lineaData.push('b1');
        console.log(lineaData);
        setLinea(lineaData);

    });
    $('#modal-linea-btn-limpiar').click(function () {
        limpiarModalLinea();
    });
    $('#modal-linea-btn-cerrar').click(function () {
        limpiarModalLinea();
    });

    function limpiarModalLinea() {
        $('#modal_linea input').val('');
    }
    function setLinea(forwaData) {
        $.ajax({
            data: {'array': JSON.stringify(forwaData)},
            url: '../ajax/Linea/setLineaAndGetRow.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json.length!==0) {
                    console.log('AGREGANDO LINEA');
                    addLinea(json);
                    $('#modal_linea').modal('hide');
                    limpiarModalLinea();
                    alerta.alerSuccess('Se ha creado nueva Linea');
                } else {
                    alerta.alerInfo('No se pudo crear Linea ingrese todos los campos');
                }
            },
            error: function () {
                alerta.alerError('Error al crear Linea<br>Reporte este error!!');
            }
        });
    }
    function addLinea(json) {
        var t = $('#table-linea').DataTable();
        var info = t.page.info();
        var count = info.recordsTotal;
        console.log(count);
        if(json){
            t.row.add([
                '<div class="text-center">'+(++count)+'</div>',
                '<div class="text-left">' +json.linea_codigo+'</div>',
                '<div class="text-left">' +
                '<label id="num_for">'+json.linea_nombre+'</label>' +
                '</div>',
                '<div class="text-center">'+
                '<a id="btn-"  class="btn btn-sm btn-danger fa fa-check btn-option" title="Anadir item" data-dismiss="modal"></a>' +
                '<input class="forwa_id no-display" type="text" value="' + json.linea_id + '">' +
                '</div>'
            ]).draw(false);
        }
    }
    function buscarRucLinea() {
        $.ajax({
            data: {ruc},
            url: '../ajax/libSunat/sunat/example/consultaRuc.php',
            type: 'POST',
            beforeSend: function() {
                alerta.alerInfo('Buscando RUC en Sunat, Porfavor espere ..');
            },
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.success) {
                    $('body').css('cursor', 'default');
                    $('#modal-linea-input-nombre').val(json.result.razon_social);
                    alerta.alerSuccess('Cargando Datos de Linea naviera');
                } else {
                    $('body').css('cursor', 'default');
                    alerta.alerWar(json.message);
                }

            },
            error: function () {
                alerta.alerError('Error al en el servidor de SUNAT<br>Reporte este error!!');
            },
        });
    }
    /*MODAL LINEA*/

    /*----------------------------------------------------------AJAX LISTA DE LINEA----------------------------------------------------------*/











    /*----------------------------------------------------------AJAX LISTA DE FORWARDER----------------------------------------------------------*/


    /*MODAL BUSCAR FORWARDER*/
    $('#orden-input-Forwarder').focus(function () {
        $('#modal_buscar_forwarder').modal('show');
    });
    $('.input-search-for').focus(function () {
        $('.input-search-for').keyup(function () {
            var letra = $('.input-search-for').val();
            $('#table-forwarder_filter label input').val(letra);
            $('#table-forwarder_filter label input').keyup();
        });
    });
    $('#table-forwarder').on('click', 'tr td a', function (evt) {
        var id = $(this).parents('tr').eq(0).find('input').val();
        var name = $(this).parents('tr').eq(0).find('label').text();
        $('#orden-input-Forwarder').val(name);
        $('#orden-input-Forwarder-id').val(id);

    });
    /*MODAL BUSCAR FORWARDER*/

    /*MODAL FORWARDER*/
    $('#modal-forwarder-btn-buscar').click(function () {
        ruc = $('#modal-forwarder-input-ruc').val();
        $('body').css('cursor', 'wait');
        buscarRucForwarder();
    });
    $('#modal-forwarder-btn-guardar').click(function () {
        var forwaData = new Array();
        forwaData.push('');
        forwaData.push($('#modal-forwarder-input-ruc').val());
        forwaData.push($('#modal-forwarder-input-nombre').val());
        forwaData.push('b1');
        console.log(forwaData);
        setForwarder(forwaData);

    });

    $('#modal-forwarder-btn-limpiar').click(function () {
        limpiarModalforwarder();
    });
    $('#modal-forwarder-btn-cerrar').click(function () {
        limpiarModalforwarder();
    });

    function limpiarModalforwarder() {
        $('#modal_forwarder input').val('');
    }
    function setForwarder(forwaData) {
        $.ajax({
            data: {'array': JSON.stringify(forwaData)},
            url: '../ajax/Forwarder/setForwarderAndGetRow.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json.length!==0) {
                    console.log('AGREGANDO FORWARDER');
                    addForwarder(json);
                    $('#modal_forwarder').modal('hide');
                    limpiarModalforwarder();
                    alerta.alerSuccess('Se ha creado nuevo forwarder');
                } else {
                    alerta.alerInfo('No se pudo crear forwarder ingrese todos los campos');
                }
            },
            error: function () {
                alerta.alerError('Error al crear forwarder<br>Reporte este error!!');
            }
        });
    }
    function addForwarder(json) {
        var t = $('#table-forwarder').DataTable();
        var info = t.page.info();
        var count = info.recordsTotal;
        console.log(count);
        if(json){
            t.row.add([
                '<div class="text-center">'+(++count)+'</div>',
                '<div class="text-left">' +json.forwa_codigo+'</div>',
                '<div class="text-left">' +
                    '<label id="num_for">'+json.forwa_nombre+'</label>' +
                '</div>',
                '<div class="text-center">'+
                    '<a id="btn-"  class="btn btn-sm btn-danger fa fa-check btn-option" title="Anadir item" data-dismiss="modal"></a>' +
                    '<input class="forwa_id no-display" type="text" value="' + json.forwa_id + '">' +
                '</div>'
            ]).draw(false);
        }
    }
    function buscarRucForwarder() {
        $.ajax({
            data: {ruc},
            url: '../ajax/libSunat/sunat/example/consultaRuc.php',
            type: 'POST',
            beforeSend: function() {
                alerta.alerInfo('Buscando RUC en Sunat, Porfavor espere ..');
            },
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.success) {
                    $('body').css('cursor', 'default');
                    $('#modal-forwarder-input-nombre').val(json.result.razon_social);
                    alerta.alerSuccess('Cargando Datos de Forwarder');
                } else {
                    $('body').css('cursor', 'default');
                    alerta.alerWar(json.message);
                }

            },
            error: function () {
                alerta.alerError('Error al en el servidor de SUNAT<br>Reporte este error!!');
            },
        });
    }
    /*MODAL FORWARDER*/

    /*----------------------------------------------------------AJAX LISTA DE FORWARDER----------------------------------------------------------*/




















    /*----------------------------------------------------------AJAX LISTA DE SUNAT----------------------------------------------------------*/


    /*----------------------------------------------------------AJAX LISTA DE SUNAT----------------------------------------------------------*/
    function clearImputFilter(){
        $('.'+filter).val('');
        $('.'+filter).keyup();
    }
    function clearSelect(select) {
        $('#select_' + select).find('option').remove();
        $('#select_' + select).append('<option value="0" selected>-Seleccione-</option>');
        $('#select_' + select).selectpicker('refresh');
    }
    var index = 0;
    function TransformDecimal(ele) {
        var numero ='';
        var moneyText = '';
        moneyText =  $('#'+ele).val();

        var decimal='';
        index = moneyText.indexOf('.');
        decimal='.'+moneyText.substring(index+1);
        moneyText=moneyText.replace(/,/g, '');
        moneyText= moneyText.substring(0, index);
        console.log('moneyText');
        console.log(moneyText);
        numero='';
        moneyText = moneyText.split('').reverse().join('');
        for (let i = moneyText.length-1; i >= 0; --i) {
            numero += moneyText.charAt(i);
            if(i%3===0&&i>0){
                numero += ',';
            }
        }
        $('#'+ele).val(numero+decimal);
    }

});