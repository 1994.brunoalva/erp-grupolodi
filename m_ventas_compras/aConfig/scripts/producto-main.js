import alerta from "../alertToas.js";

$(document).ready(function () {

    $('#select_moneda').change(function () {
        var id = $(this).val();
        runAjaxId(id,
            'CambioMoneda',
            'getAllForID',
            'Cambio de moneda',
            getCambio);

        var idOrd = $('#orden-input-orden-id').val();
        var dataOrden = [];
        dataOrden.push(['ord_id', idOrd]);
        dataOrden.push(['mone_id', id]);

        runAjaxArray(dataOrden,
            'Orden',
            'updateOrden',
            'Moneda',
            updateCambio);


    });
    funtionCambioMoneda = function () {
        runAjaxId(2,
            'CambioMoneda',
            'getAllForID',
            'Cambio de moneda',
            getCambio);

        var idOrd = $('#orden-input-orden-id').val();
        var dataOrden = [];
        dataOrden.push(['ord_id', idOrd]);
        dataOrden.push(['mone_id', 2]);

        runAjaxArray(dataOrden,
            'Orden',
            'updateOrden',
            'Moneda',
            updateCambio);
    }
    $('#select_moneda').val($('#input_cambio_id').val()).change();
    var tc=0.00;
    var tp=0.00;
    var td=0.00;
    var tt=0.00;
    calcularTotal();
    function calcularTotal(){
        tc=0.00;
        tp=0.00;
        td=0.00;
        tt=0.00;
        $('#table-orden-detalle tbody tr').each(function () {
            var vc = $(this).find('td').eq(2).text();
            vc =vc.toString().replace(/,/g, '');
            tc = parseInt(vc)+parseInt(tc);


            var vp = $(this).find('td').eq(4).text();
            vp =vp.toString().replace(/,/g, '');
            tp = parseFloat(vp)+parseFloat(tp);


            var vd = $(this).find('td').eq(5).text();
            vd =vd.toString().replace(/,/g, '');
            td = parseFloat(vd)+parseFloat(td);


            var vt = $(this).find('td').eq(6).text();
            vt =vt.toString().replace(/,/g, '');
            tt = parseFloat(vt)+parseFloat(tt);

           });

        /*console.log(tt.toFixed(2));*/


        $('#cantidad').text(tc);
        /*$('#precio').text(tp.toFixed(2));
        $('#descuento').text(td.toFixed(2));
        $('#total').text(tt.toFixed(2));*/
        $('#precio').text(tf(tt.toFixed(2)));
        $('#descuento').text(tf(td.toFixed(2)));
        $('#total').text(tf(tt.toFixed(2)));




        $('#orden-input-fob-total').val(tt.toFixed(2));

        $('#pago-input-monto-total').val(tt.toFixed(2));


        /*$('#pago-input-monto-pendiente').val(tt.toFixed(2));*/


        TransformDecimal('pago-input-monto-total');
        /*TransformDecimal('pago-input-monto-pendiente');*/

        setTimeout(calculatFob,300);

    }
    function calculatFob(){
        var fob_total = $('#orden-input-fob-total').val();
        var flete =$('#orden-input-flete-contenedor').val();
        flete =flete.replace(/,/g, '');
        fob_total =fob_total.replace(/,/g, '');
        if(flete===''){
            flete=0.00;
        }
        var conqty = $('#folio-folder-qty').val();
        if(conqty===''){
            conqty=0.00;
        }
        fob_total = (parseFloat(flete) * conqty) + parseFloat(fob_total);
        $('#orden-input-total-flete').val(fob_total.toFixed(2));


        TransformDecimal('orden-input-fob-total');
        TransformDecimal('orden-input-total-flete');

    }


    $('#table-orden-detalle').on('click', 'tr td a', function (evt) {
        var id = $(this).parents('tr').eq(0).find('input').val();
        var fila = $(this).parents('tr').eq(0).index();
        console.log('id');
        console.log(id);

        runAjaxId(id,
            'OrdenDetalle',
            'deleteForID',
            'eliminar detalle',
            deleteDetalle);
        var t = $('#table-orden-detalle').DataTable();
        t.row(fila).remove().draw();
        delete t.row(':eq(0)');
        var row = $('#table-orden-detalle tbody tr').length;
        if(row==0){
            rowValue();
        }
        resetIndex();
        calcularTotal();

    });
    function rowValue() {
        $('#cantidad').text('0.00');
        $('#precio').text('0.00');
        $('#descuento').text('0.00');
        $('#total').text('0.00');
    }
    function resetIndex() {
        var i = 0;
        $('#table-orden-detalle tr').each(function () {
            var indice = $(this).find('td').eq(0).text(i++);
           /* console.log(indice);*/
        });
    }

    function calcularPrecioTotal(){
        var pu =$('#producto-input-precio-uni').val();
        var qty =$('#producto-input-qty').val();
        var de= $('#producto-input-descuento').val();
        if(de===''){
            de ='0.00';
        }
        if(pu===''){
            de ='0.00';
        }
        pu = pu.replace(/,/g, '');
        de = de.replace(/,/g, '')
        var total= (parseFloat(pu)-(parseFloat(pu)*(parseFloat(de)/100)));
        total =parseFloat(total)*qty;
        $('#producto-input-total').val(total.toFixed(2));
        TransformDecimal('producto-input-total');
    }
    $('#producto-input-precio-uni').focusout(function () {
        calcularPrecioTotal();
    });
    $('#producto-input-descuento').focusout(function () {
        calcularPrecioTotal();
    });
    $('#producto-input-qty').focusout(function () {
        calcularPrecioTotal();
    });
    $('#q-dwn').mouseover(function () {
        var pro = $('#producto-input-total').val();
        if(pro.length>0){
            calcularPrecioTotal();
        }
    });
    $('#q-up').mouseover(function () {
        var pro = $('#producto-input-total').val();
        if(pro.length>0){
            calcularPrecioTotal();
        }
    });



    $('#producto-btn-guardar').click(function () {

        if ($('#producto-input-productos-id').val().length>0){
            if ($('#producto-input-precio-uni').val().length>0){
                var dataProducto = [];
                dataProducto.push('');
                dataProducto.push($('#orden-input-orden-id').val());
                dataProducto.push($('#producto-input-productos-id').val());
                dataProducto.push($('#producto-input-productos').val());
                dataProducto.push($('#producto-input-qty').val());
                var pu =$('#producto-input-precio-uni').val();
                var de= $('#producto-input-descuento').val();
                var to =$('#producto-input-total').val();
                if(de===''){
                    de='0.00';
                }

                dataProducto.push(pu);
                dataProducto.push(de);
                dataProducto.push(to);
                dataProducto.push($('#producto-select-presentacion2').val());
                dataProducto.push($("#producto-input-qty-presen").val());
                dataProducto.push($("#select-tipo-envio").val());
                dataProducto.push('1');
                /* console.log(dataProducto);*/
                runAjaxArray(dataProducto,
                    'OrdenDetalle',
                    'setDetalleAndGetRow',
                    'Detalle',
                    addTableProducto);
                $('#producto-btn-clear').click();
            }else{
                $.toast({
                    heading: 'INFORMACION',
                    text: "Agrege el precio del producto",
                    icon: 'info',
                    position: 'top-right',
                    hideAfter: '2500',
                });
                $('#producto-input-precio-uni').focus();

            }
        }else{
            $.toast({
                heading: 'INFORMACION',
                text: "No se seleccionó correctamente el producto",
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
            $('#producto-input-productos').focus();

        }

    });

    $('#producto-btn-clear').click(function () {
        limpiarForm('row-box-producto');
    });

    function limpiarForm(box) {
        $('#' + box + ' input').each(function () {
            $(this).val('');
        });
        $('.input-buscar-p').keyup();
        $('#select_medida').val(0).change();
    }

    /*row-box-producto*/


    function runAjaxId(id, file, method, title, func) {
        $.ajax({
            data: {id},
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                /*console.log(json);*/
                if (json) {
                    func(json);
                } else {
                    /*clearSelect(puerto);*/
                }
            },
            error: function (err) {
                console.log(err.responseText)
                alerta.alerError('Error al cargar ' + title + '<br>Reporte este error!!');
            }
        });
    }

    function runAjaxArray(array, file, method, title, func) {
        $.ajax({
            data: {'array': JSON.stringify(array)},
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                /*console.log(json);*/
                if (json) {
                    func(json);
                } else {
                    /*clearSelect(puerto);*/
                }
            },
            error: function (err) {
                console.log(err.responseText)
                alerta.alerInfo('No se pudo cargar ' + title + '<br>Reporte este error!!');
            }
        });
    }

    function updateCambio(json) {
        console.log('CAMBIO ACTUALIZADO');
    }

    function deleteDetalle(json) {
        console.log('DETALLE ELIMINADO');
    }

    function getCambio(json) {
        /*console.log("<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        console.log(json);
        console.log("<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>");*/
        var tipo = '';
        if (json.tas_sunat_compra) {
            tipo = json.tas_sunat_compra;
            idUtlimaTasaCambio=json.tas_id;
        } else {
            tipo = '0.00';
        }
        $('#label-cambio').text('Cambio: ' + tipo + ' S/.');
        $('#label-precio-cambio').val(tipo);
    }

    function addTableProducto(json) {
        var t = $('#table-orden-detalle').DataTable();
        var info = t.page.info();
        var count = info.recordsTotal;
        /*console.log(count);
        console.log(json);*/
        if (json) {
            t.row.add([
                '<div class="text-center"  style="color: white">' + (++count) + '</div>',
                '<div class="text-left">' + json.produ_id.produ_nombre + '</div>',
                '<div class="text-center">' + json.prod_cantidad + '</div>',
                '<div class="text-left">' + json.produ_id.unidad_id.unidad_nombre + '</div>',
                '<div class="text-right">' + json.prod_precio + '</div>',
                '<div class="text-right">' + validar(json.prod_descuento) + '</div>',
                '<div class="text-right">' + json.prod_total + '</div>',
                '<div class="text-center">' +
                '<a id="btn-deta" class="btn btn-sm btn-danger fa fa-times btn-option" title="eliminar item" data-dismiss="modal"></a>' +
                '<input class="deta_id no-display" type="text" value="' + json.orddeta_id + '">' +
                '</div>'
            ]).draw(false);
        }
        setTimeout(calcularTotal,300);

        count=0;
        $('#table-orden-detalle tbody tr').each(function () {
            var cant = $(this).find('td').eq(6).text();
            cant=cant.replace(/,/g, '');
            count = parseInt(cant)+parseInt(count);
        });
        console.log(count);
        $('#seguro-input-total-fob').val(count);
        /*TransformDecimal('seguro-input-total-fob');*/


    }
    function validar(json) {
        var tipo = json;
        if(tipo) {
            return tipo;
        } else {
            tipo = '0.00';
        }
        return tipo
    }


    $('.input-buscar-p').focus(function () {
        $('.input-buscar-p').keyup(function (e) {
            var letra = $('.input-buscar-p').val();
            $('#table-orden-detalle_filter label input').val(letra);
            $('#table-orden-detalle_filter label input').keyup();
        });
    });



    var index = 0;

    function tf(num) {
        var numero ='';
        var moneyText = '';
        moneyText =  num;

        var decimal='';
        index = moneyText.indexOf('.');
        decimal='.'+moneyText.substring(index+1);
        moneyText=moneyText.replace(/,/g, '');
        moneyText= moneyText.substring(0, index);
        /*console.log('moneyText');
        console.log(moneyText);*/
        numero='';
        moneyText = moneyText.split('').reverse().join('');
        for (let i = moneyText.length-1; i >= 0; --i) {
            numero += moneyText.charAt(i);
            if(i%3===0&&i>0){
                numero += ',';
            }
        }
        return (numero+decimal);
    }




























    var index = 0;
function TransformDecimal(ele) {
    var numero ='';
        var moneyText = '';
        moneyText =  $('#'+ele).val();

    var decimal='';
        index = moneyText.indexOf('.');
        decimal='.'+moneyText.substring(index+1);
        moneyText=moneyText.replace(/,/g, '');
        moneyText= moneyText.substring(0, index);
        /*console.log('moneyText');
        console.log(moneyText);*/
        numero='';
        moneyText = moneyText.split('').reverse().join('');
        for (let i = moneyText.length-1; i >= 0; --i) {
            numero += moneyText.charAt(i);
            if(i%3===0&&i>0){
                numero += ',';
            }
        }
    $('#'+ele).val(numero+decimal);
}







});