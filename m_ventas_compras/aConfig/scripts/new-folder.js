import alerta from "../alertToas.js";
import images from "../Myjs/Images.js";

$(document).ready(function () {

    $('#select_marca').prop('disabled', true);
    $('#select_puerto').prop('disabled', true);

    /*CHANGE SELECT CATEGORIA*/
    $('#select_categoria').change(function () {
        if ($(this).val() > 0) {
            $('#select_marca').prop('disabled', false);
            $('#btn_select_marca').prop('disabled', false);
            var id = $(this).val();
            chargeMarca(id, 'marca');
        } else {
            $('#select_marca').prop('disabled', true);
            $('#btn_select_marca').prop('disabled', true);
            clearSelect('marca');
        }

    });
    $('#select_pais').change(function () {
        if ($(this).val() > 0) {
            $('#select_puerto').prop('disabled', false);
            $('#btn_select_puerto').prop('disabled', false);
            var id = $(this).val();
            chargePuerto(id, 'puerto');
        } else {
            $('#select_puerto').prop('disabled', true);
            $('#btn_select_puerto').prop('disabled', true);
            clearSelect('puerto');
        }
    });

    $('#frame-new-folder-btn-limpiar').click(function () {
        limpiarFrameNuevoFolder();
    });

    function limpiarFrameNuevoFolder() {
        $('#content-new-folder input').val('');
        $('.number-spinner input').val('1');
        $(".selectpicker").val(0).change();
        $('#frame-new-folder-input-serie').val('2020');
        $(".filter-option-inner .filter-option-inner-inner").text('-Seleccione-');
    }

    /*============================================NUEVO FOLDER==============================================================000*/

    $("#frame-new-folder-form").submit(function(e){
        if (valIpor){
            valIpor=false;
            var folderData = [];
            folderData.push('');
            folderData.push($('#frame-new-folder-input_importador_id').val());
            folderData.push($('#select_categoria').val());
            folderData.push($('#frame-new-folder-input-serie').val());
            folderData.push($('#frame-new-folder-input-numero').val());
            folderData.push($('#select_incoterm').val());
            folderData.push($('#select_tipo_contenedor').val());
            var qty =$('#frame-new-folder-qty').val();
            folderData.push(qty);
            folderData.push($('#select_marca').val());
            folderData.push($('#select_pais').val());
            folderData.push($('#select_puerto').val());
            folderData.push($('#frame-new-folder-input_proveedor_id').val());
            folderData.push($('#frame-new-folder-num-proforma').val());
            folderData.push(getDate());
            folderData.push('b1');
            console.log(folderData);
            setFolder(folderData);
        }




        e.preventDefault();
    });
    $('#frame-new-folder-btn-guardar').click(function () {
        var folderData = [];
        folderData.push('');
        folderData.push('');
        folderData.push($('#select_categoria').val());
        folderData.push($('#frame-new-folder-input-serie').val());
        folderData.push($('#frame-new-folder-input-numero').val());
        folderData.push($('#select_incoterm').val());
        folderData.push($('#select_tipo_contenedor').val());
        var qty =$('#frame-new-folder-qty').val();
        folderData.push(qty);
        folderData.push($('#select_marca').val());
        folderData.push($('#select_pais').val());
        folderData.push($('#select_puerto').val());
        folderData.push($('#frame-new-folder-input_proveedor_id').val());
        folderData.push($('#frame-new-folder-num-proforma').val());
        folderData.push(getDate());
        folderData.push('b1');
        console.log("**********7")
        console.log(folderData);
        console.log("**********7")
        setFolder(folderData);
    });
    $('#frame-new-folder-btn-limpiar').click(function () {
        limpiar();
    });
    function getDate() {
        var fecha = $('#frame-new-folder-input_fecha').val();

        return fecha;
    }
    function limpiar() {
        $('#content-new-folder input').val('');
        $('.number-spinner input').val('1');
        $(".selectpicker").val(0).change();
        $('#frame-new-folder-input-serie').val('2020');
        $(".filter-option-inner .filter-option-inner-inner").text('-Seleccione-');
    }
    function setFolder(folderData) {
        $.ajax({
            data: {'array': JSON.stringify(folderData)},
            url: '../ajax/Folder/setFolder.php',
            type: 'POST',
            async: true,
            success: function (response) {
                console.log(response);
                var json = JSON.parse(JSON.stringify(response));

                if (json) {
                    alerta.alerSuccess('Se ha creado un nuevo Folder');
                    setTimeout(redireccionar,1000);

                } else {
                    valIpor=true;
                    alerta.alerInfo('No se pudo crear nuevo Folder, porfavor ingrese todos los campos necesarios');
                }
            },
            error: function (response) {
                console.log(response.responseText);
                valIpor=true;
                alerta.alerError('Error al cargar Lista de folders<br>Reporte este error!!');
            }
        });
    }
    function redireccionar(){
        var url = "import.php";
        $(location).attr('href',url);
    }
    /*============================================NUEVO FOLDER==============================================================000*/



    /*----------------------------------------------------------PRINCIPAL PROVEEDOR----------------------------------------------------------*/
    $('#table-proveedor').on('click', 'tr td a', function (evt) {
        var name = $(this).parents('tr').eq(0).find('label').text();
        var id = $(this).parents('tr').eq(0).find('input').val();
        console.log('TABLE PROBEEDOR');
        console.log('NOMBRE =');
        console.log(name);
        console.log('ID =');
        console.log(id);
        $('#frame-new-folder-input_proveedor').val(name);
        $('#frame-new-folder-input_proveedor_id').val(id);
    });

    $('#table-empresa').on('click', 'tr td a', function (evt) {
        var name = $(this).parents('tr').eq(0).find('label').text();
        var id = $(this).parents('tr').eq(0).find('input').val();
        console.log('TABLE PROBEEDOR');
        console.log('NOMBRE =');
        console.log(name);
        console.log('ID =');
        console.log(id);
        $('#frame-new-folder-input_importador').val(name);
        $('#frame-new-folder-input_importador_id').val(id);
    });


    $('.input-search').focus(function () {
        $('.input-search').keyup(function (e) {
            var letra = $('.input-search').val();
            $('#table-proveedor_filter label input').val(letra);
            $('#table-proveedor_filter label input').keyup();
        });
    });
    $('#frame-new-folder-input_proveedor').focus(function () {
        $('#modal_buscar_proveedor').modal('show');
    });
    $('#frame-new-folder-input_importador').focus(function () {
        $('#modal_buscar_empresa').modal('show');
    });
    /*AJAX PROVEEDOR*/
    function setProveedores(proveedoresData) {
        $.ajax({
            data: {'array': JSON.stringify(proveedoresData)},
            url: '../ajax/Proveedores/setProveedorAndGetRow.php',
            type: 'POST',
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json.length !== 0) {
                    alerta.alerSuccess('Se ha creado un nuevo proveedor');
                    limpiarModalProveedor();
                    $('#modal_proveedor').modal('hide');
                    addProveedores(json);
                } else {
                    alerta.alerInfo('No se pudo crear nuevo proveedor');
                }
            },
            error: function () {
                /*alerta.alerError('Error al cargar Proveedores<br>Reporte este error!!');*/
                alerta.alerInfo('No se pudo crear nuevo proveedor asegurese de iingresaro todos lo campos');

            }
        });
    }
    function addProveedores(json) {
        var t = $('#table-proveedor').DataTable();
        var info = t.page.info();
        var count = info.recordsTotal;
        console.log(count);
        if(json){
            t.row.add([
                '<div class="text-center">'+(++count)+'</div>',
                '<div class="text-center">'+json.pais_id.pais_nombre+'</div>',
                '<div  class="text-center"><label>'+json.provee_desc+'</label></div>',
                '<div class="text-center">'+json.provee_telf+'</div>',
                '<div class="text-center">'+json.provee_contacto+'</div>',
                '<div class="text-center">' +
                    '<a id="btn-"  class="btn btn-sm btn-danger fa fa-check btn-option" title="Anadir item" data-dismiss="modal"></a>' +
                    '<input class="no-display" type="text" value="' + json.provee_id + '">' +
                '</div>'
            ]).draw(false);
        }
    }
    /*AJAX PROVEEDOR*/

    /*----------------------------------------------------------PRINCIPAL PROVEEDOR----------------------------------------------------------*/

    /*----------------------------------------------------------AJAX LISTA DE PROVEEDOR----------------------------------------------------------*/

    /*MODAL BUSCAR PROVEEDOR*/
    $('#modal-proveedor-btn-guardar').click(function () {

        var paisNo = $('#select_modal_pais  option:selected').text();

        if (paisNo != "-Seleccione-"){

            const nombRSProvee = $("#modal-proveedor-input-nom_proveedor").val();

            if (nombRSProvee.length >0){

                var proveedorData = new Array();
                proveedorData.push('');
                // proveedorData.push($('#select_modal_prov_tipo_documento').val());
                proveedorData.push('');
                proveedorData.push($('#select_modal_pais').val());
                proveedorData.push($('#modal-proveedor-input-num-doc').val());
                proveedorData.push($('#modal-proveedor-input-nom_proveedor').val());
                proveedorData.push($('#modal-proveedor-input-telf-1').val());
                proveedorData.push($('#modal-proveedor-input-email').val());
                proveedorData.push($('#modal-proveedor-input-tlf-2').val());
                proveedorData.push($('#modal-proveedor-input-contacto').val());
                proveedorData.push($('#modal-proveedor-input-direccion').val());
                proveedorData.push('b1');
                console.log('DATOS DEL PROVEEEDOR');
                console.log(proveedorData);
                setProveedores(proveedorData);

            }else{
                $.toast({
                    heading: 'INFORMACION',
                    text: "Debe escribir la razón social o nombre del proveedor ",
                    icon: 'info',
                    position: 'top-right',
                    hideAfter: '2500',
                });
                $('#modal-proveedor-input-nom_proveedor').focus();
            }

        }else{
            $.toast({
                heading: 'INFORMACION',
                text: "Debe seleccione un país",
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
            $('#select_modal_pais').focus();
        }

    });
    $('#modal-proveedor-btn-limpiar').click(function () {
        limpiarModalProveedor();
    });
    $('#modal-proveedor-btn-cerrar').click(function () {
        $('#modal_proveedor').modal('hide');
        limpiarModalProveedor();
    });
    function limpiarModalProveedor() {
        $('#modal_proveedor input').val('');
        $('#modal_proveedor textarea').val('');

        $('#modal_proveedor select').val('0').change();
        $('#modal_proveedor select').selectpicker('refresh');
    }
    /*MODAL NUEVO PROVEEDOR*/

    /*----------------------------------------------------------AJAX LISTA DE PROVEEDOR----------------------------------------------------------*/

    /*----------------------------------------------------------AJAX LISTA DE TIPO DOCUMENTO----------------------------------------------------------*/

    /*MODAL TIPO DOCUMENTO*/
    $('#modal-tipo-documento-btn-guardar').click(function () {
        var documentoData = new Array();
        documentoData.push('');
        documentoData.push($('#modal-tipo-documento-input-nombre').val());
        documentoData.push($('#modal-tipo-documento-input-descripccion').val());
        documentoData.push('b1');
        console.log(documentoData);
        /*setDocumento(documentoData, 'tipo_documento');*/
        setDocumento(documentoData);
    });
    $('#modal-tipo-documento-btn-limpiar').click(function () {
        limpiarModalDocumento();
    });
    $('#modal-tipo-documento-btn-cerrar').click(function () {
        $('#modal_tipo_documento').modal('hide');
        limpiarModalDocumento();
    });
    function limpiarModalDocumento() {
        $('#modal_tipo_documento input').val('');
        $('#modal_tipo_documento textarea').val('');
    }
    /*MODAL TIPO DOCUMENTO*/
    function setDocumento(documentoData) {
        $.ajax({
            data: {'array': JSON.stringify(documentoData)},
            url: '../ajax/Tipo_docuemnto/setTipoDocumentoAndGetRow.php',
            type: 'POST',
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length !== 0) {
                    alerta.alerSuccess('Se ha creado un nuevo tipo de documento');
                    var docRefresh=['tipo_documento','modal_prov_tipo_documento','prov_tipo_documento'];
                    $(docRefresh).each(function (i, v) {
                        $('#select_' + v).append('<option value="' + json.tipodoc_id + '">' + json.tipodoc_nombre + '</option>');
                        $('#select_' + v).selectpicker('refresh');
                        $('#select_' + v).val('0').change();
                    });
                    limpiarModalDocumento();
                    $('#modal_tipo_documento').modal('hide');
                } else {
                    alerta.alerInfo('No se pudo crear nuevo tipo de documento');
                }
            },
            error: function () {
                alerta.alerError('Error al crear nuevo tipo de documento<br>Reporte este error!!');
            }
        });
    }

    /*----------------------------------------------------------AJAX LISTA DE TIPO DOCUMENTO----------------------------------------------------------*/


    /*----------------------------------------------------------AJAX LISTA DE INCOTERM----------------------------------------------------------*/

    /*MODAL INCOTERM*/
    $('#modal-incoterm-btn-guardar').click(function () {
        var incotermData = new Array();
        incotermData.push('');
        incotermData.push($('#modal-incoterm-input-nombre').val());
        incotermData.push($('#modal-incoterm-input-descripccion').val());
        incotermData.push('b1');
        console.log(incotermData);
        setIncoterm(incotermData, 'incoterm');

    });
    $('#modal-incoterm-btn-limpiar').click(function () {
        limpiarModalIncoterm();
    });
    $('#modal-incoterm-btn-cerrar').click(function () {
        limpiarModalIncoterm();
    });

    function limpiarModalIncoterm() {
        $('#modal_incoterm input').val('');
        $('#modal_incoterm textarea').val('');
    }
    function setIncoterm(incotermData, incoterm) {
        $.ajax({
            data: {'array': JSON.stringify(incotermData)},
            url: '../ajax/Incoterm/setIncotermAndGetRow.php',
            type: 'POST',
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log('json');
                console.log(json);
                if (json.length !== 0) {
                    alerta.alerSuccess('Se ha creado un nuevo incoterm');
                    $('#select_' + incoterm).append('<option value="' + json.inco_id + '">' + json.inco_nombre + '</option>');
                    limpiarModalIncoterm();
                    $('#select_' + incoterm).selectpicker('refresh');
                    $('#select_' + incoterm).val('0').change();
                    $('#modal_incoterm').modal('hide');
                } else {
                    alerta.alerInfo('No se pudo crear nuevo incoterm');
                }
            },
            error: function () {
                alerta.alerError('Error al crear nuevo incoterm<br>Reporte este error!!');
            }
        });
    }

    /*----------------------------------------------------------AJAX LISTA DE INCOTERM----------------------------------------------------------*/


    /*----------------------------------------------------------AJAX LISTA DE TIPO CONTENEDOR----------------------------------------------------------*/

    /*MODAL TIPO CONTENEDOR*/
    $('#modal-tipo-contenedor-btn-guardar').click(function () {
        var contenedorData = new Array();
        contenedorData.push('');
        contenedorData.push($('#modal-tipo-contenedor-input-nombre').val());
        contenedorData.push($('#modal-tipo-contenedor-input-descripccion').val());
        contenedorData.push('b1');
        console.log(contenedorData);
        setContenedor(contenedorData, 'tipo_contenedor')
    });
    $('#modal-tipo-contenedor-btn-limpiar').click(function () {
        limpiarModalTipoContenedor();
    });
    $('#modal-tipo-contenedor-btn-cerrar').click(function () {
        limpiarModalTipoContenedor();
    });

    function limpiarModalTipoContenedor() {
        $('#modal_tipo_contenedor input').val('');
        $('#modal_tipo_contenedor textarea').val('');
    }
    /*MODAL TIPO CONTENEDOR*/
    function setContenedor(contenedorData, tipo_contenedor) {
        $.ajax({
            data: {'array': JSON.stringify(contenedorData)},
            url: '../ajax/Tipo_contenedor/setContenedorAndGetRow.php',
            type: 'POST',
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length !== 0) {
                    alerta.alerSuccess('Se ha creado un nuevo contendor');
                    $('#select_' + tipo_contenedor).append('<option value="' + json.ticon_id + '">' + json.ticon_nombre + '</option>');
                    $('#select_' + tipo_contenedor).selectpicker('refresh');
                    $('#select_' + tipo_contenedor).val('0').change();
                    limpiarModalTipoContenedor();
                    $('#modal_tipo_contenedor').modal('hide');
                } else {
                    alerta.alerInfo('No se pudo crear nuevo contendor');
                }
            },
            error: function () {
                alerta.alerError('Error al crear nuevo contendor<br>Reporte este error!!');
            }
        });

    }


    /*----------------------------------------------------------AJAX LISTA DE TIPO CONTENEDOR----------------------------------------------------------*/


    /*----------------------------------------------------------AJAX LISTA DE CATEGORIA----------------------------------------------------------*/

    /*MODAL CATEGORIA*/
    $('#modal-categoria-btn-guardar').click(function () {
        var cateData = [];
        cateData.push('');
        cateData.push($('#modal-categoria-input-nombre').val());
        cateData.push('b1');
        console.log(cateData);
        setCategoria(cateData, 'categoria');
    });
    $('#modal-categoria-btn-limpiar').click(function () {
        limpiarModalcategoria();
    });
    $('#modal-categoria-btn-cerrar').click(function () {
        limpiarModalcategoria();
    });

    function limpiarModalcategoria() {
        $('#modal_categoria input').val('');
    }
    /*MODAL CATEGORIA*/
    function setCategoria(categoriaData, categoria) {
        $.ajax({
            data: {'array': JSON.stringify(categoriaData)},
            url: '../ajax/Categoria/setCategoriaAndGetRow.php',
            type: 'POST',
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length !== 0) {
                    alerta.alerSuccess('Se ha creado una nueva categoria');
                    $('#select_' + categoria).append('<option value="' + json.cat_id + '">' + json.cat_nombre + '</option>');
                    limpiarModalcategoria()
                    $('#select_' + categoria).selectpicker('refresh');
                    $('#select_' + categoria).val('0').change();
                    $('#modal_categoria').modal('hide');

                } else {
                    alerta.alerInfo('No se pudo crear nueva categoria');
                }
            },
            error: function () {
                alerta.alerError('Error al crear nueva categoria<br>Reporte este error!!');
            }
        });
    }

    /*----------------------------------------------------------AJAX LISTA DE CATEGORIA----------------------------------------------------------*/

    /*----------------------------------------------------------AJAX LISTA DE MARCA----------------------------------------------------------*/

    /*MODAL MARCA*/
    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-file-preview-zone').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-file-image").change(function () {
        readImage(this);
        var url = '';
        var valor = $('#input-file-image').val();
        for (var i = 0; i < valor.length; i++) {
            if (i >= 12) {
                url += valor[i];
            }
        }

        $("#label-file-image").text(url);
    });
    $("#modal-marca-btn-eliminar-logo").click(function () {
        $("#modal-marca-file-logo").val('');
        $("#input-file-image").val('');
        $("#label-file-image").text('( Ninguna imagen seleccionada.... )');
        $('#img-file-preview-zone').attr('src', '../imagenes/logo/img-icon.svg');
    });
    $('#modal-marca-btn-limpiar').click(function () {
        limpiarModalMarca();
    });
    $('#modal-marca-btn-cerrar').click(function () {
        limpiarModalMarca();
    });

    $('#modal-marca-btn-guardar').click(function () {
        var dataInput = $('#modal-marca-input-nombre').val();
        var dataFile = $('#input-file-image').val();
        if (dataInput.length > 0) {
            //if (dataFile.length > 0) {
                insertarMarca();
            /*} else {
                alerta.alerInfo('Porfavor asigne logo para marca');
            }*/
        } else {
            alerta.alerInfo('Porfavor asigne un nombre a esta marca');
        }

    });

    function insertarMarca() {
        var nombreImagen = '';
        if ( $('#input-file-image').val().length>0){
            var frmData = new FormData();
            frmData.append('image-file', $("#input-file-image")[0].files[0]);
            frmData.append('image-name', getNameImage());
            frmData.append('image-set-ruta', '../../imagenes/logo/');
            console.log('getNameImage()');
            console.log(getNameImage());
            images.set(frmData);
            nombreImagen = getNameImage();
        }

        var marcaData = [];
        marcaData.push('');
        marcaData.push($('#select_categoria').val());
        marcaData.push($('#modal-marca-input-nombre').val());
        marcaData.push(nombreImagen);
        marcaData.push('b1');
        console.log(marcaData);
        setMarca(marcaData, 'marca');
    }
    function getNameImage() {
        var name = '';
        name = $("#modal-marca-input-nombre").val();
        name += '_' + $('#select_categoria').val();
        /*var name = valor.substring(valor.length - 4, valor.length);*/
        console.log(name);
        return name;
    }
    function limpiarModalMarca() {
        $("#modal-marca-btn-eliminar-logo").click();
        $("#modal-marca-input-nombre").val('');
        $("#input-file-image").val('');
    }
    /*MODAL MARCA*/
    function chargeMarca(id, marca) {
        $.ajax({
            data: {id},
            url: '../ajax/Marca/getMarcaForCategoria.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length > 0) {
                    alerta.alerSuccess('Cargando marca de producto');
                    selectMarca(json, marca);
                } else {
                    alerta.alerInfo('Agregue marca para esta categoria');
                    clearSelect(marca);
                }
            },
            error: function () {
                alerta.alerError('Error al cargar marca por categoria<br>Reporte este error!!');
            }
        });
    }
    function setMarca(marcaData, marca) {
        $.ajax({
            data: {'array': JSON.stringify(marcaData)},
            url: '../ajax/Marca/setMarcaAndGetRow.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length !== 0) {
                    alerta.alerSuccess('Se ha añadido una nueva marca');
                    var marcaRefresh=['marca','pro_marca','modal_prod_marca'];
                    $(marcaRefresh).each(function (i, v) {
                        $('#select_' + v).append('<option value="' + json.mar_id + '">' + json.mar_nombre + '</option>');
                        $('#select_' + v).selectpicker('refresh');
                        /*$('#select_' + v).val('0').change();*/
                    });
                   /* $('#select_' + marca).append('<option value="' + json.mar_id + '">' + json.mar_nombre + '</option>');
                    $('#select_' + marca).selectpicker('refresh');
                    $('#select_' + marca).val('0').change();*/
                    limpiarModalMarca();
                    $('#modal_marca').modal('hide');

                } else {
                    alerta.alerInfo('No se pudo añadir nueva marca');
                }

            },
            error: function () {
                alerta.alerError('Error al crear nueva marca<br>Reporte este error!!');
            }
        });

    }
    function selectMarca(json, marca) {
        clearSelect(marca);
        $(json).each(function (i, v) { // indice, valor
            $('#select_' + marca).append('<option value="' + v.mar_id + '">' + v.mar_nombre + '</option>');
        });
        $('#select_' + marca).selectpicker('refresh');
    }

    /*----------------------------------------------------------AJAX LISTA DE MARCA----------------------------------------------------------*/
    /*----------------------------------------------------------AJAX LISTA DE PUERTO----------------------------------------------------------*/

    /*MODAL PUERTO*/
    $('#modal-puerto-btn-guardar').click(function () {
        var puertoData = [];
        puertoData.push('');
        puertoData.push($('#modal-puerto-input-nombre').val());
        puertoData.push($('#select_pais').val());
        puertoData.push('b1');
        setPuerto(puertoData, 'puerto');

    });
    $('#modal-puerto-btn-limpiar').click(function () {
        limpiarModalPuerto();
    });
    $('#modal-puerto-btn-cerrar').click(function () {
        $('#modal_puerto').modal('hide');
        limpiarModalPuerto();
    });

    function limpiarModalPuerto() {
        $('#modal_puerto input').val('');
    }

    /*MODAL PUERTO*/
    function chargePuerto(id, puerto) {
        $.ajax({
            data: {id},
            url: '../ajax/Puerto/getAllPuertoForPais.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length > 0) {
                    alerta.alerSuccess('Cargando puertos por pais');
                    selectPuerto(json, puerto);
                } else {
                    alerta.alerInfo('Agregue puertos pora este pais');
                    clearSelect(puerto);
                }
            },
            error: function () {
                alerta.alerError('Error al cargar puertos por pais<br>Reporte este error!!');
            }
        });
    }
    function setPuerto(puertoData, puerto) {
        $.ajax({
            data: {'array': JSON.stringify(puertoData)},
            url: '../ajax/Puerto/setPuertoAndGetRow.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length !== 0) {
                    alerta.alerSuccess('Se ha añadido una nuevo puerto');
                    $('#select_' + puerto).append('<option value="' + json.puerto_id + '">' + json.puerto_nombre + '</option>');
                    $('#select_' + puerto).selectpicker('refresh');
                    $('#select_' + puerto).val('0').change();
                    $('#modal_puerto').modal('hide');
                    limpiarModalPuerto();
                } else {
                    alerta.alerInfo('No se pudo añadir un nuevo puerto');
                }
            },
            error: function () {
                alerta.alerError('Error al crear nuevo puerto<br>Reporte este error!!');
            }
        });
    }
    function selectPuerto(json, puerto) {
        clearSelect(puerto);
        $(json).each(function (i, v) { // indice, valor
            $('#select_' + puerto).append('<option value="' + v.puerto_id + '">' + v.puerto_nombre + '</option>');
        });
        $('#select_' + puerto).selectpicker('refresh');
    }

    /*----------------------------------------------------------AJAX LISTA DE PUERTO----------------------------------------------------------*/


    /*----------------------------------------------------------AJAX LISTA DE PAISES----------------------------------------------------------*/
    /*MODAL PAIS*/

    $('#modal-pais-btn-guardar').click(function () {
        var paisData = [];
        paisData.push('');
        paisData.push($('#modal-pais-input-nombre').val());
        paisData.push('b1');
        console.log(paisData);
        setPais(paisData);

        /*paisRefresh='prov_pais';
        setTimeout(addPais,300);*/
       /* paisRefresh='prov_pais';
        setTimeout(addPais,200);*/
       /* setPais(paisData, 'modal_pais');*/
    });
    $('#modal-pais-btn-limpiar').click(function () {
        limpiarModalpais();
    });
    $('#modal-pais-btn-cerrar').click(function () {
        limpiarModalpais();
    });
    function limpiarModalpais() {
        $('#modal_pais input').val('');
    }

    /*MODAL PAIS*/
    function setPais(paisData) {
        $.ajax({
            data: {'array': JSON.stringify(paisData)},
            url: '../ajax/Pais/setPaisAndGetRow.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length !== 0) {
                    alerta.alerSuccess('Se ha añadido una nuevo Pais');
                    var paisRefresh=['pais','modal_pais','prov_pais','prod_pais'];
                    $(paisRefresh).each(function (i, v) {
                        $('#select_' + v).append('<option value="' + json.pais_id + '">' + json.pais_nombre + '</option>');
                        $('#select_' + v).selectpicker('refresh');
                        $('#select_' + v).val('0').change();
                    });
                    limpiarModalpais();
                    $('#modal_pais').modal('hide');
                } else {
                    alerta.alerInfo('No se pudo añadir un nuevo Pais');
                }
            },
            error: function () {
                alerta.alerError('Error al crear nuevo Pais<br>Reporte este error!!');
            }
        });
    }
    /*----------------------------------------------------------AJAX LISTA DE PAISES----------------------------------------------------------*/
    function clearSelect(select) {
        $('#select_' + select).find('option').remove();
        $('#select_' + select).append('<option value="0" selected>-Seleccione-</option>');
        $('#select_' + select).selectpicker('refresh');
    }

    function setImage(frmData) {
        $.ajax({
            data: frmData,
            url: '../ajax/SetImage/uploadImage.php',
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                if (json) {
                    alerta.alerSuccess('Cargando nueva imagen');
                } else {
                    alerta.alerInfo('no se pudo cargar nueva imagen');
                }
            },
            error: function () {
                alerta.alerError('Error al cargar imagen<br>Reporte este error!!');
            }
        });
    }


});