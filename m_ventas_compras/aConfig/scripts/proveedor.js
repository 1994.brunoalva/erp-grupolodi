import alerta from "../alertToas.js";

$(document).ready(function () {

    $('#proveedor-btn-guardar').click(function () {
        var dataProve = [];
        var id = $('#frame-new-folder-input_proveedor_id').val();
        var id_doc = $('#select_prov_tipo_documento').val();
        var id_pais = $('#select_prov_pais').val();
        var mun_doc = $('#proveedor-input-iddoc').val();
        var nombre = $('#proveedor-input-edit').val();
        var tlf1 = $('#proveedor-input-telf-1').val();
        var email = $('#proveedor-input-email').val();
        var tlf2 = $('#proveedor-input-tlf-2').val();
        var contacto = $('#proveedor-input-contacto').val();
        var direc = $('#proveedor-input-direccion').val();
        var estado = 'b1';


        dataProve.push(['provee_id', id]);
        dataProve.push(['tipodoc_id', id_doc]);
        dataProve.push(['pais_id', id_pais]);
        dataProve.push(['provee_num_doc', mun_doc]);
        dataProve.push(['provee_desc', nombre]);
        dataProve.push(['provee_telf', tlf1]);
        dataProve.push(['provee_email', email]);
        dataProve.push(['provee_telf2', tlf2]);
        dataProve.push(['provee_contacto', contacto]);
        dataProve.push(['provee_direc', direc]);
        dataProve.push(['estado', estado]);

        console.log('ACTUALIZANDO PRO------------------');
        console.log(dataProve);
        blok(true);
        $('#provee-box').show();
        $('#provee-edit').hide();
      //  updateProveedor(dataProve);

        var dataFol = [];
        dataFol.push(['impor_id', $('#folio_input_id').val()]);
        dataFol.push(['provee_id', $('#frame-new-folder-input_proveedor_id').val()]);
        console.log('dataFol');
        console.log(dataFol);
        //updateFolder(dataFol);updateOnliProveedor
        $.ajax({
            type: "POST",
            url: "../ajax/Folder/updateOnliProveedor.php",
            data: {impor_id:$('#folio_input_id').val(),provee_id: $('#frame-new-folder-input_proveedor_id').val()},
            success: function (resq) {
                    alerta.alerSuccess('Se ha actualizado el folio');
            }
        });

    });

    $('#proveedor-input-edit').focus(function () {
        $('#proveedor-input-edit').keyup(function (e) {
            var letra = $('#proveedor-input-edit').val();
            $('#frame-new-folder-input_proveedor').val(letra);
            $('#frame-new-folder-input_proveedor').keyup();
        });
    });


    blok(true);
    $('#proveedor-btn-editar').click(function () {
        desblok(false);
        $('#provee-edit').show();
        $('#provee-box').hide();
    });

    function desblok(op) {
        $('#content-proveedor  select').prop('disabled', op);
        $('#content-proveedor input').prop('disabled', op);
        $('#content-proveedor button').prop('disabled', op);
    }

    function blok(op) {
        $('#select_prov_tipo_documento').prop('disabled', op);
        $('#select_prov_pais').prop('disabled', op);
        $('#proveedor-input-nombre').prop('disabled', op);
        $('#proveedor-btn-mas-proveedor').prop('disabled', op);
    }



    $('#table-proveedor').on('click', 'tr td a', function (evt) {
        var id = $(this).parents('tr').eq(0).find('input').val();
        var name = $(this).parents('tr').eq(0).find('label').text();
        $('#proveedor-input-edit').val(name);
        var result=getAllForId(id);
    });
    /*------------------------------------AJAX LISTA DE PROVEEDOR---------------------------------------------------------*/
    function getAllForId(id) {
        $.ajax({
            data: {id},
            url: '../ajax/Proveedores/getAllForId.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(response);
                if (json) {
                    $('#proveedor-input-num-doc').val(json.provee_num_doc);
                    var idDoc=json.tipodoc_id.tipodoc_id;
                    $("#proveedor-input-Nondoc").val(json.tipodoc_id.doc_nombre)
                    $("#proveedor-input-iddoc").val(json.tipodoc_id.doc_id)
                    $('#proveedor-input-telf-1').val(json.provee_telf);
                    $('#proveedor-input-tlf-2').val(json.provee_telf2);
                    $('#proveedor-input-email').val(json.provee_email);
                    $('#proveedor-input-contacto').val(json.provee_contacto);
                    $('#proveedor-input-direccion').val(json.provee_direc);
                    $('#select_prov_pais').val(json.pais_id.pais_id).change();



                    alerta.alerSuccess('Cargando Proveedores');
                } else {
                    alerta.alerInfo('No existen Proveedores');
                }
            },
            error: function () {
                alerta.alerError('Error al cargar Proveedores<br>Reporte este error!!');
            }
        });
    }
    function updateProveedor(proveeData) {
        $.ajax({
            data: {'array': JSON.stringify(proveeData)},
            url: '../ajax/Proveedores/updateProveedor.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log('RESPUESTA UPDATE PROVEEDOR');
                console.log(response);
                if (json) {
                    alerta.alerSuccess('Se ha actualizado el proveedor');
                } else {
                    console.log()
                    //alerta.alerInfo('No se pudo actualizar del proveedor, hay campos vacios ¡¡¡');
                }
            },
            error: function (err) {
                console.log(err.responseText)
                alerta.alerError('Error al actualizar el proveedor<br>Reporte este error!!');
            }
        });
    }
    function updateFolder(folderData) {
        $.ajax({
            data: {'array': JSON.stringify(folderData)},
            url: '../ajax/Folder/updateFolder.php',
            type: 'POST',
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log('RESPUESTA UPDATE');
                console.log(response);
                if (json) {
                    alerta.alerSuccess('Se ha actualizado el folio');
                } else {
                    alerta.alerInfo('No se pudo actualizar el folio, porfavor ingrese todos los campos necesarios');
                }
            },
            error: function (err) {
                console.log(err.responseText)
                alerta.alerSuccess('Se ha actualizado el folio');
               // alerta.alerError('Error al actualizar el folio<br>Reporte este error!!');
            }
        });
    }






    /*------------------------------------AJAX LISTA DE PROVEEDOR----------------------------------------------------------*/

});