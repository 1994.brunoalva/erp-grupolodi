import alerta from "../alertToas.js";

var table_now = '';
var datos_table_n = [];
$(document).ready(function () {
    alertGlo = alerta;
    var num = $('#folio-folder-qty').val();
    cargarPacking(num);

    /* $('#dwn').mouseout(function () {
         $('#folio-folder-qty').change();
     });
     $('#up').mouseout(function () {
         $('#folio-folder-qty').change();
     });*/

    $('#packing-btn-refresh').click(function () {
        var id = $('#folio_input_id').val();
        location.href = "../importaciones/detalle-folio.php?id=" + id;
    });

    $('#folio-folder-qty').change(function () {
        num = $(this).val();
        cargarPacking(num);
    });

    function cargarPacking(num) {
        console.log(num + "---------------------");
        $("#box-packing").empty();
        $("#box-packing ul>li").remove();
        $("#box-packing div").remove();
        $("#box-packing ul").remove();
        $("#box-packing").append('<div id="frame-packing" class="row" style="background: #FFFFFF; margin: 0;"></div>');
        $("#box-packing #frame-packing").append('<ul class="nav nav-tabs"></ul>');
        $("#box-packing #frame-packing").append('<div class="tab-content cont-tab-packing"></div>');
        for (var i = 0; i <= num; i++) {
            if (i === 0) {
                $('#box-packing #frame-packing ul').append('<li class="active" ><a data-toggle="tab" href="#prod' + i + '">Lista de Compra</a></li>');
                $('#box-packing #frame-packing .tab-content').append('<div id="prod' + i + '" class="tab-pane fade in active"></div>');
                /* cont_table='prod'+i;*/
                addTable('prod' + i);
                var id = $('#datos_producto').val();
                idOrdenPro = id;

                runAjaxId(id,
                    'OrdenDetalle',
                    'getAll',
                    'Detalle producto',
                    getAllProducto);


            } else {
                $('#box-packing #frame-packing ul').append('<li><a data-toggle="tab" id="pack-a-cod' + i + '" href="#tag' + i + '">Contenedor #' + i + '</a></li>');
                var tab_content = $('#box-packing #frame-packing .tab-content');
                var tag_box = $('<div id="tag' + i + '" class="tab-pane pd-t-20 fade"></div>');/*<h1>Contenedor #  '+i+'</h1>*/
                tag_box.append('<div class="form-group col-xs-6 col-sm-4 col-md-3">' +
                    '               <label>CODIGO CONTENEDOR</label>' +
                    '               <input id="pack-input-codigo-' + i + '" class="form-control inp-id" type="text">' +
                    '               <input id="pack-input-codigo-id-' + i + '" class="form-control inp-id no-display" type="text">' +
                    '           </div>' +
                    '<div class="form-group col-xs-6 col-sm-4 col-md-3">' +
                    '               <label>FECHA DE INGRESO ALMACEN</label>' +
                    '               <input id="pack-input-fech-i-id-' + i + '"   class="form-control inp-id" type="Date">' +
                    '           </div>' +
                    '<div class="form-group col-xs-6 col-sm-4 col-md-3">' +
                    '               <label>FECHA DE SALIDA ALMACEN</label>' +
                    '               <input id="pack-input-fech-s-id-' + i + '" class="form-control inp-id" type="Date">' +
                    '           </div>'+
                    '<div class="form-group col-xs-6 col-sm-4 col-md-3">' +
                    '               <label style="color: white">.</label>' +
                    '<div style="width: 100%"><a target="_blank" class="btn btn-info"  id="pac-a-' + i + '" ><i class="fa fa-print"></i> Imprimir</a></div>'+
                    '           </div>');

                tab_content.append(tag_box);


                var id = $('#folio_input_id').val();
                id_folio = id;

                runAjaxId(id,
                    'Contenedor',
                    'getAllContenedor',
                    'Contenedores',
                    getAllContenedor);
            }
        }
    }
    funtionPackContener = cargarPacking;
    function addTable(ele) {
        var tabla = '<table id="table-pack" class="table table-striped table-bordered table-hover style="width: 100%;">' +
            '                <thead class="bg-head-table">' +
            '                <tr style="height: 10px; padding: 0;">' +
            '                    <th class="text-center">DETALLE</th>' +
            '                    <th class="text-center">DESCRIPCION</th>' +
            '                    <th class="text-center">MODELO</th>' +
            '                    <th class="text-center">CNT</th>' +
            '                    <th class="text-center">MEDIDA</th>' +
            '                    <th class="text-center">RESTANTE</th>' +
            '                    <th class="text-center">OPCION</th>' +
            '                </tr>' +
            '                </thead>' +
            '                <tbody id="table-pack-tr">' +

            '                </tbody> ' +
            '                <tfoot id="table-pack-footer">' +
            '                     <tr>' +
           ' <th colspan="3" class="text-right">TOTAL</th>' +
        '                        <th colspan="1" class="text-center"></th>' +
            '                        <th colspan="1" class="text-right">TOTAL ASIGNADO</th>' +
            '                        <th colspan="1" class="text-center"></th>' +
            '                     </tr>' +
            '                </tfoot>' +
            '          </table>';

        $('#' + ele).append(tabla);
    }


    setTimeout(cargar, 500);
    setTimeout(readTable, 700);
    funReable=readTable;
    funCarga=cargar;
    var index_tbody = 0;
    var contenedor_id = [];

    function readTable() {
        contenedor_id = [];
        console.log(datos_table_n+"----------");
        $(datos_table_n).each(function (i, v) {
            var tag_box = $('#tag' + (i + 1));
            var id = v.con_id;
            index_tbody = (i + 1);
            tag_box.append(getTable(i));
            contenedor_id.push(v.con_id);
        });
        chargeTableTag(contenedor_id);

    }

    function getTable(i) {

        var tabla = '<div class="table-responsive form-group col-xs-12 no-padding">' +
            '           <table id="table-' + i + '" class="table table-striped table-bordered table-hover style="width: 100%;">' +
            '                <thead class="bg-head-table">' +
            '                <tr style="height: 10px; padding: 0;">' +
            '                    <th class="text-center" style="color: white">#</th>' +
            '                    <th class="text-center">DESCRIPCION</th>' +
            '                    <th class="text-center">MODELO</th>' +
            '                    <th class="text-center">U.MEDIDA</th>' +
            '                    <th class="text-center">QTY</th>' +
            '                    <th class="text-center">ELIMINAR</th>' +
            '                </tr>' +
            '                </thead>' +
            '                <tbody id="table-pack-tr' + i + '">' +

            '                </tbody> ' +
            '                <tfoot id="table-pack-footer' + i + '">' +
            '                     <tr>' +
            '                        <th colspan="2"></th>' +
            '                        <th colspan="2" class="text-right">TOTAL </th>' +
            '                        <th colspan="1" class="text-center"></th>' +
            '                     </tr>' +
            '                </tfoot>' +
            '          </table>' +
            '      </div>';
        return tabla;
    }

    var ondex = 0;

    function chargeTableTag(arrayConte_id) {
        var id = '';
        $(arrayConte_id).each(function (i, v) {
            id = v;
            runAjaxAsyncId(id,
                'Packing',
                'getAllPackingForID',
                'Packing',
                cargarBody);
        });
        llenartablapaking();

    }

    function cargarBody(json) {
        if (json.length != 0) {
            console.log("a")
            console.log(json);
            console.log(index_tabe);
            console.log("b")
            paking_tab.push(json)
        } else {
            paking_tab.push([]);
        }
        //paking_tab[index_tabe-1]=json;
        /*var row_tr='';
        console.log(json);
        $(json).each(function (i,v) {
            row_tr+='<tr>' +
                '           <td class="contador">'+(i+1)+'</td>' +
                '           <td>'+v.orddeta_id.produ_id.produ_nombre+'</td>' +
                '           <td>'+v.orddeta_id.produ_id.mod_id.mod_nombre+'</td>' +
                '           <td>'+v.orddeta_id.produ_id.unidad_id.unidad_nombre+'</td>' +
                '           <td>'+valida(v.pack_cant)+'</td>' +
                '           <td class="text-center"> ' +
                '               <a  data-index="'+(i+1)+'" id-dp="'+v.orddeta_id.orddeta_id+'"  id="btn-" class="btn btn-sm btn-danger fa fa-times" title="Eliminar item" data-dismiss="modal"></a>\n' +
                '                <input class="pack_id no-display" type="text" value="'+v.pack_id+'">' +
                '                <input class="ordeta_id no-display" type="text" value="'+v.orddeta_id.orddeta_id+'">' +
                '           </td>' +
                '       </tr>';
        });
        $('#table-pack-tr'+(index_tabe-1)).append(row_tr); */
    }

    setTimeout(countTable, 1000);

    function countTable() {
        $('#table-pack tbody tr').each(function (j, b) {
            var id = $(this).find('.pack-input-orden-id').val();
            var count = 0;
            $(datos_table_n).each(function (i, v) {
                $('#table-' + (i) + ' tbody tr').each(function () {
                    var cant = $(this).find('td').eq(4).text();
                    var id_tag = $(this).find('td').find('input').eq(1).val();
                    if (id === id_tag) {
                        console.log('ES IGUAL');
                        count = parseInt(count) + parseInt(cant);
                    }
                });

            });
            var cantidad = $(this).find('td').eq(5).text();
            cantidad = parseInt(cantidad) - parseInt(count);
            console.log(cantidad);
            $(this).find('td').eq(5).text(cantidad);
        });
        contabilizarTabla();
    }


    function valida(data) {
        if (data) {
            return data;
        } else {
            return '';
        }
    }

    var id_folio = '';

    $('.inp-id').focusout(function () {
        var id = $(this).attr('id');
        var data = $(this).val();
        if (data.length > 0) {
            id = id.substring(id.length - 1, id.length);

            var codigo = $('#pack-input-codigo-' + id).val();
            $('#pack-a-cod' + id).text('CTN #COD : ' + codigo);
            $('#table-pack tbody tr').each(function (i, v) {
                $('#select_' + (i + 1) + ' option').each(function (j, v) {
                    var o = (j);
                    if (j > 0) {
                        if (o == id) {
                            $(this).text('CTN #COD : ' + codigo);
                        }
                    }
                });
            });
            var fechS = $('#pack-input-fech-s-id-' + id).val();
            var fecheI = $('#pack-input-fech-i-id-' + id).val();
            console.log(fechS + "///////"+fecheI)
            id = $('#pack-input-codigo-id-' + id).val();
            var dataPack = [];
            dataPack.push(['con_id', id]);
            dataPack.push(['codigo', codigo]);
            dataPack.push(['fecha_entrada', fecheI]);
            dataPack.push(['fecha_salida', fechS]);
            console.log(dataPack);
            runAjaxArray(dataPack,
                'Contenedor',
                'updateContenedor',
                'Contenedores',
                setContenedor);
        }
    });


    var dir = 0;
    var producto = '';
    var id_deta = '';
    var cantidad = '';
    var sop = 0;
    $('#table-pack').on('click', 'tr td select', function (evt) {
        dir = 0;
        sop = $(this).parents('tr').eq(0).index();
        id_deta = $(this).parents('tr').find('input').val();
        producto = $(this).parents('tr').find('td').eq(2).text();
        cantidad = $(this).parents('tr').find('td').eq(5).text();
        var id_select = $('#select_' + (sop + 1)).attr('id');
        $('#modal-packing-input-produ-cantidad').val(cantidad);
        alerSelect(id_select);
    });

    function alerSelect(id_select) {
        $('#' + id_select).change(function () {
            if (dir <= 0) {
                ++dir;
                var con = $(this).find('option:selected').text();
                var n = $(this).val();
                if (n > 0) {
                    $('#modal_packing').modal('show');
                    var title = 'Cuantos items desea agregar al contenedor';
                    console.log(title);
                    $('#modal_packing .t-1').text('PRODUCTO : ' + producto);
                    $('#modal_packing .t-2').text(title);
                    $('#modal_packing .t-3').text(con + '  ?');
                    $('#modal-packing-input-produ-id').val(id_deta);
                    id_select = $(this).val();
                    $('#modal-packing-input-id-index').val(id_select);
                }
            }

        });
    }

    function getAllContenedor(json) {
        console.log("############################################11")
        console.log(json)
        datos_table_n = [];
        datos_table_n = json;
        datos_table_n2 = [];
        datos_table_n2 = json;
        $(json).each(function (i, v) {
            $('#pack-input-codigo-id-' + (i + 1)).val(v.con_id);

            if (v.codigo) {
                $('#pack-a-cod' + (i + 1)).text('CTN. COD # ' + v.codigo);
                $('#pack-input-codigo-' + (i + 1)).val(v.codigo);
            } else {
                $('#pack-a-cod' + (i + 1)).text('Contenedor #' + (i + 1));
                $('#pack-input-codigo-' + (i + 1)).val('');
            }
            console.log("1111111111111111111111111111111111111")
            console.log(v)

            $('#pack-input-fech-s-id-' + (i + 1)).val(v.fecha_salida);
            $('#pack-input-fech-i-id-' + (i + 1)).val(v.fecha_entrada);
            console.log('#pack-a-' + (i + 1) + "<<<<<<<<");
            $('#pac-a-' + (i + 1)).attr('href','./pdf/pdf_importaciones_gen.php?folder='+$('#folio_input_id').val() +'&conte='+v.con_id);
        });
    }


    function cargar() {

        $('#table-pack tbody tr').each(function (j, v) {
            var opcion = [];
            $('#select_' + (j + 1) + ' option').each(function (i, v) {
                if (i > 0) {
                    opcion.push($(this).attr('id'));
                }
            });
            $(datos_table_n).each(function (i, v) {
                if (v.codigo) {
                    $('#' + opcion[i]).text('CTN. COD # ' + v.codigo);
                } else {
                    $('#' + opcion[i]).text('Contenedor #' + (i + 1));
                }
            });
        });
    }


    function setContenedor(json) {
        console.log('EXITOSO' + json);
    }

    function getAllProducto(json) {
        var row = '';
        var cant = 0;
        paking_listc.push(...json);
        $(json).each(function (i, v) {
            var medida = v.produ_id.detalle[0] + ' - ';
            medida += v.produ_id.detalle[1] + ' - ';
            medida += v.produ_id.detalle[2];
            cant = parseInt(cant) + parseInt(v.prod_cantidad);
            //console.log((v.produ_id.mod_id?"1":"2")+"<<<<<<<<<<<");
            var modeloNobre  = v.produ_id.mod_id? v.produ_id.mod_id.mod_nombre: '';
           // console.log(v.produ_id.unidad_id)
            row += '<tr >' +

                '<td class="text-center">' + medida + '</td>' +
                '<td class="text-center">' + v.produ_id.produ_nombre + '</td>' +
                '<td class="text-center">' + modeloNobre + '</td>' +
                '<td class="text-center">' +v.prod_cantidad+ '</td>' +
                '<td class="text-center">'+ v.produ_id.unidad_id.unidad_nombre + '</td>' +
                '<td class="text-center" id="row-unic-pck-value-' + v.orddeta_id + '" >' + v.prod_cantidad + '</td>' +
                '<td class="text-center no-padding">' +
                '<input type="text" class="no-display pack-input-orden-id" value="' + v.orddeta_id + '">' +
                '<select id="select_' + (i + 1) + '" class="form-control show-tick no-padding esadoSelect">' +
                '<option id="op-0" value="0" selected>-Seleccione-</option>';
            for (let j = 0; j < num; j++) {
                row += '<option id="op-' + (i + 1) + '-' + (j + 1) + '" value="' + (j + 1) + '">Contenedor #' + (i + 1) + '</option>';
            }

            row += '</select>' +
                '</td>' +
                '</tr>';

            $('#table-pack tbody tr').find('td').last().append('<select id="select_' + (i + 1) + '" class="form-control show-tick no-padding">');
        });

        //console.log(row)

        $('#table-pack tfoot').find('th').eq(1).text(cant);
        $('#table-pack tbody').append(row);
    }


    function runAjaxArray(array, file, method, title, func) {
        $.ajax({
            data: {'array': JSON.stringify(array)},
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json) {
                    func(json);
                } else {
                }
            },
            error: function (err) {
                console.log(err.responseText);
                alerta.alerInfo('No se pudo cargar ' + title + '<br>Reporte este error!!');
            }
        });
    }

    function runAjaxId(id, file, method, title, func) {
        $.ajax({
            data: {id},
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: true,
            success: function (response) {
                console.log(response)

                var json = JSON.parse(JSON.stringify(response));
                if (json) {
                    func(json);
                } else {
                }
            },
            error: function (request, status, error) {
                console.log(request.responseText);
                alerta.alerError('Error al cargar ' + title + '<br>Reporte este error!!');
            }
        });
    }

    function runAjaxId_none(id, file, method, title, func) {
        $.ajax({
            data: {id},
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                if (json) {
                    func(json);
                } else {
                }
            },
            error: function () {
            }
        });
    }

    function runAjaxAsyncId(id, file, method, title, func) {
        $.ajax({
            data: {id},
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: false,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                ondex++;
                if (json) {
                    if (json.length > 0) {
                        index_tabe = ondex;
                    }
                    func(json);
                } else {
                }
            },
            error: function (err) {
                console.log(err.responseText)
                alerta.alerError('Error al cargar ' + title + '<br>Reporte este error!!');
            }
        });
    }

    var index_tabe = 0;
    var id_tr = 0;
    var indexTemp=0;
    $('#modal-packing-btn-agregar').click(function () {
        var can = $('#modal-packing-input-nombre').val();
        var id = $('#modal-packing-input-produ-id').val();
        var id_cont = $('#modal-packing-input-id-index').val();
        id_tr = id_cont;
        var isNew = true;

        var idPac_cot = 0;
        var cnt_atc = 0;


        for (var i = 0; i < paking_tab[id_tr - 1].length; i++) {
            if (id == paking_tab[id_tr - 1][i].orddeta_id.orddeta_id) {
                isNew = false;
                idPac_cot = paking_tab[id_tr - 1][i].pack_id;
                cnt_atc = paking_tab[id_tr - 1][i].pack_cant;
                indexTemp=i;
                break;
            }
        }

        var dataPack = [];
        dataPack.push('');
        dataPack.push(id);


        id_cont = $('#pack-input-codigo-id-' + id_cont).val();
        dataPack.push(id_cont);
        dataPack.push(can);
        dataPack.push('b1');
        console.log(dataPack);
        can = parseInt(can);
        cantidad = parseInt(cantidad);
        if (can <= cantidad) {
            var t = $('#table-pack tbody tr').eq(sop).find('td').eq(5);
            var cant = t.text();
            cant = parseInt(cant) - parseInt(can);
            t.text(cant);
            if (isNew) {
                runAjaxArray(dataPack,
                    'Packing',
                    'setPacking',
                    'PAcking',
                    setPacking);
            } else {
               const  nuevacantidad = parseInt(can) + parseInt(cnt_atc);
                runAjaxDataSpecifica({
                        id: idPac_cot,
                        cnt: nuevacantidad
                    }, 'Packing',
                    'updateForid',
                    'PAcking',
                    function (re) {
                        if (re.res) {
                           paking_tab[id_tr - 1][indexTemp].pack_cant=nuevacantidad;
                            llenartablapakingunique(id_tr - 1);
                            $('#modal-packing-btn-cerrar').click();
                            $('#modal-packing-btn-limpiar').click();
                        }
                    }
                )
            }



        } else {
            if (cantidad === 0) {
                alerta.alerInfo('Ya agrego todo al packing');
            } else {
                alerta.alerWar('cantidad es mayo a la compra');
            }

        }
    });

    function runAjaxDataSpecifica(data, file, method, title, func) {

        $.ajax({
            data: data,
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json) {
                    func(json);
                } else {
                }
            },
            error: function (err) {
                console.log(err.responseText);
                alerta.alerInfo('No se pudo cargar ' + title + '<br>Reporte este error!!');
            }
        });
    }

    function setPacking(json) {
        console.log(json);
        //console.log("1--------")
        //var tablatem = $('#table-pack-tr'+(id_tr-1)+" tr");
        console.log(id_tr + '<--------------');
        paking_tab[id_tr - 1].push(json);
        llenartablapakingunique(id_tr - 1);
        /*
        var row_tr='<tr>' +
            '           <td class="contador">'+(tablatem.length+1)+'</td>' +
            '           <td>'+json.orddeta_id.produ_id.produ_nombre+'</td>' +
            '           <td>'+json.orddeta_id.produ_id.mod_id.mod_nombre+'</td>' +
            '           <td>'+json.orddeta_id.produ_id.unidad_id.unidad_nombre+'</td>' +
            '           <td>'+valida(json.pack_cant)+'</td>' +
            '           <td class="text-center"> ' +
            '               <a id="btn-" data-index="'+(tablatem.length+1)+'" id-dp="'+json.orddeta_id.orddeta_id+'"  class="btn btn-sm btn-danger fa fa-times" title="Anadir item" data-dismiss="modal"></a>' +
            '                <input class="pack_id no-display" type="text"  value="'+json.pack_id+'">' +
            '                <input class="ordeta_id no-display" type="text" value="'+json.orddeta_id.orddeta_id+'">' +
            '           </td>' +
            '       </tr>';
        $('#modal_packing').modal('hide');
        $('#table-pack-tr'+(id_tr-1)).append(row_tr);*/
        contabilizarTabla();

        $('#modal-packing-btn-cerrar').click();
        $('#modal-packing-btn-limpiar').click();
    }

    function contabilizarTabla() {
        var count = 0;
        $(datos_table_n).each(function (i, v) {
            /* console.log(i);*/
            count = 0;
            $('#table-' + i + ' tbody tr').each(function (j, h) {
                var tmp_count = $(this).find('td').eq(4).text();
                count = parseInt(count) + parseInt(tmp_count);
            });
            $('#table-pack-footer' + i + ' tr').find('th').eq(2).text(count);
        });
        count = 0;
        $('#table-pack tbody tr').each(function (i, v) {
            var tmp_count = $(this).find('td').eq(5).text();
            count = parseInt(tmp_count) + parseInt(count);
            $('#table-pack-footer tr').find('th').eq(3).text(count);
        });


    }


    $('#modal-packing-btn-limpiar').click(function () {
        $('#modal-packing-input-nombre').val('');
    });
    $('#modal-packing-btn-cerrar').click(function () {
        $('#modal-packing-btn-limpiar').click();
    });

    /*mouseover*/

    $('.pd-t-20').mouseup(function () {
        var tage = $(this).attr('id');
        tage = tage.substring(tage.length - 1, tage.length);
        table_now = $('#table-' + (tage - 1)).attr('id');
        tabnow = table_now;
        var rowIndex = 0;
        $('#' + table_now + ' tr').click(function (evt) {
            rowIndex = $('#' + table_now + ' tr').index(this);
            console.log('#' + table_now + ' tr')
            console.log('INDEX   ' + rowIndex);
        });


        $('#' + table_now + ' tr td a').click(function (evt) {

            console.log("entro")
            console.log()
            /* var elementTemporalA =$(evt.currentTarget);
             var index = elementTemporalA.attr("data-index")
             var id = elementTemporalA.attr("id-dp");

             var id_l = $('#'+table_now+' tbody tr').eq(index-1).find('.pack_id').val();
             console.log( "index>>>>>>>>>>>>>>>>"+ '#'+table_now+' tbody tr')

             var cant = $('#'+table_now+' tbody tr').eq(index-1).find('td').eq(4).text();
             // var id = $('#'+ta+' tbody tr td input').eq(1).val();


             resetTable(cant,id);
             id=id_l;

             runAjaxId_none(id,
                 'Packing',
                 'deleteForID',
                 'Packing',
                 deletePacking);
             $('#'+table_now+' tbody tr').eq(index-1).remove();

             reasignacionProcicion(table_now);
             contabilizarTabla();*/


        });
    });
    var limitador = false;

    function reasignacionProcicion(table) {
        $("#" + table + " tbody tr").each(function (i) {
            $("#" + table + " tbody tr").eq(i).find("#btn-").eq(0).attr("data-index", i + 1);
            $("#table-0 tbody tr").eq(i).find("td.contador").text(i + 1)
        })
    }


    function deletePacking(json) {
        console.log(json);
    }

    function resetTable(cant, id_t) {
        $('#table-pack tbody tr').each(function (j, b) {
            var id = $(this).find('.pack-input-orden-id').val();
            if (id_t === id) {
                var elementCell = $("#row-unic-pck-value-" + id_t);

                var cantidad = elementCell.text();
                cantidad = parseInt(cantidad) + parseInt(cant);
                console.log(cant);
                console.log(cantidad);
                elementCell.text(cantidad);
                console.log("#row-unic-pck-" + id_t)
                //$(this).find('td').eq(5).text(cantidad);
            }

        });
        /*contabilizarTabla();*/
    }


    /*$('#table-pack').DataTable({
        /!*scrollY: false,*!/
        /!*scrollX: true,*!/
        paging: false,
        lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
        language: {
            url: '../assets/Spanish.json'
        }
    });*/

});


function reasignacionProcicion2(table) {
    $("#" + table + " tbody tr").each(function (i) {
        $("#" + table + " tbody tr").eq(i).find("#btn-").eq(0).attr("data-index", i + 1);
        $("#table-0 tbody tr").eq(i).find("td.contador").text(i + 1)
    })
}

function contabilizarTabla2() {
    var count = 0;
    $(datos_table_n).each(function (i, v) {
        console.log(i);
        count = 0;
        $('#table-' + i + ' tbody tr').each(function (j, h) {
            var tmp_count = $(this).find('td').eq(4).text();
            count = parseInt(count) + parseInt(tmp_count);
        });
        $('#table-pack-footer' + i + ' tr').find('th').eq(2).text(count);
    });
    count = 0;
    $('#table-pack tbody tr').each(function (i, v) {
        var tmp_count = $(this).find('td').eq(5).text();
        count = parseInt(tmp_count) + parseInt(count);
        $('#table-pack-footer tr').find('th').eq(2).text(count);
    });


}


function resetTable2(cant, id_t) {
    $('#table-pack tbody tr').each(function (j, b) {
        var id = $(this).find('.pack-input-orden-id').val();
        if (id_t === id) {
            var elementCell = $("#row-unic-pck-value-" + id_t);

            var cantidad = elementCell.text();
            cantidad = parseInt(cantidad) + parseInt(cant);
            console.log(cant);
            console.log(cantidad);
            elementCell.text(cantidad);
            console.log("#row-unic-pck-" + id_t)
            //$(this).find('td').eq(5).text(cantidad);
        }

    });
    /*contabilizarTabla();*/
}

function runAjaxArray2(array, file, method, title, func) {
    $.ajax({
        data: {'array': JSON.stringify(array)},
        url: '../ajax/' + file + '/' + method + '.php',
        type: 'POST',
        async: true,
        success: function (response) {
            var json = JSON.parse(JSON.stringify(response));
            console.log(json);
            if (json) {
                func(json);
            } else {
            }
        },
        error: function () {
            alerta.alerInfo('No se pudo cargar ' + title + '<br>Reporte este error!!');
        }
    });
}

function runAjaxId_none2(id, file, method, title, func) {
    $.ajax({
        data: {id},
        url: '../ajax/' + file + '/' + method + '.php',
        type: 'POST',
        async: true,
        success: function (response) {
            var json = JSON.parse(JSON.stringify(response));
            if (json) {
                func(json);
            } else {
            }
        },
        error: function () {
        }
    });
}