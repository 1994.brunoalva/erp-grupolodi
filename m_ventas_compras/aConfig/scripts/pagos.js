import alerta from "../alertToas.js";

$(document).ready(function () {
    setTimeout(calMTotal,1000);

    function calMTotal(){
        var mot= $('#pago-input-monto-pendiente').val();
        if(mot==='.NaN'){
            mot= $('#pago-input-monto-total').val();
           $('#pago-input-monto-pendiente').val(mot);
        }
    }


    $('#select_tasas').change(function () {
        var mtopagar =$('#pago-input-monto-pendiente').val();
        mtopagar=mtopagar.replace(/,/g,'');
        console.log(mtopagar);
        if(parseFloat(mtopagar)>0){
            var mot= $('#pago-input-monto-total').val();
            mot=mot.replace(/,/g,'');
            var por =$(this).find('option:selected').attr('name');
            por = parseFloat(por)/100;
            console.log(parseFloat(mot)*por);
            mot =(parseFloat(mot)*por);
            $('#pago-input-monto-pagar').val(mot.toFixed(2));
            TransformDecimal('pago-input-monto-pagar');
        }else{
            alerta.alerInfo('Ya esta cancelado!!');

        }

    });

    $('#pago-btn-guardar').click(function () {
        var mtopagar =$('#pago-input-monto-pendiente').val();

        var motototal = $("#pago-input-monto-total").val();
        var motoapagar = $("#pago-input-monto-pagar").val();

        motototal=motototal.replace(/,/g,'');
        motoapagar=motoapagar.replace(/,/g,'');
        mtopagar=mtopagar.replace(/,/g,'');
        console.log(mtopagar);
        if(parseFloat(mtopagar)>0){

            //console.log((parseFloat(mtopagar)-parseFloat(motoapagar))+"555555555555555555555555555555");

            if ((parseFloat(mtopagar)-parseFloat(motoapagar))>=0){
                var datPagos =[];

                datPagos.push(['pago_id',$('#pago-input-pago-id').val()]);
                datPagos.push(['impor_id',$('#pago-input-folder-id').val()]);
                datPagos.push(['pago_mon_total',$('#pago-input-monto-total').val()]);
                datPagos.push(['pago_mon_pend',$('#pago-input-monto-pendiente').val()]);
                datPagos.push(['estado','b1']);
                console.log(datPagos);
                runAjaxArray(datPagos,
                    'Pagos',
                    'updatePago',
                    'Pagos',
                    updatePago);



                var datDetaPagos =[];
                datDetaPagos.push('');
                datDetaPagos.push($('#select_tasas').val());
                datDetaPagos.push($('#pago-input-monto-pagar').val());
                datDetaPagos.push($('#pago-input-fecha').val());
                datDetaPagos.push($('#pago-input-pago-id').val());
                datDetaPagos.push($("#select_banco").val());
                datDetaPagos.push($("#input-num-operacion-pago").val());

                console.log(datDetaPagos);
                var filUp= $("#fileSwift")[0];
                if (filUp.files.length==0) {

                    datDetaPagos.push('');
                    datDetaPagos.push('b1');

                    runAjaxArray(datDetaPagos,
                        'DetallePagos',
                        'setDetallePago',
                        'Detallea de pagos',
                        addDetallePago);
                    $('#pago-btn-limpiar').click();
                    $("#input-num-operacion-pago").val("");
                    $("#fileSwift").val('');





            }else{
                var fd = new FormData();
                var files = filUp.files[0];
                fd.append('file',files);
                $.ajax({
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = ((evt.loaded / evt.total) * 100);
                            }
                        }, false);
                        return xhr;
                    },
                    type: 'POST',
                    url: '../ajax/ManagerFiles/upd_swift.php',
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        /*app._data.progreso=0;
                        app._data.starProgres=true;*/
                        $.toast({
                            heading: 'INFORMACION',
                            text: "Se esta subiendo el archivo",
                            icon: 'info',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                    },
                    error:function(err){
                       /* console.log(err)
                        alert('File upload failed, please try again.');*/
                        $.toast({
                            heading: 'ERROR',
                            text: "Error al subir el Archivo",
                            icon: 'error',
                            position: 'top-center',
                            hideAfter: '2500',
                        });

                    },
                    success: function(resp){
                        console.log(resp)


                            var obj = JSON.parse(resp);


                            datDetaPagos.push(obj.dstos);
                            datDetaPagos.push('b1');

                            runAjaxArray(datDetaPagos,
                                'DetallePagos',
                                'setDetallePago',
                                'Detallea de pagos',
                                addDetallePago);
                            $('#pago-btn-limpiar').click();
                        $("#input-num-operacion-pago").val("")


                    }
                });
            }

            }else{
                $.toast({
                    heading: 'INFORMACION',
                    text: $("#pago-input-monto-pagar").val()==""? "Seleccione una condición de pago":"Monto a pagar Superior al pendiente",
                    icon: 'info',
                    position: 'top-right',
                    hideAfter: '2500',
                });
                $("#select_tasas").focus();

            }
        }else{
            alerta.alerInfo('Ya esta cancelado!!');
        }


    });

    $('#pago-btn-limpiar').click(function () {
        $('#pago-input-monto-pagar').val('');
    });
    setTimeout(function () {
        calcularSaldo();
    },100)
    function calcularSaldo(){
        var mot= $('#pago-input-monto-total').val();
        mot =mot.replace(/,/g,'');
        /*console.log(mot);*/
        var sumMon=0;
        $('#table-pagos tbody tr').each(function () {
            var val = $(this).find('td').eq(2).text();

            val =val.replace(/,/g,'');
            val =val.replace("$",'');
            sumMon = parseFloat(sumMon)+parseFloat(val);
        });
        mot = parseFloat(mot)-parseFloat(sumMon);
       /* console.log(mot.toFixed(2));*/

           /* console.log(mot);
            mot = $('#pago-input-monto-total').val();
            $('#pago-input-monto-pendiente').val(mot.toFixed(2));*/
        $('#pago-input-monto-pendiente').val(mot.toFixed(2));
        TransformDecimal('pago-input-monto-pendiente');
        $('#pago-input-monto-pendiente').change();

    }

    funtionPagRecal =calcularSaldo;
    $('#pago-input-monto-pendiente').change(function () {
        var val = $(this).val();
        if(val === '.NaN'){
            val = $('#pago-input-monto-total').val();
            $('#pago-input-monto-pendiente').val(val);
        }
    });

    $('#table-pagos').on('click', 'tr td a', function (evt) {
        var id = $(this).parents('tr').eq(0).find('input').val();
        var fila = $(this).parents('tr').eq(0).index();
        console.log(fila+ " ///////////")

        runAjaxId(id,
            'DetallePagos',
            'deleteForID',
            'eliminar detalle',
            deleteDetalle);

        var t = $('#table-pagos').DataTable();
        t.row(fila).remove().draw();
        //delete t.row(fila);
        console.log(id);
        //resetIndex();

        calcularSaldo();




    });
    function resetIndex() {
        var i = 0;
        var nFilas = $("#table-pagos tr").length;
        console.log(nFilas);
        $('#table-pagos tr').each(function () {
            var indice = $(this).find('td').eq(0).text(i++);
        });
    }

    function runAjaxArray(array, file, method, title, func) {
        $.ajax({
            data: {'array': JSON.stringify(array)},
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json) {
                    func(json);
                } else {
                    /*clearSelect(puerto);*/
                }
            },
            error: function () {
                alerta.alerInfo('No se pudo cargar ' + title + '<br>Reporte este error!!');
            }
        });
    }

    function runAjaxId(id, file, method, title, func) {
        $.ajax({
            data: {id},
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json) {
                    func(json);
                } else {
                    /*clearSelect(puerto);*/
                }
            },
            error: function () {
                alerta.alerError('Error al cargar ' + title + '<br>Reporte este error!!');
            }
        });
    }
    function deleteDetalle() {
        console.log('eliminado detalle');
    }
    function updatePago(json) {
            console.log('updatePago');
    }
    function addDetallePago(json) {
        console.log('ingresado = '+json);
        var t = $('#table-pagos').DataTable();
        var info = t.page.info();
        var count = info.recordsTotal;
        console.log(count);
        console.log(json);
        if (json) {
            t.row.add([

                '<div class="text-center">'+json.conpa_id.conpa_nombre+'</div>',
                '<div class="text-center">'+json.detpa_fecha+'</div>',
                '<div class="text-center">$'+json.detpa_monto+'</div>',
                '<td class="text-center">' +((json.src_file&&json.src_file.length>0)?
                '<button style="display: block; margin: auto;" onclick="open_newfill(\'../imagenes/doc-folder/pago/'+json.src_file+'\')" class="btn btn-info"><i class="fa fa-file"></i></button>':"") +
                '</td>',
                '<div class="text-center">'+
                    '<a id="btn-pa" class="btn btn-sm btn-danger fa fa-times btn-option" title="eliminar item" data-dismiss="modal"></a>'+
                    '<input class="depa_id no-display" type="text" value="'+json.detpa_id+'">'+
                '</div>'
            ]).draw(false);
        }
        calcularSaldo();
    }








    var index = 0;
    function TransformDecimal(ele) {
        var numero ='';
        var moneyText = '';
        moneyText =  $('#'+ele).val();

        var decimal='';
        index = moneyText.indexOf('.');
        decimal='.'+moneyText.substring(index+1);
        moneyText=moneyText.replace(/,/g, '');
        moneyText= moneyText.substring(0, index);
        /*console.log('moneyText');
        console.log(moneyText);*/
        numero='';
        moneyText = moneyText.split('').reverse().join('');
        for (let i = moneyText.length-1; i >= 0; --i) {
            numero += moneyText.charAt(i);
            if(i%3===0&&i>0){
                numero += ',';
            }
        }
        $('#'+ele).val(numero+decimal);
    }
});