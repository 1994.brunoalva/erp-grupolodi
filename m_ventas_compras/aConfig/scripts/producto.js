import alerta from "../alertToas.js";
import images from "../Myjs/Images.js";

$(document).ready(function () {


    /*MODAL PRODUCTOS - IMPORTANTE*/
    $('#box-nomenclatura').hide();
    $('.bo-modelo-pro').hide();
    $('#select_modal_pro_categoria').change(function () {
        $('.box-mutel').show();
        var categoria = $(this).find('option:selected').text();
        console.log(categoria);
        var index = $('#select_modal_pro_categoria').val();
        if (index > 0) {
            $('#item-produ-detalle').show();

        } else {
            $('#item-produ-detalle').hide();

        }
        if (categoria == 'NEUMATICOS') {
            $('.NEUMATICO').show();
            $('.CAMARA').hide();
            $('.inpt-descripcion').hide();
            $('.box-accesorios').hide();
            $('.ARO').hide();
            $('#box-nomenclatura').show();
            $('.box-siempre-show').show();
            $('.bo-modelo-pro').hide();
        } else if (categoria == 'CAMARAS') {
            $('.box-accesorios').hide();
            $('.inpt-descripcion').hide();
            $('.NEUMATICO').hide();
            $('.CAMARA').show();
            $('.ARO').hide();
            $('#box-nomenclatura').hide();
            $('.bo-modelo-pro').hide();
        } else if (categoria == 'AROS') {
            $('.box-accesorios').hide();
            $('.inpt-descripcion').hide();
            $('.NEUMATICO').hide();
            $('.CAMARA').hide();
            $('.ARO').show();
            $('#box-nomenclatura').hide();
            $('.bo-modelo-pro').hide();
            $('.box-siempre-show').show();
        }  else if (categoria == catTe) {
            $('.box-accesorios').hide();
            $('.inpt-descripcion').hide();
            $('.NEUMATICO').hide();
            $('.CAMARA').hide();
            $('.ARO').hide();
            $('#box-nomenclatura').hide();
            $('.bo-modelo-pro').hide();
            $('.box-siempre-show').hide();
            $('.box-varios').show();
            $('.box-mutel').hide();
        }else {
            $('.box-accesorios').hide();
            $('.inpt-descripcion').show();
            $('.NEUMATICO').hide();
            $('.CAMARA').hide();
            $('.ARO').hide();
            $('#box-nomenclatura').hide();
            $('.box-siempre-show').show();
            $('.bo-modelo-pro').hide();

        }
        if (categoria == 'ACCESORIOS') {
            $('.box-accesorios').show();
        }
        alistarMedidas(index,categoria);
    });

    function alistarMedidas(index,catSelect){
        $('#select_modal_medida').prop('disabled',false);
        $('#select_modal_medida').val('0').change();
        if(catSelect!=='NEUMATICOS'){
            $('#select_modal_medida').prop('disabled',true);
            $('#select_modal_medida option').last().show();
            var op = $('#select_modal_medida option').last().val();
            $('#select_modal_medida').val(10).change();
        }else{
            $('#select_modal_medida').prop('disabled',false);
            $('#select_modal_medida option').hide();
            $('#select_modal_medida option').each(function () {
                if ($(this).val()<=9){
                    $(this).show();
                }

            });
            //$('#select_modal_medida option').last().hide();
        }
        $('#select_modal_medida').selectpicker('refresh');
    }
    $('#select_modal_medida option').last().hide();

    $('#select_modal_prod_modelo').prop('disabled', true);
    $('#producto_modal_btn_select_modelo').prop('disabled', true);

    $('#select_modal_pro_categoria').change(function () {
        genNombreProducto();
        if ($(this).val() > 0) {
            $('#select_modal_prod_marca').prop('disabled', false);
            $('#producto_modal_btn_select_marca').prop('disabled', false);
            $('#select_modal_prod_modelo').prop('disabled', false);
            $('#producto_modal_btn_select_modelo').prop('disabled', false)
            var id = $(this).val();
            chargeMarca(id, 'modal_prod_marca');
            setTimeout(function () {
                if ($('#select_modal_prod_marca').val()!=null && $('#select_modal_prod_marca').val().length>0){

                    $('#select_modal_prod_modelo').prop('disabled', false);
                    $('#producto_modal_btn_select_modelo').prop('disabled', false);
                    chargeModelo($('#select_modal_prod_marca').val(),'modal_prod_modelo');
                }
            },100)
        } else {
            $('#select_modal_prod_marca').prop('disabled', true);
            $('#producto_modal_btn_select_marca').prop('disabled', true);
            $('#select_modal_prod_modelo').prop('disabled', true);
            $('#producto_modal_btn_select_modelo').prop('disabled', true);
            clearSelect('modal_prod_marca');
            clearSelect('modal_prod_modelo');
        }
    });
    $('#select_modal_prod_marca').change(function () {
        genNombreProducto();
        if ($(this).val() > 0) {
            $('#select_modal_prod_modelo').prop('disabled', false);
            $('#producto_modal_btn_select_modelo').prop('disabled', false);
            var id = $(this).val();
            chargeModelo(id,'modal_prod_modelo');
        } else {
            $('#select_modal_prod_modelo').prop('disabled', true);
            $('#producto_modal_btn_select_modelo').prop('disabled', true);
            clearSelect('modal_prod_modelo');
        }
    });
    /*MODAL PRODUCTOS - IMPORTANTE*/









    /*TABLA SELECCIONAR PRODUCTOS*/


















    $('#table-productos').on('click', 'tr td #btn-sku', function (evt) {
        var sku_text = $(this).parents('tr').eq(0).find('#btn-sku').text() ;
        verSku(sku_text);
        $('#modal_sku').modal('show');
    });
    function verSku(sku_text){
        JsBarcode('#barcode', sku_text, {
            lineColor: "#000",
            height: 30,
            displayValue: true
        });

    }


    $('#table-productos').on('click', 'tr td #btn-a', function (evt) {
        var catgoria = $(this).parents('tr').eq(0).find('#btn-cat').text();
        if (catgoria == 'NEUMATICOS'){
            $("#box-presentacion").hide();
            $("#box-cant-presentacion").hide();
        }else{
            $("#box-presentacion").show();
            $("#box-cant-presentacion").show();
        }
        var id = $(this).parents('tr').eq(0).find('input').val();
        var name = $(this).parents('tr').eq(0).find('label').text();
        var m_id = $(this).parents('tr').eq(0).find('#med-data').text();
        $('#producto-input-productos').val(name);
        $('#producto-input-productos-id').val(id);
        $('#select_medida').val(m_id).change();
        setTimeout(limpiarSelector, 200);
    });
    $('#producto-input-productos').focus(function () {
        $('#modal_buscar_producto').modal('show');
    });
    $('#modal-buscar-productos-btn-cerrar').click(function () {
        setTimeout(limpiarSelector, 200);
    });
    function limpiarSelector() {
        $('#select_pro_marca').val('0').change();
        $('#select_pro_categoria').val('0').change();
        $('#CATEGORIA').val('');
        $('#CATEGORIA').keyup();
        $('#MARCA').val('');
        $('#MARCA').keyup();
        $('#MODELO').val('');
        $('#MODELO').keyup();
    }

    /*TABLA SELECCIONAR PRODUCTOS*/

    $('#select_pro_marca').prop('disabled', true);
    $('#producto_btn_select_marca').prop('disabled', true);
    $('#select_pro_categoria').change(function () {
        var categoria = $(this).find('option:selected').text();
        if ($(this).val() > 0) {
            $('#select_pro_marca').prop('disabled', false);
            $('#producto_btn_select_marca').prop('disabled', false);
            var id = $(this).val();
            chargeMarca(id, 'pro_marca');
            $('#CATEGORIA').val(categoria);
        } else {

            $('#select_pro_marca').prop('disabled', true);
            $('#producto_btn_select_marca').prop('disabled', true);
            clearSelect('pro_marca');
            $('#CATEGORIA').val('');
        }
        $('#CATEGORIA').keyup();
        $('#MARCA').val('');
        $('#MARCA').keyup();
        $('#MODELO').val('');
        $('#MODELO').keyup();
    });



    /*MODAL MARCA  SELECT MARCA*/
    $('#select_pro_modelo').prop('disabled', true);
    $('#folio_btn_select_modelo').prop('disabled', true);
    $('#select_pro_marca').change(function () {
        var marca = $(this).find('option:selected').text();
        console.log(marca);
        if ($(this).val() > 0) {
            $('#select_pro_modelo').prop('disabled', false);
            $('#folio_btn_select_modelo').prop('disabled', false);
            $('#MARCA').val(marca);
            var id = $(this).val();
            chargeModelo(id,'pro_modelo');
        } else {
            $('#select_pro_modelo').prop('disabled', true);
            $('#folio_btn_select_modelo').prop('disabled', true);
            $('#MARCA').val('');
            clearSelect('pro_modelo');
        }
        $('#MARCA').keyup();
        $('#MODELO').val('');
        $('#MODELO').keyup();
    });
    /*MODAL MODELO  SELECT MODELO*/

    $('#select_pro_modelo').change(function () {
        var modelo = $(this).find('option:selected').text();
        console.log(modelo);
        if ($(this).val() > 0) {
            $('#MODELO').val(modelo);
        } else {
            $('#MODELO').val('');
        }
        $('#MODELO').keyup();
    });


    function chargeMarca(id, marca) {
        $.ajax({
            data: {id},
            url: '../ajax/Marca/getMarcaForCategoria.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length > 0) {
                    alerta.alerSuccess('Cargando marca de producto');
                    selectMarca(json, marca);
                } else {
                    alerta.alerInfo('Agregue marca para esta categoria');
                    clearSelect(marca);
                }
            },
            error: function () {
                alerta.alerError('Error al cargar marca por categoria<br>Reporte este error!!');
            }
        });
    }
    function selectMarca(json, marca) {
        clearSelect(marca);
        $(json).each(function (i, v) {
            $('#select_' + marca).append('<option value="' + v.mar_id + '">' + v.mar_nombre + '</option>');
            $('#select_' + marca).selectpicker('refresh');
        });
    }
    function clearSelect(select) {
            $('#select_' + select).find('option').remove();
            //$('#select_' + select).append('<option value="0" selected>-Seleccione-</option>');
            $('#select_' + select).selectpicker('refresh');
    }
    /*MODAL MARCA*/


    /*MODAL MODELO*/
    function readImage(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-file-modelo-preview-zone').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#input-file-image").change(function () {
        readImage(this);
        var url = '';
        var valor = $('#input-file-image').val();
        for (var i = 0; i < valor.length; i++) {
            if (i >= 12) {
                url += valor[i];
            }
        }

        $("#label-file-image").text(url);
    });
    $("#modal-modelo-btn-eliminar-logo").click(function () {
        $("#modal-modelo-file-logo").val('');
        $("#input-file-image").val('');
        $("#label-file-image").text('( Ninguna imagen seleccionada.... )');
        $('#img-file-modelo-preview-zone').attr('src', '../imagenes/logo/img-icon.svg');
    });
    $('#modal-modelo-btn-limpiar').click(function () {
        limpiarModalModelo();
    });
    $('#modal-modelo-btn-cerrar').click(function () {
        limpiarModalModelo();
    });
    $('#modal-modelo-btn-guardar').click(function () {
        var dataInput = $('#modal-modelo-input-nombre').val();
        var dataFile = $('#input-file-image').val();
        if (dataInput.length > 0) {
           // if (dataFile.length > 0) {
                insertarModelo();
            /*} else {
                alerta.alerInfo('Porfavor asigne logo para este modelo');
            }*/
        } else {
            alerta.alerInfo('Porfavor asigne un nombre a este modelo');
        }

    });
    function insertarModelo() {
        var nombreModel = '';
        if ($('#input-file-image').val().length>0){
            var frmData = new FormData();
            frmData.append('image-file', $("#input-file-image")[0].files[0]);
            frmData.append('image-name', getNameImage());
            frmData.append('image-set-ruta', '../../imagenes/modelo/');
            console.log('getNameImage()');
            console.log(getNameImage());
            images.set(frmData);
            nombreModel = getNameImage();
        }

        var modeloData = [];
        modeloData.push('');
        modeloData.push($('#modal-modelo-input-nombre').val());
        var esVisible = $("#modal_producto").is(":visible");
        if(esVisible){
            console.log('MODAL PRODUCTO ES VISIBLE');
            modeloData.push($('#select_modal_prod_marca').val());
        }else{
            modeloData.push($('#select_pro_marca').val());
        }

        modeloData.push(nombreModel);
        modeloData.push('b1');
        console.log(modeloData);
        setModelo(modeloData);
    }
    function getNameImage() {
        var name = '';
        name = $("#modal-modelo-input-nombre").val();
        name += '_' + $('#select_categoria').val();
        /*var name = valor.substring(valor.length - 4, valor.length);*/
        console.log(name);
        return name;
    }
    function limpiarModalModelo() {
        $("#modal-modelo-btn-eliminar-logo").click();
        $("#modal-modelo-input-nombre").val('');
        $("#input-file-image").val('');
    }
    function chargeModelo(id,modelo) {
        $.ajax({
            data: {id},
            url: '../ajax/Modelo/getModeloForMarca.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length > 0) {
                    alerta.alerSuccess('Cargando Modelo de producto');
                    selectModelo(json,modelo);
                } else {
                    alerta.alerInfo('Agregue Modelo para esta Marca');
                    clearSelect(modelo);
                }
            },
            error: function () {
                alerta.alerError('Error al cargar Modelo por Marca<br>Reporte este error!!');
            }
        });
    }
    function setModelo(marcaData) {
        $.ajax({
            data: {'array': JSON.stringify(marcaData)},
            url: '../ajax/Modelo/setModeloAndGetRow.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json.length !== 0) {
                    alerta.alerSuccess('Se ha añadido una nueva marca');
                    var modeloRefresh=['pro_modelo','modal_prod_modelo'];
                    $(modeloRefresh).each(function (i, v) {
                        $('#select_' + v).append('<option value="' + json.mod_id + '">' + json.mod_nombre + '</option>');
                        $('#select_' + v).selectpicker('refresh');
                    });
                    limpiarModalModelo();
                    $('#modal_modelo').modal('hide');

                } else {
                    alerta.alerInfo('No se pudo añadir nueva marca');
                }

            },
            error: function () {
                alerta.alerError('Error al crear nueva marca<br>Reporte este error!!');
            }
        });

    }
    function selectModelo(json, modelo) {
        clearSelect(modelo);
        $(json).each(function (i, v) { // indice, valor
            $('#select_' + modelo).append('<option value="' + v.mod_id + '">' + v.mod_nombre + '</option>');
        });
        $('#select_' + modelo).selectpicker('refresh');
    }
    /*MODAL MODELO*/









    /*MODAL BUSCAR CODIGO SUNAT*/
    var filter = '';
    $('#modal-producto-input-codigo-sunat').focus(function () {
        $('#modal_buscar_codigo_sunat').modal('show');
    });
    $('#table-codigo-sunat').on('click', 'tr td a', function (evt) {
        var id = $(this).parents('tr').eq(0).find('input').val();
        var name = $(this).parents('tr').eq(0).find('label').text();
        var title = $(this).parents('tr').eq(0).find('#desc').text();
        $('#modal-producto-input-codigo-sunat').val(name);
        $('#modal-producto-input-codigo-sunat').prop('title', name + ' = ' + title);
        $('#modal-producto-input-codigo-sunat-id').val(id);
        setTimeout(clearImputFilter, 200);
        filter = 'input-search-cods';
    });
    $('.input-search-cods').focus(function () {
        $('.input-search-cods').keyup(function () {
            var letra = $('.input-search-cods').val();
            $('#table-codigo-sunat_filter label input').val(letra);
            $('#table-codigo-sunat_filter label input').keyup();
        });
    });

    /*MODAL BUSCAR CODIGO SUNAT*/

    function clearImputFilter() {
        $('.' + filter).val('');
        $('.' + filter).keyup();
    }


    /*$('#select_medida option').each(function(){
       $(this).hide();
    });
    $('#select_medida option').last().show();
    $('#select_medida option').last().attr("selected",true).change();
    $('#select_medida').prop('disabled',true);*/
    /* $('#select_medida option').each(function(){
         $(this).show();
     });
     $('#select_medida option').last().hide();*/
});
