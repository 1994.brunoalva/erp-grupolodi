// import { colorCode } from './first.js'


/*const MODAL ={
    cliente:1
};*/

const catTe='OTROS';

function alerInfo(msg) {
    $.toast({
        heading: 'INFORMACION',
        text: msg,
        icon: 'info',
        position: 'top-right',
        hideAfter: '2500',
    });
}


var swall =swal;
/*this.agenciatrasporte.dataRegistro.id=tans.id;
this.agenciatrasporte.dataRegistro.ruc=tans.ruc;
this.agenciatrasporte.dataRegistro. nombresocial=tans.razon_social;
this.agenciatrasporte.dataRegistro. direccion=tans.direccion;
this.agenciatrasporte.dataRegistro.estado=tans.estado;
this.agenciatrasporte.dataRegistro.telefono=tans.telefono;
this.agenciatrasporte.dataRegistro.condicion=tans.condicion;*/
//MODALES.selecdClienteEdit()
departamentoCod=0;
provincia=0;

var MODALES = new Vue({
    el:"#conten-modales",
    data:{
        url_ruc:"../ajax/libSunat/sunat/example/consultaRuc.php",
        url_dni:"../ajax/Dni/consultaDNI.php",
        productos:{
            url_data:"../ajax/Producto/get_all_data.php",
            id_dataTable:'#table-productos',
            iniciarDatos:false,
            isSelected:false,
            view_std:true,
            lista:[],
            sku:{
                cat :'',
                mar :'',
                uso :'',
                aro :'',
                ancho :'',
                tipo :'',
                medida :'',
                pais :'',
                desc :'',
                pn :'',
            },
            productoSelected:{},
            moduladores:{
                global:false,
                global2:false,
                isNeumatico:false,
                isCamara:false,
                isAros:false,
                isProtectores:false,
                isAccesorios:false,
            },
            dataRegistro:{
                peso:'',
                idCategoria:0,
                idMarca:'',
                idModelo:'',
                idCodSunat:'',
                CodSunat:'',
                sku:'',
                idUndMedida:'',
                descripcion:'',
                nombre:'',
                pn:'',
                idTipo:'',
                idnomenglatura:'',
                isPais:'',
                ancho:'',
                serie:'',
                aro:'',
                pliegles:'',
                Uso:'',
                Uso2:'',
                partiAran:'',

                /* camra*/
                medidaCamara:'',
                valvula:'',
                camAro:'',
                /*aros*/
                medidaAro:'',
                modeloAro:'',
                espesormm:'',
                huecos:'',

                espesorhueco:'',
                CBD:'',
                PCD:'',
                offSet:'',


                material:'',
                ancho_aduana:'',
                serie_aduana:'',
                tipo_construccion:'',
                indice_carga:'',
                velocidad:'',
                constancia:'',
                item_const:'',
                vigencia:'',
                conformidad:'',
                suce:'',


            },
            indexA:-1
        },
        marcasProducto:{
            lista:[],
        },
        modeloProducto:{
            lista:[],
        },
        agenciaTansporte:{
            url_data:"../ajax/AgensiaTransporte/get_all_data.php",
            url_registro:"../ajax/AgensiaTransporte/set_dato.php",
            url_edit:"../ajax/AgensiaTransporte/udt_dato.php",
            id_dataTable:'#table-agencia-transporte',
            iniciarDatos:false,
            isSelected:false,
            lista:[],
            agenciaSelected:{},
            indexSeled:-1,
            dataRegistro:{
                ruc:'',
                nombresocial:'',
                direccion:'',
                estado:'',
                telefono:'',
                condicion:''
            }

        },
        clientes:{
            isSelected:false,
            url_data:"../ajax/ServerSide/serversideClientes.php",
            url_registro:"../ajax/Clientes/set_clientes.php",
            url_edit:"../ajax/Clientes/udt_clientes.php",
            id_dataTable:'#table-empresa',
            iniciarDatos:false,
            lista:[],
            indexSeled:-1,
            clienteSelected:{},
            clasificaciones:[],
            departamentos:[],
            distritos:[],
            provincia:[],
            visualDireciones:false,
            cod_distrito:'',
            dataRegistro:{
                direciones:[],
                representantes:[],
                ruc:'',
                indexDireccion:1,
                nombresocial:'',
                direccion:'',
                departamento:'',
                provincia:'',
                distrito:'',
                ubigeo:'',
                estado:'',
                condicion:'',
                email1:'',
                email2:'',
                email3:'',
                telefono:'',
                clasificacion:'',
                contacto:'',
                vendedor:''
            }
        },

    },
    methods:{
        onlyNumber ($event) {
            //console.log($event.keyCode); //keyCodes value
            let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
            if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                $event.preventDefault();
            }
        },
        eventoNombreProduct(){
            genNombreProducto()
        },
        guardarMarca(){
            $("#modal-marca-input-nombre")
            //$("#input-file-image")

            if ( $("#input-file-image").val().length>0){
                var frmData = new FormData();
                frmData.append('file', $("#input-file-image")[0].files[0]);
                //frmData.append('image-name', );
                frmData.append('folder', 'logo');

                $.ajax({
                    data: frmData,
                    url: '../ajax/ManagerFiles/upd_img_mm.php',
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        console.log(response);
                        var json = JSON.parse(response);
                        if (json.res){
                            $.ajax({
                                type: "POST",
                                url: "../ajax/Marca/setNewMarca.php",
                                data: {
                                    idc:$("#select_modal_pro_categoria").val(),nom:$("#modal-marca-input-nombre").val(),log:json.datos
                                },
                                success: function (data) {
                                    console.log(data)
                                    if (isJson(data)){
                                        var jsn=JSON.parse(data)
                                        if (jsn.res){
                                            MODALES._data.marcasProducto.lista.push(jsn.data)
                                            $('#modal_marca').modal('hide');
                                            $("#modal-marca-input-nombre").val("")
                                            $("#input-file-image").val("")
                                            $("#img-file-preview-zone").attr("src","../imagenes/logo/img-icon.svg");
                                            setTimeout(function () {
                                                $(".selectpicker").selectpicker('refresh');
                                            },100)
                                        }
                                    }
                                }
                            });
                        }
                        /*if (json) {
                            alerta.alerSuccess('Cargando nueva imagen');
                        } else {
                            alerta.alerInfo('no se pudo cargar nueva imagen');
                        }*/
                    },
                    error: function (err) {
                        console.log(err)
                        //alerta.alerError('Error al cargar imagen<br>Reporte este error!!');
                    }
                });
            }else{
                $.ajax({
                    type: "POST",
                    url: "../ajax/Marca/setNewMarca.php",
                    data: {
                        idc:$("#select_modal_pro_categoria").val(),nom:$("#modal-marca-input-nombre").val(),log:''
                    },
                    success: function (data) {
                        console.log(data)
                        if (isJson(data)){
                            var jsn=JSON.parse(data)
                            if (jsn.res){
                                MODALES._data.marcasProducto.lista.push(jsn.data)
                                $('#modal_marca').modal('hide');
                                $("#modal-marca-input-nombre").val("")
                                $("#input-file-image").val("")
                                $("#img-file-preview-zone").attr("src","../imagenes/logo/img-icon.svg");
                                setTimeout(function () {
                                    $(".selectpicker").selectpicker('refresh');
                                },100)
                            }
                        }
                    }
                });
            }


        },
        guardarModelo(){

            //$("#input-file-image")

            if ($("#input-file-image-modelo").val().length>0){
                var frmData = new FormData();
                frmData.append('file', $("#input-file-image-modelo")[0].files[0]);
                //frmData.append('image-name', );
                frmData.append('folder', 'modelo');

                $.ajax({
                    data: frmData,
                    url: '../ajax/ManagerFiles/upd_img_mm.php',
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        console.log(response);
                        var json = JSON.parse(response);
                        if (json.res){
                            $.ajax({
                                type: "POST",
                                url: "../ajax/Modelo/setNewModelo.php",
                                data: {
                                    idc:$("#select_modal_prod_marca").val(),nom:$("#modal-modelo-input-nombre").val(),log:json.datos
                                },
                                success: function (data) {
                                    console.log(data)
                                    if (isJson(data)){
                                        var jsn=JSON.parse(data)
                                        if (jsn.res){
                                            MODALES._data.modeloProducto.lista.push(jsn.data)
                                            $('#modal_modelo').modal('hide');
                                            $("#modal-modelo-input-nombre").val("")
                                            $("#input-file-image-modelo").val("")
                                            $("#img-file-preview-zone-modelo").attr("src","../imagenes/logo/img-icon.svg");
                                            setTimeout(function () {
                                                $(".selectpicker").selectpicker('refresh');
                                            },100)
                                        }
                                    }
                                }
                            });
                        }
                        /*if (json) {
                            alerta.alerSuccess('Cargando nueva imagen');
                        } else {
                            alerta.alerInfo('no se pudo cargar nueva imagen');
                        }*/
                    },
                    error: function (err) {
                        console.log(err)
                        //alerta.alerError('Error al cargar imagen<br>Reporte este error!!');
                    }
                });
            }else{
                $.ajax({
                    type: "POST",
                    url: "../ajax/Modelo/setNewModelo.php",
                    data: {
                        idc:$("#select_modal_prod_marca").val(),nom:$("#modal-modelo-input-nombre").val(),log:''
                    },
                    success: function (data) {
                        console.log(data)
                        if (isJson(data)){
                            var jsn=JSON.parse(data)
                            if (jsn.res){
                                MODALES._data.modeloProducto.lista.push(jsn.data)
                                $('#modal_modelo').modal('hide');
                                $("#modal-modelo-input-nombre").val("")
                                $("#input-file-image-modelo").val("")
                                $("#img-file-preview-zone-modelo").attr("src","../imagenes/logo/img-icon.svg");
                                setTimeout(function () {
                                    $(".selectpicker").selectpicker('refresh');
                                },100)
                            }
                        }
                    }
                });
            }

        },
        setCodigoSunatRegister(id, numero){
            this.productos.dataRegistro.idCodSunat=id;
            this.productos.dataRegistro.CodSunat=numero;
        },
        resetDataRegisterProducto(val){
            idproducto=0;
            idproductoDeta=0;
            this.productos.view_std=true;
            this.productos.moduladores={
                global:false,
                marcaSS:true,
                global2:false,
                isNeumatico:false,
                isCamara:false,
                isAros:false,
                isProtectores:false,
                isAccesorios:false,
            };
            this.productos.dataRegistro={
                idCategoria:  val? this.productos.dataRegistro.idCategoria:0,
                idMarca:'',
                idModelo:'',
                idCodSunat:'',
                CodSunat:'',
                sku:'',
                idUndMedida:'',
                descripcion:'',
                nombre:'',
                pn:'',
                idTipo:'',
                idnomenglatura:'',
                isPais:'',
                ancho:'',
                serie:'',
                aro:'',
                pliegles:'',
                Uso:'',
                Uso2:'',
                partiAran:'',
                peso:'',

                /* camra*/
                medidaCamara:'',
                valvula:'',
                camAro:'',
                /*aros*/
                medidaAro:'',
                modeloAro:'',
                huecos:'',
                espesorhueco:'',
                CBD:'',
                PCD:'',
                offSet:''


            };
        },
        llenarDepartamentos(){
            $.ajax({
                type: "GET",
                url: '../ajax/Localidad/lista_departamento.php',
                success: function (data) {
                   // console.log(data)
                    MODALES._data.clientes.departamentos= JSON.parse(data)
                }
            });

        },
        cambioDepartamento(evt){
            var codDep= evt.target.value;
            departamentoCod = codDep;
            $.ajax({
                type: "GET",
                url: '../ajax/Localidad/lista_provincia.php?provin='+codDep,
                success: function (data) {
                    console.log(data)
                    MODALES._data.clientes.provincia= JSON.parse(data)
                }
            });
        },
        cambioProvincia(evt){

            provincia=evt.target.value;
            $.ajax({
                type: "GET",
                url: '../ajax/Localidad/lista_distrito.php?provin='+evt.target.value+"&depart="+departamentoCod,
                success: function (data) {
                    console.log(data)
                    MODALES._data.clientes.distritos= JSON.parse(data)
                }
            });
        },
        cambioDistrito(index){
            console.log(index+"--------------")
            this.clientes.dataRegistro.direciones[parseInt(index)].departamento= $( "#depart"+index+" option:selected" ).text();
            this.clientes.dataRegistro.direciones[parseInt(index)].provincia= $( "#prov"+index+" option:selected" ).text();
            this.clientes.dataRegistro.direciones[parseInt(index)].distrito= $( "#dist"+index+" option:selected" ).text();
            this.clientes.dataRegistro.direciones[parseInt(index)].ubigeo=this.clientes.cod_distrito+provincia+departamentoCod;
        },
        selecdClienteEdit(idCliente){
            //console.log(idCliente);
            $.ajax({
                type: "POST",
                url: "../ajax/Clientes/get_data_cliente.php",
                data: {id:idCliente},
                success: function (data) {
                  //  console.log(data);
                    var clie = JSON.parse(data);
                    MODALES._data.clientes.dataRegistro={
                        id:clie.id,
                        ruc:clie.ruc,
                        nombresocial:clie.razon_social,
                        direciones:clie.direcciones,
                        estado:clie.estado_sunat,
                        condicion:clie.condicion_sunat,
                        email1:clie.email1,
                        email2:clie.email2,
                        email3:clie.telefono2,
                        telefono:clie.telefono,
                        clasificacion:clie.id_clasificacion,
                        contacto:clie.contacto,
                        vendedor:clie.vendedor_responsable
                    };
                   /* setTimeout(function () {
                        $('#id_clasificacion').selectpicker("refresh");
                    },100)*/

                }
            });
        },
        nuwDirecCliente(){
            MODALES._data.clientes.dataRegistro.direciones.push({
                id:-1,
                idagencia:-1,
                nomagencia:"",
                idcliente:-1,
                direccion:'',
                departamento:'',
                distrito:'',
                provincia:'',
                tipo:"",
                ubigeo:"",
                st:true
            });
        },
        selectItemAgencia(index){
            var tans = this.agenciaTansporte.lista[index];
            this.agenciaTansporte.indexSeled = index;
            this.agenciaTansporte.dataRegistro={
                id:tans.id,
                ruc:tans.ruc,
                nombresocial:tans.razon_social,
                direccion:tans.direccion,
                estado:tans.estado,
                telefono:tans.telefono,
                condicion:tans.condicion
            };

        },
        limpiarDataRegisteragencia(){
          this.agenciaTansporte.dataRegistro={
                ruc:'',
                    nombresocial:'',
                    direccion:'',
                    estado:'',
                    telefono:'',
                    condicion:''
            };
        },
        selectesItemClien(index){
            var clie = this.clientes.lista[index];
            this.clientes.indexSeled = index;
            this.clientes.dataRegistro={
                id:clie.id,
                ruc:clie.ruc,
                nombresocial:clie.razon_social,
                direccion:clie.direccion,
                departamento:clie.departamento,
                provincia:clie.provincia,
                distrito:clie.distrito,
                ubigeo:clie.ubigeo,
                estado:clie.estado_sunat,
                condicion:clie.condicion_sunat,
                email1:clie.email1,
                email2:clie.email2,
                email3:clie.email3,
                telefono:clie.telefono,
                clasificacion:clie.id_clasificacion,
                contacto:clie.contacto,
                vendedor:clie.vendedor_responsable
            }
        },
        setSelecteditemProductoIN(index){
            this.productos.isSelected=true;
           this.productos.productoSelected= this.productos.lista[index];
        },
        setSelecteditemProducto(index){
            this.productos.isSelected=true;
            var prod = this.productos.lista[index];
            this.productos.indexA=index;
            this.productos.dataRegistro.idCategoria=prod.cat_id;
            setTimeout(function () {
                //$('select').selectpicker('refresh');
                $("#select_modal_pro_categoria").trigger("change");
            },100);
            MODALES._data.productos.dataRegistro.idMarca=prod.mar_id;
            setTimeout(function () {
                this.getModelosProductos(idMarca)
                //$('#select_modal_prod_marca').selectpicker('refresh');
                $("#select_modal_prod_marca").trigger("change");
            },200);
            this.productos.dataRegistro.idModelo=prod.mod_id;
            this.productos.dataRegistro.idCodSunat=prod.sunat_cod_id;
            this.productos.dataRegistro.sku=prod.produ_sku;
            this.productos.dataRegistro.idnomenglatura=prod.nom_id;



            this.productos.dataRegistro.CodSunat=prod.sunat_cod_codigo;

            this.productos.dataRegistro.idUndMedida=prod.unidad_id;
            this.productos.dataRegistro.descripcion=prod.produ_desc;
            this.productos.dataRegistro.pn=prod.produ_pn;
            this.productos.dataRegistro.idTipo=prod.produ_neu_tp_const;

            this.productos.dataRegistro.isPais=prod.pais_id;
                this.productos.dataRegistro.ancho=prod.produ_neu_ancho_interno;
                this.productos.dataRegistro.serie=prod.produ_neu_serie;
                this.productos.dataRegistro.aro=prod.produ_neu_aro;
                this.productos.dataRegistro.pliegles=prod.produ_neu_pliegue;
                this.productos.dataRegistro.Uso2=prod.produ_neu_uso_comercial.trim();
                this.productos.dataRegistro.partiAran=prod.produ_partida;

                /* camra*/
                this.productos.dataRegistro.medidaCamara=prod.produ_cam_ancho;
                this.productos.dataRegistro.camAro=prod.produ_cam_aro;
                this.productos.dataRegistro.valvula=prod.produ_cam_valvula;

                /*aros*/
                this.productos.dataRegistro.medidaAro=prod.produ_aro_medida;
                this.productos.dataRegistro.modeloAro=prod.produ_aro_modelo;
                this.productos.dataRegistro.huecos=prod.produ_aro_huecos;
                this.productos.dataRegistro.espesorhueco=prod.produ_aro_espe_hueco;
                this.productos.dataRegistro.CBD=prod.produ_aro_cbd;
                this.productos.dataRegistro.PCD=prod.produ_aro_pcd;
                this.productos.dataRegistro.offSet=prod.produ_aro_offset;

        },
        getModelosProductos(idMarca){
            $.ajax({
                type: "POST",
                url: "../ajax/Modelo/getModeloForMarca.php",
                data: {
                    id:idMarca
                },
                success: function (data) {
                    console.log(data);
                    MODALES._data.modeloProducto.lista = (data);
                    setTimeout(function () {
                        $(".selectpicker").selectpicker('refresh');
                    },100)
                }
            });
        },
        getMarcasProductos(idCategoria){
            $.ajax({
                type: "POST",
                url: "../ajax/Marca/getMarcaForCategoria.php",
                data: {
                    id:idCategoria
                },
                success: function (data) {
                    console.log(data);
                    MODALES._data.marcasProducto.lista = (data);
                    setTimeout(function () {
                        $(".selectpicker").selectpicker('refresh');
                    },200)
                }
            });
        },
        relevadorOpcionesRegitroProducto(categoria){
            console.log(categoria +"*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-")
            this.productos.moduladores.global=true;
            this.productos.moduladores.global2=true;
            this.productos.moduladores.marcaSS=true;
            if (categoria == 'NEUMATICOS') {

                this.productos.moduladores.isNeumatico=true;
                this.productos.moduladores.isAccesorios=false;
                this.productos.moduladores.isAros=false;
                this.productos.moduladores.isCamara=false;
                this.productos.moduladores.isProtectores=false;
                /* $('.NEUMATICO').show();
                 $('.CAMARA').hide();
                 $('.ARO').hide();
                 $('#box-nomenclatura').show();
                 $('.box-siempre-show').show();
                 $('.bo-modelo-pro').hide();*/
            } else if (categoria == 'CAMARAS') {
                this.productos.moduladores.isNeumatico=false;
                this.productos.moduladores.isAccesorios=false;
                this.productos.moduladores.isAros=false;
                this.productos.moduladores.isCamara=true;
                this.productos.moduladores.isProtectores=false;
                /* $('.NEUMATICO').hide();
                 $('.CAMARA').show();
                 $('.ARO').hide();
                 $('#box-nomenclatura').hide();
                 $('.bo-modelo-pro').hide();*/
            } else if (categoria == 'AROS') {
                this.productos.moduladores.isNeumatico=false;
                this.productos.moduladores.isAccesorios=false;
                this.productos.moduladores.isAros=true;
                this.productos.moduladores.isCamara=false;
                this.productos.moduladores.isProtectores=false;

                /* $('.NEUMATICO').hide();
                 $('.CAMARA').hide();
                 $('.ARO').show();
                 $('#box-nomenclatura').hide();
                 $('.bo-modelo-pro').hide();
                 $('.box-siempre-show').show();*/
            }  else if (categoria == 'CAMARA') {
                this.productos.moduladores.isNeumatico=false;
                this.productos.moduladores.isAccesorios=false;
                this.productos.moduladores.isAros=false;
                this.productos.moduladores.isCamara=true;
                this.productos.moduladores.isProtectores=false;

                /* $('.NEUMATICO').hide();
                 $('.CAMARA').hide();
                 $('.ARO').hide();
                 $('#box-nomenclatura').hide();
                 $('.box-siempre-show').show();
                 $('.bo-modelo-pro').hide();*/

            }else if (categoria == 'PROTECTORES') {
                this.productos.moduladores.isNeumatico=false;
                this.productos.moduladores.isAccesorios=false;
                this.productos.moduladores.isAros=false;
                this.productos.moduladores.isCamara=false;
                this.productos.moduladores.isProtectores=true;

                /* $('.NEUMATICO').hide();
                 $('.CAMARA').hide();
                 $('.ARO').hide();
                 $('#box-nomenclatura').hide();
                 $('.box-siempre-show').show();
                 $('.bo-modelo-pro').hide();*/

            }else if (categoria == 'ACCESORIOS') {
                this.productos.moduladores.isNeumatico=false;
                this.productos.moduladores.isAccesorios=true;
                this.productos.moduladores.isAros=false;
                this.productos.moduladores.isCamara=false;
                this.productos.moduladores.isProtectores=false;

                /* $('.NEUMATICO').hide();
                 $('.CAMARA').hide();
                 $('.ARO').hide();
                 $('#box-nomenclatura').hide();
                 $('.box-siempre-show').show();
                 $('.bo-modelo-pro').hide();*/

            }else if (categoria == catTe) {
                this.productos.moduladores.isNeumatico=false;
                this.productos.moduladores.isAccesorios=false;
                this.productos.moduladores.isAros=false;
                this.productos.moduladores.isCamara=false;
                this.productos.moduladores.isProtectores=false;
                this.productos.moduladores.global=false;
                this.productos.moduladores.marcaSS=false;
                this.productos.moduladores.global2=true;
            }
            setTimeout(function () {
                $( "#modal-producto-input-codigo-sunat" ).focus(function() {
                    console.log("ejjjjjjj")
                    $('#modal_buscar_codigo_sunat').modal('show');
                });
            },200)
            console.log(this.productos.moduladores)
        },
        reLoadSkau(){
            setTimeout(function () {
                var categoria =  $("#select_modal_pro_categoria").find('option:selected').text();
                MODALES._data.productos.sku.cat=categoria;
                MODALES._data.productos.sku.tipo =  $("#select_opc").find('option:selected').text();
                MODALES._data.productos.sku.pais =  $("#select_prod_pais").find('option:selected').text();
                var marca =  $("#select_modal_prod_marca").find('option:selected').text().trim();
                MODALES._data.productos.sku.mar = marca;
            },100)

/*
            var sku="";


            var cat ='';
            var mar ='';
            var uso ='';
            var aro ='';
            var ancho ='';
            var tipo ='';
            var medida ='';
            var pais ='';
            var desc ='';
            var pn ='';
            var  categoria =this.productos.sku.cat;
            if(categoria ==='NEUMATICOS'){
                var serie = this.productos.dataRegistro.serie+this.productos.dataRegistro.pliegles

                //genNombreProducto()
                const tempUsoTex = (this.productos.dataRegistro.Uso && this.productos.dataRegistro.Uso === undefined)?"":getLetra(this.productos.dataRegistro.Uso, 3);
                const tempanchoNeTex = (this.productos.dataRegistro.ancho && this.productos.dataRegistro.ancho === undefined)?"":getFull(this.productos.dataRegistro.ancho,(this.productos.dataRegistro.ancho+"").length);

                sku =  'NE-'+
                    getLetra(this.productos.sku.mar,4)+'-'+
                    tempUsoTex+'-'+
                    this.productos.dataRegistro.aro+'-'+
                    tempanchoNeTex;
            }
            if(categoria ==='CAMARAS'){
                sku = ( +'CA-'+
                    getLetra(this.productos.sku.mar,3)+'-'+
                    getLetra( this.productos.sku.tipo, 3)+'-'+
                    this.productos.dataRegistro.medidaCamara);


            }
            if(categoria ==='AROS'){
                sku =  ( 'AR-'+
                    getLetra(this.productos.sku.mar,3)+'-'+
                    getLetra( this.productos.sku.tipo, 2)+'-'+
                    getFull(this.productos.dataRegistro.medidaAro,this.productos.dataRegistro.medidaAro.length));
            }
            if(categoria ==='PROTECTORES'){

                var pri = getLetra(this.productos.sku.pais,1);
                var sec = this.productos.sku.pais.charAt(2);

                sku = ( 'PR-'+
                    getLetra(this.productos.sku.mar,4)+'-'+
                    pri+sec+'-'+
                    getLetra(this.productos.dataRegistro.descripcion, 4));
            }
            if(categoria ==='ACCESORIOS'){
                var pri = getLetra(this.productos.sku.pais,1);
                var sec = this.productos.sku.pais.charAt(3);

                sku = ( 'AC-'+
                    getLetra(this.productos.sku.mar,3)+'-'+
                    pri+sec+'-'+
                    getLetra(this.productos.dataRegistro.descripcion, 4)+'-'+
                    getLetra(this.productos.dataRegistro.pn,4));
            }
            return sku;*/

        },
        onChangeCategoriProduc(event){

            var categoria =  $("#select_modal_pro_categoria").find('option:selected').text();
            this.productos.sku.cat=categoria.trim();
            console.log(categoria+"------------");
            this.resetDataRegisterProducto(true);
            setTimeout(function () {
                genNombreProducto();
            },300)

            this.relevadorOpcionesRegitroProducto(categoria);
            this. getMarcasProductos(event.target.value)

        },
        onChangeUsoProduc(event){
            $(event.target).find('option:selected').text();
            genNombreProducto()
        },
        onChangeTipoProduc(event){
            this.productos.sku.tipo =  $(event.target).find('option:selected').text().trim();
            genNombreProducto()
        },
        onChangePaisProduc(event){
            genNombreProducto()
            this.productos.sku.pais =  $(event.target).find('option:selected').text().trim();
        },
        onChangeNomengProduc(event){
            genNombreProducto()
            //this.productos.sku.pais =  $(event.target).find('option:selected').text();
        },
        onChangeModeloProduc(event){
            genNombreProducto()
            //this.productos.sku.pais =  $(event.target).find('option:selected').text();
        },
        onChangeMarcaiProduc(event){
            genNombreProducto()
            var marca =  $("#select_modal_prod_marca").find('option:selected').text();
            this.productos.sku.mar = marca.trim();
            //console.log(categoria+"------------");


           // this.relevadorOpcionesRegitroProducto(categoria);
            this. getModelosProductos(event.target.value)

        },
        getDataDBProducto(idprod,est){
            idproducto= idprod;
            $.ajax({
                type: "POST",
                url: "../ajax/Producto/get_unic_data2.php",
                data: {
                    idprod
                },
                success: function (data) {
                    console.log(JSON.parse(data))
                    MODALES.setDataProductoUNIQ(JSON.parse(data),est);
                }
            });
        },
        setDataProductoUNIQ(prodc,std){
            var prod = prodc;
            idproductoDeta=prod.produ_deta_id
            this.productos.dataRegistro.idCategoria=prod.cat_id;
            this.productos.view_std=std;
           this. getMarcasProductos(prod.cat_id);

            setTimeout(function () {
                MODALES.getModelosProductos(prod.cat_id);
                MODALES.relevadorOpcionesRegitroProducto( $("#select_modal_pro_categoria").find('option:selected').text());
            },100)

            MODALES._data.productos.dataRegistro.idMarca=prod.mar_id;
            MODALES._data.productos.dataRegistro.peso=prod.peso;
            this.productos.dataRegistro.idModelo=prod.mod_id;
            this.productos.dataRegistro.idCodSunat=prod.sunat_cod_id;
            this.productos.dataRegistro.sku=prod.produ_sku;
            this.productos.dataRegistro.idnomenglatura=prod.nom_id;


            this.productos.dataRegistro.CodSunat=prod.sunat_cod_codigo;

            this.productos.dataRegistro.idUndMedida=prod.unidad_id;
            this.productos.dataRegistro.descripcion=prod.produ_desc;
            this.productos.dataRegistro.nombre=prod.produ_nombre;
            this.productos.dataRegistro.pn=prod.produ_pn;
            this.productos.dataRegistro.idTipo=prod.tipro_id;

            this.productos.dataRegistro.isPais=prod.pais_id;
                this.productos.dataRegistro.ancho=prod.produ_neu_ancho_interno;
                this.productos.dataRegistro.serie=prod.produ_neu_serie;
                this.productos.dataRegistro.aro=prod.produ_neu_aro;
                this.productos.dataRegistro.pliegles=prod.produ_neu_pliegue;
                this.productos.dataRegistro.Uso=prod.produ_neu_uso_comercial;
                this.productos.dataRegistro.partiAran=prod.produ_partida;

                /* camra*/
                this.productos.dataRegistro.medidaCamara=prod.produ_cam_ancho;
                this.productos.dataRegistro.camAro=prod.produ_cam_aro;
                this.productos.dataRegistro.valvula=prod.produ_cam_valvula;

                /*aros*/
                this.productos.dataRegistro.medidaAro=prod.produ_aro_medida;
                this.productos.dataRegistro.modeloAro=prod.produ_aro_modelo;
                this.productos.dataRegistro.huecos=prod.produ_aro_huecos;
                this.productos.dataRegistro.espesorhueco=prod.produ_aro_espe_hueco;
                this.productos.dataRegistro.espesormm=prod.produ_aro_espesor;
                this.productos.dataRegistro.CBD=prod.produ_aro_cbd;
                this.productos.dataRegistro.PCD=prod.produ_aro_pcd;
                this.productos.dataRegistro.offSet=prod.produ_aro_offset;

            this.productos.dataRegistro.material=prod.produ_neu_mate;
            this.productos.dataRegistro.ancho_aduana=prod.produ_neu_ancho_adua;
            this.productos.dataRegistro.serie_aduana=prod.produ_neu_serie_adua;
            this.productos.dataRegistro.tipo_construccion=prod.produ_neu_tp_const;
            this.productos.dataRegistro.indice_carga=prod.produ_neu_indice;
            this.productos.dataRegistro.velocidad=prod.produ_neu_vel;
            this.productos.dataRegistro.constancia=prod.produ_neu_consta;
            this.productos.dataRegistro.item_const=prod.produ_neu_item;
            this.productos.dataRegistro.vigencia=prod.produ_neu_vigencia;
            this.productos.dataRegistro.conformidad=prod.produ_neu_confor;
            this.productos.dataRegistro.suce=prod.produ_neu_suce;

        },
        cargarDataEdit(index){
          this.setSelecteditemProducto(index);

        },
        cargarDataProductos(){
            $.ajax({
                type: "GET",
                url: MODALES._data.productos.url_data,
                success: function (resp) {
                    if (isJson(resp)){
                        // console.log(resp);
                        MODALES._data.productos.lista = JSON.parse(resp);
                    }else{
                        console.log(resp);
                    }
                }
            });
        },
        setSelecteditemAgencia(index){
            this.agenciaTansporte.isSelected=true;
            this.agenciaTansporte.agenciaSelected = this.agenciaTansporte.lista[index];
        },
        actualizarAgensiaTransporte(){
            var data = this.agenciaTansporte.dataRegistro;
            $.ajax({
                type: "POST",
                url: MODALES._data.agenciaTansporte.url_edit,
                data: data,
                success: function (resp) {
                    if (isJson(resp)){
                        var json = JSON.parse(resp);
                        if (json.res){
                            //swal("Regi", "You clicked the button!", "success")
                            MODALES._data.agenciaTansporte.lista[MODALES._data.agenciaTansporte.indexSeled]=(json.data);
                            MODALES.limpiarCamposRegistroCliente();
                            $("#modal_edit_agencia_transporte").modal('toggle');
                            location.reload();
                            /* setTimeout(function () {
                                 restarDataTable(MODALES._data.clientes.id_dataTable);
                             },300);*/
                        }else{
                            swall('No de Pudo Registrar Problemas en el servidor');
                        }
                    }else{
                        console.log(resp);
                    }
                }
            });
        },
        registrarAgensiaTransporte(){
            var data = this.agenciaTansporte.dataRegistro;
            $.ajax({
                type: "POST",
                url: MODALES._data.agenciaTansporte.url_registro,
                data: data,
                success: function (resp) {
                    if (isJson(resp)){
                        var json = JSON.parse(resp);
                        if (json.res){
                            //swal("Regi", "You clicked the button!", "success")
                            MODALES._data.agenciaTansporte.lista.push(json.data);
                            MODALES.limpiarCamposRegistroCliente();
                            $("#modal_agregar_agencia_transporte").modal('toggle');
                            /* setTimeout(function () {
                                 restarDataTable(MODALES._data.clientes.id_dataTable);
                             },300);*/
                        }else{
                            swall('No de Pudo Registrar Problemas en el servidor');
                        }
                    }else{
                        console.log(resp);
                    }
                }
            });
        },
        limpiarCamposRegistroCliente(){
            this.clientes.dataRegistro.ruc='';
            this.clientes.dataRegistro.direciones=[];
            this.clientes.dataRegistro.nombresocial='';
            this.clientes.dataRegistro.direccion='';
            this.clientes.dataRegistro.departamento='';
            this.clientes.dataRegistro.provincia='';
            this.clientes.dataRegistro.distrito='';
            this.clientes.dataRegistro.ubigeo='';
            this.clientes.dataRegistro.estado='';
            this.clientes.dataRegistro.condicion='';
            this.clientes.dataRegistro.email1='';
            this.clientes.dataRegistro.email2='';
            this.clientes.dataRegistro.email3='';
            this.clientes.dataRegistro.telefono='';
            this.clientes.dataRegistro.clasificacion='';
            this.clientes.dataRegistro.contacto='';
            this.clientes.dataRegistro.vendedor='';
        },
        cargarDataTransporte(){
            $.ajax({
                type: "GET",
                url: MODALES._data.agenciaTansporte.url_data,
                success: function (resp) {
                    if (isJson(resp)){
                        console.log(resp);
                        MODALES._data.agenciaTansporte.lista = JSON.parse(resp);
                    }else{
                        console.log(resp);
                    }
                }
            });
        },
        cargarDataClientes(){
            $.ajax({
                type: "GET",
                url: MODALES._data.clientes.url_data,
                success: function (resp) {
                    if (isJson(resp)){
                        // console.log(resp);
                        MODALES._data.clientes.lista = JSON.parse(resp);
                    }else{
                        console.log(resp);
                    }
                }
            });
        },
        consultaCliente(){
            alert("+++++")
            return false;
        },
        consultaDocumentoNum(){

            if (MODALES._data.clientes.dataRegistro.ruc.length>8){
                $.ajax({
                    data: {ruc:MODALES._data.clientes.dataRegistro.ruc},
                    url: MODALES._data.url_ruc,
                    type: 'POST',
                    async: true,
                    beforeSend: function() {
                        alerInfo('Buscando RUC en Sunat, Porfavor espere ..');

                    },
                    success: function (response) {
                        console.log(response);
                        MODALES.limpiarCamposRegistroCliente();
                        if (response.success) {

                            var result = response.result;
                            MODALES._data.clientes.dataRegistro.representantes=result.representantes_legales;

                            MODALES._data.clientes.dataRegistro.ruc=result.ruc;
                            MODALES._data.clientes.dataRegistro.nombresocial=result.razon_social;
                            MODALES._data.clientes.dataRegistro.direccion=result.direccion;
                            MODALES._data.clientes.dataRegistro.estado=result.estado;
                            MODALES._data.clientes.dataRegistro.condicion=result.condicion;
                            var distr="",prov="",depart="";
                            try {


                            var extraData = result.direccion.split('-');
                                prov=extraData[extraData.length-2].trim();

                                distr=extraData[extraData.length-1].trim();
                           // MODALES._data.clientes.dataRegistro.provincia=extraData[extraData.length-2];
                            //MODALES._data.clientes.dataRegistro.distrito=extraData[extraData.length-1];
                            console.log(extraData)
                            var extraData2 = extraData[extraData.length-3].trim().split(' ');


                            //MODALES._data.clientes.dataRegistro.departamento=extraData2[extraData2.length-1];
                            //alerSuccess('Cargando Datos de empresa');
                                depart=extraData2[extraData2.length-1];

                            } catch (error) {
                                console.log(error);
                                // expected output: ReferenceError: nonExistentFunction is not defined
                                // Note - error messages will vary depending on browser
                            }
                            $.ajax({
                                type: "GET",
                                url: "../ajax/localidad/get_ubigeo.php?pv="+prov.trim()+"&dp="+depart.trim()+"&di="+distr.trim(),
                                success: function (data) {
                                    if (isJson(data)){
                                        var jss= JSON.parse(data);

                                        MODALES._data.clientes.dataRegistro.departamento=depart;
                                        MODALES._data.clientes.dataRegistro.provincia=prov;
                                        MODALES._data.clientes.dataRegistro.distrito=distr;
                                        MODALES._data.clientes.dataRegistro.ubigeo=jss.codigo;

                                    }else{
                                        console.log(data)
                                    }

                                }
                            });


                            console.log("---------------------")
                            if (result.establecimientos.length>0){
                                MODALES._data.clientes.visualDireciones=true;
                            }else{
                                MODALES._data.clientes.visualDireciones=false;
                            }
                            result.establecimientos.forEach(function (el) {
                                var distr2="",prov2="",depart2="", tipo="";
                                try {


                                    var extraData2 = el.Direccion.split('-');
                                    prov2=extraData2[extraData2.length-2].trim();

                                    distr2=extraData2[extraData2.length-1].trim();
                                    console.log(extraData)
                                    var extraData2 = extraData2[extraData2.length-3].trim().split(' ');

                                    depart2=extraData2[extraData2.length-1].trim();

                                } catch (error) {
                                    console.log(error);
                                    // expected output: ReferenceError: nonExistentFunction is not defined
                                    // Note - error messages will vary depending on browser
                                }
                                if (el.tipo.includes("SUCURSAL")){
                                    tipo="SUCURSAL";
                                }else if (el.tipo.includes("DEPOSITO")){
                                    tipo="DEPOSITO";
                                }else if (el.tipo.includes("ALMACEN")){
                                    tipo="ALMACEN";
                                }
                                $.ajax({
                                    type: "GET",
                                    url: "../ajax/localidad/get_ubigeo.php?pv="+prov2.trim()+"&dp="+depart2.trim()+"&di="+distr2.trim(),
                                    success: function (data) {
                                        if (isJson(data)){
                                            var jss= JSON.parse(data);
                                            MODALES._data.clientes.dataRegistro.direciones.push({
                                                id:-1,
                                                idagencia:-1,
                                                idcliente:-1,
                                                nomagencia:"",
                                                direccion:el.Direccion,
                                                departamento:depart2,
                                                distrito:distr2,
                                                provincia:prov2,
                                                tipo:el.tipo,
                                                ubigeo:jss.codigo,
                                                st:false
                                            });
                                        }else{
                                            console.log(data)
                                        }

                                    }
                                });

                            });

                        } else {
                            swall(response.message);
                        }
                    },
                    error: function (err) {
                        console.log(err);
                        swall('Error al en el servidor de SUNAT Reporte este error!!');
                    },
                });
            }
            else  if (MODALES._data.clientes.dataRegistro.ruc.length==8){
                MODALES._data.clientes.visualDireciones=false;
                $.ajax({
                    data: {dni:MODALES._data.clientes.dataRegistro.ruc},
                    url: MODALES._data.url_dni,
                    type: 'POST',
                    async: true,
                    beforeSend: function() {
                        alerInfo('Buscando RUC en Sunat, Porfavor espere ..');

                    },
                    success: function (response) {
                        console.log(response);
                        MODALES.limpiarCamposRegistroCliente();
                        response = JSON.parse(response);
                        if (response.success) {

                            var result = response.result;
                            MODALES._data.clientes.dataRegistro.representantes=[];
                            MODALES._data.clientes.dataRegistro.ruc=result.DNI;
                            MODALES._data.clientes.dataRegistro.nombresocial=result.Nombres + " " + result.ApellidoPaterno +" "+result.ApellidoMaterno ;

                            MODALES._data.clientes.dataRegistro.direciones.push({
                                id:-1,
                                idagencia:-1,
                                nomagencia:"",
                                idcliente:-1,
                                direccion:'-',
                                departamento:'',
                                distrito:'',
                                provincia:'',
                                tipo:"DIRECCION",
                                ubigeo:"",
                                st:true
                            });

                           // MODALES._data.clientes.dataRegistro.direccion=result.direccion;
                           /* MODALES._data.clientes.dataRegistro.estado=result.estado;
                            MODALES._data.clientes.dataRegistro.condicion=result.condicion;*/
                         /*   var distr="",prov="",depart="";
                            try {


                                var extraData = result.direccion.split('-');
                                prov=extraData[extraData.length-2].trim();

                                distr=extraData[extraData.length-1].trim();
                                // MODALES._data.clientes.dataRegistro.provincia=extraData[extraData.length-2];
                                //MODALES._data.clientes.dataRegistro.distrito=extraData[extraData.length-1];
                                console.log(extraData)
                                var extraData2 = extraData[extraData.length-3].trim().split(' ');


                                //MODALES._data.clientes.dataRegistro.departamento=extraData2[extraData2.length-1];
                                //alerSuccess('Cargando Datos de empresa');
                                depart=extraData2[extraData2.length-1];

                            } catch (error) {
                                console.log(error);
                                // expected output: ReferenceError: nonExistentFunction is not defined
                                // Note - error messages will vary depending on browser
                            }
                            MODALES._data.clientes.dataRegistro.direciones.push({
                                id:-1,
                                idagencia:-1,
                                nomagencia:"",
                                idcliente:-1,
                                direccion:result.direccion,
                                departamento:depart,
                                distrito:distr,
                                provincia:prov,
                                tipo:"DIRECCION FISCAL",
                                ubigeo:"",
                                st:false
                            });
                            console.log("---------------------")
                            result.establecimientos.forEach(function (el) {
                                var distr2="",prov2="",depart2="", tipo="";
                                try {


                                    var extraData2 = el.Direccion.split('-');
                                    prov2=extraData2[extraData2.length-2].trim();

                                    distr2=extraData2[extraData2.length-1].trim();
                                    console.log(extraData)
                                    var extraData2 = extraData2[extraData2.length-3].trim().split(' ');

                                    depart2=extraData2[extraData2.length-1];

                                } catch (error) {
                                    console.log(error);
                                    // expected output: ReferenceError: nonExistentFunction is not defined
                                    // Note - error messages will vary depending on browser
                                }
                                if (el.tipo.includes("SUCURSAL")){
                                    tipo="SUCURSAL";
                                }else if (el.tipo.includes("DEPOSITO")){
                                    tipo="DEPOSITO";
                                }else if (el.tipo.includes("ALMACEN")){
                                    tipo="ALMACEN";
                                }

                                MODALES._data.clientes.dataRegistro.direciones.push({
                                    id:-1,
                                    idagencia:-1,
                                    idcliente:-1,
                                    nomagencia:"",
                                    direccion:el.Direccion,
                                    departamento:depart2,
                                    distrito:distr2,
                                    provincia:prov2,
                                    tipo:tipo,
                                    ubigeo:"",
                                    st:false
                                });
                            });*/
                        } else {
                            swall(response.message);
                        }
                    },
                    error: function (err) {
                        console.log(err);
                        swall('Error al en el servidor de SUNAT Reporte este error!!');
                    },
                });
            }

        },
        consultaRucAgensia(){

            if (true){
                $.ajax({
                    data: {ruc:MODALES._data.agenciaTansporte.dataRegistro.ruc},
                    url: MODALES._data.url_ruc,
                    type: 'POST',
                    async: true,
                    beforeSend: function() {
                        $.toast({
                            heading: 'INFORMACION',
                            text: "Buscando",
                            icon: 'info',
                            position: 'top-right',
                            hideAfter: '2500',
                        });

                    },
                    success: function (response) {
                        console.log(response);
                        if (response.success) {
                            var result = response.result;

                            MODALES._data.agenciaTansporte.dataRegistro.nombresocial=result.razon_social;
                            MODALES._data.agenciaTansporte.dataRegistro.direccion=result.direccion;
                            MODALES._data.agenciaTansporte.dataRegistro.estado=result.estado;
                            MODALES._data.agenciaTansporte.dataRegistro.condicion=result.condicion;


                            //alerSuccess('Cargando Datos de empresa');


                        } else {
                            swall(response.message);
                        }
                    },
                    error: function (err) {
                        console.log(err);
                        swall('Error al en el servidor de SUNAT Reporte este error!!');
                    },
                });
            }

        },
        registrarCliente(){
            var data = {...this.clientes.dataRegistro};
            data.direciones = JSON.stringify(data.direciones);
            data.representantes = JSON.stringify(data.representantes);

            $.ajax({
                type: "POST",
                url: MODALES._data.clientes.url_registro,
                data: data,
                success: function (resp) {
                    if (isJson(resp)){
                        var json = JSON.parse(resp);
                        if (json.res){
                            //swal("Regi", "You clicked the button!", "success")
                            MODALES._data.clientes.lista.push(json.data);
                            MODALES.limpiarCamposRegistroCliente();
                            $("#modal_agregar_cliente").modal('toggle');
                            location.reload();
                            datatableclientes.ajax.reload();
                            /* setTimeout(function () {
                                 restarDataTable(MODALES._data.clientes.id_dataTable);
                             },300);*/
                        }else{
                            swall('No de Pudo Registrar Problemas en el servidor');
                        }
                    }else{
                        console.log(resp);
                    }
                }
            });
        },
        editarCliente(){
            var data ={ ... this.clientes.dataRegistro};
            data.direciones = JSON.stringify(data.direciones);
            console.log(data)
            $.ajax({
                type: "POST",
                url: MODALES._data.clientes.url_edit,
                data: data,
                success: function (resp) {
                    console.log('/////////////////////////');
                    console.log(resp);
                    if (isJson(resp)){
                        var json = JSON.parse(resp);
                        if (json.res){
                            //swal("Regi", "You clicked the button!", "success")
                            MODALES._data.clientes.lista[MODALES._data.clientes.indexSeled] = json.data;
                            MODALES.limpiarCamposRegistroCliente();
                              $("#modal_editar_cliente").modal('toggle');
                            datatableclientes.ajax.reload();
                             /*setTimeout(function () {
                                 restarDataTable(MODALES._data.clientes.id_dataTable);
                             },300);*/
                        }else{
                            swall('No de Pudo Registrar Problemas en el servidor');
                        }
                    }else{
                        console.log(resp);
                    }
                }
            });
        },
        setSelecteditem(index){
            this.clientes.isSelected=true;
            this.clientes.clienteSelected = this.clientes.lista[index];
        }
    },
    computed:{
        getSKU(){
            var sku="";


            var cat ='';
            var mar ='';
            var uso ='';
            var aro ='';
            var ancho ='';
            var tipo ='';
            var medida ='';
            var pais ='';
            var desc ='';
            var pn ='';
            var  categoria =this.productos.sku.cat;
            if(categoria ==='NEUMATICOS'){
                var serie = this.productos.dataRegistro.serie+this.productos.dataRegistro.pliegles

                genNombreProducto()
                const tempUsoTex = (this.productos.dataRegistro.Uso === undefined)?"":getLetra(this.productos.dataRegistro.Uso, 3);
                const tempanchoNeTex = (this.productos.dataRegistro.ancho === undefined)?"":getFull(this.productos.dataRegistro.ancho,(this.productos.dataRegistro.ancho+"").length);

                sku =  'NE-'+
                    getLetra(this.productos.sku.mar,4)+'-'+
                    tempUsoTex+'-'+
                    this.productos.dataRegistro.aro+'-'+
                    tempanchoNeTex;
            }
            if(categoria ==='CAMARAS'){
                sku = ( 'CA-'+
                    getLetra(this.productos.sku.mar,3)+'-'+
                    getLetra( this.productos.sku.tipo, 3)+'-'+
                    this.productos.dataRegistro.medidaCamara);


            }
            if(categoria ==='AROS'){
                const mediaario = (this.productos.dataRegistro.medidaAro === undefined)?"":getFull(this.productos.dataRegistro.medidaAro,this.productos.dataRegistro.medidaAro.length);

                sku =  ( 'AR-'+
                    getLetra(this.productos.sku.mar,3)+'-'+
                    getLetra( this.productos.sku.tipo, 2)+'-'+
                    mediaario);
            }
            if(categoria ==='PROTECTORES'){

                var pri = getLetra(this.productos.sku.pais,1);
                var sec = this.productos.sku.pais.charAt(2);

                sku = ( 'PR-'+
                    getLetra(this.productos.sku.mar,4)+'-'+
                    pri+sec+'-'+
                    getLetra(this.productos.dataRegistro.descripcion, 4));
            }
            if(categoria ==='ACCESORIOS'){
                var pri = getLetra(this.productos.sku.pais,1);
                var sec = this.productos.sku.pais.charAt(3);

                sku = ( 'AC-'+
                    getLetra(this.productos.sku.mar,3)+'-'+
                    pri+sec+'-'+
                    getLetra(this.productos.dataRegistro.descripcion, 4)+'-'+
                    getLetra(this.productos.dataRegistro.pn,4));
            }
            sku = sku.replace(/\n|\r/g, "");

            $.ajax({
                type: "POST",
                url: "../ajax/Producto/product_sku_unic.php",
                data: {sku},
                success: function (rsp) {
                    console.log(rsp)
                    const jsonD = JSON.parse(rsp);
                    MODALES._data.productos.dataRegistro.sku = jsonD.res
                    //$('#modal-producto-input-sku').val(jsonD.res);
                }
            });

           // this.productos.dataRegistro.sku

            return sku;
        }
    }

});

function genNombreProducto() {
    const cate =$("#select_modal_pro_categoria option:selected").text();
    var nombrePr=cate;

    $("#modal-producto-input-nombre").prop( "disabled", true );
    if (cate=="NEUMATICOS"){

        const  nomen = $("#select_nomenglatura option:selected").text().trim();
        const  tipo = $("#select_opc option:selected").text().trim();
        const ancho =  $("#modal-producto-input-neu-ancho").val();
        nombrePr+=" "+ancho;
        if (nomen=="MILIMETRICA"){
            const serie= $("#modal-producto-input-neu-serie").val();
            nombrePr+="/"+serie;
        }
        if (tipo=="RADIAL"){
            nombrePr+="R";
        }
        const aroNeu=$("#modal-producto-input-neu-aro").val();
        const plieges = $("#modal-producto-input-neu-pliege").val();
        const marca =$("#select_modal_prod_marca option:selected").text().trim();
        const modelo =$("#select_modal_prod_modelo option:selected").text().trim();
        const pais =$("#select_prod_pais option:selected").text().trim();
        nombrePr+="-"+aroNeu+" "+plieges+"PR " +marca +" "+modelo+" "+pais;


    } else if(cate=="ACCESORIOS") {
        nombrePr += " "+$("#modal-producto-input-desc").val().trim();
        nombrePr += " "+$("#modal-producto-input-pn").val().trim();
        var marca =$("#select_modal_prod_marca option:selected").text().trim();
        var pais =$("#select_prod_pais option:selected").text().trim();
        marca=(marca=="-Seleccione-")?"":marca;
        pais=(pais=="-Seleccione-")?"":pais;

        nombrePr += " "+marca;
        nombrePr += " "+pais;
    }else if(cate=="AROS") {
        nombrePr += " "+$("#modal-producto-input-aro-medida").val().trim();
        nombrePr += " "+$("#modal-producto-input-aro-espesor").val().trim();
        nombrePr += " ("+$("#modal-producto-input-aro-num-huecos").val().trim();
        nombrePr += ","+$("#modal-producto-input-aro-espesor-hueco").val().trim();
        nombrePr += ","+$("#modal-producto-input-aro-cbd").val().trim();
        nombrePr += ","+$("#modal-producto-input-aro-pcd").val().trim();
        nombrePr += ","+$("#modal-producto-input-aro-offset").val().trim()+")";

        var marca =$("#select_modal_prod_marca option:selected").text().trim();
        var pais =$("#select_prod_pais option:selected").text().trim();
        marca=(marca=="-Seleccione-")?"":marca;
        pais=(pais=="-Seleccione-")?"":pais;

        nombrePr += " "+marca;
        nombrePr += " "+pais;
    }
    else if(cate=="CAMARAS") {
        const  nomen = $("#select_nomenglatura option:selected").text().trim();
        const  tipo = $("#select_opc option:selected").text().trim();
        nombrePr += " "+$("#modal-producto-input-cam-medida").val().trim();
        if (tipo=="RADIAL"){
            nombrePr+="R";
        }
        if (tipo=="CONVENCIONAL"){
            nombrePr+="-";
        }
        nombrePr += " "+$("#modal-producto-input-cam-aro").val().trim();
        nombrePr += " "+$("#modal-producto-input-cam-valvula").val().trim();
        var marca =$("#select_modal_prod_marca option:selected").text().trim();
        var pais =$("#select_prod_pais option:selected").text().trim();
        marca=(marca=="-Seleccione-")?"":marca;
        pais=(pais=="-Seleccione-")?"":pais;

        nombrePr += " "+marca;
        nombrePr += " "+pais;

    }else if(cate=="PROTECTORES") {
        nombrePr += " "+$("#modal-producto-input-desc").val().trim();
        var marca =$("#select_modal_prod_marca option:selected").text().trim();
        var pais =$("#select_prod_pais option:selected").text().trim();
        marca=(marca=="-Seleccione-")?"":marca;
        pais=(pais=="-Seleccione-")?"":pais;

        nombrePr += " "+marca;
        nombrePr += " "+pais;
    }else if(cate==catTe) {
        nombrePr="";
        $("#modal-producto-input-nombre").removeAttr('disabled');
    }


    MODALES._data.productos.dataRegistro.nombre=(nombrePr);
    console.log(nombrePr+"-*******************")

}

function restarDataTable(id){
    $(id).DataTable({
        "dom": 'Bfrtip',
        "buttons": [
            'csv', 'excel'
        ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        language: {
            url: '../assets/Spanish.json'
        }
    });
}

var datatableclientes ;

function obtenerDataClasificcion(){
   /* $.ajax({
        type: "GET",
        url: "../ajax/ClienteClasificacion/get_all_clasificacion.php",
        success: function (data) {
            console.log(data);
            MODALES._data.clientes.clasificaciones = JSON.parse(data);
            setTimeout(function () {
               // $('select').selectpicker('refresh');
            },150)
        }
    });*/

}

function getFull(text,num) {
    if (text){
        text=text.split('.').join('');
        text=text.split('-').join('');
        text= getLetra(text,num);
        return text;
    }else{
        return "";
    }

}

function getLetra(texto,num) {
    if (texto){
        texto= texto.replace(/ /g,'');
        //texto= texto.replace('.','');
        texto = texto.substr(0, num);
        //console.log("***<")
        return texto;
    }else{
        return "";
    }

}


$(document).ready(function () {


    obtenerDataClasificcion();
//MODALES.llenarDepartamentos();
    console.log('111');

   /* setTimeout(function () {
        console.log("$$$$$$$$$$$$$$$$$$$$$$")
        $( "#modal-producto-input-codigo-sunat" ).focus(function() {
            console.log("ejjjjjjj")
            $('#modal_buscar_codigo_sunat').modal('toggle');
        });
    },300);*/

    if (MODALES._data.clientes.iniciarDatos){
        datatableclientes = $(MODALES._data.clientes.id_dataTable).DataTable({
            "processing": true,
            "serverSide": true,
            filter: true,
            "sAjaxSource": MODALES._data.clientes.url_data,
            "dom": 'Bfrtip',
            "buttons": [
                'csv', 'excel'
            ],
            columnDefs:[

                {
                    "targets": 5,
                    "data": "id",
                    "render": function ( data, type, row, meta ) {
                        //console.log(meta)
                        return '<button  data-toggle="modal" data-target="#modal_editar_cliente" onclick="MODALES.selecdClienteEdit('+row[5]+')"  class="btn btn-sm btn-info fa fa-edit btn-selector-cliente"\n' +
                            '                                                        title="Anadir item" ></button>';
                    }
                }
            ]

        });

    }
    if (MODALES._data.agenciaTansporte.iniciarDatos){
        console.log('222');
        MODALES.cargarDataTransporte();
        setTimeout(function () {
            restarDataTable(MODALES._data.agenciaTansporte.id_dataTable);
        },300);
    }

    if (MODALES._data.productos.iniciarDatos){
        console.log('222');
        MODALES.cargarDataProductos();
        setTimeout(function () {
            restarDataTable(MODALES._data.productos.id_dataTable);
        },300);
    }
    $('#modal_buscar_cliente').on('shown.bs.modal', function (e) {
        MODALES._data.clientes.isSelected=false;
    });
    $('#modal_buscar_Agencia_transporte').on('shown.bs.modal', function (e) {
        MODALES._data.agenciaTansporte.isSelected=false;
    });
    $('#modal_buscar_productos').on('shown.bs.modal', function (e) {
        MODALES._data.productos.isSelected=false;
    });
    $('#inputConsultarRUc').keyup(function(e){
        if(e.keyCode == 13){
            //entered is clicked
            alert('Entered button clicked inside input');
        }
        return false;
    });




});

var nombre ='';

function guardarProductoModal() {

    $.ajax({
        type: "POST",
        url: "../ajax/Producto/get_existe_nombre.php",
        data: {nombre:$("#modal-producto-input-nombre").val()},
        success: function (res) {
            console.log(res)
            const jsm= JSON.parse(res);
            if (!jsm.res){
                var detalleData = [];
                detalleData.push(idproductoDeta);                                               /*0*/
                /*CAMARA*/
                detalleData.push($('#modal-producto-input-cam-medida').val());      /*1*/
                detalleData.push($('#modal-producto-input-cam-aro').val());         /*2*/
                detalleData.push($('#modal-producto-input-cam-valvula').val());     /*3*/
                /*CAMARA*/

                /*AROS*/
                detalleData.push($('#modal-producto-input-aro-modelo').val());      /*4*/
                detalleData.push($('#modal-producto-input-aro-medida').val());      /*5*/
                detalleData.push($('#modal-producto-input-aro-espesor').val());     /*6*/
                detalleData.push($('#modal-producto-input-aro-num-huecos').val());  /*7*/
                detalleData.push($('#modal-producto-input-aro-espesor-hueco').val());/*8*/
                detalleData.push($('#modal-producto-input-aro-cbd').val());         /*9*/
                detalleData.push($('#modal-producto-input-aro-pcd').val());         /*10*/
                detalleData.push($('#modal-producto-input-aro-offset').val());      /*11*/
                /*AROS*/

                /*NEUMATICOS*/
                detalleData.push($('#modal-producto-input-neu-ancho').val());       /*12*/
                detalleData.push($('#modal-producto-input-neu-serie').val());       /*13*/
                detalleData.push($('#modal-producto-input-neu-aro').val());         /*14*/
                detalleData.push($('#modal-producto-input-neu-pliege').val());      /*15*/
                detalleData.push($('#select_uso').find('option:selected').text().trim());  /*16*/


                detalleData.push(' ');/*17*/    /*detalleData.push($('#modal-producto-input-neu-set').val());*/
                detalleData.push($('#modal-producto-input-neu-mate').find('option:selected').text());
                detalleData.push($('#modal-producto-input-neu-ancho-adua').val());
                detalleData.push($('#modal-producto-input-neu-serie-adua').val());
                detalleData.push($('#modal-producto-input-neu-serie-tipo-cons').find('option:selected').text());
                detalleData.push(' ');/*22*/    /*detalleData.push($('#modal-producto-input-neu-carga').val());*/
                detalleData.push(' ');/*23*/    /*detalleData.push($('#modal-producto-input-neu-pisa').val());*/
                detalleData.push(' ');/*24*/    /*detalleData.push($('#modal-producto-input-neu-externo').val());*/
                detalleData.push($('#modal-producto-input-neu-veloci').find('option:selected').text()); /*25*/    /*detalleData.push($('#modal-producto-input-neu-veloci').val());*/
                detalleData.push($('#modal-producto-input-neu-consta').val());/*26*/    /**/
                detalleData.push($('#modal-producto-input-neu-item').val());/*27*/    /**/
                detalleData.push($('#modal-producto-input-neu-vigencia').val());/*28*/    /**/
                detalleData.push($('#modal-producto-input-neu-confor').val());/*29*/    /**/
                detalleData.push($('#modal-producto-input-neu-indice-carga').val());/*29*/    /**/
                detalleData.push($('#modal-producto-input-neu-suce').val());/*29*/    /**/


                detalleData.push($('#modal-producto-input-neu-parti').val());        /*30*/


                detalleData.push($('#modal-producto-input-produ-medida').val());/*31*/
                detalleData.push('b1');/*32*/

                console.log('detalleData');
                console.log(detalleData);
                /*NEUMATICOS*/
                /*    setDetalleProducto(detalleData);*/


                var c = $('#select_modal_pro_categoria').val();
                var m = $('#select_modal_prod_marca').val();
                var mar = $('#select_modal_prod_marca option:selected').text();
                var mo = $('#select_modal_prod_modelo').val();
                var mode = $('#select_modal_prod_modelo option:selected').text();

                var co = $('#modal-producto-input-codigo-sunat-id').val();
                var sk = $('#modal-producto-input-sku').val();
                var me = $('#select_modal_medida').val();
                var de = $('#modal-producto-input-desc').val();
                var pn = $('#modal-producto-input-pn').val();
                var pa = $('#select_prod_pais').val();
                var pai = $('#select_prod_pais option:selected').val();

                var cate = $("#select_modal_pro_categoria option:selected").text();

                var sTCAT = (c==7);

                if (c > 0 && m > 0 || sTCAT) {
                    console.log(co + ' - ' + sk + ' - ' + me + ' - ' + de + ' - ' + pn + ' - ' + pa);
                    if ( sk !== '' && me > 0  ) {
                        var ti = $('#select_opc').val();
                        var tip = $('#select_opc option:selected').val();

                        alerSuccess('guardando datos NEUMATICO');
                        setDetalleProducto(detalleData);


                    } else {
                        alerWar('ingrese los datos del nuevo producto');
                        console.log('-1');
                    }
                } else {
                    alerWar('Seleccione marca y modelo del producto');
                }

            }else{
                $.toast({
                    heading: 'ALERTA',
                    text: "Este producto ya existe",
                    icon: 'warning',
                    position: 'top-right',
                    hideAfter: '2500',
                });
            }

        }})



}

function guardarProductoModalSoloPeso() {
    const data={
        prod:idproducto,
        peso:$("#modal-producto-input-peso").val()
    }

    $.ajax({
        type: "POST",
        url: "../ajax/Producto/updt_prod_peso.php",
        data: data,
        success: function (resp) {
            console.log(resp);
            alerSuccess('Se ha guardado');
            $('#modal_editar_productos').modal('hide');
        }
    });

}

function setDetalleProducto(detalleData) {
    $.ajax({
        data: {'array': JSON.stringify(detalleData)},
        url: '../ajax/DetalleProducto/setDetalleAndGetRow.php',
        type: 'POST',
        success: function (response) {
            var json = JSON.parse(JSON.stringify(response));
            console.log('RESPUESTA DE DETALLE');
            console.log(response);
            if (true) {

                var productoData = []

                productoData.push(idproducto);/*0*/
                productoData.push('');/*0*/
                productoData.push(idproductoDeta==0?response:idproductoDeta);/*1*/
                productoData.push('');/*2*/
                productoData.push($('#select_modal_medida').val());/*3*/
                productoData.push($('#modal-producto-input-codigo-sunat-id').val());/*4*/
                productoData.push($('#modal-producto-input-desc').val());/*5*/
                productoData.push($('#modal-producto-input-sku').val());/*6*/
                productoData.push($('#modal-producto-input-pn').val());/*7*/
                var o = $('#select_opc').val();
                var n = $('#select_nomenglatura').val();
                if (o === '0') {
                    productoData.push('');
                } else {
                    productoData.push($('#select_opc').val());/*8*/
                }
                if (n === '0') {
                    productoData.push('');
                } else {
                    productoData.push($('#select_nomenglatura').val());/*9*/
                }
                var modeloid= 1;
                if ($('#select_modal_pro_categoria').val()==1){
                    modeloid= $('#select_modal_prod_modelo').val();
                }
                productoData.push($('#select_modal_pro_categoria').val());/*10*/
                productoData.push($('#select_modal_prod_marca').val()===null?'159':$('#select_modal_prod_marca').val());/*11*/
                //var modelo= $('#select_modal_prod_modelo').val();
                productoData.push(modeloid);/*12*/
                productoData.push($('#select_prod_pais').val()===null?'348':$('#select_prod_pais').val());/*13*/
                //productoData.push(nombre);/*14*/
                productoData.push($("#modal-producto-input-nombre").val());/*14*/

                productoData.push('n');/*15*/
                productoData.push($("#modal-producto-input-peso").val());/*14*/

                console.log('productoData');
                console.log(productoData);
                setProducto(productoData);

                alerSuccess('Se ha guardado el detalle');

            } else {
                alerInfo('No se pudo guardar el detalle, porfavor ingrese todos los campos necesarios');
            }
        },
        error: function (err) {
            console.log(err.responseText)
            alerError('Error al guardado el detalle<br>Reporte este error!!');
        }
    });
}


function setProducto(productoData) {
    $.ajax({
        data: {'array': JSON.stringify(productoData)},
        url: '../ajax/Producto/setProductoAndGetRow.php',
        type: 'POST',
        success: function (response) {
            console.log('RESPUESTA DEL PRODUCTO');
            console.log(response);
            var json = JSON.parse(JSON.stringify(response));




            if (json) {

                MODALES.resetDataRegisterProducto(false);
                //console.log(json)
                $('#modal_editar_productos').modal('toggle');
                $('#modal-producto-btn-cerrar').click();
                $('#modal-producto-btn-limpiar').click();
                alerSuccess('Se ha guardado el producto');
                tabla_producto_alma.ajax.reload()
               // location.reload();



            } else {
                alerInfo('No se pudo guardar el producto, porfavor ingrese todos los campos necesarios');
            }
        },
        error: function  (err) {
            console.log(err.responseText)
            alerError('Error al guardado el producto<br>Reporte este error!!');
        }
    });
}

function alerError(msg) {
    $.toast({
        heading: 'ERROR',
        text: msg,
        icon: 'error',
        position: 'top-right',
        hideAfter: '2500',
    });
}
function alerSuccess(msg) {
    $.toast({
        heading: 'EXITOSO',
        text: msg,
        icon: 'success',
        position: 'top-right',
        hideAfter: '2500',
    });
}
function alerInfo(msg) {
    $.toast({
        heading: 'INFORMACION',
        text: msg,
        icon: 'info',
        position: 'top-right',
        hideAfter: '2500',
    });
}
function alerWar(msg) {
    $.toast({
        heading: 'ALERTA',
        text: msg,
        icon: 'warning',
        position: 'top-right',
        hideAfter: '2500',
    });
}