$(document).ready(function () {
    $('#venta-input-buscar-producto').focus(function () {
        $('#modal_buscar_producto').modal('show');
    });

    $('#venta-input_cliente').focus(function () {
        $('#modal_buscar_empresa').modal('show');
    });

    $('#modal_buscar_empresa').on('click', 'tr td a', function (evt) {
        var id = $(this).parents('tr').eq(0).find('input').val();
        var name = $(this).parents('tr').eq(0).find('label').text();
        var ruc = $(this).parents('tr').eq(0).find('#emp_num_ruc').text();
        var dir = $(this).parents('tr').eq(0).find('#emp-dir').text();
        $('#venta-input_cliente').val(name);
        $('#ruc').val(ruc);
        $('#direccion').val(dir);

    });

    $('#tabla-detalle').on('click', 'tr td a', function (evt) {
        $(this).closest('tr').remove();
    });

    $('#modal_buscar_producto').on('click', 'tr td a', function (evt) {
        var id = $(this).parents('tr').eq(0).find('input').val();
        var name = $(this).parents('tr').eq(0).find('#btn-nombre').text();

        $('#venta-input-buscar-producto').val(name);
        $('#stock').val('123');
        $('#lote').val('LT0012');
        $('#precio').val('46.87');


    });

    $('#btn-agregar').click(function () {
        var cant =$('#cantidad').val();
        var pre =$('#precio').val();
        var total = parseFloat(pre)*cant;
        var row='<tr>' +
            '          <td class="text-center">Item</td>' +
            '          <td class="text-center">'+$('#venta-input-buscar-producto').val()+'</td>' +
            '          <td class="text-center">'+cant+'</td>' +
            '          <td class="text-center">'+pre+'</td>' +
            '          <td class="text-center">'+total.toFixed(2)+'</td>' +
            '          <td class="text-center">' +
            '               <a id="btn-a" class="btn btn-sm btn-danger fa fa-times btn-option"' +
            '                                               title="Eliminar item" data-dismiss="modal">' +
            '               </a>'+
            '      </tr>';

        $('#tabla-detalle tbody').append(row);
        var index=0;
        $('#tabla-detalle tbody tr').each(function () {
            $(this).find('td').eq(0).text(++index);
        });
    });
});