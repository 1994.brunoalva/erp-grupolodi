import alerta from "./alertToas.js";

$(document).ready(function () {
    $('#table-folder-import').on('click', 'tr td .btn-op2', function (evt) {
        var ser = $(this).parents('tr').eq(0).find('#ser-fol').val();
        var num = $(this).parents('tr').eq(0).find('#num-fol').text();
        var id_fol = $(this).parents('tr').eq(0).find('input').val();
        $('#fol_id').val(id_fol);
        $('#data-fol').text('ESTADO DE FOLDER '+ser+' - '+num);
        $('#modal_estado').modal('show');
        runAjaxId(id_fol,
            'DetalleEstado',
            'getForID',
            'detalle Estado',
            getDetalle);

    });
    $('#select_estado').change(function () {
        var id = $(this).val();
        var dataEstado=[];
        dataEstado.push(['de_id',$('#deta_id').val()]);
        dataEstado.push(['est_id',id]);
        dataEstado.push(['estado','b1']);
        console.log(dataEstado);
        runAjaxArray(dataEstado,
            'DetalleEstado',
            'updateEstado',
            'estado actualizado',
            updateDetalle);
    });

    $('#table-folder-import').on('click', 'tr td .btn-op3', function (evt) {



    });

    function runAjaxArray(array, file, method, title, func) {
        $.ajax({
            data: {'array': JSON.stringify(array)},
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json) {
                    func(json);
                } else {
                    /*clearSelect(puerto);*/
                }
            },
            error: function () {
                alerta.alerInfo('No se pudo cargar ' + title + '<br>Reporte este error!!');
            }
        });
    }
    function runAjaxId(id, file, method, title, func) {
        $.ajax({
            data: {id},
            url: '../ajax/' + file + '/' + method + '.php',
            type: 'POST',
            async: true,
            success: function (response) {
                var json = JSON.parse(JSON.stringify(response));
                console.log(json);
                if (json) {
                    func(json);
                } else {
                    /*clearSelect(puerto);*/
                }
            },
            error: function () {
                alerta.alerError('Error al cargar ' + title + '<br>Reporte este error!!');
            }
        });
    }

    function getDetalle(json) {
        console.log(json);
        $('#deta_id').val(json.de_id);
        if(json.est_id){
            $('#select_estado').val(json.est_id).change();
        }
        console.log('DETALLE ESTADO');
    }
    function updateDetalle(json) {
        console.log(json);
        console.log('DETALLE ESTADO');
    }
});