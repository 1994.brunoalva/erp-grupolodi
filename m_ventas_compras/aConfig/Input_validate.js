$(document).ready(function () {
    $(".input-number").keypress(function (e) {
        var regex = new RegExp("^[0-9]+$");
        var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (!regex.test(key)) {
            e.preventDefault();
            return false;
        }

    });
    $(".input-letter").keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z ]+$");
        var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (!regex.test(key)) {
            e.preventDefault();
            return false;
        }
    });
    $(".input-alfa-numeric").keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9 ]+$");
        var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (!regex.test(key)) {
            e.preventDefault();
            return false;
        }
    });


    var index = 0;
    $('.money').keypress(function (e) {
        var regex = new RegExp("^[0-9]+$");
        var letra = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        var moneyText = $(this).val();
        if ((moneyText.length) === 0) {
            if (letra === '.' ) {
                e.preventDefault();
                return false;
            }
        } else {
            var tienePunto = moneyText.toString().includes('.');
            if (tienePunto === false && letra === '.') {
                $(this).val(moneyText + letra);
            }
        }
        if (conuntChart>=2) {
            if(conuntChart===3){
                $(this).val(moneyText.substring(0,moneyText.length-1));
            }
            e.preventDefault();
            return false;
        }
        if (!regex.test(letra)) {
            e.preventDefault();
            return false;
        }
    });


    var numero ='';
    var conuntChart=0;


    $('.money').keyup(function () {
        var moneyText = $(this).val();
        conuntChart=0;
        var tienePunto=true;
        if ((moneyText.length) > 0) {
            index=0;
            index = moneyText.indexOf('.');
            tienePunto = moneyText.toString().includes('.');
            if (tienePunto) {
                for (let i = (index+1); i < moneyText.length; i++) {
                    ++conuntChart;
                }
            }
        }
        moneyText =  $(this).val();
        tienePunto = moneyText.toString().includes('.');
        var decimal='';
        if(tienePunto){
            decimal='';
            index = moneyText.indexOf('.');
            decimal='.'+moneyText.substring(index+1);
        }else{
            moneyText=moneyText.replace(/,/g, '');
            moneyText.substring(0, index);
            numero='';
            moneyText = moneyText.split('').reverse().join('');
            for (let i = moneyText.length-1; i >= 0; --i) {
                numero += moneyText.charAt(i);
                if(i%3===0&&i>0){
                    numero += ',';
                }
            }
        }
        $(this).val(numero+decimal);
    });

    $('.money').focusout(function () {
        var moneyText=$(this).val();
        var xy =moneyText.indexOf('.');
        var tienePunto = moneyText.toString().includes('.');
        if(moneyText.length>0){
            if(tienePunto===false){
                $(this).val(moneyText+'.00');
            }else{
                var beforePunto =moneyText.substring(xy+1);
                if(beforePunto.length===0){
                    $(this).val(moneyText+'00');
                }else if(beforePunto.length===1){
                    $(this).val(moneyText+'0');
                }
            }
        }
    });

});