function alerError(msg) {
    $.toast({
        heading: 'ERROR',
        text: msg,
        icon: 'error',
        position: 'top-right',
        hideAfter: '2500',
    });
}
function alerSuccess(msg) {
    $.toast({
        heading: 'EXITOSO',
        text: msg,
        icon: 'success',
        position: 'top-right',
        hideAfter: '2500',
    });
}
function alerInfo(msg) {
    $.toast({
        heading: 'INFORMACION',
        text: msg,
        icon: 'info',
        position: 'top-right',
        hideAfter: '2500',
    });
}
function alerWar(msg) {
    $.toast({
        heading: 'ALERTA',
        text: msg,
        icon: 'warning',
        position: 'top-right',
        hideAfter: '2500',
    });
}
export default {
    alerSuccess: alerSuccess,
    alerError: alerError,
    alerInfo: alerInfo,
    alerWar: alerWar
}
