import alerta from "../alertToas.js";

function setImage(frmData) {
    $.ajax({
        data: frmData,
        url: '../ajax/SetImage/uploadImage.php',
        type: 'POST',
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {
            var json = JSON.parse(JSON.stringify(response));
            if (json) {
                alerta.alerSuccess('Cargando nueva imagen');
            } else {
                alerta.alerInfo('no se pudo cargar nueva imagen');
            }
        },
        error: function () {
            alerta.alerError('Error al cargar imagen<br>Reporte este error!!');
        }
    });
}

export default {
    set: setImage,
}
