<div class="modal fade" id="modal_categoria" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xs " role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nuevo Categoria</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                <form action="#">
                    <div class="container-fluid">
                        <div class="form-group col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Nombre:</label>
                            <input id="modal-categoria-input-nombre" class="form-control" type="text" placeholder="ejem. LADY2019" required>
                        </div>
                        <!--<div class="form-group  col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Descripccion:</label>
                            <textarea id="modal-categoria-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                        </div>-->
                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <a  type="submit" id="modal-categoria-btn-guardar" class="btn btn-primary">
                            Guardar
                        </a>
                        <button  type="button" id="modal-categoria-btn-limpiar" class="btn btn-default">
                            Limpiar
                        </button>
                        <button  type="button" id="modal-categoria-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


