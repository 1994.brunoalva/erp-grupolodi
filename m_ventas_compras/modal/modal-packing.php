<div class="modal fade" id="modal_packing" tabindex="-1" role="dialog" aria-hidden="true"
     xmlns="http://www.w3.org/1999/html">
    <div class="modal-dialog modal-xs modal-bg-none" role="document" style="height: 100vh;display: flex; align-items: center">
        <div class="modal-content" style="margin: auto;">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header no-padding">
                    <h4 class="modal-title t-1 bg-info" style="color: black; padding: 10px;"></h4>
                    <h4 class="modal-title t-2"></h4>
                    <h4 class="modal-title t-3"></h4>
                </div>
            </div>
            <div class="modal-body  no-border">
                <form action="#">
                    <div class="container-fluid">
                        <div class="form-group col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">CANTIDAD:</label>
                            <input id="modal-packing-input-nombre" class="form-control input-number" maxlength="4" type="text" placeholder="# de items" required>
                            <input id="modal-packing-input-produ-id" class="form-control no-display" type="text">
                            <input id="modal-packing-input-produ-cantidad" class="form-control no-display" type="text">
                            <input id="modal-packing-input-id-index" class="form-control no-display" type="text">
                        </div>
                        <!--<div class="form-group  col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Descripccion:</label>
                            <textarea id="modal-packing-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                        </div>-->
                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <a  type="submit" id="modal-packing-btn-agregar" class="btn btn-primary">
                            Guardar
                        </a>
                        <button  type="button" id="modal-packing-btn-limpiar" class="btn btn-default">
                            Limpiar
                        </button>
                        <button  type="button" id="modal-packing-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<style>
    .modal-backdrop {
        background-color: rgba(0,0,0,.0001) !important;
    }
</style>