<div class="modal fade" id="modal_almacen" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Nuevo almacen</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                <div id="form-modal-almacen" >
                    <div class="container-fluid">
                        <div class="row text-left no-padding">
                            <div class="form-group col-xs-12 col-sm-5 col-md-5">

                                <label label class="col-xs-12 no-padding">BUSCAR POR N° RUC :</label>
                                <div class="input-group  col-xs-12 no-padding ">
                                    <input id="modal-almacen-input-ruc" name="folder_input_ruc_emp" maxlength="11"
                                           required
                                           placeholder="Ingrese numero de Ruc" data-date-format="yyyy-dd-mm"
                                           class='form-control'/>
                                    <span class="input-group-btn">
                                            <a class="btn btn-primary" id="modal-almacen-btn-buscar"><i
                                                        class="fa fa-search"></i></a>
                                    </span>
                                </div>

                            </div>
                            <div class="form-group col-xs-12 col-sm-7 col-md-7">
                                <label class="col-xs-12 no-padding">RAZON SOCIAL :</label>
                                <input id="modal-almacen-input-razon-social" disabled type="text" required
                                       class="form-control col-xs-12 bg-blanco" placeholder="Razon social">
                            </div>
                            <!--OCULTO  Y  POR APROBAR SU UTILIZACION-->
                            <div class="form-group col-xs-12 col-sm-12 col-md-6 no-display">
                                <label class="col-xs-12 no-padding">JURISDICCION :</label>
                                <input id="modal-almacen-input-juris" disabled type="text" required
                                       class="form-control col-xs-12 bg-blanco" placeholder="Direccion fiscal">
                            </div>
                            <!--OCULTO  Y  POR APROBAR SU UTILIZACION-->
                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <label class="col-xs-12 no-padding">REPRESENTANTE :</label>
                                <input id="modal-almacen-input-represen" disabled type="text" required
                                       class="form-control col-xs-12 bg-blanco" placeholder="Direccion fiscal">
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <label class="col-xs-12 no-padding">CARGO :</label>
                                <input id="modal-almacen-input-cargo" disabled type="text" required
                                       class="form-control col-xs-12 bg-blanco" placeholder="Cargo de representante">
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                <label class="col-xs-12 no-padding">DIRECCION :</label>
                                <input id="modal-almacen-input-direccion" disabled type="text" required
                                       class="form-control col-xs-12 bg-blanco" placeholder="Direccion fiscal">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                <label class="col-xs-12 no-padding">TELEFONO 2 :</label>
                                <input id="modal-almacen-input-telefono-1" type="text" required
                                       class="form-control col-xs-12 input-number" placeholder="Telef. N° ">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                <label class="col-xs-12 no-padding">TELEFONO 2 :</label>
                                <input id="modal-almacen-input-telefono-2" type="text" required
                                       class="form-control col-xs-12 input-number" placeholder="Telef. N° ">
                            </div>

                        </div>
                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <button type="button" id="modal-almacen-btn-guardar" class="btn btn-primary">
                            Guardar
                        </button>
                        <button type="button" id="modal-almacen-btn-limpiar" class="btn btn-default">
                            Limpiar
                        </button>
                        <button type="button" id="modal-almacen-btn-cerrar" class="btn btn-success"
                                data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script type="module" src="../aConfig/Myjs/modal-almacen.js"></script>-->

