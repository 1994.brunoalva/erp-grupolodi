<div class="modal fade" id="modal_puerto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xs " role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nuevo Puerto</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                    <div class="container-fluid">
                        <div class="form-group col-xs-12">
                            <label class="col-xs-12 no-padding">Nombre:</label>
                            <input id="modal-puerto-input-nombre" class="form-control" type="text" placeholder="ejem. HONG KONG" required>
                        </div>
                        <!--<div class="form-group  col-xs-6">
                            <label class="col-xs-12 no-padding">Descripccion:</label>
                            <div class="input-group  col-xs-12 no-padding">
                                <select id="modal_select_pais" required
                                        class="selectpicker form-control no-padding" data-live-search="true"
                                        data-size="5">
                                    <option value="0" selected>-Seleccione-</option>
                                </select>

                                <span class="input-group-btn">
                                    <button id="boton_modales" type="button" class="btn btn-primary"  data-toggle="modal" data-target="#modal_pais"><i
                                            class="fa fa-plus"></i></button>
                                </span>
                            </div>
                        </div>-->
                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <button type="submit" id="modal-puerto-btn-guardar" class="btn btn-primary">
                            Guardar
                        </button>
                        <button  type="button" id="modal-puerto-btn-limpiar" class="btn btn-default">
                            Limpiar
                        </button>
                        <button  type="button" id="modal-puerto-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>-->
            <!--<hr style="width: 100%;border-bottom: 0.2em #0866C6;">-->
            <!--<div class="modal-footer">

            </div>-->
        </div>
    </div>
</div>


