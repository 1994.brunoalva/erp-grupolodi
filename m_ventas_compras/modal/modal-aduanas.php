<div class="modal fade" id="modal_aduanas" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nueva agencia de Aduanas</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                <div class="container-fluid">
                    <div class="form-group col-xs-12 col-sm-8 col-md-5">
                        <label class="col-xs-12 no-padding">RUC:</label>
                        <div id="aduana-box" class="input-group col-xs-12 no-padding">
                            <input id="modal-aduana-input-ruc-num" class="form-control" type="text"
                                   value="" placeholder="Numero de RUC">
                            <span class="input-group-btn">
                    <button id="modal-aduana-btn-ruc-num" type="button" class="btn btn-primary" >
                        <i class="fa fa-search"></i> Consultar</button>
                </span>
                        </div>
                    </div>

                    <div class="form-group col-xs-12 col-md-7">
                        <label class="col-xs-12 no-padding">Nombre:</label>
                        <input id="modal-aduanas-input-nombre" class="form-control" type="text"
                               placeholder="ejem. SALINAS & CASSARETTO" required>
                    </div>
                    <div class="form-group col-xs-12 col-md-7">
                        <label class="col-xs-12 no-padding">Jurisdicion:</label>
                        <input id="modal-aduanas-input-juris" class="form-control" type="text"
                               placeholder="ejem. LIMA CALLAO" required>
                    </div>
                    <div class="form-group col-xs-12 col-md-5">
                        <label class="col-xs-12 no-padding">Codigo:</label>
                        <input id="modal-aduanas-input-codigo" class="form-control" type="text" placeholder="#codigo"
                               required>
                    </div>
                </div>
                <div class="container-fluid">
                    <hr class="line-frame-modal">
                </div>
                <div class="container-fluid text-right">
                    <button type="submit" id="modal-aduanas-btn-guardar" class="btn btn-primary">
                        Guardar
                    </button>
                    <button type="button" id="modal-aduanas-btn-limpiar" class="btn btn-default">
                        Limpiar
                    </button>
                    <button type="button" id="modal-aduanas-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script type="module" src="../aConfig/Myjs/modal-proveedor.js"></script>-->

