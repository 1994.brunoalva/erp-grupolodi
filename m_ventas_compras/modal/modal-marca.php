<div class="modal fade" id="modal_marca" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800;">
    <div class="modal-dialog modal-xs " role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nueva Marca</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                    <div class="container-fluid">
                        <div class="form-group col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Nombre:</label>
                            <input id="modal-marca-input-nombre" class="form-control" type="text"
                                   placeholder="ejem. CHINA TYRE" required>
                        </div>
                        <div class="form-group col-xs-12 no-padding text-center"
                             style="max-height: 275px;padding: 15px; border: 1px solid rgba(10,107,206,0.6);border-radius: 1px;">
                            <div class="col-xs-12" style="padding: 0;">

                                <div class="md-12" width="100%"
                                     style="max-height: 167px; display: flex; padding: 15px;">
                                    <img src="../imagenes/logo/img-icon.svg"
                                         id="img-file-preview-zone" width="auto" style="margin: auto"
                                         height="170">
                                </div>

                                <div class="col-xs-12 text-center" style="margin-top: 25px;">
                                    <div class="form-group col-xs-12 no-margin no-padding">
                                        <h5 id="label-file-image"
                                            class="col-xs-12">
                                            ( Ninguna imagen seleccionada.... )
                                        </h5>
                                    </div>
                                    <input type="file" accept=".png" id="input-file-image"
                                           name="input-file-image" name="attachments[]"
                                           class="no-display">
                                    <div class="form-group col-xs-6">
                                        <label type="button" for="input-file-image"
                                               class="btn btn-sm btn-primary col-xs-12"
                                               title="Subir logotipo de importador">
                                            <i class="fa fa-file"></i> Subir Logo
                                        </label>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <label id="modal-marca-btn-eliminar-logo" type="button"
                                               class="btn btn-sm btn-danger col-xs-12"
                                               title="Subir logotipo de importador">
                                            <i class="fa fa-remove"></i> Eliminar
                                        </label>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        /* $(document).ready(function () {
                             $('#fileupload').fileupload();
                         });*/
                    </script>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <button type="submit" id="modal-marca-btn-guardar" class="btn btn-primary">
                            Guardar
                        </button>
                        <button type="button" id="modal-marca-btn-limpiar" class="btn btn-default">
                            Limpiar
                        </button>
                        <button type="button" id="modal-marca-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
            </div>
        </div>
    </div>
</div>
<style>
    #img-file-preview-zone{
        -webkit-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
        -moz-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
        box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
        border-radius: 3px;
    }
</style>

