<div class="modal fade" id="modal_buscar_almacen" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Buscar Almacenes Aduaneros</h3>
                </div>
            </div>
            <style>
                /*.bg-head-table tr th {
                    padding: 0;
                }*/

                div.dataTables_wrapper div.dataTables_info {
                    display: none;
                }

                div.dataTables_wrapper div.dataTables_length {
                    display: none;
                }

                /*


                                table.dataTable thead > tr > th.sorting {
                                    padding-right: 13px;
                                }*/
                #table-almacen_filter {
                   /* display: none;*/
                }
            </style>
            <div class="modal-body  no-border">
                <form action="#">
                    <div class="container-fluid">
                        <div class="col-xs-12 no-padding">
                            <div id="" class="dataTables_filter">
                                <label>Buscar:
                                    <input class="form-control input-sm input-search-alm" placeholder=""
                                           aria-controls="table-proveedor">
                                </label>
                            </div>
                        </div>
                        <div class="table-responsive form-group col-xs-12 no-padding">
                            <table id="table-almacen" class="table table-striped table-bordered table-hover">
                                <thead class="bg-head-table">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-left">RAZON SOCIAL</th>
                                    <th class="text-left">REPRESENTANTE</th>
                                    <th class="text-left">DIRECCION</th>
                                    <th class="text-left">OPCION</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $almacen = new Almacen('SELECT');
                                $resultAlm = $almacen->selectAll();
                                $indice = 0;
                                foreach ($resultAlm as $item) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo ++$indice; ?></td>
                                        <td class="text-left">
                                            <label><?php echo $item->alm_razon_social; ?></label>
                                        </td>
                                        <td class="text-left"><?php echo $item->alm_rep; ?></td>
                                        <td class="text-left"><?php echo $item->alm_dir; ?></td>
                                        <td class="text-center">
                                            <a id="btn-" class="btn btn-sm btn-danger fa fa-check btn-option"
                                               title="Anadir item" data-dismiss="modal"></a>
                                            <input class="emp_id no-display" type="text"
                                                   value="<?php echo $item->alm_id; ?>">
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>

                            </table>
                        </div>

                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <!-- <a type="submit" id="modal-buscar-almacen-btn-guardar" class="btn btn-primary">
                             Guardar
                         </a>
                         <button type="button" id="modal-buscar-almacen-btn-limpiar" class="btn btn-default">
                             Limpiar
                         </button>-->
                        <button type="button" id="modal-buscar-almacen-btn-agregar" class="btn btn-primary"
                                data-toggle="modal" data-target="#modal_almacen">
                            Agregar
                        </button>
                        <button type="button" id="modal-buscar-almacen-btn-cerrar" class="btn btn-success"
                                data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#table-almacen').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
    });
    $(document).ready(function () {
        $('select').selectpicker();
    });
</script>

