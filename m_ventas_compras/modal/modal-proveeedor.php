<div class="modal fade" id="modal_proveedor" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Nuevo Proveedor</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                    <div class="container-fluid">
                        <div class="form-group col-xs-12 col-sm-12 col-md-6 no-padding">
                            <label class="col-xs-12">PAIS:</label>
                            <div class="input-group  col-xs-12 no-padding">
                                <select id="select_modal_pais" required
                                        class="selectpicker form-control no-padding" data-live-search="true"
                                        data-size="5">
                                    <option value="0" selected>-Seleccione-</option>
                                    <?php
                                    $Pais = new Pais('SELECT');
                                    $datosPais = $Pais->selectAll();
                                    foreach ($datosPais as $row){
                                        if ($row->pais_id != 348){
                                            echo '<option value="'.$row->pais_id.'">'.$row->pais_nombre.'</option>';
                                        }

                                    }
                                    ?>
                                </select>

                                <span class="input-group-btn">
                                    <button id="boton_modales" type="button" class="btn btn-primary"  data-toggle="modal" data-target="#modal_pais"><i
                                                class="fa fa-plus"></i></button>
                                </span>
                            </div>
                        </div>
                            <div hidden class="form-group col-xs-6 col-sm-5 col-md-3">
                                <label class="col-xs-12 no-padding">N° DOCUMENTO :</label>
                                <input id="modal-proveedor-input-num-doc" type="text" required class="form-control col-xs-12" placeholder="Numero de documento">
                            </div>
                            <div class="form-group col-xs-6 col-sm-7 col-md-6">
                                <label class="col-xs-12 no-padding">PROVEEDOR:</label>
                                <input id="modal-proveedor-input-nom_proveedor" type="text" required class="form-control col-xs-12" placeholder="Digite razon social de proveedor">
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-12 col-md-4">
                                <label class="col-xs-12 no-padding">TIPO DOCUMENTO :</label>
                                <div class="input-group  col-xs-12 no-padding">
                                    <select id="select_modal_prov_tipo_documento" required
                                            class="selectpicker form-control no-padding"
                                            aria-label="select for following text input" data-live-search="true"
                                            data-size="5">
                                        <option value="0" selected>-Seleccione-</option>
                                        <?php
                                        $tipoDocumento = new TipoDocumento('SELECT');
                                        $datosDocumento = $tipoDocumento->getForSQl("SELECT * FROM sys_adm_documentos WHERE doc_tipo= 'DE IDENTIDAD'");
                                        foreach ($datosDocumento as $row){
                                            echo '<option value="'.$row->doc_id.'">'.$row->doc_nombre.'</option>';
                                        }
                                        ?>
                                    </select>
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_tipo_documento"><i
                                            class="fa fa-plus"></i></button>
                                </span>
                                </div>

                        </div>
                        <div class="row text-left no-padding">
                            <div class="form-group col-xs-6 col-sm-6 col-md-3">
                                <label class="col-xs-12 no-padding">TELEFONO N°1 :</label>
                                <input id="modal-proveedor-input-telf-1" type="text" required class="form-control col-xs-12" placeholder="Telef. N° 1">
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 col-md-3">
                                <label class="col-xs-12 no-padding">TELEFONO N°2 :</label>
                                <input id="modal-proveedor-input-tlf-2" type="text" class="form-control col-xs-12" placeholder="Telef. N° 2">

                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                <label class="col-xs-12 no-padding">EMAIL :</label>
                                <input id="modal-proveedor-input-email" required class="form-control col-xs-12" placeholder="myEmail@myEmpresa.com">

                            </div>
                        </div>
                        <div class="row text-left no-padding">
                            <div class="form-group col-xs-12 col-sm-5 col-md-3">
                                <label class="col-xs-12 no-padding">CONTACTO :</label>
                                <input id="modal-proveedor-input-contacto" type="text" required class="form-control col-xs-12" placeholder="Contacto para informacion">

                            </div>
                            <div class="form-group col-xs-12 col-sm-7 col-md-9">
                                <label class="col-xs-12 no-padding">DIRECCION :</label>
                                <input id="modal-proveedor-input-direccion" type="text" required class="form-control col-xs-12" placeholder="Direccion del proveedor ?">

                            </div>

                        </div>

                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <button type="submit" id="modal-proveedor-btn-guardar" class="btn btn-primary">
                            Guardar
                        </button>
                        <button type="button" id="modal-proveedor-btn-limpiar" class="btn btn-default">
                            Limpiar
                        </button>
                        <button type="button" id="modal-proveedor-btn-cerrar" class="btn btn-success"
                                data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
            </div>
        </div>
    </div>
</div>
<!--<script type="module" src="../aConfig/Myjs/modal-proveedor.js"></script>-->

