<div class="modal fade" id="modal_incoterm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xs " role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nuevo Incoterm</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                <form action="#">
                    <div class="container-fluid">
                        <div class="form-group col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Nombre:</label>
                            <input id="modal-incoterm-input-nombre" class="form-control" type="text" placeholder="ejem. LADY2019" required>
                        </div>
                        <div class="form-group  col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Descripccion:</label>
                            <textarea id="modal-incoterm-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <a  type="submit" id="modal-incoterm-btn-guardar" class="btn btn-primary">
                            Guardar
                        </a>
                        <button  type="button" id="modal-incoterm-btn-limpiar" class="btn btn-default">
                            Limpiar
                        </button>
                        <button  type="button" id="modal-incoterm-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>-->
            <!--<hr style="width: 100%;border-bottom: 0.2em #0866C6;">-->
            <!--<div class="modal-footer">

            </div>-->
        </div>
    </div>
</div>


