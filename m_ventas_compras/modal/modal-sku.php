<style>
    #modal_sku{
        padding-top :  16% !important;
    }

</style>

<div class="modal fade" id="modal_sku" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800;">
    <div class="modal-dialog" role="document" style="margin: 0 auto ;">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <!--<div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nueva sku</h3>
                </div>-->
            </div>
            <div class="modal-body  no-border">

                    <div id="content-sku" class="container-fluid text-center">
                        <svg id='barcode'>
                    </div>

                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                      <!--  <button type="submit" id="modal-sku-btn-guardar" class="btn btn-primary">
                            Guardar
                        </button>
                        <button type="button" id="modal-sku-btn-limpiar" class="btn btn-default">
                            Limpiar
                        </button>-->
                        <button type="button" id="modal-sku-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#modal-producto-btn-view-sku').click(function () {
            var miCode = $('#modal-producto-input-sku').val();
            JsBarcode('#barcode', miCode, {
                lineColor: "#000",
                height: 30,
                displayValue: true
            });
        });

    });
</script>