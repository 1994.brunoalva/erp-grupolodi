<style>
    #modal_estado {
        padding-top: 16% !important;
    }

</style>

<div class="modal fade" id="modal_estado" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800;">
    <div class="modal-dialog" role="document" style="margin: 0 auto ;">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <!--<div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nueva sku</h3>
                </div>-->
            </div>
            <div class="modal-body  no-border">
                <div class="input-group col-xs-12 no-padding text-center" style="padding-bottom: 20px;">
                    <label id="data-fol"></label>
                </div>
                <div class="input-group col-xs-12 no-padding text-center no-display">
                    <input id="fol_id" class="">
                </div>
                <div class="input-group col-xs-12 no-padding text-center no-display">
                    <input id="deta_id" class="">
                </div>
                <div class="input-group col-xs-12 no-padding">
                    <select id="select_estado" required
                            class="selectpicker form-control show-tick no-padding" data-live-search="true"
                            data-size="5">
                        <option value="0" selected>-Seleccione-</option>
                        <?php
                        $estado = new Estado('SELECT');
                        $datos = $estado->selectAll();
                        foreach ($datos as $row) {
                                echo '<option value="' . $row->est_id . '">' . $row->est_nombre . '</option>';
                        }
                        ?>
                    </select>
                </div>

                <div class="container-fluid">
                    <hr class="line-frame-modal">
                </div>
                <div class="container-fluid text-right">

                    <!--  <button type="submit" id="modal-sku-btn-guardar" class="btn btn-primary">
                          Guardar
                      </button>
                      <button type="button" id="modal-sku-btn-limpiar" class="btn btn-default">
                          Limpiar
                      </button>-->
                    <button type="button" id="modal-sku-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#modal-producto-btn-view-sku').click(function () {
            var miCode = $('#modal-producto-input-sku').val();
            JsBarcode('#barcode', miCode, {
                lineColor: "#000",
                height: 30,
                displayValue: true
            });
        });

    });
</script>