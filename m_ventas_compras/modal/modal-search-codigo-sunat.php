<div class="modal fade" id="modal_buscar_codigo_sunat" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800;">
    <div class="modal-dialog modal-xs " role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Buscar codigo sunat</h3>
                </div>
            </div>
            <style>
                .bg-head-table tr th {
                    padding: 0;
                }

                div.dataTables_wrapper div.dataTables_info {
                    display: none;
                }

                div.dataTables_wrapper div.dataTables_length {
                    display: none;
                }

                /*


                                table.dataTable thead > tr > th.sorting {
                                    padding-right: 13px;
                                }*/
                #table-codigo-sunat_filter {
                    display: none;
                }
            </style>
            <div class="modal-body  no-border">
                <div class="container-fluid">
                    <div class="col-xs-12 no-padding">
                        <div id="" class="dataTables_filter">
                            <label>Buscar:
                                <input class="form-control input-sm input-search-cods">
                            </label>
                        </div>
                    </div>


                    <div class="form-group col-xs-12 no-padding" style="max-height: 320px;overflow-x:hidden;">
                        <table id="table-codigo-sunat" class="table table-striped table-bordered table-hover"
                               style="width: 100%;">
                            <thead class="bg-head-table">
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-left">CODIGO</th>
                                <th class="text-left">DESCRIPCCION</th>
                                <th class="text-left">OPCION</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $codSunat = new CodSunat('SELECT');
                            $result = $codSunat->selectAll();
                            $index=0;
                            foreach ($result as $item) {
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo ++$index;?></td>
                                    <td class="text-left">
                                        <label><?php echo $item->sunat_cod_codigo;?></label>
                                    </td>
                                    <td class="text-left">
                                        <div id="desc"><?php echo $item->sunat_cod_desc;?></div>
                                    </td>
                                    <td class="text-center">
                                        <a id="btn-" class="btn btn-sm btn-danger fa fa-check btn-option"
                                           title="Anadir item" data-dismiss="modal"></a>
                                        <input class="emp_id no-display" type="text" value="<?php echo $item->sunat_cod_id; ?>">
                                    </td>

                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>

                        </table>
                    </div>


                    <!--<div class="form-group  col-xs-12 no-padding">
                        <label class="col-xs-12 no-padding">Descripccion:</label>
                        <textarea id="modal-buscar-codigo-sunat-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                    </div>-->
                </div>
                <div class="container-fluid">
                    <hr class="line-frame-modal">
                </div>
                <div class="container-fluid text-right">

                    <!-- <a type="submit" id="modal-buscar-codigo-sunat-btn-guardar" class="btn btn-primary">
                         Guardar
                     </a>
                     <button type="button" id="modal-buscar-codigo-sunat-btn-limpiar" class="btn btn-default">
                         Limpiar
                     </button>-->
                    <!--<button type="button" id="modal-buscar-codigo-sunat-btn-agregar" class="btn btn-primary"
                            data-toggle="modal" data-target="#modal_aduanas">
                        Agregar
                    </button>-->
                    <button type="button" id="modal-buscar-codigo-sunat-btn-cerrar" class="btn btn-success"
                            data-dismiss="modal">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#table-codigo-sunat').DataTable({
            /*scrollY: false,*/
            /*scrollX: true,*/
            paging: false,
           /* lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],*/
            language: {
                url: '../assets/Spanish.json'
            }
        });
    });
</script>


<style>
    #img-file-preview-zone{
        -webkit-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
        -moz-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
        box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
        border-radius: 3px;
    }
</style>

