<div class="modal fade" id="modal_buscar_empresa" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Buscar <!--Empresa-->Importadora</h3>
                </div>
            </div>
            <style>
                .bg-head-table tr th {
                    padding: 0;
                }

                div.dataTables_wrapper div.dataTables_info {
                    display: none;
                }

                div.dataTables_wrapper div.dataTables_length {
                    display: none;
                }

                /*


                                table.dataTable thead > tr > th.sorting {
                                    padding-right: 13px;
                                }*/
                #table-empresa_filter {
                    display: none;
                }
            </style>
            <div class="modal-body  no-border">
                <form action="#">
                    <div class="container-fluid">
                        <div class="col-xs-12 no-padding">
                            <div id="" class="dataTables_filter">
                                <label>Buscar:
                                    <input class="form-control input-sm input-search-emp" placeholder=""
                                           aria-controls="table-proveedor">
                                </label>
                            </div>
                        </div>
                        <div class="table-responsive form-group col-xs-12 no-padding">
                            <table id="table-empresa" class="table table-striped table-bordered table-hover">
                                <thead class="bg-head-table">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-left">NOMBRE</th>
                                    <th class="text-left">RUC</th>
                                    <th class="text-left">TELEFONO</th>
                                    <th class="text-left">DIRECCION</th>
                                    <th class="text-left">ESTADO</th>
                                    <th class="text-left">OPCION</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $empresa = new Empresa('SELECT');
                                $resultEmp = $empresa->selectAll();
                                $indice = 0;
                                foreach ($resultEmp as $item) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo ++$indice; ?></td>
                                        <td class="text-left">
                                            <label><?php echo $item->emp_nombre; ?></label>
                                        </td>
                                        <td class="text-left">
                                            <div id="emp_num_ruc" ><?php echo $item->emp_ruc; ?></div>
                                        </td>
                                        <td class="text-left"><?php echo $item->emp_telefono; ?></td>
                                        <td id="emp-dir" class="text-left"><?php echo $item->emp_direccion; ?></td>
                                        <td class="text-center"><?php echo ($item->emp_estatus) ?'ACTIVO':'INACTIVO'; ?></td>
                                        <td class="text-center">
                                            <a id="btn-" class="btn btn-sm btn-danger fa fa-check btn-option"
                                               title="Anadir item" data-dismiss="modal"></a>
                                            <input class="emp_id no-display" type="text" value="<?php echo $item->emp_id; ?>">
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>

                            </table>
                        </div>

                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <!-- <a type="submit" id="modal-buscar-empresa-btn-guardar" class="btn btn-primary">
                             Guardar
                         </a>
                         <button type="button" id="modal-buscar-empresa-btn-limpiar" class="btn btn-default">
                             Limpiar
                         </button>-->
                        <button type="button" id="modal-buscar-empresa-btn-agregar" class="btn btn-primary"
                                data-toggle="modal" data-target="#modal_empresa">
                            Agregar
                        </button>
                        <button type="button" id="modal-buscar-empresa-btn-cerrar" class="btn btn-success"
                                data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#table-empresa').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
    });
    $(document).ready(function () {
        $('select').selectpicker();
    });
</script>

