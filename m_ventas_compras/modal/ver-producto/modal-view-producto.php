<div class="modal fade" id="modal_neumatico" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400;">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title text-title-producto"">NUEMATICO</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                <div class="container-fluid">
                    <div id="p_c" class="row text-left no-padding">
                        <div class="form-group col-xs-6 col-sm-4">
                            <label class="col-xs-12 no-padding">Marca :</label>
                            <input disabled id="p-marca" type="text" class="form-control col-xs-1">
                        </div>
                        <div class="form-group col-xs-6 col-sm-4">
                            <label class="col-xs-12 no-padding">Modelo :</label>
                            <div class="input-group  col-xs-12 no-padding">
                                <input disabled id="p-modelo" type="text" class="form-control col-xs-1">
                            </div>
                        </div>
                        <div class="form-group col-xs-4 col-sm-4">
                            <label class="col-xs-12 no-padding">Unidad m. :</label>
                            <div class="input-group  col-xs-12 no-padding">
                                <input disabled id="p-unidad" type="text" class="form-control col-xs-1">
                            </div>
                        </div>
                        <div class="form-group col-xs-8 col-sm-6">
                            <label class="col-xs-12 no-padding">sku :</label>
                            <input disabled id="p-sku" type="text" class="form-control col-xs-1">
                        </div>
                        <div class="form-group col-xs-6 col-sm-3">
                            <label class="col-xs-12 no-padding">Cod. Sunat :</label>
                            <input disabled id="p-cod" type="text" class="form-control col-xs-1">
                        </div>
                        <div class="form-group col-xs-6 col-sm-3">
                            <label class="col-xs-12 no-padding">PN :</label>
                            <div class="input-group  col-xs-12 no-padding">
                                <input disabled id="p-pn" type="text" class="form-control col-xs-1">
                            </div>
                        </div>
                        <div class="form-group col-xs-12 col-sm-7">
                            <label class="col-xs-12 no-padding"><!--Nombre-->Nombre :</label>
                            <input disabled id="p-nombre" type="text" class="form-control col-xs-1">
                        </div>

                        <div class="form-group col-xs-6 col-sm-5">
                            <label class="col-xs-12 no-padding">Pais :</label>
                            <div class="input-group  col-xs-12 no-padding">
                                <input disabled id="p-pais" type="text" class="form-control col-xs-12">
                            </div>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12">
                            <label class="col-xs-12 no-padding"><!--Nombre-->Descripccion :</label>
                            <input disabled id="p-desc" type="text" class="form-control col-xs-1">
                        </div>




                        <!--<div id="p_c">

                        </div>-->

                    </div>



                </div>
                <div class="container-fluid">
                    <hr class="line-frame-modal">
                </div>
                <div class="container-fluid text-right">
                    <button type="button" id="modal-neumatico-btn-cerrar" class="btn btn-success"
                            data-dismiss="modal">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script type="module" src="../aConfig/Myjs/modal-neumatico.js"></script>-->

