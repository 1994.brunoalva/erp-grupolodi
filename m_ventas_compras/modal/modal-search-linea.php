<div class="modal fade" id="modal_buscar_linea" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Buscar lineas</h3>
                </div>
            </div>
            <style>
                .bg-head-table tr th {
                    padding: 0;
                }

                div.dataTables_wrapper div.dataTables_info {
                    display: none;
                }

                div.dataTables_wrapper div.dataTables_length {
                    display: none;
                }

                /*


                                table.dataTable thead > tr > th.sorting {
                                    padding-right: 13px;
                                }*/
                #table-linea_filter {
                    display: none;
                }
            </style>
            <div class="modal-body  no-border">
                <div class="container-fluid">
                    <div class="col-xs-12 no-padding">
                        <div id="" class="dataTables_filter">
                            <label>Buscar:
                                <input class="form-control input-sm input-search-lin" placeholder=""
                                       aria-controls="table-linea">
                            </label>
                        </div>
                    </div>


                    <div class="form-group col-xs-12 no-padding" style="overflow-x:hidden;">
                        <table id="table-linea" class="table table-striped table-bordered table-hover"
                               style="width: 100%;">
                            <thead class="bg-head-table">
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-left">CODIGO SUNAT</th>
                                <th class="text-left">RAZON SOCIAL</th>
                                <th class="text-left">OPCION</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $linea = new linea('SELECT');
                            $result = $linea->selectAll();
                            $index=0;
                            foreach ($result as $item) {
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo ++$index;?></td>
                                    <td class="text-left"><?php echo $item->linea_codigo;?></td>
                                    <td class="text-left">
                                        <label><?php echo $item->linea_nombre;?></label>
                                    </td>
                                    <td class="text-center">
                                        <a id="btn-" class="btn btn-sm btn-danger fa fa-check btn-option"
                                           title="Anadir item" data-dismiss="modal"></a>
                                        <input class="forwa_id no-display" type="text" value="<?php echo $item->linea_id; ?>">
                                    </td>

                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>

                        </table>
                    </div>


                    <!--<div class="form-group  col-xs-12 no-padding">
                        <label class="col-xs-12 no-padding">Descripccion:</label>
                        <textarea id="modal-buscar-linea-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                    </div>-->
                </div>
                <div class="container-fluid">
                    <hr class="line-frame-modal">
                </div>
                <div class="container-fluid text-right">

                    <!-- <a type="submit" id="modal-buscar-linea-btn-guardar" class="btn btn-primary">
                         Guardar
                     </a>
                     <button type="button" id="modal-buscar-linea-btn-limpiar" class="btn btn-default">
                         Limpiar
                     </button>-->
                    <button type="button" id="modal-buscar-linea-btn-agregar" class="btn btn-primary"
                            data-toggle="modal" data-target="#modal_linea">
                        Agregar
                    </button>
                    <button type="button" id="modal-buscar-linea-btn-cerrar" class="btn btn-success"
                            data-dismiss="modal">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#table-linea').DataTable({
            /*scrollY: false,*/
            /*scrollX: true,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
    });
</script>


