<div class="modal fade" id="modal_buscar_producto" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400;">
    <div class="modal-dialog" role="document" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Buscar Productos</h3>
                </div>
            </div>
            <style>
                .bg-head-table tr th {
                    padding: 0;
                }

                div.dataTables_wrapper div.dataTables_info {
                    display: none;
                }

                div.dataTables_wrapper div.dataTables_length {
                    display: none;
                }

                /*


                                table.dataTable thead > tr > th.sorting {
                                    padding-right: 13px;
                                }*/
                #table-productos_filter {
                    display: none;
                }
            </style>
            <div class="modal-body  no-border">
                <div class="container-fluid">
                    <div class="row no-padding">
                        <div class="form-group col-xs-12 col-sm-4">
                            <label>Categoria Producto:</label>
                            <div class="input-group col-xs-12 no-padding">
                                <select id="select_pro_categoria" required
                                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                                        data-size="5">
                                    <option value="0" selected>-Seleccione-</option>
                                    <?php
                                    $categoria = new Categoria('SELECT');
                                    $datos = $categoria->selectAll();
                                    foreach ($datos as $row) {
                                        echo '<option value="' . $row->cat_id . '">' . $row->cat_nombre . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 col-sm-4">
                            <label>Marca:</label>
                            <div class="input-group col-xs-12 no-padding">
                                <select id="select_pro_marca" required
                                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                                        data-size="5">
                                    <option value="0" selected>-Seleccione-</option>
                                </select>
                                <span class="input-group-btn">
                                    <button id="producto_btn_select_marca" type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#modal_marca">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 col-sm-4">
                            <label>Modelo:</label>
                            <div class="input-group col-xs-12 no-padding">
                                <select id="select_pro_modelo" required
                                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                                        data-size="5">
                                    <option value="0" selected>-Seleccione-</option>
                                </select>
                                <span class="input-group-btn">
                                    <button id="folio_btn_select_modelo" type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#modal_modelo">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </span>
                            </div>
                        </div>


                        <!--<script>
                            $(document).ready(function () {
                                $('#folio_btn_select_modelo').click();
                            });
                        </script>-->









                    </div>


                    <div class="form-group col-xs-12 no-padding" style="overflow-x:hidden;">

                        <div class="table-responsive">
                            <table id="table-productos" class="table table-striped table-bordered table-hover"
                                   style="width: 100%;">
                                <thead class="bg-head-table">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-left">SKU</th>
                                    <th class="text-left">NOMBRE</th>
                                    <th class="text-left">CATEGORIA</th>
                                    <th class="text-left">MARCA</th>
                                    <th class="text-left">MODELO</th>
                                    <th class="text-left">PAIS</th>
                                    <th class="text-left">OPCION</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                include '../ajax/Producto/getAllProducto-in.php';
                                $dataProducto = getAllProducto();
                                $index = 0;
                                foreach ($dataProducto as $item) {
                                    ?>
                                    <tr>
                                        <td class="text-center" style="color: white"><?php echo ++$index; ?></td>
                                        <td class="text-left">
                                            <a id="btn-sku" href="#"><?php echo $item->produ_sku; ?></a>
                                        </td>
                                        <td class="text-left">
                                            <label><a id="btn-nombre" href="#"><?php echo $item->produ_nombre; ?></a>
                                            </label>
                                        </td>
                                        <td id="btn-cat" class="text-left"><?php echo $item->cat_id->cat_nombre; ?></td>
                                        <td class="text-left"><?php echo $item->mar_id->mar_nombre; ?></td>
                                        <td class="text-left"><?php echo isset($item->mod_id->mod_nombre)?$item->mod_id->mod_nombre: ''?></td>
                                        <td class="text-left">
                                            <?php echo $item->pais_id->pais_nombre; ?>
                                            <a id="med-data" class="no-display" href="#"><?php echo $item->unidad_id->unidad_id; ?></a>
                                        </td>
                                        <td class="text-center">
                                            <a id="btn-a" class="btn btn-sm btn-danger fa fa-check btn-option"
                                               title="Anadir item" data-dismiss="modal"></a>
                                            <input class="emp_id no-display" type="text"
                                                   value="<?php echo $item->produ_id; ?>">
                                        </td>

                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                                <tfoot class="no-display">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-left">SKU</th>
                                    <th class="text-left">NOMBRE</th>
                                    <th class="text-left">CATEGORIA</th>
                                    <th class="text-left">MARCA</th>
                                    <th class="text-left">MODELO</th>
                                    <th class="text-left">PAIS</th>
                                    <th class="text-left">OPCION</th>
                                </tr>
                                </tfoot>

                            </table>

                        </div>


                    </div>


                    <!--<div class="form-group  col-xs-12 no-padding">
                        <label class="col-xs-12 no-padding">Descripccion:</label>
                        <textarea id="modal-buscar-productos-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                    </div>-->
                </div>
                <div class="container-fluid">
                    <hr class="line-frame-modal">
                </div>
                <div class="container-fluid text-right">

                    <!-- <a type="submit" id="modal-buscar-productos-btn-guardar" class="btn btn-primary">
                         Guardar
                     </a>
                     <button type="button" id="modal-buscar-productos-btn-limpiar" class="btn btn-default">
                         Limpiar
                     </button>-->
                    <button type="button" id="modal-buscar-productos-btn-agregar" class="btn btn-primary"
                            data-toggle="modal" data-target="#modal_producto">
                        Agregar Nuevo
                    </button>
                    <button type="button" id="modal-buscar-productos-btn-cerrar" class="btn btn-success"
                            data-dismiss="modal">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#table-productos tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input id="' + title + '" type="text" placeholder="Search ' + title + '" />');
        });

        var table = $('#table-productos').DataTable({
            paging: true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]

        });

        table.columns().every(function () {
            var that = this;
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
    });
</script>


