<div class="modal fade" id="modal_buscar_proveedor" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Buscar Proveedor</h3>
                </div>
            </div>
            <style>
                .bg-head-table tr td {
                    padding: 0;
                }

                div.dataTables_wrapper div.dataTables_info {
                    display: none;
                }

                div.dataTables_wrapper div.dataTables_lengtd {
                    display: none;
                }

                /*


                                table.dataTable tdead > tr > td.sorting {
                                    padding-right: 13px;
                                }*/
                #table-proveedor_filter {
                    display: none;
                }
            </style>
            <div class="modal-body  no-border">
                <form action="#">
                    <div class="container-fluid">
                        <div class="col-xs-12 no-padding">
                            <div id="" class="dataTables_filter">
                                <label>Buscar:
                                    <input class="form-control input-sm input-search" placeholder=""
                                           aria-controls="table-proveedor">
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 no-padding" style="">
                            <table id="table-proveedor" class="table table-striped table-bordered table-hover"
                                   style="width: 100%;">
                                <thead class="bg-head-table">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-left">PAIS</th>
                                    <th class="text-left">NOMBRE</th>
                                    <th class="text-left">TELEFONO</th>
                                    <th class="text-left">CONTACTO</th>
                                    <th class="text-left">OPCION</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                include '../ajax/Proveedores/getAllProveedores-in.php';
                                $resultProveedores = getAllProveedores();
                                $indice = 0;

                                foreach ($resultProveedores as $item) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo ++$indice; ?></td>
                                        <td class="text-center"><?php echo $item->pais_id->pais_nombre; ?></td>
                                        <td class="text-center">
                                            <label><?php echo $item->provee_desc; ?></label>
                                        </td>
                                        <td class="text-center"><?php echo $item->provee_telf; ?></td>
                                        <td class="text-center"><?php echo $item->provee_contacto; ?></td>
                                        <td class="text-center">
                                            <a id="btn-" class="btn btn-sm btn-danger fa fa-check btn-option"
                                               title="Anadir item"
                                               data-dismiss="modal"></a>
                                            <input class="no-display" type="text"
                                                   value="<?php echo $item->provee_id; ?>">
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>

                            </table>
                        </div>


                        <!--<div class="form-group  col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Descripccion:</label>
                            <textarea id="modal-buscar-proveedor-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                        </div>-->
                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <!-- <a type="submit" id="modal-buscar-proveedor-btn-guardar" class="btn btn-primary">
                             Guardar
                         </a>
                         <button type="button" id="modal-buscar-proveedor-btn-limpiar" class="btn btn-default">
                             Limpiar
                         </button>-->
                        <button type="button" id="modal-buscar-proveedor-btn-agregar" class="btn btn-primary"
                                data-toggle="modal" data-target="#modal_proveedor">
                            Agregar
                        </button>
                        <button type="button" id="modal-buscar-proveedor-btn-cerrar" class="btn btn-success"
                                data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#table-proveedor').DataTable({
            /*scrollY: false,*/
            /*scrollX: true,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
    });
    $(document).ready(function () {
        $('select').selectpicker();
    });
</script>
