<div class="modal fade" id="modal_tipo_documento" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1600;">
    <div class="modal-dialog modal-xs " role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nuevo Tipo de documento</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                <div class="container-fluid">
                    <div class="form-group col-xs-12 no-padding">
                        <label class="col-xs-12 no-padding">Nombre:</label>
                        <input id="modal-tipo-documento-input-nombre" class="form-control" type="text"
                               placeholder="ejem. LADY2019">
                    </div>
                    <div class="form-group  col-xs-12 no-padding">
                        <label class="col-xs-12 no-padding">Descripccion:</label>
                        <textarea id="modal-tipo-documento-input-descripccion" class="form-control" type="text" rows="3"
                                  style="resize: none; overflow: hidden;"></textarea>
                    </div>
                </div>
                <div class="container-fluid text-right">

                    <button  type="button" id="modal-tipo-documento-btn-guardar" class="btn btn-primary">
                        Guardar
                    </button>
                    <button  type="button" id="modal-tipo-documento-btn-limpiar" class="btn btn-default">
                        Limpiar
                    </button>
                    <button  type="button" id="modal-tipo-documento-btn-cerrar" class="btn btn-success">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


