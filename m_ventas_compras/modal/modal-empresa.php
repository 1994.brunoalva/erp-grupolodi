<div class="modal fade" id="modal_empresa" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Nueva Empresa</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                <div id="form-modal-empresa" >
                    <div class="container-fluid">
                        <div class="row text-left no-padding">
                            <div class="form-group col-xs-12 col-sm-5 col-md-5">

                                <label label class="col-xs-12 no-padding">N° RUC :</label>
                                <div class="input-group  col-xs-12 no-padding ">
                                    <input id="modal-empresa-input-ruc" name="folder_input_ruc_emp" maxlength="11"
                                           required
                                           placeholder="Ingrese numero de Ruc" data-date-format="yyyy-dd-mm"
                                           class='form-control'/>
                                    <span class="input-group-btn">
                                            <a class="btn btn-primary" id="modal-empresa-btn-buscar"><i
                                                        class="fa fa-search"></i></a>
                                    </span>
                                </div>

                            </div>
                            <div class="form-group col-xs-12 col-sm-7 col-md-7">
                                <label class="col-xs-12 no-padding">RAZON SOCIAL :</label>
                                <input id="modal-empresa-input-razon-social" disabled type="text" required
                                       class="form-control col-xs-12 bg-blanco" placeholder="Razon social">
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                <label class="col-xs-12 no-padding">DIRECCION :</label>
                                <input id="modal-empresa-input-direccion" disabled type="text" required
                                       class="form-control col-xs-12 bg-blanco" placeholder="Direccion fiscal">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label class="col-xs-12 no-padding">TELEFONO :</label>
                                <input id="modal-empresa-input-telefono" type="text" required
                                       class="form-control col-xs-12 input-number" placeholder="Telef. N° ">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label class="col-xs-12 no-padding">ESTADO</label>
                                <input id="modal-empresa-input-estado" disabled required
                                       class="form-control col-xs-12 bg-blanco" placeholder="Activo / Inactivo">

                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <button type="submit" id="modal-empresa-btn-guardar" class="btn btn-primary">
                            Guardar
                        </button>
                        <button type="button" id="modal-empresa-btn-limpiar" class="btn btn-default">
                            Limpiar
                        </button>
                        <button type="button" id="modal-empresa-btn-cerrar" class="btn btn-success"
                                data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script type="module" src="../aConfig/Myjs/modal-empresa.js"></script>-->

