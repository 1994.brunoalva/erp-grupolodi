<div class="modal fade" id="modal_pais" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800;">
    <div class="modal-dialog modal-xs " role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nuevo Pais</h3>
                </div>
            </div>
            <div class="modal-body  no-border">

                    <div class="container-fluid">
                        <div class="form-group col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Nombre:</label>
                            <input autocomplete="off" id="modal-pais-input-nombre" class="form-control" type="text" placeholder="ejem. LADY2019" required>
                        </div>
                        <!--<div class="form-group  col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Descripccion:</label>
                            <textarea id="modal-pais-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                        </div>-->
                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <button  type="button" id="modal-pais-btn-guardar" class="btn btn-primary">
                            Guardar
                        </button>
                        <button  type="button" id="modal-pais-btn-limpiar" class="btn btn-default">
                            Limpiar
                        </button>
                        <button  type="button" id="modal-pais-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>

            </div>
        </div>
    </div>
</div>


