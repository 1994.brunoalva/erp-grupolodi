<div class="modal fade" id="modal_linea" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nuevo linea</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                <div class="container-fluid">
                    <div class="form-group col-xs-12 col-sm-5 col-md-5">
                        <label label class="col-xs-12 no-padding">BUSCAR POR N° RUC :</label>
                        <div class="input-group  col-xs-12 no-padding ">
                            <input id="modal-linea-input-ruc" name="folder_input_ruc_emp" maxlength="11"
                                   placeholder="Ingrese numero de Ruc" class='form-control'/>
                            <span class="input-group-btn">
                                    <a class="btn btn-primary" id="modal-linea-btn-buscar"><i
                                            class="fa fa-search"></i></a>
                            </span>
                        </div>

                    </div>
                    <div class="form-group col-xs-12 col-sm-7 col-md-7">
                        <label class="col-xs-12 no-padding">Codigo Sunat:</label>
                        <input id="modal-linea-input-codigo" class="form-control" type="text" placeholder="ejem. 0071"
                               required>
                    </div>
                    <div class="form-group col-xs-12">
                        <label class="col-xs-12 no-padding">Razon Social:</label>
                        <input id="modal-linea-input-nombre" class="form-control" type="text"
                               placeholder="ejem. COSCO" required>
                    </div>
                </div>
                <div class="container-fluid">
                    <hr class="line-frame-modal">
                </div>
                <div class="container-fluid text-right">
                    <button type="submit" id="modal-linea-btn-guardar" class="btn btn-primary">
                        Guardar
                    </button>
                    <button type="button" id="modal-linea-btn-limpiar" class="btn btn-default">
                        Limpiar
                    </button>
                    <button type="button" id="modal-linea-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script type="module" src="../aConfig/Myjs/modal-proveedor.js"></script>-->

