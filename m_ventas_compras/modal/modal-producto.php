<style>
    @media (min-width: 800px) {
        #modal_producto .modal-dialog {
            width: 80% !important;
        }
    }
    .me-select .bootstrap-select button{
        opacity: 100 !important;
    }
</style>

<div class="modal fade" id="modal_producto" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1600;">
    <div class="modal-dialog modal-lg" role="document" ><!--style="width: 90%; margin: auto;"-->
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Nuevo producto</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                <div class="container-fluid">
                    <div class="row no-padding">
                        <div class="form-group col-xs-12 col-sm-4">
                            <label>CATEGORIA:</label>
                            <div class="input-group col-xs-12 no-padding">
                                <select id="select_modal_pro_categoria" required
                                        class="selectpicker form-control show-tick no-padding n1"
                                        data-live-search="true"
                                        data-size="5">
                                    <option value="0" selected>-Seleccione-</option>
                                    <?php
                                    $categoria = new Categoria('SELECT');
                                    $datos = $categoria->selectAll();
                                    foreach ($datos as $row) {
                                        if ($row->cat_id!=6){
                                            echo '<option value="' . $row->cat_id . '">' . $row->cat_nombre . '</option>';
                                        }

                                    }
                                    ?>
                                </select>
                                <!--<span class="input-group-btn">
                                    <button  type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#modal_marca">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </span>-->
                            </div>
                        </div>
                        <!--<div class="form-group col-xs-12 col-sm-4 col-md-4">
                            <label>MARCA:</label>
                            <div class="input-group col-xs-12 no-padding">
                                <select disabled id="select_modal_prod_marca" required
                                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                                        data-size="5">
                                    <option value="0" selected>-Seleccione-</option>
                                </select>
                                <span class="input-group-btn">
                                        <button id="producto_modal_btn_select_marca" disabled type="button"
                                                class="btn btn-primary" data-toggle="modal"
                                                data-target="#modal_marca">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 col-sm-4 col-md-4">
                            <label>MODELO:</label>
                            <div class="input-group col-xs-12 no-padding">
                                <select id="select_modal_prod_modelo" required
                                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                                        data-size="5">
                                    <option value="0" selected>-Seleccione-</option>
                                </select>
                                <span class="input-group-btn">
                                    <button id="producto_modal_btn_select_modelo" type="button" class="btn btn-primary"
                                            data-toggle="modal"
                                            data-target="#modal_modelo">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </span>
                            </div>
                        </div>-->
                        <div id="box-productos-inputs">
                            <div class="form-group col-xs-12 col-sm-4 box-mutel" >
                                <label>MARCA:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <select  disabled id="select_modal_prod_marca" required
                                            class="selectpicker form-control show-tick no-padding n2"
                                            data-live-search="true"
                                            data-size="5">
                                        <option value="0" selected>-Seleccione-</option>
                                    </select>
                                    <span class="input-group-btn">
                                        <button id="producto_modal_btn_select_marca" disabled type="button"
                                                class="btn btn-primary" data-toggle="modal"
                                                data-target="#modal_marca">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>MODELO:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <select id="select_modal_prod_modelo"
                                            class="selectpicker form-control show-tick no-padding n3"
                                            data-live-search="true"
                                            data-size="5">
                                        <option value="0" selected>-Seleccione-</option>
                                    </select>
                                    <span class="input-group-btn">
                                    <button id="producto_modal_btn_select_modelo" type="button" class="btn btn-primary"
                                            data-toggle="modal"
                                            data-target="#modal_modelo">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </span>
                                </div>
                                <!--<div class="input-group col-xs-12 no-padding">
                                    <select disabled id="select_modal_prod_modelo" required
                                            class="selectpicker form-control show-tick no-padding" data-live-search="true"
                                            data-size="5">
                                        <option value="0" selected>-Seleccione-</option>
                                    </select>
                                    <span class="input-group-btn">
                                        <button id="producto_modal_btn_select_modelo" disabled type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#modal_marca">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </span>
                                </div>-->
                            </div>
                            <div hidden id="item-produ-detalle" class="no-padding">
                                <div class="form-group col-xs-12 col-sm-4  box-mutel">
                                    <label class="col-xs-12 no-padding">COD. SUNAT:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <div class="input-group col-xs-12 no-padding">
                                            <input id="modal-producto-input-codigo-sunat" class="form-control"
                                                   type="text" autocomplete="off"
                                                   required
                                                   placeholder="Click para agregar" title="">
                                            <input id="modal-producto-input-codigo-sunat-id"
                                                   class="form-control no-display"
                                                   type="text">
                                            <!--<span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#modal_linea">
                                                    <i class="fa fa-plus"></i></button>
                                            </span>-->
                                        </div>
                                        <!--<select id="select_codigo" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                                        /*                    $codSunat = new CodSunat('SELECT');
                                                            $datos = $codSunat->selectAll();
                                                            foreach ($datos as $row) {
                                                                echo '<option value="' . $row->sunat_cod_id . '">' . $row->sunat_cod_codigo . '</option>';
                                                            }
                                                            */ ?>
                </select>-->
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4 box-mutel">
                                    <label>SKU:</label>
                                    <div class="input-group col-xs-12 no-padding me-select">
                                        <div class="input-group col-xs-12 no-padding">
                                            <input id="modal-producto-input-sku" disabled class="form-control" type="text" required
                                                   placeholder="Codigo de sku">
                                        </div>
                                        <span class="input-group-btn">
                                            <button id="modal-producto-btn-view-sku" type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#modal_sku">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group  col-xs-12 col-sm-4 box-varios">
                                    <label>U. DE MEDIDA :</label>
                                    <div class="input-group col-xs-12 no-padding me-select">
                                        <select id="select_modal_medida" required
                                                class="form-control no-padding"
                                                data-size="5">
                                            <option value="0" selected>-Seleccione-</option>
                                            <?php
                                            $unidad = new Unidad('SELECT');
                                            $datos = $unidad->selectAll();
                                            foreach ($datos as $row) {
                                                echo '<option value="' . $row->unidad_id . '">' . $row->unidad_nombre . '</option>';
                                            }
                                            ?>
                                        </select>
                                        <!--<span class="input-group-btn">
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#modal_categoria">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </span>-->
                                    </div>
                                </div>

                                <div class="form-group  col-xs-12 col-sm-4 inpt-descripcion">
                                    <label>DESCRIPCCION :</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-desc" autocomplete="off" class="form-control" type="text"
                                               placeholder="Descripcion">
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4 box-varios">
                                    <label>Nombre producto:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input autocomplete="off" disabled id="modal-producto-input-nombre" class="form-control" type="text"
                                               required
                                               placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4 box-accesorios">
                                    <label>P/N:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-pn" autocomplete="off" class="form-control" type="text" required
                                               placeholder="Numero de Parte">
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4 box-opc">
                                    <label class="col-xs-12 no-padding">TIPO:</label><!--OPC-->
                                    <div class="input-group col-xs-12 no-padding">
                                        <select id="select_opc" required
                                                class="selectpicker form-control show-tick no-padding"
                                                data-live-search="true"
                                                data-size="5">
                                            <option value="0" selected>-Seleccione-</option>
                                            <?php
                                            $tipoProducto = new TipoProducto('SELECT');
                                            $datos = $tipoProducto->selectAll();
                                            foreach ($datos as $row) {
                                                echo '<option value="' . $row->tipro_id . '">' . $row->tipro_nombre . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div id="box-nomenclatura" class="form-group col-xs-12 col-sm-4">
                                    <label>NOMENGLATURA:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <select id="select_nomenglatura" required
                                                class="selectpicker form-control show-tick no-padding"
                                                data-live-search="true"
                                                data-size="5">
                                            <option value="0" selected>-Seleccione-</option>
                                            <?php
                                            $nomenglatura = new Nomenglatura('SELECT');
                                            $datos = $nomenglatura->selectAll();
                                            foreach ($datos as $row) {
                                                if($row->nom_id!=6){
                                                    echo '<option value="' . $row->nom_id . '">' . $row->nom_nombre . '</option>';
                                                }

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4 box-mutel">
                                    <label>PAIS:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <select id="select_prod_pais" required
                                                class="selectpicker form-control show-tick no-padding"
                                                data-live-search="true"
                                                data-size="5">
                                            <option value="0" selected>-Seleccione-</option>
                                            <?php
                                            $pais = new Pais('SELECT');
                                            $datos = $pais->selectAll();
                                            foreach ($datos as $row) {
                                                echo '<option value="' . $row->pais_id . '">' . $row->pais_nombre . '</option>';
                                            }
                                            ?>
                                            ?>
                                        </select>
                                        <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#modal_pais">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </span>
                                    </div>
                                </div>

                                <!--CAMARA-->
                                <div hidden class="form-group  col-xs-12 col-sm-4 CAMARA">
                                    <label>MEDIDA:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-cam-medida" autocomplete="off" class="form-control" type="text" required
                                               placeholder="Medida de camara">
                                    </div>
                                </div>
                                <div hidden class="form-group  col-xs-12 col-sm-4 CAMARA">
                                    <label>ARO:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-cam-aro" autocomplete="off" class="form-control" type="text" required
                                               placeholder="Aro de camara">
                                    </div>
                                </div>
                                <div hidden class="form-group  col-xs-12 col-sm-4 CAMARA">
                                    <label> VALVULA:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-cam-valvula" autocomplete="off" class="form-control" type="text" required
                                               placeholder="Tipo de valvula">
                                    </div>
                                </div>
                                <!--CAMARA-->



                                <!--AROS-->
                                <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                    <label>MODELO:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-aro-modelo" autocomplete="off" class="form-control" type="text" required
                                               placeholder="Modelo aro">
                                    </div>
                                </div>

                                <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                    <label>MEDIDA:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-aro-medida" autocomplete="off" class="form-control" type="text" required
                                               placeholder="medida aro">
                                    </div>
                                </div>
                                <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                    <label>ESPESOR MM:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-aro-espesor" autocomplete="off" class="form-control" type="text" required
                                               placeholder="Espesor aro">
                                    </div>
                                </div>

                                <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                    <label># HUECOS:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-aro-num-huecos" autocomplete="off" class="form-control" type="text" required
                                               placeholder="# de huecos">
                                    </div>
                                </div>
                                <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                    <label>ESPESOR HUECO:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-aro-espesor-hueco" autocomplete="off" class="form-control" type="text" required
                                               placeholder="Espesor de hueco">
                                    </div>
                                </div>
                                <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                    <label>C.B.D:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-aro-cbd" autocomplete="off" class="form-control" type="text" required
                                               placeholder="C.B.D aro">
                                    </div>
                                </div>
                                <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                    <label>P.C.D:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-aro-pcd" autocomplete="off" class="form-control" type="text" required
                                               placeholder="P.C.D aro">
                                    </div>
                                </div>
                                <div hidden class="form-group  col-xs-12 col-sm-4 ARO">
                                    <label>OFF SET:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-aro-offset" autocomplete="off" class="form-control" type="text" required
                                               placeholder="Off Set aro">
                                    </div>
                                </div>

                                <!--AROS-->

                                <!--NEUMATICO-->
                                <div hidden class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                    <label>ANCHO:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-neu-ancho" autocomplete="off" class="form-control" type="text" required
                                               placeholder="Ancho neumatico">
                                    </div>
                                </div>
                                <div hidden class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                    <label>SERIE:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="modal-producto-input-neu-serie" autocomplete="off" class="form-control" type="text" required
                                               placeholder="Serie Neumatico">
                                    </div>
                                </div>
                            </div>

                            <!--NEUMATICO-->
                            <!--<div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>ANCHO:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-ancho" class="form-control" type="text" required
                                           placeholder="Ancho neumatico">
                                </div>
                            </div>-->
                            <!--<div hidden class="form-group col-xs-12 col-sm-2 NEUMATICO">
                                <label>SERIE:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input class="form-control" type="text" required
                                           placeholder="Serie Neumatico">
                                </div>
                            </div>-->
                            <div hidden class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                <label>ARO:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-aro" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Aro de Neumatico">
                                </div>
                            </div>
                            <div hidden class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                <label>PLIEGUES:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-pliege" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Numero de pliegues">
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4">
                                <label>SET:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-set" autocomplete="off" class="form-control" type="text" required
                                           placeholder="numero de Set">
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>USO:</label>
                                <div id="select_uso" class="input-group col-xs-12 no-padding">
                                    <select id="select_modal_uso" class="selectpicker form-control show-tick no-padding"
                                            data-live-search="true"
                                            data-size="5">
                                        <option value="0" selected="">-Seleccione-</option>
                                        <option value="1">PPE-Pasajeros, uso permanente</option>
                                        <option value="2">PTE-Pasajeros, uso temporal</option>
                                        <option value="3">CLT-Comerciales, para camioneta de carga, microbuses o camiones ligeros</option>
                                        <option value="4">CTR-Comerciales, para camión y/o ómnibus</option>
                                        <option value="5">CML-Comerciales, para uso minero y forestal</option>
                                        <option value="6">CMH-Comerciales, para casas rodantes</option>
                                        <option value="7">CST-Comerciales, remolcadores en carretera</option>
                                        <option value="8">ALN-Agrícolas</option>
                                        <option value="9">OTG-Maquinaria, tractores niveladores</option>
                                    </select>
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>MATERIAL:</label>
                                <div class="input-group col-xs-12 no-padding">

                                    <select id="modal-producto-input-neu-mate" class="selectpicker form-control show-tick no-padding"
                                            data-live-search="true"
                                            data-size="5">
                                        <option value="0" selected="">-Seleccione-</option>
                                        <option value="1">NYLON</option>
                                        <option value="2">ACERO</option>
                                        <option value="3">POLIÉSTER</option>
                                        <option value="4">RAYON</option>
                                        <option value="5">POLIAMIDA</option>
                                    </select>
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>ANCHO ADUANA:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-ancho-adua" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Ancho aduana">
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>SERIE ADUANA:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-serie-adua" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Serie aduana">
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>TIPO CONSTRUCCION:</label>
                                <div class="input-group col-xs-12 no-padding">

                                    <select id="modal-producto-input-neu-serie-tipo-cons" class="selectpicker form-control show-tick no-padding"
                                            data-live-search="true"
                                            data-size="5">
                                        <option value="0" selected="">-Seleccione-</option>
                                        <option value="1">CTT-CONVENCIONAL CON CAMARA</option>
                                        <option value="2">CTL-CONVENCIONAL SIN CAMARA</option>
                                        <option value="3">RTT-RADIAL CON CAMARA</option>
                                        <option value="4">RTL-RADIAL SIN CAMARA</option>
                                    </select>
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>INDICE DE CARGA:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-indice-carga" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Serie aduana">
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4">
                                <label>CARGA:</label>
                                <div class="input-group col-xs-12 no-padding ">
                                    <input id="modal-producto-input-neu-carga" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Numero de Parte">
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4">
                                <label>PISA:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-pisa" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Numero de Parte">
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4">
                                <label>EXTERNO:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-externo" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Numero de Parte">
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4">
                                <label>INDICE DE CARGA:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-externo" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Indice de carga">
                                </div>
                            </div>

                            <div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>VELOCIDAD:</label>
                                <div class="input-group col-xs-12 no-padding">

                                    <select id="modal-producto-input-neu-veloci" class="selectpicker form-control show-tick no-padding"
                                            data-live-search="true"
                                            data-size="5">
                                        <option value="0" selected="">-Seleccione-</option>
                                        <option value="1">E-70KM/H</option>
                                        <option value="2">F-80KM/H</option>
                                        <option value="3">G-90KM/H</option>
                                        <option value="4">J-100KM/H</option>
                                        <option value="5">K-110KM/H</option>
                                        <option value="6">L-120KM/H</option>
                                        <option value="7">M-130KM/H</option>
                                        <option value="8">N-140KM/H</option>
                                        <option value="9">P-150KM/H</option>
                                        <option value="10">Q-160KM/H</option>
                                        <option value="11">R-170KM/H</option>
                                        <option value="12">S-180KM/H</option>
                                        <option value="13">T-190KM/H</option>
                                        <option value="14">U-200KM/H</option>
                                        <option value="15">H-210KM/H</option>
                                        <option value="16">V-240KM/H</option>
                                        <option value="17">W-270KM/H</option>
                                        <option value="18">Y-300KM/H</option>
                                        <option value="19">Z-MAYOR A 300KM/H</option>
                                    </select>
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>CONSTANCIA:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-consta" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Constancia de cumplimiento">
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>ITEM:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-item" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Item de la constancia">
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>VIGENCIA:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-vigencia" autocomplete="off" class="form-control" type="date" required
                                           >
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4 NEUMATICO">
                                <label>CONFORMIDAD:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-confor" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Declaracion de conformidad">
                                </div>
                            </div>
                            <div hidden class="form-group col-xs-12 col-sm-4 box-siempre-show">
                                <label>PARTIDA ARANCELARIA:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-parti" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Partida Arancelaria">
                                </div>
                            </div>
                            <!--NEUMATICO-->

                            <div hidden class="form-group col-xs-12 col-sm-4">
                                <label>PRO MEDIDA:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-produ-medida" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Numero de Parte">
                                </div>
                            </div>
                            <div hidden class="form-group  col-xs-12 col-sm-4 NEUMATICO">
                                <label>SUCE:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <input id="modal-producto-input-neu-suce" autocomplete="off" class="form-control" type="text" required
                                           placeholder="Ancho neumatico">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <hr class="line-frame-modal">
                </div>
                <div class="container-fluid text-right">

                    <button type="submit" id="modal-producto-btn-guardar" class="btn btn-primary">
                        Guardar
                    </button>
                    <button type="button" id="modal-producto-btn-limpiar" class="btn btn-default">
                        Limpiar
                    </button>
                    <button type="button" id="modal-producto-btn-cerrar" class="btn btn-success">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#select_nomenglatura').change(function () {
        genNombreProducto();
    });
    $('#select_modal_prod_modelo').change(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-neu-pliege").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-neu-serie").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-pn").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-cam-medida").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-cam-aro").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-cam-valvula").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-aro-medida").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-aro-espesor").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-aro-num-huecos").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-aro-espesor-hueco").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-aro-cbd").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-aro-pcd").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-aro-offset").keyup(function () {
        genNombreProducto();
    });
    $("#modal-producto-input-desc").keyup(function () {
        genNombreProducto();
    });
    function genNombreProducto() {
        const cate =$("#select_modal_pro_categoria option:selected").text().trim();
        var  nombrePr=cate;
        $("#modal-producto-input-nombre").prop( "disabled", true );
        //$("#modal-producto-input-nombre").removeAttr('disabled');
        if (cate=="NEUMATICOS"){
            //

            const  nomen = $("#select_nomenglatura option:selected").text();
            const  tipo = $("#select_opc option:selected").text();
            const ancho =  $("#modal-producto-input-neu-ancho").val();
            nombrePr+=" "+ancho;
            if (nomen=="MILIMETRICA"){
                const serie= $("#modal-producto-input-neu-serie").val();
                nombrePr+="/"+serie;
            }
            if (tipo=="RADIAL"){
                nombrePr+="R";
            }
            const aroNeu=$("#modal-producto-input-neu-aro").val();
            const plieges = $("#modal-producto-input-neu-pliege").val();
            var marca =$("#select_modal_prod_marca option:selected").text();
            var modelo =$("#select_modal_prod_modelo option:selected").text();
            var pais =$("#select_prod_pais option:selected").text();
            marca=(marca=="-Seleccione-")?"":marca;
            modelo=(modelo=="-Seleccione-")?"":modelo;
            pais=(pais=="-Seleccione-")?"":pais;
            nombrePr+="-"+aroNeu+" "+plieges+"PR " +marca +" "+modelo+" "+pais;


        }else if(cate=="ACCESORIOS") {
            nombrePr += " "+$("#modal-producto-input-desc").val().trim();
            nombrePr += " "+$("#modal-producto-input-pn").val().trim();
            var marca =$("#select_modal_prod_marca option:selected").text();
            var pais =$("#select_prod_pais option:selected").text();
            marca=(marca=="-Seleccione-")?"":marca;
            pais=(pais=="-Seleccione-")?"":pais;

            nombrePr += " "+marca;
            nombrePr += " "+pais;
        }else if(cate=="AROS") {
            nombrePr += " "+$("#modal-producto-input-aro-medida").val().trim();
            nombrePr += " "+$("#modal-producto-input-aro-espesor").val().trim();
            nombrePr += " ("+$("#modal-producto-input-aro-num-huecos").val().trim();
            nombrePr += ","+$("#modal-producto-input-aro-espesor-hueco").val().trim();
            nombrePr += ","+$("#modal-producto-input-aro-cbd").val().trim();
            nombrePr += ","+$("#modal-producto-input-aro-pcd").val().trim();
            nombrePr += ","+$("#modal-producto-input-aro-offset").val().trim()+")";

            var marca =$("#select_modal_prod_marca option:selected").text();
            var pais =$("#select_prod_pais option:selected").text();
            marca=(marca=="-Seleccione-")?"":marca;
            pais=(pais=="-Seleccione-")?"":pais;

            nombrePr += " "+marca;
            nombrePr += " "+pais;
        }
        else if(cate=="CAMARAS") {
            const  nomen = $("#select_nomenglatura option:selected").text();
            const  tipo = $("#select_opc option:selected").text();
            nombrePr += " "+$("#modal-producto-input-cam-medida").val().trim();
            if (tipo=="RADIAL"){
                nombrePr+="R";
            }
            if (tipo=="CONVENCIONAL"){
                nombrePr+="-";
            }
            nombrePr += " "+$("#modal-producto-input-cam-aro").val().trim();
            nombrePr += " "+$("#modal-producto-input-cam-valvula").val().trim();
            var marca =$("#select_modal_prod_marca option:selected").text();
            var pais =$("#select_prod_pais option:selected").text();
            marca=(marca=="-Seleccione-")?"":marca;
            pais=(pais=="-Seleccione-")?"":pais;

            nombrePr += " "+marca;
            nombrePr += " "+pais;

        }else if(cate=="PROTECTORES") {
            nombrePr += " "+$("#modal-producto-input-desc").val().trim();
            var marca =$("#select_modal_prod_marca option:selected").text();
            var pais =$("#select_prod_pais option:selected").text();
            marca=(marca=="-Seleccione-")?"":marca;
            pais=(pais=="-Seleccione-")?"":pais;

            nombrePr += " "+marca;
            nombrePr += " "+pais;
        }else if(cate==catTe) {
            nombrePr="";
            $("#modal-producto-input-nombre").removeAttr('disabled');
        }
        console.log(nombrePr);
        $("#modal-producto-input-nombre").val(nombrePr);


    }
</script>
<!--<script type="module" src="../aConfig/Myjs/modal-producto.js"></script>-->

