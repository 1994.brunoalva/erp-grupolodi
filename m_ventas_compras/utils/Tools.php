<?php

class Tools{

    public function __construct()
    {
    }

    public function numeroParaDocumento($numero, $cnt0){
        $num ="".$numero;
        for ($i = strlen($numero); $i<=$cnt0;$i++){
            $num= "0".$num;
        }
        return $num;
    }


    function getToken($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $token;
    }
    function abreviaturaMes($mes){
        $meses = array("en","febr","mzo","abr","my","jun","jul","agto","sept","oct","nov","dic");

        return $meses[$mes];

    }
    function nombreMes($mes){
        $meses = array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");

        return $meses[$mes];

    }

}