<?php


class SendCurlVenta
{
    private $dataSend=[];
    private $urlCurl;
    private $tipo;

    /**
     * SendCurlVenta constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return array
     */
    public function getDataSend()
    {
        return $this->dataSend;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @param array $dataSend
     */
    public function setDataSend($dataSend)
    {
        $this->dataSend = $dataSend;
    }

    /**
     * @return mixed
     */
    public function getUrlCurl()
    {
        return $this->urlCurl;
    }

    /**
     * @param mixed $urlCurl
     */
    public function setUrlCurl($urlCurl)
    {
        $this->urlCurl = $urlCurl;
    }

    function enviar() {

        $archivo = "factura";
        if ($this->tipo == 3) {
            $archivo = "factura";
        }
        if ($this->tipo == 2) {
            $archivo = "boleta";
        }
        $ruta = $this->urlCurl . $archivo . ".php";
        //echo $ruta;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ruta);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->dataSend);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec($ch);
        curl_close($ch);

        return $respuesta;
    }

}