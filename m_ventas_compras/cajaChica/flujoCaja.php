<?php
$indexRuta=1;
require "../entidadDB/DataBase.php";
require '../conexion/Conexion.php';
require '../model/Pais.php';
require "../model/CajaChica.php";
require "../model/FlujoCajaChica.php";


$conexion = (new Conexion())->getConexion();

$cajaChica =  new CajaChica();
$flujoCajaChica= new FlujoCajaChica();
$flujoCajaChica->setIdCaja($_GET['caja']);



$resul_lista = $flujoCajaChica->lista();

$resul_conceptos = $conexion->query("SELECT * FROM sys_caja_conceptos");


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <script src="../aConfig/plugins/sweetalert2/sweetalert2.min.css"></script>

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
        .box-shadow{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .box-shadow:hover{
            box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .fade-enter-active, .fade-leave-active {
            transition: opacity .2s;
        }
        .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
            opacity: 0;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <input id="idCaja" type="hidden" value="<?php echo $_GET['caja']?>">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">flujo - Caja Chica</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <!--<div class="col-lg-6 text-right">
                                        <a href="new-folder.php" id="folder_btn_nuevo_folder" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Folder
                                        </a>

                                    </div>-->

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div class="col-md-12 text-right" style="margin-bottom: 20px">
                                    <button  class="btn btn-primary"  data-toggle="modal" data-target="#modal_caja_flujo"><i class="fa fa-plus"></i> Nuevo</button>

                                </div>

                                <table id="tabla-caja-flujo" class="table table-striped table-bordered">
                                    <thead>
                                    <tr  style="background-color: #007ac3; color: white">
                                        <th style="border-right-color: #007ac3" >HORA</th>
                                        <th style="border-right-color: #007ac3" >CONCEPTO</th>
                                        <th style="border-right-color: #007ac3" >DETALLE</th>
                                        <th style="border-right-color: #007ac3" >MONEDA</th>
                                        <th style="border-right-color: #007ac3" >INGRESO</th>
                                        <th style="border-right-color: #007ac3" >EGRESO</th>
                                        <th>VER</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $totalSolesEgre = 0;
                                    $totalSolesingr = 0;
                                    $totalDolEgre = 0;
                                    $totalDolingr = 0;

                                    foreach ($resul_lista as $item){

                                        if ($item['id_moneda'] == 1){
                                            $totalDolEgre += $item['egreso'];
                                            $totalDolingr += $item['ingreso'];
                                        }elseif ($item['id_moneda'] == 4){
                                            $totalSolesEgre +=$item['egreso'];
                                            $totalSolesingr += $item['ingreso'];
                                        }


                                        ?>
                                        <tr>
                                            <td><?php echo $item['hora'] ?></td>
                                            <td><?php echo $item['nombre_concepto'] ?></td>
                                            <td><?php echo $item['detalle'] ?></td>
                                            <td><?php echo $item['mone_nombre'] ?></td>
                                            <td style="color: #2a5eed;" ><?php echo $item['ingreso']>0?$item['ingreso']:'' ?></td>
                                            <td style="color: #da384c;"><?php echo $item['egreso']>0?$item['egreso']:''  ?></td>
                                            <td><button  data-toggle="modal" data-target="#modal_caja_flujo" class="btn btn-info"><i class="fa fa-eye"></i></button></td>
                                        </tr>
                                    <?php     }

                                   // echo $totalSolesEgre."<br>".$totalSolesingr."<br>".$totalDolEgre."<br>".$totalDolingr;


                                    ?>

                                    </tbody>
                                </table>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <table  class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                            <tr  style="background-color: #007ac3; color: white">
                                                <th  style="border-right-color: #007ac3">TOTAL INGRESOS</th>
                                                <th  style="border-right-color: #007ac3">TOTAL EGRESOS</th>
                                                <th  style="border-right-color: #007ac3">SALDO</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="text-center"><span style=" text-align: center ;color: #2a5eed; font-size: 20px; font-weight: bold">S/. <?php echo $totalSolesingr ?></span></td>
                                                <td class="text-center"><span style=" text-align: center ;color: #da384c; font-size: 20px; font-weight: bold">S/. <?php echo $totalSolesEgre ?></span></td>
                                                <td  style=" text-align: center ;font-size: 20px; ">S/. <?php echo $totalSolesingr - $totalSolesEgre ?></td>
                                            </tr>
                                            <tr>
                                                <td  class="text-center" ><span style=" text-align: center ;color: #2a5eed; font-size: 20px; font-weight: bold">$ <?php echo $totalDolingr ?></span></td>
                                                <td class="text-center"><span style=" text-align: center ;color: #da384c; font-size: 20px; font-weight: bold">$ <?php echo $totalDolEgre ?></span></td>
                                                <td style=" text-align: center ;font-size: 20px; ">$ <?php echo $totalDolingr-$totalDolEgre ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>


    <div class="modal fade" id="modal_caja_flujo" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Egreso</h3>
                    </div>
                </div>

                <div class="modal-body  no-border">
                    <form action="#" >
                        <div class="container-fluid">
                            <div class="row no-padding">
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>CONCEPTO:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <select id="consepto-id"
                                            class="form-control show-tick no-padding"
                                            data-live-search="true"
                                            data-size="5">
                                            <?php
                                            foreach ($resul_conceptos as $rowco){
                                                echo " <option value='{$rowco['id']}'>{$rowco['nombre_concepto']}</option>";
                                            }
                                            ?>

                                        </select>

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>MONEDA:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <select id="moneda-id"
                                            class="form-control show-tick no-padding"
                                            data-live-search="true"
                                            data-size="5">
                                            <option value="1">DOLAR</option>
                                            <option value="4">SOLES</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>HORA:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="hora-input" disabled class="form-control" value="16:50">

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>TIPO DOCUMENTO:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <select class="form-control">
                                            <option selected>FACTURA</option>
                                            <option >BOLETA</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>NUMERO:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input  class="form-control" value="">

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4">
                                    <label>MONTO:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="monto-egreso" class="form-control" value="">

                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label>DETALLE:</label>
                                    <div class="input-group col-xs-12 no-padding">
                                        <input id="detalle-egreso" class="form-control"  value="">

                                    </div>
                                </div>






                            </div>

                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">

                            <button onclick="registrar()"  type="button"  class="btn btn-primary">
                                Guardar
                            </button>
                            <button type="button"  class="btn btn-success"
                                    data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>



    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>

        function registrar() {
            var idCaja = $("#idCaja").val();
            var hora = $("#hora-input").val();
            var idconcepto = $("#consepto-id").val();
            var idmoneda = $("#moneda-id").val();
            var detalle = $("#detalle-egreso").val();
            var egreso = $("#monto-egreso").val();
            $.ajax({
                type: "POST",
                url: "../ajax/CajaChicaFlujo/newCajaFlujo.php",
                data: {
                    idCaja, hora,idconcepto,idmoneda,detalle,egreso
                },
                success: function (data) {
                    console.log(data)
                    location.reload();
                }
            });
        }
        const APP =  new Vue({
            el:"#contenedorprincipal",
            data:{

            },
            methods:{
            }
        });


        $( document ).ready(function() {
            $('#tabla-caja-flujo').DataTable({

            });
            $("#tabla-Caja").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'csvHtml5',
                ]
            });
        });
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }

        function alerInfo(msg) {
            $.toast({
                heading: 'INFORMACION',
                text: msg,
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
        }
    </script>



</body>


</html>
