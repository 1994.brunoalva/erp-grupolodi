<?php
$indexRuta=1;
require '../conexion/Conexion.php';
require '../model/TipoPago.php';
require_once '../model/model.php';
$tipoPago = new TipoPago();
$categorias= new Categoria('SELECT');

$listaPa= $tipoPago->lista();
$listaTemTP = [];

foreach ($listaPa as $item){
    $listaTemTP []= $item;
}


$conexionp =  (new Conexion())->getConexion();

$sql = "SELECT prod.*, cate.cat_nombre, mar.mar_nombre, model.mod_nombre, pais.pais_nombre
FROM sys_producto AS prod
INNER JOIN sys_com_categoria AS cate ON prod.cat_id = cate.cat_id
INNER JOIN sys_com_marca AS mar ON prod.mar_id = mar.mar_id 
INNER JOIN sys_com_modelo AS model ON prod.mod_id = model.mod_id
INNER JOIN sys_pais AS pais  ON prod.pais_id = pais.pais_id";
//$result_productos = $conexionp->query($sql);


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <script src="../aConfig/plugins/sweetalert2/sweetalert2.min.css"></script>

    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 200px;
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
        .box-shadow{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .box-shadow:hover{
            box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .fade-enter-active, .fade-leave-active {
            transition: opacity .2s;
        }
        .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
            opacity: 0;
        }
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="../"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body"> Países</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <button  type="button"  data-toggle="modal" data-target="#modal_pais" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Pais
                                        </button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div   class="col-md-12" >



                                    <div v-for="(item , index) in listaPaices" v-if="item.estado!=0" class="col-md-2">
                                        <div  class="panel box-shadow" style="height: 140px">
                                            <div class="panel-body text-center">
                                                <div style="height: 70px; overflow: hidden">
                                                    <p style="font-size: 20px" class="text-semibold mar-no text-main">{{item.pais_nombre}}</p>

                                                </div>


                                                <div class="mar-top">
                                                    <button v-on:click="cargarDataPaisAll(item.pais_id,item.pais_nombre)" data-toggle="modal" data-target="#modal_edt_pais" class="btn btn-info">VER</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>



                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>



    <div class="modal fade" id="modal_pais" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800; display: none;">
        <div class="modal-dialog modal-xs " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Agregar Nuevo Pais</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form action="#">
                        <div class="container-fluid">
                            <div class="form-group col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Nombre:</label>
                                <input id="modal-pais-input-nombre" class="form-control" type="text" placeholder="" required="">
                            </div>
                            <!--<div class="form-group  col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Descripccion:</label>
                                <textarea id="modal-pais-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                            </div>-->
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">

                            <button type="button" onclick=" addPais()" class="btn btn-primary">
                                Guardar
                            </button>

                            <button type="button" class="btn btn-success" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="modal_edt_paises">
        <div class="modal fade" id="modal_edt_pais" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800; display: none;">
            <div class="modal-dialog modal-xs " role="document">
                <div class="modal-content">
                    <div class="modal-header no-border no-padding">
                        <div class="modal-header text-center color-modal-header">
                            <h3 class="modal-title">Pais</h3>
                        </div>
                    </div>
                    <div class="modal-body  no-border">
                        <form action="#">
                            <div class="container-fluid">
                                <div class="form-group col-xs-12 no-padding">
                                    <label class="col-xs-12 no-padding">Nombre:</label>
                                    <input id="modal-pais-input-nombre-edt" v-model="nombre" class="form-control" type="text" placeholder="" required="">
                                </div>
                                <div class="form-group  col-xs-12 no-padding">
                                    <label class="col-xs-12 no-padding">Puertos: <span  data-toggle="modal" data-target="#modal_puerto" class="btn btn-primary"><i class="fa fa-plus"></i></span></label>
                                    <div class="col-xs-12 table-wrapper-scroll-y my-custom-scrollbar">

                                        <ul class="list-group">
                                            <li v-for="iten in lista" class="list-group-item">{{iten.puerto_nombre}}</li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="container-fluid">
                                <hr class="line-frame-modal">
                            </div>
                            <div class="container-fluid text-right">

                                <button v-on:click="editarpaisNon()" type="button" onclick="" class="btn btn-primary">
                                    Guardar
                                </button>

                                <button type="button" class="btn btn-success" data-dismiss="modal">
                                    Cerrar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal_puerto" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800; display: none;">
            <div class="modal-dialog modal-xs " role="document">
                <div class="modal-content">
                    <div class="modal-header no-border no-padding">
                        <div class="modal-header text-center color-modal-header">
                            <h3 class="modal-title">Agregar Nuevo Puerto</h3>
                        </div>
                    </div>
                    <div class="modal-body  no-border">
                        <form action="#">
                            <div class="container-fluid">
                                <div class="form-group col-xs-12 no-padding">
                                    <label class="col-xs-12 no-padding">Nombre:</label>
                                    <input id="modal-puerto-imput" class="form-control" type="text" placeholder="" required="">
                                </div>
                                <!--<div class="form-group  col-xs-12 no-padding">
                                    <label class="col-xs-12 no-padding">Descripccion:</label>
                                    <textarea id="modal-pais-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                                </div>-->
                            </div>
                            <div class="container-fluid">
                                <hr class="line-frame-modal">
                            </div>
                            <div class="container-fluid text-right">

                                <button type="button" v-on:click="agregarpuerto()" class="btn btn-primary">
                                    Guardar
                                </button>

                                <button type="button" class="btn btn-success" data-dismiss="modal">
                                    Cerrar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <script>

        function addPais(){
            const pais = $("#modal-pais-input-nombre").val();

            $.ajax({
                type: "POST",
                url: "../ajax/Pais/add_pais.php",
                data: {pais},
                success: function (data) {

                    console.log(data);
                    if (isJson(data)){
                        const jso = JSON.parse(data);
                        if (jso.res){
                            $("#select_prod_pais").append('<option value="' + jso.data.id + '">' + jso.data.nombre + '</option>');
                            $('#producto-select-presentacion2').selectpicker('refresh');
                            $("#modal-pais-input-nombre").val('');
                            $("#modal_pais").modal('hide');
                            cargarDataPais();
                            $.toast({
                                heading: 'EXITOSO',
                                text: "Se agrego",
                                icon: 'success',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        }else{
                            console.log(data);
                            $.toast({
                                heading: 'ERROR',
                                text: "Error No se pudo agregar",
                                icon: 'error',
                                position: 'top-center',
                                hideAfter: '2500',
                            });
                        }
                    }else{
                        $.toast({
                            heading: 'ERROR',
                            text: "Error",
                            icon: 'error',
                            position: 'top-center',
                            hideAfter: '2500',
                        });
                        console.log(data);
                    }
                }
            });

        }

        $(document).ready(function () {
            $('#table-codigo-sunat').DataTable({
                /*scrollY: false,*/
                /*scrollX: true,*/
                paging: false,
                /* lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],*/
                language: {
                    url: '../assets/Spanish.json'
                }
            });
        });
    </script>


    <style>
        #img-file-preview-zone{
            -webkit-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            -moz-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            border-radius: 3px;
        }
    </style>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <!--script  type="module" src="../aConfig/scripts/sku.js"></script-->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>

        const modaPais = new Vue({
            el:"#modal_edt_paises",
            data:{
                nombre:'',
                idpa:0,
                lista:[],
            },
            methods:{
                agregarpuerto(){
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Puerto/add_pauerto.php",
                        data:{idpai:modaPais._data.idpa,puerto:$("#modal-puerto-imput").val()},
                        success: function (data) {
                            console.log(data)
                            app.cargarDataPaisAll(modaPais._data.idpa,modaPais._data.nombre)
                            $('#modal_puerto').modal('hide');
                        }
                    });
                },
               editarpaisNon() {

                    $.ajax({
                        type: "POST",
                        url: "../ajax/Pais/edt_pais.php",
                        data: {id:modaPais._data.idpa, pais:modaPais._data.nombre},
                        success: function (data) {
                            console.log(data);
                            cargarDataPais();
                            $('#modal_edt_pais').modal('hide');
                        }
                    });

                }
            }
        });
        const app = new Vue({
            el:"#contenedorprincipal",
            data:{
                listaPaices:[]
            },
            methods:{
                cargarDataPaisAll(idpa,nombre) {
                    modaPais._data.nombre=nombre;
                    modaPais._data.idpa=idpa;
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Pais/get_data_all.php",
                        data:{idpai:idpa},
                        success: function (data) {
                            console.log(data)
                           // app._data.listaPaices = data
                            if (isJson(data)){
                                modaPais._data.lista=JSON.parse(data)
                            }
                        }
                    });

                },
            }
        });


        function cargarDataPais() {
            $.ajax({
                type: "POST",
                url: "../ajax/Pais/getAllPais.php",
                success: function (data) {
                    console.log(data)
                    app._data.listaPaices = data
                }
            });

        }


        $( document ).ready(function() {
            cargarDataPais()
        });
        
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>


</html>
