<?php
require "../entidadDB/DataBase.php";
require '../conexion/Conexion.php';
require '../model/Pais.php';

$conexion = (new Conexion())->getConexion();

$indexRuta=1;

$resul_proveedor = $conexion->query("SELECT sys_com_proveedor.*,sys_pais.pais_nombre FROM sys_com_proveedor INNER JOIN sys_pais ON sys_com_proveedor.pais_id=sys_pais.pais_id WHERE sys_pais.pais_nombre != 'PERU'");

$resul_tipoDoc = $conexion->query("SELECT * FROM sys_adm_documentos WHERE doc_tipo ='DE IDENTIDAD'");


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <script src="../aConfig/plugins/sweetalert2/sweetalert2.min.css"></script>

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
        .box-shadow{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .box-shadow:hover{
            box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .fade-enter-active, .fade-leave-active {
            transition: opacity .2s;
        }
        .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
            opacity: 0;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Proveedores</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right" style="">
                                        <button  class="btn btn-primary"  data-toggle="modal" data-target="#modal_proveedor"><i class="fa fa-plus"></i> Nuevo</button>

                                    </div>
                                    <!--BOTONES-->
                                    <!--<div class="col-lg-6 text-right">
                                        <a href="new-folder.php" id="folder_btn_nuevo_folder" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Folder
                                        </a>

                                    </div>-->

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">

                                <table id="tabla-proveedores" class="table table-striped table-bordered">
                                    <thead>
                                    <tr style="background-color: #007ac3; color: white">
                                        <th  class="text-center" style="border-right-color: #007ac3" >PROVEEDOR</th>
                                        <th class="text-center" style="border-right-color: #007ac3" >PAIS</th>
                                        <th class="text-center" style="border-right-color: #007ac3" >TELEFONO 1</th>
                                        <th class="text-center" style="border-right-color: #007ac3" >TELEFONO 2</th>
                                        <th class="text-center" style="border-right-color: #007ac3" >EMAIL</th>
                                        <th class="text-center">VER</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($resul_proveedor as $item){



                                        ?>
                                        <tr>
                                            <td class="text-center"><?php echo $item['provee_desc'] ?></td>
                                            <td class="text-center"><?php echo $item['pais_nombre'] ?></td>
                                            <td class="text-center"><?php echo $item['provee_telf'] ?></td>
                                            <td class="text-center"><?php echo $item['provee_telf2'] ?></td>
                                            <td class="text-center"><?php echo $item['provee_email'] ?></td>
                                            <td><button style="display: block;margin: auto" v-on:click="setDato(<?php echo $item['provee_id'] ?>)" class="btn btn-info"><i class="fa fa-eye"></i></button></td>
                                        </tr>
                                    <?php     }
                                    ?>
                                    </tbody>
                                </table>

                                <div class="modal fade" id="modal_proveedor" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document" >
                                        <div class="modal-content">
                                            <div class="modal-header no-border no-padding">
                                                <div class="modal-header text-center color-modal-header">
                                                    <h3 class="modal-title">Proveedor</h3>
                                                </div>
                                            </div>

                                            <div class="modal-body  no-border">
                                                <form action="#" >
                                                    <div class="container-fluid">
                                                        <div class="row no-padding">
                                                            <div class="form-group col-xs-12 col-sm-12 col-sm-6 ">
                                                                <label>PAIS:</label>
                                                                <div class="input-group col-xs-12 no-padding">
                                                                    <select id="selector_pais" v-model="dataRegistro.idpais"
                                                                        class="form-control show-tick no-padding selectpicker"
                                                                        data-live-search="true"
                                                                        data-size="5">

                                                                        <?php
                                                                        $pais = new Pais('SELECT');
                                                                        $datos = $pais->selectAll();
                                                                        foreach ($datos as $row) {
                                                                            if ($row->pais_nombre != 'PERU' && $row->estado !=0 ){
                                                                                echo '<option value="' . $row->pais_id . '">' . $row->pais_nombre . '</option>';
                                                                            }

                                                                        }
                                                                        ?>
                                                                        ?>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                            <div  class="form-group col-xs-12 col-sm-3 no-display">
                                                                <label>Tipo Doc.:</label>
                                                                <div class="input-group col-xs-12 no-padding">
                                                                    <select v-model="dataRegistro.tipoDoc"
                                                                        class="form-control show-tick no-padding"
                                                                        data-live-search="true"
                                                                        data-size="5">

                                                                        <?php

                                                                        foreach ($resul_tipoDoc as $row2) {
                                                                            echo '<option value="' . $row2['doc_id'] . '">' . $row2['doc_nombre'] . '</option>';
                                                                        }
                                                                        ?>
                                                                        ?>
                                                                    </select>

                                                                </div>
                                                            </div>

                                                            <div class="form-group col-xs-12 col-sm-4 no-display">
                                                                <label>Numero Documento:</label>
                                                                <div class="input-group col-xs-12 no-padding">
                                                                    <input :disabled="dataRegistro.idproveedor!=0"  v-model="dataRegistro.numeroDoc" class="form-control">
                                                                    <span  :disabled="dataRegistro.idproveedor!=0"  v-if="dataRegistro.tipoDoc==1||dataRegistro.tipoDoc==3" class="input-group-btn">
                                                                            <button v-on:click="consultaDocumentoNum()" type="button" class="btn btn-primary" >
                                                                                <i class="fa fa-search"></i>
                                                                            </button>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-xs-12 col-sm-12 col-sm-6">
                                                                <label>Nombre:</label>
                                                                <div class="input-group col-xs-12 no-padding">
                                                                    <input v-model="dataRegistro.nombre" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-xs-6 col-sm-6 col-md-3">
                                                                <label>Direccion:</label>
                                                                <div class="input-group col-xs-12 no-padding">
                                                                    <input v-model="dataRegistro.direccion"  class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-xs-6 col-sm-6 col-md-3">
                                                                <label>Telefono 1:</label>
                                                                <div class="input-group col-xs-12 no-padding">
                                                                    <input v-model="dataRegistro.telefono1"  class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                                <label>Telefono 2:</label>
                                                                <div class="input-group col-xs-12 no-padding">
                                                                    <input  v-model="dataRegistro.telefono2" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-xs-12 col-sm-5 col-md-3">
                                                                <label>Email:</label>
                                                                <div class="input-group col-xs-12 no-padding">
                                                                    <input  v-model="dataRegistro.email"  class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-xs-12 col-sm-7 col-md-9">
                                                                <label>Contacto:</label>
                                                                <div class="input-group col-xs-12 no-padding">
                                                                    <input  v-model="dataRegistro.contacto"   class="form-control">
                                                                </div>
                                                            </div>


                                                        </div>

                                                    </div>
                                                    <div class="container-fluid">
                                                        <hr class="line-frame-modal">
                                                    </div>
                                                    <div class="container-fluid text-right">

                                                        <!-- <a type="submit" id="modal-buscar-empresa-btn-guardar" class="btn btn-primary">
                                                             Guardar
                                                         </a>
                                                         <button type="button" id="modal-buscar-empresa-btn-limpiar" class="btn btn-default">
                                                             Limpiar
                                                         </button>-->
                                                        <button v-if="dataRegistro.idproveedor==0" type="button" v-on:click="guardarProveedor()"  class="btn btn-primary">
                                                            Guardar
                                                        </button>
                                                        <button v-if="dataRegistro.idproveedor!=0"  type="button" v-on:click="actualizarProveedor()"  class="btn btn-primary">
                                                            Guardar
                                                        </button>
                                                        <button type="button"  class="btn btn-success"
                                                                data-dismiss="modal">
                                                            Cerrar
                                                        </button>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>




    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>

        const APP =  new Vue({
            el:"#contenedorprincipal",
            data:{
                dataRegistro:{
                    idproveedor:0,
                    idpais:0,
                    numeroDoc:'',
                    tipoDoc:'',
                    nombre:'',
                    direccion:'',
                    telefono1:'',
                    telefono2:'',
                    email:'',
                    contacto:''
                }
            },
            methods:{
                setDato(idProvee){
                    $('#modal_proveedor').modal('toggle');
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Proveedores/getAllForId.php",
                        data: {id:idProvee},
                        success: function (data) {
                            console.log(data);
                            APP._data.dataRegistro={
                                idproveedor:data.provee_id,
                                idpais:data.pais_id.pais_id,
                                numeroDoc:data.provee_num_doc,
                                tipoDoc:data.tipodoc_id.doc_id,
                                nombre:data.provee_desc,
                                direccion:data.provee_direc,
                                telefono1:data.provee_telf,
                                telefono2:data.provee_telf2,
                                email:data.provee_email,
                                contacto:data.provee_contacto
                            };
                        }
                    });
                },
                limpiar(){
                   this.dataRegistro={
                        idproveedor:0,
                        idpais:0,
                        numeroDoc:'',
                        tipoDoc:'',
                        nombre:'',
                        direccion:'',
                        telefono1:'',
                        telefono2:'',
                        email:'',
                        contacto:''
                    };
                },
                actualizarProveedor(){
                    var dataSend = {... this.dataRegistro};
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Proveedores/udtProveedor.php",
                        data: dataSend,
                        success: function (dat) {
                            console.log(dat)
                            APP.limpiar();
                            $('#modal_proveedor').modal('toggle');
                            location.reload();
                        }
                    });
                },
                guardarProveedor(){
                    var dataSend = {... this.dataRegistro};
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Proveedores/newProveedor.php",
                        data: dataSend,
                        success: function (dat) {
                            console.log(dat)
                            APP.limpiar();
                            $('#modal_proveedor').modal('toggle');
                            location.reload();
                        }
                    });
                },
                consultaDocumentoNum(){

                    if (this.dataRegistro.tipoDoc==3){
                        $.ajax({
                            data: {ruc:APP._data.dataRegistro.numeroDoc},
                            url: "../ajax/libSunat/sunat/example/consultaRuc.php",
                            type: 'POST',
                            async: false,
                            beforeSend: function() {
                                alerInfo('Buscando RUC en Sunat, Porfavor espere ..');
                            },
                            success: function (response) {
                                console.log(response);
                                if (response.success) {

                                    var result = response.result;

                                    APP._data.dataRegistro.numeroDoc=result.ruc;
                                    APP._data.dataRegistro.nombre=result.razon_social;
                                    APP._data.dataRegistro.direccion=result.direccion;

                                } else {
                                    swall(response.message);
                                }
                            },
                            error: function (err) {
                                console.log(err);
                                swall('Error al en el servidor de SUNAT Reporte este error!!');
                            },
                        });
                    }
                    else  if (this.dataRegistro.tipoDoc==1){

                        $.ajax({
                            data: {dni:APP._data.dataRegistro.numeroDoc},
                            url: "../ajax/Dni/consultaDNI.php",
                            type: 'POST',
                            async: false,
                            beforeSend: function() {
                                alerInfo('Buscando DNI, Porfavor espere ..');

                            },
                            success: function (response) {
                                console.log(response);
                                response = JSON.parse(response);
                                if (response.success) {

                                    var result = response.result;

                                    APP._data.dataRegistro.numeroDoc=result.DNI;
                                    APP._data.dataRegistro.nombre=result.Nombres + " " + result.ApellidoPaterno +" "+result.ApellidoMaterno ;

                                } else {
                                    swall(response.message);
                                }
                            },
                            error: function (err) {
                                console.log(err);
                                swall('Error al en el servidor !!');
                            },
                        });
                    }

                },
            }
        });


        $( document ).ready(function() {
            $("#tabla-proveedores").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'csvHtml5',
                ]
            });
        });
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }

        function alerInfo(msg) {
            $.toast({
                heading: 'INFORMACION',
                text: msg,
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
        }
    </script>



</body>


</html>
