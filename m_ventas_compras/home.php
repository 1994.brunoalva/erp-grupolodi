<div class="panel-body">
    <!-- Small boxes (Stat box) -->


    <div class="row">
        <div class="col-lg-9">
            <div class="callout callout-success">
                <h4 align="center"><i class="fa fa-exchange fa-fw"></i> <strong>Importaciones - Fechas / Pagos</strong>
                </h4>
            </div>
            <div class="box box-success">
                <div class="box-body">

                    <table id="table-folder-alert1" class="table table-striped table-bordered datatable_alert" style="width:100%">
                        <thead>

                        <tr class="bg-green-gradient" role="row">

                            <th class="text-center sorting">FOLDER
                            </th>
                            <th class="text-center sorting" style="width: 154px;">IMPORTADOR
                            </th>
                            <th class="text-center sorting" style="width: 152px;">PROVEEDOR
                            </th>
                            <th class="text-center sorting" style="width: 106px;">ETA
                            </th>
                            <th class="text-center sorting" style="width: 221px;">SOBREESTADIA
                            <th class="text-center sorting" style="width: 221px;">HOY
                            </th>
                            <th class="text-center sorting" style="width: 221px;">LIBRES
                            </th>
                            <th class="text-center sorting" style="width: 60px;">Pagos
                            </th>

                        </tr>
                        </thead>
                        <tbody>

                        <?php


                        //echo date_format($date1, 'd-m-Y');

                        foreach ($listaAviso as $row){
                            $date1 = date_create($row['eta']);
                            $date2 = date_create($row['libre_sobre']);
                            $datetime1 = new DateTime($row['libre_sobre']);
                            $datetime2 = new DateTime(date("y-m-d"));

                            $diasRes =  $datetime1->diff($datetime2);
                            ?>
                            <tr class="odd">
                                <td class="text-center"><?php echo $row['folder']?></td>
                                <td class="text-center"><?php echo $row['emp_nombre']?></td>
                                <td class="text-center"><?php echo $row['provee_desc']?></td>
                                <td class="text-center"><?php echo date_format($date1, 'd-m-Y')?></td>
                                <td class="text-center"><?php echo date_format($date2, 'd-m-Y')?></td>
                                <td class="text-center"><?php echo date('d-m-Y')?></td>
                                <td class="text-center"><?php

                                    if ($diasRes->days <= 7){
                                        echo '<span  style="padding-left: 15px;padding-right: 15px;" class="label label-danger">'.$diasRes->days.'</span>';
                                    }elseif ($diasRes->days <= 20){
                                        echo '<span style="padding-left: 15px;padding-right: 15px;" class="label label-success">'.$diasRes->days.'</span>';
                                    }else{
                                        echo $diasRes->days;
                                    }

                                    ?></td>
                                <td class="text-center">
                                    <a href="./importaciones/detalle-folio.php?id=<?php echo $row['impor_id']?>&tab=pa" class="btn btn-primary btn-sm glyphicon glyphicon-list"></a>
                                </td>
                            </tr>
                        <?php  }
                        ?>


                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-lg-3" style="">
            <div class="contenedor-boder-r" style="">
                <div class="text-center headcontenedor-cal">
                    <span style="color: white; font-weight: bold;font-size: 15px">{{getAgendaProyect.mes}}</span>
                </div>
                <div class="contenedor-cal cal-cont-se">
                    <div class="cont-ds-cal">
                        <div class="box-d-cal text-center">
                            <span class="elemt-box-cal" style="font-weight: bold;">DO</span>
                        </div>
                        <div class="box-d-cal">
                            <span class="elemt-box-cal" style="font-weight: bold;">LU</span>
                        </div>
                        <div class="box-d-cal">
                            <span class="elemt-box-cal" style="font-weight: bold;">MA</span>
                        </div>
                        <div class="box-d-cal">
                            <span class="elemt-box-cal" style="font-weight: bold;">MI</span>
                        </div>
                        <div class="box-d-cal">
                            <span class="elemt-box-cal" style="font-weight: bold;">JU</span>
                        </div>
                        <div class="box-d-cal">
                            <span class="elemt-box-cal" style="font-weight: bold;">VI</span>
                        </div>
                        <div class="box-d-cal">
                            <span class="elemt-box-cal" style="font-weight: bold;">SA</span>
                        </div>
                    </div>
                </div>
                <div class="contenedor-cal" style="height: 180px;">
                    <div v-for="item in getAgendaProyect.dias" :class=" item.sta?'box-d-cal box-day-cal': 'box-d-cal'" :style="item.agst?'background-color: rgba(8 ,102 ,198,0.48)':''">
                        <span v-if="item.sta" class="elemt-box-cal">{{item.num}}</span>
                    </div>


                </div
            </div>

        </div>

    </div>
    <!--</div>-->

</div>