<?php
$indexRuta = 1;
require '../conexion/Conexion.php';



$conexionp = (new Conexion())->getConexion();

$sql = "SELECT * FROM sys_agencia_aduana";
$result_AGEN = $conexionp->query($sql);


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <script src="../aConfig/plugins/sweetalert2/sweetalert2.min.css"></script>

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }


        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="../"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Agencias de Aduanas</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <button onclick="MODALES. resetDataRegisterProducto(false)" type="button"
                                                data-toggle="modal" data-target="#modal_aduanas"
                                                class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nueva Agencia
                                        </button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal" class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <table id="table-agencia-lista"
                                       class="table table-striped table-bordered table-hover">
                                    <thead class="bg-head-table">
                                    <tr style="background-color: #007ac3; color: white">
                                        <th style="border-right-color: #007ac3" class="text-center"></th>
                                        <th style="border-right-color: #007ac3" class="text-center">NOMBRE</th>
                                        <th style="border-right-color: #007ac3" class="text-center">RUC</th>
                                        <th style="border-right-color: #007ac3" class="text-center">JURISDICCION</th>
                                        <th style="border-right-color: #007ac3" class="text-center">CODIGO</th>
                                        <th class="text-center">OPCION</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $contador = 1;
                                    foreach ( $result_AGEN as $row){ ?>
                                        <tr>
                                            <td style="color: white"><?=$contador?></td>
                                            <td  class="text-center"><?=$row['age_adua_nombre'] ?></td>
                                            <td  class="text-center"><?=$row['age_adua_ruc'] ?></td>
                                            <td class="text-center"><?=$row['age_adua_juris'] ?></td>
                                            <td class="text-center"><?=$row['age_adua_cod'] ?></td>
                                            <td  class="text-center"><button data-toggle="modal" data-target="#modal_edit_aduanas" onclick="veragencia(<?=$row['age_adua_id'] ?>)" class="btn btn-info"><i class="fa fa-edit"></i></button></td>
                                          </tr>
                                    <?php    $contador++; }
                                    ?>

                                    </tbody>

                                </table>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div id="conten-modales">





                                </div>

                            </div>

                            <div class="modal fade" id="modal_aduanas" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400;">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-border no-padding">
                                            <div class="modal-header text-center color-modal-header">
                                                <h3 class="modal-title">Agregar Nueva agencia de Aduanas</h3>
                                            </div>
                                        </div>
                                        <div class="modal-body  no-border">
                                            <div class="container-fluid">
                                                <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                                    <label class="col-xs-12 no-padding">RUC:</label>
                                                    <div id="aduana-box" class="input-group col-xs-12 no-padding">
                                                        <input id="modal-aduana-input-ruc-num" class="form-control" type="text"
                                                               value="" placeholder="Numero de RUC">
                                                        <span class="input-group-btn">
                    <button onclick="consultaR()" id="modal-aduana-btn-ruc-num" type="button" class="btn btn-primary" >
                        <i class="fa fa-search"></i></button>
                </span>
                                                    </div>
                                                </div>

                                                <div class="form-group col-xs-12 col-md-7">
                                                    <label class="col-xs-12 no-padding">Nombre:</label>
                                                    <input id="modal-aduanas-input-nombre" class="form-control" type="text"
                                                           placeholder="ejem. SALINAS & CASSARETTO" required>
                                                </div>
                                                <div class="form-group col-xs-12 col-md-7">
                                                    <label class="col-xs-12 no-padding">Jurisdicion:</label>
                                                    <input id="modal-aduanas-input-juris" class="form-control" type="text"
                                                           placeholder="ejem. LIMA CALLAO" required>
                                                </div>
                                                <div class="form-group col-xs-12 col-md-5">
                                                    <label class="col-xs-12 no-padding">Codigo:</label>
                                                    <input id="modal-aduanas-input-codigo" class="form-control" type="text" placeholder="#codigo"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="container-fluid">
                                                <hr class="line-frame-modal">
                                            </div>
                                            <div class="container-fluid text-right">
                                                <button type="button" onclick="registrar_aduanas()" id="modal-aduanas-btn-guardar" class="btn btn-primary">
                                                    Guardar
                                                </button>

                                                <button type="button" id="modal-aduanas-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                                                    Cerrar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="modal_edit_aduanas" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400;">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-border no-padding">
                                            <div class="modal-header text-center color-modal-header">
                                                <h3 class="modal-title">Edtar agencia de Aduanas</h3>
                                            </div>
                                        </div>
                                        <div class="modal-body  no-border">
                                            <div class="container-fluid">
                                                <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                                    <label class="col-xs-12 no-padding">RUC:</label>
                                                    <div id="aduana-box" class="input-group col-xs-12 no-padding">
                                                        <input type="hidden" id="modal-aduana-input-id">
                                                        <input id="modal-aduana-input-ruc-nume" class="form-control" type="text"
                                                               value="" placeholder="Numero de RUC">
                                                        <span class="input-group-btn">
                    <button onclick="consultaR2()" id="modal-aduana-btn-ruc-num" type="button" class="btn btn-primary" >
                        <i class="fa fa-search"></i></button>
                </span>
                                                    </div>
                                                </div>

                                                <div class="form-group col-xs-12 col-md-7">
                                                    <label class="col-xs-12 no-padding">Nombre:</label>
                                                    <input id="modal-aduanas-input-nombree" class="form-control" type="text"
                                                           placeholder="ejem. SALINAS & CASSARETTO" required>
                                                </div>
                                                <div class="form-group col-xs-12 col-md-7">
                                                    <label class="col-xs-12 no-padding">Jurisdicion:</label>
                                                    <input id="modal-aduanas-input-jurise" class="form-control" type="text"
                                                           placeholder="ejem. LIMA CALLAO" required>
                                                </div>
                                                <div class="form-group col-xs-12 col-md-5">
                                                    <label class="col-xs-12 no-padding">Codigo:</label>
                                                    <input id="modal-aduanas-input-codigoe" class="form-control" type="text" placeholder="#codigo"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="container-fluid">
                                                <hr class="line-frame-modal">
                                            </div>
                                            <div class="container-fluid text-right">
                                                <button type="button" onclick="guardar_aduanas()" id="modal-aduanas-btn-guardar" class="btn btn-primary">
                                                    Guardar
                                                </button>

                                                <button type="button" id="modal-aduanas-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                                                    Cerrar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>



    <style>
        #img-file-preview-zone {
            -webkit-box-shadow: 1px 1px 4px 1px rgba(75, 87, 209, 1);
            -moz-box-shadow: 1px 1px 4px 1px rgba(75, 87, 209, 1);
            box-shadow: 1px 1px 4px 1px rgba(75, 87, 209, 1);
            border-radius: 3px;
        }
    </style>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <!--script  type="module" src="../aConfig/scripts/sku.js"></script-->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        function guardar_aduanas() {
            var  id = $("#modal-aduana-input-id").val();
            var  ruc = $("#modal-aduana-input-ruc-nume").val();
            var  nombre = $("#modal-aduanas-input-nombree").val();
            var  jurisdiccion = $("#modal-aduanas-input-jurise").val();
            var  codigo = $("#modal-aduanas-input-codigoe").val();
            $.ajax({
                type: "POST",
                url: "../ajax/AgenciaAduana/adtAgencia.php",
                data: {id,ruc,nombre,jurisdiccion,codigo},
                success: function (data) {
                    console.log(data)
                    location.reload();
                }
            });
        }
        function  consultaR(){
            var numRuc= $('#modal-aduana-input-ruc-num').val();
            consultarRuc(numRuc,'#modal-aduanas-input-nombre');
        }
        function  consultaR2(){
            var numRuc= $('#modal-aduana-input-ruc-nume').val();
            consultarRuc(numRuc,'#modal-aduanas-input-nombree');
        }
        function consultarRuc(ruc,nomimp) {
            var json =[];

            $.ajax({
                data: {ruc},
                url: '../ajax/libSunat/sunat/example/consultaRuc.php',
                type: 'POST',
                async: true,
                beforeSend: function() {
                   // alerta.alerInfo('Buscando RUC en Sunat, Porfavor espere ..');
                    $.toast({
                        heading: 'INFORMACION',
                        text: 'Buscando RUC en Sunat, Porfavor espere ..',
                        icon: 'info',
                        position: 'top-right',
                        hideAfter: '2500',
                    });
                },
                success: function (response) {
                    json = JSON.parse(JSON.stringify(response));
                    if (json.success) {
                        $(nomimp).val(json.result.razon_social);

                        $.toast({
                            heading: 'EXITOSO',
                            text: 'Cargando Datos de empresa',
                            icon: 'success',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                    } else {
                        $.toast({
                            heading: 'ALERTA',
                            text: 'El numero RUC es incorrecto ..',
                            icon: 'warning',
                            position: 'top-right',
                            hideAfter: '2500',
                        });

                    }

                },
                error: function () {
                    $.toast({
                        heading: 'ERROR',
                        text: 'Error al en el servidor de SUNAT<br>Reporte este error!!',
                        icon: 'error',
                        position: 'top-right',
                        hideAfter: '2500',
                    });
                    //alerta.alerError('Error al en el servidor de SUNAT<br>Reporte este error!!');
                },
            });
            return json;
        }

        function veragencia(id) {
            $.ajax({
                type: "POST",
                url: "../ajax/AgenciaAduana/getAgencia.php",
                data: {id},
                success: function (data) {
                    console.log(data)
                    data = JSON.parse(data);
                    $("#modal-aduana-input-id").val(data.age_adua_id);
                    $("#modal-aduana-input-ruc-nume").val(data.age_adua_ruc);
                     $("#modal-aduanas-input-nombree").val(data.age_adua_nombre);
                    $("#modal-aduanas-input-jurise").val(data.age_adua_juris);
                    $("#modal-aduanas-input-codigoe").val(data.age_adua_cod);
                }
            });

        }

        function registrar_aduanas(){
           /* var  ruc = $("#modal-aduana-input-ruc-num").val();
            var  nombre = $("#modal-aduanas-input-nombre").val();
            var  jurisdiccion = $("#modal-aduanas-input-juris").val();
            var  codigo = $("#modal-aduanas-input-codigo").val();*/

            var aduaData = new Array();
            aduaData.push('');
            aduaData.push($('#modal-aduanas-input-codigo').val());
            aduaData.push($('#modal-aduanas-input-nombre').val());
            aduaData.push($('#modal-aduana-input-ruc-num').val());
            aduaData.push($('#modal-aduanas-input-juris').val());
            aduaData.push('b1');
            console.log(aduaData);
            $.ajax({
                data: {'array': JSON.stringify(aduaData)},
                url: '../ajax/AgenciaAduana/setAgenciaAndGetRow.php',
                type: 'POST',
                async: true,
                success: function (response) {
                    var json = JSON.parse(JSON.stringify(response));
                    console.log(response);
                    if (json) {
                        location.reload();
                    } else {
                        $.toast({
                            heading: 'INFORMACION',
                            text: 'No se pudo crear agencia de aduana',
                            icon: 'info',
                            position: 'top-right',
                            hideAfter: '2500',
                        });

                    }
                },
                error: function (err) {
                    console.log(err)
                    $.toast({
                        heading: 'ERROR',
                        text: 'Error al crear agencia de aguana<br>Reporte este error!!',
                        icon: 'error',
                        position: 'top-right',
                        hideAfter: '2500',
                    });

                }
            });

        }




        $('#select_nomenglatura').change(function () {
            genNombreProducto();
        });
        $('#select_modal_prod_modelo').change(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-pliege").keyup(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-serie").keyup(function () {
            genNombreProducto();
        });

        function genNombreProducto2() {
            const cate = $("#select_modal_pro_categoria option:selected").text();
            var nombrePr = cate;
            if (cate == "NEUMATICOS") {
                const nomen = $("#select_nomenglatura option:selected").text();
                const tipo = $("#select_opc option:selected").text();
                const ancho = $("#modal-producto-input-neu-ancho").val();
                nombrePr += " " + ancho;
                if (nomen == "MILIMETRICA") {
                    const serie = $("#modal-producto-input-neu-serie").val();
                    nombrePr += "/" + serie;
                }
                if (tipo == "RADIAL") {
                    nombrePr += "R";
                }
                const aroNeu = $("#modal-producto-input-neu-aro").val();
                const plieges = $("#modal-producto-input-neu-pliege").val();
                const marca = $("#select_modal_prod_marca option:selected").text();
                const modelo = $("#select_modal_prod_modelo option:selected").text();
                const pais = $("#select_prod_pais option:selected").text();
                nombrePr += "-" + aroNeu + " " + plieges + "PR " + marca + " " + modelo + " " + pais;

            } else if (cate == "CAMARAS") {
                /*  const  nomen = $("#select_nomenglatura option:selected").text();
                  if (nomen=="MILIMETRICA"){
                      const serie= $("#modal-producto-input-neu-serie").val();
                      nombrePr+="/"+serie;
                  }
                  if (tipo=="RADIAL"){
                      nombrePr+="R";
                  }*/
            }
            $("#modal-producto-input-nombre").val(nombrePr);
        }
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function removeItemFromArr(arr, index) {

            arr.splice(index, 1);
        }

        function addPais() {
            const pais = $("#modal-pais-input-nombre").val();

            $.ajax({
                type: "POST",
                url: "../ajax/Pais/add_pais.php",
                data: {pais},
                success: function (data) {

                    console.log(data);
                    if (isJson(data)) {
                        const jso = JSON.parse(data);
                        if (jso.res) {
                            $("#select_prod_pais").append('<option value="' + jso.data.id + '">' + jso.data.nombre + '</option>');
                            $('#producto-select-presentacion2').selectpicker('refresh');
                            $("#modal-pais-input-nombre").val('');
                            $("#modal_pais").modal('hide');
                            $.toast({
                                heading: 'EXITOSO',
                                text: "Se agrego",
                                icon: 'success',
                                position: 'top-right',
                                hideAfter: '2500',
                            });
                        } else {
                            console.log(data);
                            $.toast({
                                heading: 'ERROR',
                                text: "Error No se pudo agregar",
                                icon: 'error',
                                position: 'top-center',
                                hideAfter: '2500',
                            });
                        }
                    } else {
                        $.toast({
                            heading: 'ERROR',
                            text: "Error",
                            icon: 'error',
                            position: 'top-center',
                            hideAfter: '2500',
                        });
                        console.log(data);
                    }
                }
            });

        }

        $(document).ready(function () {
            $('#table-codigo-sunat').DataTable({
                /*scrollY: false,*/
                /*scrollX: true,*/
                paging: false,
                /* lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],*/
                language: {
                    url: '../assets/Spanish.json'
                }
            });
        });
    </script>


</body>

<script src="../aConfig/scripts/modal_controller.js"></script>

<script type="text/javascript">
    var idproducto=0;
    var idproductoDeta=0;

    MODALES._data.productos.url_data = '../ajax/Producto/get_all_data2.php';
    //MODALES._data.productos.iniciarDatos= true;

    $(document).ready(function () {

        $("#table-agencia-lista").DataTable({
            "buttons": [
                'csv', 'excel'
            ],
            "dom": 'Bfrtip',


        });


        $('#tttttttttttttttt').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
        $('#1111111111111').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });


    });

    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-file-preview-zone').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-file-image").change(function () {
        readImage(this);
        var url = '';
        var valor = $('#input-file-image').val();
        for (var i = 0; i < valor.length; i++) {
            if (i >= 12) {
                url += valor[i];
            }
        }

        $("#label-file-image").text(url);
    });

    function readImage2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-file-preview-zone-modelo').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-file-image-modelo").change(function () {
        readImage2(this);
        var url = '';
        var valor = $('#input-file-image-modelo').val();
        for (var i = 0; i < valor.length; i++) {
            if (i >= 12) {
                url += valor[i];
            }
        }

        // $("#label-file-image").text(url);
    });

</script>

</html>
