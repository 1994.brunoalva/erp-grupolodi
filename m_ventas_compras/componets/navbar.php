<!--<nav class="navbar navbar-default navbar-fixed-top no-radius nav-top bg-azul" role="navigation">-->
<nav class="navbar navbar-default navbar-fixed-top top-navbar no-radius" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <!-- <div class="visible-md visible-lg">
          <a class="navbar-brand" href="index.php"><strong>ERP - GrupoLodi</strong></a>
        </div>-->
        <!--MENU PARA MOVIL-->
        <?php
        $pathIN='';
        for ($idt=0;$idt<$indexRuta;$idt++){
            $pathIN .='../';
        }
        ?>
        <div class="visible-xs">
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> WENDY YELINA SESA QUIJANDRIA&nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configuración</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
        </div>
    </div>
    <div class="visible-md visible-lg">
        <ul class="nav navbar-top-links">
            <li><a href="<?php echo $pathIN ?>"><strong><i class="fa fa-home fa-fw"></i> Incio</strong></a></li>
            <!--<li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <strong><i class="fa fa-book fa-fw"></i> Almacen&nbsp;<i class="fa fa-caret-down"></i></strong>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="index.php?menu=14"><i class="fa fa-asterisk fa-fw"></i> Marcas / Modelos <span class="fa arrow rtop"></span></a></li>
                </ul>
            </li>
            <li><a href="<?php echo $pathIN ?>importaciones/import.php"><strong>Importaciones</strong></a></li>
-->
            <li><a href="<?php echo $pathIN ?>ventas/productos.php"><i class="fa fa-warehouse"></i> <strong>Almacen</strong></a></li>
            <!--li><a href="<?php echo $pathIN ?>#"><i class="fa fa-building"></i> <strong>Empresa</strong></a></li>


            <li class="dropdown text-right">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <strong><i class="fa fa-cogs fa-fw"></i> Ventas<i class="fa fa-caret-down"></i></strong>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo $pathIN ?>ventas/cotizaciones.php"> Cotizaciones<span class="fa arrow rtop"></span></a></li>
                    <li><a href="<?php echo $pathIN ?>ventas/ventas.php"> Ventas<span class="fa arrow rtop"></span></a></li>
                </ul>
            </li-->
            <li class="dropdown text-right">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <strong><i class="fa fa-blinds "></i> Productos<i class="fa fa-caret-down"></i></strong>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo $pathIN ?>productos"> Lista de productos<span class="fa arrow rtop"></span></a></li>
                    <li><a href="<?php echo $pathIN ?>productos/marcas">Marcas<span class="fa arrow rtop"></span></a></li>
                    <li><a href="<?php echo $pathIN ?>productos/modelos">Modelos<span class="fa arrow rtop"></span></a></li>
                </ul>
            </li>

            <!--li class="dropdown text-right">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <strong><i class="fa fa-cogs fa-fw"></i> Notas Electronicas<i class="fa fa-caret-down"></i></strong>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo $pathIN ?>notaselectronicas"> Notas de Credito<span class="fa arrow rtop"></span></a></li>
                    <li><a href="<?php echo $pathIN ?>notaselectronicas/debito.php"> Notas de Debito<span class="fa arrow rtop"></span></a></li>
                </ul>
            </li-->
            <li><a href="<?php echo $pathIN ?>proveedores/"><i class="fa fa-handshake"></i> <strong>Proveedores</strong></a></li>
            <li><a href="<?php echo $pathIN ?>importaciones/import.php"><i class="fa fa-globe-stand"></i> <strong>Importaciones</strong></a></li>
            <li><a href="<?php echo $pathIN ?>productos/compras.php"><i class="fa fa-shopping-basket"></i> <strong>Compras</strong></a></li>
            <li class="dropdown text-right">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <strong><i class="fa fa-cogs fa-fw"></i> Configuración&nbsp;<i class="fa fa-caret-down"></i></strong>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo $pathIN ?>ventas/agencias.php"><i class="fa fa-truck fa-fw"></i> Agencias De Transporte<span class="fa arrow rtop"></span></a></li>
                    <li><a href="<?php echo $pathIN ?>aduanas/"><i class="fa fa-id-card fa-fw"></i> Agencias De Aduanas<span class="fa arrow rtop"></span></a></li>
                    <li><a href="<?php echo $pathIN ?>datosimport/almacenes_aduaneros.php"><i class="fa fa-warehouse fa-fw"></i> Almacenes Aduaneros<span class="fa arrow rtop"></span></a></li>
                    <li><a href="<?php echo $pathIN ?>productos/categorias/" ><i class="fa fa-cog fa-fw"></i> Categorias<span class="fa arrow rtop"></span></a></li>
                    <li><a href="<?php echo $pathIN ?>pais/" data-toggle="modal"><i class="fa fa-flag fa-fw"></i> Países<span class="fa arrow rtop"></span></a></li>
                    <li><a href="#" data-toggle="modal"><i class="fa fa-user fa-fw"></i> Perfil<span class="fa arrow rtop"></span></a></li>
                    <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Salir <span class="fa arrow rtop"></span></a></li>
                </ul>
            </li>

            <!-- <li><a href="<?php echo $pathIN ?>ventas/clientes.php"><strong>Clientes</strong></a></li>
            <li><a href="<?php echo $pathIN ?>proveedores"><strong>Proveedores</strong></a></li>
            <li><a href="<?php echo $pathIN ?>ventas/agencias.php"><strong>Agencias de transporte</strong></a></li>
            <li><a href="<?php echo $pathIN ?>cajaChica"><strong>Caja chica</strong></a></li>
            <li class="dropdown text-right">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <strong><i class="fa fa-cogs fa-fw"></i> Configuración&nbsp;<i class="fa fa-caret-down"></i></strong>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="index.php?menu=6&amp;opc=consul"><i class="fa fa-truck fa-fw"></i> Agencias<span class="fa arrow rtop"></span></a></li>
                    <li><a href="index.php?menu=5" data-toggle="modal"><i class="fa fa-cog fa-fw"></i> Categorias<span class="fa arrow rtop"></span></a></li>
                    <li><a href="index.php?menu=4" data-toggle="modal"><i class="fa fa-flag fa-fw"></i> Países<span class="fa arrow rtop"></span></a></li>
                    <li><a href="#M_Perfil" data-toggle="modal"><i class="fa fa-user fa-fw"></i> Perfil<span class="fa arrow rtop"></span></a></li>
                    <li><a href="../Funciones/Salir.php"><i class="fa fa-sign-out fa-fw"></i> Salir <span class="fa arrow rtop"></span></a></li>
                </ul>
            </li>-->
            <!-- /.dropdown -->
        </ul>
    </div>
    <!--#hidden-xs oculto / visible-xs -->
</nav>


<!--<nav class="navbar navbar-default navbar-fixed-top no-radius nav-top bg-azul" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>

        </button>
        <div class="visible-md visible-lg">
            <a class="navbar-brand" href="index.php"><strong>ERP - GrupoLodi</strong></a>
        </div>

        <div class="visible-xs">
            <ul class="nav navbar-nav navbar-top-links navbar-right">

                <li class="dropdown content-a-nav">
                    <a class="dropdown-toggle a-xs" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user-sub">

                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configuraci&oacute;n</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../Funciones/Salir.php"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                        </li>
                    </ul>

                </li>

            </ul>
        </div>
    </div>
    <div class="visible-md visible-lg menu-bar-lodi bg-azul">

        <ul class="nav navbar-nav navbar-top-links">

            <li class="dropdown menu-bar-lodi-li">
                <a class="dropdown-toggle a-nav" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-home"></i> Inicio&nbsp;
                </a>

            </li>
            <li class="dropdown menu-bar-lodi-li">
                <a class="dropdown-toggle dropdown-a a-nav" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-book fa-fw"></i> Compras&nbsp;<i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">

                    <li><a href="../importaciones/import.php"><i class="fa fa-book fa-fw"></i> Importaciones</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="../ventas/ventas.php"><i class="fa fa-book fa-fw"></i> Ventas</a></li>

                </ul>

            </li>
            <li class="dropdown menu-bar-lodi-li">
                <a class="dropdown-toggle a-nav" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-book fa-fw"></i> Ventas&nbsp;<i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">

                    <li class="divider"></li>
                    <li><a href="index.php?menu=19"><i class="fa fa-book fa-fw"></i> Cotizacion</a>
                    </li>


                </ul>

            </li>
            <li class="dropdown menu-bar-lodi-li">
                <a class="dropdown-toggle a-nav" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-cog fa-fw"></i> Configuraci&oacute;n&nbsp;<i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="index.php?menu=4"><i class="fa fa-group fa-fw"></i>Marcas</a>
                    </li>

                    <li class="divider"></li>
                    <li><a href="index.php?menu=1"><i class="fa fa-group fa-fw"></i>Productos</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="index.php?menu=20"><i class="fa fa-group fa-fw"></i>Paises</a>
                    </li>

                </ul>

            </li>


        </ul>

    </div>

-->