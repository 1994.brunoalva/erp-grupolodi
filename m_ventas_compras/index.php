<?php
date_default_timezone_set("America/Lima");
require "conexion/Conexion.php";
$conexion = (new Conexion())->getConexion();

$listaAviso = $conexion->query("SELECT * FROM vista_impor_aviso");

$indexRuta=0;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="aConfig/Mycss/css-home.css" rel="stylesheet">
    <link href="aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="assets/datatables.css" rel="stylesheet">
    <link href="assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">
    <link href="aConfig/plugins/bootstrap-datepicker-1.9.0-dist/css/bootstrap-datepicker.css">

    <script src="assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="assets/datatables.js"></script>
    <script src="assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>


    <style>

        .contenedor-boder-r{
            border: 1px solid #d4cfcf;
            border-radius: 10px;
            -webkit-box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
            -moz-box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
            box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
        }
        .onliborder{
            border: 1px solid #d4cfcf;
            border-radius: 10px;
        }

        .contenedor-cal{
            clear: both;
            overflow: hidden;
        }
        .cont-ds-cal{

        }
        .headcontenedor-cal{
            background-color: #0866c6;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            padding-bottom: 5px;
            padding-top: 5px;
        }

        .cal-cont-se{
            border-bottom: 1px  solid rgba(6, 33, 73, 0.94);
            clear: both;
            overflow: hidden;
            background-color: #0866c61f;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .box-d-cal{
            width: 14.284%;
            text-align: center;

            display: flex ;
            position: relative ;
            float: left ;
            min-height: 1px ;
            height: 16.5%;

        }
        .box-day-cal:hover{
            border: 1px  solid rgb(194, 194, 194);
            cursor: pointer;
            color: white;
            background-color: rgba(8, 102, 198, 0.53);
        }
        .elemt-box-cal{
            display: block;
            margin: auto;
            font-size: 14px;
        }
        .send-left{
            float: left;
        }
        .send-right{
            float: right;
        }

        .box-unit{


            clear: both;
            overflow: hidden;

            height: 120px;
        }
        .box-footer-unic{

            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
            text-align: center;
            padding: 5px;
        }
        .headbox{
            clear: both;
            overflow: hidden;
            padding: 15px;
            margin-bottom: 13px;
        }
        .boxclice:hover{
            cursor: pointer;
            -webkit-box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
            -moz-box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
            box-shadow: -1px 3px 29px -8px rgba(0,0,0,0.75);
        }
    </style>


</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


   /* require_once 'model/model.php';*/
    $home='./';
    $ventas='ventas/ventas.php';
    $importaciones='importaciones/import.php';
    include 'componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <!--<a href="importaciones/import.php"><i class="fa fa-home"></i> Inicio</a>-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include 'componets/nav.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">

                                <div class="row">
                                    <div class="col-lg-2 col-md-4 col-sm-6" >
                                        <a href="importaciones/import.php">
                                            <div class="onliborder box-unit boxclice" style="background-color: #0073b6;">
                                                <div class="headbox" style="">
                                                    <p style="color: white; font-size: 25px;" class="send-left">Importaciones</p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-asterisk fa-lg"></i>

                                                </div>
                                                <div style="background-color: #0066a4;" class="box-footer-unic" >
                                                    <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                </div>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-6" >
                                        <a href="./productos/">
                                            <div style="background-color: #dd4c39;" class="onliborder box-unit boxclice">
                                                <div class="headbox" style="">
                                                    <p style="color: white; font-size: 25px;" class="send-left">Productos</p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-cubes fa-lg"></i>

                                                </div>
                                                <div style="background-color: #c64233;" class="box-footer-unic">
                                                    <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                </div>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-6" >
                                        <a href="./productos/marcas/">
                                            <div class="onliborder box-unit boxclice"  style="background-color: #00a65a;">
                                                <div class="headbox" style="">
                                                    <p style="color: white; font-size: 25px;" class="send-left">Marcas</p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-file-image fa-lg"></i>

                                                </div>
                                                <div style="background-color: #009551;" class="box-footer-unic">
                                                    <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                </div>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-6" >
                                        <a href="./productos/modelos/">
                                            <div class="onliborder box-unit boxclice"  style="background-color: #f39c11;">
                                                <div class="headbox" style="">
                                                    <p style="color: white; font-size: 25px;" class="send-left">Modelos</p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-clone fa-lg"></i>

                                                </div>
                                                <div style="background-color: #da8c10;" style="" class="box-footer-unic">
                                                    <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                </div>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-6" >
                                        <a href="./aduanas/">
                                            <div class="onliborder box-unit boxclice"  style="background-color: #8683bc;">
                                                <div class="headbox" style="">
                                                    <p style="color: white; font-size: 25px;" class="send-left">Aduanas</p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-car fa-lg"></i>

                                                </div>
                                                <div style="background-color: #5d5b9a;" class="box-footer-unic">
                                                    <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-6" >
                                        <a href="./proveedores/">
                                            <div class="onliborder box-unit boxclice"  style="background-color: #00c0ef;">
                                                <div  class="headbox" style="">
                                                    <p style="color: white; font-size: 25px;" class="send-left">Proveedores</p> <i  style="color: white;    font-size: 32px;" class="send-right fa fa-building fa-lg"></i>

                                                </div>
                                                <div style="background-color: #00abd7;" class="box-footer-unic">
                                                    <span style="color: white">Mas Informacion <i class="fa fa-arrow-circle-right"></i></span>
                                                </div>
                                            </div>
                                        </a>

                                    </div>

                                </div>
                            </div>


                            <div id="conten-principal"  class="col-xs-12 col-sm-12 col-md-12">
                                <?php
                                include 'home.php';
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>
    <?php
    /*include 'modal/modal.php';*/
    ?>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="aConfig/Input_validate.js"></script>
    <script type="module" src="aConfig/Import-main.js"></script>
    <script type="module" src="aConfig/alertToas.js"></script>
    <script type="text/javascript" src="assets/Toast/build/jquery.toast.min.js"></script>
    <script src="aConfig/Myjs/contador_espinner.js"></script>
    <script src="aConfig/plugins/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="https://unpkg.com/default-passive-events"></script>

    <script>

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }
        var dt = new Date();
        const app = new Vue({
            el:"#conten-principal",
            data:{
                datalistTab:[],
                anio: dt.getFullYear(),
                mesA:dt.getMonth(),
                semanas: ['DO', 'LU', 'MA', 'MI', 'JU', 'VI', 'SA'],
                meses: ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"],
                calendario :[],
            },
            methods:{

            },
            computed: {
                getAgendaProyect() {
                    var tempMeses = [];

                    var i = this.mesA;
                    var mesTep = {mes: this.meses[i], mesNum: i};

                    var tempDiasme = [];
                    const sumdTemp = i + 1;
                    var diaTemp = new Date(this.anio + "-" + (sumdTemp < 10 ? '0' + sumdTemp : i + 1) + "-01");
                    //  console.log(this.anio+"-"+(sumdTemp<10?'0'+sumdTemp:i+1)+"-01")
                    for (var di = 0; di < diaTemp.getUTCDay(); di++) {
                        tempDiasme.push({
                            num: 0,
                            sta: false
                        })
                    }
                    var lastDia = 0;
                    for (var x = 0; x < diasEnUnMes(i + 1, this.anio); x++) {
                        var tempDia = {
                            num: x + 1,
                            sta: true,
                            agst: false
                        };
                        if ((this.anio+"-"+(sumdTemp<10?'0'+sumdTemp:i+1)+"-"+(x+1<10?'0'+(x+1):x+1))==formatDate(dt)){
                            tempDia.agst=true;
                        }

                        tempDiasme.push(tempDia)
                        lastDia = x + 1;
                    }
                    diaTemp = new Date(this.anio + "-" + (sumdTemp < 10 ? '0' + sumdTemp : i + 1) + "-" + (lastDia < 10 ? '0' + lastDia : lastDia));
                    //console.log(this.anio+"-"+(sumdTemp<10?'0'+sumdTemp:i+1)+"-"+(lastDia<10?'0'+lastDia:lastDia))
                    for (var df = diaTemp.getUTCDay(); df < 6; df++) {
                        var diaTemporal = {
                            num: 0,
                            sta: false
                        };
                        tempDiasme.push(diaTemporal)
                    }

                    console.log("-------")
                    mesTep.dias = tempDiasme;
                    //tempMeses.push(mesTep)
                    this.calendario = mesTep;

                    return mesTep;
                },
            }
        });

        function diasEnUnMes(mes, año) {
            return new Date(año, mes, 0).getDate();
        }
    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            restDataTable();
        });
        function restDataTable() {
            $('.datatable_alert').DataTable({
                scrollY: false,
                scrollX: true,
                scrollCollapse: false,
                paging: true,
                autoWidth: false,
                fixedColumns: {
                    leftColumns: 2,
                },
                language: {
                    url: './assets/Spanish.json'
                }
            });
        }

    </script>


</body>

</html>
