<?php
$indexRuta=1;
require '../conexion/Conexion.php';





$conexionp =  (new Conexion())->getConexion();



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <script src="../aConfig/plugins/sweetalert2/sweetalert2.min.css"></script>

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Notas De Credito</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <button onclick="" type="button"  data-toggle="modal" data-target="#modal-gen-nota-electronica" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Nota De Credito
                                        </button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <table id="table-productos" class="table table-striped table-bordered table-hover">
                                    <thead class="bg-head-table">
                                    <tr style="background-color: #007ac3; color: white">
                                        <th style="border-right-color: #007ac3" class="text-left">FECHA</th>
                                        <th style="border-right-color: #007ac3" class="text-left">NUMERO / SERIE</th>
                                        <th style="border-right-color: #007ac3" class="text-left">CLIENTE</th>
                                        <th style="border-right-color: #007ac3" class="text-left">MONEDA</th>
                                        <th style="border-right-color: #007ac3" class="text-left">TOTAL</th>
                                        <th style="border-right-color: #007ac3" class="text-left">SUANT</th>
                                        <th class="text-left">OPCION</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        <tr v-for="(item ,index) in lista">
                                            <td>{{item.fecha}}</td>
                                            <td>{{item.ns}}</td>
                                            <td>{{item.cliente}}</td>
                                            <td>{{item.moneda}}</td>
                                            <td>{{item.total}}</td>
                                            <td>No Enviado</td>
                                            <td>
                                                <button  class="btn btn-sm btn-warning fa fa-file btn-selector-cliente"  onclick=""></button>
                                                <button  class="btn btn-sm btn-info fa fa-archive btn-selector-cliente"  onclick="" ></button>
                                            </td>
                                        </tr>



                                    </tbody>

                                </table>


                                <div class="modal fade" id="modal-gen-nota-electronica" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog" role="document" style="width: 60%;">
                                        <div class="modal-content">
                                            <div class="modal-header no-border no-padding">
                                                <div class="modal-header text-center color-modal-header">
                                                    <h3 class="modal-title">NOTA ELECTRONICA</h3>
                                                </div>
                                            </div>

                                            <div class="modal-body  no-border">
                                                <form action="#">
                                                    <div class="container-fluid">
                                                        <form>
                                                            <div class="row">
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <label class="col-xs-12 no-padding">DOCUMENTO:</label>
                                                                    <div class="input-group col-xs-12">

                                                                        <select v-model="dataNE.tipo" class="form-control">
                                                                            <option selected value="1">NOTA DE CREDITO</option>
                                                                            <!--option value="2">NOTA DE DEBITO</option-->
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">S -N:</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input v-model="dataNE.serieNumero" disabled class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">FECHA:</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input v-model="dataNE.fecha" disabled value="" class="form-control">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                                                    <label class="col-xs-12 no-padding">MOTIVO:</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <select v-model="dataNE.motivo" class="form-control" id="" name="select_motivo">
                                                                            <option value="1">ANULACION DE LA OPERACION</option>
                                                                            <option value="2">ANULACION POR ERROR EN EL RUC</option>
                                                                            <option value="8">BONIFICACION</option>
                                                                            <option value="3">CORRECCION POR ERROR EN LA DESCRIPCION</option>
                                                                            <option value="4">DESCUENTO GLOBAL</option>
                                                                            <option value="5">DESCUENTO POR ITEM</option>
                                                                            <option value="7">DEVOLUCION POR ITEM</option>
                                                                            <option value="6">DEVOLUCION TOTAL</option>
                                                                            <option value="9">DISMINUCION EN EL VALOR</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <label class="col-xs-12 no-padding">TIPO DOCUMENTO</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <select id="tipodo" class="form-control">
                                                                            <option value="1">FACTURA</option>
                                                                            <option value="2">BOLETA</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <label class="col-xs-12 no-padding">EMPRESA</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <select id="empresa" class="form-control">
                                                                            <option value="1">GRUPO LODI SRL</option>
                                                                            <option value="2">CONCORD PLUS S.R.LTDA.</option>
                                                                            <option value="3">LODI IMPORT CENTER S.R.L.</option>
                                                                            <option value="4">PERU TIRES S.R.L.</option>
                                                                            <option value="5">ASIAPERU TRADING S.R.L.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <label class="col-xs-12 no-padding">NUMERO DE DOCUMENTO</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input v-model="dataNE.documentoRelacionado"  class="form-control col-xs-6">
                                                                        <span class="input-group-btn">
                                                                            <button v-on:click=" buscarDocumento()" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_incoterm">
                                                                                <i class="fa fa-search"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <label class="col-xs-12 no-padding">TOTAL DEL DOCUMENTO</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input v-model="dataNE.totalDocumento" disabled class="form-control col-xs-6">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                                                    <label class="col-xs-12 no-padding">DESCRIPCION</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input v-model="dataNE.dataRegistro.descrip"  class="form-control col-xs-6">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">CANTIDAD</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input v-model="dataNE.dataRegistro.cantidad"  class="form-control col-xs-6">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">PRECIO U.</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input  v-model="dataNE.dataRegistro.precio_unitario"  class="form-control col-xs-6">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                    <div class="input-group col-xs-12 text-center" style="margin-bottom: 5px;">
                                                                        <button v-on:click="agregar()" type="button" class="btn btn-primary">AGREGAR</button>
                                                                    </div>
                                                                    <div class="input-group col-xs-12 text-center" >
                                                                        <button  type="button" class="btn btn-danger">ELIMINAR</button>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <table id="tabla-poductos-coti" class="table table-bordered table-hover" style="width:100%">
                                                                        <thead>
                                                                        <tr  style="background-color: #007ac3; color: white">

                                                                            <th style="border-right-color: #007ac3" >#</th>
                                                                            <th style="border-right-color: #007ac3">DESCRIPCION</th>
                                                                            <th style="border-right-color: #007ac3">MEDIDA</th>
                                                                            <th style="border-right-color: #007ac3">CANTIDAD</th>
                                                                            <th style="border-right-color: #007ac3">PRECIO</th>
                                                                            <th style="border-right-color: #007ac3">IMPORTE</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr v-for="(pro, ix) in dataNE.productos">
                                                                            <td>{{ix + 1}}</td>
                                                                            <td>{{pro.descrip}}</td>
                                                                            <td>{{pro.medida}}</td>
                                                                            <td  class="text-center">{{pro.cantidad}}</td>
                                                                            <td  class="text-center">{{simboleModena}} {{pro.precio_unitario}}</td>
                                                                            <td  class="text-center" style="background-color: #feffcb">{{simboleModena}} {{ (pro.cantidad * pro.precio_unitario).toFixed(2)}}</td>
                                                                        </tr>
                                                                        </tbody>
                                                                        <tfoot>
                                                                        <tr>
                                                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">Sub. Total</td>
                                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{dataNE.subTotal}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">IGV</td>
                                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{dataNE.igv}} </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" style="text-align: right; font-weight: bold; font-size: 18px">Total</td>
                                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{getTotoal}}</td>
                                                                        </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </div>
                                                    <div class="container-fluid">
                                                        <hr class="line-frame-modal">
                                                    </div>
                                                    <div class="container-fluid text-right">

                                                        <button v-on:click="GuardarDocumento()" type="button"  class="btn btn-primary">
                                                            Guardar
                                                        </button>
                                                        <button type="button" id="modal-buscar-empresa-btn-cerrar" class="btn btn-danger"
                                                                data-dismiss="modal">
                                                            Cancelar
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div id="conten-modales">


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>



       <script>
        $(document).ready(function () {
            $('#table-codigo-sunat').DataTable({
                /*scrollY: false,*/
                /*scrollX: true,*/
                paging: false,
                /* lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],*/
                language: {
                    url: '../assets/Spanish.json'
                }
            });
        });
    </script>


    <style>
        #img-file-preview-zone{
            -webkit-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            -moz-box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            box-shadow: 1px 1px 4px 1px rgba(75,87,209,1);
            border-radius: 3px;
        }
    </style>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <!--script  type="module" src="../aConfig/scripts/sku.js"></script-->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        var  fecha =  new Date();
        const APP = new Vue({
            el:"#contenedorprincipal",
            data:{
                simboleModena:'S/.',
                contador:3,
                lista:[
                    {fecha:'21/08/2020',ns:'F001 - 000002',cliente:"AGRONEGOCIOS VIRGEN DE YAUCA S.A.C.",moneda:"DOLARES",total:"4300.00",}
                ],
                dataNE:{
                    idVenta:0,
                    tipo:1,
                    subTotal:0,
                    igv:0,
                    total:0,
                    serieNumero:"00003",
                    fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate(),
                    motivo:1,
                    documentoRelacionado:"",
                    totalDocumento:0,
                    dataRegistro:{
                        descrip:'',medida:'UND',cantidad:0,precio_unitario:0,
                    },
                    productos:[]
                },

            },
            methods:{
                buscarDocumento(){
                    var tipo = $("#tipodo").val();
                    var empresa = $("#empresa").val();
                    var numero = this.dataNE.documentoRelacionado;
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Ventas/busquedaVenta.php",
                        data: {
                            empres:empresa,
                            tipo:tipo,
                            numero:numero
                        },
                        success: function (dataResp) {
                            console.log(dataResp)
                            if (isJson(dataResp)){
                                var jso = JSON.parse(dataResp);
                               // this.dataNE.serieNumero = jso;
                                APP._data.dataNE.totalDocumento =jso.monto;
                                swal("Documento encontrado", "", "success");

                            }else{
                                swal("Documento no encontrado")
                            }
                        }
                    });
                },
                GuardarDocumento(){


                    this.lista.push({fecha:APP._data.dataNE.fecha,
                        ns:"F001 - "+APP._data.dataNE.serieNumero,
                        cliente:"AGUILAR EVENTOS S.A.C.",
                        moneda:"SOLES",
                        total:APP._data.dataNE.total,});
                    swal("Documento Guardado", "", "success");
                    this.dataNE={
                        idVenta:0,
                            tipo:1,
                            subTotal:0,
                            igv:0,
                            total:0,
                            serieNumero:"0000"+APP._data.contador,
                            fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate(),
                            motivo:1,
                            documentoRelacionado:"",
                            totalDocumento:0,
                            dataRegistro:{
                            descrip:'',medida:'UND',cantidad:0,precio_unitario:0,
                        },
                        productos:[]
                    };
                    APP._data.contador++;
                    $('#modal-gen-nota-electronica').modal('toggle');


                },
                agregar(){
                    const tempo = {... APP._data.dataNE.dataRegistro};
                    this.dataNE.productos.push({... APP._data.dataNE.dataRegistro})
                    this.dataNE.dataRegistro={
                        descrip:'',medida:'UND',cantidad:0,precio_unitario:0,
                    }

                }
            },
            computed:{
                getTotoal(){
                    this.dataNE.subTotal=0;
                    for (var i=0 ;  i< this.dataNE.productos.length; i++){
                        this.dataNE.subTotal+= parseInt(this.dataNE.productos[i].cantidad) * parseFloat(this.dataNE.productos[i].precio_unitario);
                    }
                    this.dataNE.igv= this.dataNE.subTotal * 0.18;
                    this.dataNE.total =  this.dataNE.subTotal+this.dataNE.igv;
                    return this.dataNE.total;
                }
            }
        });


    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>


<script type="text/javascript">


    $(document).ready(function() {


        $('#table-productos').DataTable();
    });


</script>

</html>
