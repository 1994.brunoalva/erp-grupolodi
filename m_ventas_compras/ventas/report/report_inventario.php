<?php
require "../../conexion/Conexion.php";
require_once "../../Lib/libexel/vendor/autoload.php";
require_once "../../Lib/mpdf/vendor/autoload.php";
require "../../utils/Tools.php";

$conexion = (new Conexion())->getConexion();
$tools = new Tools();

$empresa= $_POST['empresa'];
$categoria = $_POST['catego'];
$clau="";

if ($categoria==0){
    $clau=" AND emp.emp_id = ". $empresa;
}else{
    $clau=" AND emp.emp_id = ". $empresa . " AND cate.cat_id=".$categoria;
}

    $sql="SELECT
  `prod_empre`.`prod_empre_id` AS `prod_empre_id`,
  `prod`.`produ_id`            AS `produ_id`,
  `emp`.`emp_nombre`           AS `emp_nombre`,
  `prod`.`produ_nombre`        AS `produ_nombre`,
  `unidad`.`unidad_nombre`     AS `unidad_nombre`,
  `prod`.`produ_sku`           AS `produ_sku`,
  `cate`.`cat_nombre`          AS `cat_nombre`,
  `marc`.`mar_nombre`          AS `mar_nombre`,
  `modelo`.`mod_nombre`        AS `mod_nombre`,
  `pais`.`pais_nombre`         AS `pais_nombre`,
  `prod_empre`.`cantidad` AS `cantidad`
FROM (((((((`sys_producto_empresa` `prod_empre`
         JOIN `sys_producto` `prod`
           ON (`prod_empre`.`id_prod` = `prod`.`produ_id`))
        JOIN `sys_empresas` `emp`
          ON (`prod_empre`.`id_empresa` = `emp`.`emp_id`))
       JOIN `sys_com_categoria` `cate`
         ON (`prod`.`cat_id` = `cate`.`cat_id`))
      JOIN `sys_com_marca` `marc`
        ON (`prod`.`mar_id` = `marc`.`mar_id`))
     JOIN `sys_pais` `pais`
       ON (`prod`.`pais_id` = `pais`.`pais_id`))
    JOIN `sys_com_modelo` `modelo`
      ON (`prod`.`mod_id` = `modelo`.`mod_id`))
   JOIN `sys_unidad` `unidad`
     ON (`prod`.`unidad_id` = `unidad`.`unidad_id`))
WHERE `prod`.`cat_id` <> 6 $clau";



$respuPRo = $conexion->query($sql);
$rowTable="";
foreach ($respuPRo as $prod){
    $rowTable = $rowTable . "<tr>
                <td style='text-align: center'>{$prod['produ_nombre']}</td> 
                <td style='text-align: center'>{$prod['produ_sku']}</td> 
                <td style='text-align: center'>{$prod['unidad_nombre']}</td> 
                <td style='text-align: center'>{$prod['cat_nombre']}</td> 
                <td style='text-align: center'>{$prod['mar_nombre']}</td> 
                <td style='text-align: center'>{$prod['pais_nombre']}</td> 
                <td style='text-align: center'>{$prod['cantidad']}</td> 
              </tr>";
}


$html="<table >
             
            <tr  style=\"background-color: #007ac3; color: white\" >

                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 70px\"  >PRODUCTO</th>
                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 50px\"  >SKU</th>
                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 20px\" >MEDIDA</th>
                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 20px\"  >CATEGORIA</th>
                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 20px\" >MARCA</th>
                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 20px\" >PAIS</th>
                <th  style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 20px\"  >STOCK</th> 

            </tr>
             
$rowTable
        </table>";


$reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
$spreadsheet = $reader->loadFromString($html);
/*
$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
$writer->save('write.xlsx');*/
$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
$writer->save("inventario principal.xlsx");


