<?php
require "../../conexion/Conexion.php";
require_once "../../Lib/libexel/vendor/autoload.php";
require_once "../../Lib/mpdf/vendor/autoload.php";
require "../../utils/Tools.php";

$conexion = (new Conexion())->getConexion();
$tools = new Tools();

$empresa= $_POST['empresa'];
$categoria = $_POST['catego'];
$clau="";

if ($categoria==0){
    $clau=" AND prod_emp.id_empresa = ". $empresa;
}else{
    $clau=" AND prod_emp.id_empresa = ". $empresa . " AND cat.cat_id=".$categoria;
}

    $sql="SELECT
  `promo`.`prod_promo_id`  AS `prod_promo_id`,
  `promo`.`prod_emp_id`    AS `prod_emp_id`,
  `promo`.`prod_id`        AS `prod_id`,
  `promo`.`emp_id`         AS `emp_id`,
  `promo`.`precio`         AS `precio`,
  `promo`.`cantidad`       AS `cantidad`,
  `promo`.`estado`         AS `estado`,
  `prod`.`produ_sku`       AS `produ_sku`,
  `prod`.`produ_nombre`    AS `produ_nombre`,
  `unidad`.`unidad_nombre` AS `unidad_nombre`,
  `marca`.`mar_nombre`     AS `mar_nombre`,
  `cat`.`cat_nombre`       AS `cat_nombre`
FROM (((((`sys_prod_promocion` `promo`
       JOIN `sys_producto_empresa` `prod_emp`
         ON (`promo`.`prod_emp_id` = `prod_emp`.`prod_empre_id`))
      JOIN `sys_producto` `prod`
        ON (`prod_emp`.`id_prod` = `prod`.`produ_id`))
     JOIN `sys_com_marca` `marca`
       ON (`prod`.`mar_id` = `marca`.`mar_id`))
    JOIN `sys_unidad` `unidad`
      ON (`prod`.`unidad_id` = `unidad`.`unidad_id`))
   JOIN `sys_com_categoria` `cat`
     ON (`prod`.`cat_id` = `cat`.`cat_id`))
WHERE `promo`.`estado` <> 'REGALO' $clau";

//echo $sql;


$respuPRo = $conexion->query($sql);
$rowTable="";
foreach ($respuPRo as $prod){
    $rowTable = $rowTable . "<tr>
                <td style='text-align: center'>{$prod['produ_nombre']}</td> 
                <td style='text-align: center'>{$prod['produ_sku']}</td> 
                <td style='text-align: center'>{$prod['unidad_nombre']}</td> 
                <td style='text-align: center'>{$prod['cat_nombre']}</td> 
                <td style='text-align: center'>{$prod['mar_nombre']}</td> 
                <td style='text-align: center'>{$prod['cantidad']}</td> 
                <td style='text-align: center'>{$prod['estado']}</td> 
              </tr>";
}


$html="<table >
             
            <tr  style=\"background-color: #007ac3; color: white\" >

                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 70px\"  >PRODUCTO</th>
                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 50px\"  >SKU</th>
                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 20px\" >MEDIDA</th>
                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 20px\"  >CATEGORIA</th>
                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 20px\" >MARCA</th>
                <th style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 20px\" >STOCK</th>
                <th  style=\"border-right-color: #007ac3;text-align: center; font-weight: bold; width: 20px\"  >ESTADO</th> 

            </tr>
             
$rowTable
        </table>";


$reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
$spreadsheet = $reader->loadFromString($html);
/*
$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
$writer->save('write.xlsx');*/
$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
$writer->save("inventario promociones.xlsx");


