<div class="modal fade" id="modal_agregar_cliente" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Registrar <!--Empresa-->Cliente</h3>
                </div>
            </div>

            <div class="modal-body  no-border">
                <form action="#" v-on:submit.prevent="registrarCliente()">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                <label class="col-xs-12 no-padding">Documento RUC / DNI:</label>

                                <div class="input-group col-xs-12 no-padding">
                                    <input id="inputConsultarRUc" placeholder="Numero de documento" required v-model="clientes.dataRegistro.ruc"  type="text" class="form-control">
                                    <span class="input-group-btn">
                                                <button v-on:click="consultaDocumentoNum()" type="button"  class="btn btn-primary">
                                                    <i class="fa fa-search"></i></button>
                                            </span>
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                <label class="col-xs-12 no-padding">NOMBRE / RAZON SOCIAL :</label>
                                <div class="input-group col-xs-12">
                                    <input required disabled v-model="clientes.dataRegistro.nombresocial"  type="text" class="form-control input-number"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>

                            <div class="form-group col-xs-5 col-sm-6 col-md-5">
                                <label class="col-xs-12 no-padding">DIRECCION FISCAL/ SUCURSAL:</label>
                                <div class="input-group col-xs-12">

                                    <input v-model="clientes.dataRegistro.direccion"  type='text' class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">DEPARTAMENTO:</label>
                                <div class="input-group col-xs-12">
                                    <input required v-model="clientes.dataRegistro.departamento"   type='text' class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">PROVINCIA:</label>
                                <div class="input-group col-xs-12">
                                    <input required v-model="clientes.dataRegistro.provincia"  type='text' class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">DISTRITO:</label>
                                <div class="input-group col-xs-12">
                                    <input required v-model="clientes.dataRegistro.distrito"  type='text' class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">UBIGEO:</label>
                                <div class="input-group col-xs-12">
                                    <input v-model="clientes.dataRegistro.ubigeo"  type='text' class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>


                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">EMAIL 1:</label>
                                <div class="input-group col-xs-12">
                                    <input v-model="clientes.dataRegistro.email1"  type="email" class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">EMAIL 2:</label>
                                <div class="input-group col-xs-12">
                                    <input v-model="clientes.dataRegistro.email2"  type="email" class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">TELEFONO 1:</label>
                                <div class="input-group col-xs-12">
                                    <input  v-model="clientes.dataRegistro.telefono" type="text" class="form-control input-number"
                                            aria-describedby="basic-addon1"
                                            value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">TELEFONO 2:</label>
                                <div class="input-group col-xs-12">
                                    <input v-model="clientes.dataRegistro.email3"  type="text" class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>

                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding"> CLASIFICACION:</label>
                                <div class="input-group col-xs-12 no-padding">
                                    <select  v-model="clientes.dataRegistro.clasificacion"
                                             class="form-control no-padding"
                                             data-live-search="true">
                                        <option value="1">USUARIO</option>
                                        <option value="1">FLOTA</option>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">CONTACTO:</label>
                                <div class="input-group col-xs-12">
                                    <input  v-model="clientes.dataRegistro.contacto" type="text" class="form-control"
                                            aria-describedby="basic-addon1"
                                            value="" placeholder="">
                                </div>
                            </div>



                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">VENDEDOR:</label>
                                <div class="input-group col-xs-12">
                                    <input v-model="clientes.dataRegistro.vendedor"  type=text class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">ESTADO:</label>
                                <div class="input-group col-xs-12">
                                    <input disabled v-model="clientes.dataRegistro.estado"
                                           type='text' class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">CONDICION:</label>
                                <div class="input-group col-xs-12">
                                    <input disabled  v-model="clientes.dataRegistro.condicion" type='text' class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div v-if="clientes.visualDireciones" class="form-group ">
                        <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                          <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                           SUCURSALES
                          </span>

                        </div>
                    </div>
                    <div v-if="clientes.visualDireciones"  class="container-fluid">
                       <table class="table table-bordered table-condensed">
                           <tr>
                               <th>Tipo</th>
                               <th>Direccion</th>
                               <th>Departamento</th>
                               <th>Provincia</th>
                               <th>Distrito</th>
                               <th>Ubigeo</th>


                           </tr>
                           <tr  v-for="(dir, index) in clientes.dataRegistro.direciones">
                               <td v-if="!dir.st">{{dir.tipo}}</td>
                               <td v-if="!dir.st">{{dir.direccion}}</td>
                               <td v-if="!dir.st">{{dir.departamento}}</td>
                               <td v-if="!dir.st">{{dir.provincia}}</td>
                               <td v-if="!dir.st">{{dir.distrito}}</td>
                               <td v-if="!dir.st">{{dir.ubigeo}}</td>

                               <!--td v-if="!dir.st"><button type="button" v-on:click="dir.st=true" class="btn btn-primary"><i class="fa fa-edit"></i></button></td-->

                               <td v-if="dir.st"><input v-model="dir.direccion" type="text" style="width: 100%"></td>
                               <td v-if="dir.st"><select v-bind:id="'depart'+index" @change="cambioDepartamento($event)" class="form-control">
                                       <option v-for="dep in clientes.departamentos" v-bind:value="dep.dep_cod">{{dep.dep_nombre}}</option>
                                   </select></td>
                               <td v-if="dir.st"><select v-bind:id="'prov'+index"  @change="cambioProvincia($event)" class="form-control">
                                       <option v-for="pro in clientes.provincia" v-bind:value="pro.pro_cod">{{pro.pro_nombre}}</option>
                                   </select></td>
                               <td v-if="dir.st"><select  v-bind:id="'dist'+index" v-model="clientes.cod_distrito" @change="cambioDistrito(index)" class="form-control">
                                       <option v-for="dist in clientes.distritos" v-bind:value="dist.dis_codigo">{{dist.dis_nombre}}</option>
                                   </select></td>
                               <td v-if="dir.st">{{dir.ubigeo}}</td>
                               <td v-if="dir.st">{{dir.tipo}}</td>
                               <td v-if="dir.st">{{dir.nomagencia}}</td>
                               <td v-if="dir.st"><button type="button" v-on:click="dir.st=false" class="btn btn-success"><i class="fa fa-check"></i></button></td>
                           </tr>
                       </table>
                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <!-- <a type="submit" id="modal-buscar-empresa-btn-guardar" class="btn btn-primary">
                             Guardar
                         </a>
                         <button type="button" id="modal-buscar-empresa-btn-limpiar" class="btn btn-default">
                             Limpiar
                         </button>-->
                        <button type="submit"  class="btn btn-primary">
                            Agregar
                        </button>
                        <button type="button"  class="btn btn-success"
                                data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
