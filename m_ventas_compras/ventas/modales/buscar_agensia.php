<div class="modal fade" id="modal_buscar_Agencia_transporte" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Buscar Agencia de Transporte</h3>
                </div>
            </div>

            <div class="modal-body  no-border">
                <form action="#">
                    <div class="container-fluid">

                        <div class="table-responsive form-group col-xs-12 no-padding">
                            <table id="table-agencia-transporte" class="table table-striped table-bordered table-hover">
                                <thead class="bg-head-table">
                                <tr>

                                    <th class="text-left">RAZON SOCIAL</th>
                                    <th class="text-left">RUC</th>
                                    <th class="text-left">TELEFONO</th>
                                    <th class="text-left">DIRECCION</th>
                                    <th class="text-left">OPCION</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="(agen, index) in agenciaTansporte.lista">

                                    <td class="text-left">
                                        <label>{{agen.razon_social}}</label>
                                    </td>
                                    <td class="text-left">
                                        <div id="emp_num_ruc" >{{agen.ruc}}</div>
                                    </td>
                                    <td class="text-left">{{agen.telefono}}</td>
                                    <td id="emp-dir" class="text-left">{{agen.direccion}}</td>

                                    <td class="text-center">
                                        <button v-on:click="setSelecteditemAgencia(index)" class="btn btn-sm btn-success fa fa-check btn-selector-cliente"
                                                title="Anadir item" data-dismiss="modal"></button>

                                    </td>
                                </tr>

                                </tbody>

                            </table>
                        </div>

                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <!-- <a type="submit" id="modal-buscar-empresa-btn-guardar" class="btn btn-primary">
                             Guardar
                         </a>
                         <button type="button" id="modal-buscar-empresa-btn-limpiar" class="btn btn-default">
                             Limpiar
                         </button>-->
                        <button type="button"  class="btn btn-primary"
                                data-toggle="modal" data-target="#modal_agregar_agencia_transporte">
                            Agregar
                        </button>
                        <button type="button" class="btn btn-danger"
                                data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

