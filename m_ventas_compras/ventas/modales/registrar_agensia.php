<div class="modal fade" id="modal_agregar_agencia_transporte" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Registrar Agencia De Transporte</h3>
                </div>
            </div>

            <div class="modal-body  no-border">
                <form action="#" v-on:submit.prevent="registrarAgensiaTransporte()">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                <label class="col-xs-12 no-padding">RUC:</label>

                                <div class="input-group col-xs-12 no-padding">
                                    <input required v-model="agenciaTansporte.dataRegistro.ruc"  type="text" class="form-control">
                                    <span class="input-group-btn">
                                                <button v-on:click="consultaRucAgensia()" type="button" class="btn btn-primary">
                                                    <i class="fa fa-search"></i></button>
                                            </span>
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                <label class="col-xs-12 no-padding">NOMBRE / RAZON SOCIAL :</label>
                                <div class="input-group col-xs-12">
                                    <input required disabled v-model="agenciaTansporte.dataRegistro.nombresocial"  type="text" class="form-control input-number"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">ESTADO:</label>
                                <div class="input-group col-xs-12">
                                    <input  disabled v-model="agenciaTansporte.dataRegistro.estado"  type='text' class="form-control"
                                            aria-describedby="basic-addon1"
                                            value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                <label class="col-xs-12 no-padding">CONDICION:</label>
                                <div class="input-group col-xs-12">
                                    <input required disabled  v-model="agenciaTansporte.dataRegistro.condicion" type='text' class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-5 col-sm-6 col-md-8">
                                <label class="col-xs-12 no-padding">DIRECCION FISCAL:</label>
                                <div class="input-group col-xs-12">
                                    <input required v-model="agenciaTansporte.dataRegistro.direccion"  type="text" class="form-control"
                                           aria-describedby="basic-addon1"
                                           value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                <label class="col-xs-12 no-padding">TELEFONO:</label>
                                <div class="input-group col-xs-12">
                                    <input  v-model="agenciaTansporte.dataRegistro.telefono" type="text" class="form-control input-number"
                                            aria-describedby="basic-addon1"
                                            value="" placeholder="">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <!-- <a type="submit" id="modal-buscar-empresa-btn-guardar" class="btn btn-primary">
                             Guardar
                         </a>
                         <button type="button" id="modal-buscar-empresa-btn-limpiar" class="btn btn-default">
                             Limpiar
                         </button>-->
                        <button type="submit"  class="btn btn-primary">
                            Agregar
                        </button>
                        <button type="button"  class="btn btn-success"
                                data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>