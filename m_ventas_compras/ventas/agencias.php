<?php
$indexRuta=1;
require '../conexion/Conexion.php';
require '../model/TipoPago.php';
$tipoPago = new TipoPago();
$listaPa= $tipoPago->lista();
/*$listaTemTP = [];

foreach ($listaPa as $item){
    $listaTemTP []= $item;
}*/


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <script src="../aConfig/plugins/sweetalert2/sweetalert2.min.css"></script>

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Agencia de Transporte</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <button onclick="MODALES.limpiarDataRegisteragencia()" id="folder_btn_nuevo_folder" class="btn btn-primary" data-toggle="modal" data-target="#modal_agregar_agencia_transporte">
                                            <i class="fa fa-plus "></i> Nuevo
                                        </button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div id="conten-modales">
                                    <?php include 'modales/registrar_agensia.php' ?>
                                    <table id="table-agencia-transporte" class="table table-striped table-bordered table-hover">
                                        <thead class="bg-head-table">
                                        <tr style="background-color: #007ac3; color: white">
                                            <th   style="border-right-color: #007ac3" class="text-center"></th>
                                            <th   style="border-right-color: #007ac3" class="text-center">RAZON SOCIAL</th>
                                            <th   style="border-right-color: #007ac3" class="text-center">RUC</th>
                                            <th  style="border-right-color: #007ac3"  class="text-center">TELEFONO</th>
                                            <th  style="border-right-color: #007ac3"  class="text-center">DIRECCION</th>
                                            <th class="text-center">OPCION</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr v-for="(agen, index) in agenciaTansporte.lista">
                                            <td class="text-center" style="color: white">{{index+1}}</td>
                                            <td class="text-center">
                                                <label>{{agen.razon_social}}</label>
                                            </td>
                                            <td class="text-center">
                                                <div id="emp_num_ruc" >{{agen.ruc}}</div>
                                            </td>
                                            <td class="text-center">{{agen.telefono}}</td>
                                            <td id="emp-dir" class="text-center">{{agen.direccion}}</td>

                                            <td class="text-center">
                                                <button data-toggle="modal" data-target="#modal_edit_agencia_transporte" v-on:click="selectItemAgencia(index)" class="btn btn-sm btn-info fa fa-edit btn-selector-cliente"
                                                        title="Anadir item" ></button>

                                            </td>
                                        </tr>

                                        </tbody>

                                    </table>

                                    <div class="modal fade" id="modal_edit_agencia_transporte" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog" role="document" style="width: 60%;">
                                            <div class="modal-content">
                                                <div class="modal-header no-border no-padding">
                                                    <div class="modal-header text-center color-modal-header">
                                                        <h3 class="modal-title">Actualizar Agensia De Transporte</h3>
                                                    </div>
                                                </div>

                                                <div class="modal-body  no-border">
                                                    <form action="#" v-on:submit.prevent="actualizarAgensiaTransporte()">
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="form-group col-xs-7 col-sm-7 col-md-3">
                                                                    <label class="col-xs-12 no-padding">RUC:</label>

                                                                    <div class="input-group col-xs-12 no-padding">
                                                                        <input required v-model="agenciaTansporte.dataRegistro.ruc"  type="text" class="form-control">
                                                                        <span class="input-group-btn">
                                                <button v-on:click="consultaRucAgensia()" type="button" class="btn btn-primary">
                                                    <i class="fa fa-search"></i></button>
                                            </span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                                                    <label class="col-xs-12 no-padding">NOMBRE / RAZON SOCIAL :</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input required disabled v-model="agenciaTansporte.dataRegistro.nombresocial"  type="text" class="form-control input-number"
                                                                               aria-describedby="basic-addon1"
                                                                               value="" placeholder="">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">ESTADO:</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input  disabled v-model="agenciaTansporte.dataRegistro.estado"  type='text' class="form-control"
                                                                                aria-describedby="basic-addon1"
                                                                                value="" placeholder="">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                    <label class="col-xs-12 no-padding">CONDICION:</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input required disabled  v-model="agenciaTansporte.dataRegistro.condicion" type='text' class="form-control"
                                                                               aria-describedby="basic-addon1"
                                                                               value="" placeholder="">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-xs-5 col-sm-6 col-md-8">
                                                                    <label class="col-xs-12 no-padding">DIRECCION FISCAL:</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input required v-model="agenciaTansporte.dataRegistro.direccion"  type="text" class="form-control"
                                                                               aria-describedby="basic-addon1"
                                                                               value="" placeholder="">
                                                                    </div>
                                                                </div>




                                                                <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                                                    <label class="col-xs-12 no-padding">TELEFONO:</label>
                                                                    <div class="input-group col-xs-12">
                                                                        <input  v-model="agenciaTansporte.dataRegistro.telefono" type="text" class="form-control input-number"
                                                                                aria-describedby="basic-addon1"
                                                                                value="" placeholder="">
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                        <div class="container-fluid">
                                                            <hr class="line-frame-modal">
                                                        </div>
                                                        <div class="container-fluid text-right">

                                                            <!-- <a type="submit" id="modal-buscar-empresa-btn-guardar" class="btn btn-primary">
                                                                 Guardar
                                                             </a>
                                                             <button type="button" id="modal-buscar-empresa-btn-limpiar" class="btn btn-default">
                                                                 Limpiar
                                                             </button>-->
                                                            <button type="submit"  class="btn btn-primary">
                                                                Actualizar
                                                            </button>
                                                            <button type="button"  class="btn btn-success"
                                                                    data-dismiss="modal">
                                                                Cerrar
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>




    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>

<script src="../aConfig/scripts/modal_controller.js"></script>

<script type="text/javascript">

    MODALES._data.agenciaTansporte.iniciarDatos= true;
/*
    const APP = new Vue({
        el:"#contenedorprincipal",
        data:{
            cliente:{
                id:0,
                nombre:'',
                ruc:'',
                direccion:'',
                telefono:'',
                atencion:'',
                formapago:'',
                tipopago:'',
                cantidaddias:'',
                agenciatrasporte:'',
                idagencia:'',
                observaciones:''
            },
            listaTipoPago:'',
            listaDetallePago:[],
            producto:{
                predata:{},
                nombre:'',
                precio:0,
                cantidad:'',
                stock:0,
                descuento:'',
                total:0
            },
            index_producto:-1,
            productos:[]
        },
        methods:{
            seleccionarProducnto(index){
                this.index_producto=index;
                console.log('hhhhholaaaaaaaaaaa'+ index)
            },
            eliminarItemProducto(){
                if (this.index_producto!=-1){
                    this.productos.splice(  this.index_producto, 1 );
                    this.index_producto=-1;
                }else{
                    swal('Primero selecione Una fila');
                }

            },
            guardarCotizacion(){
                var datacoti = this.cliente;
                $.ajax({
                    type: "POST",
                    url: "../ajax/Cotizacion/set_new_cotizacion.php",
                    data: datacoti,
                    success: function (resp) {
                        if (isJson(resp)){
                            var json = JSON.parse(resp);
                            if (json.res){
                                $.ajax({
                                    type: "POST",
                                    url: '../ajax/DetalleCotizacion/set_new_data.php',
                                    data: {
                                        idCoti:json.idCoti,
                                        productos: JSON.stringify(APP._data.productos)
                                    },
                                    success: function (response) {
                                        console.log(response)
                                    }
                                });
                            }else{
                                console.log("error")
                            }
                        }else{
                            console.log(resp);
                        }
                    }
                });
            },
            agregarProducto(){
                var indexProd=-1;
                for (var i =0; i<this.productos.length;i++){
                    if (this.producto.predata.produ_id===this.productos[i].id){
                        indexProd=i;

                    }
                }
                if (indexProd==-1){
                    this.productos.push({
                        id:this.producto.predata.produ_id,
                        producto:this.producto.nombre,
                        empresa:this.producto.predata.emp_nombre,
                        marca:this.producto.predata.mar_nombre,
                        sku:this.producto.predata.produ_sku,
                        pais:this.producto.predata.pais_nombre,
                        cantidad:this.producto.cantidad,
                        precio:this.producto.precio,
                        subtotal:this.producto.total

                    });
                }else {
                    this.productos[indexProd].cantidad = parseInt( this.productos[indexProd].cantidad ) +parseInt(this.producto.cantidad);
                }

                $('#tabla-poductos-coti tbody tr').removeClass('bg-success');
                this.producto.predata = {};
                this.producto.nombre ='';
                this.producto.stock =0;
                this.producto.cantidad='';
                this.producto.precio=0;
                this.producto.descuento='';
            },
            setDataCliente(dat){
                this.cliente.id=dat.id;
                this.cliente.nombre=dat.razon_social;
                this.cliente.ruc=dat.ruc;
                this.cliente.direccion=dat.direccion;
                this.cliente.telefono=dat.telefono;
                this.cliente.atencion='';
                this.cliente.formapago='';
                this.cliente.tipopago='';
                this.cliente.cantidaddias='';
                this.cliente.agenciatrasporte='';
                this.cliente.idagencia='';
                this.cliente.observaciones='';
            },
            setDataTransporte(data){
                this.cliente.agenciatrasporte=data.razon_social;
                this.cliente.idagencia=data.id;
            },
            setDataProducto(data){
                this.producto.predata = data;
                this.producto.nombre =data.produ_nombre;
                this.producto.stock =data.cantidad;
                this.producto.cantidad='';
                this.producto.precio=NumeroAleatorio(50,300);
                this.producto.descuento='';


            },
            onformaPago(evt){
                var id = evt.target.value;
                $.ajax({
                    type: "POST",
                    url: '../ajax/PagoDetalle/getData.php',
                    data: {id},
                    success: function (resp) {
                        if (isJson(resp)){
                            APP._data.listaDetallePago = JSON.parse(resp);
                            setTimeout(function () {
                                $('#select-tipoPago').selectpicker('refresh');
                            },100)
                        }else{
                            console.log(resp)
                        }
                    }
                });
            }
        },
        computed:{
            totalpropetido(){
                var desc = (this.producto.descuento +"").length>0?this.producto.descuento:0;
                var cnt = (this.producto.cantidad +"").length>0?this.producto.cantidad:0;
                var toT = this.producto.precio * cnt;
                this.producto.total = toT - ((toT*desc)/100);
                return this.producto.total.toFixed(3);
            },
            totalTabla(){
                setTimeout(function () {
                    $('#tabla-poductos-coti tbody tr').click(function() {
                        //console.log('sasasasasasasasasa')
                        $(this).addClass('bg-success').siblings().removeClass('bg-success');
                    });
                },200);

                var total = 0;
                for (var i=0; i<this.productos.length; i++){
                    total += this.productos[i].subtotal;
                }
                return total.toFixed(3);
            }
        }
    });
*/
    $(document).ready(function() {



        $('#tttttttttttttttt').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
        $('#1111111111111').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });


        $("#input-coti-cliente").focus(function () {
            $("#modal_buscar_cliente").modal('show');
        });
        $("#input-producto-buscar").focus(function () {
            $("#modal_buscar_productos").modal('show');
        });

        $("#input-coti-agendia").focus(function () {
            $("#modal_buscar_Agencia_transporte").modal('show');
        });

        $('#modal_buscar_cliente').on('hidden.bs.modal', function () {
            if (MODALES._data.clientes.isSelected){
                APP.setDataCliente(MODALES._data.clientes.clienteSelected);
            }
        });
        $('#modal_buscar_Agencia_transporte').on('hidden.bs.modal', function () {
            if (MODALES._data.agenciaTansporte.isSelected){
                APP.setDataTransporte(MODALES._data.agenciaTansporte.agenciaSelected);
            }
        });
        $('#modal_buscar_productos').on('hidden.bs.modal', function () {
            if (MODALES._data.productos.isSelected){
                APP.setDataProducto(MODALES._data.productos.productoSelected);
            }
        });
    });

    function NumeroAleatorio(min, max) {
        var num = Math.round(Math.random() * (max - min) + min);
        return num;
    }


</script>

</html>
