<?php
$indexRuta=1;
require '../conexion/Conexion.php';
require '../model/TipoPago.php';
require_once '../model/model.php';

$conexion = (new Conexion())->getConexion();

$tipoPago = new TipoPago();
$categorias= new Categoria('SELECT');

$listaPa= $tipoPago->lista();
$listaTemTP = [];

foreach ($listaPa as $item){
    $listaTemTP []= $item;
}


$listaPrecios = $conexion->query("SELECT
  tipl_id,
  tipl_categoria,
  tipl_estatus
FROM sys_cos_tipo_lista");

$empresas = $conexion->query("SELECT * FROM sys_empresas");
$catego = $conexion->query("SELECT * FROM sys_com_categoria");


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../aConfig/plugins/sweetalert2/sweetalert2.min.css">

    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }

        .fullTsable{
            width: 100%!important;
        }


        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
    </style>

</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Productos</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">

                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">

                                        <button style="display: none" id="temp1" data-toggle="modal" data-target="#modal_gen_exel_inventario" class="btn btn-success">Generar Exel</button>
                                        <button style="display: none" id="temp2"  class="btn btn-success" data-toggle="modal" data-target="#modal_gen_exel_promo" >Generar Exel</button>
                                        <button style="display: none" id="temp3" class="btn btn-success" data-toggle="modal" data-target="#modal_gen_exel_regalos" >Generar Exel</button>

                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id=""  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div id="conten-modales">

                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item ">
                                            <a onclick="relevador('temp1')" class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Inventario de productos</a>
                                        </li>
                                        <li class="nav-item">
                                            <a onclick="relevador('temp2')" class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Productos en promocion</a>
                                        </li>
                                        <li class="nav-item">
                                            <a onclick="relevador('temp3')" class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Productos para Regalo</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">

                                            <table  style="margin-top: 10px;" id="table-productos-almacen" class="table table-striped table-bordered table-hover fullTsable">
                                                <thead class="bg-head-table">
                                                <tr  style="background-color: #007ac3; color: white" >

                                                    <th style="border-right-color: #007ac3" class="text-center">PRODUCTO</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">SKU</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">MEDIDA</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">CATEGORIA</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">MARCA</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">PAIS</th>
                                                    <th  class="text-center">STOCK</th>
                                                    <th  class="text-center">OPCION</th>

                                                </tr>
                                                </thead>
                                                <tbody>



                                                </tbody>

                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                                            <table style="margin-top: 10px;"id="table-productos-promo" class="table table-striped table-bordered table-hover fullTsable">

                                                <thead class="bg-head-table">
                                                <tr  style="background-color: #007ac3; color: white" >

                                                    <th style="border-right-color: #007ac3" class="text-center">EMPRESA</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">PRODUCTO</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">SKU</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">MEDIDA</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">CATEGORIA</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">MARCA</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">STOCK</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">ESTADO</th>
                                                    <th  class="text-center">PRECIO</th>

                                                </tr>
                                                </thead>
                                                <tbody>



                                                </tbody>

                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">

                                            <table style="margin-top: 10px;" id="table-productos-regalos" class="table table-striped table-bordered table-hover fullTsable">
                                                <thead class="bg-head-table" s>
                                                <tr  style="background-color: #007ac3; color: white" >


                                                    <th style="border-right-color: #007ac3" class="text-center">PRODUCTO</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">MARCA</th>
                                                    <th  style="border-right-color: #007ac3" class="text-center">STOCK</th>
                                                    <th style="border-right-color: #007ac3" class="text-center">PRECIO</th>

                                                </tr>
                                                </thead>
                                                <tbody>



                                                </tbody>

                                            </table>
                                        </div>
                                    </div>




                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
    </div>

    <div class="modal fade" id="modal_data_p_s" tabindex="-1" role="dialog" aria-hidden="true"
         style="z-index: 1800; display: none;">
        <div class="modal-dialog modal-xs " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Informacion del producto</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form action="#">
                        <div class="container-fluid">
                            <div class="form-group col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Nombre:</label>
                                <input id="modal-producto-input-nombre-list" class="form-control" type="text" placeholder=""
                                       disabled>
                            </div>

                            <div class="form-group  col-xs-12 no-padding">
                                <table style="width:100%" class="table table-striped">
                                    <tr style="background-color:  rgb(0, 122, 195);">
                                        <th style="border-right-color: rgb(0, 122, 195); color: white" class="text-center">EMPRESA</th>
                                        <th style=" color: white" class="text-center">STOCK</th>
                                    </tr>
                                    <tr v-for="item in listaDinamic">
                                        <td class="text-center">{{item.emp_nombre}}</td>
                                        <td class="text-center">{{item.cantidad}}</td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">


                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_gen_exel_inventario" tabindex="-1" role="dialog" aria-hidden="true"
         style="z-index: 1800; display: none;">
        <div class="modal-dialog modal-xs " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Generar Reporte en Exel</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form action="#">
                        <div class="container-fluid">

                            <div class="form-group col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Empresa:</label>
                                <select id="empres_gen" class="form-control">
                                    <?php
                                    foreach ($empresas as $emp){
                                        echo "<option value='{$emp['emp_id']}'>{$emp['emp_nombre']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Categorias:</label>
                                <select id="catego_gen" class="form-control">
                                    <option  value="0">Todos</option>
                                    <?php
                                    foreach ($catego as $cat){
                                        echo "<option value='{$cat['cat_id']}'>{$cat['cat_nombre']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group  col-xs-12 no-padding">
                               <button type="button" onclick="generarInventarioG()" class="col-xs-12 btn btn-success">Generar</button>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">


                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_gen_exel_promo" tabindex="-1" role="dialog" aria-hidden="true"
         style="z-index: 1800; display: none;">
        <div class="modal-dialog modal-xs " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Generar Reporte en Exel</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form action="#">
                        <div class="container-fluid">

                            <div class="form-group col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Empresa:</label>
                                <select id="empres_prom" class="form-control">
                                    <?php
                                    foreach ($empresas as $emp){
                                        echo "<option value='{$emp['emp_id']}'>{$emp['emp_nombre']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Categorias:</label>
                                <select id="catego_prom" class="form-control">
                                    <option value="0">Todos</option>
                                    <?php
                                    foreach ($catego as $cat){
                                        echo "<option value='{$cat['cat_id']}'>{$cat['cat_nombre']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group  col-xs-12 no-padding">
                               <button onclick="generarInventarioPromo()" type="button" class="col-xs-12 btn btn-success">Generar</button>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">


                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_gen_exel_regalos" tabindex="-1" role="dialog" aria-hidden="true"
         style="z-index: 1800; display: none;">
        <div class="modal-dialog modal-xs " role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Generar Reporte en Exel</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form action="#">
                        <div class="container-fluid">

                            <div class="form-group col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Empresa:</label>
                                <select id="empre_regalo" class="form-control">
                                    <?php
                                    foreach ($empresas as $emp){
                                        echo "<option value='{$emp['emp_id']}'>{$emp['emp_nombre']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group  col-xs-12 no-padding">
                               <button onclick="generarInventarioRegalo()" type="button" class="col-xs-12 btn btn-success">Generar</button>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">


                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <script  type="module" src="../aConfig/scripts/producto.js"></script>
    <script  type="module" src="../aConfig/scripts/sku.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>

        function relevador(idB) {
            if (idB=="temp1"){
                $("#temp1").show();
                $("#temp2").hide();
                $("#temp3").hide();
            }else if (idB=="temp2"){
                $("#temp1").hide();
                $("#temp2").show();
                $("#temp3").hide();
            }else if (idB=="temp3"){
                $("#temp1").hide();
                $("#temp2").hide();
                $("#temp3").show();
            }
        }

        function generarInventarioG() {
            $.toast({
                heading: 'INFORMACION',
                text: "Generado archivo",
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
            $.ajax({
                type: "POST",
                url: "report/report_inventario.php",
                data: {empresa:$("#empres_gen").val(),catego:$("#catego_gen").val()},
                success: function (data) {
                    console.log(data)
                    $.toast({
                        heading: 'EXITOSO',
                        text: "Descargando Archivo",
                        icon: 'success',
                        position: 'top-right',
                        hideAfter: '2500',
                    });
                    setTimeout(function () {
                        window.location.href = 'report/inventario principal.xlsx';
                    },100)
                }
            });

        }
        function generarInventarioPromo() {
            $.toast({
                heading: 'INFORMACION',
                text: "Generado archivo",
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
            $.ajax({
                type: "POST",
                url: "report/report_promo.php",
                data: {empresa:$("#empres_prom").val(),catego:$("#catego_prom").val()},
                success: function (data) {
                    console.log(data)
                    $.toast({
                        heading: 'EXITOSO',
                        text: "Descargando Archivo",
                        icon: 'success',
                        position: 'top-right',
                        hideAfter: '2500',
                    });
                    setTimeout(function () {
                        window.location.href = 'report/inventario promociones.xlsx';
                    },100)
                }
            });

        }
        function generarInventarioRegalo() {
            $.toast({
                heading: 'INFORMACION',
                text: "Generado archivo",
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
            $.ajax({
                type: "POST",
                url: "report/report_regalo.php",
                data: {empresa:$("#empre_regalo").val()},
                success: function (data) {
                    console.log(data)
                    $.toast({
                        heading: 'EXITOSO',
                        text: "Descargando Archivo",
                        icon: 'success',
                        position: 'top-right',
                        hideAfter: '2500',
                    });
                    setTimeout(function () {
                        window.location.href = 'report/inventario regalos.xlsx';
                    },100)
                }
            });

        }
        const app_dimanic_precio= new Vue({
            el:"#modal_data_p_s",
            data:{
                idproducto:0,
                listaDinamic:[]
            },
            methods:{
                getDatainfoProduct(idp){
                    this.idproducto = idp
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Producto/getDataPS_stock.php",
                        data: {idp},
                        success: function (data) {
                            data = JSON.parse(data);
                            console.log(data);
                            $("#modal-producto-input-nombre-list").val(data.nombre)
                            app_dimanic_precio._data.listaDinamic = data.prodStock

                        }
                    });

                }
            }
        });

        $('#select_nomenglatura').change(function () {
            genNombreProducto();
        });
        $('#select_modal_prod_modelo').change(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-pliege").keyup(function () {
            genNombreProducto();
        });
        $("#modal-producto-input-neu-serie").keyup(function () {
            genNombreProducto();
        });
        function getDataPrecios(idpro) {
            app_dimanic_precio.getDatainfoProduct(idpro)
        }
        $( document ).ready(function() {

            $("#table-productos-almacen").DataTable({
                "processing": true,
                "serverSide": true,
                "sAjaxSource": "../ajax/ServerSide/serversideProductosAlmacen.php",
                order: [[3, "desc" ]],

                columnDefs: [
                    {
                        "targets": 0,
                        "data": "coti_id",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[0]+'</span>';

                        }
                    },
                    {
                        "targets": 1,
                        "data": "cli_tdoc",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[1]+'</span>';

                        }
                    },
                    {
                        "targets": 2,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[2]+'</span>';

                        }
                    },
                    {
                        "targets": 3,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[3]+'</span>';

                        }
                    },
                    {
                        "targets": 4,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[4]+'</span>';

                        }
                    },
                    {
                        "targets": 5,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[5]+'</span>';

                        }
                    },
                    {
                        "targets": 6,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<span style="display: block;margin: auto;text-align: center;">'+row[6]+'</span>';

                        }
                    },
                    {
                        "targets": 7,
                        "data": "",
                        "render": function (data, type, row, meta) {

                            return '<button  data-toggle="modal" data-target="#modal_data_p_s" onclick="getDataPrecios('+row[7]+')" class="btn btn-info" style="display: block;margin: auto;text-align: center;"><i class="fa fa-eye"></i></button>';

                        }
                    },
                ]
            });


            $("#select_modal_pro_categoria").change(function () {
                const cate =$("#select_modal_pro_categoria option:selected").text();
                console.log(cate+"<<<<<<<<<sssss")
            });
        });

        function genNombreProducto() {
            const cate =$("#select_modal_pro_categoria option:selected").text();
            var nombrePr=cate;

            //$("#modal-producto-input-nombre").removeAttr('disabled');
            if (cate=="NEUMATICOS"){
                //$("#modal-producto-input-nombre").prop( "disabled", true );
                const  nomen = $("#select_nomenglatura option:selected").text();
                const  tipo = $("#select_opc option:selected").text();
                const ancho =  $("#modal-producto-input-neu-ancho").val();
                nombrePr+=" "+ancho;
                if (nomen=="MILIMETRICA"){
                    const serie= $("#modal-producto-input-neu-serie").val();
                    nombrePr+="/"+serie;
                }
                if (tipo=="RADIAL"){
                    nombrePr+="R";
                }
                const aroNeu=$("#modal-producto-input-neu-aro").val();
                const plieges = $("#modal-producto-input-neu-pliege").val();
                const marca =$("#select_modal_prod_marca option:selected").text();
                const modelo =$("#select_modal_prod_modelo option:selected").text();
                const pais =$("#select_prod_pais option:selected").text();
                nombrePr+="-"+aroNeu+" "+plieges+"PR " +marca +" "+modelo+" "+pais;

            }else if(cate=="ACCESORIOS") {
                nombrePr += " "+$("#modal-producto-input-desc").val().trim();
                nombrePr += " "+$("#modal-producto-input-pn").val().trim();
                nombrePr += " "+$("#select_modal_prod_marca option:selected").text().trim();
                nombrePr += " "+$("#select_prod_pais option:selected").text().trim();
            }
            $("#modal-producto-input-desc").val(nombrePr);
        }
    </script>
    <script>
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>

<script src="../aConfig/scripts/modal_controller.js"></script>

<script type="text/javascript">
    //MODALES._data.productos.url_data='../ajax/Producto/get_all_data2.php';
    //MODALES._data.productos.iniciarDatos= true;
/*
    const APP = new Vue({
        el:"#contenedorprincipal",
        data:{
            cliente:{
                id:0,
                nombre:'',
                ruc:'',
                direccion:'',
                telefono:'',
                atencion:'',
                formapago:'',
                tipopago:'',
                cantidaddias:'',
                agenciatrasporte:'',
                idagencia:'',
                observaciones:''
            },
            listaTipoPago:,
            listaDetallePago:[],
            producto:{
                predata:{},
                nombre:'',
                precio:0,
                cantidad:'',
                stock:0,
                descuento:'',
                total:0
            },
            index_producto:-1,
            productos:[]
        },
        methods:{
            seleccionarProducnto(index){
                this.index_producto=index;
                console.log('hhhhholaaaaaaaaaaa'+ index)
            },
            eliminarItemProducto(){
                if (this.index_producto!=-1){
                    this.productos.splice(  this.index_producto, 1 );
                    this.index_producto=-1;
                }else{
                    swal('Primero selecione Una fila');
                }

            },
            guardarCotizacion(){
                var datacoti = this.cliente;
                $.ajax({
                    type: "POST",
                    url: "../ajax/Cotizacion/set_new_cotizacion.php",
                    data: datacoti,
                    success: function (resp) {
                        if (isJson(resp)){
                            var json = JSON.parse(resp);
                            if (json.res){
                                $.ajax({
                                    type: "POST",
                                    url: '../ajax/DetalleCotizacion/set_new_data.php',
                                    data: {
                                        idCoti:json.idCoti,
                                        productos: JSON.stringify(APP._data.productos)
                                    },
                                    success: function (response) {
                                        console.log(response)
                                    }
                                });
                            }else{
                                console.log("error")
                            }
                        }else{
                            console.log(resp);
                        }
                    }
                });
            },
            agregarProducto(){
                var indexProd=-1;
                for (var i =0; i<this.productos.length;i++){
                    if (this.producto.predata.produ_id===this.productos[i].id){
                        indexProd=i;

                    }
                }
                if (indexProd==-1){
                    this.productos.push({
                        id:this.producto.predata.produ_id,
                        producto:this.producto.nombre,
                        empresa:this.producto.predata.emp_nombre,
                        marca:this.producto.predata.mar_nombre,
                        sku:this.producto.predata.produ_sku,
                        pais:this.producto.predata.pais_nombre,
                        cantidad:this.producto.cantidad,
                        precio:this.producto.precio,
                        subtotal:this.producto.total

                    });
                }else {
                    this.productos[indexProd].cantidad = parseInt( this.productos[indexProd].cantidad ) +parseInt(this.producto.cantidad);
                }

                $('#tabla-poductos-coti tbody tr').removeClass('bg-success');
                this.producto.predata = {};
                this.producto.nombre ='';
                this.producto.stock =0;
                this.producto.cantidad='';
                this.producto.precio=0;
                this.producto.descuento='';
            },
            setDataCliente(dat){
                this.cliente.id=dat.id;
                this.cliente.nombre=dat.razon_social;
                this.cliente.ruc=dat.ruc;
                this.cliente.direccion=dat.direccion;
                this.cliente.telefono=dat.telefono;
                this.cliente.atencion='';
                this.cliente.formapago='';
                this.cliente.tipopago='';
                this.cliente.cantidaddias='';
                this.cliente.agenciatrasporte='';
                this.cliente.idagencia='';
                this.cliente.observaciones='';
            },
            setDataTransporte(data){
                this.cliente.agenciatrasporte=data.razon_social;
                this.cliente.idagencia=data.id;
            },
            setDataProducto(data){
                this.producto.predata = data;
                this.producto.nombre =data.produ_nombre;
                this.producto.stock =data.cantidad;
                this.producto.cantidad='';
                this.producto.precio=NumeroAleatorio(50,300);
                this.producto.descuento='';


            },
            onformaPago(evt){
                var id = evt.target.value;
                $.ajax({
                    type: "POST",
                    url: '../ajax/PagoDetalle/getData.php',
                    data: {id},
                    success: function (resp) {
                        if (isJson(resp)){
                            APP._data.listaDetallePago = JSON.parse(resp);
                            setTimeout(function () {
                                $('#select-tipoPago').selectpicker('refresh');
                            },100)
                        }else{
                            console.log(resp)
                        }
                    }
                });
            }
        },
        computed:{
            totalpropetido(){
                var desc = (this.producto.descuento +"").length>0?this.producto.descuento:0;
                var cnt = (this.producto.cantidad +"").length>0?this.producto.cantidad:0;
                var toT = this.producto.precio * cnt;
                this.producto.total = toT - ((toT*desc)/100);
                return this.producto.total.toFixed(3);
            },
            totalTabla(){
                setTimeout(function () {
                    $('#tabla-poductos-coti tbody tr').click(function() {
                        //console.log('sasasasasasasasasa')
                        $(this).addClass('bg-success').siblings().removeClass('bg-success');
                    });
                },200);

                var total = 0;
                for (var i=0; i<this.productos.length; i++){
                    total += this.productos[i].subtotal;
                }
                return total.toFixed(3);
            }
        }
    });
*/
    $(document).ready(function() {
        $("#table-productos-regalos").DataTable({
            "processing": true,
            "serverSide": true,
            language: {
                url: '../assets/Spanish.json'
            },
            "sAjaxSource": "../ajax/ServerSide/serversideProductosRegalos.php",

            "order": [[ 0, "asc" ]],
            columnDefs: [
                {
                    "targets": 0,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[0]+'</span>';

                    }
                },
                {
                    "targets": 1,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[1]+'</span>';

                    }
                },
                {
                    "targets": 2,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[2]+'</span>';

                    }
                },
                {
                    "targets": 3,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[3]+'</span>';

                    }
                },

            ]

        });
        $("#table-productos-promo").DataTable({
            "processing": true,
            "serverSide": true,
            language: {
                url: '../assets/Spanish.json'
            },
            "sAjaxSource": "../ajax/ServerSide/serversideProductosPromo.php",

            "order": [[ 0, "asc" ]],
            columnDefs: [
                {
                    "targets": 0,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[0]+'</span>';

                    }
                },
                {
                    "targets": 1,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[1]+'</span>';

                    }
                },
                {
                    "targets": 2,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[2]+'</span>';

                    }
                },
                {
                    "targets": 3,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[3]+'</span>';

                    }
                },
                {
                    "targets": 4,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[4]+'</span>';

                    }
                },
                {
                    "targets": 5,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[5]+'</span>';

                    }
                },
                {
                    "targets": 6,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[6]+'</span>';

                    }
                },
                {
                    "targets": 7,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[7]+'</span>';

                    }
                },
                {
                    "targets": 8,
                    "data": "",
                    "render": function (data, type, row, meta) {

                        return '<span style="display: block;margin: auto;text-align: center;">'+row[8]+'</span>';

                    }
                },

            ]

        });


        $('#tttttttttttttttt').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
        $('#1111111111111').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });


        $("#input-coti-cliente").focus(function () {
            $("#modal_buscar_cliente").modal('show');
        });
        $("#input-producto-buscar").focus(function () {
            $("#modal_buscar_productos").modal('show');
        });

        $("#input-coti-agendia").focus(function () {
            $("#modal_buscar_Agencia_transporte").modal('show');
        });

        $('#modal_buscar_cliente').on('hidden.bs.modal', function () {
            if (MODALES._data.clientes.isSelected){
                APP.setDataCliente(MODALES._data.clientes.clienteSelected);
            }
        });
        $('#modal_buscar_Agencia_transporte').on('hidden.bs.modal', function () {
            if (MODALES._data.agenciaTansporte.isSelected){
                APP.setDataTransporte(MODALES._data.agenciaTansporte.agenciaSelected);
            }
        });
        $('#modal_buscar_productos').on('hidden.bs.modal', function () {
            if (MODALES._data.productos.isSelected){
                APP.setDataProducto(MODALES._data.productos.productoSelected);
            }
        });

        setTimeout(function () {
            $("#home-tab").click()
        },100)
    });

    function NumeroAleatorio(min, max) {
        var num = Math.round(Math.random() * (max - min) + min);
        return num;
    }


</script>

</html>
