<?php
$indexRuta=1;
require '../conexion/Conexion.php';
require '../model/TipoPago.php';
require '../model/Cotizacion.php';
$tipoPago = new TipoPago();
$cotizacion= new Cotizacion();


$listaPa= $tipoPago->lista();
$listaTemTP = [];

$isCo='true';
$idCoti=0;
$cotizacionActual='';
foreach ($listaPa as $item){
    $listaTemTP []= $item;
}

if (isset($_GET['view'])){
    $isCo='false';
    $idCoti = $_GET['view'];
}


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <script src="../aConfig/plugins/sweetalert2/sweetalert2.min.css"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <!--suppress JSAnnotator -->
    <script>
        var isRegister = <?php echo $isCo ?>;
        var idCoti = <?php echo $idCoti ?>;

        var idVenta = -1;
        var isventa=false;


        function openPrint() {
            if (idCoti >0){
                window.open("pdf/cotizacion.php?coti="+idCoti)
            }else{
                swall('Primero Guarde la cotizacion')
            }

        }
    </script>
    <style>
        .bg-head-table tr th {
            padding: 0;
        }

        div.dataTables_wrapper div.dataTables_info {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length {
            display: none;
        }

        #table-empresa_filter {
            display: none;
        }




        .well {
            background: none;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #CFF5FF;
        }
        #loader{

            position:absolute;/*agregamos una posición absoluta para que nos permita mover la capa en el espacio del navegador*/

            top:0;/*posicionamiento en Y */

            left:0;/*pocisionamiento en X*/

            z-index:9999; /* Le asignamos la pocisión más alta en el DOM */

            background-color:#ffffff; /* le asignamos un color de fondo */

            width:100%; /* maximo ancho de la pantalla */

            height:100%; /* maxima altura de la pantalla */

            display:block; /* mostramos el layer */

        }

        .preloader {
            width: 70px;
            height: 70px;
            border: 10px solid #eee;
            border-top: 10px solid #666;
            border-radius: 50%;
            animation-name: girar;
            animation-duration: 2s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }
        @keyframes girar {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
    </style>

</head>

<body>

<!--div id="loader">Cargando.........</div-->
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav_ventas.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Resumen de Venta</strong>
                                        </h2>
                                    </div>
                                    <div class="col-lg-6 text-right">
                                        <button type="button" class="btn btn-success"><i
                                                class="fa fa-map"></i> GUIA DE REMISION</button>
                                        <a href="pdf/comprobante.php?coti=<?php echo $_GET['view'] ?>" target="_blank"   type="button" class="btn btn-info"><i
                                                    class="glyphicon glyphicon-print"></i> IMPRIMIR COMPROBANTE
                                        </a>
                                        <!--button onclick="APP. guardarYVenterCotizacion()"  type="button" class="btn btn-primary"><i
                                                class="glyphicon glyphicon-floppy-save"></i> Procesar Venta
                                        </button-->

                                        <!--button type="button" class="btn btn-danger"><i
                                                    class="fa fa-debug"></i> ANULAR TODO</button-->
                                        <span style="color: white">----------------------------</span>
                                        <a id="btn-salir" href="ventas.php" type="reset" class="btn btn-warning"><i
                                                class="glyphicon glyphicon-chevron-left"></i>Salir
                                        </a>
                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>
                                    <!--BOTONES-->
                                    <!--<div class="col-lg-6 text-right">
                                        <a href="new-folder.php" id="folder_btn_nuevo_folder" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Folder
                                        </a>

                                    </div>-->

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="contenedorprincipal"  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                <div>
                                    <form id="frame-new-folder-form">
                                        <input id="fecha"  type="hidden" value="<?php echo date("Y-m-d") ?>" class="form-control  text-center">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                                    <label class="col-xs-12 no-padding">CLIENTE:</label>

                                                    <div class="input-group col-xs-12 no-padding">
                                                        <input disabled v-model="cliente.nombre" id="input-coti-cliente"  type="text" class="form-control" autocomplete="off"
                                                                 aria-describedby="basic-addon1"
                                                                 value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">NUMERO DOCUMENTO:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  v-model="cliente.ruc" type="text" class="form-control"
                                                                 aria-describedby="basic-addon1"
                                                                 value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-5 col-sm-7 col-md-5">
                                                    <label class="col-xs-12 no-padding">DIRECCION FISICA / SUCURSAL:</label>
                                                        <select disabled data-width="100%"  v-model="cliente.idDireccion" class="form-control">
                                                            <option v-for="dir in cliente.direcciones" v-bind:value="dir.id" >{{dir.direccion}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">TELEFONO:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  v-model="cliente.telefono" type="text" class="form-control"
                                                                 aria-describedby="basic-addon1"
                                                                 value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">VENDEDOR:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled v-model="cliente.atencion" type="text" class="form-control"
                                                                 aria-describedby="basic-addon1"
                                                                 value="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding"> FORMA DE PAGO:</label>
                                                    <div class="input-group col-xs-12  no-padding">
                                                        <select disabled  @change="onformaPago($event)"  required v-model="cliente.formapago"
                                                                 class="form-control no-padding"
                                                                data-live-search="true">
                                                            <option v-for="item in listaTipoPago" v-bind:value="item.pag_id">{{item.pag_nombre}}</option>
                                                        </select>
                                                        <!--span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                                    data-target="#modal_incoterm">
                                                                <i class="fa fa-plus"></i></button>
                                                        </span-->
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding"> TIPO DE PAGO:</label>
                                                    <div class="input-group col-xs-12 no-padding">
                                                        <select disabled id="select-tipoPago" required v-model="cliente.tipopago"
                                                                class="form-control no-padding"
                                                                data-live-search="true">
                                                            <option v-for="dpago in listaDetallePago" v-bind:value="dpago.tip_id">{{dpago.tip_descrip}}</option>
                                                        </select>
                                                        <!--span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                                    data-target="#modal_incoterm">
                                                                <i class="fa fa-plus"></i></button>
                                                        </span-->
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-4 col-sm-4 col-md-1">
                                                    <label class="col-xs-12 no-padding"> MONEDA:</label>
                                                    <div class="input-group col-xs-12 no-padding">
                                                        <select disabled  required v-model="cliente.moneda"
                                                                class="form-control no-padding"
                                                                data-live-search="true">
                                                            <option value="4">SOLES</option>
                                                            <option value="1">DOLARES</option>

                                                        </select>
                                                        <!--span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                                    data-target="#modal_incoterm">
                                                                <i class="fa fa-plus"></i></button>
                                                        </span-->
                                                    </div>
                                                </div>

                                                <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                    <label class="col-xs-12 no-padding">CNT. DIAS:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  v-model="cliente.cantidaddias" type="text" class="form-control input-number"
                                                                 aria-describedby="basic-addon1"
                                                                 value="" placeholder="">
                                                    </div>
                                                </div>

                                                <!--div class="form-group col-xs-6 col-sm-6 col-md-4">
                                                    <label class="col-xs-12 no-padding">AGENCIA DE TRANSPORTE:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input id="input-coti-agendia"  v-model="cliente.agenciatrasporte" type="text" class="form-control"
                                                                 aria-describedby="basic-addon1"
                                                                 value="" placeholder="">
                                                        <span class="input-group-btn">
                                                            <button id="frame-new-btn-add-importador" type="button" class="btn btn-primary"
                                                                    data-toggle="modal" data-target="#modal_agregar_agencia_transporte">
                                                                <i class="fa fa-plus"></i></button>
                                                        </span>
                                                    </div>
                                                </div-->

                                                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                                    <label class="col-xs-12 no-padding">OBSERVACIONES:</label>
                                                    <div class="input-group col-xs-12">
                                                        <textarea disabled v-model="cliente.observaciones" style="height: 50px; resize: none;"  class="form-control"></textarea>

                                                    </div>
                                                </div>
                                        </div>
                                            <div  style="width: 100%; text-align: center;">
                                                <span style="margin-right: 50px"><strong>Venta: </strong>{{tasaCambio.venta}}</span>
                                                <span><strong>Compra: </strong>{{tasaCambio.compra}}</span>
                                            </div>

                                            <div class="form-group ">
                                                <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    Datos de venta<!--Padding is optional-->
                                                  </span>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <table id="tabla-poductos-coti" class="table table-bordered table-hover" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 50px;">#</th>
                                                        <th class="col-sm-2" scope="col">EMPRESA</th>
                                                        <th class="col-sm-3">PROODUCTO</th>
                                                        <th class="col-sm-1">MARCA</th>
                                                        <th class="col-sm-1">SKU</th>
                                                        <th class="col-sm-1">PAIS</th>
                                                        <th class="col-sm-1">CANTIDAD</th>
                                                        <th class="col-sm-1">PRECIO</th>
                                                        <th class="col-sm-1">DESCUENTO</th>

                                                        <th class="col-sm-1">IMPORTE</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr v-for="(prod, index ) in productos" v-on:click="seleccionarProducnto(index)">
                                                        <td>{{index+1}}</td>
                                                        <td>{{prod.empresa}}</td>
                                                        <td>{{prod.producto}}</td>
                                                        <td>{{prod.marca}}</td>
                                                        <td>{{prod.sku}}</td>
                                                        <td>{{prod.pais}}</td>
                                                        <td  class="text-center">{{prod.cantidad}}</td>
                                                        <td  class="text-center">{{simboleModena}} {{prod.precio}}</td>
                                                        <td  class="text-center">{{prod.descuento}}%</td>
                                                        <td  class="text-center" style="background-color: #feffcb">{{simboleModena}} {{parseInt(prod.subtotal).toFixed(2)}}</td>
                                                    </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="9" style="text-align: right; font-weight: bold; font-size: 18px">Sub. Total</td>
                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{subTotal.toFixed(2)}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="9" style="text-align: right; font-weight: bold; font-size: 18px">IGV</td>
                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{igv.toFixed(2)}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="9" style="text-align: right; font-weight: bold; font-size: 18px">Descuento</td>
                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{descuento}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="9" style="text-align: right; font-weight: bold; font-size: 18px">Total</td>
                                                            <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{totalTabla}}</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        <div hidden class="form-group text-right">


                                            <div class="col-md-12 no-padding">
                                                <div class="refresh-table">
                                                    <button  type="submit" class="btn btn-primary">Guardar</button>
                                                    <button id="frame-new-folder-btn-limpiar" type="button" class="btn btn-default">Limpiar</button>
                                                    <a href="import.php" id="frame-new-folder-btn-back" type="reset" class="btn btn-warning"><i
                                                            class="glyphicon glyphicon-chevron-left"></i>Salir
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>


                                <div class="form-group ">
                                    <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                   Factura Por Empresa<!--Padding is optional-->
                                                  </span>

                                    </div>
                                </div>


                                <div id=""  class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li v-for="(item, index) in datosProEmpre" v-bind:class=" (index==0)? 'nav-item active':'nav-item' ">
                                            <a class="nav-link" v-bind:id="'idtab'+index+2" data-toggle="tab" v-bind:aria-controls="'#idtab'+index"  v-bind:href="'#idtab'+index" role="tab"  aria-expanded="true" v-bind:aria-selected="(index==0)? 'true':'false'">{{item.nomempre}}</a>
                                        </li>


                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div v-for="(item, index) in datosProEmpre" v-bind:class=" (index==0)? 'tab-pane fade active in':'tab-pane fade' " v-bind:id="'idtab'+index" role="tabpanel" v-bind:aria-labelledby="'idtab'+index">
                                            <div style="margin-top: 20px;" class="col-md-12">
                                                <div class="form-group col-xs-3 col-sm-3 col-md-3">
                                                    <label class="col-xs-12 no-padding">S - N:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  type="text" class="form-control input-number"
                                                               aria-describedby="basic-addon1"
                                                               v-bind:value="item.serie +' - '+item.numero" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group col-xs-2 col-sm-2 col-md-2">
                                                    <label class="col-xs-12 no-padding">FECHA-HORA:</label>
                                                    <div class="input-group col-xs-12">
                                                        <input disabled  type="text" class="form-control input-number"
                                                               aria-describedby="basic-addon1"
                                                               v-bind:value="item.fecha +' '+item.hora" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group col-xs-7 col-sm-7 col-md-7">
                                                    <label class="col-md-12" style="color: white">.</label>
                                                    <div class="col-md-12 text-right">
                                                        <!--button v-on:click="setDataNotaElectronica(item)" data-toggle="modal" data-target="#modal-gen-nota-electronica" class="btn btn-primary col-md-4">NOTA ELECTRONICA</button-->
                                                        <button v-on:click="setAnulacionelectronica(item)"  data-toggle="modal" data-target="#modal-gen-nota-electronica-anulacion" class="btn btn-danger col-md-4">ANULAR</button>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <table id="tabla-poductos-coti" class="table table-bordered table-hover" style="width:100%">
                                                    <thead>
                                                    <tr>

                                                        <th class="col-sm-3">PROODUCTO</th>
                                                        <th class="col-sm-1">MARCA</th>
                                                        <th class="col-sm-1">SKU</th>
                                                        <th class="col-sm-1">PAIS</th>
                                                        <th class="col-sm-1">CANTIDAD</th>
                                                        <th class="col-sm-1">PRECIO</th>
                                                        <th class="col-sm-1">IMPORTE</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr v-for="(pro, ix) in item.productosVe">
                                                        <td>{{pro.producto_desc}}</td>
                                                        <td>{{pro.mar_nombre}}</td>
                                                        <td>{{pro.produ_sku}}</td>
                                                        <td>{{pro.pais_nombre}}</td>
                                                        <td  class="text-center">{{pro.cantidad}}</td>
                                                        <td  class="text-center">{{simboleModena}} {{pro.precio_unitario}}</td>
                                                        <td  class="text-center" style="background-color: #feffcb">{{simboleModena}} {{(pro.precio_unitario*pro.cantidad).toFixed(2)}}</td>
                                                    </tr>
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="6" style="text-align: right; font-weight: bold; font-size: 18px">Sub. Total</td>
                                                        <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{item.subtotal.toFixed(2)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6" style="text-align: right; font-weight: bold; font-size: 18px">IGV</td>
                                                        <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{item.igv.toFixed(2)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6" style="text-align: right; font-weight: bold; font-size: 18px">Descuento</td>
                                                        <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}}  0.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6" style="text-align: right; font-weight: bold; font-size: 18px">Total</td>
                                                        <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{item.totalpro.toFixed(2)}}</td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>

                                        </div>

                                    </div>



                                </div>


                                <div class="modal fade" id="modal-gen-nota-electronica-anulacion" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog" role="document" style="width: 80%;">
                                        <div class="modal-content">
                                            <div class="modal-header no-border no-padding">
                                                <div class="modal-header text-center color-modal-header">
                                                    <h3 class="modal-title">NOTA ELECTRONICA</h3>
                                                </div>
                                            </div>

                                            <div class="modal-body  no-border">
                                                <form action="#">
                                                    <div class="container-fluid">
                                                       <form>
                                                           <div class="row">
                                                               <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                   <label class="col-xs-12 no-padding">DOCUMENTO:</label>
                                                                   <div class="input-group col-xs-12">

                                                                       <select v-model="dataNE.tipo" class="form-control">
                                                                           <option selected value="1">NOTA DE CREDITO</option>
                                                                           <!--option value="2">NOTA DE DEBITO</option-->
                                                                       </select>
                                                                   </div>
                                                               </div>
                                                               <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                   <label class="col-xs-12 no-padding">S -N:</label>
                                                                   <div class="input-group col-xs-12">
                                                                       <input v-model="dataNE.serieNumero" disabled class="form-control">
                                                                   </div>
                                                               </div>
                                                               <div class="form-group col-xs-4 col-sm-4 col-md-2">
                                                                   <label class="col-xs-12 no-padding">FECHA:</label>
                                                                   <div class="input-group col-xs-12">
                                                                       <input v-model="dataNE.fecha" disabled value="" class="form-control">
                                                                   </div>
                                                               </div>

                                                               <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                                                   <label class="col-xs-12 no-padding">MOTIVO:</label>
                                                                   <div class="input-group col-xs-12">
                                                                       <select v-model="dataNE.motivo" class="form-control" id="" name="select_motivo">
                                                                           <option value="1">ANULACION DE LA OPERACION</option>
                                                                           <option value="2">ANULACION POR ERROR EN EL RUC</option>
                                                                           <!--option value="8">BONIFICACION</option>
                                                                           <option value="3">CORRECCION POR ERROR EN LA DESCRIPCION</option>
                                                                           <option value="4">DESCUENTO GLOBAL</option>
                                                                           <option value="5">DESCUENTO POR ITEM</option>
                                                                           <option value="7">DEVOLUCION POR ITEM</option>
                                                                           <option value="6">DEVOLUCION TOTAL</option>
                                                                           <option value="9">DISMINUCION EN EL VALOR</option-->
                                                                       </select>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                           <div class="row">
                                                               <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                   <label class="col-xs-12 no-padding">DOCUMENTO RELACIONADO:</label>
                                                                   <div class="input-group col-xs-12">
                                                                       <input v-model="dataNE.documentoRelacionado" disabled class="form-control col-xs-6">
                                                                   </div>
                                                               </div>
                                                               <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                                                   <label class="col-xs-12 no-padding">TOTAL DEL DOCUMENTO:</label>
                                                                   <div class="input-group col-xs-12">
                                                                       <input v-model="dataNE.totalDocumento" disabled class="form-control col-xs-6">
                                                                   </div>
                                                               </div>
                                                               <div class="form-group col-xs-4 col-sm-4 col-md-5">
                                                                   <label class="col-xs-12 no-padding">DESCRIPCION O SUSTENTO:</label>
                                                                   <div class="input-group col-xs-12">
                                                                       <input   class="form-control col-xs-6">
                                                                   </div>
                                                               </div>
                                                           </div>
                                                           <!--div class="row">
                                                               <div class="form-group">
                                                                   <div class="form-group col-xs-7 col-sm-7 col-md-4">
                                                                       <label class="col-xs-12 no-padding">BUSCAR PRODUCTO:</label>
                                                                       <div class="input-group col-xs-12">
                                                                           <input v-model="producto.nombre" id="input-producto-buscar"  type="text" class="form-control"
                                                                                  aria-describedby="basic-addon1"
                                                                                  value="" placeholder="">
                                                                       </div>
                                                                   </div>
                                                                   <div class="form-group col-xs-4 col-sm-3 col-md-1">
                                                                       <label class="col-xs-12 no-padding">DISPONIBLE:</label>
                                                                       <div class="input-group col-xs-12">
                                                                           <input v-model="producto.stock" style="text-align: center" disabled  type="text" class="form-control"
                                                                                  aria-describedby="basic-addon1"
                                                                                  value="" placeholder="">
                                                                       </div>
                                                                   </div>
                                                                   <div class="form-group col-xs-4 col-sm-3 col-md-1">
                                                                       <label class="col-xs-12 no-padding">PRECIO/UND:</label>
                                                                       <div class="input-group col-xs-12">
                                                                           <input v-model="producto.precio" style="text-align: center" disabled  type="text" class="form-control"
                                                                                  aria-describedby="basic-addon1"
                                                                                  value="" placeholder="">
                                                                       </div>
                                                                   </div>
                                                                   <div class="form-group col-xs-4 col-sm-3 col-md-2">
                                                                       <label class="col-xs-12 no-padding">CANTIDAD:</label>
                                                                       <div class="input-group col-xs-12">
                                                                           <input v-model="producto.cantidad" style="text-align: center"   type="text" class="form-control"
                                                                                  aria-describedby="basic-addon1"
                                                                                  value="" placeholder="">
                                                                       </div>
                                                                   </div>
                                                                   <div class="form-group col-xs-4 col-sm-3 col-md-2">
                                                                       <label class="col-xs-12 no-padding">TOTAL: {{simboleModena}}</label>
                                                                       <div class="input-group col-xs-12">
                                                                           <input v-model="totalpropetido" style="text-align: center" disabled  type="text" class="form-control"
                                                                                  aria-describedby="basic-addon1"
                                                                                  value="" placeholder="">
                                                                       </div>
                                                                   </div>
                                                                   <div class="form-group col-xs-7 col-sm-7 col-md-1">
                                                                       <label style="color: white" class="col-xs-12 no-padding"></label>
                                                                       <div class="input-group col-xs-12">
                                                                           <button style="margin-bottom: 5px;"  type="button"  class="btn btn-primary">GUARDAR</button>
                                                                           <button type="button"  class="btn btn-danger">ELIMINAR</button>

                                                                       </div>
                                                                   </div>
                                                               </div>
                                                           </div-->

                                                           <div class="row">
                                                               <div class="form-group">
                                                                   <table id="tabla-poductos-coti" class="table table-bordered table-hover" style="width:100%">
                                                                       <thead>
                                                                       <tr>
                                                                           <th style="width: 50px;">#</th>
                                                                           <th class="col-sm-2" scope="col">EMPRESA</th>
                                                                           <th class="col-sm-3">PROODUCTO</th>
                                                                           <th class="col-sm-1">MARCA</th>
                                                                           <th class="col-sm-1">SKU</th>
                                                                           <th class="col-sm-1">PAIS</th>
                                                                           <th class="col-sm-1">CANTIDAD</th>
                                                                           <th class="col-sm-1">PRECIO</th>
                                                                           <th class="col-sm-1">IMPORTE</th>
                                                                       </tr>
                                                                       </thead>
                                                                       <tbody>
                                                                       <tr v-for="(pro, ix) in dataNE.productos">
                                                                           <td>{{ix + 1}}</td>
                                                                           <td></td>
                                                                           <td>{{pro.producto_desc}}</td>
                                                                           <td>{{pro.mar_nombre}}</td>
                                                                           <td>{{pro.produ_sku}}</td>
                                                                           <td>{{pro.pais_nombre}}</td>
                                                                           <td  class="text-center">{{pro.cantidad}}</td>
                                                                           <td  class="text-center">{{simboleModena}} {{pro.precio_unitario}}</td>
                                                                           <td  class="text-center" style="background-color: #feffcb">{{simboleModena}} {{ (pro.cantidad * pro.precio_unitario).toFixed(2)}}</td>
                                                                       </tr>
                                                                       </tbody>
                                                                       <tfoot>
                                                                       <tr>
                                                                           <td colspan="8" style="text-align: right; font-weight: bold; font-size: 18px">Sub. Total</td>
                                                                           <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{dataNE.totalDocumento}}</td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td colspan="8" style="text-align: right; font-weight: bold; font-size: 18px">IGV</td>
                                                                           <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{dataNE.igv}} </td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td colspan="8" style="text-align: right; font-weight: bold; font-size: 18px">Total</td>
                                                                           <td style="text-align: center; font-size: 18px;background-color: #feffcb">{{simboleModena}} {{dataNE.total}}</td>
                                                                       </tr>
                                                                       </tfoot>
                                                                   </table>
                                                               </div>
                                                           </div>

                                                       </form>
                                                    </div>
                                                    <div class="container-fluid">
                                                        <hr class="line-frame-modal">
                                                    </div>
                                                    <div class="container-fluid text-right">

                                                        <button type="button" v-on:click="anularDocumento()" class="btn btn-primary"
                                                               >
                                                            Anular Documento
                                                        </button>
                                                        <button type="button" id="modal-buscar-empresa-btn-cerrar" class="btn btn-danger"
                                                                data-dismiss="modal">
                                                            Cancelar
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>


    <div id="conten-modales">



    </div>

    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="../aConfig/plugins/sweetalert2/vue-swal.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
       //parseFloat()
        function isJson(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
        function removeItemFromArr ( arr, index ) {

            arr.splice( index, 1 );
        }


    </script>



</body>

<script src="../aConfig/scripts/modal_controller.js"></script>
<script type="text/javascript">

   // MODALES._data.clientes.iniciarDatos= true;
    /*MODALES._data.agenciaTansporte.iniciarDatos= true;
    MODALES._data.productos.iniciarDatos= true;*/



   var  fecha =  new Date();
    const APP = new Vue({
        el:"#contenedorprincipal",
        data:{
            idcotizacion:0,
            clasificacion:[],
            funExeSavaCoti:function () {

            },
            cliente:{
                id:0,
                nombre:'',
                ruc:'',
                direcciones:[],
                idDireccion:'',
                telefono:'',
                atencion:'',
                formapago:'',
                moneda:4,
                tipopago:'',
                cantidaddias:'',
                agenciatrasporte:'',
                idagencia:'',
                fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate(),
                observaciones:''
            },
            listaTipoPago:'',
            listaDetallePago:[],
            tasaCambio:{},
            datosProEmpre:[],
            igv:0,
            descuento:0,
            subTotal:0,
            totalcoti:0,
            producto:{
                predata:{},
                nombre:'',
                precio:0,
                cantidad:'',
                stock:0,
                descuento:0,
                total:0
            },
            index_producto:-1,
            productos:[],
            dataNE:{
                idVenta:0,
                tipo:1,
                igv:0,
                total:0,
                serieNumero:"",
                fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate(),
                motivo:1,
                documentoRelacionado:"",
                totalDocumento:0,
                productos:[]
            }
        },
        methods:{
            anularDocumento(){
                swal({
                    title: "¿Desea anular este documento?",
                    text: "",
                    icon: "warning",
                    dangerMode: false,
                    buttons: ["NO", "SI"],
                })
                    .then((ressss) => {
                            console.log(ressss);
                            if (ressss){
                                swal("Documento anulado", "", "success");
                                $('#modal-gen-nota-electronica-anulacion').modal('hide');
                            }
                    });
            },
            setAnulacionelectronica(venta){
                console.log(venta);
                this.dataNE.idVenta=venta.id;
                this.dataNE.documentoRelacionado=venta.serie + " - "+venta.numero;
                this.dataNE.totalDocumento=venta.monto;
                this.dataNE.productos=venta.productosVe;
                this.dataNE.igv= (venta.monto*0.18).toFixed(2);
                this.dataNE.total= ( parseFloat( this.dataNE.igv) +  parseFloat( this.dataNE.totalDocumento)).toFixed(2);
            },
            setDataNotaElectronica(dataobj){
                console.log(dataobj)
                APP._data.dataNE.tipo=1;
                APP._data.dataNE.serieNumero= (dataobj.tipo_venta==1?"F":"B")+"0001 - 000023";
                APP._data.dataNE.motivo=1;
                APP._data.dataNE.documentoRelacionado=dataobj.serie + " - "+ dataobj.numero;
                APP._data.dataNE.totalDocumento=dataobj.monto;
                APP._data.dataNE.productos=dataobj;
            },
            realizarVenta(){
                var d = new Date();
                $.ajax({
                type: "POST",
                url: "../ajax/Ventas/set_new_venta.php",
                data: {
                    fecha:$("#fecha").val()
                    ,hora: d.getHours()+":"+d.getMinutes(),
                    moneda:APP._data.cliente.moneda,
                    idcoti:idCoti
                    ,idpago:APP._data.cliente.formapago
                    ,idcliente:APP._data.cliente.id
                    ,tipoventa:(APP._data.cliente.ruc +"").length==8?2:1
                },
                success: function (data) {
                    if (isJson(data)){
                        var jso= JSON.parse(data);
                        if (jso.res){
                            isventa=true;
                            idVenta = jso.idVenta;
                            swal("Venta Realizada", {
                                icon: "success",
                            });
                        }else{
                            swall('No se pudo Realizar la venta');
                        }
                    }else{
                        console.log(data);
                        swall('no se pudo realizar la venta, Error en el servidor');
                    }
                }
            });
            },
            cargarDatoCotizacion(){

                $.ajax({
                    type: "POST",
                    url: '../ajax/Cotizacion/getDataCotizacion.php',
                    data:{
                        idCoti
                    },
                    success: function (data) {
                        //console.log(data)
                        if (isJson(data)){
                            var json = JSON.parse(data);
                            
                            APP._data.cliente.id=json.id_cliente;
                            APP._data.cliente.nombre=json.coti_razon;
                            APP._data.cliente.ruc=json.coti_ruc;
                            APP._data.cliente.idDireccion=json.id_direccion;
                            APP._data.cliente.telefono=json.coti_telf;
                            APP._data.cliente.atencion=json.coti_ate;
                            APP._data.cliente.formapago=json.formapago;
                            APP._data.cliente.tipopago=json.coti_tp_pago;
                            APP._data.cliente.cantidaddias=json.coti_dias_credito;
                            APP._data.cliente.agenciatrasporte=json.agencia;
                            APP._data.cliente.idagencia=json.id_agencia;
                            APP._data.cliente.fecha=json.coti_fecha;
                            APP._data.cliente.observaciones=json.coti_obs;
                            APP._data.idcotizacion = json.coti_id;

                            json.productos.forEach(function (prod) {
                                var totalT =parseInt(prod.cantidad) *parseInt(prod.precio_unitario);
                                APP._data.productos.push({
                                    id:prod.produ_id,
                                    producto:prod.produ_nombre,
                                    empresa:prod.emp_nombre,
                                    idempresa:prod.emp_id,
                                    marca:prod.mar_nombre,
                                    sku:prod.produ_sku,
                                    pais:prod.pais_nombre,
                                    cantidad:prod.cantidad,
                                    descuento:prod.descuento,
                                    precio:prod.precio_unitario,
                                    subtotal:totalT-((totalT* prod.descuento)/100)

                                });
                            });

                            $.ajax({
                                type: "POST",
                                url: "../ajax/Clientes/getAllDirecciones.php",
                                data: {idc:APP._data.cliente.id},
                                success: function (data) {
                                    if (isJson(data)){
                                        var direc =JSON.parse(data);
                                        APP._data.cliente.direcciones = direc;
                                    }else{
                                        console.log(data);
                                    }

                                }
                            });


                           /* setTimeout(function () {
                                $('select').selectpicker('refresh');
                            },100);*/
                            $.ajax({
                                type: "POST",
                                url: '../ajax/PagoDetalle/getData.php',
                                data: {id:json.formapago},
                                success: function (resp) {
                                   // console.log(resp)
                                    if (isJson(resp)){
                                        APP._data.listaDetallePago = JSON.parse(resp);
                                       /* setTimeout(function () {
                                            $('#select-tipoPago').selectpicker('refresh');
                                        },100)*/
                                    }else{
                                        console.log(resp)
                                    }
                                }
                            });

                            //console.log(json)
                        }else{
                            console.log(data)
                        }
                    }
                });
            },
            seleccionarProducnto(index){
                this.index_producto=index;
                //console.log('hhhhholaaaaaaaaaaa'+ index)
            },
            eliminarItemProducto(){
                if (this.index_producto!=-1){
                    this.productos.splice(  this.index_producto, 1 );
                    this.index_producto=-1;
                }else{
                   swal('Primero selecione Una fila');
                }

            },
            guardarCotizacion(){
                var datacoti = {... APP._data.cliente};
                datacoti.direcciones='';
                datacoti.idcotizacion = idCoti;
                datacoti.tasacambio= this.tasaCambio.idc;
                datacoti.totalcoti =this.totalcoti;
                console.log(datacoti)
                $.ajax({
                    type: "POST",
                    url: isRegister? "../ajax/Cotizacion/set_new_cotizacion.php":'../ajax/Cotizacion/udp_cotizacion.php',
                    data: datacoti,
                    success: function (resp) {
                        if (isJson(resp)){
                            var json = JSON.parse(resp);
                            if (json.res){
                                $.ajax({
                                    type: "POST",
                                    url: isRegister?'../ajax/DetalleCotizacion/set_new_data.php':'../ajax/DetalleCotizacion/udp_data.php',
                                    data: {
                                        idCoti:json.idCoti,
                                        productos: JSON.stringify(APP._data.productos)
                                    },
                                    success: function (response) {
                                        console.log(response)
                                        if (response){

                                            swal("Cotizacion Guardada", "", "success");
                                            idCoti=json.idCoti;
                                            isRegister=false;
                                        }else{
                                            swal('Hubo un problema al guardar los productos de la cotización')
                                        }
                                    }
                                });
                            }else{
                                console.log("error")
                                console.log(resp)
                                swall('Error, no de pudo guardar la cotizacion');
                            }
                        }else{
                            console.log(resp);
                        }
                    }
                });
            },
            guardarYVenterCotizacion(){
                var datacoti = {... APP._data.cliente};
                datacoti.direcciones='';
                datacoti.idcotizacion = idCoti;
                datacoti.tasacambio= this.tasaCambio.idc;
                datacoti.totalcoti =this.totalcoti;
                console.log(datacoti)

                swal({
                    title: "¿Desea realizar la venta?",
                    text: "",
                    icon: "warning",
                    dangerMode: false,
                    buttons: ["NO", "SI"],
                })
                    .then((ressss) => {
                        console.log(ressss);
                        if (ressss){
                            $.ajax({
                                type: "POST",
                                url: isRegister? "../ajax/Cotizacion/set_new_cotizacion.php":'../ajax/Cotizacion/udp_cotizacion.php',
                                data: datacoti,
                                success: function (resp) {
                                    if (isJson(resp)){
                                        var json = JSON.parse(resp);
                                        if (json.res){
                                            $.ajax({
                                                type: "POST",
                                                url: isRegister?'../ajax/DetalleCotizacion/set_new_data.php':'../ajax/DetalleCotizacion/udp_data.php',
                                                data: {
                                                    idCoti:json.idCoti,
                                                    productos: JSON.stringify(APP._data.productos)
                                                },
                                                success: function (response) {
                                                    console.log(response)
                                                    if (isJson(response)){
                                                        idCoti=json.idCoti;
                                                        isRegister=false;
                                                        APP.realizarVenta();
                                                    }else{
                                                        swal('Hubo un problema al guardar los productos de la cotización')
                                                    }
                                                }
                                            });
                                        }else{
                                            console.log("error")
                                            console.log(resp)
                                            swall('Error, no de pudo guardar la cotizacion');
                                        }
                                    }else{
                                        console.log(resp);
                                    }
                                }
                            });

                        }

                    });


            },
            agregarProducto(){
                console.log(this.producto.predata);
                if (parseInt(this.producto.predata.cantidad)>=parseInt(this.producto.cantidad)){
                    var indexProd=-1;
                    var cambio = 1;
                    if (this.cliente.moneda==1){
                        cambio= this.tasaCambio.venta;
                    }
                    for (var i =0; i<this.productos.length;i++){
                        if (this.producto.predata.produ_id==this.productos[i].id && this.producto.predata.emp_id ==this.productos[i].idempresa){
                            indexProd=i;

                        }
                    }


                    if (indexProd==-1){
                        this.productos.push({
                            id:this.producto.predata.produ_id,
                            producto:this.producto.nombre,
                            empresa:this.producto.predata.emp_nombre,
                            idempresa:this.producto.predata.emp_id,
                            marca:this.producto.predata.mar_nombre,
                            sku:this.producto.predata.produ_sku,
                            pais:this.producto.predata.pais_nombre,
                            cantidad:this.producto.cantidad,
                            precio:this.producto.precio,
                            descuento:this.producto.descuento.length>0?this.producto.descuento:0,
                            subtotal:this.producto.total

                        });
                    }else {
                        this.productos[indexProd].cantidad = parseInt( this.productos[indexProd].cantidad ) +parseInt(this.producto.cantidad);
                        var precioU = parseInt(this.producto.cantidad)*parseFloat(this.producto.precio);
                        var descuento = precioU*parseFloat( this.productos[indexProd].descuento)/100;
                        this.productos[indexProd].subtotal  = (parseFloat( this.productos[indexProd].subtotal ) +(precioU-descuento)).toFixed(2);
                    }

                    $('#tabla-poductos-coti tbody tr').removeClass('bg-success');
                    this.producto.predata = {};
                    this.producto.nombre ='';
                    this.producto.stock =0;
                    this.producto.cantidad='';
                    this.producto.precio=0;
                    this.producto.descuento='';
                }else{
                    swal('La cantidad sobrepasa el stock actual')
                }

            },
            setDataCliente(dat){
                console.log(dat);
                this.cliente.id=dat.id;
                this.cliente.nombre=dat.razon_social;
                this.cliente.ruc=dat.ruc;
                this.cliente.direccion=dat.direccion;
                this.cliente.telefono=dat.telefono;
               // this.cliente.idDireccion=(typeof dat.direcciones[0]!=='undefined')?dat.direcciones[0].id:'';
                this.cliente.atencion='';
                this.cliente.formapago='';
                this.cliente.tipopago='';
                this.cliente.cantidaddias='';
                this.cliente.agenciatrasporte='';
                this.cliente.idagencia='';
                this.cliente.observaciones='';
            },
            setDataTransporte(data){
                this.cliente.agenciatrasporte=data.razon_social;
                this.cliente.idagencia=data.id;
            },
            setDataProducto(data){
                var cambio = 1;
                if (this.cliente.moneda==1){
                    cambio= this.tasaCambio.venta;
                }
                this.producto.predata = data;
                this.producto.nombre =data.produ_nombre;
                this.producto.stock =data.cantidad;
                this.producto.cantidad='';
                this.producto.precio= (parseFloat( data.precio)/cambio).toFixed(2);
                this.producto.descuento='';


            },
            onformaPago(evt){
                var id = evt.target.value;
                $.ajax({
                    type: "POST",
                    url: '../ajax/PagoDetalle/getData.php',
                    data: {id},
                    success: function (resp) {
                        if (isJson(resp)){
                            APP._data.listaDetallePago = JSON.parse(resp);
                            /*setTimeout(function () {
                                $('#select-tipoPago').selectpicker('refresh');
                            },100)*/
                        }else{
                            console.log(resp)
                        }
                    }
                });
            }
        },
        computed:{
            simboleModena(){
              return this.cliente.moneda==1?'$':'S/.';
            },
            isCredito(){
                this.cliente.cantidaddias="";
                return this.cliente.formapago!=1;
            },
            isDolar(){
                return this.cliente.moneda==1;
            },
            totalpropetido(){
                var desc = (this.producto.descuento +"").length>0?this.producto.descuento:0;
                var cnt = (this.producto.cantidad +"").length>0?this.producto.cantidad:0;
                var toT = this.producto.precio * cnt;
                this.producto.total = toT - ((toT*desc)/100);
                return this.producto.total.toFixed(2);
            },
            totalTabla(){
                setTimeout(function () {
                    $('#tabla-poductos-coti tbody tr').click(function() {
                        //console.log('sasasasasasasasasa')
                        $(this).addClass('bg-success').siblings().removeClass('bg-success');
                    });
                },200);

                var total = 0;
                for (var i=0; i<this.productos.length; i++){
                    total += this.productos[i].subtotal;
                }
                this.subTotal=total;
                this.igv = (total*0.18);
                this.totalcoti=(total+ this.igv).toFixed(2);
                return this.totalcoti;
            }
        }
    });

    function geTasaCambio(){
        $.ajax({
            type: "POST",
            url: "../ajax/CambioMoneda/iniciartipoCambio.php",
            data: {
                fecha:fecha.getFullYear() +'-'+(fecha.getMonth()+1)+'-'+fecha.getDate()
            },
            success: function (dat) {
                if (isJson(dat)){
                    APP._data.tasaCambio= JSON.parse(dat);
                   // swal("Tasa de cambio: "+APP._data.tasaCambio.fecha, "Venta: "+APP._data.tasaCambio.venta+"\nCompra: "+APP._data.tasaCambio.compra)
                }else{
                    console.log(dat);
                }
            }
        });
    }


    function getDataVenta(){
        $.ajax({
            type: "POST",
            url: "../ajax/Ventas/get_dataVenta.php",
            data: {
                idcoti:idCoti
            },
            success: function (dat) {
                if (isJson(dat)){
                    const jsObj= JSON.parse(dat)
                    console.log(jsObj)
                    APP._data.datosProEmpre =jsObj;

                }else{
                    console.log("errr")

                    console.log(dat);
                }
            }
        });
    }

    $(document).ready(function() {

        geTasaCambio();
        getDataVenta();
        if (!isRegister){
            APP.cargarDatoCotizacion();
        }

        $('#tttttttttttttttt').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            serverSide: true,
            processing: true,
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
        $('#1111111111111').DataTable({
            /*scrollY: false,
            scrollX: false,*/
            paging: true,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
        //$('select').selectpicker();

        $("#input-coti-cliente").autocomplete({
            source: "../ajax/Clientes/buscar_clientes.php",
            minLength: 2,
            select: function (event, ui) {
                console.log(ui.item)
                event.preventDefault();
                APP.setDataCliente(ui.item);

                $.ajax({
                    type: "POST",
                    url: "../ajax/Clientes/getAllDirecciones.php",
                    data: {idc:APP._data.cliente.id},
                    success: function (data) {
                        if (isJson(data)){
                            var direc =JSON.parse(data);
                            APP._data.cliente.direcciones = direc;
                            APP._data.cliente.idDireccion=(typeof direc[0]!=='undefined')?direc[0].id:'';
                           /* setTimeout(function () {
                                $('select').selectpicker('refresh');
                            },100);*/
                        }else{
                            console.log(data);
                        }

                    }
                });

               /* $('#hidden_id_proveedor').val(ui.item.id);
                $('#input_ruc_proveedor').val(ui.item.ruc);
                $('#input_razon_social').val(ui.item.razon_social);
                $('#input_direccion').val(ui.item.direccion);
                $('#input_producto').focus();*/
            }
        });

        $("#input-coti-cliente").focus(function () {
            //$("#modal_buscar_cliente").modal('show');
        });
        $("#input-producto-buscar").focus(function () {
            $("#modal_buscar_productos").modal('show');
        });

        $("#input-coti-agendia").focus(function () {
            $("#modal_buscar_Agencia_transporte").modal('show');
        });

        $('#modal_buscar_cliente').on('hidden.bs.modal', function () {
            if (MODALES._data.clientes.isSelected){
                APP.setDataCliente(MODALES._data.clientes.clienteSelected);
            }
        });
        $('#modal_buscar_Agencia_transporte').on('hidden.bs.modal', function () {
            if (MODALES._data.agenciaTansporte.isSelected){
                APP.setDataTransporte(MODALES._data.agenciaTansporte.agenciaSelected);
            }
        });
        $('#modal_buscar_productos').on('hidden.bs.modal', function () {
            if (MODALES._data.productos.isSelected){
                APP.setDataProducto(MODALES._data.productos.productoSelected);
            }
        });
        //$("#loader").fadeOut("slow");
    });

    function NumeroAleatorio(min, max) {
        var num = Math.round(Math.random() * (max - min) + min);
        return num;
    }







</script>

</html>
