<?php


class DataBase
{
    private $action;
    private $table;
    private $idTable;
    private $db;
    private $conectar;

    public function __construct($action,$table,$id) {
        $this->action=(string) $action;
        $this->table=(string) $table;
        $this->idTable=(string) $id/*$table.'_id'*/;
        $file= 'conexion/Conexion.php';
        if(file_exists($file)){
            require_once "".$file."";
        }else{
            $file='../'.$file;
            if(file_exists($file))
            {
                require_once "".$file."";
            }else{
                $file= '../../conexion/Conexion.php';
                if (file_exists($file)){
                    require_once "".$file."";
                }
                else{
                    $file= '../conexion/Conexion.php';
                    if (file_exists($file)){
                        require_once "".$file."";
                    }
                }
            }

        }
        /*$file= 'View/conexion/Conexion.php';
        if(file_exists($file)){
            require_once "".$file."";
        }else{
            $file=''.$file;
            require_once "".$file."";
        }*/
        $this->conectar=new Conexion();

        $this->db=$this->conectar->getConexion();
    }

    public function getConetar(){
        return $this->conectar;
    }
    public function db(){
        return $this->db;
    }

    public function selectAll(){
        $sql =" $this->action * FROM $this->table ORDER BY $this->idTable ASC";
        $query=$this->db->query($sql);
        $resultSet=[];
        while ($row = $query->fetch_object()) {
            $resultSet[]=$row;
        }
        $this->conectar->closeConexion();
        return $resultSet;
    }
    public function insertAll2($datos){
        $valores="";
        foreach ($datos as $dato) {
            if($dato == ''){
                $valores .= "NULL,";
            }else{
                $valores .= "'".$dato."',";
            }

        }
        $valores = "(".substr($valores, 0, -1).")";
        $sql =" $this->action INTO $this->table VALUES $valores".';';
        //echo $sql;
        $res = $this->db->query($sql);
        $this->conectar->closeConexion();
        return $res;
    }
    public function insertAll($datos){
        $valores="";
        foreach ($datos as $dato) {
            if($dato == ''){
                $valores .= "NULL,";
            }else{
                $valores .= "'".$dato."',";
            }

        }
        $valores = "(".substr($valores, 0, -1).")";
        $sql =" $this->action INTO $this->table VALUES $valores".';';
       // echo $sql;
        $res = $this->db->query($sql);
        $this->conectar->closeConexion();
        return $res;
    }

    public function insertAllAndGetId($datos){
        $valores="";
        foreach ($datos as $dato) {
            if($dato == ''){
                $valores .= "NULL,";
            }else{
                $valores .= "'".$dato."',";
            }
        }
        $valores = "(".substr($valores, 0, -1).")";
        $sql =" $this->action INTO $this->table VALUES $valores".';';
       // echo $sql;
        $this->db->query($sql);
        return $this->db->insert_id;
        $this->conectar->closeConexion();
    }
    public function updateById2($data){
        $valores="";
        $idDatos =$data[0];

        for($i = 1; $i < count($data);$i++){
            $valores .= $data[$i][0]." = '".$data[$i][1]."',";
        }
        foreach ($idDatos as $item){

        }

        $valores = "".substr($valores, 0, -1)."";
        $sql ="$this->action $this->table SET $valores where $this->idTable = '$idDatos';";
        $res = $this->db->query($sql);
        $this->conectar->closeConexion();
        return $res;
    }

    public function updateById($data){
        $valores="";
        $idDatos =$data[0][0];

        $idDatos =$data[0][1];
        for($i = 1; $i < count($data);$i++){
            $valores .= $data[$i][0]." = '".$data[$i][1]."',";
        }
        $valores = "".substr($valores, 0, -1)."";
        $sql ="$this->action $this->table SET $valores where $this->idTable = '$idDatos';";
        //echo $sql;
        $res = $this->db->query($sql);
        $this->conectar->closeConexion();
        return $res;
    }
    public function updateByColumn($datos,$Column,$value){
        $valores="";
        foreach ($datos as $dato) {
            $valores .= "'".$dato."',";
        }
        $valores = "(".substr($valores, 0, -1).")";
        $sql =" $this->action $this->table SET $valores where $Column =$value".';';
        $res = $this->db->query($sql);
        $this->conectar->closeConexion();
        return $sql;
    }
    public function updateByColumnAndID($data,$Column,$value){
        $valores="";
        for($i = 0; $i < count($data);$i++){
            $valores .= $data[$i][0]." = '".$data[$i][1]."',";
        }
        $valores = "".substr($valores, 0, -1)."";
        $sql ="$this->action $this->table SET $valores where $Column = '$value';";
        $res = $this->db->query($sql);
        $this->conectar->closeConexion();
        return $res;
    }
    public function selectById($id){
        $sql=" $this->action * FROM $this->table WHERE $this->idTable=$id".';';
        //echo $sql;
        $query=$this->db->query($sql);
        $resultSet = array();
        if($row = $query->fetch_object()) {
            $resultSet = $row;
        }
        /* echo "$this->action * FROM $this->table WHERE $this->idTable=$id".';';*/
        /*$this->conectar->closeConexion();*/
        return $resultSet;
    }
    public function selectId(){
        $sql =" $this->action * FROM $this->table ORDER BY $this->idTable DESC LIMIT 1".';';
        $query=$this->db->query($sql);
        if($row = $query->fetch_object()) {
            $resultSet=$row;
        }
        $this->conectar->closeConexion();
        return $resultSet;
    }

    public function selectAllByColumn($column,$value){
        $sql =" $this->action * FROM $this->table WHERE $column='$value'".';';
        //echo $sql;
        $query=$this->db->query($sql);
        $resultSet= Array();
        while($row = $query->fetch_object()) {
            $resultSet[]=$row;
        }
        $this->conectar->closeConexion();
        return $resultSet;
    }

    public function selectByColumn($column,$value){
        $sql ="$this->action * FROM $this->table WHERE $column='$value'".';';
        $query=$this->db->query($sql);
        $resultSet= Array();
        while($row = $query->fetch_object()) {
            $resultSet=$row;
        }
        $this->conectar->closeConexion();
        return $resultSet;
    }
    public function selectForSql($sql){
        $query=$this->db->query($sql);
        $resultSet = array();
        while ($row = $query->fetch_object()) {
            $resultSet[]=$row;
        }
        $this->conectar->closeConexion();
        return $resultSet;
    }
    public function deleteById($id){
        $sql =" $this->action FROM $this->table WHERE $this->idTable=$id".';';
        $result = $this->db->query($sql);
        $this->db->close();
        return $result;
    }
    public function updateCntByID($id,$cnt){
        $sql ="UPDATE sys_com_packing SET  pack_cant = ".$cnt." WHERE pack_id = " . $id;
        $result = $this->db->query($sql);
        $this->db->close();
        return $result;
    }

    public function deleteByColumn($column,$value){
        $sql =" $this->action FROM $this->table WHERE $column='$value'".';';
        $result = $this->db->query($sql);
        $this->db->close();
        return $result;
        /*return $this->db->insert_id;*/
    }
    public function existData($sql){
        $result=$this->db->query( $sql);
        return $result;
    }
    public function getForSQl($sql){
        $query=$this->db->query($sql);

        while ($row = $query->fetch_object()) {
            $resultSet[]=$row;
        }
        $this->conectar->closeConexion();
        return $resultSet;
    }
}