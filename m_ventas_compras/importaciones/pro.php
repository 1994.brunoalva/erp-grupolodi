<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>
    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>
    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">
    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>
    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
</head>

<body>
<div id="wrapper">
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">

                    <input type="text" class="inp">

                    <script>
                        $(document).ready(function () {


                            var index = 0;
                            $('.inp').keypress(function (e) {
                                var regex = new RegExp("^[0-9]+$");
                                var letra = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                                var moneyText = $(this).val();
                                if ((moneyText.length) === 0) {
                                    if (letra === '.' || letra === '0') {
                                        e.preventDefault();
                                        return false;
                                    }
                                } else {
                                    var tienePunto = moneyText.toString().includes('.');
                                    if (tienePunto === false && letra === '.') {
                                        $(this).val(moneyText + letra);
                                    }
                                }
                                if (conuntChart>=2) {
                                    if(conuntChart===3){
                                        $(this).val(moneyText.substring(0,moneyText.length-1));
                                    }
                                    e.preventDefault();
                                    return false;
                                }
                                if (!regex.test(letra)) {
                                    e.preventDefault();
                                    return false;
                                }
                            });


                            var numero ='';
                            var conuntChart=0;


                            $('.inp').keyup(function () {
                                var moneyText = $(this).val();
                                conuntChart=0;
                                var tienePunto=true;
                                if ((moneyText.length) > 0) {
                                    index=0;
                                    index = moneyText.indexOf('.');
                                    tienePunto = moneyText.toString().includes('.');
                                    if (tienePunto) {
                                        for (let i = (index+1); i < moneyText.length; i++) {
                                            ++conuntChart;
                                        }
                                    }
                                }
                                moneyText =  $(this).val();
                                tienePunto = moneyText.toString().includes('.');
                                var decimal='';
                                if(tienePunto){
                                    decimal='';
                                    index = moneyText.indexOf('.');
                                    decimal='.'+moneyText.substring(index+1);
                                }else{
                                    moneyText=moneyText.replace(/,/g, '');
                                    moneyText.substring(0, index);
                                    numero='';
                                    moneyText = moneyText.split('').reverse().join('');
                                    for (let i = moneyText.length-1; i >= 0; --i) {
                                        numero += moneyText.charAt(i);
                                        if(i%3===0&&i>0){
                                            numero += ',';
                                        }
                                    }
                                }
                                $(this).val(numero+decimal);
                            });

                            $('.inp').focusout(function () {
                                var moneyText=$(this).val();
                                var xy =moneyText.indexOf('.');
                                var tienePunto = moneyText.toString().includes('.');
                                if(tienePunto===false){
                                    $(this).val(moneyText+'.00');
                                }else{
                                    var beforePunto =moneyText.substring(xy+1);
                                    if(beforePunto.length===0){
                                        $(this).val(moneyText+'00');
                                    }else if(beforePunto.length===1){
                                        $(this).val(moneyText+'0');
                                    }
                                }
                            });
                        });

                    </script>
                </div>
            </div>
        </div>
    </div>


    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="module" src="../aConfig/alertToas.js"></script>
    <script src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="text/javascript" src="https://unpkg.com/default-passive-events"></script>

    <!--<script type="text/javascript">

        $(document).ready(function () {
            $('#table-folder-import').DataTable({
                scrollY: false,
                scrollX: true,
                scrollCollapse: false,
                paging: true,
                autoWidth: false,
                fixedColumns: {
                    leftColumns: 2,
                },
                language: {
                    url: '../assets/Spanish.json'
                }
            });
        });

    </script>-->


</body>

</html>
