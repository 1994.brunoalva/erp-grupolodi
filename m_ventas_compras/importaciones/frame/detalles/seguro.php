<?php
include '../ajax/Poliza/getForID-in.php';
date_default_timezone_set("America/Lima");


$pol_id='';
$sum_ase='';
$ref='';
$vig_des='';
$vig_has='';
$num_pol='';
$aplica='';
$taza_id='';
$prima_neta='';
$prima_total='';
$cif_total='';
$folder_id='';

$dataPoliza = getAllPolizaForId($_GET['id']);

if ($dataPoliza) {
    $pol_id =$dataPoliza->pol_id;
    $sum_ase =$dataPoliza->pol_suma_aseg;
    $ref =$dataPoliza->pol_referencia;
    $vig_des =$dataPoliza->pol_vig_desde;
    $vig_has =$dataPoliza->pol_vig_hasta;
    $num_pol =$dataPoliza->poliza_id;
    $aplica =$dataPoliza->pol_aplicacion;
    /*$taza_id =$dataPoliza->tasa_poliza_id;*/
    $prima_neta =$dataPoliza->pol_prima_neta;
    $prima_total =$dataPoliza->pol_prima_total;
    $cif_total =$dataPoliza->pol_cif;
    $folder_id =$dataPoliza->impor_id;
    if(isset($dataPoliza->tasa_poliza_id)){
        $taza_id = $dataPoliza->tasa_poliza_id->tasa_poliza_id;
    }
}


/*echo json_encode($dataPoliza,JSON_PRETTY_PRINT);*/
?>

<div class="modal fade" id="modal_tazaPolisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xs " role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nueva Tasa</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                <form action="#">
                    <div class="container-fluid">
                        <div class="form-group col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Porcentaje:</label>
                            <input id="modal-procentaje-input-tasa" class="form-control" type="text" placeholder="ejem: 0.14 %" required="">
                        </div>

                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <button onclick="agregarTasa()" type="button" class="btn btn-primary">
                            Guardar
                        </button>

                        <button type="button"  class="btn btn-success" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>-->
            <!--<hr style="width: 100%;border-bottom: 0.2em #0866C6;">-->
            <!--<div class="modal-footer">

            </div>-->
        </div>
    </div>
</div>

<input type="text" id="seguro-input-poliza-id" value="<?php echo $pol_id?>" class="no-display">
<input type="text" id="seguro-input-folder-id" value="<?php echo $folder_id?>" class="no-display">
<div class="panel-box" id="form-seguro">
    <div class="row">
        <div class="col-md-12 no-padding text-right" style="padding-bottom: 15px; margin-bottom: 15px; display: ">

            <button onclick="openSeguguro()" type="button" class="btn btn-primary" ><i class="fa fa-print"></i> Imprimir Solicitud</button>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>TOTAL FOB:</label>
            <div class="input-group  col-xs-12 ">
            <input  <?php echo $desactivar?'disabled':'' ?>  disabled id="seguro-input-total-fob" type="text" class="form-control" placeholder="0.00 $">
            <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
        </div>
        </div>
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>TOTAL FLETE:</label>
            <div class="input-group  col-xs-12 ">
            <input disabled id="seguro-input-total-flete" type="text" class="form-control" placeholder="0.00 $">
            <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
        </div>
        </div>
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>ASEGURADO:</label>
            <div class="input-group  col-xs-12 ">
            <input disabled value="<?php echo ($sum_ase=='')?$flete_total:$sum_ase ?>" id="seguro-input-suma-asegurada" type="text" class="form-control" placeholder="0.00 $">
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div id="box-input-seguro">
            <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
                <label>REFERENCIA:</label>
                <input  <?php echo $desactivar?'disabled':'' ?>  id="seguro-input-referencia" value="<?php echo $ref?>" type="text" class="n1 form-control" placeholder="N° 1">
            </div>
            <?php
            $fecha1= new DateTime(($vig_des=='')?$fecha:$vig_des);
            $fecha2= new DateTime(($vig_has=='')?$fecha:$vig_has);
            $diff = $fecha1->diff($fecha2);

            ?>

            <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
                <label>% TASA :</label>
                <div class="input-group col-xs-12 no-padding">
                    <select  <?php echo $desactivar?'disabled':'' ?>  id="select_tasa" required
                            class="selectpicker form-control show-tick no-padding" data-live-search="true"
                            data-size="5">
                        <option value="0" selected>-Seleccione-</option>
                        <?php
                        $tasaPoliza = new TasaPoliza('SELECT');
                        $datos = $tasaPoliza->selectAll();
                        foreach ($datos as $row) {
                             if ($taza_id != $row->tasa_poliza_id) {
                                 echo '<option value="' . $row->tasa_poliza_id . '">' . $row->tasa_porcentaje . '</option>';
                             } else {
                                 echo '<option selected value="' . $row->tasa_poliza_id . '">' . $row->tasa_porcentaje . '</option>';
                             }
                        }
                        ?>
                    </select>
                    <span class="input-group-btn">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            onclick=" $('#modal-procentaje-input-tasa').val('')"
                            data-target="#modal_tazaPolisa">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>
                </div>
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>PRIMA NETA:</label>
            <div class="input-group  col-xs-12 ">
            <input disabled id="seguro-input-prima-neta" value="<?php echo $prima_neta?>" type="text" class="form-control" placeholder="0.00 $">
            <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
        </div>
        </div>
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>PRIMA TOTAL:</label>
            <div class="input-group  col-xs-12 ">
            <input disabled id="seguro-input-prima-total" value="<?php echo $prima_total?>" type="text" class="form-control" placeholder="0.00 $">
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>CIF MONTO TOTAL:</label>
            <div class="input-group  col-xs-12 ">
            <input disabled id="seguro-input-monto-total" value="<?php echo $cif_total?>" type="text" class="form-control" placeholder="0.00 $">
            <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
        </div>
        </div>

        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>VIGENCIA DESDE:</label>
            <div class="input-group  col-xs-12 ">
                <?php
                $fecha = date('Y-m-d');
                ?>
                <?php /*echo ($aduana_f_num=='')?$fecha:$aduana_f_num; */?>
                <input  <?php echo $desactivar?'disabled':'' ?>  id="seguro-input-vigencia-desde" type="date" class='n1 form-control'
                                                                 value="<?php echo ($vig_des=='')?$fecha:$vig_des ?>">
                <!-- <span class="input-group-btn">
                     <a class="btn btn-primary"><i class="fa fa-calendar"></i></a>
             </span>-->
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>PLAZO:</label>
            <input  value="<?php echo  $diff->days  ?>" id="seguro-plazo-dias-vigensia"  type="text" class="form-control" placeholder="">
        </div>
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>VIGENCIA HASTA:</label>
            <div class="input-group  col-xs-12 ">
                <?php
                $fecha = date('Y-m-d');
                ?>
                <input  disabled id="seguro-input-vigencia-hasta" type="date" class='form-control'
                                                                 value="<?php echo ($vig_has=='')?$fecha:$vig_has ?>">
                <!-- <span class="input-group-btn">
                     <a class="btn btn-primary"><i class="fa fa-calendar"></i></a>
             </span>-->
            </div>
        </div>

        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label for="Incoterm">POLIZA:</label>
            <div class="input-group col-xs-12 no-padding">
                <input  <?php echo $desactivar?'disabled':'' ?>  id="seguro-input-poliza" value="<?php echo $num_pol?>" type="text" class='n1 form-control' placeholder="">
                <!--<select id="select_poliza" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                /*                    $incoterm = new Incoterm('SELECT');
                                    $datos = $incoterm->selectAll();
                                    foreach ($datos as $row) {
                                        if ($incoterm_id != $row->inco_id) {
                                            echo '<option value="' . $row->inco_id . '">' . $row->inco_nombre . '</option>';
                                        } else {
                                            echo '<option selected value="' . $row->inco_id . '">' . $row->inco_nombre . '</option>';
                                        }
                                    }
                                    */ ?>
                </select>
                <span class="input-group-btn">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_incoterm">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>-->
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>APLICACION:</label>
            <input  <?php echo $desactivar?'disabled':'' ?>  id="seguro-input-aplicacion" value="<?php echo $aplica?>"  type="text" class="n1 form-control input-number" placeholder="">
        </div>
    </div>
    <div class="form-group text-right">
        <div>
            <hr class="line-frame">
        </div>
        <div class="col-md-12 no-padding">
            <button  <?php echo $desactivar?'disabled':'' ?>  id="seguro-btn-limpiar" type="button" class="btn btn-default">Limpiar</button>
            <button  <?php echo $desactivar?'disabled':'' ?>  id="seguro-btn-guardar" type="button" class="btn btn-primary">Guardar</button>
            <!--<button id="folio-folder-btn-salir" href="import.php" type="reset" class="btn btn-warning"><i
                        class="glyphicon glyphicon-chevron-left"></i>Salir
            </button>-->
        </div>
    </div>
</div>
<script>
    function agregarTasa(){
        var tasa = $("#modal-procentaje-input-tasa").val()

        $.ajax({
            type: "POST",
            url: "../ajax/TasaSeguro/add_tasa_seguro.php",
            data: {tasa},
            success: function (data) {
                console.log(data)
                data=JSON.parse(data);
                $("#modal-procentaje-input-tasa").val('')
                $("#select_tasa").append("<option value='"+data.idta+"'>"+tasa+"</option>");
                $('#modal_tazaPolisa').modal('hide');
                $('#select_tasa').selectpicker('refresh');
            }
        });
    }



</script>