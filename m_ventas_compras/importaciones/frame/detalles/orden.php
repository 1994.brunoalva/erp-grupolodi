<?php
/*include '../ajax/Orden/getForID-in.php';*/

$orden_id = '';

$nro_proforma = '';
$fecha_proforma = '';
$moneda_id = '';
$dataOrden = getAllOrdenForId($_GET['id']);
if ($dataOrden) {
    $orden_id = $dataOrden->ord_id;
    if(isset($dataOrden->mone_id)){
        $moneda_id = $dataOrden->mone_id->sun_id;
    }
    $nro_proforma = $dataOrden->impor_id->impor_nro_profo;
    $fecha_proforma = $dataOrden->impor_id->impor_fech_profo;
}
include '../ajax/Embarque/getAllForID-in.php';
$dataEmbarque = getEmbarqueForId($orden_id);
/*echo json_encode($dataOrden,JSON_PRETTY_PRINT);*/

$embarque_id = '';
$despacho_id = '';
$fob_total = '';
$flete_contenedor = '';
$costo_adicional = '';
$thc_contenedor = '';
$flete_total = '';
$embarque_serie_BL = '';
$forwarder_id = '';
$forwarder_nombre = '';
$linea_id = '';
$linea_nombre = '';
$almacen_id = '';
$almacen_nombre = '';
$nave_id = '';
$embarque_etd = '';
$embarque_eta = '';
$embarque_estadia = '';
if ($dataEmbarque) {
    /*echo json_encode($dataEmbarque,JSON_PRETTY_PRINT);*/
    $embarque_id = $dataEmbarque->emb_id;
   // $despacho_id = $dataEmbarque->desp_id->desp_id;
    $fob_total = $dataEmbarque->emb_monto;
    $flete_contenedor = $dataEmbarque->emb_flete;
    $thc_contenedor = $dataEmbarque->emb_thc;
    $costo_adicional = $dataEmbarque->emb_cost_origen;
    $flete_total = $dataEmbarque->emb_total_flete;
    $embarque_serie_BL = $dataEmbarque->emb_serie_bl;
    $forwarder_id = $dataEmbarque->forwa_id?$dataEmbarque->forwa_id->forwa_id:'';
    $forwarder_nombre = $dataEmbarque->forwa_id?$dataEmbarque->forwa_id->forwa_nombre:'';
    //$linea_id = $dataEmbarque->linea_id->linea_id;
    //$linea_nombre = $dataEmbarque->linea_id->linea_nombre;
    //$nave_id = $dataEmbarque->nave_id->nave_id;

    //$almacen_id = $dataEmbarque->alm_id->alm_id;
    //$almacen_nombre = $dataEmbarque->alm_id->alm_razon_social;

    $embarque_etd = $dataEmbarque->emb_etd;
    $embarque_eta = $dataEmbarque->emb_eta;
    $embarque_estadia = $dataEmbarque->emb_fech_sobre;
}
include '../ajax/Nave/getAllNaveForLinea-in.php';

$dataNave = getAllNaveForLinea($linea_id);

$despacho = new Despacho('SELECT');
$datos = $despacho->selectAll();

?>
<input  <?php echo $desactivar?'disabled':'' ?>  id="input_cambio_id" class="form-control no-display" type="text" value="<?php echo $moneda_id; ?>">


<div id="embar-id-cont" class="panel-box">
    <input type="hidden" id="id-orden-folder-sp" value="<?php echo $orden_id ?>">
    <input type="hidden" id="id-embarque-folder-sp" value="<?php echo $embarque_id ?>">
    <div class="row">
        <input  <?php echo $desactivar?'disabled':'' ?>  id="orden-input-embarque-id" class="form-control no-display" type="text"
               value="<?php echo $embarque_id; ?>"><!--EVALUAR SU USO-->
        <input  <?php echo $desactivar?'disabled':'' ?>  id="orden-input-orden-id" class="form-control no-display" type="text" value="<?php echo $orden_id; ?>">
         <!--EVALUAR SU USO-->
        <div class="form-group col-xs-6 col-sm-4 col-md-4">
            <label>PROFORMA</label>
            <input  <?php echo $desactivar?'disabled':'' ?>  id="orden-input-serie-factura" class="form-control" type="text" value="<?php echo $nro_proforma; ?>">
        </div>
        <div class="form-group col-xs-6 col-sm-4 col-md-4">
            <label>FECHA/PROFORMA</label>
            <input  <?php echo $desactivar?'disabled':'' ?>  id="orden-input-fecha-factura" class="form-control" type="date"
                   value="<?php echo $fecha_proforma; ?>">
        </div>
        <div class="form-group col-xs-8 col-sm-4 col-md-4">
            <label>FORWARDER</label>
            <div class="input-group col-xs-12 no-padding">
                <!--<input id="ford-index" class="no-display" type="text" value="<?php /*echo $forwarder_id;*/ ?>">-->
                <div class="input-group col-xs-12 no-padding">
                    <input  <?php echo $desactivar?'disabled':'' ?>  id="orden-input-Forwarder" class="form-control" type="text"
                                                                     value="<?php echo $forwarder_nombre; ?>" required
                                                                     placeholder="Haga click para seleccionar forwarder">
                    <input id="orden-input-Forwarder-id" class="form-control no-display" type="text"
                           value="<?php echo $forwarder_id; ?>">
                    <span class="input-group-btn">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#modal_forwarder">
                                <i class="fa fa-plus"></i></button>
                        </span>
                </div>
                <!--<select id="select_fordware" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                /*                    $forwarder = new Forwarder('SELECT');
                                    $datos = $forwarder->selectAll();
                                    foreach ($datos as $row) {
                                        echo '<option value="' . $row->forwa_id . '">' . $row->forwa_nombre . '</option>';
                                    }
                                    */ ?>
                </select>
                <span class="input-group-btn">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_incoterm">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>-->
            </div>
        </div>

    </div>
    <div class="row">
        <div class="form-group col-xs-5 col-sm-2 col-md-2">
            <label>FOB TOTAL :</label>
            <div class="input-group  col-xs-12 ">
                <input  <?php echo $desactivar?'disabled':'' ?>  disabled id="orden-input-fob-total" type="text" class='form-control'>


                <!--<input disabled type="text" class='form-control'
                       value="<?php /*echo $fob_total; */?>">-->
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-5 col-sm-2 col-md-2">
            <label>COSTO DE ORIGEN</label>
            <div class="input-group  col-xs-12 ">
                <input  id="orden-input-cost-adicional"
                        type="text"
                        class='form-control money'
                        value="<?php echo $costo_adicional; ?>"
                        placeholder="0.00 $">


                <!--<input disabled type="text" class='form-control'
                       value="<?php /*echo $fob_total; */?>">-->
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-5 col-sm-2 col-md-2">
            <label>QTY CTN</label>
            <div class="input-group  col-xs-12 ">
                <input disabled value="<?php echo $cantidad_contenedor; ?>" id="orden-input-qty-cont-" type="text" class='form-control'>


                <!--<input disabled type="text" class='form-control'
                       value="<?php /*echo $fob_total; */?>">-->
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-3 col-sm-2 col-md-2">
            <label>FLETE/CTN :</label>
            <div class="input-group  col-xs-12 ">
                <input  <?php echo $desactivar?'disabled':'' ?> autocomplete="off"  id="orden-input-flete-contenedor" type="text" class='form-control money'
                       value="<?php echo $flete_contenedor; ?>" >
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-2 col-md-2">
            <label>THC / CTN :</label>
            <div class="input-group  col-xs-12 ">
                <input  <?php echo $desactivar?'disabled':'' ?> autocomplete="off"  id="orden-input-thc-contenedor" type="text" class='form-control money'
                       value="<?php echo $thc_contenedor; ?>" placeholder="0.00 $">
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-2 col-md-2">
            <label>TOTAL FLETE :</label>
            <div class="input-group  col-xs-12 ">
                <input  disabled id="orden-input-total-flete" type="text" class='form-control money'
                       value="">
                <!--<input disabled id="orden-input-total-flete" type="text" class='form-control'
                       value="<?php /*echo $flete_total; */?>">-->
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
    </div>
<br>
    <div class="form-group ">
        <div   style="width: 100%; height: 20px; border-bottom: 2px solid #869fba; text-align: left">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                    Datos De Embarque<!--Padding is optional-->
                                                  </span>

        </div>
    </div>

    <div class="form-group  text-right">
        <div   style="width: 100%; height: 20px; border-bottom: 2px solid #ffffff; text-align: right">
                                                  <span style="font-size: 16px; font-weight: bold ; background-color: #ffffff; padding: 0 5px;">
                                                   <button type="button" v-on:click="addListaBlanck()" class="btn btn-success"><i class="fa fa-plus"></i> Agregar otro Embarque</button>
                                                  </span>

        </div>
    </div>
    <div v-for="(item, index) in listaEmbarque">
        <div class="row">
            <div class="form-group col-xs-12 col-sm-4 col-md-4">
                <label>EMBARQUE: # {{index + 1 }}</label> <span :onclick="'openCaratula('+item.idemp+')'" class="btn btn-info"> <i class="fa fa-print"></i> Caratula</span>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12 col-sm-4 col-md-4">
                <label>LINEA :</label>
                <div class="input-group col-xs-12 no-padding">
                    <!--<input id="linea-index" class="no-display" type="text" value="<?php /*echo $linea_id; */ ?>">-->
                    <div class="input-group col-xs-12 no-padding">
                        <input v-bind:dx="index" v-model="item.nombrelinea"  <?php echo $desactivar?'disabled':'' ?>   class="form-control searsh-linea-orden" type="text"
                                                                         placeholder="Haga click para seleccionar Linea Naviera">

                        <span class="input-group-btn">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#modal_linea">
                            <i class="fa fa-plus"></i></button>
                    </span>
                    </div>

                </div>

            </div>

            <div class="form-group col-xs-12 col-sm-4 col-md-4">
                <label>ALMACEN :</label>
                <!--<input id="linea-index" class="no-display" type="text" value="<?php /*echo $almacen_id; */ ?>">-->
                <div class="input-group col-xs-12 no-padding">
                    <input :id="'almacen'+item.idemp" v-bind:dx="index"  v-model="item.nomalmacen"  <?php echo $desactivar?'disabled':'' ?>   class="form-control searsh-almacen-orden" type="text"

                                                                     placeholder="Haga click para agregar almacen">

                    <span class="input-group-btn">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_almacen">
                        <i class="fa fa-plus"></i></button>
                </span>
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-4 col-md-4">
                <label>BL</label>
                <input v-model="item.bl"   <?php echo $desactivar?'disabled':'' ?>   type="text" class="form-control">
            </div>


        </div>
        <div class="row">
            <div class="form-group col-xs-12 col-sm-4 col-md-4">
                <label>NAVE :</label>

                <div class="input-group col-xs-12 no-padding">

                    <select  v-model="item.idnave"  <?php echo $desactivar?'disabled':'' ?>
                                                                      class="selectpicker form-control show-tick no-padding" data-live-search="true"
                                                                      data-size="5">
                        <option v-for="(it, index) in item.naves" v-bind:value="it.nave_id">{{it.nave_nombre}}</option>
                    </select>
                    <span class="input-group-btn">
                    <button  v-on:click=" setPosition(index)"  type="button"  class="btn btn-primary" data-toggle="modal"
                                                                      data-target="#modal_nave">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>
                </div>
            </div>
            <div class="form-group col-xs-6 col-sm-4 col-md-4">
                <label>ETD</label>
                <input  v-model="item.etd" <?php echo $desactivar?'disabled':'' ?>  type="date" class="form-control">
            </div>
            <div class="form-group col-xs-6 col-sm-4 col-md-4">
                <label>ETA: </label><span> {{getDateETA(item.etd,item.eta)}}</span>
                <input  v-model="item.eta"  <?php echo $desactivar?'disabled':'' ?> @keypress="isNumber($event)"  type="text" class="form-control input-number"
                                                                 value="0">
            </div>
            <div class="form-group col-xs-12 col-sm-4 col-md-4">
                <label>LIBRE/SOBREESTADIA: </label><span :id="'libres'+item.idemp"> {{getDateLibreEstadia(item.etd,item.eta,item.libres)}}</span>
                <input  v-model="item.libres"  <?php echo $desactivar?'disabled':'' ?>  @keypress="isNumber($event)" type="text" class="form-control input-number"
                                                                 >
            </div>
            <div class="form-group col-xs-12 col-sm-4 col-md-4">
                <label>TIPO/DESPACHO</label>
                <div class="input-group col-xs-12 no-padding">
                    <select  v-model="item.iddepacho"  <?php echo $desactivar?'disabled':'' ?>
                                                                      class="selectpicker form-control show-tick no-padding" data-live-search="true"
                                                                      data-size="5">
                        <option value="0" selected>-Seleccione-</option>
                        <?php

                        foreach ($datos as $row) {

                                echo '<option value="' . $row->desp_id . '">' . $row->desp_nombre . '</option>';

                        }
                        ?>
                    </select>

                </div>

            </div>

            <div class="form-group col-xs-12 col-sm-4 col-md-4">
                <label>DIAS ALMACENAJE: </label><span :id="'alma'+item.idemp"> {{getDateDiasAlma(item.etd,item.eta,item.libres,item.diasalma)}}</span>
                <input  v-model="item.diasalma"  <?php echo $desactivar?'disabled':'' ?>  @keypress="isNumber($event)" type="text" class="form-control input-number"
                >
            </div>

            <div class="row">


            </div>


        </div>
        <div>
           <hr class="line-frame">
       </div>

    </div>





    <div class="form-group text-right">
        <!--div>
            <hr class="line-frame">
        </div-->
        <div class="col-md-12 no-padding">
            <button  <?php echo $desactivar?'disabled':'' ?>  v-on:click="guardarOrdenEmbarque()" type="button" class="btn btn-primary">Guardar</button>
            <!--<button id="folio-btn-clear" type="reset" class="btn btn-default">Limpiar</button>-->
            <!--<button id="frame-new-folder-btn-salir" href="import.php" type="reset" class="btn btn-warning"><i
                    class="glyphicon glyphicon-chevron-left"></i>Salir
            </button>-->
        </div>
    </div>


</div>