<div class="panel-box">

    <div class="modal fade" id="modal_carga_suelta" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1400; display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">CARGA SUELTA</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <div id="form-modal-almacen">
                        <div class="container-fluid">
                            <div class="form-group col-xs-6 col-sm-6 col-md-6">
                                <label>FECHA DE INGRESO ALMACEN</label>
                                <input id="pack-input-fech-de-llegada" class="form-control" type="Date">
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 col-md-6">
                                <label>FECHA DE SALIDA ALMACEN</label>
                                <input id="pack-input-fech-salida" class="form-control" type="Date">
                            </div>
                            <div class="row text-left no-padding">
                                <table class="table table-hover table-bordered" style="width:100%">
                                    <tr>
                                        <th>DESCRIPCION</th>
                                        <th>U.MEDIDA</th>
                                        <th>QTY</th>
                                    </tr>
                                    <tr>
                                        <td>PITON PARA LLANTA SIN CAMARA TR413 -VP-ACCESORIOS CN</td>
                                        <td>LL + C + P</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>PITON PARA LLANTA SIN CAMARA TR413 -VP-ACCESORIOS CN</td>
                                        <td>LL + C + P</td>
                                        <td>10</td>
                                    </tr>

                                </table>

                            </div>
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">

                            <button type="button" id="modal-almacen-btn-guardar" class="btn btn-primary">
                                Guardar
                            </button>

                            <button type="button" id="modal-almacen-btn-cerrar" class="btn btn-success" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #0866c6;color: white">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel" style="text-align: center;">Seleccione la categoría a imprimir</h2>

                </div>
                <div class="modal-body">
                    <ul class="list-group">
                        <li onclick=" window.open('./pdf/pdf_importaciones_gen.php?folder='+$('#folio_input_id').val());" class="list-group-item list-group-item-action over-clicke">FORMATO 1</li>
                        <?php
                        //print_r($listaCategoria)
                        foreach ($listaCategoria as $itemC){
                            if ($itemC->cat_id!='6'){
                                echo ' <li onclick="openPDF(\''.$itemC->cat_nombre.'\')" class="list-group-item list-group-item-action over-clicke">' . $itemC->cat_nombre .'</li>';
                            }

                        }
                        ?>
                        <li onclick=" window.open('./pdf/pdf_importaciones_carga_suelta.php?folder='+$('#folio_input_id').val());" class="list-group-item list-group-item-action over-clicke">CARGAMENTO SUELTO</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 no-padding text-right" style="padding-bottom: 15px; display: ">
        <button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#exampleModal"><i class="fa fa-print"></i> Imprimir</button>
        <a href="detalle-folio.php?id=<?php echo $_GET['id']?>&tab=pk" class="btn btn-success">Refrescar</a>
    </div>
    <div class="row">
        <div class="tab-pane" id="box-packing" style="padding-top: 30px">


        </div>
    </div>
</div>