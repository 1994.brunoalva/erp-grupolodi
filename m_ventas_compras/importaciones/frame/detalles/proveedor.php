<div class="panel-box">
    <?php
    include '../ajax/Proveedores/getAllForId-in.php';
    $prov_nombre ='';
    $prov_id ='';
    $prov_nro_documento ='';
    $documento_id ='';
    $documento_nom ='';
    $telefono_1 ='';
    $telefono_2 ='';
    $email ='';
    $contacto ='';
    $direccion ='';
    $pais_id ='';
    $prov = getProveedorForId($dataFolio->provee_id->provee_id);
    if($prov){
        $prov_nombre= $dataFolio->provee_id->provee_desc;
        $prov_id=$dataFolio->provee_id->provee_id;
        $prov_nro_documento =$dataFolio->provee_id->provee_num_doc;
        $documento_id=$dataFolio->provee_id->tipodoc_id?$dataFolio->provee_id->tipodoc_id->doc_id:'';
        $documento_nom=$dataFolio->provee_id->tipodoc_id?$dataFolio->provee_id->tipodoc_id->doc_nombre:'';
        $telefono_1 =$dataFolio->provee_id->provee_telf;
        $telefono_2 =$dataFolio->provee_id->provee_telf2;
        $email =$dataFolio->provee_id->provee_email;
        $contacto=$dataFolio->provee_id->provee_contacto;
        $direccion=$dataFolio->provee_id->provee_direc;
        $pais_id=$dataFolio->provee_id->pais_id->pais_id;
    }
    ?>
    <div class="row">
        <div class="container-fluid" id="content-proveedor">
            <div class=" text-left no-padding">
                <div class="form-group col-xs-6 col-sm-7 col-md-5">
                    <label class="col-xs-12 no-padding">PROVEEDOR:</label>
                    <div id="provee-edit" hidden>
                        <input  <?php echo $desactivar?'disabled':'' ?>  id="proveedor-input-edit" class="form-control" type="text" placeholder="Nuevo Proveedor"
                               value="<?php echo $prov_nombre; ?>">
                    </div>
                    <div id="provee-box" class="input-group col-xs-12 no-padding">
                        <div class="input-group col-xs-12 no-padding">
                            <input  <?php echo $desactivar?'disabled':'' ?>  id="frame-new-folder-input_proveedor" class="form-control" type="text"
                                   value="<?php echo $prov_nombre; ?>"
                                   placeholder="Haga click para agregar proveedor">
                            <input id="frame-new-folder-input_proveedor_id" class="form-control no-display" type="text"
                                   value="<?php echo $prov_id; ?>">
                            <span class="input-group-btn">
                            <button id="frame-new-btn-add-proveedor" type="button" class="btn btn-primary"
                                    data-toggle="modal" data-target="#modal_proveedor">
                                <i class="fa fa-plus"></i></button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-xs-3 col-sm-6 col-md-4">
                    <label class="col-xs-12">PAIS :</label>
                    <div class="input-group  col-xs-12 no-padding">
                        <select  <?php echo $desactivar?'disabled':'' ?>  id="select_prov_pais" class="selectpicker form-control no-padding"
                                                                          data-live-search="true" data-size="5">
                            <option value="0" selected>-Seleccione-</option>
                            <?php
                            foreach ($datosPais as $row) {
                                if($pais_id !=$row->pais_id)
                                {
                                    echo '<option value="' . $row->pais_id . '">' . $row->pais_nombre . '</option>';
                                }else{
                                    echo '<option selected value="' . $row->pais_id . '">' . $row->pais_nombre . '</option>';
                                }
                            }
                            ?>
                        </select>
                        <span class="input-group-btn">
                            <button id="boton_modales" type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#modal_pais"><i
                                        class="fa fa-plus"></i></button>
                        </span>
                    </div>
                </div>
                <div hidden class="form-group col-xs-6 col-sm-5 col-md-4">
                    <label class="col-xs-12 no-padding">N° DOCUMENTO :</label>
                    <input  <?php echo $desactivar?'disabled':'' ?>  id="proveedor-input-num-doc" type="text" disabled class="form-control col-xs-12"
                           value="<?php echo $prov_nro_documento; ?>"
                           placeholder="Numero de documento">
                </div>
                <div hidden class="form-group col-xs-12 col-sm-12 col-md-4">
                    <label class="col-xs-12 no-padding">TIPO DOCUMENTO :</label>
                    <input  <?php echo $desactivar?'disabled':'' ?>  id="doc-index" class="no-display" type="text"
                           value="<?php echo $documento_id; ?>">
                    <div class="input-group  col-xs-12 no-padding">
                        <input  disabled id="proveedor-input-Nondoc" type="text"  class="form-control col-xs-12"  value="<?php echo $documento_nom; ?>" >
                        <input   id="proveedor-input-iddoc" type="hidden"  value="<?php echo $documento_id; ?>" >

                    </div>
                </div>
            </div>
            <div class=" text-left no-padding">
                <div class="form-group col-xs-6 col-sm-6 col-md-3">
                    <label class="col-xs-12 no-padding">TELEFONO N°1 :</label>
                    <input  <?php echo $desactivar?'disabled':'' ?>  id="proveedor-input-telf-1" type="text" disabled class="form-control col-xs-12"
                           value="<?php echo $telefono_1; ?>" placeholder="Telef. N° 1">
                </div>
                <div class="form-group col-xs-6 col-sm-6 col-md-3">
                    <label class="col-xs-12 no-padding">TELEFONO N°2 :</label>
                    <input  <?php echo $desactivar?'disabled':'' ?>  id="proveedor-input-tlf-2" type="text" disabled class="form-control col-xs-12"
                           value="<?php echo $telefono_2; ?>" placeholder="Telef. N° 2">
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3">
                    <label class="col-xs-12 no-padding">EMAIL :</label>
                    <input  <?php echo $desactivar?'disabled':'' ?>  id="proveedor-input-email" disabled class="form-control col-xs-12"
                           value="<?php echo $email; ?>"  placeholder="myEmail@myEmpresa.com">
                </div>
            </div>
            <div class=" text-left no-padding">
                <div class="form-group col-xs-12 col-sm-5 col-md-3">
                    <label class="col-xs-12 no-padding">CONTACTO :</label>
                    <input  <?php echo $desactivar?'disabled':'' ?>  id="proveedor-input-contacto" type="text" disabled class="form-control col-xs-12"
                           value="<?php echo $contacto; ?>"  placeholder="Contacto para informacion">
                </div>
                <div class="form-group col-xs-12 col-sm-7 col-md-3">
                    <label class="col-xs-12 no-padding">DIRECCION :</label>
                    <input  <?php echo $desactivar?'disabled':'' ?>  id="proveedor-input-direccion" type="text" disabled class="form-control col-xs-12"
                           value="<?php echo $direccion; ?>"    placeholder="Direccion del proveedor ?">
                </div>

            </div>
        </div>
        <div>
            <hr class="line-frame">
        </div>
        <div class="container-fluid text-right">

            <button  <?php echo $desactivar?'disabled':'' ?>  type="button" id="proveedor-btn-active-disable" class="btn btn-primary invisible">
                disable
            </button>
            <button  <?php echo $desactivar?'disabled':'' ?>  type="button" id="proveedor-btn-guardar" class="btn btn-primary">
                Guardar
            </button>
            <button  <?php echo $desactivar?'disabled':'' ?>  type="button" id="proveedor-btn-editar" class="btn btn-default">
                Editar
            </button>
            <!--<button type="button" id="proveedor-btn-cerrar" class="btn btn-success"
                    data-dismiss="modal">
                Cerrar
            </button>-->
        </div>
    </div>
</div>