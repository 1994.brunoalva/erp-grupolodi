<?php
include '../ajax/Pagos/getForID-in.php';
include '../ajax/DetallePagos/getForID-in.php';

$pago_id = '';
$folder_id = '';
$monto_total = '';
$monto_pendiente = '';

$dataDetaPagos = Array();
$dataPagos = getAllPagosForId($_GET['id']);
if ($dataPagos) {
    $pago_id = $dataPagos->pago_id;
    $folder_id = $dataPagos->impor_id;
    $monto_total = $dataPagos->pago_mon_total;
    $monto_pendiente = $dataPagos->pago_mon_pend;
    $dataDetaPagos = getAllDetaPagosForId($pago_id);
}
/*echo json_encode($dataPagos,JSON_PRETTY_PRINT);*/

/*echo json_encode($dataDetaPagos, JSON_PRETTY_PRINT);*/


?>
<input id="pago-input-pago-id" type="text" value="<?php echo $pago_id ?>" class="no-display">
<input id="pago-input-folder-id" type="text" value="<?php echo $folder_id ?>" class="no-display">
<div class="panel-box">
    <div class="row">

        <div class="form-group col-xs-12 col-sm-4 col-md-4">
            <label>INCOTERM :</label>
            <div class="input-group  col-xs-12 ">
            <input disabled value="<?php echo $pago_incoterm_nombre ?>" id="pago-input-incoterm" type="text"
                   class="form-control" placeholder="0.00 $">
            <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
        </div>

        </div>
        <div class="form-group col-xs-6 col-sm-4 col-md-4">
            <label>MONTO TOTAL :</label>
            <div class="input-group  col-xs-12 ">
            <input disabled value="<?php echo $monto_total ?>" id="pago-input-monto-total" type="text"
                   class="form-control" placeholder="0.00 $">
            <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
        </div>
        </div>
        <div class="form-group col-xs-6 col-sm-4 col-md-4">
            <label>PENDIENTE :</label>
            <div class="input-group  col-xs-12 ">
            <input disabled value="<?php echo $monto_pendiente ?>" id="pago-input-monto-pendiente" type="text"
                   class="form-control" placeholder="0.00 $">
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>

        </div>

        <div class="form-group col-xs-12  col-sm-7 col-md-6">
            <label>CONDICION DE PAGO :</label>
            <div class="input-group col-xs-12 no-padding">
                <select   <?php echo $desactivar?'disabled':'' ?>   id="select_tasas" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                    $condicionPagos = new CondicionPagos('SELECT');
                    $datos = $condicionPagos->selectAll();
                    foreach ($datos as $row) {
                        echo '<option name="' . $row->conpa_por . '" value="' . $row->conpa_id . '">' . $row->conpa_nombre . '</option>';

                        /*if ($taza_id != $row->tasa_poliza_id) {
                            echo '<option value="' . $row->tasa_poliza_id . '">' . $row->tasa_porcentaje . '</option>';
                        } else {
                            echo '<option selected value="' . $row->tasa_poliza_id . '">' . $row->tasa_porcentaje . '</option>';
                        }*/
                    }
                    ?>
                </select>
                <!--<span class="input-group-btn">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_incoterm">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>-->
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-2 col-md-3">
            <label><!--MONTO-->A PAGAR :</label>
            <div class="input-group  col-xs-12 ">
            <input disabled id="pago-input-monto-pagar" type="text" class="form-control" placeholder="0.00 $">
            <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
        </div>
        </div>
        <div class="form-group col-xs-6  col-sm-3 col-md-3">
            <label>FECHA:</label>
            <div class="input-group  col-xs-12 ">
                <?php
                $fecha = date('Y-m-d');
                ?>
                <input   <?php echo $desactivar?'disabled':'' ?>   id="pago-input-fecha" type="date" class='form-control'
                       value="<?php echo $fecha?>">
                <!--input   <?php echo $desactivar?'disabled':'' ?>   id="pago-input-fecha" type="date" class='form-control'
                       value="<?php echo ($vig_has == '') ? $fecha : $vig_has ?>"-->
                <!-- <span class="input-group-btn">
                     <a class="btn btn-primary"><i class="fa fa-calendar"></i></a>
             </span>-->
            </div>
        </div>

        <div class="form-group col-xs-6 col-sm-2 col-md-3">
            <label>BANCO</label>
            <select   <?php echo $desactivar?'disabled':'' ?>   id="select_banco" required
                                                                class="selectpicker form-control show-tick no-padding" data-live-search="true"
                                                                data-size="5">
                <!--option value="0" selected>-Seleccione-</option-->
                <?php
                foreach ($listaBancos as $row){
                    echo '<option value="'.$row["ban_id"].'" >' .$row['ban_nombre'] . '</option>';
                }

                ?>
            </select>


        </div>

        <div class="form-group col-xs-6 col-sm-2 col-md-3">
            <label>NUMERO DE OPERACION:</label>
            <input id="input-num-operacion-pago"  type="text" class="form-control" placeholder="">
        </div>

        <div class="form-group col-xs-6 col-sm-2 col-md-3">
            <label>DOC. SWIFT:</label>
            <input accept=".pdf, image/*" type="file" id="fileSwift">
        </div>

    </div>

    <div class="form-group text-right">
        <div>
            <hr class="line-frame">
        </div>
        <div class="col-md-12 no-padding">
            <button   <?php echo $desactivar?'disabled':'' ?>   id="pago-btn-limpiar" type="button" class="btn btn-default">Limpiar</button>
            <button   <?php echo $desactivar?'disabled':'' ?>   id="pago-btn-guardar" type="button" class="btn btn-primary">Agregar</button>
            <!--<button id="folio-folder-btn-salir" href="import.php" type="reset" class="btn btn-warning"><i
                        class="glyphicon glyphicon-chevron-left"></i>Salir
            </button>-->
        </div>
    </div>

    <div class="form-group col-xs-12 no-padding" style="overflow-x:hidden; margin-top: 15px;">

        <div class="table-responsive">
            <table id="table-pagos" class="table table-striped table-bordered table-hover"
                   style="width: 100%;">
                <thead class="bg-head-table">
                <tr>

                    <th class="text-center">CONDICION DE PAGO</th>
                    <th class="text-center">FECHA</th>
                    <th class="text-center">MONTO</th>
                    <th class="text-center">DOC. SWIFT</th>
                    <th class="text-center">OPCION</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $indice = 0;
               /* $countArray = count($dataDetaPagos);*/
                if ($dataDetaPagos) {
                    foreach ($dataDetaPagos as $item) {
                        ++$indice
                        ?>
                        <tr>

                            <td class="text-center"><?php echo $item->conpa_id->conpa_nombre ?></td>
                            <td class="text-center"><?php echo $item->detpa_fecha ?></td>
                            <td class="text-center"><?php echo "$".$item->detpa_monto ?></td>
                            <td class="text-center">
                                <?php
                                if (strlen($item->src_file)>0){  ?>
                                    <button onclick="open_newfill('../imagenes/doc-folder/pago/<?php echo $item->src_file ?>')" class="btn btn-info" ><i class="fa fa-file"></i></button>

                                <?php  }
                                ?>

                            </td>
                            <td class="text-center">
                                <?php if($desactivar){ ?>
                                    <span disabled  class="btn btn-sm btn-danger fa fa-times"
                                       title="eliminar item" data-dismiss="modal"></span>
                                <?php  }else{ ?>
                                    <a id="btn-pa" class="btn btn-sm btn-danger fa fa-times btn-option"
                                       title="eliminar item" data-dismiss="modal"></a>
                                <?php }?>

                                <input class="depa_id no-display" type="text"
                                       value="<?php echo $item->detpa_id; ?>">
                            </td>
                        </tr>
                        <?php
                    }
                }
                /*function validar($data)
                {
                    if (isset($data)) {
                        return $data;
                    } else {
                        return '';
                    }
                }*/

                ?>
                </tbody>


            </table>

        </div>


    </div>


</div>
<style>
    #table-pagos_filter {
        display: none;
    }
</style>
<script>
    $(document).ready(function () {
        $('#table-pagos').DataTable({
            /*scrollY: false,*/
            /*scrollX: true,*/
            paging: false,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
    });
</script>