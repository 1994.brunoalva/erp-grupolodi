<style>
    .table > thead > tr > th {
        padding: 4px !important;
    }

    .medida .bootstrap-select button {
        opacity: 100 !important;
    }
</style>

<?php
/* include '../ajax/Orden/getForID-in.php';*/
/*include '../ajax/OrdenDetalle/getAllForID-in.php';*/
?>
<div class="panel-box">

    <!--<div class="form-group col-xs-12 col-sm-3 col-md-4">
        <label>Modelo:</label>
        <div class="input-group col-xs-12 no-padding">
            <select id="select_pro_modelo" required
                    class="selectpicker form-control show-tick no-padding" data-live-search="true"
                    data-size="5">
                <option value="0" selected>-Seleccione-</option>
            </select>
            <span class="input-group-btn">
                <button id="folio_btn_select_modelo" type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#modal_modelo">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>-->

    <div id="row-box-producto">
        <div class="">
            <!--<div class="form-group col-xs-12 col-sm-8 col-md-4">
            <label>Categoria Producto:</label>
            <div class="input-group col-xs-12 no-padding">
                <select id="select_prod_categoria" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
            /*                    $categoria = new Categoria('SELECT');
                                $datos = $categoria->selectAll();
                                foreach ($datos as $row) {
                                    echo '<option value="' . $row->cat_id . '">' . $row->cat_nombre . '</option>';
                                }
                                */ ?>
                </select>
            </div>
        </div>
        <div class="form-group col-xs-12 col-sm-4 col-md-4">
            <label>Marca:</label>
            <div class="input-group col-xs-12 no-padding">
                <select id="select_pro_marca" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                </select>
                <span class="input-group-btn">
                    <button id="producto_btn_select_marca" type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_marca">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-4">
            <label>Modelo:</label>
            <div class="input-group col-xs-12 no-padding">
                <select id="select_pro_modelo" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                </select>
                <span class="input-group-btn">
                    <button id="folio_btn_select_marca" type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_marca">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>
            </div>
        </div>-->
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label class="col-xs-12 no-padding">PRODUCTOS:</label>
                <div class="input-group col-xs-12 no-padding">
                    <input  <?php echo $desactivar?'disabled':'' ?>  id="producto-input-productos" class="form-control text-left" type="text" required
                           placeholder="Escriba para buscar un producto">
                    <input id="producto-input-productos-id" class="form-control no-display" type="text">
                    <span class="input-group-btn">
                    <button id="btn-modal" type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_producto">
                        <i class="fa fa-plus"></i></button>
                </span>
                </div>
            </div>
            <!-- <script>
                 $(document).ready(function () {
                     $('#btn-modal').click();
                 });
             </script>-->
            <!-- <div class="form-group col-xs-12 col-sm-3 col-md-3">
                 <label>AVD:</label>
                 <div class="input-group col-xs-12 no-padding">
                     <input id="producto-input-avd" class="form-control" type="text">
                     <span class="input-group-btn">
                         <label type="button" class="btn bg-primary">
                             <i class="fa fa-percentage fg-blanco"></i>
                         </label>
                     </span>
                 </div>
             </div>-->
            <!--<div class="form-group col-xs-12 col-sm-3 col-md-3">
                <label>AVD:</label>
                <div class="input-group col-xs-12 no-padding">
                    <input disabled id="producto-input-medida" class="form-control bg-blanco" type="text">
                    <span class="input-group-btn">
                        <label type="button" class="btn bg-primary">
                            <i class="fa fa-percentage fg-blanco"></i>
                        </label>
                    </span>
                </div>
            </div>-->
            <div class="form-group col-xs-12 col-sm-3 col-md-3">
                <label>U - MEDIDA:</label>
                <div class="input-group col-xs-12 no-padding medida">
                    <select  <?php echo $desactivar?'disabled':'' ?>  disabled id="select_medida" required
                            class="selectpicker form-control show-tick no-padding" data-live-search="true"
                            data-size="5">
                        <option value="0" selected>-</option>
                        <?php
                        $unidad = new Unidad('SELECT');
                        $datos = $unidad->selectAll();
                        foreach ($datos as $row) {
                            echo '<option value="' . $row->unidad_id . '">' . $row->unidad_nombre . '</option>';
                        }
                        ?>
                    </select>
                    <!--<span class="input-group-btn">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#modal_marca">
                            <i class="fa fa-plus"></i>
                        </button>
                    </span>-->
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-3" id="qty">
                <label class="col-xs-12 no-padding">CANTIDAD:</label>
                <div class="input-group number-spinner col-xs-12 no-padding">
                <span class="input-group-btn">
                    <a  <?php echo $desactivar?'disabled':'' ?>  id="q-dwn" class="btn btn-primary" data-dir="dwn">
                        <span class="fa fa-minus"></span>
                    </a>
                </span>
                    <input  <?php echo $desactivar?'disabled':'' ?>  id="producto-input-qty" type="text" class="form-control text-center input-number" value="1"
                           maxlength="4">
                    <span class="input-group-btn">
                    <a  <?php echo $desactivar?'disabled':'' ?>  id="q-up" class="btn btn-primary" data-dir="up">
                        <span class="fa fa-plus"></span>
                    </a>
                </span>
                </div>
            </div>
            <div hidden class="form-group col-xs-12 col-sm-4 col-md-4" id="box-presentacion">
                <label class="col-xs-12 no-padding">PRESENTACION:</label>
                <div class="input-group number-spinner col-xs-12 no-padding">
                    <select <?php echo $desactivar?'disabled':'' ?> id="producto-select-presentacion2"
                            class="selectpicker form-control show-tick no-padding" data-live-search="true"
                            data-size="5">

                        <?php


                        foreach ($listaPre as $item){
                            echo '<option value="'.$item->id.'">'.$item->nombre.'</option>';
                        }
                        ?>

                    </select>

                    <!--select  <?php echo $desactivar?'disabled':'' ?>  id="producto-select-presentacion2" class="selectpicker form-control text-center">
                        <option value="1">CAJA</option>
                        <option value="2">BOLSA</option>
                    </select-->
                    <span  data-toggle="modal" data-target="#modal_presentacion"  class="input-group-btn">
                    <a  class="btn btn-primary">
                        <span class="fa fa-plus"></span>
                    </a>
                </span>
                </div>
            </div>
            <div hidden class="form-group col-xs-12 col-sm-4 col-md-4" id="box-cant-presentacion">
                <label class="col-xs-12 no-padding">CANTIDAD POR PRESENTACION:</label>
                <div class="input-group number-spinner col-xs-12 no-padding">
                    <span class="input-group-btn">
                    <a  <?php echo $desactivar?'disabled':'' ?>  id="q-dwn" class="btn btn-primary" data-dir="dwn">
                        <span class="fa fa-minus"></span>
                    </a>
                </span>
                    <input  <?php echo $desactivar?'disabled':'' ?>  id="producto-input-qty-presen" type="text" class="form-control text-center input-number" value="1"
                           maxlength="4">
                    <span class="input-group-btn">
                    <a id="q-up" class="btn btn-primary" data-dir="up">
                        <span class="fa fa-plus"></span>
                    </a>
                </span>
                </div>
            </div>
            <div hidden class="form-group col-xs-12 col-sm-4 col-md-4" id="box-tipo-env">
                <label class="col-xs-12 no-padding">FORMA DE ENVIO:</label>
                <div class="input-group col-xs-12 no-padding">

                    <select   <?php echo $desactivar?'disabled':'' ?> id="select-tipo-envio"  class="form-control">
                        <option value="1">EMBARQUE EN CONTENEDOR</option>
                        <option value="2">CARGA SUELTA</option>
                    </select>
                </div>
            </div>

        </div>
        <div class="">
            <div class="form-group col-xs-12 col-sm-5 col-md-4">
                <label class="col-xs-5 no-padding"> MONEDA:</label>
                <label id="label-cambio" class="col-xs-7 no-padding text-right"> CAMBIO: 0.00 s/.</label>
                <button id="label-precio-cambio" class="no-display"></button>
                <div class="input-group col-xs-12 no-padding">
                    <input disabled class="form-control show-tick col-lg-6" value=" DOLAR">
                    <input type="hidden"  id="select_moneda" value="2">


                    <!-- <span class="input-group-btn bg-warning" style="width: 50%">
                         <input id="producto-input-precio-p-mone" type="text" class="form-control col-xs-12">
                     </span>-->
                </div>

                <!--
            <div class="input-group col-xs-12 no-padding">
                <select id="select_moneda" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                /*                    $moneda = new Moneda('SELECT');
                                    $datos = $moneda->selectAll();
                                    foreach ($datos as $row) {
                                        echo '<option value="' . $row->mone_id . '">' . $row->mone_nombre . '</option>';
                                    }
                                    */ ?>
                </select>
            </div>
            <div class="input-group col-xs-12 no-padding">
                <input id="producto-input-cambi0" type="text" class="form-control input-number" maxlength="4"
                       placeholder="0.00 $">
            </div>-->
            </div>
            <!--<div class="form-group col-xs-12 col-sm-3 col-md-2">
                <label class="col-xs-12 no-padding"> Precio Moneda:</label>
                <div class="input-group col-xs-12 no-padding">
                    <input id="producto-input-cambi0" type="text" class="form-control input-number" maxlength="4"
                           placeholder="0.00 $">
                </div>
            </div>-->
            <div class="form-group col-xs-12 col-sm-3 col-md-3">
                <label class="col-xs-12 no-padding"> PRECIO UNITARIO :</label>
                <input  <?php echo $desactivar?'disabled':'' ?>  id="producto-input-precio-uni" type="text" class="form-control input-number money has-error" maxlength="8" required
                       placeholder="0.00 $">
            </div>
            <div class="form-group col-xs-12 col-sm-2 col-md-2">
                <label class="col-xs-12 no-padding"> DES. %:</label>
                <input  <?php echo $desactivar?'disabled':'' ?>  id="producto-input-descuento" type="text" class="form-control input-number money" maxlength="8"
                       placeholder="0.00 $">
            </div>
            <div class="form-group col-xs-12 col-sm-2 col-md-3">
                <label class="col-xs-12 no-padding"> TOTAL:</label>
                <input  <?php echo $desactivar?'disabled':'' ?>  disabled id="producto-input-total" type="text" class="form-control input-number money" maxlength="15"
                       placeholder="0.00 $">
            </div>
        </div>
        <div class="col-xs-12" style="padding-bottom: 20px;"></div>
        <div class="form-group text-right">
            <!--<div>
                <hr class="line-frame">
            </div>-->
            <div class="col-md-12 no-padding">
                <div class="col-xs-5 text-left no-padding" style="display: flex">
                    <label style="padding: 5px">BUSCAR:</label>
                    <input  <?php echo $desactivar?'disabled':'' ?>  class="form-control input-buscar-p" placeholder="Buscar en la Tabla">
                </div>
                <div class="col-xs-7 no-padding">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#lista-traduccion"><i class="fa fa-print"></i> Traduccion Sunat</button>
                    <button  <?php echo $desactivar?'disabled':'' ?>  id="producto-btn-clear" type="reset" class="btn btn-success">Limpiar</button>
                    <button  <?php echo $desactivar?'disabled':'' ?>  id="producto-btn-guardar" type="button" class="btn btn-primary">Agregar</button>
                </div>


                <!--<button id="folio-folder-btn-salir" href="import.php" type="reset" class="btn btn-warning"><i
                            class="glyphicon glyphicon-chevron-left"></i>Salir
                </button>-->
            </div>
            <div class="col-xs-12" style="padding-bottom: 10px;"></div>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive col-xs-12" style="">
            <?php
            include '../ajax/Orden/getForID-in.php';
            include '../ajax/OrdenDetalle/getAllForID-in.php';
            $o_id = '';
            $detalle_id = '';
            $nombre = '';
            $cantidad = '';
            $medida = '';
            $precioU = '';
            $descu = '';
            $subTotal = '';
            $dataOrden = getAllOrdenForId($_GET['id']);
            $dataDetalle = Array();
            if ($dataOrden) {
                $o_id = $dataOrden->ord_id;
                $dataDetalle = getAllForId($o_id);
                $index = 0;
            }
            echo  '<input id="datos_producto" type="text" value="'.$o_id.'" class="no-display">';
            ?>
            <table id="table-orden-detalle" class="table table-striped table-bordered table-hover  table-produc"
                   style="width: 100%;">
                <thead class="bg-head-table">
                <tr style="height: 10px; padding: 0;">
                    <th class="text-center"></th>
                    <th class="text-center">NOMBRE</th>
                    <!--<th class="text-center">modelo</th>-->
                    <th class="text-center">CANTIDAD</th>
                    <th class="text-center">U. MEDIDA</th>
                    <th class="text-center">P. UNITARIO</th>
                    <th class="text-center">% DESCUENTO</th>
                    <th class="text-center">SUB. TOTAL</th>
                    <th class="text-center">OPCION</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $indice = 0;
                foreach ($dataDetalle as $item) {

                    ?>
                    <tr>
                        <td class="text-center" style="color: white"><?php echo ++$indice; ?></td>
                        <td class="text-left"><?php echo $item->produ_id->produ_nombre; ?></td>
                       <!-- <td class="text-left"><?php /*echo $item->produ_id->mod_id->mod_nombre; */?></td>-->
                        <td class="text-center"><?php echo $item->prod_cantidad; ?></td>
                        <td class="text-left"><?php echo $item->produ_id->unidad_id->unidad_nombre; ?></td>
                        <td class="text-right"><?php echo $item->prod_precio; ?></td>
                        <td class="text-right"><?php echo $item->prod_descuento; ?></td>
                        <td class="text-right"><?php echo $item->prod_total; ?></td>
                        <td class="text-center">
                            <?php echo $desactivar?' <span  disabled id="btn-deta" class="btn btn-sm btn-danger fa fa-times"
                               title="eliminar item" ></span>':' <a   id="btn-deta" class="btn btn-sm btn-danger fa fa-times btn-option"
                               title="eliminar item" data-dismiss="modal"></a>' ?>

                            <input class="deta_id no-display" type="text" value="<?php echo $item->orddeta_id; ?>">
                        </td>
                    </tr>

                    <?php
                }
                ?>
                </tbody>
                <tfoot>
                <tr style="border: none;">
                    <th colspan="8" style="border-right: none;" class="text-center"></th>
                </tr>
                <tr>
                    <th colspan="6" class="text-right cel-border-y-none">TODAL UNIDADES :</th>
                    <th colspan="2" id="cantidad" class="text-right cel-border-box" style="padding-right: 5%">0.00</th>
                </tr>
                <tr class="no-display">
                    <th colspan="6" class="text-right cel-border-y-none">SUB TOTAL :</th>
                    <th colspan="2" id="precio" class="text-right cel-border-box" style="padding-right: 5%">0.00</th>
                </tr>
                <tr class="no-display">
                    <th colspan="6" class="text-right cel-border-y-none">TOTAL DESCUENTO :</th>
                    <th colspan="2" id="descuento" class="text-right cel-border-box" style="padding-right: 5%">0.00</th>
                </tr>
                <tr>
                    <th colspan="6" class="text-right cel-border-y-none">TOTAL :</th>
                    <th colspan="2" id="total" class="text-right cel-border-box" style="padding-right: 5%">0.00</th>
                </tr>
                </tfoot>

            </table>
        </div>
    </div>


</div>

<div class="modal fade" id="modal_presentacion" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1800; display: none;">
    <div class="modal-dialog modal-xs " role="document">
        <div class="modal-content">
            <div class="modal-header no-border no-padding">
                <div class="modal-header text-center color-modal-header">
                    <h3 class="modal-title">Agregar Nueva Presentacion</h3>
                </div>
            </div>
            <div class="modal-body  no-border">
                <form action="#">
                    <div class="container-fluid">
                        <div class="form-group col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Nombre:</label>
                            <input id="presentacion-nombre" class="form-control" type="text">
                        </div>
                        <!--<div class="form-group  col-xs-12 no-padding">
                            <label class="col-xs-12 no-padding">Descripccion:</label>
                            <textarea id="modal-pais-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                        </div>-->
                    </div>
                    <div class="container-fluid">
                        <hr class="line-frame-modal">
                    </div>
                    <div class="container-fluid text-right">

                        <button onclick="addPresentacion()" type="button"  class="btn btn-primary">
                            Guardar
                        </button>
                        <button type="button"  class="btn btn-success" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="lista-traduccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0866c6;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white">&times;</span>
                </button>
                <h2 class="modal-title" id="exampleModalLabel" style="text-align: center;color: white">Seleccione la categoría a imprimir</h2>

            </div>
            <div class="modal-body">
                <ul class="list-group">

                    <?php
                    //print_r($listaCategoria)
                    foreach ($listaCategoria as $itemC){
                        if ($itemC->cat_id!='6'){
                            echo ' <li onclick="openPDFTraSunat(\''.$itemC->cat_nombre.'\')" class="list-group-item list-group-item-action over-clicke">' . $itemC->cat_nombre .'</li>';
                        }

                    }
                    ?>

                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<style>
    #table-orden-detalle_filter {
        display: none;
    }

    /* #table-orden-detalle tfoot{
        border-color: #FFFFFF !important;
        border-collapse: collapse !important;
    }*/

</style>
<script>

    function addPresentacion(){
        const nombre = $("#presentacion-nombre").val();
        $.ajax({
            type: "POST",
            url: "../ajax/Presentacion/add_presentacion.php",
            data: {nombre},
            success: function (data) {
                console.log(data);
                if (isJson(data)){
                    const jso = JSON.parse(data);
                    if (jso.res){
                        $("#producto-select-presentacion2").append('<option value="' + jso.data.id + '">' + jso.data.nombre + '</option>');
                        $('#producto-select-presentacion2').selectpicker('refresh');
                        $("#presentacion-nombre").val('');
                        $("#modal_presentacion").modal('hide');
                        $.toast({
                            heading: 'EXITOSO',
                            text: "Se agrego",
                            icon: 'success',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                    }else{
                        console.log(data);
                        $.toast({
                            heading: 'ERROR',
                            text: "Error No se pudo agregar",
                            icon: 'error',
                            position: 'top-center',
                            hideAfter: '2500',
                        });
                    }
                }else{
                    $.toast({
                        heading: 'ERROR',
                        text: "Error",
                        icon: 'error',
                        position: 'top-center',
                        hideAfter: '2500',
                    });
                    console.log(data);
                }
            }
        });

    }
    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    $("#producto-select-presentacion2").selectpicker();
    $(document).ready(function () {
        $('#table-orden-detalle').DataTable({
            /*scrollY: false,*/
            /*scrollX: true,*/
            paging: false,
            lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
            language: {
                url: '../assets/Spanish.json'
            }
        });
    });
</script>


