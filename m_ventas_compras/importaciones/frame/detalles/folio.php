<?php
    $folder_serie ='';
    $folder_numero ='';
    $incoterm_id ='';
    $contenedor_id ='';
    $cantidad_contenedor ='';

    $categoria_id ='';
    $marca_id ='';
    $pais_id ='';
    $puerto_id ='';
    $fechaFolder="";
    if($dataFolio){
        $folder_serie=$dataFolio->impor_folder;
        $folder_numero=$dataFolio->impor_nrfolder;
        $incoterm_id=$dataFolio->inco_id->inco_id;
        $contenedor_id =$dataFolio->ticon_id->ticon_id;
        $cantidad_contenedor=$dataFolio->impor_qty;
        $categoria_id=$dataFolio->cat_id->cat_id;
        $marca_id=$dataFolio->mar_id->mar_id;
        $pais_id=$dataFolio->pais_id->pais_id;
        $puerto_id=$dataFolio->puerto_id->puerto_id;
        $fechaFolder=$dataFolio->impor_fech_profo;
    }
?>

<div class="panel-box">
    <input type="hidden" id="fechafolderimport" value="<?php echo $fechaFolder?>">
    <div class="row">
        <div class="form-group col-xs-12 col-sm-6 col-md-3">
            <label class="col-xs-12 no-padding"> FOLIO:</label>
            <div class="input-group col-xs-12">
                <input id="folio-input-serie" disabled type="text" class="form-control input-number" maxlength="4"
                       value="<?php echo $folder_serie; ?>">
                <span class="input-group-addon" id="basic-addon1">N°</span>
                <input id="folio-input-numero" disabled type="text" class="form-control" placeholder="# Serie"
                       maxlength="5"
                       value="<?php echo $folder_numero; ?>">
            </div>
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-md-5">
            <label class="col-xs-12 no-padding">EMPRESA:</label>
            <div class="input-group col-xs-12 no-padding">
                <?php
                $emp_nombre = '';
                $emp_id = '';
                $emp_ruc = '';
                if (isset($dataFolio->emp_id->emp_nombre)) {
                    $emp_nombre = $dataFolio->emp_id->emp_nombre;
                    $emp_id = $dataFolio->emp_id->emp_id;
                    $emp_ruc = $dataFolio->emp_id->emp_ruc;
                }
                ?>
                <input <?php echo $desactivar?'disabled':'' ?>   id="folio-input-empresa" class="form-control" type="text" required
                       placeholder="Haga click para agregar Empresa"
                       value="<?php echo $emp_nombre; ?>">
                <input id="folio-input-empresa-id" class="form-control no-display" type="text"
                       value="<?php echo $emp_id; ?>">
                <span class="input-group-btn">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#modal_empresa">
                                <i class="fa fa-plus"></i></button>
                        </span>
            </div>
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-md-4">
            <label>RUC:</label>
            <input  <?php echo $desactivar?'disabled':'' ?>  id="folio-input-ruc" disabled type="text" class="form-control bg-blanco" placeholder="# de RUC"
                   value="<?php echo $emp_ruc; ?>">
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-md-4">
            <label for="Incoterm">INCOTERM:</label>
            <div class="input-group col-xs-12 no-padding">
                <select  <?php echo $desactivar?'disabled':'' ?>  id="select_incoterm" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                    $incoterm = new Incoterm('SELECT');
                    $datos = $incoterm->selectAll();
                    $pago_incoterm_nombre ='';
                    foreach ($datos as $row) {
                        if($incoterm_id !=$row->inco_id)
                        {
                            echo '<option value="' . $row->inco_id . '">' . $row->inco_nombre . '</option>';
                        }else{
                            echo '<option selected value="' . $row->inco_id . '">' . $row->inco_nombre . '</option>';
                            $pago_incoterm_nombre =$row->inco_nombre;
                        }
                    }
                    ?>
                </select>
                <span class="input-group-btn">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_incoterm">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-md-4">
            <label> TIPO DE CTN:</label>
            <div class="input-group col-xs-12 no-padding">
                <select  <?php echo $desactivar?'disabled':'' ?>  id="select_tipo_contenedor" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                    $contenedor = new TipoContenedor('SELECT');
                    $datos = $contenedor->selectAll();
                    foreach ($datos as $row) {
                        if($contenedor_id !=$row->ticon_id)
                        {
                            echo '<option value="' . $row->ticon_id . '">' . $row->ticon_nombre . '</option>';
                        }else{
                            echo '<option selected value="' . $row->ticon_id . '">' . $row->ticon_nombre . '</option>';
                        }

                    }
                    ?>
                </select>
                <span class="input-group-btn">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_tipo_contenedor">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-md-4">
            <label class="col-xs-12 no-padding">QTY CTN</label>
            <div class="input-group number-spinner col-xs-12 no-padding">
                <span class="input-group-btn">
                    <a id="dwn" class="btn btn-primary" data-dir="dwn">
                        <span class="fa fa-minus"></span>
                    </a>
                </span>
                <input  <?php echo $desactivar?'disabled':'' ?>  id="folio-folder-qty" type="text" class="form-control text-center input-number"
                       value="<?php echo $cantidad_contenedor; ?>"
                       maxlength="4">
                <span class="input-group-btn">
                    <a id="up" class="btn btn-primary" data-dir="up">
                        <span class="fa fa-plus"></span>
                    </a>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-6 col-md-3">
            <label>CATEGORIA PRODUCTO:</label>
            <div class="input-group col-xs-12 no-padding">
                <select  <?php echo $desactivar?'disabled':'' ?>  id="select_categoria"
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                    $categoria = new Categoria('SELECT');
                    $datos = $categoria->selectAll();
                    foreach ($datos as $row) {
                        if($categoria_id !=$row->cat_id)
                        {
                            if ($row->cat_id!=6){
                                echo '<option value="' . $row->cat_id . '">' . $row->cat_nombre . '</option>';
                            }

                        }else{
                            echo '<option selected value="' . $row->cat_id . '">' . $row->cat_nombre . '</option>';
                        }

                    }
                    ?>
                </select>
                <span class="input-group-btn">
                            <button type="button" class="btn bg-primary">
                                <i class="fa fa-grip-vertical fg-blanco"></i>
                            </button>
                        </span>
                <!--<span class="input-group-btn">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_categoria">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>-->
            </div>
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-md-3">
            <label for="marca">MARCA:</label>
            <div class="input-group col-xs-12 no-padding">
                <select  <?php echo $desactivar?'disabled':'' ?>  id="select_marca"
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                    include '../ajax/Marca/getMarcaForCategoria-in.php';
                    $dataMarca = getMarcaForCategoria($categoria_id);
                    foreach ($dataMarca as $row){
                        if($marca_id !=$row->mar_id)
                        {
                            echo '<option value="' . $row->mar_id . '">' . $row->mar_nombre . '</option>';
                        }else{
                            echo '<option selected value="' . $row->mar_id . '">' . $row->mar_nombre . '</option>';
                        }

                    }
                    ?>
                </select>

                <span class="input-group-btn">
                    <button id="btn_select_marca" type="button" class="btn btn-primary"  data-toggle="modal" data-target="#modal_marca">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-md-3">
            <label for="pais">PAÍS:</label>
            <div class="input-group col-xs-12 no-padding">
                <select  <?php echo $desactivar?'disabled':'' ?>  id="select_pais" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                    $pais = new Pais('SELECT');
                    $datosPais = $pais->selectAll();
                    foreach ($datosPais as $row) {
                        if($pais_id!=$row->pais_id)
                        {
                            echo '<option value="' . $row->pais_id . '">' . $row->pais_nombre . '</option>';
                        }else{
                            echo '<option selected value="' . $row->pais_id . '">' . $row->pais_nombre . '</option>';
                        }

                    }
                    ?>
                </select>
                <span class="input-group-btn">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_pais">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-md-3">
            <label for="puerto">PUERTO:</label>
            <div class="input-group col-xs-12 no-padding">
                <select  <?php echo $desactivar?'disabled':'' ?>  id="select_puerto" required
                        class="selectpicker form-control show-tick no-padding" data-live-search="true"
                        data-size="5">
                    <option value="0" selected>-Seleccione-</option>
                    <?php
                    include '../ajax/Puerto/getAllPuertoForPais-in.php';
                    $dataPuerto = getPuertoForPais($pais_id);
                    foreach ($dataPuerto as $row){
                        if($puerto_id!=$row->puerto_id)
                        {
                            echo '<option value="' . $row->puerto_id . '">' . $row->puerto_nombre . '</option>';
                        }else{
                            echo '<option selected value="' . $row->puerto_id . '">' . $row->puerto_nombre . '</option>';
                        }
                    }
                    ?>
                </select>
                <span class="input-group-btn">
                    <button id="btn_select_puerto" type="button" class="btn btn-primary"
                            data-toggle="modal" data-target="#modal_puerto">
                        <i class="fa fa-plus"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>

    <div class="form-group text-right">
        <div>
            <hr class="line-frame">
        </div>
        <div class="col-md-12 no-padding">
            <button  <?php echo $desactivar?'disabled':'' ?>  id="folio-btn-guardar" type="button" class="btn btn-primary">Guardar</button>
            <!--<button id="folio-btn-clear" type="reset" class="btn btn-default">Limpiar</button>-->
            <!--<button id="folio-folder-btn-salir" href="import.php" type="reset" class="btn btn-warning"><i
                        class="glyphicon glyphicon-chevron-left"></i>Salir
            </button>-->
        </div>
    </div>
</div>
