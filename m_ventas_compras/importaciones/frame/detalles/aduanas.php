<?php
include '../ajax/Aduana/getForID-in.php';
$aduana = getAduanaForId($dataFolio->impor_id);
$aduana_id='';
$agencia = '';
$agencia_id = '';
$aduana_dua = '';
$aduana_f_num = '';
$canal_id = '';
$aduana_f_ingreso = '';
if ($aduana) {
    $aduana_id = $aduana->impor_aduana_id;
    $agencia = $aduana->age_adua_id->age_adua_nombre;
    $agencia_id = $aduana->age_adua_id->age_adua_id;
    $aduana_dua = $aduana->impor_dua;
    $aduana_f_num = $aduana->impor_fecha_num;
    $canal_id = $aduana->canal_id;
    $aduana_f_ingreso = $aduana->impor_fecha_ingreso;
}
?>

<div class="panel-box" id="aduana-box">
    <div class="col-md-12 no-padding text-right" style="">

    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4">
        <label class="col-xs-12 no-padding">AGENCIA ADUANAS:</label>
        <div id="aduana-box" class="input-group col-xs-12 no-padding">
            <input   <?php echo $desactivar?'disabled':'' ?>   id="aduana-input-nombre" class="form-control" type="text"
                   value="<?php echo $agencia; ?>" placeholder="Haga click para agregar proveedor">
            <input id="aduana-input-agencia_id" class="form-control no-display" type="text"
                   value="<?php echo $agencia_id; ?>">
            <input id="aduana-input_id" class="form-control no-display" type="text"
                   value="<?php echo $aduana_id; ?>">
            <span class="input-group-btn">
                    <button id="proveedor-btn-mas-aduanas" type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_aduanas">
                        <i class="fa fa-plus"></i></button>
                </span>
        </div>
    </div>
    <div class="form-group col-xs-12 col-sm-2 col-md-2">
        <label>DUA :</label>
        <input   <?php echo $desactivar?'disabled':'' ?>   id="aduana-input-dua" type="text" class="form-control"
               value="<?php echo $aduana_dua; ?>" placeholder="">
    </div>
    <div class="form-group col-xs-12 col-sm-2 col-md-2">
        <label>FECHA DE NUMERACION:</label>
        <div class="input-group  col-xs-12 ">
            <?php
                $fecha = date('Y-m-d');
            ?>
            <input   <?php echo $desactivar?'disabled':'' ?>   id="aduana-input-fecha-numeracion" type="date" class='form-control' data-date-format="YYYY-MMMM-DD"
                   value="<?php echo ($aduana_f_num=='')?$fecha:$aduana_f_num; ?>">
            <span class="input-group-btn">
                        <a class="btn btn-primary" id="btn-calendar"><i class="fa fa-calendar"></i></a>
                </span>
        </div>
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4">
        <label>CANAL:</label>
        <select   <?php echo $desactivar?'disabled':'' ?>   id="select_canal"
                class="selectpicker form-control show-tick no-padding" data-live-search="true"
                data-size="5">
            <option value="0" selected>-Seleccione-</option>
            <?php
            $canal = new Canal('SELECT');
            $datosPais = $canal->selectAll();
            foreach ($datosPais as $row) {
                if($canal_id!=$row->canal_id)
                {
                    echo '<option value="' . $row->canal_id . '">' . $row->canal_nombre . '</option>';

                }else{
                    echo '<option selected value="' . $row->canal_id . '">' . $row->canal_nombre . '</option>';
                }
              }
            ?>
        </select>
    </div>



    <div>
        <hr class="line-frame">
    </div>
    <div class="form-group">
        <div class="form-group col-xs-5 col-sm-2 col-md-2">
            <label>FOB TOTAL:</label>
            <div class="input-group  col-xs-12 ">
                <input  disabled id="adua-input-fob-total" type="text" class='form-control'>


                <!--<input disabled type="text" class='form-control'
                       value="<?php /*echo $fob_total; */?>">-->
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>

        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>FLETE: </label>
            <div class="input-group  col-xs-12 ">
            <input disabled id="adua-input-total-flete" type="text" class="form-control" placeholder="0.00 $">
            <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
        </div>
        </div>
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>PRIMA NETA:</label>
            <div class="input-group  col-xs-12 ">
                <input disabled id="adua-input-prima-neta" value="<?php echo $prima_neta?>" type="text" class="form-control" placeholder="0.00 $">
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>

        </div>
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>COSTO DE ORIGEN:</label>
            <div class="input-group  col-xs-12 ">
                <input disabled id="adua-input-cost_origen" value="<?php echo $costo_adicional?>" type="text" class="form-control" placeholder="0.00 $">
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>

        </div>



        <div class="form-group col-xs-6 col-sm-2 col-md-2">
            <label>THC:</label>
            <div class="input-group  col-xs-12 ">
                <input disabled  id="adua-input-thc-contenedor" type="text" class='form-control money'
                                                                 value="<?php echo $thc_contenedor; ?>" placeholder="">
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-2">
            <label>CIF MONTO TOTAL: </label>
            <div class="input-group  col-xs-12 ">
            <input disabled id="adua-input-monto-total" value="<?php echo $cif_total?>" type="text" class="form-control" placeholder="0.00 $">
            <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
        </div>
        </div>
        <div class="form-group col-xs-6 col-sm-2 col-md-2">
            <label>ADV:</label>
            <div class="input-group  col-xs-12 ">
                <input disabled id="adua-input-adv"  type="text" class='form-control money'>
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-2 col-md-2">
            <label>IGV:</label>
            <div class="input-group  col-xs-12 ">
                <input disabled  id="adua-input-igv" type="text" class='form-control money'>
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-2 col-md-2">
            <label>IPM:</label>
            <div class="input-group  col-xs-12 ">
                <input disabled id="adua-input-ipm" type="text" class='form-control money'>
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>

        <div class="form-group col-xs-6 col-sm-2 col-md-2">
            <label>DERECHOS ARANCE.:</label>
            <div class="input-group  col-xs-12 ">
                <input disabled id="adua-input-derecho" type="text" class='form-control money'>
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>

        <div class="form-group col-xs-6 col-sm-2 col-md-2">
            <label>PERCEPCION:</label>
            <div class="input-group  col-xs-12 ">
                <input disabled id="adua-input-percepcion" type="text" class='form-control money'>
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-2 col-md-2">
            <label>TIPO CAMBIO:</label>
            <div class="input-group  col-xs-12 ">
                <input disabled id="adua-input-tipo-camb" type="text" class='form-control money'>
                <span class="input-group-btn">
                        <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                </span>
            </div>
        </div>
        <div class="form-group col-xs-6 col-sm-2 col-md-2">
            <label>TOTAL:</label>
            <div class="input-group  col-xs-12 ">
                <input  disabled id="adua-input-total" type="text" class='form-control money'>
                <span class="input-group-btn">
                        <a class="btn btn-primary">S/.</a>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group text-right">
        <div>
            <hr class="line-frame">
        </div>
        <div class="col-md-12 no-padding text-right" style="padding-bottom: 15px; margin-bottom: 15px; display: ">
            <button   <?php echo $desactivar?'disabled':'' ?>   id="aduana-btn-guardar" type="button" class="btn btn-primary">Guardar</button>

            <button onclick="openDerechoEstimado()" type="button" class="btn btn-info" ><i class="fa fa-print"></i> Imprimir</button>
        </div>


    </div>





</div>
