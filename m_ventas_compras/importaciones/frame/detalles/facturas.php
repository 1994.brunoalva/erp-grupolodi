
<div class="panel-box" id="factura-box">
    <div></div>
    <div class="col-md-12 text-right" style="padding-bottom: 20px;">

        <button type="button"  onclick="descargarReporCost(<?php echo $_GET['id']?>)" class="btn btn-success"><i class="fa fa-calculator"></i> COSTEO DE IMPORTACION</button>
        <button type="button"  data-toggle="modal" data-target="#modal_add_factura" class="btn btn-primary"><i class="fa fa-plus"></i> AGREGAR</button>
    </div>


    <div class="row">
        <table class="table table-striped table-bordered" style="width:100%">
            <tr style="background-color: #007ac3; color: white">
                <th style="border-right-color: #007ac3" class="text-center">FECHA</th>
                <th style="border-right-color: #007ac3" class="text-center">TIPO DOCUMENTO</th>
                <th style="border-right-color: #007ac3" class="text-center">NUMERO</th>
                <th style="border-right-color: #007ac3" class="text-center">PROVEEDOR</th>
                <th style="border-right-color: #007ac3" class="text-center">TC</th>
                <th style="border-right-color: #007ac3" class="text-center">MONTO</th>
                <th style="border-right-color: #007ac3" class="text-center">DOC</th>
                <th></th>
            </tr>
            <tr>
                <td class="text-center">{{formatDate(dataPri.fecha)}}</td>
                <td class="text-center">{{dataPri.tipo}}</td>
                <td class="text-center">{{dataPri.num}}</td>
                <td class="text-center">{{dataPri.proveedor}}</td>
                <td class="text-center">{{dataPri.tc}}</td>
                <td class="text-center">{{formatNumber(dataPri.monto)}}</td>
                <td class="text-center"><a v-if="dataPri.file.length>0" class="btn btn-info"  target="_blank" :href="'../imagenes/doc-facturas-import/' "><i class="fa fa-file"></i></a></td>
                <td class="text-center"> <button type="button" data-toggle="modal" data-target="#modal_editar_factura"  class="btn btn-primary"><i class="fa fa-edit"></i></button></td>
            </tr>

            <tr v-for="(item, index) in listaFact">
                <td class="text-center">{{ formatDate(item.fecha)}}</td>
                <td class="text-center">{{item.tipo_doc}}</td>
                <td class="text-center">{{item.numero}}</td>
                <td class="text-center">{{item.proveedor}}</td>
                <td class="text-center">{{item.taza_cambio}}</td>
                <td class="text-center">{{formatNumber(item.monto)}}</td>
                <td class="text-center"><a class="btn btn-info" v-if="item.archivo.length>0" target="_blank" :href="'../imagenes/doc-facturas-import/'+item.archivo"><i class="fa fa-file"></i></a></td>
                <td class="text-center"> <button type="button" v-on:click="eliminarFac(item.impor_fac_id)" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
            </tr>

        </table>
    </div>




    <div class="modal fade" id="modal_add_factura" tabindex="-1" role="dialog" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Registro de facturas</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form id="formulario_reg_fac" action="#" v-on:submit.prevent="guardarFactu()">
                        <div class="container-fluid">

                            <div class="form-group col-xs-4 col-sm-5 col-md-5">
                                <label class="col-xs-12 no-padding">PROVEEDOR:</label>
                                <div class="input-group col-xs-12">
                                    <input autocomplete="off" v-model="itemSelect.nombre" name="proveedor" required  type="text" class="form-control">
                                    <span class="input-group-btn">
                                            <button type="button" data-toggle="modal" data-target="#modal_buscar_proveedor_factura" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                <label class="col-xs-12 no-padding">FECHA:</label>
                                <div class="input-group col-xs-12">
                                    <input v-model="dataR.fecha" required name="fecha" type="date" class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">TIPO DOCUMENTO</label>
                                <div class="input-group col-xs-12">
                                    <select required v-model="dataR.tipo" name="tipo_doc"  class="form-control">
                                        <option  value="FACTURA">FACTURA</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">N. DOCUMENTO</label>
                                <div class="input-group col-xs-12">
                                    <input autocomplete="off" v-model="dataR.num" required name="num-doc" class="form-control col-xs-6">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">TIPO CAMBIO</label>
                                <div class="input-group col-xs-12">
                                    <input @keypress="onlyNumber" autocomplete="off" v-model="dataR.tc" class="form-control" required name="tc">
                                    <span class="input-group-btn">
                                            <a class="btn btn-primary">S/.</a>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">MONTO</label>
                                <div class="input-group col-xs-12">
                                    <input @keypress="onlyNumber" autocomplete="off" v-model="dataR.monto" required name="monto" class="form-control">
                                    <span class="input-group-btn">
                                            <a class="btn btn-primary"><i class="fa fa-dollar-sign"></i></a>
                                    </span>

                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">DOCUMENTO</label>
                                <div class="input-group col-xs-12">
                                    <button type="button" onclick="$('#file-factura').click()" class="btn btn-info">SELECCIONAR</button>
                                    <input  id="file-factura" type="file" class="form-control" style="display: none">
                                </div>
                            </div>


                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">
                            <button type="submit"  class="btn btn-primary">
                                Guardar
                            </button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_editar_factura" tabindex="-1" role="dialog" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Registro de facturas</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form id="formulario_reg_fac" action="#" v-on:submit.prevent="guardarFactuInvoice()">
                        <div class="container-fluid">

                            <div class="form-group col-xs-4 col-sm-5 col-md-5">
                                <label class="col-xs-12 no-padding">PROVEEDOR:</label>
                                <div class="input-group col-xs-12">
                                    <input disabled v-model="dataPri.proveedor" name="proveedor" required  type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-xs-4 col-sm-4 col-md-4">
                                <label class="col-xs-12 no-padding">FECHA:</label>
                                <div class="input-group col-xs-12">
                                    <input v-model="dataPri.fecha" required name="fecha" type="date" class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">TIPO DOCUMENTO</label>
                                <div class="input-group col-xs-12">
                                    <input disabled required v-model="dataPri.tipo" name="tipo_doc"  class="form-control">


                                </div>
                            </div>

                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">N. DOCUMENTO</label>
                                <div class="input-group col-xs-12">
                                    <input autocomplete="off" v-model="dataPri.num"  class="form-control col-xs-6">
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">TIPO CAMBIO</label>
                                <div class="input-group col-xs-12">
                                    <input @keypress="onlyNumber" autocomplete="off" v-model="dataPri.tc" class="form-control" required name="tc">
                                    <span class="input-group-btn">
                                            <a class="btn btn-primary">S/.</a>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">MONTO <i class="fa fa-dollar-sign"></i></label>
                                <div class="input-group col-xs-12">
                                    <input disabled autocomplete="off" :value="formatNumber(dataPri.monto)" required name="monto" class="form-control">
                                    <span class="input-group-btn">
                                            <button v-on:click="recargarData()" type="button" class="btn btn-primary"><i class="fa fa-sync"></i></button>
                                    </span>

                                </div>
                            </div>
                            <div class="form-group col-xs-4 col-sm-4 col-md-3">
                                <label class="col-xs-12 no-padding">DOCUMENTO</label>
                                <div class="input-group col-xs-12">
                                    <button type="button" onclick="$('#file-factura2').click()" class="btn btn-info">SELECCIONAR</button>
                                    <input  id="file-factura2" type="file" class="form-control" style="display: none">
                                </div>
                            </div>


                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">
                            <button type="submit"  class="btn btn-primary">
                                Guardar
                            </button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_buscar_proveedor_factura" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header no-border no-padding">
                    <div class="modal-header text-center color-modal-header">
                        <h3 class="modal-title">Proveedores</h3>
                    </div>
                </div>
                <div class="modal-body  no-border">
                    <form action="#">
                        <div class="container-fluid">

                            <div class="form-group col-xs-12 no-padding" style="">
                                <table  class="table table-striped table-bordered table-hover"
                                        style="width: 100%;">
                                    <thead class="bg-head-table">
                                    <tr>
                                        <th class="text-center">NOMBRE</th>
                                        <th class="text-center">RUC</th>
                                        <th class="text-center">TIPO</th>
                                        <th class="text-center">OPCION</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="item in listaProvedoresSel">
                                        <td class="text-center">{{item.nombre}}</td>
                                        <td class="text-center">{{item.ruc}}</td>
                                        <td class="text-center">{{item.tipo}}</td>
                                        <td class="text-center"> <button type="button" v-on:click="selectetProvedor(item)" class="btn btn-success"><i class="fa fa-check"></i></button></td>
                                    </tr>
                                    </tbody>

                                </table>
                            </div>


                            <!--<div class="form-group  col-xs-12 no-padding">
                                <label class="col-xs-12 no-padding">Descripccion:</label>
                                <textarea id="modal-buscar-proveedor-input-descripccion" class="form-control" type="text" rows="3" required style="resize: none; overflow: hidden;"></textarea>
                            </div>-->
                        </div>
                        <div class="container-fluid">
                            <hr class="line-frame-modal">
                        </div>
                        <div class="container-fluid text-right">

                            <!-- <a type="submit" id="modal-buscar-proveedor-btn-guardar" class="btn btn-primary">
                                 Guardar
                             </a>
                             <button type="button" id="modal-buscar-proveedor-btn-limpiar" class="btn btn-default">
                                 Limpiar
                             </button>-->

                            <button type="button"  class="btn btn-danger"
                                    data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>