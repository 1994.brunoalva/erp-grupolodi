<div class="table-responsive col-lg-12 no-padding">
    <table class="table table-striped table-bordered table-hover"
           id="table-folder-import">
        <style>
            .bg-head-table tr th {
                padding: 0;
            }

            table.dataTable thead > tr > th.sorting {
                padding-right: 13px;
            }

            /* .pagination > li > a{
                 background: red;
             }*/
        </style>
        <thead class="bg-head-table">
        <tr  style="background-color: #007ac3; color: white">

            <th  style="border-right-color: #007ac3"  class="text-center">AÑO</th>
            <th  style="border-right-color: #007ac3"  class="text-center">FOLDER</th>
            <th  style="border-right-color: #007ac3"  class="text-center">FECHA</th>
            <th  style="border-right-color: #007ac3"  class="text-center" style="min-width: 250px;">IMPORTADOR</th>
            <th  style="border-right-color: #007ac3" class="text-center" style="min-width: 250px;">PROVEEDOR</th>
            <th  style="border-right-color: #007ac3" class="text-center">PRODUCTO</th>
            <th  style="border-right-color: #007ac3" class="text-center" style="min-width: 100px;">MARCA</th>
            <th style="border-right-color: #007ac3"  class="text-center">PAIS</th>
            <th style="border-right-color: #007ac3"  class="text-center">PUERTO</th>
            <th style="border-right-color: #007ac3"  class="text-center">CONTE</th>
            <th style="border-right-color: #007ac3"  class="text-center">Qty</th>
            <th  style="border-right-color: #007ac3" class="text-center" style="min-width: 100px;">ETD</th>
            <th style="border-right-color: #007ac3"  class="text-center">MONTO</th>
            <th style="border-right-color: #007ac3"  class="text-center">DETALLE</th>
            <th  style="border-right-color: #007ac3" class="text-center">ESTADO</th>
            <th class="text-center">PROCESAR</th>
        </tr>
        </thead>
        <tbody class="text-center">
        <?php

        include '../ajax/Folder/getAllFolder-in.php';
        include '../ajax/Pagos/getForID-in.php';

        include '../ajax/Orden/getForID-in.php';
        include '../ajax/Embarque/getAllForID-in.php';


        $monto_total = '';
        $o_id = '';
        $etd = '';


        $resultFolder = getAllFolder();
        $indice = 0;

        foreach ($resultFolder as $item) {
            $emp_nombre = '';
            $idEmpresa=0;
            if (isset($item->emp_id->emp_nombre)) {
                $emp_nombre = $item->emp_id->emp_nombre;
                $idEmpresa= $item->emp_id->emp_id;
            }
            $monto_total = '';
            $dataPagos = getAllPagosForId($item->impor_id);

            if ($dataPagos) {
                $monto_total = $dataPagos->pago_mon_total;
            }

            $dataOrden = getAllOrdenForId($item->impor_id);
            $o_id = $dataOrden->ord_id;
            $dataEmbarque = getEmbarqueForId($o_id);
            $etd = '';
            if ($dataEmbarque) {
                $etd = $dataEmbarque->emb_etd;
            }

            $dia = date("d", strtotime($item->impor_fech_profo));
            $mes = date("m", strtotime($item->impor_fech_profo));
            $anoi = date("yy", strtotime($item->impor_fech_profo));

           // $fecha_import= $dia . " " .$tools->abreviaturaMes(intval($mes)-1);
            $fecha_import= $dia . "-" .$mes."-".$anoi;
            ?>
            <tr>

                <td id="ser-fol"><?php echo $item->impor_folder; ?></td>
                <td id="num-fol"><?php echo $item->impor_nrfolder; ?></td>
                <td><?php echo $fecha_import ?></td>
                <td><?php echo $emp_nombre; ?></td>
                <td><?php echo $item->provee_id->provee_desc; ?></td>
                <td><?php echo $item->cat_id->cat_nombre; ?></td>
                <td><?php echo $item->mar_id->mar_nombre; ?></td>
                <td><?php echo $item->pais_id->pais_nombre; ?></td>
                <td><?php echo $item->puerto_id->puerto_nombre; ?></td>
                <td><?php echo $item->ticon_id->ticon_nombre; ?></td>
                <td><?php echo $item->impor_qty; ?></td>
                <td><?php echo $etd; ?></td>
                <td><?php echo  $monto_total>0?'$'.$monto_total:''; ?></td>
                <td>
                    <a href="detalle-folio.php?id=<?php echo $item->impor_id ?>"
                       class="btn btn-info btn-sm fa fa-edit btn-op1" title="Ver detalles de importacion"></a>

                </td>
                <td>
                    <a href="#" onclick="inavilitarSelect(<?php echo $item->estado ?>)"
                       class="btn btn-warning btn-sm fa fa-edit btn-op2" title="Ver estado de importacion"></a>
                    <input type="text" value="<?php echo $item->impor_id ?>" class="no-display">
                </td>
                <td>
                    <?php
                    if ($item->estado ==1){ ?>
                        <a id="botonProcesar<?php echo $item->impor_id ?>"  href="#"
                           class="btn btn-success btn-sm fa fa-edit btn-op3" onclick="procesar('<?php echo $item->impor_folder."-".$item->impor_nrfolder ?>',<?php echo $idEmpresa ?>,<?php echo $item->impor_id ?>,<?php echo $o_id ?>)" title="Procesar importacion"></a>
                    <?php }else{ ?>
                    <a disabled="true" href="#"
                       class="btn btn-danger btn-sm fa fa-edit" onclick="swal('Este folder ya esta prosesado!')" title="Importacion Procesada"></a>
                    <?php    }
                    ?>

                    <input type="text" value="<?php echo $item->impor_id ?>" class="no-display">

                </td>

            </tr>
            <?php
        }
        ?>
        </tbody>

    </table>
</div>
<script>

    $(document).ready(function () {

        $('#table-folder-import').DataTable({
            scrollY: false,
            scrollX: true,
            /* scrollCollapse: false,*/
            paging: true,
            autoWidth: false,
            lengthMenu: [[10, 20, 100, -1], [10, 20, 100, "All"]],
            "order": [[ 2, "desc" ]],
            fixedColumns: {
                leftColumns: 3,
            },
            language: {
                url: '../assets/Spanish.json'
            }
        });

    });
</script>