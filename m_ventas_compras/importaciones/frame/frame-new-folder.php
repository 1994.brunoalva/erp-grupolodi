<div id="content-new-folder">

    <form id="frame-new-folder-form">
        <div class="form-group">
            <div class="row">
                <div class="form-group col-xs-6 col-sm-6 col-md-3">
                    <label class="col-xs-12 no-padding"> Folio:</label>
                    <div class="input-group col-xs-12">
                        <input  id="frame-new-folder-input-serie" disabled type="text" class="form-control input-number"
                               aria-describedby="basic-addon1" maxlength="4"
                               value="<?=date("Y")?>">
                        <span class="input-group-addon" id="basic-addon1">N°</span>
                        <input required id="frame-new-folder-input-numero" type="text" class="form-control" placeholder="Folio"
                               aria-describedby="basic-addon1" maxlength="5">
                    </div>
                </div>
                <div class="form-group col-xs-6 col-sm-6 col-md-3">
                    <label class="col-xs-12 no-padding"> Incoterm :</label>
                    <div class="input-group col-xs-12 no-padding">
                        <select id="select_incoterm" required
                                class="selectpicker form-control no-padding"
                                data-live-search="true"
                                data-size="5">
                            <option value="0">-Seleccione-</option>
                            <?php
                            $incoterm = new Incoterm('SELECT');
                            $datos = $incoterm->selectAll();
                            foreach ($datos as $row) {
                                echo '<option value="' . $row->inco_id . '">' . $row->inco_nombre . '</option>';
                            }
                            ?>
                        </select>
                        <span class="input-group-btn">
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#modal_incoterm">
                                    <i class="fa fa-plus"></i></button>
                            </span>
                    </div>
                </div>
                <div class="form-group col-xs-6 col-sm-6 col-md-3">
                    <label class="col-xs-12 no-padding"> Tipo de CTN:</label>
                    <div class="input-group col-xs-12 no-padding">
                        <select id="select_tipo_contenedor" required
                                class="selectpicker form-control no-padding"
                                data-live-search="true"
                                data-size="5">
                            <option value="0">-Seleccione-</option>
                            <?php
                            $contenedor = new TipoContenedor('SELECT');
                            $datos = $contenedor->selectAll();
                            foreach ($datos as $row) {
                                echo '<option value="' . $row->ticon_id . '">' . $row->ticon_nombre . '</option>';
                            }
                            ?>
                        </select>
                        <span class="input-group-btn">
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#modal_tipo_contenedor">
                                    <i class="fa fa-plus"></i></button>
                            </span>
                    </div>
                </div>
                <div class="form-group col-xs-6 col-sm-6 col-md-3">
                    <label class="col-xs-12 no-padding">QTY CTN</label>
                    <div class="input-group number-spinner col-xs-12 no-padding">
                            <span class="input-group-btn">
                                <a class="btn btn-primary" data-dir="dwn">
                                    <span class="fa fa-minus"></span>
                                </a>
                            </span>
                        <input  required  id="frame-new-folder-qty" type="text" class="form-control text-center input-number" value="1"
                               maxlength="4">
                        <span class="input-group-btn">
                                <a class="btn btn-primary" data-dir="up">
                                    <span class="fa fa-plus"></span>
                                </a>
                            </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-6 col-sm-6 col-md-3">
                    <label class="col-xs-12 no-padding">Categoria:</label>
                    <div class="input-group col-xs-12 no-padding">
                        <select id="select_categoria" required
                                class="selectpicker form-control no-padding" data-live-search="true"
                                data-size="5">
                            <option value="0" selected>-Seleccione-</option>
                            <?php
                            $categoria = new Categoria('SELECT');
                            $datos = $categoria->selectAll();
                            foreach ($datos as $row) {
                                if ($row->cat_id!=6){
                                    echo '<option value="' . $row->cat_id . '">' . $row->cat_nombre . '</option>';
                                }

                            }
                            ?>
                        </select>
                        <span class="input-group-btn">
                                <button type="button" class="btn bg-primary">
                                    <i class="fa fa-grip-vertical fg-blanco"></i>
                                </button>
                            </span>
                       <!-- <span class="input-group-btn">
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#modal_categoria">
                                    <i class="fa fa-grip-vertical"></i>
                                </button>
                            </span>-->
                    </div>
                </div>
                <div class="form-group col-xs-6 col-sm-6 col-md-3">
                    <label for="marca">Marca:</label>
                    <div class="input-group col-xs-12 no-padding">
                        <select  required  id="select_marca"
                                class="selectpicker form-control no-padding" data-live-search="true"
                                data-size="5">
                            <option value="0" selected>-Seleccione-</option>
                        </select>
                        <span class="input-group-btn">
                                <button id="btn_select_marca" disabled type="button" class="btn btn-primary"
                                        data-toggle="modal" data-target="#modal_marca">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </span>
                    </div>

                </div>
                <div class="form-group col-xs-6 col-sm-6 col-md-3">
                    <label for="pais">País:</label>
                    <div class="input-group col-xs-12 no-padding">
                        <select id="select_pais" required
                                class="selectpicker form-control no-padding" data-live-search="true"
                                data-size="5">
                            <option value="0" selected>-Seleccione-</option>
                            <?php
                            $pais = new Pais('SELECT');
                            $datosPais = $pais->selectAll();
                            foreach ($datosPais as $row) {
                                echo '<option value="' . $row->pais_id . '">' . $row->pais_nombre . '</option>';
                            }
                            ?>
                        </select>
                        <span class="input-group-btn">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_pais">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </span>
                    </div>
                </div>
                <div class="form-group col-xs-6 col-sm-6 col-md-3">
                    <label for="puerto">Puerto:</label>
                    <div class="input-group col-xs-12 no-padding">
                        <select  required  id="select_puerto"
                                class="selectpicker form-control no-padding disabled" data-live-search="true"
                                data-size="5">
                            <option value="0" selected>-Seleccione-</option>
                        </select>
                        <span class="input-group-btn">
                                <button id="btn_select_puerto" type="button" disabled class="btn btn-primary"
                                        data-toggle="modal" data-target="#modal_puerto">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </span>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="form-group col-xs-12 col-sm-4 col-md-3">
                    <label class="col-xs-12 no-padding">Importador:</label>
                    <div class="input-group col-xs-12 no-padding">
                        <input  required  id="frame-new-folder-input_importador" class="form-control" type="text"
                                placeholder="Haga click para agregar importador">
                        <input  required  id="frame-new-folder-input_importador_id" class="form-control no-display" type="text">
                        <span class="input-group-btn">
                                <button id="frame-new-btn-add-importador" type="button" class="btn btn-primary"
                                        data-toggle="modal" data-target="#modal_empresa">
                                    <i class="fa fa-plus"></i></button>
                            </span>
                    </div>

                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3">
                    <label class="col-xs-12 no-padding">Proveedor:</label>
                    <div class="input-group col-xs-12 no-padding">
                        <input  required  id="frame-new-folder-input_proveedor" class="form-control" type="text"
                               placeholder="Haga click para agregar proveedor">
                        <input  required  id="frame-new-folder-input_proveedor_id" class="form-control no-display" type="text">
                        <span class="input-group-btn">
                                <button id="frame-new-btn-add-proveedor" type="button" class="btn btn-primary"
                                        data-toggle="modal" data-target="#modal_proveedor">
                                    <i class="fa fa-plus"></i></button>
                            </span>
                    </div>

                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3">
                    <label class="col-xs-12 no-padding">Proforma:</label>
                    <input  required  id="frame-new-folder-num-proforma" class="form-control" type="text" placeholder="ejem. LADY2019">
                    <!-- <input class="form-control m1" type="text" placeholder="">
                     <input class="form-control m2" type="text" placeholder="">
                     <input class="form-control m3" type="text" placeholder="">-->
                </div>
                <div class="form-group col-xs-12 col-sm-4 col-md-3">
                    <label class="col-xs-12 no-padding">Fecha/Proforma:</label>
                    <div class="input-group  col-xs-12 ">
                        <?php
                        $fecha = date('Y-m-d');
                        ?>
                        <input  id="frame-new-folder-input_fecha" type="date" required class='form-control'
                               data-date-format="YYYY MMMM  DD" style="margin-right: -60px;"
                               data-language='es' value="<?php echo $fecha; ?>">
                        <span class="input-group-btn">
                                    <a class="btn btn-primary" id="btn-calendar"><i class="fa fa-calendar"></i></a>
                            </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group text-right">
            <div>
                <hr class="line-frame">
            </div>
            <div class="col-md-12 no-padding">
                <div class="refresh-table">
                    <button id="" type="submit" class="btn btn-primary">Guardar</button>
                    <button id="frame-new-folder-btn-limpiar" type="button" class="btn btn-default">Limpiar</button>
                    <a href="import.php" id="frame-new-folder-btn-back" type="reset" class="btn btn-warning"><i
                                class="glyphicon glyphicon-chevron-left"></i>Salir
                    </a>
                </div>
            </div>

        </div>
    </form>
</div>
