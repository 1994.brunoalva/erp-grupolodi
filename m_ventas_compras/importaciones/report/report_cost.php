<?php
require "../../conexion/Conexion.php";
require_once "../../Lib/libexel/vendor/autoload.php";
require_once "../../Lib/mpdf/vendor/autoload.php";
require "../../utils/Tools.php";

$conexion = (new Conexion())->getConexion();
$tools = new Tools();

$idimpor= $_POST['idimport'];

$tasaCambioProductoInvoice= 3.65;
$tasaCambioSeguro= 3.65;

$sql = "SELECT  * FROM sys_impor_facturas WHERE impor_id = ".$idimpor;
$resultFacturas = $conexion->query($sql);



$sql = "SELECT 
  impor.*,
  emp.emp_nombre,
  emp.emp_logo,
  prove.provee_desc
FROM
  sys_impor_folder AS impor 
  INNER JOIN sys_empresas AS emp 
    ON impor.emp_id = emp.emp_id 
  INNER JOIN sys_com_proveedor AS prove 
    ON impor.provee_id = prove.provee_id 
    WHERE impor.impor_id = " . $idimpor;
//echo  $sql;
$result = $conexion->query($sql);

$importRes= $result->fetch_assoc();

//echo date("m", strtotime($importRes['impor_fech_profo']));

$nomMes = $tools->nombreMes(date("m", strtotime($importRes['impor_fech_profo'])));

$sql="SELECT 
  orde_detalle.*
FROM
  sys_com_orden AS orden 
  INNER JOIN sys_com_orden_detalle AS orde_detalle 
    ON orden.ord_id = orde_detalle.ord_id 
    WHERE orden.impor_id =".$idimpor;
$result = $conexion->query($sql);

$sql="SELECT * FROM sys_com_poliza WHERE impor_id = ".$idimpor;

$resultSeguro = $conexion->query($sql)->fetch_assoc();

$totalSeguro = $resultSeguro['pol_prima_total'];



$sql="SELECT * FROM sys_tasa_cambio WHERE tas_id =". $resultSeguro['tasa_camb_id'];

$resultSeguroTasa = $conexion->query($sql)->fetch_assoc();
$tasaCambioSeguro = $resultSeguroTasa['tas_comercial_venta'];

$totalSeguroSoles = $resultSeguro['pol_prima_total'] * $tasaCambioSeguro;

$totalInvoice=0;

//total de la tabla 1
$valorPlanillaD=0;
$duaD=0;
$flteD=0;
$invoiceD=0;
$seguroD=0;
$gastosVariosD=0;
$totalMED=0;

$valorPlanillaS=0;
$duaS=0;
$flteS=0;
$invoiceS=0;
$seguroS=0;
$gastosVariosS=0;
$totalMES=0;

//data prod
$totalPorcentaje =0;
$totalCantidad=0;
$totalValorFob=0;
$totalCostUni=0;

$htmlFacturas = "";

foreach ($resultFacturas as $rowFac){

    $montoSoles=$rowFac['monto']*$rowFac['taza_cambio'];

    $htmTempDol="";
    $htmTempSol="";

    if ($rowFac['tipo_doc']=="INVOICE"){
        $htmTempDol="<td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'>{$rowFac['monto']}</td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'>{$rowFac['monto']}</td>";

        $totalInvoice+=$rowFac['monto'] ;
        $montoTepSol= $rowFac['monto'] * $rowFac['taza_cambio'];
        $invoiceS+=$montoTepSol;
        $montoTepSol=number_format($montoTepSol, 2, '.', '');
        $htmTempSol="<td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'>$montoTepSol</td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'>$montoTepSol</td>";
    }elseif ($rowFac['proveedor']=="MAPFRE PERU"){
        $htmTempDol="<td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'>{$rowFac['monto']}</td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'>{$rowFac['monto']}</td>";
        $seguroD+=$rowFac['monto'];
        $montoTepSol= $rowFac['monto'] * $rowFac['taza_cambio'];

        $seguroS+=$montoTepSol;
        $montoTepSol=number_format($montoTepSol, 2, '.', '');
        $htmTempSol="<td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'>$montoTepSol</td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'>$montoTepSol</td>";
    }else{
        $htmTempDol="<td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'>{$rowFac['monto']}</td>
    <td style='border: 1px solid black'>{$rowFac['monto']}</td>";
        $gastosVariosD+=$rowFac['monto'] ;
        $montoTepSol= $rowFac['monto'] * $rowFac['taza_cambio'];
        $gastosVariosS+=$montoTepSol;

        $montoTepSol=number_format($montoTepSol, 2, '.', '');

        $htmTempSol="<td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black'>$montoTepSol</td>
    <td style='border: 1px solid black'>$montoTepSol</td>";

    }

    $htmlFacturas = $htmlFacturas. "  <tr>
    <td style='border: 1px solid black;text-align: center'>{$rowFac['fecha']}</td>
    <td style='border: 1px solid black;text-align: center'>{$rowFac['tipo_doc']}</td>
    <td style='border: 1px solid black;text-align: center'  colspan='2'>{$rowFac['numero']}</td>
    <td style='border: 1px solid black'>{$rowFac['proveedor']}</td>
    <td style='border: 1px solid black;text-align: center;width: 15px'>{$rowFac['ruc']}</td>
    <td style='border: 1px solid black'>{$rowFac['taza_cambio']}</td>
    $htmTempDol
    $htmTempSol
  </tr>";
}



$HTMLrOWprODU = "";
$contador=1;

$totalValorFob=$totalInvoice;

$invoiceD = $totalInvoice;


$totalMES+=$gastosVariosS+$seguroS+$invoiceS;

$totLiNVOCE=number_format($totalInvoice, 2, '.', '');
$totalSeguroSoles=number_format($totalSeguroSoles, 2, '.', '');
$totalMES=number_format($totalMES, 2, '.', '');
$totalValorFob=number_format($totalValorFob, 2, '.', '');

$totalMED += $totalInvoice+$gastosVariosD+$seguroD;
//$totalMED += $totalSeguro;
//$seguroS=$totalSeguroSoles;

foreach ($result as $row){

    $precioTotal = str_replace(",","",$row['prod_total']);
    $precioTotal = str_replace(",","",$precioTotal);

    //$totalCostUni += str_replace(",","",$row['prod_precio']);
    $totalTemp = $precioTotal;
    $totalCantidad += $row['prod_cantidad'];
    $precioUnitarioProD=0;

    $tempPorcen = ($totalTemp*100)/$totalInvoice;

    $tempProceRow = number_format($tempPorcen, 2, '.', '');
    $totalPorcentaje +=$tempProceRow;

    $tempPlanillaD = ($valorPlanillaD * $tempPorcen)/100;
    $tempFleteD = ($flteD * $tempPorcen)/100;
    $TempInvoiceD = ($invoiceD * $tempPorcen)/100;
    $TempSeguroD = ($seguroD * $tempPorcen)/100;
    $TempGastosVariosD = ($gastosVariosD * $tempPorcen)/100;
    $TempTotalMED = $tempPlanillaD+$tempFleteD+$TempInvoiceD+$TempSeguroD+$TempGastosVariosD;

    $precioUnitarioProD = $TempTotalMED/$row['prod_cantidad'];

    $totalCostUni +=$precioUnitarioProD;

        //$TempInvoice= number_format($TempInvoice, 2, '.', '');
    $tempPlanillaD= number_format($tempPlanillaD, 2, '.', '');
    $tempFleteD= number_format($tempFleteD, 2, '.', '');
    $TempSeguroD= number_format($TempSeguroD, 2, '.', '');
    $TempGastosVariosD= number_format($TempGastosVariosD, 2, '.', '');
    $TempTotalMED= number_format($TempTotalMED, 2, '.', '');
    $precioUnitarioProD= number_format($precioUnitarioProD, 2, '.', '');
    $TempInvoiceD= number_format($TempInvoiceD, 2, '.', '');

    $tempPlanillaS = ($valorPlanillaS * $tempPorcen)/100;
    $tempFleteS = ($flteS * $tempPorcen)/100;
    $TempInvoiceS = ($totalMES * $tempPorcen)/100;
    $TempSeguroS = ($seguroS * $tempPorcen)/100;
    $TempGastosVariosS = ($gastosVariosS * $tempPorcen)/100;




    $TempTotalMES = $tempPlanillaS+$tempFleteS+$TempInvoiceS+$TempSeguroS+$TempGastosVariosS;

    $temCosUnitarioSol = $TempTotalMES/$row['prod_cantidad'];

    $tempPlanillaS= number_format($tempPlanillaS, 2, '.', '');
    $tempFleteS= number_format($tempFleteS, 2, '.', '');
    $TempSeguroS= number_format($TempSeguroS, 2, '.', '');
    $TempGastosVariosS= number_format($TempGastosVariosS, 2, '.', '');
    $TempTotalMES= number_format($TempTotalMES, 2, '.', '');
    $TempInvoiceS= number_format($TempInvoiceS, 2, '.', '');


    $temCosUnitarioSol= number_format($temCosUnitarioSol, 2, '.', '');

    $HTMLrOWprODU = $HTMLrOWprODU . "
      <tr>
    <td style='border: 1px solid black;width: 20px;text-align: center'>$contador</td>
    <td style='border: 1px solid black;text-align: right'>$tempProceRow%</td>
    <td style='border: 1px solid black;text-align:right;width: 15px'>{$row['prod_cantidad']}</td>
    <td style='border: 1px solid black;text-align:right'>{$row['prod_total']}</td>
    <td style='border: 1px solid black;width: 50px'>{$row['prod_nombre']}</td>
    <td style='border: 1px solid black;text-align:right'>$precioUnitarioProD</td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black;text-align: right'>$tempPlanillaD</td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black;text-align: right'>$tempFleteD</td>
    <td style='border: 1px solid black;text-align: right'>$TempInvoiceD</td>
    <td style='border: 1px solid black;text-align: right'>$TempSeguroD</td>
    <td style='border: 1px solid black;text-align: right'>$TempGastosVariosD</td>
    <td style='border: 1px solid black;text-align: right'>$TempTotalMED</td>
    <td style='border: 1px solid black;text-align: right'>$tempPlanillaS</td>
    <td style='border: 1px solid black'></td>
    <td style='border: 1px solid black;text-align: right'>$tempFleteS</td>
    <td style='border: 1px solid black;text-align: right'>$TempInvoiceS</td>
    <td style='border: 1px solid black;text-align: right'>$TempSeguroS</td>
    <td style='border: 1px solid black;text-align: right'>$TempGastosVariosS</td>
    <td style='border: 1px solid black;text-align: right'>$TempTotalMES</td>
    <td style='border: 1px solid black;text-align: right'>$temCosUnitarioSol</td>
  </tr>
    ";
    $contador++;

}



$valorPlanillaD=number_format($valorPlanillaD, 2, '.', '');
$duaD=number_format($duaD, 2, '.', '');
$flteD=number_format($flteD, 2, '.', '');
$invoiceD=number_format($invoiceD, 2, '.', '');
$seguroD=number_format($seguroD, 2, '.', '');
$gastosVariosD=number_format($gastosVariosD, 2, '.', '');
$totalMED=number_format($totalMED, 2, '.', '');

$valorPlanillaS=number_format($valorPlanillaS, 2, '.', '');
$duaS=number_format($duaS, 2, '.', '');
$flteS=number_format($flteS, 2, '.', '');
$invoiceS=number_format($invoiceS, 2, '.', '');
$seguroS=number_format($seguroS, 2, '.', '');
$gastosVariosS=number_format($gastosVariosS, 2, '.', '');
$totalMES=number_format($totalMES, 2, '.', '');
$totalCostUni=number_format($totalCostUni, 2, '.', '');


$html="<div>
<table>
  <tr>
    <td><img src='../../imagenes/logo/{$importRes['emp_logo']}'></td>  
  </tr>
  <tr>
 
    <td colspan='21' style='text-align: center;font-weight: bold;font-size: 20px'>COSTEO DE IMPORTACION</td>
  </tr>

</table>


<table>
  <tr>
    <th>{$importRes['emp_nombre']}</th>
    <td></td>
    <td>FOLDER :</td>
    <td>{$importRes['impor_nrfolder']}-{$importRes['impor_folder']}</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>F. NUMERACION:</td>
    <td>{$importRes['impor_fech_profo']}</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>IMPORTACION:</td>
    <td>$nomMes</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>PROVEEDOR:</td>
    <td>{$importRes['provee_desc']}</td>
  </tr>
</table>

</div>

<div>
<table>
<tr>
 
    <td colspan='7'></td>
    <td style='border: 1px solid black;text-align:center;font-weight: bold' colspan='7'>VALORES EN DOLARES</td>
    <td style='border: 1px solid black;text-align:center;font-weight: bold' colspan='7'>VALOR EN SOLES</td>
  </tr>
  <tr>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>FECHA</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>TIPO DOCUMENTO</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'  colspan='2'>NUMERO</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>PROVEEDOR</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>RUC</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>TC</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>VALOR PLANILLA</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>DUA</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>FLETES</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>INVOICE</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>SEGURO</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>GASTOS VARIOS</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>TOTAL M.E</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>VALOR PLANILLA</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>DUA</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>FLETES</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>INVOICE</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>SEGURO</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>GASTOS VARIOS</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>TOTAL M.N</td>
  </tr>
  
 $htmlFacturas
  
  
  
   <tr>
    
   
    <td colspan='7'></td>
    <td style='border: 1px solid black;font-weight: bold'>$valorPlanillaD</td>
    <td style='border: 1px solid black;font-weight: bold'>$duaD</td>
    <td style='border: 1px solid black;font-weight: bold'>$flteD</td>
    <td style='border: 1px solid black;font-weight: bold'>$invoiceD</td>
    <td style='border: 1px solid black;font-weight: bold'>$seguroD</td>
    <td style='border: 1px solid black;font-weight: bold'>$gastosVariosD</td>
    <td style='border: 1px solid black;font-weight: bold'>$totalMED</td>
    <td style='border: 1px solid black;font-weight: bold'>$valorPlanillaS</td>
    <td style='border: 1px solid black;font-weight: bold'>$duaS</td>
    <td style='border: 1px solid black;font-weight: bold'>$flteS</td>
    <td style='border: 1px solid black;font-weight: bold'>$invoiceS</td>
    <td style='border: 1px solid black;font-weight: bold'>$seguroS</td>
    <td style='border: 1px solid black;font-weight: bold'>$gastosVariosS</td>
    <td style='border: 1px solid black;font-weight: bold'>$totalMES</td>
    
  </tr>
  
</table>

</div>

<div>
<table>
<tr>
 
    <td colspan='7'></td>
    <td style='border: 1px solid black;text-align:center;font-weight: bold' colspan='7'>VALORES EN DOLARES</td>
    <td style='border: 1px solid black;text-align:center;font-weight: bold' colspan='7'>VALOR EN SOLES</td>
  </tr>
  <tr>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>ITEM</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>%</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>CANTIDAD</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>VALORFOB</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>DETALLE</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>COSTO UNITARIO</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>SET o LL</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>VALOR PLANILLA</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>DUA</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>FLETES</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>INVOICE</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>SEGURO</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>GASTOS VARIOS</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>TOTAL M.E</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>VALOR PLANILLA</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>DUA</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>FLETES</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>INVOICE</td>
    <td style='border: 1px solid black;font-weight: bold;text-align:center'>SEGURO</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>GASTOS VARIOS</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>TOTAL M.N</td>
    <td style='border: 1px solid black;word-wrap: break-word;font-weight: bold;text-align:center'>COSTO UNITARIO M.N</td>
  </tr>
  
 $HTMLrOWprODU
  <tr>
    <td style='border: 1px solid black;font-weight: bold'></td>
    <td style='border: 1px solid black;font-weight: bold'>$totalPorcentaje</td>
    <td style='border: 1px solid black;font-weight: bold'>$totalCantidad</td>
    <td style='border: 1px solid black;font-weight: bold'>$totalValorFob</td>
    <td style='border: 1px solid black;font-weight: bold'></td>
    <td style='border: 1px solid black;font-weight: bold'>$totalCostUni</td>
    <td style='border: 1px solid black;font-weight: bold'></td>
    <td style='border: 1px solid black;font-weight: bold'>$valorPlanillaD</td>
    <td style='border: 1px solid black;font-weight: bold'>$duaD</td>
    <td style='border: 1px solid black;font-weight: bold'>$flteD</td>
    <td style='border: 1px solid black;font-weight: bold'>$invoiceD</td>
    <td style='border: 1px solid black;font-weight: bold'>$seguroD</td>
    <td style='border: 1px solid black;font-weight: bold'>$gastosVariosD</td>
    <td style='border: 1px solid black;font-weight: bold'>$totalMED</td>
    <td style='border: 1px solid black;font-weight: bold'>$valorPlanillaS</td>
    <td style='border: 1px solid black;font-weight: bold'>$duaS</td>
    <td style='border: 1px solid black;font-weight: bold'>$flteS</td>
    <td style='border: 1px solid black;font-weight: bold'>$invoiceS</td>
    <td style='border: 1px solid black;font-weight: bold'>$seguroS</td>
    <td style='border: 1px solid black;font-weight: bold'>$gastosVariosS</td>
    <td style='border: 1px solid black;font-weight: bold'>$totalMES</td>
    <td style='border: 1px solid black;font-weight: bold'></td>
  </tr>
  </table>

</div>
";


echo $html;

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
$spreadsheet = $reader->loadFromString($html);
/*
$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
$writer->save('write.xlsx');*/
$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
$writer->save("report cos.xlsx");


