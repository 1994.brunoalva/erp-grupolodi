
<?php
$indexRuta=1;
require_once '../model/model.php';
require "../model/Banco.php";
include  '../ajax/Folder/getAllForID-in.php';

$dataFolio = getFolderForID($_GET['id']);

$categorias= new Categoria('SELECT');
$presentaciones = new Precentacion('SELECT');

$banco = new Banco();

$listaCategoria = $categorias->selectAll();
$listaPre = $presentaciones->selectAll();
//print_r($listaPre);

$desactivar= $dataFolio->estado==0;

$listaBancos = $banco->getLista();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">



    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../assets/sweetalert/sweetalert.css"rel="stylesheet">
    <script src="../assets/sweetalert/sweetalert.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script>

        const folder = '<?php echo  $dataFolio->impor_nrfolder?>';
        const procedencia ='<?php echo $dataFolio->pais_id->pais_nombre ?>';
        const marca = '<?php echo  $dataFolio->mar_id->mar_nombre ?>';
        const cantidad = '<?php echo  $dataFolio->impor_qty." x ". $dataFolio->ticon_id->ticon_nombre ?>';
        const idImpor = '<?php echo $dataFolio->impor_id ?>' ;
        const catTe = "OTROS";

    </script>
    <style>
        .over-clicke{
            cursor: pointer;
        }
        .over-clicke:hover{
            background-color: #d2d2d2;
        }
    </style>
</head>

<body>
<div id="wrapper">
    <?php

    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li> -->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Detalles de Folder: </strong> <span id="nom-tad-folder">Folio</span>

                                        </h2>
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <a id="btn-salir" href="import.php" type="reset" class="btn btn-warning"><i
                                                class="glyphicon glyphicon-chevron-left"></i>Salir
                                        </a>
                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>
                            <div id="body-new-folder" class="col-xs-12 col-sm-12 col-md-12">
                                <?php

                                echo '<input id="folio_input_id" value="'.$dataFolio->impor_id.'" class="no-display">';
                                ?>

                                <div id="content-detalle-folder">
                                    <form>
                                        <div class="form-group">
                                            <div class="container-fluid row">
                                                <ul class="nav nav-tabs ">
                                                    <li class="active">
                                                        <a data-toggle="tab" href="#m1" class="no-padding">
                                                            <button onclick="setNombreDetalle('Folio')" id="list-detalle-btn-folio" type="reset" class="btn btn-primary form-group"
                                                                    style="font-size: 12px; font-weight: bold;">
                                                                <i class="fa fa-folder fa-2x"></i>
                                                                Folio
                                                            </button>
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-toggle="tab" href="#m2" class="no-padding">
                                                            <button  onclick="setNombreDetalle('Proveedor')"  id="list-detalle-btn-proveedor" type="reset" class="btn btn-success form-group"
                                                                    style="font-size: 12px; font-weight: bold;">
                                                                <i class="fa fa-user-friends fa-2x"></i>
                                                                Proveedor
                                                            </button>
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-toggle="tab" href="#m3" class="no-padding">
                                                            <button  onclick="setNombreDetalle('Productos')"  type="reset" class="btn btn-info form-group"
                                                                    style="font-size: 12px; font-weight: bold;">
                                                                <i class="fas fa-sort-size-down-alt fa-2x"></i></i>
                                                                Productos
                                                            </button>
                                                        </a>
                                                    </li>
                                                    <!------------------------------------------>
                                                    <li class="">
                                                        <a data-toggle="tab" href="#m4" class="no-padding">
                                                            <button  onclick="setNombreDetalle('Orden')" type="reset" class="btn btn-danger form-group"
                                                                    style="font-size: 12px; font-weight: bold;">
                                                                <i class="fa fa-file-invoice fa-2x"></i>
                                                                Orden
                                                            </button>
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-toggle="tab" href="#m8" onclick="funtionPagRecal()" class="no-padding">
                                                            <button id="tabpago" onclick="setNombreDetalle('Pagos')" type="reset" class="btn btn-warning form-group"
                                                                    style="font-size: 12px; font-weight: bold;">
                                                                <i class="fa fa-usd-square fa-2x"></i>
                                                                Pagos
                                                            </button>
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a  onclick="setNombreDetalle('Seguro')"  data-toggle="tab" href="#m5" class="no-padding">
                                                            <button type="reset" class="btn btn-success form-group"
                                                                    style="font-size: 12px; font-weight: bold;">
                                                                <i class="fa fa-shield-check fa-2x"></i>
                                                                Seguro
                                                            </button>
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-toggle="tab" href="#m6" class="no-padding">
                                                            <button id="tabpak" onclick="setNombreDetalle('Packing')" type="reset" class="btn btn-info form-group"
                                                                    style="font-size: 12px; font-weight: bold;">
                                                                <i class="fa fa-hand-holding-box fa-2x"></i>
                                                                Packing
                                                            </button>
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-toggle="tab" href="#m7" class="no-padding">
                                                            <button onclick="setNombreDetalle('Aduanas',asignador )" id="list-detalle-btn-aduana" type="reset" class="btn btn-primary form-group"
                                                                    style="font-size: 12px; font-weight: bold;">
                                                                <i class="fa fa-file-signature fa-2x"></i>
                                                                Aduanas
                                                            </button>
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a data-toggle="tab" href="#m88" class="no-padding">
                                                            <button onclick="setNombreDetalle('Facturas',facturas.getDataReload )" id="list-detalle-btn-facturas" type="reset" class="btn btn-info form-group"
                                                                    style="font-size: 12px; font-weight: bold;">
                                                                <i class="fa fa-file-signature fa-2x"></i>
                                                                Facturas
                                                            </button>
                                                        </a>
                                                    </li>

                                                </ul>

                                                <div class="tab-content">
                                                    <div id="m1" class="tab-pane fade  in active"><!--in active-->
                                                        <?php
                                                        include 'frame/detalles/folio.php';
                                                        ?>
                                                    </div>
                                                    <div id="m2" class="tab-pane fade">
                                                        <?php
                                                        include 'frame/detalles/proveedor.php';
                                                        ?>
                                                    </div>
                                                    <div id="m3" class="tab-pane fade">
                                                        <?php
                                                        include 'frame/detalles/producto.php';
                                                        ?>
                                                    </div>
                                                    <div id="m4" class="tab-pane fade">
                                                        <?php
                                                        include 'frame/detalles/orden.php';
                                                        ?>
                                                    </div>
                                                    <div id="m8" class="tab-pane fade ">
                                                        <?php
                                                        include 'frame/detalles/pagos.php';
                                                        ?>
                                                    </div>
                                                    <div id="m5" class="tab-pane fade ">
                                                        <?php
                                                        include 'frame/detalles/seguro.php';
                                                        ?>
                                                    </div>
                                                    <div id="m6" class="tab-pane fade">
                                                        <?php
                                                        include 'frame/detalles/packing.php';
                                                        ?>
                                                    </div>
                                                    <div id="m7" class="tab-pane fade ">
                                                        <?php
                                                        include 'frame/detalles/aduanas.php';
                                                        ?>
                                                    </div>
                                                    <div id="m88" class="tab-pane fade ">
                                                        <?php
                                                        include 'frame/detalles/facturas.php';
                                                        ?>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <?php
/*                                include 'frame/frame-detalles-folder.php';
                                */?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    include '../modal/modal.php';
    ?>
    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>
    <!--<script type="module" src="../aConfig/Myjs/importaciones.js"></script>-->
    <script>

        var paking_listc=[];
        var paking_tab=[];
        var datos_table_n2=[];
        var alertGlo;
        var tabnow;
        var idOrdenPro=0;

    </script>
    <script  type="module" src="../aConfig/scripts/new-folder.js?red="></script>
    <script  type="module" src="../aConfig/scripts/folio.js"></script>
    <script  type="module" src="../aConfig/scripts/proveedor.js"></script>
    <script  type="module" src="../aConfig/scripts/producto.js?red="></script>
    <script  type="module" src="../aConfig/scripts/sku.js"></script>
    <script  type="module" src="../aConfig/scripts/add-producto.js?red="></script>
    <script  type="module" src="../aConfig/scripts/aduanas.js"></script>
    <script  type="module" src="../aConfig/scripts/orden.js"></script>
    <script  type="module" src="../aConfig/scripts/seguros.js"></script>
    <script  type="module" src="../aConfig/scripts/pagos.js"></script>
    <script  type="module" src="../aConfig/scripts/verProducto.js"></script>
    <script  type="module" src="../aConfig/scripts/producto-main.js"></script>
    <script   type="module" src="../aConfig/scripts/packing.js"></script>
    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script type="text/javascript" src="../assets/JsBarcode/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!--    <script type="text/javascript" src="https://unpkg.com/default-passive-events"></script>-->

    <script>
        <?php
        if (isset($_GET['tab'])){
            $tab = $_GET['tab'];
            if ($tab=="pk"){
                echo '   setTimeout(function () {
            $("#tabpak").click()
        },60)';
            }
            if ($tab=="pa"){
                echo '   setTimeout(function () {
            $("#tabpago").click()
        },60)';
            }
        }

        ?>

       /* setTimeout(function () {
            $("#tabpak").click()
        },50)*/
    </script>
    <script>
        var funtionPackContener;
        var funtionPagRecal;
        var funtionCambioMoneda;
        var funReable;
        var funCarga;

        var idUtlimaTasaCambio;
        var provedorAB=0;

    </script>
    <script>
        const facturas =  new Vue({
            el:"#factura-box",
            data:{
                inputdatafile:'',
                listaFact:[],
                listaProvedoresSel:[],
                itemSelect:{nombre:''},
                dataPri:{
                    idFact:'',
                    proveedor:'',
                    fecha:'',
                    tipo:'INVOICE',
                    num:'',
                    tc:'0.000',
                    monto:'',
                    file:''
                },
                dataR:{
                    fecha:'',
                    tipo:'FACTURA',
                    num:'',
                    tc:'',
                    monto:''
                }

            },
            methods: {
                getDataInvoice(){
                    $.ajax({
                        type: "POST",
                        url: "../ajax/FacturaImport/getInvoice.php",
                        data: {impor:$("#folio_input_id").val()},
                        success: function (reso) {
                            reso = JSON.parse(reso);
                            console.log(reso);
                            if (reso.res){
                                const DTA= reso.data;
                                facturas._data.dataPri={
                                    idFact:DTA.impor_fac_id,
                                        proveedor:DTA.proveedor,
                                        fecha:DTA.fecha,
                                        tipo:'INVOICE',
                                        num:DTA.ruc,
                                        tc:DTA.taza_cambio,
                                        monto:DTA.monto,
                                        file:DTA.archivo
                                }
                            }
                        }
                    });

                },
                recargarData(){
                    var monto = $("#pago-input-monto-total").val();
                    monto = monto.replace(/,/g, '');
                    this.dataPri.monto=monto
                    this.dataPri.proveedor=$("#frame-new-folder-input_proveedor").val()
                },
                getDataReload(){

                    if (this.dataPri.idFact==''){
                        var monto = $("#pago-input-monto-total").val();
                        monto = monto.replace(/,/g, '');
                        this.dataPri.monto=monto
                        this.dataPri.proveedor=$("#frame-new-folder-input_proveedor").val()
                    }

                },
                 formatNumber(num) {
                    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
                },
                 formatDate(date) {
                    if (date==''){
                        return date;
                    }else{
                        var d = new Date(date),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                        if (month.length < 2)
                            month = '0' + month;
                        if (day.length < 2)
                            day = '0' + day;

                        return [day, month, year ].join('-');
                    }


                },
                onlyNumber ($event) {
                    //console.log($event.keyCode); //keyCodes value
                    let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
                    if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                        $event.preventDefault();
                    }
                },
                eliminarFac(fact){
                    console.log(fact)
                    $.ajax({
                        type: "POST",
                        url: "../ajax/FacturaImport/eliminar.php",
                        data: {fact},
                        success: function (data) {
                            console.log(data)
                            data=JSON.parse(data)
                            if (data.res){
                                facturas.getdatos();
                            }
                        }
                    });

                },
                limpiarForm(){
                    this.itemSelect.nombre='';
                    this.dataR={
                        fecha:'',
                            tipo:'FACTURA',
                            num:'',
                            tc:'',
                            monto:''
                    }
                },
                guardarFactu(){
                    if ($("#file-factura").val().length>0){
                        var frmData = new FormData();
                        frmData.append('file', $("#file-factura")[0].files[0]);

                        $.ajax({
                            xhr: function() {
                                var xhr = new window.XMLHttpRequest();
                                xhr.upload.addEventListener("progress", function(evt) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = ((evt.loaded / evt.total) * 100);
                                        //app._data.progreso=percentComplete;
                                    }
                                }, false);
                                return xhr;
                            },
                            type: 'POST',
                            url: '../ajax/ManagerFiles/upd_files.php',
                            data: frmData,
                            contentType: false,
                            cache: false,
                            processData:false,
                            beforeSend: function(){
                                // app._data.progreso=0;
                                // app._data.starProgres=true;
                                $.toast({
                                    heading: 'INFORMACION',
                                    text: "guardando archivo",
                                    icon: 'info',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });
                            },
                            error:function(err){
                                console.log(err.responseText)
                                //alert('File upload failed, please try again.');
                                $.toast({
                                    heading: 'ERROR',
                                    text: "error al guardar el archivo",
                                    icon: 'error',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });

                            },
                            success: function(resp){
                                $.toast({
                                    heading: 'INFORMACION',
                                    text: "archivo guardado",
                                    icon: 'info',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });

                                console.log(resp)
                                var jsp = JSON.parse(resp);
                                //var nombrelog = jsp.datos;
                                // dat.imgt =nombrelog;
                                var dataG = {... facturas._data.dataR};
                                dataG.empresa = facturas._data.itemSelect.nombre;
                                dataG.ruc = facturas._data.itemSelect.ruc;
                                dataG.nomFile =  jsp.dstos;
                                dataG.import =  $("#folio_input_id").val();

                                console.log(dataG)

                                $.ajax({
                                    type: "POST",
                                    url: "../ajax/FacturaImport/agregar.php",
                                    data: dataG,
                                    success: function (resp) {
                                        resp = JSON.parse(resp);
                                        if (resp.res){
                                            facturas._data.listaFact.push(resp.data)
                                            facturas.limpiarForm();
                                            $('#modal_add_factura').modal('hide');
                                        }else{

                                        }
                                        console.log(resp)
                                        /*$("#modal_marca_editar").modal("toggle")
                                        APP.getDataMarcas();
                                        APP._data.listaHoja[APP._data.indexMarca].imgt = nombrelog;
                                        setTimeout(function () {
                                            APP.llenarHoja();
                                        },300)*/
                                    }
                                });

                            }
                        });
                    }else{
                        var dataG = {... this.dataR};
                        dataG.empresa = this.itemSelect.nombre;
                        dataG.ruc = this.itemSelect.ruc;
                        dataG.nomFile =  '';
                        dataG.import =  $("#folio_input_id").val();

                        console.log(dataG)

                        $.ajax({
                            type: "POST",
                            url: "../ajax/FacturaImport/agregar.php",
                            data: dataG,
                            success: function (resp) {
                                resp = JSON.parse(resp);
                                if (resp.res){
                                    facturas._data.listaFact.push(resp.data)
                                    facturas.limpiarForm();
                                    $('#modal_add_factura').modal('hide');
                                }else{

                                }
                                console.log(resp)
                                /*$("#modal_marca_editar").modal("toggle")
                                APP.getDataMarcas();
                                APP._data.listaHoja[APP._data.indexMarca].imgt = nombrelog;
                                setTimeout(function () {
                                    APP.llenarHoja();
                                },300)*/
                            }
                        });
                    }
                },
                guardarFactuInvoice(){
                    if ($("#file-factura").val().length>0){
                        var frmData = new FormData();
                        frmData.append('file', $("#file-factura2")[0].files[0]);

                        $.ajax({
                            xhr: function() {
                                var xhr = new window.XMLHttpRequest();
                                xhr.upload.addEventListener("progress", function(evt) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = ((evt.loaded / evt.total) * 100);
                                        //app._data.progreso=percentComplete;
                                    }
                                }, false);
                                return xhr;
                            },
                            type: 'POST',
                            url: '../ajax/ManagerFiles/upd_files.php',
                            data: frmData,
                            contentType: false,
                            cache: false,
                            processData:false,
                            beforeSend: function(){
                                // app._data.progreso=0;
                                // app._data.starProgres=true;
                                $.toast({
                                    heading: 'INFORMACION',
                                    text: "guardando archivo",
                                    icon: 'info',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });
                            },
                            error:function(err){
                                console.log(err.responseText)
                                //alert('File upload failed, please try again.');
                                $.toast({
                                    heading: 'ERROR',
                                    text: "error al guardar el archivo",
                                    icon: 'error',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });

                            },
                            success: function(resp){
                                $.toast({
                                    heading: 'INFORMACION',
                                    text: "archivo guardado",
                                    icon: 'info',
                                    position: 'top-right',
                                    hideAfter: '2500',
                                });

                                console.log(resp)
                                if (facturas._data.dataPri.idFact==''){
                                    var jsp = JSON.parse(resp);
                                    //var nombrelog = jsp.datos;
                                    // dat.imgt =nombrelog;
                                    var dataG = {... facturas._data.dataPri};
                                    dataG.empresa = dataG.proveedor
                                    dataG.ruc = ''
                                    dataG.nomFile =  jsp.dstos;
                                    dataG.import =  $("#folio_input_id").val();

                                    console.log(dataG)

                                    $.ajax({
                                        type: "POST",
                                        url: "../ajax/FacturaImport/agregar.php",
                                        data: dataG,
                                        success: function (resp) {
                                            resp = JSON.parse(resp);
                                            if (resp.res){
                                                facturas._data.dataPri.idFact = resp.data.impor_fac_id
                                                //facturas._data.listaFact.push(resp.data)
                                                //facturas.limpiarForm();
                                                $('#modal_editar_factura').modal('hide');
                                            }else{

                                            }
                                            console.log(resp)
                                            /*$("#modal_marca_editar").modal("toggle")
                                            APP.getDataMarcas();
                                            APP._data.listaHoja[APP._data.indexMarca].imgt = nombrelog;
                                            setTimeout(function () {
                                                APP.llenarHoja();
                                            },300)*/
                                        }
                                    });
                                }else{
                                    var dataG = {... facturas._data.dataPri};
                                    console.log(dataG)
                                    $.ajax({
                                        type: "POST",
                                        url: '../ajax/FacturaImport/actualizar.php',
                                        data: dataG,
                                        success: function (resp) {
                                            console.log(resp)
                                            $('#modal_editar_factura').modal('hide');
                                        }
                                    });

                                }


                            }
                        });
                    }else{
                        if (facturas._data.dataPri.idFact==''){
                            var dataG = {... facturas._data.dataPri};
                            dataG.empresa = dataG.proveedor
                            dataG.ruc = ''
                            dataG.nomFile =  '';
                            dataG.import =  $("#folio_input_id").val();

                            console.log(dataG)

                            $.ajax({
                                type: "POST",
                                url: "../ajax/FacturaImport/agregar.php",
                                data: dataG,
                                success: function (resp) {
                                    resp = JSON.parse(resp);
                                    if (resp.res){
                                        facturas._data.dataPri.idFact = resp.data.impor_fac_id
                                        /*  facturas._data.listaFact.push(resp.data)
                                          facturas.limpiarForm();*/
                                        $('#modal_editar_factura').modal('hide');
                                    }else{

                                    }
                                    console.log(resp)
                                    /*$("#modal_marca_editar").modal("toggle")
                                    APP.getDataMarcas();
                                    APP._data.listaHoja[APP._data.indexMarca].imgt = nombrelog;
                                    setTimeout(function () {
                                        APP.llenarHoja();
                                    },300)*/
                                }
                            });
                        }else{
                            var dataG = {... facturas._data.dataPri};
                            console.log(dataG)
                            $.ajax({
                                type: "POST",
                                url: '../ajax/FacturaImport/actualizar.php',
                                data: dataG,
                                success: function (resp) {
                                    console.log(resp)
                                    $('#modal_editar_factura').modal('hide');
                                }
                            });
                        }

                    }
                },
                getProveedor(){

                    $.ajax({
                        type: "POST",
                        url: "../ajax/FacturaImport/getProveedores.php",
                        data: {inport:$("#folio_input_id").val()},
                        success: function (data) {
                            console.log(data);
                            data = JSON.parse(data);
                            facturas._data.listaProvedoresSel = data;

                        }
                    });

                },
                selectetProvedor(jsn){
                    this.itemSelect=jsn;
                    $('#modal_buscar_proveedor_factura').modal('hide');
                    console.log(jsn);
                },
                getdatos(){

                    $.ajax({
                        type: "POST",
                        url: "../ajax/FacturaImport/getLista.php",
                        data: {inport:$("#folio_input_id").val()},
                        success: function (data) {
                            console.log(data);
                            data = JSON.parse(data);
                            facturas._data.listaFact = data;

                        }
                    });

                }
            },
            computed: {
                getLista(){
                    var lista =[];
                    for (var i =0; i<this.listaProvedoresSel.length;i++){
                        if(this.listaFact.length>0){
                            if(this.listaFact.findIndex(item => item.proveedor === this.listaProvedoresSel[i].nombre) == -1){
                                lista.push(this.listaProvedoresSel[i]);
                            }
                        }else{
                            lista.push(this.listaProvedoresSel[i]);
                        }


                    }
                    return lista;
                }
            }
        })
        $( document ).ready(function() {

            setTimeout(function () {
                facturas.getdatos();
                facturas.getProveedor();
            },100);
            $("#input-provedor-fact").focus(function () {
                $("#modal_buscar_proveedor_factura").modal('show');
            });

            $("#producto-input-productos").autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "../ajax/Producto/search_producto_auto.php",
                        data: { term: request.term},
                        success: function(data){
                            response(data);
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log(errorThrown)
                        },
                        dataType: 'json'
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    console.log(ui.item)
/*
                    APP._data.producto.predata = ui.item;
                    APP._data.producto.nombre = ui.item.produ_nombre;
                    APP._data.producto.precio = ui.item.precio;
                    APP._data.producto.cantidad = '';
                    APP._data.producto.stock = ui.item.cantidad;
                    APP._data.producto.total = 0;*/
                    var catgoria = ui.item.cat_nombre;

                    $("#box-presentacion").hide();
                    $("#box-cant-presentacion").hide();
                    $("#box-tipo-env").hide();

                    if (catgoria == 'ACCESORIOS'){
                        $("#box-presentacion").show();
                        $("#box-cant-presentacion").show();
                        $("#box-tipo-env").show();
                    }
                    var id = ui.item.produ_id;
                    var name = ui.item.produ_nombre;
                    var m_id = ui.item.unidad_id;
                    $('#producto-input-productos').val(name);
                    $('#producto-input-productos-id').val(id);
                    $('#select_medida').val(m_id).change();


                    event.preventDefault();

                }
            });
        });

        function addProduc() {

            var catgoria = $(this).parents('tr').eq(0).find('#btn-cat').text();
            if (catgoria == 'NEUMATICOS'){
                $("#box-presentacion").hide();
                $("#box-cant-presentacion").hide();
            }else{
                $("#box-presentacion").show();
                $("#box-cant-presentacion").show();
            }
            var id = $(this).parents('tr').eq(0).find('input').val();
            var name = $(this).parents('tr').eq(0).find('label').text();
            var m_id = $(this).parents('tr').eq(0).find('#med-data').text();
            $('#producto-input-productos').val(name);
            $('#producto-input-productos-id').val(id);
            $('#select_medida').val(m_id).change();

        }

        function  open_newfill(archivo) {
            window.open(archivo)
        }

        function asignador() {

            var incote = $("#select_incoterm option:selected").text();

            var tipocambio= 3.56;
            var fob= $("#orden-input-fob-total").val();
            var flete= $("#orden-input-flete-contenedor").val();
            var primaneta= $("#seguro-input-prima-neta").val();
            var thc= (incote == "FOB")?"0.00":parseFloat($("#orden-input-thc-contenedor").val()) * parseInt($("#folio-folder-qty").val());
            //var thc= $("#orden-input-thc-contenedor").val();
            var cifmontototal= $("#seguro-input-monto-total").val().replace(",", "");
             cifmontototal= cifmontototal.replace(",", "");

            var adv=0;
            var igv= (parseFloat(cifmontototal)*0.16).toFixed(2);
            var ipm= (parseFloat(cifmontototal)*0.02).toFixed(2);
            var derecho= (parseFloat(igv)+parseFloat(ipm));
            var percep= ((0.035*(parseFloat(cifmontototal)+derecho))*tipocambio).toFixed(2);

            var total= parseFloat(derecho*tipocambio)+parseFloat(percep);

            derecho = derecho.toFixed(2)

            console.log(derecho)
            $("#adua-input-cost_origen").val($("#orden-input-cost-adicional").val())
            $("#adua-input-fob-total").val((fob));
            $("#adua-input-total-flete").val((flete));
            $("#adua-input-prima-neta").val((primaneta));
            $("#adua-input-thc-contenedor").val((thc));
            $("#adua-input-monto-total").val($("#seguro-input-monto-total").val());
            $("#adua-input-adv").val(formatNumerDecimal(adv.toFixed(2)));
            $("#adua-input-igv").val(validarnum(igv));
            $("#adua-input-ipm").val(validarnum(ipm));
            $("#adua-input-derecho").val(validarnum(derecho));
            $("#adua-input-percepcion").val(validarnum(percep));
            $("#adua-input-tipo-camb").val(validarnum(tipocambio));
            $("#adua-input-total").val(validarnum(total.toFixed(2)));

        }
        function validarnum(num){
            console.log(num+"")
            if ((num+"")=="NaN"){
                return "0.00"
            }else{
                return formatNumerDecimal(num+"")
            }
        }
        function  formatNumerDecimal(nStr) {
            console.log(nStr);
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

        function sumarDias(fecha, dias){
            fecha.setDate(fecha.getDate() + (dias+1));
            return fecha;
        }


        const EMBARQUE = new Vue({
            el:"#embar-id-cont",
            data:{
                indexareb:-1,
                naves:[],
                datoG:{

                },
                listaEmbarque:[]
            },
            methods:{
                isNumber: function($event) {
                    let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
                    if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                        $event.preventDefault();
                    }
                },
                getDateETA(date,eta){
                    var fechast="";

                    if(date!="0000-00-00"&&date!=""&&eta>0){
                        var fecha = new Date( Date.parse(date));
                        sumarDias(fecha,parseInt(eta+""));
                        fechast =   fecha.getDate() + "/" + (fecha.getMonth() + 1) + "/"+ fecha.getFullYear() ;
                    }
                    return fechast;
                },
                getDateLibreEstadia(date,eta,libre){
                    var fechast="";

                    if(date!="0000-00-00"&&date!="" && eta>0 && libre>0){
                        var fecha = new Date( Date.parse(date));
                        sumarDias(fecha,parseInt(eta+"")+parseInt(libre+""));
                        fechast =   fecha.getDate() + "/" + (fecha.getMonth() + 1) + "/"+ fecha.getFullYear() ;
                    }
                    return fechast;
                },
                getDateDiasAlma(date,eta,libre,diasalm){
                    var fechast="";

                    if(date!="0000-00-00"&&date!=""&&eta>0&& libre>0&&diasalm>0){
                        var fecha = new Date( Date.parse(date));
                        sumarDias(fecha,parseInt(eta+"")+parseInt(libre+"")+parseInt(diasalm+""));
                        fechast =   fecha.getDate() + "/" + (fecha.getMonth() + 1) + "/"+ fecha.getFullYear() ;
                    }
                    return fechast;
                },
                setPosition(index){
                   // console.log("-++++++++++++++++++++++++++++++++++++"+index)
                    EMBARQUE._data.indexareb=index;
                },
                rellenarNavesAll(){
                    for(var i=0; i<EMBARQUE._data.listaEmbarque.length;i++){

                        EMBARQUE.generarNaves(EMBARQUE._data.listaEmbarque[i].idLinea,i);
                    }
                    setTimeout(function () {
                        $('.selectpicker').selectpicker('refresh');
                    },100);
                },
                getDataOrdenEmbarque(){
                    //listaEmbarqueDetalle.php
                    $.ajax({
                        type: "POST",
                        url: "../ajax/EmbarqueDetalle/listaEmbarqueDetalle.php",
                        data: {
                            idembarque:$("#id-embarque-folder-sp").val()
                        },
                        success: function (data) {
                            //console.log(data);
                            var json = JSON.parse(data);
                            if (json.length ==0){
                                EMBARQUE.addListaBlanck();
                            }else{
                                EMBARQUE._data.listaEmbarque= json;
                                for(var i=0; i<json.length;i++){
                                    //console.log("////////////////////////////////////")
                                    EMBARQUE.generarNaves(json[i].idLinea,i);
                                }
                            }


                            setTimeout(function () {
                                $('.selectpicker').selectpicker('refresh');
                                $( ".searsh-almacen-orden" ).focus(function(event) {
                                    $("#modal_buscar_almacen").modal('show');
                                    EMBARQUE._data.indexareb=$(event.target).attr('dx');

                                });
                                $( ".searsh-linea-orden" ).focus(function(event) {
                                    $("#modal_buscar_linea").modal('show');
                                    EMBARQUE._data.indexareb=$(event.target).attr('dx');
                                });
                            },150);
                        }
                    });
                },
                guardarOrdenEmbarque(){
                    var jsData = {
                        idorden:$("#id-orden-folder-sp").val(),
                        idembarque:$("#id-embarque-folder-sp").val(),
                        proforma:$("#orden-input-serie-factura").val(),
                        fecha:$("#orden-input-fecha-factura").val(),
                        idfolwa:$("#orden-input-Forwarder-id").val(),
                        fobtotal:$("#orden-input-fob-total").val(),
                        fletecont:$("#orden-input-flete-contenedor").val(),
                        thc:$("#orden-input-thc-contenedor").val(),
                        cost_ad:$("#orden-input-cost-adicional").val(),
                        totalflete:$("#orden-input-total-flete").val(),
                        embarques:  JSON.stringify(this.listaEmbarque)
                    };
                    console.log(jsData)
                   $.ajax({
                        type: "POST",
                        url: "../ajax/EmbarqueDetalle/guardarOrden.php",
                        data: jsData,
                        success: function (data) {
                            console.log(data);
                                var json = JSON.parse(data);
                                console.log(json)
                            $("#id-embarque-folder-sp").val(json.ideb);
                                for (var ii =0;  ii< json.eb.length; ii++){
                                    json.eb[ii].naves =  EMBARQUE._data.listaEmbarque[ii].naves;
                                }
                            EMBARQUE._data.listaEmbarque= json.eb;
                            $.toast({
                                heading: 'EXITOSO',
                                text: 'Guargado',
                                icon: 'success',
                                position: 'top-right',
                                hideAfter: '2500',
                            });

                            setTimeout(function () {
                                $('.selectpicker').selectpicker('refresh');
                                $( ".searsh-almacen-orden" ).focus(function(event) {
                                    $("#modal_buscar_almacen").modal('show');
                                    EMBARQUE._data.indexareb=$(event.target).attr('dx');
                                });
                                $( ".searsh-linea-orden" ).focus(function(event) {
                                    $("#modal_buscar_linea").modal('show');
                                    EMBARQUE._data.indexareb=$(event.target).attr('dx');
                                });
                            },100);
                        }
                    });

                },
                generarNaves(idLinea,index){
                    if (idLinea!=""&&idLinea!=null){
                        console.log("sssssssssssss")
                        $.ajax({
                            type: "POST",
                            url: "../ajax/Nave/databylinea.php",
                            data: {idlinea : idLinea},
                            success: function (data) {

                                EMBARQUE._data.listaEmbarque[index].naves = JSON.parse(data);
                            }
                        });

                    }
                },
                addListaBlanck(){
                    this.naves.push([]);
                    this.listaEmbarque.push({
                        iddetaemb:'',
                        idemp:'',
                        idLinea:'',
                        nombrelinea:"",
                        idalmacen:'',
                        nomalmacen:"",
                        bl:'',
                        idnave:'',
                        naves:[],
                        etd:"",
                        eta:0,
                        libres:0,
                        iddepacho:'',
                        diasalma:0
                    });
                    setTimeout(function () {
                        $('.selectpicker').selectpicker('refresh');
                        $( ".searsh-almacen-orden" ).focus(function(event) {
                            $("#modal_buscar_almacen").modal('show');
                            EMBARQUE._data.indexareb=$(event.target).attr('dx');
                        });
                        $( ".searsh-linea-orden" ).focus(function(event) {
                            $("#modal_buscar_linea").modal('show');
                            EMBARQUE._data.indexareb=$(event.target).attr('dx');
                        });
                    },50)
                },

            },
            computed:{

            }
        });

    </script>
    <script type="text/javascript">
        function openCaratula(idemp) {
            const thcFlete = $("#orden-input-thc-contenedor").val();
            const poldes = $("#seguro-input-vigencia-desde").val();
            const polast = $("#seguro-input-vigencia-hasta").val();
            const totalFlete = $("#orden-input-total-flete").val();
            const flete = $("#orden-input-flete-contenedor").val();
            const totalfo = $("#seguro-input-total-fob").val();
            const proforma = $("#orden-input-serie-factura").val();
            const cntConte = $("#orden-input-qty-cont-").val();
            const numdua = $("#aduana-input-dua").val();
            const fechafac = $("#orden-input-fecha-factura").val();
            const intercor = $("#select_incoterm  option:selected").text();
            const canalad = $("#select_canal  option:selected").text();
            const libres = $("#libres"+idemp).text();
            const alma = $("#alma"+idemp).text();
            const almac = $("#almacen"+idemp).val();
            const nupo= $("#seguro-input-poliza").val();
            const neta= $("#seguro-input-prima-neta").val();
            const tota= $("#seguro-input-prima-total").val();
            const nas= $("#seguro-input-monto-total").val();

            window.open("./pdf/pdf_importaciones_caratula.php?folder=<?php echo $_GET['id'] ?> &emb="+
                idemp+"&thc="+thcFlete+"&totalf="+
                totalFlete+"&flete="+flete+"&totalfo="+totalfo+"&profor="+
                proforma+"&cntcont="+cntConte+"&numdu="+numdua+"&fechaf="+
                fechafac+"&interco="+intercor+"&libre="+libres+"&alma="+alma+
                "&almacenn="+almac+"&canal="+canalad+"&desde="+poldes+"&ast="+polast+
                "&polisa="+nupo+"&neta="+neta+"&total="+tota+"&nas="+nas);
        }

        function  setNombreDetalle(nombre,fun){
            $('#nom-tad-folder').text(nombre);
            console.log(fun)
            if (typeof(fun) != "undefined"){
                fun();
            }

        }

        function prueba() {
            swal("Here's a message!")
        }
        function openSeguguro() {
            var emp=$("#folio-input-empresa-id").val();
            var prov=$("#frame-new-folder-input_proveedor_id").val();
            var ref=$("#seguro-input-referencia").val();
            var mar=$("#select_marca option:selected").text();
            var ori=$("#select_pais option:selected").text() ;
            var num=$("#orden-input-serie-factura").val();
            var fecha=$("#fechafolderimport").val();
            var cate=$("#select_categoria option:selected").text();
            var contc=$("#folio-folder-qty").val();
            var tipoc=$("#select_tipo_contenedor option:selected").text();
            var merc=$("#seguro-input-total-fob").val();
            var flet=$("#seguro-input-total-flete").val();
            var tas=$("#select_tasa option:selected").text();
            var ase = $("#seguro-input-suma-asegurada").val();
            var cos_ori = $("#adua-input-cost_origen").val();
            var thcs = $("#adua-input-thc-contenedor").val();
            window.open("./pdf/poliza.php?aseg="+ase+"&emp="+emp+"&prov="+prov+"&ref="+ref+"&mar="+mar+"&ori="+ori+"&num="+num+"&fecha="+fecha+"&cate="+cate+"&contc="+contc+"&tipoc="+tipoc+"&merc="+merc+"&flet="+flet+"&tas="+tas);
        }
        function openDerechoEstimado() {
            var emp=$("#folio-input-empresa-id").val();
            var prov=$("#frame-new-folder-input_proveedor_id").val();
            var ref=$("#seguro-input-referencia").val();
            var mar=$("#select_marca option:selected").text();
            var ori=$("#select_pais option:selected").text() ;
            var num=$("#orden-input-serie-factura").val();
            var fecha=$("#fechafolderimport").val();
            var cate=$("#select_categoria option:selected").text();
            var contc=$("#folio-folder-qty").val();
            var tipoc=$("#select_tipocon option:selected").text();
            var merc=$("#seguro-input-total-fob").val();
            var flet=$("#adua-input-total-flete").val();
            var tas=$("#select_tasa option:selected").text();
            var ase = $("#seguro-input-suma-asegurada").val();
            var prineta = $("#seguro-input-prima-neta").val();
            var cif = $("#seguro-input-monto-total").val();
            var cambio =$("#adua-input-tipo-camb").val();
            var percep =$("#adua-input-percepcion").val();
            var igv = $("#adua-input-igv").val();
            var ipm = $("#adua-input-ipm").val();
            var derechoa = $("#adua-input-derecho").val();
            var totalp = $("#adua-input-total").val();
            var cos_ori = $("#adua-input-cost_origen").val();
            var thcs = $("#adua-input-thc-contenedor").val();
            window.open("./pdf/derechoestimado.php?cambiot="+cambio+"&cosor="+cos_ori+"&thcs="+thcs+"&perset="+percep+"&totalp="+totalp+"&ipm="+ipm+"&dereco="+derechoa+"&igvp="+igv+"&cif="+cif+"&prim="+prineta+"&aseg="+ase+"&emp="+emp+"&prov="+prov+"&ref="+ref+"&mar="+mar+"&ori="+ori+"&num="+num+"&fecha="+fecha+"&cate="+cate+"&contc="+contc+"&tipoc="+tipoc+"&merc="+merc+"&flet="+flet+"&tas="+tas);
        }
        function openPDF(cateroria) {
            if (cateroria == 'CAMARAS' ){

                window.open("./pdf/pdf_importacion_cam-prot.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            } else if ( cateroria == 'PROTECTORES'){
                window.open("./pdf/pdf_importacion_prot.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            }else if(cateroria == 'NEUMATICOS'){
                window.open("./pdf/pdf_importacion_neu.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            }else if(cateroria == 'AROS'){
                window.open("./pdf/pdf_importacion_aros.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            }else if(cateroria == 'ACCESORIOS'){
                window.open("./pdf/pdf_importacion_cam-prot.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            }else{
                alert("esta es una nueva categoria debe asignarle una plantilla para imprimir");
                window.open("./pdf/pdf_importacion_cam-prot.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            }

        }

        function openPDFTraSunat(cateroria) {
            if (cateroria == 'CAMARAS' ){
                window.open("./pdf_tra/pdf_trducion_camaras.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            } else if ( cateroria == 'PROTECTORES'){
                window.open("./pdf_tra/pdf_trducion_protectores.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            }else if(cateroria == 'NEUMATICOS'){
                window.open("./pdf_tra/pdf_trducion_neumatico.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            }else if(cateroria == 'AROS'){
                window.open("./pdf_tra/pdf_trducion_aros.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            }else if(cateroria == 'ACCESORIOS'){
                window.open("./pdf_tra/pdf_trducion_accesorios.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            }else{
                alert("esta es una nueva categoria debe asignarle una plantilla para imprimir");
                window.open("./pdf_tra/pdf_trducion_accesorios.php?od="+idOrdenPro+"&cd="+idImpor+"&tit="+cateroria+"&fol="+folder+"&proc="+procedencia+"&cnt="+cantidad+"&marca="+marca);
            }

        }

        $(document).ready(function () {

            $( "#seguro-plazo-dias-vigensia" ).keyup(function() {
                console.log( Date.parse($("#seguro-input-vigencia-desde").val()))
                var dias = $("#seguro-plazo-dias-vigensia").val();
                console.log(dias)
                var fecha = new Date( Date.parse($("#seguro-input-vigencia-desde").val()));
                //fecha = fecha.setDate(fecha.getDate() +  parseInt(dias))
                sumarDias(fecha, parseInt(dias))

                var fecha = new Date(fecha);
                console.log(fecha)
                var mes= (fecha.getMonth() + 1);
                var dia= fecha.getDate();
                var neFecha =  fecha.getFullYear()+ "-"+ (mes>9?mes:'0'+mes) + "-"+(dia>9?dia:'0'+dia) ;
                console.log(neFecha);
                $("#seguro-input-vigencia-hasta").val(neFecha);
            });


            $('#modal_nave').on('hidden.bs.modal', function () {
                EMBARQUE.rellenarNavesAll();
            });
            $('#table-agencia-aduana').DataTable({
                /*scrollY: false,*/
                /*scrollX: true,*/
                paging: true,
                lengthMenu: [[4, 8, 14, -1], [4, 8, 14, "All"]],
                language: {
                    url: '../assets/Spanish.json'
                }
            });
            <?php  if($desactivar){ ?>


            setTimeout(function () {
                $(".esadoSelect").prop('disabled', true);
            }, 1000);
            <?php   } ?>

        });
        function isEmptyValue(val){
            return (val === undefined || val == null || val.length <= 0) ? true : false;
        }
        function llenartablapaking() {

            paking_tab.forEach(function (element, index) {
                llenartablapakingunique(index);
            });

        }
        function llenartablapakingunique(index) {
            var rowTem="";
            $('#table-pack-tr'+(index)).empty();
            for (var i = 0; i < paking_tab[index].length ; i++) {

                rowTem += (genElementRowPacking( paking_tab[index][i], i));
            }
            $('#table-pack-tr'+(index)).append(rowTem);
        }

        function genElementRowPacking(el, index) {
            <?php  if($desactivar){ ?>
            var  rowS= '              <a  disabled  id="btn-" class="btn btn-sm btn-danger fa fa-times" title="Eliminar item" data-dismiss="modal"></a>';

        <?php   }else{ ?>
            var  rowS= '              <a   onclick="evt_rem_pakin('+(index)+' ,'+el.orddeta_id.orddeta_id+', '+el.pack_id+', '+valida2(el.pack_cant)+')" id="btn-" class="btn btn-sm btn-danger fa fa-times" title="Eliminar item" data-dismiss="modal"></a>';

            <?php   } ?>

            var modeloNobre  = el.orddeta_id.produ_id.mod_id? el.orddeta_id.produ_id.mod_id.mod_nombre: '';
            var row_tr='<tr>' +
                '           <td class="contador" style="color: white">'+(index+1)+'</td>' +
                '           <td>'+el.orddeta_id.produ_id.produ_nombre+'</td>' +
                '           <td style="text-align: center">'+modeloNobre+'</td>' +
                '           <td style="text-align: center">'+el.orddeta_id.produ_id.unidad_id.unidad_nombre+'</td>' +
                '           <td style="text-align: center">'+valida2(el.pack_cant)+'</td>' +
                '           <td class="text-center"> ' +
                rowS+
                '                <input class="pack_id no-display" type="text" value="'+el.pack_id+'">' +
                '                <input class="ordeta_id no-display" type="text" value="'+el.orddeta_id.orddeta_id+'">' +
                '           </td>' +
                '       </tr>';
            return row_tr;
        }

        $(document).ready(function () {
            EMBARQUE.getDataOrdenEmbarque()
            facturas.getDataInvoice()
        });


        function  evt_rem_pakin(index, id, idl, cnt) {
            console.log("evt eliminar:");
            const numtab =tabnow.substring(tabnow.length-1);
            resetTable2(cnt,id);
              runAjaxId_none2(idl,
                  'Packing',
                  'deleteForID',
                  'Packing',
                  function (e) {

                  });
            paking_tab[numtab].splice( index, 1 );
            llenartablapakingunique(numtab);
            contabilizarTabla2();
        }



       function valida2(data) {
            if(data){
                return data;
            }else{
                return '';
            }
        }

        function reasignacionProcicion2(table) {
            $("#"+table+" tbody tr").each(function(i){
                $("#"+table+" tbody tr").eq(i).find("#btn-").eq(0).attr("data-index", i+1);
                $("#table-0 tbody tr").eq(i).find("td.contador").text(i+1)
            })
        }

        function contabilizarTabla2(){
            var count=0;
            $(datos_table_n2).each(function (i,v) {
                console.log(i);
                count=0;
                $('#table-'+i+' tbody tr').each(function (j,h) {
                    var tmp_count=$(this).find('td').eq(4).text();
                    count=parseInt(count)+parseInt(tmp_count);
                });
                $('#table-pack-footer'+i+' tr').find('th').eq(2).text(count);
            });
            count=0;
            $('#table-pack tbody tr').each(function (i,v) {
                var tmp_count = $(this).find('td').eq(5).text();
                count = parseInt(tmp_count)+parseInt(count);
                $('#table-pack-footer tr').find('th').eq(3).text(count);
            });


        }

        function resetTable2(cant,id_t){
            var elementCell = $("#row-unic-pck-value-"+id_t);

            var cantidad = elementCell.text();
            cantidad = parseInt(cantidad)+parseInt(cant);
            elementCell.text(cantidad);
            console.log("#row-unic-pck-"+id_t)

            /*contabilizarTabla();*/
        }

        function runAjaxArray2(array, file, method, title, func) {
            $.ajax({
                data: {'array': JSON.stringify(array)},
                url: '../ajax/' + file + '/' + method + '.php',
                type: 'POST',
                async: true,
                success: function (response) {
                    var json = JSON.parse(JSON.stringify(response));
                    console.log(json);
                    if (json) {
                        func(json);
                    } else {
                    }
                },
                error: function () {
                    alertGlo.alerInfo('No se pudo cargar ' + title + '<br>Reporte este error!!');
                }
            });
        }

        function runAjaxId_none2(id, file, method, title, func) {
            console.log('../ajax/' + file + '/' + method + '.php');
            $.ajax({
                data: {id},
                url: '../ajax/' + file + '/' + method + '.php',
                type: 'POST',
                async: true,
                success: function (response) {
                    var json = JSON.parse(JSON.stringify(response));
                    if (json) {
                        func(json);
                    } else {
                    }
                },
                error: function () {
                }
            });
        }

        function countTable2(){
            $('#table-pack tbody tr').each(function (j,b) {
                var id=$(this).find('.pack-input-orden-id').val();
                var count=0;
                $(datos_table_n2).each(function (i,v) {
                    $('#table-'+(i)+' tbody tr').each(function () {
                        var cant=$(this).find('td').eq(4).text();
                        var id_tag=$(this).find('td').find('input').eq(1).val();
                        if(id===id_tag){
                            console.log('ES IGUAL');
                            count=parseInt(count)+parseInt(cant);
                        }
                    });

                });
                var cantidad = $(this).find('td').eq(5).text();
                cantidad = parseInt(cantidad)-parseInt(count);
                console.log(cantidad);
                $(this).find('td').eq(5).text(cantidad);
            });
            contabilizarTabla2();
        }
        function descargarReporCost(id) {
            console.log(id)
            $.toast({
                heading: 'INFORMACION',
                text: "Generado EXEL",
                icon: 'info',
                position: 'top-right',
                hideAfter: '2500',
            });
            $.ajax({
                type: "POST",
                url: "report/report_cost.php",
                data: {idimport:id},
                success: function (data) {
                    console.log(data)
                    setTimeout(function () {
                        $.toast({
                            heading: 'EXITOSO',
                            text: "Descargando",
                            icon: 'success',
                            position: 'top-right',
                            hideAfter: '2500',
                        });
                        window.location.href = 'report/report cos.xlsx';
                    },100)

                }
            });

        }
    </script>

</body>

</html>


