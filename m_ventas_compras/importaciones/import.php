<?php
/*phpinfo();
*/
require '../utils/Tools.php';
$tools = new Tools();
$indexRuta=1;



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>ERP - GrupoLodi</title>

    <link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">


    <link href="../assets/datatables.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.js" type="text/javascript"></script>


    <script src="../assets/datatables.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.js"></script>
    <link href="../aConfig/plugins/sweetalert2/sweetalert2.min.css">




    <!--<script src="../assets/Bootstrap-3.3.7/js/bootstrap.js" type="text/javascript"></script>-->

    <!--<link href="../aConfig/Mycss/lodi-css.css" rel="stylesheet">
    <link href="../assets/fontawesome-pro-5.12/css/all.min.css" rel="stylesheet">
    <link href="../assets/Toast/build/jquery.toast.min.css" rel='stylesheet'/>

    <link href="../assets/Bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">


    <link href="../assets/datatables.min.css" rel="stylesheet">
    <link href="../assets/Bootstrap-select-1.13.9/dist/css/bootstrap-select.min.css" rel="stylesheet">


    <script src="../assets/jQuery-3.3.1/jquery-3.3.1.min.js" type="text/javascript"></script>


    <script src="../assets/datatables.min.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/bootstrap-select.min.js"></script>
    <script src="../assets/Bootstrap-select-1.13.9/dist/js/i18n/defaults-es_ES.min.js"></script>-->


</head>

<body>
<div id="wrapper">
    <?php
    /*include '../entidadDB/DataBase.php';*/


    require_once '../model/model.php';
    include '../componets/navbar.php'
    ?>
    <script>
    </script>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side hidden-lg hidden-md" role="navigation">
        <div class="hidden-md hidden-lg">
            <div id="sideNav" href="">
                <!--<i class="fa fa-caret-right"></i>-->
            </div>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="lii">
                    <!--class="active-menu"-->
                    <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <!-- <li class="lii">

                    <a href="index.php"><i class="fa fa-calendar"></i> Inventario <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?menu=33">Productos<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#">Busqueda de Asiento <span class="fa arrow"></span></a>
                        </li>

                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div class="header">
            <h6 class="page-header">

            </h6>
        </div>
        <div id="page-inner">

            <?php
            include '../componets/nav.php';
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="fg-azul no-padding no-margin">
                                            <i class="fa fa-folder-open fa-fw"></i>
                                            <strong id="tittle-header-body">Importaciones</strong>
                                        </h2>
                                    </div>
                                    <!--BOTONES-->
                                    <div class="col-lg-6 text-right">
                                        <a href="new-folder.php?#" id="folder_btn_nuevo_folder" class="btn btn-primary">
                                            <i class="fa fa-plus "></i> Nuevo Folder
                                        </a>
                                        <!--<button id="folder-btn-salir" class="btn btn-warning"><i
                                                class="fa fa-chevron-left" style="padding-right: 8px;"></i>   Salir
                                        </button>-->
                                    </div>

                                    <!--BOTONES-->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <hr class="fg-black line-body"/>
                            </div>

                            <div id="body-importaciones"  class="col-xs-12 col-sm-12 col-md-12">
                                <?php
                                include 'frame/frame-table-folder.php';
                                ?>
                            </div>
                           <!-- <div id="body-new-folder" hidden class="col-xs-12 col-sm-12 col-md-12">
                                <?php
/*                                include 'frame/frame-new-folder.php';
                                */?>
                            </div>

                            <div id="body-detalles-folder"  hidden class="col-xs-12 col-sm-12 col-md-12">
                                <?php
/*                                include 'frame/frame-detalles-folder.php';
                                */?>
                            </div>-->
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>
    <?php
    include '../modal/modal.php';
    ?>


    <style>
        .sorting:after {
            display: none !important;
        }

        #table-folder-import_info {
            display: none !important;
            color: rgba(255, 255, 0, 0) !important;
        }
    </style>

    <script type="module" src="../aConfig/Input_validate.js"></script>
    <script type="module" src="../aConfig/Import-main.js"></script>
    <script type="module" src="../aConfig/alertToas.js"></script>
    <script type="text/javascript" src="../assets/Toast/build/jquery.toast.min.js"></script>
    <script src="../aConfig/Myjs/contador_espinner.js"></script>
    <script type="text/javascript" src="https://unpkg.com/default-passive-events"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!--<script type="text/javascript">

        $(document).ready(function () {
            $('#table-folder-import').DataTable({
                scrollY: false,
                scrollX: true,
                scrollCollapse: false,
                paging: true,
                autoWidth: false,
                fixedColumns: {
                    leftColumns: 2,
                },
                language: {
                    url: '../assets/Spanish.json'
                }
            });
        });

    </script>-->


</body>
<script>


    function descargarReporCost(id) {
        console.log(id)
        $.ajax({
            type: "POST",
            url: "report/report_cost.php",
            data: {idimport:id},
            success: function (data) {
                console.log(data)
                setTimeout(function () {
                    window.location.href = 'report/report cos.xlsx';
                },100)

            }
        });

    }

    function inavilitarSelect(vl) {
        console.log()
        if (vl==0){
            $("#select_estado").prop( "disabled", true );
        }else{
            $("#select_estado").prop( "disabled", false );
        }

    }
    function procesar(folderNm,empresa, folder, orden) {
        swal({
            title: "¿Desea procesar este folder?",
            text: "Despues ya no podra editar el folder",
            icon: "warning",
            dangerMode: false,
            buttons: ["NO", "SI"],
        })
            .then((ressss) => {
                console.log(ressss);
                if (ressss){
                    $.ajax({
                        type: "POST",
                        url: "../ajax/Procesar/procesar_productos.php",
                        data: {ie: empresa,ifl:folder, ior:orden,fol:folderNm},
                        success: function (resp) {
                            if (isJson(resp)){
                                var jsn= JSON.parse(resp);
                                if (jsn.res){
                                    swal("Folder procesado", {
                                        icon: "success",
                                    });
                                    console.log("#botonProcesar"+folder);
                                    $("#botonProcesar"+folder).attr('onclick', "swal('Este folder ya esta prosesado!')");
                                    $("#botonProcesar"+folder).attr('class', "btn btn-danger btn-sm fa fa-edit");
                                }else{
                                    swal("No se pudo procesar este Folder",{
                                        dangerMode: true
                                    });
                                }
                            }else {
                                console.log(resp);
                            }
                        }
                    });
                }

            });
    }

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

</script>

</html>
