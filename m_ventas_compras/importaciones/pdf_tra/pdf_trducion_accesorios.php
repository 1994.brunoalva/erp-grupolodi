<?php
date_default_timezone_set('America/Lima');

require_once('../../Lib/mpdf/vendor/autoload.php');
require "../../utils/Tools.php";
require  "../../conexion/Conexion.php";

$conexion = (new Conexion())->getConexion();

$sql = "SELECT
  sys_impor_folder.*,
  sys_com_proveedor.provee_desc
FROM sys_impor_folder
INNER JOIN sys_com_proveedor ON sys_impor_folder.provee_id = sys_com_proveedor.provee_id
WHERE sys_impor_folder.impor_id = ". $_GET['cd'];

$resImport = $conexion->query($sql)->fetch_array();

$tools = new Tools();
$mpdf = new \Mpdf\Mpdf();

$fechaescrita = date("d"). " de " . $tools->nombreMes(date("m")-1). " del ". date("Y");


$rowProd ="";

$sql = "SELECT
  dord.prod_cantidad,
  prod.*,
  produ_desc.*,
  pais.pais_nombre,
  marca.mar_nombre,
  unidad.unidad_nombre
FROM sys_com_orden_detalle AS dord
INNER JOIN sys_producto AS prod ON dord.produ_id = prod.produ_id
INNER JOIN sys_pais AS pais ON prod.pais_id = pais.pais_id
INNER JOIN sys_unidad AS unidad  ON prod.unidad_id = unidad.unidad_id
INNER JOIN sys_com_marca AS marca ON prod.mar_id = marca.mar_id
INNER JOIN sys_produ_detalle AS produ_desc ON prod.produ_deta_id = produ_desc.produ_deta_id
WHERE prod.cat_id = 5 AND dord.ord_id = ".$_GET['od'];
$resProd = $conexion->query($sql);

$contador=1;

foreach ($resProd as $pro){
    $rowProd =$rowProd."<tr style='border: 1px solid black;'>
    <td style='border: 1px solid black;font-size: 13px; text-align: center'>$contador</td>
    <td style='border: 1px solid black;font-size: 13px;text-align: center'>{$pro['prod_cantidad']}</td>
    <td style='border: 1px solid black;font-size: 13px;text-align: center'>{$pro['unidad_nombre']}</td>
    <td style='border: 1px solid black;font-size: 13px;text-align: center'>{$pro['produ_desc']}</td>
    <td style='border: 1px solid black;font-size: 13px;text-align: center'>{$pro['produ_pn']}</td>
    <td style='border: 1px solid black;font-size: 13px;text-align: center'>{$pro['mar_nombre']}</td>
    <td style='border: 1px solid black;font-size: 13px;text-align: center'>{$pro['pais_nombre']}</td>
  </tr>";
    $contador++;
}


$html = "
<div style='width: 100%'>
<div style='width: 100%; text-align: center'>
<h2>Traducción de Factura-Aduanas</h2>
</div>
<div style='width: 100%'>
<span>Lima, $fechaescrita</span>
</div>
<div style='width: 100%'>
<span>Señores</span>
</div>

<div style='width: 100%'>
<span><strong>SUPERINTENDENCIA NACIONAL  DE ADUANAS Y DE ADMINISTRACION TRIBUTARIA.</strong></span>
</div>
<div style='width: 100%'>
<span>Presente.-</span>
</div>
<div style='width: 100%'>
<table style='width:100%; padding-left: 50px' >

  <tr>
    <td style='width: 180px'><strong>Factura Comercial:</strong></td>
    <td>Nº {$resImport['impor_nro_profo']}</td>
 
  </tr>
  <tr>
    <td><strong>Proveedor:</strong></td>
    <td>{$resImport['provee_desc']}</td>
 
  </tr>
</table>
</div>

<div style='width: 100%'>
<span>Estimados señores:</span>
</div>

<div style='width: 100%; padding-bottom: 5px;'>
<span>Por medio de la presente, remitimos a ustedes la traducción de la Factura Comercial</span>
</div>

<div style='width: 100%'>
<table style='width:100%; border: 1px solid black;border-collapse: collapse;'>
  <tr style='background-color: #000080; border: 1px solid black;'>
    <th style='color: white; padding: 5px;font-size: 13px; border: 1px solid black;'>ITEM</th>
    <th  style='color: white; padding: 5px;font-size: 13px; border: 1px solid black;'>CANTIDAD</th>
    <th  style='color: white; padding: 5px;font-size: 13px; border: 1px solid black;'>U.COM.</th>
    <th  style='color: white; padding: 5px;font-size: 13px; border: 1px solid black;' >DESCRIPCION</th>
    <th style='color: white; padding: 5px;font-size: 13px; border: 1px solid black;'>P/N</th>
    <th style='color: white; padding: 5px;font-size: 13px; border: 1px solid black;'>MARCA</th>
    <th style='color: white; padding: 5px;font-size: 13px; border: 1px solid black; width: 100px;'>PAIS DE ORIGEN</th>
  </tr>
  $rowProd
</table>
</div>


</div>
";

$mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
$mpdf->Output();