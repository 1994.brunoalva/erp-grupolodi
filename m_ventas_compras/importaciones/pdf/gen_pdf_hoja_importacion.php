<?php

require_once('../Lib/mpdf/vendor/autoload.php');

$html = "
<table style='width:100%'>
  <tr>
    <th colspan='6'>INGRESO MERCADERÍA DE IMPORTACIÓN - CÁMARAS </th>
    <th>FOLDER 2020-1</th> 
  </tr>
  <tr>
    <th colspan='2'>MARCA</th>
    <th>CONCORD PLUS</th>
    <th>PROCEDENCIA</th>
    <th>KOREA</th>
    <th>CTN.</th>
    <th colspan='2'>2x40HC</th>
  </tr>
  <tr>
    <th  rowspan='2'>Cant.<br>Und.</th>
    <th  rowspan='2'>Cant.<br>Cajas.</th>
    <th colspan='2'>DESCRIPCION</th>
    <th  rowspan='2'>Presentación</th>
    <th  colspan='2' rowspan='2'>TCLU5859086 <br>CTN. 01</th>
  </tr>
   <tr>
    <th >MEDIDA</th>
    <th>VALVULA</th>
  </tr>
   <tr>
    <td style='text-align: center;'>10</td>
    <td style='text-align: center;'>5</td>
    <td>26.5R25</td>
    <td>TRJ1175C</td>
    <td>2 und. x Caja</td>
    <td style='width:60px;text-align: center;'>10</td>
    <th style='width:60px;padding: 3px;'><div class='box'><span style='color: white;height: 10px;' >......</span></div></th>
  </tr>
  <tr>
    <td style='text-align: center;'>10</td>
    <td colspan='4'>Se recomienda revisar la presentación de las cámaras y/o protectores.</td>
    <td style='text-align: center;'>10</td>
    <td style='text-align: center;'>und.</td>
  </tr>
  
</table>
";
$htmlTable2="<br><table  style='width:100%'>
 <tr>
    <td colspan='7'>Observaciones:</td>
  </tr>
   <tr>
    <td colspan='7'><span style='color: white;height: 10px;' >......</span></td>
  </tr>
  <tr>
    <td colspan='2' class='texto-mediano'>FECHA LLEGADA</td>
    <td class='texto-mediano'>INICIO DE DESCARGA</td>
    <td class='texto-mediano'>FIN DE DESCARGA</td>
    <td class='texto-mediano'>CUADRILLA</td>
    <td colspan='2' class='texto-mediano'>FIRMA RESPONSABLE</td>
  </tr>
  <tr>
    <td colspan='2'><span style='color: white;' >........</span>/<span style='color: white;' >........</span>/2020<span style='color: white;' >..</span></td>
    <td><span style='color: white;' >................</span>:<span style='color: white;' >......</span></td>
    <td><span style='color: white;' >.............</span>:<span style='color: white;' >......</span></td>
    <td>si <span class='box' style='color: white;height: 10px;' >.....</span> <span style='color: white;' >..</span>no <span class='box' style='color: white;height: 10px;' >.....</span></td>
    <td colspan='2' style='height: 50px;'></td>
  </tr>
</table>";

$mpdf = new \Mpdf\Mpdf();
$stylesheet = file_get_contents('stylepdf.css');
$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($html.$htmlTable2,\Mpdf\HTMLParserMode::HTML_BODY);
$mpdf->Output();

?>


