<?php
require "../../conexion/Conexion.php";

require_once('../../Lib/mpdf/vendor/autoload.php');





$conexion = (new Conexion())->getConexion();
$sql = "SELECT 
  impor.impor_id,
  impor.impor_folder,
  impor.impor_nrfolder,
  impor.impor_nro_profo,
  empre.emp_nombre,
  prove.provee_desc,
  emb_deta.emb_det_id,
  emb_deta.emb_etd,
  tipo_co.ticon_nombre, 
  puerto.puerto_nombre,
  pais.pais_nombre,
  marca.mar_nombre 
FROM
  sys_com_embarque_detalle AS emb_deta 
  INNER JOIN sys_com_embarque AS emb 
    ON emb_deta.emb_id = emb.emb_id 
  INNER JOIN sys_com_orden AS orden 
    ON emb.ord_id = orden.ord_id 
  INNER JOIN sys_impor_folder AS impor 
    ON orden.impor_id = impor.impor_id 
  INNER JOIN sys_empresas AS empre 
    ON impor.emp_id = empre.emp_id 
  INNER JOIN sys_com_proveedor AS prove 
    ON impor.provee_id = prove.provee_id 
  INNER JOIN sys_com_tipo_contenedor AS tipo_co 
  ON impor.ticon_id = tipo_co.ticon_id 
  INNER JOIN sys_com_incoterm AS interc 
    ON impor.inco_id = interc.inco_id 
  INNER JOIN sys_com_marca AS marca 
    ON impor.mar_id = marca.mar_id 
  INNER JOIN sys_pais AS pais 
    ON impor.pais_id = pais.pais_id 
  INNER JOIN sys_com_puerto AS puerto 
    ON impor.puerto_id = puerto.puerto_id 
    
    WHERE impor.impor_id = ". $_GET['folder'];

$result1 = $conexion->query($sql);



$numfolder="";
$tipoConte="";
$importador="";
$provee="";
$puerto="";
$pais="";
$folderImpor ="";
$marcam ="";
if ($row = $result1->fetch_array()){
    $numfolder=$row['impor_nrfolder'];
    $tipoConte=$row['ticon_nombre'];
    $importador=$row['emp_nombre'];
    $provee=$row['provee_desc'];
    $puerto=$row['puerto_nombre'];
    $pais=$row['pais_nombre'];
    $folderImpor =$row['impor_folder'];
    $marcam =$row['mar_nombre'];

}
$sql="SELECT 
  det_emb.*,
  emb.emb_flete,
  despa.desp_nombre,
  nave.nave_nombre,
  folw.forwa_nombre,
  linea.linea_nombre
FROM
  sys_com_embarque_detalle AS det_emb 
  INNER JOIN sys_com_embarque AS emb 
    ON det_emb.emb_id = emb.emb_id 
  INNER JOIN sys_com_despacho AS despa 
    ON det_emb.desp_id = despa.desp_id 
  INNER JOIN sys_com_nave AS nave 
    ON det_emb.nave_id = nave.nave_id 
  INNER JOIN sys_com_forwarder AS folw 
    ON emb.forwa_id = folw.forwa_id 
  INNER JOIN sys_com_linea AS linea 
    ON det_emb.linea_id = linea.linea_id 
    WHERE emb.emb_id = ".$_GET['emb'];
$result2 = $conexion->query($sql);

$blserie="";
$despacho="";
$nave="";
$flowar="";
$fechaETD ="";
$linea ="";
$fletectn =$_GET['flete'];
$intercor =$_GET['interco'];
$thc =$_GET['thc'];
$fleteTotal =$_GET['totalf'];
$fobTotal =$_GET['totalfo'];
$ctnconte =$_GET['cntcont'];
$fechaf=formatDate($_GET['fechaf']);
$canalverde =$_GET['canal']=='VERDE'?'X':'';
$canalrojo =$_GET['canal']=='ROJO'?'X':'';
$canalnaranja=$_GET['canal']=='NARANJA'?'X':'';

if($row2 = $result2->fetch_array()){
    $blserie=$row2['emb_bl_serie'];
    $despacho=$row2['desp_nombre'];
    $nave=$row2['nave_nombre'];
    $flowar=$row2['forwa_nombre'];
    $linea=$row2['linea_nombre'];
    $fechaETD=formatDate($row2['emb_etd']);

}

$fechadesd=formatDate($_GET['desde']);
$fechaast=formatDate($_GET['ast']);


$pagosBan=array(array("","",""),array("","",""));



$sql="SELECT 
  deta_pago.*,
  banco.ban_nombre
FROM
  sys_com_pago AS pago 
  INNER JOIN sys_com_detalle_pago AS deta_pago 
    ON pago.pago_id = deta_pago.pago_id 
    INNER JOIN sys_adm_bancos AS banco  ON  deta_pago.banco= banco.ban_id
    WHERE pago.impor_id=". $_GET['folder'];

$result3 = $conexion->query($sql);
$count = 0;
foreach ($result3 as $row3){
    $pagosBan[$count]=array($row3['ban_nombre'],formatDate($row3['detpa_fecha']),$row3['detpa_monto']);
    $count++;
}

$mpdf = new \Mpdf\Mpdf();


$stylesheet = file_get_contents('stylepdf2.css');



$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);

$folderImpor = getYearFragm($folderImpor);

$columno2= "
<div style='text-align: center'><span style='font-size: 18px;' ><strong>TRANSFERENCIAS</strong></span></div>

<div style='border: 1px solid black; width: 100%; padding: 5px;margin-top: 5px;'>

   <div style='width: 100%;'>
      <div style='float: left;width:45px;font-size: 12px; '><strong>Banco:</strong> </div>
      <div style='float: left;border-bottom: 1px solid black;width: 120px;height: 20px;margin-left: 15px;font-size: 11px;'>{$pagosBan[0][0]}</div>
      <div style='float: left; font-size: 13px; width: 50px;margin-left: 3px;'> Fecha</div>
      <div style='float: left;border-bottom: 1px solid black;width: 60px;height: 20px;font-size: 11px;'>{$pagosBan[0][1]}</div>
   </div>
 <div style='width: 100%;margin-top: 5px'>
  <div style='float: left;width:45px;font-size: 12px; '><strong>Monto:</strong> </div>
    <div style='float: left;border-bottom: 1px solid black;width: 150px;height: 20px;margin-left: 15px;font-size: 11px;'>{$pagosBan[0][2]}</div>
  </div>
</div>

<div style='border: 1px solid black; width: 100%; padding: 5px;margin-top: 5px;'>

   <div style='width: 100%;'>
      <div style='float: left;width:45px;font-size: 12px; '><strong>Banco:</strong> </div>
      <div style='float: left;border-bottom: 1px solid black;width: 120px;height: 20px;margin-left: 15px;font-size: 11px;'>{$pagosBan[1][0]}</div>
      <div style='float: left; font-size: 13px; width: 50px;margin-left: 3px;'> Fecha</div>
      <div style='float: left;border-bottom: 1px solid black;width: 60px;height: 20px;font-size: 11px;'>{$pagosBan[1][1]}</div>
   </div>
 <div style='width: 100%;margin-top: 5px'>
  <div style='float: left;width:45px;font-size: 12px; '><strong>Monto:</strong> </div>
    <div style='float: left;border-bottom: 1px solid black;width: 150px;height: 20px;margin-left: 15px;font-size: 11px'>{$pagosBan[1][2]}</div>
  </div>
</div>
<!-- inicion -->


<!--  fin  -->

<div style='border: 1px solid black; width: 100%; padding: 5px;margin-top: 5px;'>
<div style='border-style: dotted; border: 2px solid black; width: 60px;padding-left: 3px;font-size: 12px'>SEG2019-</div>
<div style='width: 100%;margin-top: 5px'>
      <div style='float: left;width:45px;font-size: 12px; '><strong>Desde:</strong> </div>
      <div style='float: left;border-bottom: 1px solid black;width: 80px;height: 20px;margin-left: 15px;'>$fechadesd</div>
      <div style='float: left; font-size: 13px; width: 50px;margin-left: 3px;'> <strong>Hasta</strong> </div>
      <div style='float: left;border-bottom: 1px solid black;width: 80px;height: 20px;'>$fechaast</div>
   </div>
   
   <div style='width: 100%;margin-top: 5px'>
       <div style='float: left; width: 40px'>
            <img  style='height: 60px' src='../../imagenes/mapfre.png'>
        </div>
        <div style='float: left; width: 250px;'>
            <div style='width: 100%'>
                <div style='float: left; width: 50px;'>Póliza</div>
                <div style='border-bottom: 1px solid black;float: left; width: 100px; height: 22px;font-size: 11px'>{$_GET['polisa']}</div>
            </div>
            <div style='width: 100%;margin-top: 3px;'>
                <div style='width: 100px;font-size: 9px;text-align: center;font-weight: bold'>PRIMA</div>
            </div>
            <div style='width: 100%;'>
                <div style='float: left;border: 1px solid black; height: 25px;width: 50px;font-size: 11px'>{$_GET['neta']}</div>
                <div style='float: left;border: 1px solid black; height: 25px;width: 50px;margin-left: 3px;font-size: 11px'>{$_GET['total']}</div>
                <div style='float: left;border-bottom: 1px solid black; height: 25px;width: 100px;margin-left: 3px;font-size: 11px'>{$_GET['nas']}</div>
            </div>
            <div style='width: 100%;m'>
                <div style='float: left;width: 50px;font-size: 9px;text-align: center;font-weight: bold'>NETA</div>
                <div style='float: left;width: 50px;font-size: 9px;text-align: center;font-weight: bold'>TOTAL</div>
                <div style='float: left;width: 100px;font-size: 9px;text-align: center;font-weight: bold'>Monto asegurado</div>
            </div>
                
        </div>
    </div>
</div>

<div style='border: 1px solid black; width: 100%; padding: 5px;margin-top: 5px;'>

   <div style='width: 100%;'>
      <div style='float: left;width:12px;font-size: 12px; '><strong>$</strong> </div>
      <div style='float: left;border-bottom: 1px solid black;width: 130px;height: 20px;margin-left: 15px;'></div>
      <div style='float: left; font-size: 13px; width: 30px;margin-left: 3px;'> T/C</div>
      <div style='float: left;border-bottom: 1px solid black;width: 70px;height: 20px;'></div>
   </div>
  <div style='width: 100%;margin-top: 3px;'>
        <div style='float: left;width:120px;font-size: 12px'><strong>Liquidación: S/. </strong></div>
        <div style='float: left; border: 1px solid black; padding: 3px; text-align: center'><span style='color: white'>.</span>
        </div>
    </div>
    <div style='width: 100%;margin-top: 3px;'>
        <div style='float: left;width:120px;font-size: 12px'><strong>Percepción IGV S/.</strong></div>
        <div style='float: left; border: 1px solid black; padding: 3px; text-align: center'><span  style='color: white'>.</span>
        </div>
    </div>
</div>

<div style='width: 100%; padding: 10px;margin-top: 5px;'>
    <div style='float: left; width: 50%'>
        <div style='width: 100%;font-size: 13px;'><span>Fecha de Numeración:</span>
        </div>
        <div style='width: 100%;border: 1px solid black; padding: 3px; text-align: center'><span style='color: white;font-size: 11px;'>.</span>
        </div>
        <div style='width: 100%;font-size: 13px;'><span>Fecha de Numeración:</span>
        </div>
        <div style='width: 100%;border: 1px solid black; padding: 3px; text-align: center'><span style='color: white;font-size: 11px;'>.</span>
        </div>
    </div>
    <div style='float: left; width: 50%'>
    <div style='width: 100%;font-size: 13px;text-align: center'><span>Notas:</span>
        </div>
    </div>
</div>
<div style='width: 100%'><span style='font-size: 10px; font-style: oblique'>Si aplica Despacho Anticipado:</span></div>
<div style='border: 1px solid black; width: 100%; padding: 5px;'>

   <div style='width: 100%;margin-top: 3px'>
      <div style='float: left;width:35px;font-size: 12px; color: white;'>....</div>
      <div style='border-style: dotted;float: left;border-bottom: 1px solid black;width: 80px;height: 20px;margin-left: 15px;'></div>
      <div style='float: left; font-size: 13px; width: 80px;margin-left: 3px;font-size: 12px;'> Pago FOB</div>
      <div style='float: left;border-bottom: 1px solid black;width: 100px;height: 20px;font-size: 12px;font-style: oblique'>Solicitud de</div>
   </div>
   <div style='width: 100%;margin-top: 3px'>
      <div style='float: left;width:35px;font-size: 12px; '>  Plazos</div>
      <div style='border-style: dotted;float: left;border-bottom: 1px solid black;width: 80px;height: 20px;margin-left: 15px;'></div>
      <div style='float: left; font-size: 13px; width: 80px;margin-left: 3px;font-size: 12px;'> Pago FLETE</div>
      <div style='float: left;border-bottom: 1px solid black;width: 100px;height: 20px;font-size: 12px;font-style: oblique'></div>
   </div>
   <div style='font-size: 12px; text-align: right;width: 100%'>Vistos Buenos</div>
</div>
<div style='width: 100%'>
    <div style='width: 60px;font-size: 11px;float: left;margin-top: 5px;padding-top: 10px;'>Si aplicase:</div>
    <div style='width: 200px;float: left'>
        <div style='font-size: 11px;width: 100%'> Proforma Invoice continúa en el Fólder</div>
        <div style='font-size: 11px;width: 100%'> Proforma Invoice viene del Fólder</div>
    </div>
    <div style='border: 1px solid black;float: left; width: 40px;height: 25px;margin-top: 5px;margin-left: 5px;'></div>
</div>

<div style='width: 100%; '>


  <div style='width: 100%;'>
        <div style='float: left;width:140px;font-size: 10px'><strong>*FECHA FACT MAPFRE </strong></div>
        <div style='float: left; border: 1px solid black; padding: 3px; text-align: center'><span style='color: white;font-size: 10px;'>.</span>
        </div>
    </div>
    <div style='width: 100%;'>
        <div style='float: left;width:140px;font-size: 10px'><strong>*Nº DE FACTURA MAPFRE</strong></div>
        <div style='float: left; border: 1px solid black; padding: 3px; text-align: center'><span  style='color: white;font-size: 10px;'>.</span>
        </div>
    </div>
</div>

";

$html= "
<div style='width: 100%; background-color: #ffffff; clear: both;overflow: hidden'>
    <div style='width: 100%; background-color: #ffffff'>
        <div style='width: 70px;background-color: white;float: left'>
            <span>Fólder N°</span>
        </div>
        <div class='border_box txt-center' style='width: 100px; height: 30px; float: left; '>
            <span>$numfolder</span>
        </div>
        <div class='border_box txt-center' style='width: 260px; margin-left: 10px; height: 30px; float: left; '>

        </div>
        <div class='border_box txt-center' style='width: 100px; margin-left: 10px; height: 30px; float: left; '>
            $ctnconte
        </div>
        <div class='border_bottom txt-center' style='width: 100px; margin-left: 10px; height: 30px; float: left; '>
            <span>Containers:</span> <br>
            <span>$tipoConte</span>
        </div>
    </div>
    <div style='width: 100%; margin-top: 10px; background-color: #ffffff'>
        <div class='border_right ' style='width: 30px; float: left;'>

           $folderImpor
        </div>

        <div style='background-color: #ffffff; float: left'>
            <div class=' ' style=' margin-left: 10px;width: 250px; float: left'>
                Fecha ingreso almacén: (Fecha In) :
            </div>
            <div class='border_bottom ' style='width: 150px;height: 20px; float: left'>

            </div>
            <div class=' '
                 style=' margin-left: 10px;margin-top: 10px;width: 630px;background-color: #ffffff; float: left'>
                <table style='width:100%'>
                    <tr style='margin-bottom: 20px'>
                        <td>
                            <div style='font-size: 11px;text-align: center;background-color: #ffffff;'><strong>$despacho</strong>
                            </div>
                            <div style='font-size: 11px'>TIPO DE DESPACHO</div>
                        </td>
                        <td><span style='color: #002060; font-weight: bold; text-align: center;'>$importador</span></td>
                        <td>::::: Importador</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class='text-center' style=''><span
                                style='color: #406cba; font-weight: bold;text-align: center;'>$provee</span></td>
                        <td>::::: Exportador</td>
                    </tr>
                </table>
            </div>
        </div>


    </div>
    <div style='width: 100%; border: 1px solid #1a1a1a; margin-top: 5px;padding: 3px;'>
        <div style='width: 100%; border: 1px solid #1a1a1a;padding: 3px;'>
            <table style='width:100%'>
                <tr>
                    <td style='text-align: right'><strong>Puerto:</strong></td>
                    <td style=''>$puerto</td>
                    <td style='width: 160px; text-align: right'><strong>PAIS:</strong></td>
                    <td>$pais</td>
                    <td style='text-align: right'><strong>MARCA:</strong></td>
                    <td>$marcam</td>
                </tr>
                <tr>
                    <td style='text-align: right'><strong>Incoterm:</strongIncoterm:></td>
                    <td style=''>$intercor</td>
                    <td style='width: 160px; text-align: right'><strong>Proforma Invoice:</strong></td>
                    <td>{$_GET['profor']}</td>
                    <td style='text-align: right'><strong>FECHA:</strong></td>
                    <td>$fechaf</td>
                </tr>
                <tr>
                    <td></td>
                    <td style=''></td>
                    <td style='width: 160px; text-align: right'><strong>Commercial Invoice</strong>:</td>
                    <td>{$_GET['profor']}</td>
                    <td style='text-align: right'><strong>FECHA:</strong></td>
                    <td>$fechaf</td>
                </tr>
            </table>
        </div>
    </div>
    <div style='width: 100%; margin-top: 5px;'>
        <div style='width: 45%;float: left; text-align: center'>Monto Total Invoice USD$</div>
        <div style='width: 50%;float: left'>
            <div style='width: 100%;font-size: 20px; font-weight: bold' class='border_box txt-center'>$fobTotal</div>
        </div>
    </div>
    <div style='width: 100%'>
        <div style='width: 50%;float: left; padding: 5px'>
<div style='text-align: center'><span style='font-size: 18px;' ><strong>EMBARQUE</strong></span></div>

            <div style='border: 1px solid black; width: 100%; padding: 10px'>
                <div style='width: 100%;'>
                    <div style='float: left;width:70px'><strong>B/L Nro.</strong></div>
                    <div style='float: left; border: 1px solid black; padding: 3px; text-align: center'><span>$blserie</span>
                    </div>
                </div>
                <div style='width: 100%;'>
                    <div style='float: left;width:70px'><strong>Buque:</strong></div>
                    <div class='border_bottom' style='float: left; padding: 3px; text-align: center;font-size: 12px'>
                        <span>$nave</span></div>
                </div>
                <div style='width: 100%;'>
                    <div style='width:100%;font-size: 12px'><strong>ETD:</strong> $fechaETD<strong>Agente:</strong> $flowar
                    </div>
                </div>
                <div style='width: 100%;'>
                    <div style='width:100%;font-size: 12px'><strong>Línea Naviera:</strong> $linea</div>
                </div>
                <div style='width: 100%;'>
                    <table style='width: 100%;'>
                        <tr>
                            <td style='font-size: 12px'>Flete</td>
                            <td style='width: 150px; border: 1px solid black; text-align: right'>
                                <div style='height: 20px;font-size: 12px'>$fletectn</div>
                            </td>
                            <td style='font-size: 12px; text-align: right'>Flete x container</td>
                        </tr>
                        <tr>
                            <td style='font-size: 12px'></td>
                            <td style='width: 150px; border: 1px solid black; text-align: right'>
                                <div style='height: 20px;font-size: 12px'></div>
                            </td>
                            <td style='font-size: 12px; text-align: right'>TOTAL Flete B/L</td>
                        </tr>
                        <tr>
                            <td style='font-size: 12px'>+</td>
                            <td style='width: 150px; border: 1px solid black; text-align: right'>
                                <div style='height: 20px;font-size: 12px'>$thc</div>
                            </td>
                            <td style='font-size: 12px; text-align: right'>TOTAL THC</td>
                        </tr>
                        <tr>
                            <td style='font-size: 12px'>=</td>
                            <td style='width: 150px; border: 1px solid black; text-align: right'>
                                <div style='height: 20px;font-size: 12px'>$fleteTotal</div>
                            </td>
                            <td style='font-size: 12px; text-align: right'>Pago TOTAL</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style='border: 1px solid black; width: 100%; padding: 10px;margin-top: 5px;'>
                <div style='width: 100%;'>
                    <div style='float: left;width:120px;font-size: 12px'><strong>No. Orden Aduanas</strong></div>
                    <div style='float: left; border: 1px solid black; padding: 3px; text-align: center'><span>{$_GET['numdu']}</span>
                    </div>
                </div>
                <div style='width: 100%;margin-top: 5px'>
                    <div style='float: left;width:40px;font-size: 12px; '><strong>Canal:</strong></div>
                    <div style='float: left;border: 1px solid black;width: 30px;height: 20px;margin-left: 15px; text-align: center'>$canalverde</div>
                    <div style='float: left; font-size: 13px; width: 45px;margin-left: 3px;'> Verde</div>
                    <div style='float: left;border: 1px solid black;width: 30px;height: 20px; text-align: center;'>$canalnaranja</div>
                    <div style='float: left; font-size: 13px; width: 60px;margin-left: 3px;'> Naranja</div>
                    <div style='float: left;border: 1px solid black;width: 30px;height: 20px; text-align: center;'>$canalrojo</div>
                    <div style='float: left; font-size: 13px; width: 45px;margin-left: 3px;'> Rojo</div>
                </div>
                <div style='width: 100%;margin-top: 5px'>
                    <div style='float: left;width:120px;font-size: 12px'><strong>Número de DUA</strong></div>
                    <div style='float: left; border: 1px solid black; padding: 3px; text-align: center'><span>{$_GET['numdu']}</span>
                    </div>
                </div>
            </div>
            <div style='border: 1px solid black; width: 100%; padding: 10px;margin-top: 5px;'>
                <div style='width: 100%'>
                    <div style='float: left;width:120px;font-size: 12px'><strong>Sobrestadía</strong></div>
                    <div style='float: left; border-bottom: 1px solid black; padding: 3px; text-align: center;height: 22px; '>{$_GET['libre']}</div>
                </div>
                <div style='width: 100%;margin-top: 5px'>
                    <div style='float: left;width:120px;font-size: 12px'><strong>Almacenaje</strong></div>
                    <div style='float:left; border-bottom: 1px solid black; padding: 3px; text-align: center;height: 22px; '>{$_GET['alma']}</div>
                </div>
                <div style='width: 100%;margin-top: 5px'>
                    <div style='float: left;width:120px;font-size: 12px'><strong>Agente</strong></div>
                    <div style='float: left; border: 1px solid black; padding: 3px; text-align: center;height: 22px;'>
                        <span>$flowar</span></div>
                </div>
                <div style='width: 100%;margin-top: 5px'>
                    <div style='float: left;width:120px;font-size: 12px'><strong>Almacén</strong></div>
                    <div style='float: left; border: 1px solid black; padding: 3px; text-align: center;height: 22px;'>
                        <span style='font-size: 12px' >{$_GET['almacenn']}</span></div>
                </div>
            </div>

            <div style='border: 1px solid black; width: 100%; padding: 10px;margin-top: 5px;border-radius: 10px;'>
                <div style='width: 100%'>
                    <div class='border_bottom' style='width: 45%;float: left;margin: 3px;height:23px;'>
                    </div>
                    <div class='border_bottom' style='width: 45%;float: left;margin: 3px;height:20px;'>
                    </div>
                    <div style='width: 100%;'>
                        <div style='width: 20px; float: left'>S/.</div>
                        <div class='border_bottom' style='width: 100px; float: left; height: 22px;'></div>
                        <div class='' style='color: red;width: 70px; float: left; font-size: 13px; '>SLI-2016/
                        </div>
                        <div class='border_bottom' style='width: 100px; float: left; height: 22px;'></div>
                    </div>
                    <div style='width: 100%;'>
                        <div style='width: 50px; float: left'>$ US</div>
                        <div class='border_bottom' style='width: 100px; float: left; height: 22px;'></div>
                        <div class='' style='color: #1a1a1a; float: left; font-size: 13px; '>Servicio Logístico Integral
                        </div>
                    </div>
                </div>
            </div>
            <div style=' width: 100%; margin-top: 5px;'>
                <div style='float: left;width: 100px'>*CUADRILLA</div>
                <div style='float: left;width: 50px;text-align: right; padding-right: 3px;'>SI</div>
                <div style='float: left;width: 30px;height: 25px; border: 1px solid black; text-align: center;font-weight: bold'></div>
                <div style='float: left;width: 50px;text-align: right; padding-right: 3px;'>NO</div>
                <div style='float: left;width: 30px;height: 25px; border: 1px solid black; text-align: center;font-weight: bold'></div>

            </div>
        </div>
        <div style='width: 48%;float: left'>
            $columno2
        </div>
    </div>
</div>
";

//echo "<style>$stylesheet</style>".$html;
$mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

$mpdf->Output();




function getYearFragm($year){
    $htmlt = "";

    for ($i=0; $i <strlen($year);$i++){
        $htmlt = $htmlt . "<span style='width: 100%;text-align: center; font-weight: bold;font-size: 25px;color: #4472c4'>{$year[$i]}</span>";
    }

    return $htmlt;
}

function formatDate($date){

    $MESES =   array("ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC");
    $dater = new DateTime($date);
   // echo $date.(date_format($dater, 'm')-1);
    $res =date_format($dater, 'd')." ".$MESES[(date_format($dater, 'm')-1)]." ".date_format($dater, 'y');
    ///echo  "<br>".$res;
    return $res;
}