<?php
require "../../conexion/Conexion.php";
require_once('../../Lib/mpdf/vendor/autoload.php');

$conexcion =(new Conexion())->getConexion();


$idEmpresa = $_GET['emp'];
$idprovedor = $_GET['prov'];
$referencia= $_GET['ref'];
$marca=$_GET['mar'];
$origen = $_GET['ori'];
$numero=$_GET['num'];
$fecha =$_GET['fecha'];
$descripcion=$_GET['cate'];
$cntCon=$_GET['contc'];
$tipoCon=$_GET['tipoc'];
$costoMesca=$_GET['merc'];
$flete =$_GET['flet'];
$tasa = $_GET['tas'];
$priN=$_GET['prim'];
$cif=$_GET['cif'];
$percetTe=$_GET['perset'];

//echo floatval(str_replace(',', '', $cif));
$igvTotal =floatval(str_replace(',', '', $cif))*0.16;
$igvTotal= number_format($igvTotal, 2, '.', '');
$ipm = floatval(str_replace(',', '', $cif))*0.02;
$ipm= number_format($ipm, 2, '.', '');
$totalPredot= $_GET['aseg'];
$tasaCambio = $_GET['cambiot'];

$derechoarancelario = $ipm+$igvTotal;
$persepcion =$percetTe;
//$persepcion =( ($tasaCambio*0.001) *($derechoarancelario+floatval(str_replace(',', '', $cif))))*$tasaCambio;
$persepcion= number_format($persepcion, 2, '.', '');
$emopresa = $conexcion->query("SELECT * FROM sys_empresas WHERE emp_id=".$idEmpresa)->fetch_assoc();
$proveedor= $conexcion->query("SELECT * FROM sys_com_proveedor WHERE provee_id = ".$idprovedor)->fetch_assoc();

$totalCancelar = ($derechoarancelario * $tasaCambio)+$persepcion;

$totalCancelar= number_format($totalCancelar, 2, '.', '');

$mpdf = new \Mpdf\Mpdf();
$stylesheet = file_get_contents('stylepdf.css');

$mpdf->WriteHTML("
.table, .th, .td {
    border: 1px solid black;
    border-collapse: collapse;
}
.table{
margin-bottom: 5px;
}


", \Mpdf\HTMLParserMode::HEADER_CSS);

/*
$imgLogo = "";
$anio = date('Y');
if ($idEmpresa == 1){
    $mpdf->SetDefaultBodyCSS("background","url('./pol.jpg')");
    $mpdf->SetDefaultBodyCSS('background-image-resize', 6);
    $imgLogo="<br>
<br>";
}else{
    $imgLogo="
    <img style='max-height: 85px;' src='../../imagenes/logo/{$emopresa['emp_logo']}' />
    ";
}*/



$mpdf->SetDefaultBodyCSS('margin-bottom',0);

$html= "
<div style='width: 100%; background-color: #ffffff; '>
<img src='../../imagenes/sunat_titulo.png'>
<table  width='100%'>
  <tr>
    <td >INCOTERM: </td>
    <td>FOB</td>
    <td style='text-align: right' rowspan='2'>{$proveedor['provee_desc']}</td>
    <td rowspan='2' style='text-align: center'>::::</td>
    <td rowspan='2'>EXPORTADOR</td>
  </tr>
  <tr>
    <td>ADV:</td>
    <td>0%</td>
  </tr>
    <tr>
    
    <td colspan='3' style='text-align: right'>{$emopresa['emp_nombre']}</td>
    <td style='text-align: center'>::::</td>
    <td>IMPORTADOR</td>
  </tr>
</table>
<br/>
<br/>
<table  width='100%'>
<tr>
    <td style='width: 100px;background-color: #bfbfbf'>FÓLDER</td>
    <td style='background-color: #e5e5ff'>54</td>
    <td style='background-color: #e5e5ff'>2020</td>
    <td style='background-color: #bfbfbf;width: 100px'>CNTR(S)</td>
    <td style='background-color: #e5e5ff;width: 200px'>$cntCon</td>
    <td style='background-color: #e5e5ff'>$tipoCon</td>
  </tr>
  <tr>
    
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>MONTO FOB</td>
    <td colspan='2' style='text-align: right; font-size: 20px;'>$costoMesca</td>
  </tr>
  
  <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>PRIMA NETA SEGURO</td>
    <td colspan='2' style='text-align: right; font-size: 20px;'>$priN</td>
  </tr>
  <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>FLETE</td>
    <td colspan='2' style='text-align: right; font-size: 20px;'>$flete</td>
  </tr> 
  <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>COSTO DE ORIGEN</td>
    <td colspan='2' style='text-align: right; font-size: 20px;'>{$_GET['cosor']}</td>
  </tr>
  <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>THC</td>
    <td colspan='2' style='text-align: right; font-size: 20px;'>{$_GET['thcs']}</td>
  </tr>
  <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>CIF</td>
    <td colspan='2' style='text-align: right; background-color: #fce4d6; font-size: 20px;'><strong>$cif</strong></td>
  </tr>
  <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>ADV</td>
    <td colspan='2' style='text-align: right; font-size: 20px;'>0.00</td>
  </tr>
  <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;t'>IGV</td>
    <td colspan='2' style='text-align: right; font-size: 20px;'>{$_GET['igvp']}</td>
  </tr>
  <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>IPM</td>
    <td colspan='2' style='text-align: right; font-size: 20px;'>{$_GET['ipm']}</td>
  </tr>
  <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>DERECHOS ARANCELARIOS</td>
    <td colspan='2' style='text-align: right; font-size: 20px;'>$ {$_GET['dereco']}</td>
  </tr>
  <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>PERCEPCION</td>
    <td colspan='2' style='text-align: right; font-size: 20px;'>s/. $percetTe</td>
  </tr>
    <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>TIPO DE CAMBIO</td>
    <td colspan='2' style='text-align: right; background-color: #ffff99; font-size: 20px;'><strong>$tasaCambio</strong></td>
  </tr>
  </tr>
    <tr>
    <td colspan='3' style='border-right: 1px solid black;text-align: right; font-size: 20px;'>TOTAL A CANCELAR</td>
    <td colspan='2' style='text-align: right; font-size: 25px;'>s/. {$_GET['totalp']}</td>
  </tr>
  </tr>
    <tr>
    <td colspan='3' style='border-bottom: 1px solid black;border-right: 1px solid black;text-align: right; font-size: 25px;'></td>
    <td colspan='2' style='text-align: right; font-size: 13px;'>*Montos aproximados</td>
  </tr>
</table>
</div>
";


$mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
$mpdf->Output();




