<?php
require "../../conexion/Conexion.php";
require_once('../../Lib/mpdf/vendor/autoload.php');

$mpdf = new \Mpdf\Mpdf();
$stylesheet = file_get_contents('stylepdf.css');
$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);

$conexion = (new Conexion())->getConexion();
$iportid= $_GET['folder'];

$sql="SELECT 
impor.impor_id,
CONCAT(impor.impor_folder,'-',impor.impor_nrfolder) AS folder,
marca.mar_nombre,
pais.pais_nombre,
tico.ticon_nombre

FROM
  sys_impor_folder AS impor
  INNER JOIN sys_com_marca AS marca  ON impor.mar_id= marca.mar_id
  INNER JOIN sys_pais AS  pais ON impor.pais_id= pais.pais_id
  INNER JOIN sys_com_tipo_contenedor AS tico ON impor.ticon_id = tico.ticon_id
  WHERE impor.impor_id= $iportid";

$resul1 = $conexion->query($sql)->fetch_array();

$sql="SELECT 
  ord_deta.orddeta_id,
  cate.cat_nombre,
  prod.produ_nombre,
  unidad.unidad_nombre,
  ord_deta.prod_cantidad,
  ord_deta.prod_pre_cnt,
  ord_deta.prod_presentacion 
FROM
  sys_com_orden_detalle AS ord_deta 
  INNER JOIN sys_com_orden AS orden 
    ON ord_deta.ord_id = orden.ord_id 
  INNER JOIN sys_producto AS prod 
    ON ord_deta.produ_id = prod.produ_id 
  INNER JOIN sys_com_categoria AS cate 
    ON prod.cat_id = cate.cat_id 
  INNER JOIN sys_unidad AS unidad 
    ON prod.unidad_id = unidad.unidad_id 
    WHERE orden.impor_id = ".$iportid;

$resulProd = $conexion->query($sql);
$productos=[];

foreach ($resulProd as $prodR){
    $sql="SELECT 
  pack.*,
  SUM(pack.pack_cant) AS 'cnt_pack' 
FROM
  sys_com_packing AS pack 
  INNER JOIN sys_com_contenedor AS com 
    ON pack.con_id = com.con_id
    WHERE com.impor_id = $iportid AND pack.orddeta_id=" .$prodR['orddeta_id'];
    $tempSUM =$prodR['prod_cantidad'];
    $resTemp = $conexion->query($sql);
    if ($rowTep = $resTemp->fetch_assoc()){
        $tempSUM = $tempSUM - $rowTep['cnt_pack'];
    }
    $prodR['prod_cantidad'] = $tempSUM;
    $productos[]= $prodR;
}

//echo json_encode($productos);

$htmlRow="";


    foreach ($productos as $pro){

        if ($pro['prod_cantidad']>0){
            $presentacion = "";
            if ($pro["cat_nombre"]=="ACCESORIOS"){

                $sql = "SELECT * FROM sys_com_presentacion WHERE id = ". $pro["prod_presentacion"];
                $prodPres = $conexion->query($sql);
                if ($rowPre =$prodPres->fetch_array()){
                    $presentacion = ( $pro["prod_cantidad"] / $pro["prod_pre_cnt"] )." ".$rowPre['nombre']." c/u ". $pro["prod_pre_cnt"]." ".$pro["unidad_nombre"];
                }
            }else{
                $presentacion = $pro['unidad_nombre'];
            }
            $htmlRow = $htmlRow. "<tr>
    
            <td colspan='4'>{$pro['produ_nombre']}</td>
            <td style='width:60px;text-align: center;'>{$pro['prod_cantidad']}</td>
            <td style='width:60px;text-align: center;'>$presentacion</td>
            
            <th  colspan='2' style='width:60px;padding: 3px;'><div class='box'><span style='color: white;height: 10px;' >......</span></div></th>
          </tr>";
        }


    }

    $html = "
<table style='width:100%'>
  <tr style='background-color: #ededed'>
    <th colspan='6'>INGRESO MERCADERÍA DE IMPORTACIÓN</th>
    <th>FOLDER {$resul1['folder']}</th> 
  </tr>
  <tr>
    <th  style='background-color: #ededed' >MARCA</th>
    <td>{$resul1['mar_nombre']}</td>
    <th  style='background-color: #ededed'>PROCEDENCIA</th>
    <td>{$resul1['pais_nombre']}</td>
    <th  style='background-color: #ededed' rowspan='2'>CTN.</th>
    <th  style='background-color: #ededed' rowspan='2'>PRESENT.</th>
    <td colspan='2' style='text-align: center'><strong>1PALLET</strong></td>
  </tr>
  <tr>
    <th style='background-color: #ededed' colspan='4'>DESCRIPCION</th>
    <th  colspan='2' style='text-align: center' >PALLET #1</th>
  </tr>
   
   $htmlRow
   
  <tr>
    
    <td  style='background-color: #ededed' colspan='7'><strong>Se recomienda revisar las cantidades de cada producto mencionado.</strong></td>

  </tr>
  
</table>
";
    $htmlTable2= "<br><table  style='width:100%'>
 <tr>
    <td  style='background-color: #ededed' colspan='7'><strong> Observaciones:</strong></td>
  </tr>
   <tr>
    <td colspan='7'><br><span style='color: white;height: 40px;' >......</span></td>
  </tr>
  <tr  style='background-color: #ededed'>
    <th colspan='2' class='texto-mediano'>FECHA LLEGADA</th>
    <th class='texto-mediano'>INICIO DE DESCARGA</th>
    <th class='texto-mediano'>FIN DE DESCARGA</th>
    <th class='texto-mediano'>CUADRILLA</th>
    <th colspan='2' class='texto-mediano'>FIRMA RESPONSABLE</th>
  </tr>
  <tr>
    <td colspan='2'><span style='color: white;' >........</span>/<span style='color: white;' >........</span>/".date('Y')."<span style='color: white;' >..</span></td>
    <td><span style='color: white;' >................</span>:<span style='color: white;' >......</span></td>
    <td><span style='color: white;' >.............</span>:<span style='color: white;' >......</span></td>
    <td>si <span class='box' style='color: white;height: 10px;' >.....</span> <span style='color: white;' >..</span>no <span class='box' style='color: white;height: 10px;' >.....</span></td>
    <td colspan='2' style='height: 50px;'></td>
  </tr>
</table>";
    $mpdf->WriteHTML($html.$htmlTable2,\Mpdf\HTMLParserMode::HTML_BODY);







$mpdf->Output();


