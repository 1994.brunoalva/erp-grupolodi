<?php
require "../../conexion/Conexion.php";
require_once('../../Lib/mpdf/vendor/autoload.php');

$conexcion =(new Conexion())->getConexion();


$idEmpresa = $_GET['emp'];
$idprovedor = $_GET['prov'];
$referencia= $_GET['ref'];
$marca=$_GET['mar'];
$origen = $_GET['ori'];
$numero=$_GET['num'];
$fecha =$_GET['fecha'];
$descripcion=$_GET['cate'];
$cntCon=$_GET['contc'];
$tipoCon=$_GET['tipoc'];
$costoMesca=$_GET['merc'];
$flete =$_GET['flet'];
$tasa = $_GET['tas'];

$totalPredot= $_GET['aseg'];

$emopresa = $conexcion->query("SELECT * FROM sys_empresas WHERE emp_id=".$idEmpresa)->fetch_assoc();
$proveedor= $conexcion->query("SELECT * FROM sys_com_proveedor WHERE provee_id = ".$idprovedor)->fetch_assoc();





$mpdf = new \Mpdf\Mpdf();
$stylesheet = file_get_contents('stylepdf.css');

$mpdf->WriteHTML("
.table, .th, .td {
    border: 1px solid black;
    border-collapse: collapse;
}
.table{
margin-bottom: 5px;
}


", \Mpdf\HTMLParserMode::HEADER_CSS);


$imgLogo = "";
$anio = date('Y');
$mpdf->SetDefaultBodyCSS("background","url('../../imagenes/fondos/{$emopresa['emp_logo']}')");
$mpdf->SetDefaultBodyCSS('background-image-resize', 6);

$imgLogo="
    <img style='max-height: 80px;' src='../../imagenes/logo/{$emopresa['emp_logo']}' />
    ";

//echo $imgLogo;

$mpdf->SetDefaultBodyCSS('margin-bottom',0);

$html= "
<div style=''>
<div>
$imgLogo

<br>
<br>
<table style='margin-bottom: '  width='100%'>
  <tr>
    <td>Señores:</td>
    <td><strong>MAPFRE PERU</strong></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Sr. Guillermo Figueroa / Sr. Jose Luis Silva</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Fax: 319-1919 #102</td>
    <td class='td' style='background-color: #c0c0c0'>REF.: $referencia</td>
  </tr>
</table>

<p>San Luis,</p>
<p>Estimados Señores:</p>
<p style='text-align: justify'>Por la presente saludo muy coordialmente a usted y le ruego se sirva extender la siguiente PÓLIZA DE SEGURO DE TRANSPORTE, para nuestra mercadería la cual estamos importando, y que a continuación detallamos junto con la suma a asegurar:</p>
<h4 style='text-align: center; color: #ff0000'>Proveedor</h4>
<h3 style='text-align: center; color: #ff0000'>{$proveedor['provee_desc']}</h3>


</div>
<div style='width: 100%'>
<div style='background-color: #ffffff; width: 60%; float: left'>
<table class='table' width='100%'>
  <tr>
    <td class='td' style='width: 70px;background-color: #c0c0c0'>Marca</td>
    <td class='td'>$marca</td>
    
  </tr>
  <tr>
    <td class='td'  style='width: 70px;background-color: #c0c0c0'>Origen</td>
    <td class='td'>$origen</td>
    
  </tr>
</table>

<table class='table' width='100%'>
  <tr>
    <td class='td' colspan='2' style='text-align: center;background-color: #c0c0c0'><strong>Referencia Doc.</strong></td>
   
  </tr>
  <tr>
    <td class='td'  style='width: 70px;background-color: #c0c0c0'>Numero</td>
    <td class='td'>$numero</td>
    
  </tr>
  <tr>
    <td  style='width: 70px;background-color: #c0c0c0'>Fecha</td>
    <td  class='td'>$fecha</td>
    
  </tr>
</table>
<table class='table' width='100%'>
  <tr>
    <td class='td' colspan='2' style='text-align: center;background-color: #c0c0c0'><strong>Descripcion</strong></td>
   
  </tr>
  <tr>

    <td class='td' colspan='2'>$descripcion</td>
    
  </tr>

</table>

</div>
<div style='background-color: white; width: 38%; float: right'>

<table class='table' width='100%'>
  <tr>
    <td class='td' style='width: 100px;background-color: #c0c0c0'>Container</td>
    <td class='td' style='text-align: center'>$cntCon</td>
    
  </tr>
  <tr>
    <td class='td'  style='width: 100px;background-color: #c0c0c0'>Tipo de Cont.</td>
    <td class='td'  style='text-align: center'>$tipoCon</td>
    
  </tr>
</table>
<table class='table' width='100%'>
  <tr>
    <td class='td' style='width: 100px;background-color: #c0c0c0'>Costo de la mercaderia</td>
    <td class='td'  style='text-align: center'>USD $ $costoMesca</td>
    
  </tr>
  <tr>
    <td class='td'  style='width:100px;background-color: #c0c0c0'>Flete</td>
    <td class='td'  style='text-align: center'>USD $ $flete</td>
    
  </tr>
   <tr>
    <td class='td'  style='width: 100px;background-color: #c0c0c0'>Total Suma Asegurada</td>
    <td class='td'  style='text-align: center'>USD $ $totalPredot</td>
    
  </tr>
</table>
</div>
</div>
<ol> 
<h4>TERMINOS</h4>
<li style='text-align: justify'>Cubrimiento total (Mercadería + Flete ) </li>
<li  style='text-align: justify'>Indicar en forma precisa en la póliza el nombre de nuestro proveedor y el número de proforma. </li>
<li  style='text-align: justify'>La tasa pactada entre nuestra empresa y Mapfre Peru para este producto la cual es $tasa </li>
<li  style='text-align: justify'>El seguro cubrirá desde el depósito de embarque en origen hasta nuestros almacenes </li>
<li  style='text-align: justify'>Cláusula \"A\", a todo riesgo </li>
</ol>
<p>Agradeciendo su gentil atención nos despedimos.</p>
<div style='width: 80%;text-align: right;float: right'>
<div style='padding-bottom: 0px; padding-right: 160px'>
<span >Atentamente,</span>
</div>


<img style='max-width: 150px; ' src='../../imagenes/firma/grupolodi.png'/>
</div>
</div>
</div>
";


$mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
$mpdf->Output();




