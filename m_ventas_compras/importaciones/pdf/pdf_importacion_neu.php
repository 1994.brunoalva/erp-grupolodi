<?php
require_once '../../entidadDB/DataBase.php';
require_once '../../model/Incoterm.php';
require_once '../../model/Categoria.php';
require_once '../../model/Pais.php';
require_once '../../model/Proveedores.php';
require_once '../../model/TipoDocumento.php';
require_once '../../model/Folder.php';
require_once '../../model/Empresa.php';
require_once '../../model/TipoContenedor.php';
require_once '../../model/Marca.php';
require_once '../../model/Puerto.php';
require_once '../../model/Aduana.php';
require_once '../../model/AgenciaAduana.php';
require_once '../../model/Canal.php';
require_once '../../model/Orden.php';
require_once '../../model/Almacen.php';
require_once '../../model/Despacho.php';

require_once '../../model/Forwarder.php';
require_once '../../model/Linea.php';
require_once '../../model/Nave.php';
require_once '../../model/Embarque.php';
require_once '../../model/Unidad.php';
require_once '../../model/TipoProducto.php';
require_once '../../model/CodSunat.php';
require_once '../../model/Nomenglatura.php';
require_once '../../model/Producto.php';
require_once '../../model/Modelo.php';
require_once '../../model/ProductoDetalle.php';
require_once '../../model/Moneda.php';
require_once '../../model/CambioMoneda.php';

require_once '../../model/OrdenDetalle.php';
require_once '../../model/TasaPoliza.php';
require_once '../../model/Poliza.php';
require_once '../../model/Pagos.php';
require_once '../../model/CondicionPagos.php';
require_once '../../model/DetallePagos.php';
require_once '../../model/DetalleEstado.php';
require_once '../../model/Estado.php';
require_once '../../model/Contenedor.php';
require_once '../../model/Packing.php';
require_once '../../model/Precentacion.php';


require_once('../../Lib/mpdf/vendor/autoload.php');

$mpdf = new \Mpdf\Mpdf();
$stylesheet = file_get_contents('stylepdf.css');

$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);

include 'generador_neumaticos.php';


$anio = date('Y');

$htmlfooter="<table  style='width:100%'>
 <tr>
    <td colspan='7' class='congris'>Observaciones:</td>
  </tr>
   <tr>
    <td colspan='7'><span style='color: white;height: 10px;' >......</span></td>
  </tr>
  <tr class='congris'>
    <td colspan='2' class='texto-mediano'>FECHA LLEGADA</td>
    <td class='texto-mediano'>INICIO DE DESCARGA</td>
    <td class='texto-mediano'>FIN DE DESCARGA</td>
    <td class='texto-mediano'>CUADRILLA</td>
    <td colspan='2' class='texto-mediano'>FIRMA RESPONSABLE</td>
  </tr>
  <tr>
    <td colspan='2'><span style='color: white;' >........</span>/<span style='color: white;' >........</span>/$anio<span style='color: white;' >..</span></td>
    <td><span style='color: white;' >................</span>:<span style='color: white;' >......</span></td>
    <td><span style='color: white;' >.............</span>:<span style='color: white;' >......</span></td>
    <td>si <span class='box' style='color: white;height: 10px;' >.....</span> <span style='color: white;' >..</span>no <span class='box' style='color: white;height: 10px;' >.....</span></td>
    <td colspan='2' style='height: 50px;'></td>
  </tr>
</table>";
$mpdf->WriteHTML($htmlfooter,\Mpdf\HTMLParserMode::HTML_BODY);
$mpdf->Output();




